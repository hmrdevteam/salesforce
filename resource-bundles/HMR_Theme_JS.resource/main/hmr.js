var barContainer;var strengthBar;var progress = 42;var totalItemLimit = 42;
var pb = {
    l: function() {
        barContainer = document.querySelector('#strength-bar');
        strengthBar = new ProgressBar.Circle(barContainer, {
            color: '#00CC66',
            trailColor: '#f1f1f2',
            duration: 500,
            easing: 'easeOut',
            strokeWidth: 2
        });
        strengthBar.setText(progress + '/' + totalItemLimit);
    }
};
var bs = {//boostrap initializations
    t: function() {
        $('[data-toggle="tooltip"]').tooltip();
    }
};
var kc = {//kit config initializations
    s: function() {//scroll
        $(window).scroll(function(event){
            if($(window).scrollTop() == 0) {
                $("nav").addClass("navbar-fixed-top");
                $(".kit-config-header").removeClass("navbar-fixed-top");
            }
            else {
                $("nav").removeClass("navbar-fixed-top");
                $(".kit-config-header").addClass("navbar-fixed-top");
            }
        });
    }
};
window.addEventListener("orientationchange", function() {
    cldg.m_o_c();
});
var clinic_info_Id = "";var clinic_account_CompanyName = "";
var cldg = {
    n: function(s, c) {
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, cldg.ps, c); // Not Read-only mode
    },
    v: function() {
        $("#clinicLandingInfoForm").validate({
            rules: {
                clinicFN: {
                    required: true,
                    maxlength: 80
                },
                clinicLN: {
                    required: true,
                    maxlength: 80
                },
                clinicEmail: {
                    required: true,
                    email: true
                }
            },
            messages: {
                clinicFN :  {
                    required: 'Please enter your First Name',
                    maxlength: 'Invalid First Name, please enter less than 80 characters'
                },
                clinicLN :  {
                    required: 'Please enter your Last Name',
                    maxlength: 'Invalid Last Name, please enter less than 80 characters'
                },
                clinicEmail: {
                    required: 'Please enter your Email Address',
                    email: 'Please enter a valid Email Address'
                }
            }
        });
        cldg.b();
    },
    r: function() {
        var s = 'HMR_CMS_ClinicLanderHero_ContentTemplate';
        if(window.doServiceRequest) {
            cldg.n(s, cldg.c);
        }
    },
    c: function(t, d) {
        $(".clinic-info-to-hide").addClass("hidden");
        if(d.success) {
            $("#clinicInfoModalHeader").html("Thank you for your submission.");
        }
        else {
            $("#clinicInfoModalHeader").html("Something went wrong! Please refresh this page");
        }
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            cldg["ps"] = {
                action: "insertClinicLandingFormLead",
                Account__c: clinic_info_Id,
                Company: clinic_account_CompanyName,
                FirstName: $("#clinicFN").val(),
                LastName: $("#clinicLN").val(),
                Email: $("#clinicEmail").val(),
                Phone: $("#clinicPhone").val(),
                LeadSource: "Website",
                hmr_Lead_Source_Detail__c: "Clinic Landing Form",
                How_did_you_hear_about_us__c: $("#clinicSource").val(),
                hmr_Lead_Type__c: "Clinic"
            };
            if($("#tc_orientation").length > 0 && $("#tc_orientation").val() != null) {
                var o = $("#tc_orientation").val().split(", ");
                cldg["ps"].hmr_Orientation_Day__c = o[0];
                cldg["ps"].hmr_Orientation_Time__c = o[1];
            }
            cldg.r();
        }(window, STG.CTLib.Idea || {}));
    },
    b: function() {//clinicInfoRequestInformationBtn click handler
        $("#clinicInfoRequestInformationBtn").unbind("click").on("click", function() {
            if(clinic_info_Id == "") {
                $(".clinic-info-radiobtn-error").removeClass("hidden");
            }
            else {
                $(".clinic-info-radio-error").addClass("hidden");
                if($("#clinicLandingInfoForm").valid() && clinic_info_Id != "") {
                    cldg.i();
                }
            }
            return false;
        });
    },
    m_o_c: function(t) {//double modal
        $("input[name='clinicInfoRadio']").prop("checked", "false");
        $("#" + $(t).data("id")).prop("checked", "true");
        clinic_info_Id = $(t).data("id");
        clinic_account_CompanyName = $(t).data("name");
        cldg.v();
    },
    o: function() {//on radio change
        $("input[name='clinicInfoRadio']").each(function() {
            if($(this).prop("checked")) {
                clinic_info_Id = $(this).attr("id");
                clinic_account_CompanyName = $(this).data("name");
                $(".clinic-info-radiobtn-error").addClass("hidden");
            }
        });
    },
    s: function(id, n) {//single modal
        clinic_info_Id = id;
        clinic_account_CompanyName = n;
        cldg.v();
    },
    f: function() {
        if($(".clinicDoubleModal").length == 0) {
            var id = $("#clinicInfoModalDialog").data("id");
            var n = $("#clinicInfoModalDialog").data("name");
            cldg.s(id, n);
        }
        cldg.v();
    },
    gs: function() {//get started button override in support hands
        if(window.location.pathname.indexOf("hmr-weight-loss-clinics") != -1 || window.location.pathname.indexOf("lexington") != -1) {
            var a = $(".bg-supporthands").find("a");
            if(a.length > 0) {
                a.removeAttr("href");
                a.attr("data-toggle", "modal");
                a.attr("data-target", "#clinicInfoModalDialog");
                a.attr("onclick", "javascript:cldg.f();");
            }
            var b = $(".bg-usnews").find("a");
            if(b.length > 0) {
                $.each(b, function(i, v) {
                    v.target = "_blank";
                });
            }
        }
    }
};
var profpg = {
    n: function(s, c) {
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, profpg.ps, c); // Not Read-only mode
    },
    v: function() {
        $("#professionalsLeadForm").validate({
            rules: {
                professionalsFN: {
                    required: true,
                    maxlength: 80
                },
                professionalsLN: {
                    required: true,
                    maxlength: 80
                },
                professionalsJT: {
                    required: true,
                    maxlength: 80
                },
                professionalsPhone: {
                    required: true,
                    phoneUS: true
                },
                professionalsCompany: {
                    required: true,
                    maxlength: 80
                },
                professionalsEmail: {
                    required: true,
                    email: true
                },
                professionalsAddress1: {
                    required: true,
                    maxlength: 255
                },
                professionalsCity: {
                    required: true,
                    maxlength: 80
                },
                professionalsState: {
                    required: true
                },
                professionalsZipCode: {
                    required: true,
                    minlength: 5,
                    maxlength: 5
                },
                professionalsHMRSetting: {
                    required: true
                }
            },
            messages: {
                professionalsFN :  {
                    required: 'Please enter your First Name',
                    maxlength: 'Invalid First Name, please enter less than 80 characters'
                },
                professionalsLN :  {
                    required: 'Please enter your Last Name',
                    maxlength: 'Invalid Last Name, please enter less than 80 characters'
                },
                professionalsJT: {
                    required: 'Please enter your Job Title',
                    email: 'Please enter a valid Job Title'
                },
                professionalsPhone: {
                    required: 'Please enter your phone number',
                    phoneUS: 'Please enter a valid phone number'
                },
                professionalsCompany: {
                    required: 'Please enter your Company name',
                    maxlength: 'Only 80 characters allowed'
                },
                professionalsEmail: {
                    required: 'Please enter your Email Address',
                    email: 'Please enter a valid Email Address'
                },
                professionalsAddress1: {
                    required: 'Please enter your street address',
                    maxlength: 'Only 255 characters allowed'
                },
                professionalsCity: {
                    required: 'Please enter your City',
                    maxlength: 'Only 80 characters allowed'
                },
                professionalsState: {
                    required: 'Please select your state'
                },
                professionalsZipCode: {
                    required: 'Please enter a zip code',
                    minlength: 'Please enter a valid 5 digit zip code',
                    maxlength: 'Please enter a valid 5 digit zip code'
                },
                professionalsHMRSetting: {
                    required: 'Please select a valid HMR setting'
                }
            }
        });
        profpg.b();
    },
    r: function() {
        var s = 'HMR_CMS_ProfessionalHero_ContentTemplate';
        if(window.doServiceRequest) {
            profpg.n(s, profpg.c);
        }
    },
    c: function(t, d) {
        $(".clinic-info-to-hide").addClass("hidden");
        if(d.success) {
            $("#professionalModalHeader").removeClass("hidden");
        }
        else {
            $("#professionalModalHeaderFailure").removeClass("hidden");
        }
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            profpg["ps"] = {
                action: "insertProfessionalsPageFormLead",
                FirstName: $("#professionalsFN").val(),
                LastName: $("#professionalsLN").val(),
                Title: $("#professionalsJT").val(),
                Email: $("#professionalsEmail").val(),
                Company: $("#professionalsCompany").val(),
                Phone: $("#professionalsPhone").val(),
                Street: $("#professionalsAddress1").val() + ', ' + $("#professionalsAddress2").val(),
                City: $("#professionalsCity").val(),
                State: $("#professionalsState").val(),
                PostalCode: $("#professionalsZipCode").val(),
                hmr_Setting__c: $("#professionalsHMRSetting").val()
            };
            profpg.r();
        }(window, STG.CTLib.Idea || {}));
    },
    b: function() {//clinicInfoRequestInformationBtn click handler
        $("#professionalsLeadRequestInformationBtn").unbind("click").on("click", function() {
            if($("#professionalsLeadForm").valid()) {
                profpg.i();
            }
            return false;
        });
    }
};
var plans_c = {
    cs: function() {//coaching solutions
        if(window.location.pathname.indexOf("healthy-solutions-at-home-diet-plan") != -1) {
            var s = $(".bg-coachingsolutions").find(".col-sm-4");
            var html = '';
            if(s.length > 0) {
                html += '<div class="notes" style="text-align: left;margin-top: 30px;font-size: 16px;">' +
                            '*Average weight loss for the Healthy Solutions at Home program with phone coaching is 23 lbs. at 12 weeks. ' +
                            '<a href="http://www.nature.com/ijo/journal/v31/n8/full/0803568a.html" target="_blank">Donnelly JE et al. Int J Obes 2007;31:1270-1276.</a> ' +
                            'Average weight loss without coaching is 13 lbs. at 12 weeks. ' +
                            '<a href="http://www.obesityresearchclinicalpractice.com/article/S1871-403X(09)00033-7/abstract" target="_blank">Obes Clin Pract 2009:3:149-157</a>' +
                        '</div>';
                s.append(html);
            }
        }
    }
};
var subscribe = {
    n: function(s, c) {
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, subscribe.ps, c); // Not Read-only mode
    },
    cc: function() {//constant contact
        var formData = "";
        formData += "firstName=" + $("#subscribeModalFN").val();
        formData += "&lastName=" + $("#subscribeModalLN").val();
        formData += "&email=" + $("#subscribeModalEmail").val();
        formData += "&ca=7e444613-7d32-470c-be42-03c92c963375";
        if(window.location.href.indexOf('Knowledge') != -1) {
            formData += "&list=1128393743";
        }
        else if(window.location.href.indexOf('Recipe') != -1) {
            formData += "&list=1903706083";
        }
        else if(window.location.href.indexOf('Home') != -1) {
            formData += "&list=1986788089";
        }
        else {
            formData += "&list=1986788089";
        }
        $.ajax({
            type : 'POST', // HTTP method, always use POST for our form
            url : 'https://visitor2.constantcontact.com/api/signup',
            data : formData, // serialized form data
            dataType : 'json', // the type of data we expect back from the server
            success: function(data){
                $("#ConfirmationModalDialogHeader").html("Thank you for Joining our Mailing List");
                $("#hmrSubscribeModal").modal("hide");
                $("#ConfirmationModalDialog").modal("show");
                subscribe.mailGA();
                $(".bg-subscribe").find(".btn").attr("data-target", "#ConfirmationModalDialog");
            },
            error: function(response) {
                $("#ConfirmationModalDialogHeader").html("Something went wrong! Please refresh your page");
                $("#hmrSubscribeModal").modal("hide");
                $("#ConfirmationModalDialog").modal("show");
            }
        });
    },
    c: function(t, d) {
        if(d.success) {
            if(d.message != "Lead Created No CC") {
                subscribe.cc();
            }
            else {
                $("#ConfirmationModalDialogHeader").html("Thank you for Joining our Mailing List");
                $("#hmrSubscribeModal").modal("hide");
                $("#ConfirmationModalDialog").modal("show");
                subscribe.mailGA();
            }
        }
        else {
            $("#subscribeModalSubmitBtn").removeAttr("disabled");
            $("#hmrSubscribeModal").modal("hide");
            $("#ConfirmationModalDialog").modal("show");
            $("#ConfirmationModalDialogHeader").html(d.message);
        }
    },
    r: function() {
        var s = 'HMR_CMS_Subscribe_ServiceInterface';
        if(window.doServiceRequest) {
            subscribe.n(s, subscribe.c);
        }
    },
    //Hello and Bye
    mailGA: function() {
        dataLayer.push({
                        'event' : 'subscribeSuccess',
                          'ecommerce': {
                            'click': [
                             {
                               'name': "ccLeadCreated",
                               'id': $("#subscribeModalEmail").val()
                             }]
                          }
                        });
        console.log("subscribeSuccess");
    },
    sf: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            subscribe["ps"] = {
                action: "createLead",
                firstName: $("#subscribeModalFN").val(),
                lastName: $("#subscribeModalLN").val(),
                email: $("#subscribeModalEmail").val(),
                zipcode: $("#subscribeModalZipCode").val()
            };
            subscribe.r();
        }(window, STG.CTLib.Idea || {}));
    },
    b: function() {//subscribeModalSubmitBtn click handler
        $("#subscribeModalSubmitBtn").on("click", function() {
            if($("#hmrSubscribeModalForm").valid()) {
                $("#subscribeModalSubmitBtn").attr("disabled", "disabled");
                subscribe.sf();
            }
        });
    },
    f: function() {
        $("#hmrSubscribeModalForm").validate({
            rules: {
                subscribeModalFN: {
                    required: true,
                    maxlength: 80
                },
                subscribeModalLN: {
                    required: true,
                    maxlength: 80
                },
                subscribeModalEmail: {
                    required: true,
                    email: true
                },
                subscribeModalZipCode: {
                    number: true,
                    maxlength: 5,
                    minlength: 5
                }
            },
            messages: {
                subscribeModalFN :  {
                    required: 'Please enter your First Name',
                    maxlength: 'Invalid First Name, please enter less than 80 characters'
                },
                subscribeModalLN :  {
                    required: 'Please enter your Last Name',
                    maxlength: 'Invalid Last Name, please enter less than 80 characters'
                },
                subscribeModalEmail: {
                    required: 'Please enter your Email Address',
                    email: 'Please enter a valid Email Address'
                },
                subscribeModalZipCode: {
                    number: 'Invalid Zip Code, Please enter numbers only',
                    maxlength: 'Invalid Zip Code, Please enter only 5 digits',
                    minlength: 'Invalid Zip Code, Please enter only 5 digits'
                }
            }
        });
        subscribe.b();
    },
    i: function() {
        if($(".bg-subscribe").length > 0) {
            var b = $(".bg-subscribe").find(".btn");
            if(typeof b != "undefined") {
                b.attr("data-toggle", "modal");
                b.attr("data-target", "#hmrSubscribeModal");
                subscribe.f();
            }
        }
    }
}
var h_footer = {
    i: function() {
        var q = "";
        var h = "";
        // var a = $("#link-shop-hmr-foods");
        // if(location.href.indexOf('?') != -1) {
        //     q = location.href.substring(location.href.indexOf('?') + 1);
        // }
        // if(location.href.indexOf("DefaultStore") != -1) {
        //     if(hmr_u.g("portalUser") != null && hmr_u.g("portalUser") != "") {
        //         h += "Products?categoryId=" + a.attr("data-c");
        //     }
        //     else {
        //         h += "shop-hmr-foods?categoryId=" + a.attr("data-c");
        //     }
        // }
        // else {
        //     if(hmr_u.g("portalUser") != null && hmr_u.g("portalUser") != "") {
        //         h += "/Products?categoryId=" + a.attr("data-c");
        //     }
        //     else {
        //         h += "/shop-hmr-foods?categoryId=" + a.attr("data-c");
        //     }
        // }
        // if(q != "") {
        //     h += "&" + q;
        // }
        // a.attr("href", h);
    }
};
var c_a_guest = { //continue as guest
    i: function() {
        if(location.href.indexOf("sign-in") != -1 && $("#continue-as-guest").length == 1) {
            var c = $("#continue-as-guest").attr("data-cartId");
            var q = "";
            var cs = "";
            if(location.href.indexOf('?') != -1 ) {
                q = location.href.substring(location.href.indexOf('?') + 1);
            }
            if(hmr_u.g("cartID") == null && hmr_u.g("cartID") == "") {
                cs = "&" + "cartID=" + c;
            }
            var d = "";
            if(location.href.indexOf("/DefaultStore") != -1) {
                d = "/DefaultStore";
            }
            var h = d + "/checkout?" + q + cs;
            $("#continue-as-guest, #continue-as-guest-carat").attr("href", h);
        }
    }
};
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}
/**
* Main JavaScript for the HMR Community
*
* @Date: 2016-12-06
* @Author Pranay Mistry (Magnet 360)
*/
var HMR = [];
(function(){
    'use strict';
    delegateEvent('[data-toggle="collapse"]', 'click', collapseToggleHandler);
    delegateEvent('[data-toggle="collapse-card"]', 'click', collapseCardToggleHandler);
    delegateEvent('.shakes', 'change', shakeFlavorSelectionHandler);
    function shakeFlavorSelectionHandler(event) {
        if ( this.checked ) {

            // Get currently selected SKU
            var currentSku = window.location.href.slice(window.location.href.indexOf('sku=') + 4);

            // Remove flavor from currently selected SKU
            var skuWithoutFlavor =
                currentSku.substr(0, currentSku.length - (currentSku.endsWith('VC') ? 2 : 1));

            // Calculate newly selected Flavor
            var selectedSkuFlavor;
            console.log('this.nextSibling.data: ' + this.nextSibling.data);
            if ( this.nextSibling.data == 'Vanilla' ) {
                selectedSkuFlavor = 'V';
            } else if ( this.nextSibling.data == 'Chocolate' ) {
                selectedSkuFlavor = 'C';
            } else {
                selectedSkuFlavor = 'VC';
            }

            // If user chose a new flavor, redirect with the new SKU
            if ( currentSku != (skuWithoutFlavor + selectedSkuFlavor) ) {
                var url =
                    window.location.href.substring(0, window.location.href.indexOf('?')) +
                        '?sku=' + skuWithoutFlavor + selectedSkuFlavor;

                window.location.href = url;
            }
        }
    }
    function collapseToggleHandler(event) {
        var target = (this.dataset['target']) ? document.getElementById(this.dataset['target']) : null;
        if(target && target.firstElementChild){
            event.preventDefault();
            if(target.classList.contains('in')){
                target.style.height = null;
            } else {
                //target.style.height = target.firstElementChild.offsetHeight + 'px';
                target.style.height = '300px';
            }
            target.classList.toggle('in');
            this.classList.toggle('collapse-in');
        }
    }
    function collapseCardToggleHandler(event) {
        var target = (this.dataset['target']) ? document.getElementById(this.dataset['target']) : null;
        if (target && target.firstElementChild) {
            event.preventDefault();
            if (target.classList.contains('in')) {
                target.style.height = null;
            } else {
                //target.style.height = target.firstElementChild.offsetHeight + 'px';
                target.style.height = '420px';
            }
            target.classList.toggle('in');
            this.classList.toggle('collapse-in');
        }
    }
    /**
     * Handle events whose target matches a selector.
     * @param  {string} selector     CSS selector
     * @param  {string} eventType    The type of event to handle
     * @param  {function} handler    The event handler
     * @param  {object} elementScope (optional) Defaults to document
     * @return {void}
     */
    function delegateEvent(selector, eventType, handler, elementScope) {
        (elementScope || document).addEventListener(eventType, function(event) {
            var listeningTarget = closest(event.target, selector);
            if (listeningTarget) {
                handler.call(listeningTarget, event);
            }
        });
    }
    /**
     * Get an element's nearest parent element matching a selector
     * @param  {object} el           Element to compare with
     * @param  {object} selector     CSS selector
     * @return {object}              Returns a DOM element, or null if no match is found
     */
    var closest = (function() {
        var el = HTMLElement.prototype;
        var matches = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;

        return function closest(el, selector) {
            if(!el || !selector){
                return null;
            }
            return matches.call(el, selector) ? el : closest(el.parentElement, selector);
        };
    })();
    function checkWidth() {
        var windowsize = $(window).width();
        if (windowsize >= 768) {
            $('#primaryNav').css('height', '');
        }
    };
    $(window).resize(checkWidth);
})();
var ccContextItems = [];var loginUrl = "sign-in";var myAccountUrl = "my-account";var loginLander = "HMR_Community_LoginLander";
var isGuest = true;var site_prefix = "";var first_name = "";var welcomeUrl = "welcome";var plansUrl = "healthy-solutions-at-home-diet-plan";
var hmr_a = {
    r: function() {
        $("#hmr_register_btn").click(function(e) {
            window.location.href = "register";
        });
    },
    c: function() {
        if(typeof CCRZ != "undefined") {
            if(hmr_u.g("portalUser") == null || hmr_u.g("portalUser") == "") {
                cartDetails = function() {
                    if(document.location.href.indexOf("/DefaultStore") != -1) {
                        document.location = "/DefaultStore/shopping-cart?cartID=" + CCRZ.pagevars.currentCartID + getCSRQueryString();
                    }
                    else {
                        document.location = "/shopping-cart?cartID=" + CCRZ.pagevars.currentCartID + getCSRQueryString();
                    }
                }
            }
            $("#cartli_wrapper a").attr("href", "javascript:cartDetails('" + CCRZ.pagevars.currentCartID + "')");
            CCRZ.pubSub.on("view:cartHeaderView:refresh", function(viewRef) {
                var c = $(".shopping-cart-count-bubble").html();
                if(c != null && c != "" && parseInt(c) > 0) {
                    cc.b(parseInt(c));
                }
            });
        }
    },
    f: function() {
        var l = $("#footer nav:eq(1)").find("li.first").find("a");
        if(l.length > 0) {
            hmr_u.a(l, l.attr("href"));
        }
    },
    e: function() {
        $(".eyeclosed").on("click", function(e) {
            var i = $(this).parent().parent().find("input");
            var v = i.val();
            if(i.attr("type").toLowerCase() == "password") {
                i.attr("type", "text");
            }
            else {
                i.attr("type", "password");
            }
            i.val(v);
            e.preventDefault();
        });
    },
    g: function() {
        if(typeof CMS == "undefined" && typeof CCRZ != "undefined") {
            isGuest = CCRZ.pagevars.isGuest;
            CCRZ.pubSub.on('view:myAccountHeaderView:refresh', function(theView) {
                if(typeof CCRZ.currentUser != "undefined") {
                   first_name = CCRZ.currentUser.FirstName;
                }
                w.n();
                var c = $(".shopping-cart-count-bubble").html();
                if(c != null && c != "" && parseInt(c) > 0) {
                    cc.b(parseInt(c));
                }
            });
            site_prefix = CCRZ.pagevars.currSiteURL;
            HMR['pageVars'] = { 'cartId': CCRZ.pagevars.currentCartID };
        }
        if(typeof CCRZ == "undefined" && typeof CMS != "undefined") {
            isGuest = CMS.isGuest;
            first_name = CMS.first_name;
            if(CMS.site_prefix != null) {
                site_prefix = CMS.site_prefix + "/";
            }
        }
        if(typeof CCRZ == "undefined" && typeof CMS == "undefined") {
            if(window.location.href.indexOf("Zip-Code-Finder") == -1) {
                isGuest = false;
            }
            first_name = $("#hmr_vf_first_name").val();
            w.n();
        }
    },
    i: function() {
        hmr_a.c();
        hmr_a.r();
        hmr_a.e();
        hmr_a.g();
    }
};
/* Welcome */
var w = {
    b: function() {
        window.location.href = site_prefix + myAccountUrl;
    },
    i: function() {
        if(window.location.pathname.indexOf("welcome") != -1 || window.location.pathname.indexOf("consent") != -1) {
            $("#welcome_user").html("Welcome " + first_name);
        }
        w.n();
    },
    n: function() {
        if(isGuest) {
            $(".hdr_user_fname").html("Sign In");
            $(".hmrSignIn").attr("href", site_prefix + "/" + loginUrl);
            $(".submenu").addClass("hidden");
            $(".hmrSignIn").removeClass("hidden");
        }
        else {
            $(".hdr_user_fname").html(first_name);
            $(".submenu").removeClass("hidden");
            $(".hmrSignIn").addClass("hidden");
            if(typeof CCRZ == "undefined" || typeof CMS != "undefined") {
                hmr_u.a($(".hmrSignIn"), site_prefix + "/" + myAccountUrl);
            }
            else {
                $(".hmrSignIn").attr("href", "javascript:void(0);");
                $(".hmrSignIn").on("click", function() {
                    if(hmr_u.g("portalUser") != null && hmr_u.g("portalUser") != "") {
                        CCRZ.headerView.goToAccount();
                    }
                    else {
                        var d = "";
                        if(document.location.href.indexOf("/DefaultStore") != -1) {
                            d = "/DefaultStore";
                        }
                        if(typeof CCRZ != "undefined") {
                            window.location.href = d + '/my-account?cartID=' + CCRZ.pagevars.currentCartID + '&viewState=viewAccount' + getCSRQueryString() ;
                        }
                        else {
                            window.location.href = d + '/my-account';
                        }
                    }
                });
            }
        }
    }
};
/* Registration */
var r = {
    r: function() {
        if(!isGuest && window.location.pathname.toLowerCase().indexOf("register") != -1) {
            window.location.href = site_prefix;
        }
        else {
            $.validator.addMethod("alphanumeric", function(value, element) {
                    // allow any non-whitespace characters as the host part
                    return this.optional( element ) || /[a-z].*[0-9]|[0-9].*[a-z]/i.test( value );
                },
                'Please enter a valid email address.'
            );
            $("#hmr_registration").validate({
                errorLabelContainer: $("#hmr_registration div.form-error"),
                rules :{
                    r_fn : {
                        required : true,
                        maxlength: 255
                    },
                    r_ln : {
                        required : true,
                        maxlength: 255
                    },
                    r_email : {
                        required : true,
                        email: true
                    },
                    r_dn : {
                        maxlength: 40
                    },
                    r_password : {
                        required : true,
                        minlength: 6,
                        alphanumeric: true
                    },
                    r_cpassword : {
                        equalTo: "#hmr_reg_inputPassword"
                    },
                    r_zipcode : {
                        required : true,
                        minlength: 5,
                        maxlength: 5,
                        digits: true
                    },
                    r_termsprivacy : {
                        required : true
                    }
                },
                messages: {
                    r_fn : {
                        required : 'Please enter your First Name',
                        maxlength: 'Please enter less than 255 characters'
                    },
                    r_ln : {
                        required : 'Please enter your Last Name',
                        maxlength: 255
                    },
                    r_email : {
                        required : 'Please enter your Email Address',
                        email: 'Please enter a valid Email Address'
                    },
                    r_dn : {
                        maxlength: 'Please enter less than 40 characters'
                    },
                    r_password : {
                        required : 'Please enter a password',
                        minlength: 'Password should be a minimum of 6 characters',
                        alphanumeric: 'Password should contain at least 1 letter and 1 number'
                    },
                    r_cpassword : {
                        equalTo: 'Password and Confirm password do not match'
                    },
                    r_zipcode : {
                        required : 'Please enter your zip code',
                        minlength: 'Your Zip Code must be 5 numbers!',
                        maxlength: 'Your Zip Code must be 5 numbers!',
                        digits: 'You can only enter numbers for Zip Code'
                    },
                    r_termsprivacy : {
                        required : 'Please agree to terms and conditions'
                    }
                },
                invalidHandler: function(f, v) {
                    var e = v.numberOfInvalids();
                    if (e) {
                        $("html, body").animate({ scrollTop: 0 }, "fast");
                    }
                },
                submitHandler: function(f) {
                    r.s();
                }
            });
            //r.s();
        }
    },
    n: function(s, ac, ec, u, p, e, cp, n, f, l, t, tp, ep, z, c) {
        var ps = {
            action: 'register',
            ac: ac,
            ec: ec,
            u : u,
            p : p,
            e : e,
            cp : cp,
            n : n,
            f : f,
            l : l,
            t : t,
            tp : tp,
            ep : ep,
            z : z
        };
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, ps, c); // Not Read-only mode
    },
    o: function(e) {
        var v = '';
        if($("#" + e).length != 0) {
            v = $("#" + e).val();
        }
        return v;
    },
    b: function(e) {
        var v = '';
        if ($("#" + e).length != 0 && $("#" + e).is(":checked")){
            v = 'true';
        }
        else {
            v = 'false';
        }
        return v;
    },
    c: function(t, d) {
        if(d.success) {
            window.location.href = d.redirectUrl;
        }
        else {
            //TODO: show failure message
            var errorStr = '';
            if(d.exceptionMsg == '[User already exists.]'){
                errorStr = 'There is already a user with this email.';
            }else{
                errorStr = d.exceptionMsg;
            }
            $("#hmr_reg_inputRegistration-error").html(errorStr);
            $("#hmr_reg_inputRegistration-error").show();
            $("#regErrorDiv").show();
            $("html, body").animate({ scrollTop: 0 }, "fast");
        }
    },
    s: function() {
        var s = 'HMR_CMS_RegistrationServiceInterface';
        var ec = r.o('hmr_reg_inputEmployerCode');
        var u = $("#hmr_reg_inputEmail").val();
        var p = $("#hmr_reg_inputPassword").val();
        var e = u;
        var cp = $("#hmr_reg_inputConfirmPassword").val();
        var n = $("#hmr_reg_inputDisplayName").val();
        var f = $("#hmr_reg_inputFirstName").val();
        var l = $("#hmr_reg_inputLastName").val();
        var ref = hmr_u.p("startUrl");
        if(typeof site_prefix == "undefined" || site_prefix == null) {
            site_prefix = "";
        }
        //Check for force.com in case came from shared link or some outside source,then take to welcome
        var t = ref && (ref.indexOf('500shake') > 0 || ref.indexOf('hmr') > 0 || ref.indexOf('force.com') > 0 || ref == 'checkout') ? ref : site_prefix + welcomeUrl;
        var tp = site_prefix + plansUrl;
        var ep = r.b('hmr_reg_emailpreference');
        var z = r.o('hmr_reg_inputZipCode');
        var ac = hmr_u.p('cartID');
        if(!ac) ac = '';
        r.n(s, ac, ec, u, p, e, cp, n, f, l, t, tp, ep, z, r.c);
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            r.r();
        }(window, STG.CTLib.Idea || {}));
    }
};
/* url parameters */
var hmr_u = {
    g: function(n, u) {
        if (!u) {
            u = window.location.href;
        }
        n = n.replace(/[\[\]]/g, "\\$&");
        var r = new RegExp("[?&]" + n + "(=([^&#]*)|&|#|$)"), s = r.exec(u);
        if (!s) return null;
        if (!s[2]) return '';
        return decodeURIComponent(s[2].replace(/\+/g, " "));
    },
    q: function(l) {
        var r = {};
        if(l == null) {
            l = location.search;
        }
        if(l){
            var s = l.slice(1).split('&');
            var i;
            for(i=0;i<s.length;i++){
                var p = s[i].split('=');
                var n = p[0];
                var v = decodeURIComponent(p[1] || '');
                if(r[n]){
                    if(_.isArray(r[n])){
                        r[n].push(v);
                    }
                    else{
                        r[n] = [r[n],v];
                    }
                }
                else{
                    r[n] = v;
                }
            }
        }
        return r;
    },
    c: function(k) {
        var n = k + "=";
        var d = decodeURIComponent(document.cookie);
        var ca = d.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(n) == 0) {
                return c.substring(n.length, c.length);
            }
        }
        return "";
    },
    a: function(a, u) {
        if(a != null) {
            var h = "";
            if(typeof hmr_u.q() != "undefined") {
                $.each(hmr_u.q(), function(i, v) {
                    if(h.indexOf(i) == -1) {
                        h += i + "=" + v + "&";
                    }
                });
                if(u.indexOf("?") != -1) {
                    var l = u.substring(u.indexOf("?"));
                    u = u.substring(0, u.indexOf("?"));
                    $.each(hmr_u.q(l), function(i, v) {
                        if(h.indexOf(i) == -1) {
                            h += i + "=" + v + "&";
                        }
                    });
                }
                if(typeof HMR.pageVars != "undefined") {
                    $.each(HMR.pageVars, function(i, v) {
                        h += i + "=" + v + "&";
                    });
                }
                var l = u;
                if(h.length > 0) {
                    l += "?" + h.substring(0, h.length - 1);
                }
                a.attr("href", l);
            }
        }
    },
    p: function(n){
        var match = RegExp('[?&]' + n + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
};

/* CC Context Service Interface */
var cc = {
    ps: {
        action: 'context',
        activeCartId: hmr_u.g('cartID'),
        portalUser: hmr_u.p('portalUser')
    },
    n: function(s, c) {
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, cc.ps, c); // Not Read-only mode
    },
    b: function(c) {
        //$(".shopping-cart-badge").find("span").html(c);
        //$(".shopping-cart-badge").removeClass("hidden");
        if(c > 0) {
            // $(".hmr-cart-empty").addClass("hidden");
            // $(".hmr-cart-filled").removeClass("hidden");
            //$(".hmr-cart-empty").removeClass("hidden");
            //$(".hmr-cart-counter").removeClass("hidden");
            $(".hmr-cart-counter").html(c);
        }
        else{
            // $(".hmr-cart-filled").addClass("hidden");
            // $(".hmr-cart-empty").removeClass("hidden");
            //$(".hmr-cart-counter").addClass("hidden");
            $(".hmr-cart-counter").html("0");
        }

    },
    p: function(d) {
        if(d.cartCount == 0) {
            HMR['pageVars'] = { 'cartId': hmr_u.c('apex__currCartId') };
        }
        if(d.cartCount > 0 && d.outputData != null) {
            HMR['pageVars'] = { 'cartId': d.outputData.cartList[0].encryptedId };
        }
    },
    l: function() {
        if(typeof CMS != "undefined") {
            hmr_u.a($("#cartli_wrapper a"), "shopping-cart");
            hmr_a.f();
        }
    },
    c: function(t, d) {
        if(d.success) {
            cc.b(d.cartCount);
            cc.p(d);
            cc.l();
        }
        else {
            console.log('HMR_CMS_CCContextServiceInterface call Failed');
            console.log(d);
        }
    },
    r: function() {
        var s = 'HMR_CMS_CCContextServiceInterface';
        if(window.doServiceRequest) {
            cc.n(s, cc.c);
        }
        else {
            if(typeof CCRZ == "undefined" && typeof CMS == "undefined" && typeof HMR_CMS_CCContextServiceInterface != "undefined") {
                HMR_CMS_CCContextServiceInterface.getCartCountRemoteActionCall(
                    cc.ps,
                    function(t, d) {
                        cc.c(t, JSON.parse(d.result));
                    },
                    {
                        escape: false
                    }
                );
            }
        }
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            cc.r();
        }(window, STG.CTLib.Idea || {}));
    }
};

/* Login */
var l = {
    n: function(s, u, p, r, ac, c) {
        var ps = {
            action: 'login',
            u : u,
            p : p,
            r : r,
            ac: ac
        };
        waitingDialog.msg("Logging in..");
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, ps, c); // Not Read-only mode
    },
    c: function(t, d) {
        if(d.success) {
            if(d.cartTransferred)
                l.cac(d.userId, hmr_u.p("cartID"), d.redirectUrl);
            else
                window.location.href = d.redirectUrl;
        }
        else {
            //TODO: show failure message
            var errorStr = '';
            errorStr = d.exceptionMsg;
            $("#hmr_reg_inputLogin-error").html(errorStr);
            $("#hmr_reg_inputLogin-error").show();
            $("#loginErrorDiv").show();
            $("html, body").animate({ scrollTop: 0 }, "fast");
            waitingDialog.hide();
            waitingDialog.rmsg();
        }
    },
    cac: function(userId, encryptedCartId, redirectUrl){ //Check automatic coupon
        var s = "HMR_CMS_CCRZ_Cart_ServiceInterface";
        var request ={
            action : "checkForAutomaticCoupon",
            userId : userId,
            isGuest : false,
            encryptedCartId : encryptedCartId
        };

        if(redirectUrl && redirectUrl.length > 0)
            request.redirectUrl = redirectUrl;

        $.orchestracmsRestProxy.doAjaxServiceRequest(s, request, l.cac_c);
    },
    cac_c: function(t, d){
        if(d.success){
             window.location.href = d.redirectUrl;
        }
        else{
            $("#hmr_reg_inputLogin-error").html('Auto coupon check failed');
            $("#hmr_reg_inputLogin-error").show();
            window.location.href = d.redirectUrl;
        }

        console.log(d);
    },
    r: function() {
        $("#hmr_signin_btn").on("click", function() {
            //$("#hmr_loginform").submit();
        });
        if(!isGuest && window.location.pathname.toLowerCase().indexOf("login") != -1) {
            window.location.href = site_prefix;
        }
        else {
            $("#hmr_loginform").validate({
                errorLabelContainer: $("#hmr_loginform div.form-error"),
                rules :{
                    l_email : {
                        required : true
                    },
                    l_password : {
                        required : true,
                        minlength: 6
                    }
                },
                messages: {
                    l_email : {
                        required : "Please enter your email!"
                    },
                    l_password : {
                        required : "Please enter your password!",
                        minlength : "Please enter a valid password!"
                    }
                },
                submitHandler: function(f) {
                    waitingDialog.show();
                    l.s();
                }
            });
        }
    },
    s: function() {
        var s = 'HMR_CMS_LoginServiceInterface';
        var u = $("#hmr_signin_inputEmail").val();
        var p = $("#hmr_signin_password").val();
        var ac = hmr_u.p("cartID"); //active cart
        var k = hmr_u.p("hasKitCart");
        //var r = site_prefix + myAccountUrl;
        var ref = k == null ? document.referrer : "checkout";
        //Check for force.com in case came from shared link or some outside source,then take to welcome
        var r = ref && (ref.indexOf('500shake') > 0 || ref.indexOf('hmr') > 0 || ref.indexOf('force.com') > 0 || ref == "checkout") ? ref : site_prefix + loginLander;

        if(!ac) ac = '';

        l.n(s, u, p, r, ac, l.c);
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            l.r();
        }(window, STG.CTLib.Idea || {}));
    }
};
$(document).ready(function(){
    cc.i();
    hmr_a.i();
    l.i();
    r.i();
    w.i();
    cldg.gs();
    plans_c.cs();
    subscribe.i();
    h_footer.i();
    c_a_guest.i();
    video.doInit();
});
//Release 2 JS for Community Components
var video = {
    doInit: function(){
        $(".videoModalFrame").each(function(){
            var iframe = $(this);
            var videoUrl = $(this).attr("src");
            var modalId = $(this)[0].dataset.modal;

            $("#" + modalId).on('hide.bs.modal', function(){
                $(iframe).attr('src', '');
            });

            $("#" + modalId).on('show.bs.modal', function(){
                $(iframe).attr('src', videoUrl + '?autoplay=1');
            });
        });
    }
};

var conversation = {
    doInit: function() {
        if($(".container.success").length > 0){
            window.STG = window.STG || {};
            window.STG.CTLib = window.STG.CTLib || {};
            window.STG.CTLib.Idea = (function(global, namespace, undefined) {
                'use strict';
                conversation.serviceInit();
            }(window, STG.CTLib.Idea || {}));
        }
    },
    serviceInit: function() {
        var service = 'HMR_CMS_Conversation_ServiceInterface';
        conversation.getConversationData(service, conversation.callback);
    },
    getConversationData: function(service, callback) {
        var request = {
            action: 'getConversationData'
        };
        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, callback);
    },
    getConversationRow: function(record){
        var conversationRow = '';
        //var rowIcon = $("#icon_conversationRow").val();
        if(!record.commentCount)
            record.commentCount = 0;

        if(record.title && record.relatedTopicList){
            conversationRow = '<div class="row conversation" onclick="conversation.navigateToRecord(\'' + record.id + '\', \'' + record.relatedTopicList[0] + '\')">' +
                                   '<div class="col-xs-12">' +
                                        '<img class="icon" src="https://www.myhmrprogram.com/ContentMedia/Resources/ChatLarge.png"  />' +
                                        '<div class="content">' +
                                            '<div class="question">' +
                                                record.title +
                                            '</div>' +
                                            '<div class="replies">' +
                                                record.relatedTopicList[0] + ' | ' + record.commentCount + ' REPLIES' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                               '</div>';
        }
        return conversationRow;
    },
    callback: function(event, result) {
        if(result.success) {
            if(result.popularData){
                var popularHtml = '';
                result.popularData.forEach(function(p){
                    popularHtml += conversation.getConversationRow(p);
                });
                $("#list_mostPopular").append(popularHtml);
            }
            if(result.hmrSuggestedData){
                var suggestedHtml = '';
                result.hmrSuggestedData.forEach(function(s){
                    suggestedHtml += conversation.getConversationRow(s);
                });
                $("#list_hmrSuggested").append(suggestedHtml);
            }
        }
        else
            console.log('ERROR' + result.message);
    },
    navigateToRecord: function(id, category){
        var urlRedirect = "";

        if(window.location.href.indexOf("/DefaultStore") >= 0)
            urlRedirect = "/DefaultStore";

        if(category)
            category = category.replaceAll(' ','-').replaceAll(';','').toLowerCase();

        window.location.href = urlRedirect + '/resources/forum/' + category + '/questions-detail?feedId=' + id;
    }
};

var findLocation = {
    doInit : function(){
        var accordian = document.getElementsByClassName('location-row');

        if(accordian){
            for (var i=0; i < accordian.length; i++) {
                accordian[i].onclick = function() {
                    this.children[0].children[0].classList.toggle('open');
                    this.nextElementSibling.classList.toggle('open');
                }
            }
        }
    }
};

var QuestionsList = {
    doInit: function(pageNumber) {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            QuestionsList.serviceInit(pageNumber);
        }(window, STG.CTLib.Idea || {}));
    },
    serviceInit: function(pageNumber) {
        var service = 'HMR_CMS_Questions_List_ServiceInterface';
        QuestionsList.getFeedPage(service, QuestionsList.callback, pageNumber);
    },
    getFeedPage: function(service, callback, pageNumber) {
        var request = {
            action: 'getFeedElements',
            topicName: $('#questionListTitle').val(),
            pageNumber: pageNumber
        };
        waitingDialog.show();
        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, callback);
    },
    callback: function(event, result) {
        if(result.success) {
            if(result.setfeed != ''){
                $(".questions-topic-detail.container").replaceWith(result.setfeed);
            }
        }
        else
            console.log('ERROR' + result.message);

        waitingDialog.hide();
    }
};

var cardList = {
    doInit : function(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset){
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            cardList.serviceInit(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset);
        }(window, STG.CTLib.Idea || {}));
    },
    serviceInit: function(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset) {
        var service = 'HMR_CMS_Knowledge_ServiceInterface';
        cardList.getNextCardSet(service, cardList.callback, listTitle, objectType, dataCategory, recipeCategory, subFilters, offset);
    },
    getNextCardSet: function(service, callback, listTitle, objectType, dataCategory, recipeCategory, subFilters, offset) {
        var request = {
            action: 'getNextCardSet',
            listTitle: listTitle,
            objectType: objectType,
            dataCategory: dataCategory,
            recipeCategory: recipeCategory,
            subFilters: subFilters,
            offset: offset,
            recordCount: $(".cardlist-recordCount").val(),
            recordsShown: $(".cardlist-recordsShown").val(),
            colorBlocksShown: $(".cardlist-colorBlocksShown").val()
        };
        waitingDialog.show();
        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, callback);
    },
    callback: function(event, result) {
        if(result.success) {
            if(result.replaceAll)
                $(".ka-card-bg").replaceWith(result.htmlResult);
            else
                $(".show-more").replaceWith(result.htmlResult);
        }
        else
            console.log('ERROR' + result.message);

        waitingDialog.hide();
    },
    showMore : function(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset){
        cardList.doInit(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset);
    },
    navigateToRecord : function(recordType, category, urlName){
        var storePrefix = '';

        if(document.location.href.indexOf("/DefaultStore") != -1)
            storePrefix = '/DefaultStore';

        category = category.replaceAll(' ','-').replaceAll(';','').toLowerCase();

        if(category == 'decision-free')
            category = 'decision-free-diet';
        else if(category == 'healthy-solutions')
            category = 'healthy-solutions-diet';
        else if(category == 'healthy-shakes')
            category = 'healthy-shakes-diet';
        else if(category == 'phase-2')
            category = 'phase-2';

        if(recordType.indexOf('Recipe') >= 0)
            window.location = storePrefix + '/recipes/' + category + '\/' + urlName;
        else if(recordType.indexOf('Success') >= 0)
            window.location = storePrefix + '/' + urlName;
        else
            window.location = storePrefix + '/resources/' + category + '\/' + urlName;
    }
};

var successStoryList = {
    doInit : function(page){
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            successStoryList.serviceInit(page);
        }(window, STG.CTLib.Idea || {}));
    },
    serviceInit: function(page) {
        var service = 'HMR_CMS_Knowledge_ServiceInterface';
        successStoryList.getNextPageRecord(service, successStoryList.callback, page);
    },
    getNextPageRecord: function(service, callback, page) {
        var request = {
            action: 'getNextPage',
            pageNumber: +page+1
        };
        waitingDialog.show();

        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, callback);
    },
    callback: function(event, result) {
        if(result.success) {
           $(".success-list").replaceWith(result.htmlResult);
        }
        else
            console.log('ERROR' + result.message);

        waitingDialog.hide();
    },
    showNext : function(e, page){
        e.preventDefault();
        successStoryList.doInit(page);
    },
    navigateToRecord : function(urlName){
        var urlRedirect = "";

        if(window.location.href.indexOf("/DefaultStore") >= 0)
                urlRedirect = "/DefaultStore";

        window.location = urlRedirect + '/resources/community-success-stories/' + urlName;
    }
};

var globalFilter = {
    doInit: function(){
       globalFilter.adjustByScreen();
    },
    adjustByScreen : function(){
        var $screenWidth = $(document).width();

       if($screenWidth < 768) {
            $(".global-filter-category").each(function(i, filter){
                if(!$(filter).hasClass('active'))
                    $(filter).addClass('hide');
            });
        }
        else {
            $(".global-filter-category").each(function(i, filter){
                $(filter).removeClass('hide');
            });
        }
    }
};

var subFilter = {
    doInit: function(){
        if($('.sub-filter').length > 0){
            $('.sub-filter').hide();

            $('.show-filter').on('click', function(e){
                e.preventDefault();
                $(this).text(function(i, text){
                    return text === "SHOW FILTER +" ? "HIDE FILTER -" : "SHOW FILTER +";
                });
                $('.sub-filter').toggle();
            });

            $('#applySubFilters').on('click', function(){
                selectedSubFilters = '';
                $('.sub-filter input[name="category"]:checked').each(function() {
                    selectedSubFilters += ',' + this.id;
                });

                if(selectedSubFilters.length > 0)
                    selectedSubFilters = selectedSubFilters.substr(1);

                cardList.doInit($("#cardList_listTitle").val(),
                                $("#cardList_objectType").val(),
                                $("#cardList_dataCategory").val(),
                                $("#cardList_recipeCategory").val(),
                                selectedSubFilters,
                                0);
            });

            $('#clearSubFilters').on('click', function(e){
                e.preventDefault();
                $('.sub-filter input[name="category"]').attr('checked', false);
                cardList.doInit($("#cardList_listTitle").val(),
                                $("#cardList_objectType").val(),
                                $("#cardList_dataCategory").val(),
                                $("#cardList_recipeCategory").val(),
                                '',
                                0);
            });
        }
    }
};

var knowledgeVote = {
    doInit: function() {
        if(!isGuest){
            window.STG = window.STG || {};
            window.STG.CTLib = window.STG.CTLib || {};
            window.STG.CTLib.Idea = (function(global, namespace, undefined) {
                'use strict';
                knowledgeVote.serviceInit();
            }(window, STG.CTLib.Idea || {}));
        }
    },
    serviceInit: function() {
        var service = 'HMR_CMS_Knowledge_ServiceInterface';
        knowledgeVote.likeArticle(service, knowledgeVote.callback);
    },
    likeArticle: function(service, callback) {
        var articleName = window.location.href.split('\/')[window.location.href.split('\/').length-1].split('?')[0];
        var request = {
            action: 'likeArticle',
            articleName: articleName
        };
        $("#likeArticleAction").fadeOut(400,function(){
            $(this).fadeIn(400)[0].src = "https://www.myhmrprogram.com/ContentMedia/Resources/BlueFillHeart.svg";
        });
        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, callback);
    },
    callback: function(event, result) {
        if(result.success) {

        }
        else{
            console.log('ERROR' + result.message);
        }
    }
};

/********************************************************************************/
/*************************Question Detail Page Script*****************************/
/********************************************************************************/
var questionsDetail = {
    callbackResult: null,
    doInit: function() {
        if($(".questions-detail").length > 0){ //Added Z. Engman - Need way to detect this is on page
            waitingDialog.show();
            window.STG = window.STG || {};
            window.STG.CTLib = window.STG.CTLib || {};
            window.STG.CTLib.Idea = (function(global, namespace, undefined) {
                'use strict';
                questionsDetail.serviceInit();
            }(window, STG.CTLib.Idea || {}));
        }
    },
    serviceInit: function() {
        var service = 'HMR_CMS_QuestionAnswer_ServiceInterface';
        var request = {
            action: actionType,
            p_feedId: feedId,
            p_limit: limitVal,
            p_offset: offsetVal,
            p_likeItemId: likeItemId,
            p_userId: curUserId,
            p_bestAnswerId: bestAnswerId,
            p_replyText: replyText,
            p_updatedTargetId: updatedTargetId,
            p_updatedText: updatedText,
            p_updatedSubject: updatedSubject,
            p_deleteTargetId: deleteTargetId,
            p_hmrSuggested_old: hmrSuggested_old,
            p_hmrSuggested_new: hmrSuggested_new
        };

        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, questionsDetail.callback);
    },
    callback: function(event, result) {
        if(result.success) {
            callbackResult = result;
            if(result.feedElement){
                //populate pagination
                var pn = Math.ceil(result.feedElement.commentsTotal/limitVal);
                if(pn == 0){
                    pn = 1;
                }
                pageNum = pn;
                hmrSuggested_old = result.feedElement.hmrSuggested;

                //if newlyAddedComment is set to true, that means a new comment just added
                //set the tag back to false then re-init
                if(newlyAddedComment){
                    newlyAddedComment = false;
                    questionsDetail.setOffset(pageNum);
                }

                if(actionType == 'getAnswers' || actionType == 'likeFeed' || actionType == 'unlikeFeed' || actionType == 'updateFeedElement' ){
                    //populate question
                    $(".questions-detail .question .date").html(result.feedElement.creatDate);
                    var nameInit = result.feedElement.userSummary.firstName+' '+result.feedElement.userSummary.lastName.substring(0,1).toUpperCase()+'.';
                    $(".questions-detail .question .name").html(nameInit);
                    var uTitleQuestion = '';
                    if(result.feedElement.userSummary.userType == 'Internal'){
                        if(result.feedElement.userSummary.title !='' &&result.feedElement.userSummary.title !=null){
                           uTitleQuestion  = result.feedElement.userSummary.title;
                        }else{
                            uTitleQuestion = 'HMR';
                        }
                    }

                    $(".questions-detail .question .location").html(uTitleQuestion);

                var questionBody = '<b>'+result.feedElement.feedElement.capabilities.questionAndAnswers.questionTitle+'</b><br/>';
                    // if(result.feedElement.feedElement.body.isRichText){
                    //     richTextBody = questionsDetail.parseRichText(result.feedElement.feedElement.body);
                    //     questionBody += richTextBody;
                    // }else{
                    //     richTextBody=result.feedElement.feedElement.body.text;
                    //     questionBody +=richTextBody;
                    // }
                    richTextBody=result.feedElement.questionText;
                    questionBody +=richTextBody;
                    $(".questions-detail .question .questionTitle").html(questionBody);
                    $(".questions-detail .question .questionImg").html('<img src="'+result.feedElement.userSummary.photo.smallPhotoUrl+'" />');
                    //$(".questions-detail .question .likesTotal").html(result.feedElement.feedElement.capabilities.chatterLikes.page.total);
                                                //if liked, diffrent heart icon
                    //icon will be upadted
                    var feedLikeHTML = '';
                    if(result.feedElement.feedElement.capabilities.chatterLikes.isLikedByCurrentUser){
                        feedLikeHTML +='<img style="width:25px; cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueFillHeart.svg"';
                    }else{
                        feedLikeHTML +='<img style="width:25px; cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueHeart.svg"';
                    }
                    if(!cmsIsGuest){ //if(!CMS.isGuest){ //when not guest
                        if(result.feedElement.feedElement.capabilities.chatterLikes.isLikedByCurrentUser){
                            feedLikeHTML +='onclick="questionsDetail.unlikeFeed(\''+result.feedElement.feedElement.id+'\');"';
                        }else{
                            feedLikeHTML +='onclick="questionsDetail.likeFeed(\''+result.feedElement.feedElement.id+'\');"';
                        }
                    }
                    feedLikeHTML +=' />&nbsp; '+result.feedElement.feedElement.capabilities.chatterLikes.page.total+' LIKE';
                    // if(result.feedElement.feedElement.capabilities.chatterLikes.isLikedByCurrentUser){
                        if(result.feedElement.feedElement.capabilities.chatterLikes.page.total>1){
                            feedLikeHTML+='S';
                        }
                    feedLikeHTML +='<span class="reply"><a ';
                    if( cmsIsGuest){
                      var loc = postQuestion.getRelativeURL("/sign-in");
                      feedLikeHTML +='href="'+loc+'" ';
                    }else{
                      feedLikeHTML +='href="#replyZone" ';
                    }
                    feedLikeHTML +='>REPLY</a></span>';
                    // }
                    $(".questions-detail .question .like").html(feedLikeHTML);

                    if(adminMode || (curUserId == result.feedElement.userSummary.id)){
                      var actionList ='<div class="dropdown" onclick="$(\'#dropdownMenu1\').dropdown(\'toggle\')">';
                          actionList +='<div class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
                              actionList +='<img style="cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/ThreeDots.png" />';
                          actionList +='</div>';
                          actionList +='<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">';
                              // actionList +='<li><a href="#" data-toggle="modal" data-target="#editModal">Action</a></li>';
                                  actionList +='<li><a href="#"  onclick="questionsDetail.udpateElementModal(\''+result.feedElement.feedElement.id+'\')">Edit</a></li>';
                                  actionList +='<li><a href="#" onclick="questionsDetail.deleteElement(\''+result.feedElement.feedElement.id+'\')">Delete</a></li>';
                                  //actionList +='<li role="separator" class="divider"></li>';
                              // actionList +='<li><a href="#textAreaComment">Reply</a></li>';
                          actionList +='</ul>';
                      actionList +='</div>';
                      $(".questions-detail .question .button").html(actionList);
                    }

                }

                //if(actionType == 'getAnswers' || actionType == 'likeComment' || actionType == 'unlikeComment' || actionType == 'setTheBestAnswer' || actionType=='updateComment' ||  actionType=='deleteComment'){
                    //populate comments
                    var replyHtml = '';
                    if(result.feedElement.bestAnswer != null){
                        replyHtml +='<div class="answer">';
                            replyHtml +='<div class="container best comment ">';//
                                replyHtml +='<div class="col-xs-12 expand">';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-xs-8 col-sm-offset-2 col-md-offset-1 col-sm-4 col-md-5 date text-left">'+result.feedElement.bestAnswerDate+'</div>';
                                        replyHtml +='<div class="col-xs-4 col-sm-6 button text-right">';

                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-xs-2 col-sm-2 col-md-1 text-right avatar-info">';
                                            replyHtml +='<img src="'+result.feedElement.bestAnswer.user.photo.smallPhotoUrl+'" />';
                                        replyHtml +='</div>';
                                        replyHtml +='<div class="col-xs-10 col-sm-4 col-md-5 user-detail text-left">';
                                            replyHtml +='<div class="content">';
                                                replyHtml +='<div class="name">';
                                                    replyHtml +=''+result.feedElement.bestAnswer.user.firstName+' '+result.feedElement.bestAnswer.user.lastName.substring(0,1).toUpperCase()+'.';
                                                replyHtml +='</div>';
                                                replyHtml +='<div class="location">';
                                                    var uTitleBest = '';
                                                    if(result.feedElement.bestAnswer.user.userType == 'Internal'){
                                                        if(result.feedElement.bestAnswer.user.title !='' && result.feedElement.bestAnswer.user.title !=null){
                                                           uTitleBest  = result.feedElement.bestAnswer.user.title;
                                                        }else{
                                                            uTitleBest = 'HMR';
                                                        }
                                                    }
                                                    replyHtml +=uTitleBest;
                                                replyHtml +='</div>';
                                            replyHtml +='</div>';
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-sm-10 col-sm-offset-2 col-md-offset-1 reply">';
                                            replyHtml +=''+result.feedElement.bestAnswerText;
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-sm-10 col-sm-offset-2 col-md-offset-1">';
                                            replyHtml +='<div class="like">';
                                                //if liked, diffrent heart icon
                                                //icon will be upadted
                                                if(result.feedElement.bestAnswer.myLike != null){
                                                    replyHtml +='<img style="width:25px;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueFillHeart.svg"';
                                                }else{
                                                    replyHtml +='<img style="width:25px;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueHeart.svg"';
                                                }
                                                replyHtml +=' />&nbsp; '+result.feedElement.bestAnswer.likes.total+' LIKE';
                                                if(result.feedElement.bestAnswer.likes.total>1){
                                                    replyHtml +='S';
                                                }
                                                // if(result.feedElement.bestAnswer.myLike != null){
                                                //     replyHtml +='<span class="reply">YOU LIKE THIS!</span>';
                                                // }
                                            replyHtml +='</div>';
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="clearfix"></div>';
                                        replyHtml +='<div class=" best-answer text-right">';
                                            replyHtml +='<span>Marked as Best Answer</span> <img src="https://www.myhmrprogram.com/ContentMedia/Resources/StarWhite.svg" />';
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                replyHtml +='</div>';
                        replyHtml +='</div>';
                    }

                    result.feedElement.offsetList.forEach(function(c){
                        replyHtml +='<div class="answer">';
                            replyHtml +='<div class="container comment ">';//best
                                replyHtml +='<div class="col-xs-12 expand">';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-xs-8 col-sm-offset-2 col-md-offset-1 col-sm-4 col-md-5 date text-left">'+c.creatDate+'</div>';
                                        replyHtml +='<div class="col-xs-4 col-sm-6 button text-right">';
                                        if (adminMode|| (curUserId == c.commentRecord.user.id)) {
                                           replyHtml +='<div class="dropdown" onclick="$(\'#dropdownMenu'+c.commentRecord.id+'\').dropdown(\'toggle\')">';
                                                replyHtml +='<div class="dropdown-toggle" id="dropdownMenu'+c.commentRecord.id+'" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true" >';
                                                    replyHtml +='<img  style="cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/ThreeDots.png" />';
                                                replyHtml +='</div>';
                                              replyHtml +='<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu'+c.commentRecord.id+'">';
                                                // replyHtml +='<li><a href="#" data-toggle="modal" data-target="#editModal">Action</a></li>';
                                                replyHtml +='<li><a href="#" onclick="questionsDetail.udpateCommentModal(\''+c.commentRecord.id+'\')">Edit</a></li>';
                                                replyHtml +='<li><a href="#" onclick="questionsDetail.deleteComment(\''+c.commentRecord.id+'\');">Delete</a></li>';
                                                // replyHtml +='<li role="separator" class="divider"></li>';
                                                // replyHtml +='<li><a href="#textAreaComment">Reply</a></li>';
                                              replyHtml +='</ul>';
                                            replyHtml +='</div>';
                                        }
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-xs-2 col-sm-2 col-md-1 text-right avatar-info">';
                                            replyHtml +='<img src="'+c.commentRecord.user.photo.smallPhotoUrl+'" />';
                                        replyHtml +='</div>';
                                        replyHtml +='<div class="col-xs-10 col-sm-4 col-md-5 user-detail text-left">';
                                            replyHtml +='<div class="content">';
                                                replyHtml +='<div class="name">';
                                                    replyHtml +=''+c.commentRecord.user.firstName+' '+c.commentRecord.user.lastName.substring(0,1).toUpperCase()+'.';
                                                replyHtml +='</div>';
                                                replyHtml +='<div class="location">';
                                                    var uTitleAnswer = '';
                                                    if(c.commentRecord.user.userType == 'Internal'){
                                                        if(c.commentRecord.user.title !='' && c.commentRecord.user.title !=null){
                                                           uTitleAnswer  = c.commentRecord.user.title;
                                                        }else{
                                                            uTitleAnswer = 'HMR';
                                                        }
                                                    }
                                                    replyHtml +=uTitleAnswer;
                                                replyHtml +='</div>';
                                            replyHtml +='</div>';
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-sm-10 col-sm-offset-2 col-md-offset-1 reply uText'+c.commentRecord.id+'">';
                                            replyHtml +=''+c.answerText;
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="row">';
                                        replyHtml +='<div class="col-sm-10 col-sm-offset-2 col-md-offset-1">';
                                            replyHtml +='<div class="like">';
                                                //if liked, diffrent heart icon
                                                //icon will be upadted
                                                if(c.commentRecord.myLike != null){
                                                    replyHtml +='<img style="width:25px; cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueFillHeart.svg"';
                                                }else{
                                                    replyHtml +='<img style="width:25px; cursor:pointer;" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueHeart.svg"';
                                                }

                                                //if guest, no action allowed.
                                                //if no guest, can like
                                                if(!cmsIsGuest){ //if(!CMS.isGuest){ //when not guest
                                                    if(c.commentRecord.myLike != null){
                                                        replyHtml +='onclick="questionsDetail.unlikeComment(\''+c.commentRecord.id+'\');"';
                                                    }else{
                                                        replyHtml +='onclick="questionsDetail.likeComment(\''+c.commentRecord.id+'\');"';
                                                    }
                                                }

                                                replyHtml +=' />&nbsp; '+c.commentRecord.likes.total+' LIKE';
                                                if(c.commentRecord.likes.total > 1){
                                                    replyHtml +='S';
                                                }

                                                // if(c.commentRecord.myLike != null){
                                                //     replyHtml +='<span class="reply">YOU LIKE THIS!</span>';
                                                // }
                                                //only display select as best when current user owns this question
                                                // console.log('----)'+result.feedElement.userSummary.Id);
                                                // console.log('----)'+curUserId);
                                                if(adminMode || (!isGuest && (curUserId == result.feedElement.userSummary.id))) {
                                                    //if no best answer selected, display link to mark answer as best
                                                    if(result.feedElement.bestAnswer == null){
                                                        replyHtml +='<span class="reply" style="cursor: pointer;" onclick="questionsDetail.setTheBestAnswer(\''+c.commentRecord.id+'\');">SELECT AS BEST</span>';
                                                    }else{
                                                        //if already selected best answer, expose link to un-best the best answer
                                                        if(result.feedElement.bestAnswer.id == c.commentRecord.id ){
                                                            replyHtml +='<span class="reply" style="cursor: pointer;" onclick="questionsDetail.setTheBestAnswer(\'\');">REMOVE AS BEST</span>';
                                                        }
                                                    }
                                                }
                                            replyHtml +='</div>';
                                        replyHtml +='</div>';
                                    replyHtml +='</div>';
                                    replyHtml +='<div class="clearfix"></div>';
                                    //if(curUserId == result.feedElement.userSummary.Id){
                                        // replyHtml +='<div class=" best-answer text-right">';
                                        //     replyHtml +='<span>Marked as Best Answer</span> <img style="width:25px; padding-left:0px" src="https://www.myhmrprogram.com/ContentMedia/Resources/Star.svg" />';
                                        // replyHtml +='</div>';
                                    //}
                                    replyHtml +='</div>';
                                replyHtml +='</div>';
                        replyHtml +='</div>';


                    });
                    if(limitVal < result.feedElement.commentsTotal){
                        replyHtml +='<div class="answer">';
                            replyHtml +='<div class="container comment ">';//best
                                replyHtml +='<div style="text-align: center">';
                                    replyHtml +='<a class="hmr-link seeMore" onclick="questionsDetail.seeMore()">SEE MORE</a>';
                                replyHtml +='</div>' ;
                            replyHtml +='</div>';
                        replyHtml +='</div>';
                    }
                    $(".questions-detail .commentsZone").html(replyHtml);

                    if(!cmsIsGuest){
                    ckHtml ='<div class="answer">';
                        ckHtml +='<div class="container comment" id="replyZone">';
                            ckHtml +='<div class="col-xs-12 expand">';
                                ckHtml +='<div class="row reply-section no-wrap">';
                                    ckHtml +='<div class="col-xs-2 col-sm-2 col-md-1 text-right avatar-info">';
                                        ckHtml +='<img class="pull-right" src="'+result.currentUser.SmallPhotoUrl+'" />';
                                    ckHtml +='</div>';
                                    ckHtml +='<div class="col-xs-offset-0 col-xs-10 col-sm-offset-0 col-sm-10 col-md-11">';
                                        ckHtml +='<textarea style="width: 100%;" rows="5" class="newComment" name="replyBox" id="textAreaComment">';
                                        ckHtml +='</textarea>';
                                    ckHtml +='</div>';
                                    ckHtml +='<div class="col-xs-12 reply-btns">';
                                        ckHtml +='<div class="col-xs-4 col-sm-offset-2 col-sm-2 col-md-offset-1 col-md-3 text-left">';
                                            ckHtml +='<div class="hmr-allcaps-container">';
                                                //returnMe +='<a data-gtm-id="homeBlock3" href="about-hmr-foods-nutrition-ingredients?categoryId=a0H4100000AuRt2EAF" class="hmr-allcaps hmr-allcaps-blue">Cancel</a>';
                                            ckHtml +='</div>';
                                        ckHtml +='</div>';
                                        ckHtml +='<div class="col-xs-12 btn-sequence text-right">';
                                        ckHtml +='<span class="hmr-allcaps-container">'
                                        if(!cmsIsGuest){
                                            ckHtml +='<a class="hmr-allcaps hmr-allcaps-blue post-btn" onclick="questionsDetail.postComment();">SUBMIT</a>';
                                        }
                                        ckHtml +='</span>';
                                        ckHtml +='</div>';
                                    ckHtml +='</div>';
                                ckHtml +='</div>';
                            ckHtml +='</div>';
                        ckHtml +='</div>';
                    ckHtml +='</div>';
                    $(".questions-detail .ckeditorZone").html(ckHtml);
                    }

                    if( !cmsIsGuest){
                        questionsDetail.populateCKEDITOR('replyBox');
                    }
                //}



                //only rerender when getAnswers
                //if(actionType == 'getAnswers'  || actionType == 'updateFeedElement' || actionType=='updateComment'){
                //     var paginationHTML = '<li><a aria-label="Previous"  style="border:none;';
                //     //if offset is 0, aka on first page
                //     if(!(offsetVal < limitVal)){
                //         paginationHTML += '" onclick="questionsDetail.prevPage();"  href="#"';
                //     }else{
                //         paginationHTML += ' cursor: not-allowed;" disabled="disabled"';
                //     }
                //     paginationHTML += '><span aria-hidden="true">< Previous</span></a></li>';
                //     for(var i = 1; i<=pageNum; i++){
                //         paginationHTML += '<li><a href="#"  style="border:none;';
                //         if(currPage == i){
                //             //class will be updated later
                //             paginationHTML += ' background-color: #46bbeb; color: #fff;';
                //         }
                //         paginationHTML += '" onclick="questionsDetail.setOffset('+i+');">'+i+'</a></li>';
                //     }
                //     paginationHTML += '<li><a aria-label="Next" style="border:none;';
                //     //if current offset record + limit is more than total comments, then no next page
                //     //console.log('========='+((offsetVal+limitVal)>result.feedElement.commentsTotal));
                //     if(!(offsetVal+limitVal>result.feedElement.commentsTotal)){
                //         paginationHTML += '" onclick="questionsDetail.nextPage();" href="#"';
                //     }else{
                //         paginationHTML += ' cursor: not-allowed;" disabled="disabled"';
                //     }
                //     paginationHTML += '><span aria-hidden="true">Next ></span></a></li>';
                // //}

                //     $(".pagination").html(paginationHTML);
                    var urlRedirect = "";

                    if(window.location.href.indexOf("/DefaultStore") >= 0)
                            urlRedirect = "/DefaultStore";

                    if(!result.feedElement.questionCategory)
                        $(".backToConvLink").prop("href", urlRedirect + '/resources/forum');
                    else
                        $(".backToConvLink").prop("href", urlRedirect + '/resources/forum/'+result.feedElement.questionCategory.replaceAll('Questions-','').toLowerCase());

                    //inject the modal
                    var modalHTML = '<div id="questionModal" class="hmr-modal modal question fade" role="dialog">';
                        modalHTML += '<div class="modal-dialog">';
                            modalHTML += '<div class="modal-content">';
                                modalHTML += '<div class="modal-header">';
                                    modalHTML += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                        modalHTML += '<span aria-hidden="true">&times;</span>';
                                    modalHTML += '</button>';
                                    modalHTML += '<h4 class="modal-title">Edit the post</h4>';
                                modalHTML += '</div>';
                                modalHTML += '<div class="modal-body">';
                                    modalHTML += '<form>';
                                            modalHTML += '<div class="form-group titleField">';
                                                modalHTML += '<label for="subject" class="small-title">Subject</label>';
                                                modalHTML += '<input type="text" class="form-control updatedSubjectText" id="subject" placeholder=""/>';
                                            modalHTML += '</div>';
                                            modalHTML += '<div class="form-group bodyField">';
                                                modalHTML += '<label for="message" class="small-title">Message</label>';
                                                modalHTML += '<textarea name="richtextCKEDITOR" id="message" class="form-control updateTextMsgFe"></textarea>';
                                            modalHTML += '</div>';
                                            // modalHTML += '<div class="form-group commentFields">';
                                            //     modalHTML += '<label for="message" class="small-title">Message</label>';
                                            //     modalHTML += '<textarea id="message" class="form-control updateTextMsgCmt"></textarea>';
                                            // modalHTML += '</div>';
                                        //only will be displayed with admin mode
                                          modalHTML += '<div class="hmrSuggested"><input type="checkbox" id="hmrSuggestedBox"';
                                          if(result.feedElement.hmrSuggested){
                                            modalHTML += 'checked="checked"';
                                          }
                                          modalHTML += '></input> #HMR SUGGESTED</div>';
                                        //}
                                        modalHTML += '<div class="col-xs-12 text-right hmr-allcaps">';

                                            modalHTML += '<span data-dismiss="modal">CANCEL</span><span class="border-left" onclick="questionsDetail.udpateCommentOrElement();"> UPDATE </span>';
                                        modalHTML += '</span>';
                                    modalHTML += '</form>';
                                modalHTML += '</div>';
                                modalHTML += '<div class="modal-footer text-right">';
                                modalHTML += '</div>';
                            modalHTML += '</div>';
                        modalHTML += '</div>';
                    modalHTML += '</div>';
                    $(".modalZone").html(modalHTML);

                    questionsDetail.populateCKEDITOR('richtextCKEDITOR');
            }

        }
        else
            console.log('ERROR' + result.message);

        waitingDialog.hide();
    },
    seeMore: function(){
        actionType = 'getAnswers';
        limitVal = limitVal+10;
        questionsDetail.doInit();
    },
    setOffset: function(offs){
        actionType = 'getAnswers';
        currPage = offs;
        offsetVal = (offs-1)*limitVal;
        questionsDetail.doInit();
    },
    nextPage: function(){
        actionType = 'getAnswers';
        currPage = currPage+1;
        offsetVal = offsetVal+limitVal;
        questionsDetail.doInit();
    },
    prevPage: function(){
        actionType = 'getAnswers';
        currPage = currPage-1;
        offsetVal = offsetVal-limitVal;
        questionsDetail.doInit();
    },
    likeFeed: function(itemId){
        actionType = 'likeFeed';
        likeItemId = itemId;
        questionsDetail.doInit();
    },
    unlikeFeed: function(itemId){
        actionType = 'unlikeFeed';
        likeItemId = itemId;
        questionsDetail.doInit();
    },
    likeComment: function(itemId){
        actionType = 'likeComment';
        likeItemId = itemId;
        questionsDetail.doInit();
    },
    unlikeComment: function(itemId){
        actionType = 'unlikeComment';
        likeItemId = itemId;
        questionsDetail.doInit();
    },
    postComment: function(){
        newlyAddedComment = true;
        actionType = 'postComment';
        //var commentContent = $(".newComment").val();
        var commentContent = CKEDITOR.instances["textAreaComment"].getData();
        replyText = commentContent;
        questionsDetail.doInit();
    },
    setTheBestAnswer: function(itemId){
        bestAnswerId = itemId;
        actionType = 'setTheBestAnswer';
        questionsDetail.doInit();
    },
    udpateElementModal: function(targetId){
        updatedTargetId = callbackResult.feedElement.feedElement.id;
        //populate the subject field value
        updatedSubject = callbackResult.feedElement.feedElement.capabilities.questionAndAnswers.questionTitle;
        $(".updatedSubjectText").val(updatedSubject);
        //populate the body text value
        updatedText = richTextBody; //callbackResult.feedElement.feedElement.body.text;
        $(".updateTextMsgFe").val(updatedText);

        $(".titleField").show();
        if( adminMode){
          $(".hmrSuggested").show();
        }else{
          $(".hmrSuggested").hide();
        }
        //$(".commentFields").hide();

        actionType = 'updateFeedElement';
        CKEDITOR.instances['message'].setData(updatedText);
        $('#questionModal').modal();

    },
    udpateCommentModal: function(targetId){
        updatedTargetId = targetId;
        //populate the body text value for comment
        updatedText = $(".uText"+targetId).html();
        $(".updateTextMsgFe").val(updatedText);

        $(".titleField").hide();
        $(".hmrSuggested").hide();
        // $(".commentFields").show();

        actionType = 'updateComment';
        CKEDITOR.instances['message'].setData(updatedText);
        $('#questionModal').modal();

        //questionsDetail.populateCKEDITOR('richtextCKEDITOR');

    },
    populateCKEDITOR: function(textAreaName){
        CKEDITOR.replace(textAreaName, {
          // Define the toolbar groups as it is a more accessible solution.
          toolbar : [
          //{ name: 'undo', groups: [ 'undo' ], items: [ 'Undo', 'Redo'] },
          { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike'] },
          { name: 'links', items: [ 'Link'] },
          { name: 'paragraph', groups: [ 'list', 'indent'], items: [ 'NumberedList', 'BulletedList', '-'] }
          ]
          }
        );
    },
    udpateCommentOrElement: function(){
        if(actionType == 'updateFeedElement'){
            updatedSubject = $(".updatedSubjectText").val();
            //updatedText = $(".updateTextMsgFe").val();
            updatedText = CKEDITOR.instances["message"].getData();
            hmrSuggested_new = $('#hmrSuggestedBox').is(":checked");
        }else if(actionType == 'updateComment'){
            //updatedText = $(".updateTextMsgCmt").val();
            updatedText = CKEDITOR.instances["message"].getData();
        }
        questionsDetail.doInit();
        $('#questionModal').modal('hide');
    },
    deleteElement: function(targetId){
        if(confirm('Are you sure you want to delete the question?')){
            actionType = 'deleteElement';
            deleteTargetId = targetId;
            questionsDetail.doInit();
            window.location.href = '/';
        }
    },
    deleteComment: function(targetId){
        if(confirm('Are you sure you want to delete the comment?')){
            newlyAddedComment = true;
            actionType = 'deleteComment';
            deleteTargetId = targetId;
            questionsDetail.doInit();
        }
    },
    parseRichText: function(feedBody){
        if(feedBody != null){
            var returnMe = '';
            feedBody.messageSegments.forEach(function(s){
                if(s.type='MarkupBegin'){
                    if(s.htmlTag!= undefined){
                        returnMe += '<'+s.htmlTag+'>';
                    }else{
                        returnMe += s.text;
                    }
                }else if(s.type='MarkupEnd'){
                    returnMe += '</'+s.htmlTag+'>';
                }

            });
            return returnMe;
        }
    }
};


//added by Joey, Post a question Modal

var postQuestion = {
    doInit: function() {
        if(cmsIsGuest){
            var loc = postQuestion.getRelativeURL("/sign-in");
            $("#startTopicBtn").prop("href",loc);
        }
    },
    serviceInit: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
        var service = 'HMR_CMS_QuestionAnswer_ServiceInterface';
        var request = {
            action: actionType,
            p_userId: curUserId,
            p_topicValue: location.pathname.substring(location.pathname.lastIndexOf("\/") + 1),
            p_updatedText: updatedText,
            p_updatedSubject: updatedSubject
        };

        $.orchestracmsRestProxy.doAjaxServiceRequest(service, request, postQuestion.callback);
        }(window, STG.CTLib.Idea || {}));


    },
    callback: function(event, result) {
        if(result.success) {
          //mayt be updated later
          window.location.reload();
        }
        else
            console.log('ERROR' + result.message);
    },
    postNewQuestion: function(){
      //get the value value value
      actionType = 'post';
      updatedSubject = $("#postQuestionModal #subject").val();
      //updatedText = $(".updateTextMsgFe").val();
      updatedText = CKEDITOR.instances["message"].getData();

      postQuestion.serviceInit();
    },postQuestionModal: function(){
        if(cmsIsGuest){
            var loc = postQuestion.getRelativeURL("/sign-in");
            top.locaiton = loc;
        }else{
            if($("#postQuestionModal").length > 0){ //Added Z. Engman - Need way to detect this is on page
                $('#postQuestionModal').modal();
                CKEDITOR.replace('post-rich-text', {
                  // Define the toolbar groups as it is a more accessible solution.
                  toolbar : [
                  //{ name: 'undo', groups: [ 'undo' ], items: [ 'Undo', 'Redo'] },
                  { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike'] },
                  { name: 'links', items: [ 'Link'] },
                  { name: 'paragraph', groups: [ 'list', 'indent'], items: [ 'NumberedList', 'BulletedList', '-'] }
                  ]
                  }
                );
            }
        }
    },getRelativeURL: function(pURL){
        var d = "";
        if(location.href.indexOf("/DefaultStore") != -1) {
            d = "/DefaultStore";
        }
        var h = d + pURL;
        return h;
    }
};

var rd = {
    n: function(s, z, c) {
        var ps = {
            action: 'getRecipeDetail',
            urlName : z
        };
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, ps, c); // Not Read-only mode
    },
    c: function(t, d) {
        if(d.success) {
            $("#r_time").after(d.r_time);
            $("#r_rating").after(d.r_rating);
            $("#r_vServe").html(d.r_vServe);
            $("#r_fServe").html(d.r_fServe);
            $("#r_calories").html(d.r_calories);
            $("#r_title").html(d.r_title);
            $("#r_ingredients").html(d.r_ingredients);
            $("#r_date").html(d.r_date);
            $("#r_instructions").html(d.r_instructions);
            if(d.r_summary)
                $("#r_summary").html(d.r_summary);
            $("#r_hero").css('background-image', 'url(' + d.r_hero + ')');
        }
        else {
            $(".Recipe_Detail").hide();
            console.log('ERROR' + d.message);
        }
    },
    s: function() {
        var s = 'HMR_CMS_RecipeDetailServiceInterface';
        var urlParts = document.location.href.split('/');
        var z = urlParts[urlParts.length - 1].split('?')[0];
       rd.n(s, z, rd.c);
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            rd.s();
        }(window, STG.CTLib.Idea || {}));
    }
};
