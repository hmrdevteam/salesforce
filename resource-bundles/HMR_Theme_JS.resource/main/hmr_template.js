/* DO NOT MODIFY */
if(typeof(Handlebars) !== "undefined"){
    Handlebars.registerHelper('each_upto', function(ary, max, options) {
      if(!ary || ary.length == 0)
          return options.inverse(this);

      var result = [],
        skip = (options.hash ? (options.hash.skip ? options.hash.skip : 0) : 0),
        i = skip;

      max += skip;

      for(; i < max && i < ary.length; ++i) {
        result.push(options.fn(ary[i], { data : { itemIndex : i } } ));
      }

      return result.join('');
    });
    Handlebars.registerHelper('formatDateLong', function(value) {
        var dateComponents = value.split("-");
        var year = dateComponents[0];
        var month = parseInt(dateComponents[1], 10);
        var day = parseInt(dateComponents[2], 10);

        var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var monthStr = months[month - 1];

        return monthStr + " " + day + ", " + year;
    });
    Handlebars.registerHelper('formatMonthDigits', function(value){
            return ("00" + value).slice(-2);
    });
    Handlebars.registerHelper('ifEquals', function(v1, v2, options) {
        if (v1 === v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    Handlebars.registerHelper('ifNotEquals', function(v1, v2, options) {
        if (v1 !== v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    Handlebars.registerHelper('ifGreaterThan', function(v1, v2, options) {
        if (v1 > v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    Handlebars.registerHelper('ifLessThanEqualTo', function(v1, v2, options) {
        if (v1 <= v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    Handlebars.registerHelper('getCardImageUrl', function(paymentType) {
        switch(paymentType){
            case "001":
                return '../resource/HMRIcons/checkout/Visa.png';
            break;
            case "002":
                return '../resource/HMRIcons/checkout/MasterCard.png';
            break;
            case "003":
                return '../resource/HMRIcons/checkout/American-Express.png';
            break;
            default:
                return '../resource/HMRIcons/checkout/Discover.png';
            break;
        }
    });
    Handlebars.registerHelper('price', function(amount, options) {
        return new Handlebars.SafeString(HMR.util.formatPrice(amount));
    });
    Handlebars.registerHelper('priceAbs', function(amount, options) {
        var absVal = Math.abs(amount);
        return new Handlebars.SafeString(HMR.util.formatPrice(absVal));
    });
    Handlebars.registerHelper('getServingDisplay', function(item) {
        var value = "box";
        if(item.attributes.displayName.indexOf('120') >= 0 && item.attributes.displayName.indexOf('Shake') >= 0)
            value = "can";
        return value;
    });
    Handlebars.registerHelper('showCaloriesServingsProtein', function(item) {
        var value = "";
        if(item.attributes.displayName.indexOf('pack') >= 0 || item.attributes.displayName.indexOf('Pack') >= 0)
            value = "hidden";
        return value;
    });
    Handlebars.registerHelper('myDebug', function(value) {
        console.log(value);
    });
    Handlebars.registerHelper('getServingSize', function(specs) {
        return specs["Serving Size"];
    });
}
if (!this.HMR) {
    this.HMR = {};
}
(function(HMR, $) {
    HMR.models = {};
    HMR.collections = {};
    HMR.views = {};
    HMR.root = this;
    HMR.getQueryParam = function(name, emptyString) {
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
        }
        if(emptyString) {
            return "";
        }
        return false;
    };
    HMR.logOn = (JSON.parse(HMR.getQueryParam("enableLog", false)) == true) ? true : false;
    HMR.console = function() {
        var retVer = {
            log: function() {}
        };
        try {
            if(HMR.logOn) {
                if ('console' in window) {
                    retVer = _.extend(retVer, {
                        log: function(msg, lvl, tm, sub, l) {
                            var smsg = _.isUndefined(msg) ? 'undefined' : (_.isObject(JSON) ? JSON.stringify(msg) : msg);
                            window.console.log('[' + (l || 'c') + '][' + (tm || (new Date()).getTime()) + '][' + (lvl || 'DEBUG') + '][' + (sub || 'none') + '] ' + smsg);
                        }
                    });
                }
            }
        } catch (e) {}
        return retVer;
    }();
    HMR.util = {
        template: function(id) {
            var source = $('#' + id).html();
            source = _.isUndefined(source) ? '' : source;
            return Handlebars.compile(source);
        }
    };
    HMR.util.formatPrefix = /[^\d\-\+#]*/i;
    HMR.util.formatPostfix = /[^\d\-\+\#\.]*$/i;
    HMR.util.formatMatch = /[^\d\-\+#]/g;
    HMR.util.format = function(m, v) {
        if (!m || isNaN(+v)) {
            return v;
        }
        var m = m.toString();
        var prefix = m.match(HMR.util.formatPrefix).toString();
        var postfix = m.match(HMR.util.formatPostfix).toString();
        m = m.substring(prefix.length, m.length - postfix.length);
        var v = m.charAt(0) == "-" ? -v : +v;
        var isNegative = v < 0 ? v = -v : 0;
        var result = m.match(HMR.util.formatMatch);
        var Decimal = (result && result[result.length - 1]) || ".";
        var Group = (result && result[1] && result[0]) || ",";
        var m = m.split(Decimal);
        v = v.toFixed(m[1] && m[1].length);
        v = +(v) + "";
        var pos_trail_zero = m[1] && m[1].lastIndexOf("0");
        var part = v.split(".");
        if (!part[1] || part[1] && part[1].length <= pos_trail_zero) {
            v = (+v).toFixed(pos_trail_zero + 1);
        }
        var szSep = m[0].split(Group);
        m[0] = szSep.join("");
        var pos_lead_zero = m[0] && m[0].indexOf("0");
        if (pos_lead_zero > -1) {
            while (part[0].length < (m[0].length - pos_lead_zero)) {
                part[0] = "0" + part[0];
            }
        } else if (+part[0] == 0) {
            part[0] = "";
        }
        v = v.split(".");
        v[0] = part[0];
        var pos_separator = (szSep[1] && szSep[szSep.length - 1].length);
        if (pos_separator) {
            var integer = v[0];
            var str = "";
            var offset = integer.length % pos_separator;
            for (var i = 0, l = integer.length; i < l; i++) {
                str += integer.charAt(i);
                if (!((i - offset + 1) % pos_separator) && i < l - pos_separator) {
                    str += Group;
                }
            }
            v[0] = str;
        }
        v[1] = (m[1] && v[1]) ? Decimal + v[1] : "";
        return prefix + ((isNegative ? "-" : "") + v[0] + v[1]) + postfix;
    };
    HMR.util.formatPrice = function (amount) {
        return HMR.util.format("$#,###.00", amount);
    };
    HMR.util.isValidNumericInput = function(event) {
        var isValid = false;
        var e = event || window.event;
        var key = e.keyCode || e.which;
        HMR.console.log("key=" + key);
        if (e.which == 0 && (e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 37)) {
            HMR.console.log("keyCode was 35,36, or 37");
            return true;
        }
        if (e.which == 0 && (e.keyCode == 96 || e.keyCode == 45 || e.keyCode == 39 || e.keyCode == 46)) {
            HMR.console.log("keyCode was 96,45, 39 or 46");
            return true;
        }
        if (!e.altKey && !e.ctrlKey && (key >= 48 && key <= 57) || key == 8 || key == 9 || key == 13) {
            isValid = true;
        }
        return isValid;
    };
    HMR.util.scrubQuantity = function(qty, def) {
        var cleanQty = def || 1;
        if ($.isNumeric(qty)) {
            var qtyFloat = parseFloat(qty);
            var qtyInt = parseInt(qty);
            if (qtyInt > 0 && qtyFloat === qtyInt) {
                cleanQty = qty;
            }
        }
        return cleanQty;
    };
})(this.HMR, jQuery);
HMR.uiProperties = {
    CartDetailView : {
        tmpl : "HMR-CartDetails",
        selector : "#cart_items_wrapper",
        orderSummary: {
            tmpl : "HMR_OrderSummaryRightRail",
            selector : "#order_summary_wrapper"
        },
        partials : {
            descriptionModal : "HMR_ProductDescriptionModal",
            nutritionModal : "HMR_ProductNutritionModal",
            ingredientsModal : "HMR_ProductIngredientsModal",
            includedItemsModal : "HMR_IncludedItemsModal"
        }
    },
    CheckOutDetails : {
        OrderItemsView : {
            tmpl : "HMR_OrderItemsView",
            selector : "#order_items_wrapper"
        },
        ShippingAddressView : {
            tmpl : "HMR_OrderShippingAddress",
            selector : "#order_shipping_address_wrapper"
        },
        BillingAddressView : {
            tmpl : "HMR_OrderBillingAddress",
            selector : "#order_billing_address_wrapper"
        },
        PaymentEntryView : {
            tmpl : "HMR_OrderPaymentEntryView",
            selector : "#order_payment_entry_wrapper"
        },
        OrderReviewView : {
            tmpl: "HMR_OrderReviewView",
            selector : "#order_review_wrapper"
        }
    },
    MyAccountView : {
        AboutMeView : {
            tmpl : "HMR_AboutMeView",
            selector : "#about_me_wrapper"
        },
        BillingInformationView : {
            tmpl : "HMR_BillingInformationView",
            selector : "#billing_information_wrapper"
        },
        ChangePasswordView : {
            tmpl : "HMR_ChangePasswordView",
            selector : "#change_password_wrapper",
        },
        MyOrdersView : {
            tmpl : "HMR_MyOrdersView",
            selector : "#my_orders_wrapper"
        },
        ShippingInformationView : {
            tmpl : "HMR_ShippingInformationView",
            selector : "#shipping_information_wrapper"
        }
    },
    OrderDetailView : {
        ContactInformationView : {
            tmpl : "HMR_OrderContactInformation",
            selector : "#order_contact_information_wrapper"
        },
        OrderInformationView : {
            tmpl : "HMR_OrderInformation",
            selector : "#order_information_wrapper"
        },
        OrderDetailView : {
            tmpl : "HMR_OrderDetail",
            selector : "#order_detail_wrapper"
        },
        OrderItemsView :{
            tmpl : "HMR_OrderItemsInformation",
            selector : "#order_items_wrapper"
        },
        OrderSummaryView : {
            tmpl : "HMR_OrderSummaryInformation",
            selector : "#order_summary_wrapper"
        },
        PaymentInformationView : {
            tmpl : "HMR_OrderPaymentInformation",
            selector : "#order_payment_information_wrapper"
        },
        ShippingInformationView : {
            tmpl : "HMR_OrderShippingInformation",
            selector : "#order_shipping_information_wrapper"
        }
    },
    DashboardView : {
        WeeklyDashboardView : {
            tmpl : "HMR_WeeklyDashboardView",
            selector : "#weekly_dashboard_wrapper"
        }
    },
    GroupDataView : {
        GroupView : {
            tmpl: "HMR_GroupView",
            selector: "#group_view_wrapper"
        },
        SessionView : {
            tmpl: "HMR_SessionView",
            selector: "#session_view_wrapper"
        }
    },
    SchedulerLandingView : {
        MainView : {
            tmpl : "HMR_SchedulerLandingView",
            selector : "#scheduler_landing_wrapper"
        }
    },
    SchedulerView : {
        MySchedulerView : {
            tmpl : "HMR_Scheduler",
            selector: "#my_scheduler_wrapper"
        }
    },
    CartWidgetContainerView : {
        CartWidgetView : {
            tmpl : "HMR_CartWidget",
            selector : "#cart_widget_wrapper"
        },
        CartWidgetMobileView : {
            tmpl : "HMR_CartWidgetMobile",
            selector : "#cart_widget_mobile_wrapper"
        }
    }
};
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}
/* DO NOT MODIFY */
