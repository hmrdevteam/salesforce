var pricing = {
    s: function() {
        $(".standard_product_price").removeClass("hidden");
        $(".ff_product_price, .employees_product_price, .placeholder_product_price").remove();
    },
    i: function() {
        if($("#current_user_account_group_name").val() != null && $("#current_user_account_group_name").val() != "") {
            if($("#current_user_account_group_name").val() == "HMR Emp") {
                $(".employees_product_price").removeClass("hidden");
                $(".standard_product_price, .ff_product_price, .placeholder_product_price").remove();
            }
            else if($("#current_user_account_group_name").val() == "HMR FF") {
                $(".ff_product_price").removeClass("hidden");
                $(".standard_product_price, .employees_product_price, .placeholder_product_price").remove();
            }
            else {
                pricing.s();
            }
        }
        else {
            pricing.s();
        }
        pricing.c();
    },
    c: function(){
        //$(".add_to_cart_btn").removeAttr("disabled");
    }
};
var products = {
    e: function(k, m) {
        if(m) {
            return $("#quantity_counter_" + products.is_m(m) + k);
        }
        return $("#quantity_counter_" + k);
    },
    g: function(e, m) {
        return parseInt(products.e(e.attr("data-sku"), m).val());
    },
    s: function(e, m, v) {
        products.e(e.attr("data-sku"), m).val(v);
    },
    is_m: function(c) {
        var m = "";
        if(c) {
            m = "mobile_";
        }
        return m;
    },
    p: function() {
        $(".add-item-btn").unbind("click").bind("click", function() {
            var v = products.g($(this), $(this).hasClass("isMobile"));
            v++;
            products.s($(this), $(this).hasClass("isMobile"), v);
            var p = $(this).parents(".input-group-sm");
            if(p){
                var mb = p.find(".subtract-item-btn");
                if(mb && mb.hasClass("disabled")) {
                    mb.removeClass("disabled").removeAttr("disabled");
                }
            }
        });
    },
    m: function() {
        $(".subtract-item-btn").unbind("click").bind("click", function() {
            var v = products.g($(this), $(this).hasClass("isMobile"));
            var l = JSON.parse($(this).data("limit"));
            if(!l) l = 0;
            if(v > l) {
                v--;
                products.s($(this), $(this).hasClass("isMobile"), v);
                if(v == 0) {
                    $(this).addClass("disabled").attr("disabled", "disabled");
                }
            }
        });
    },
    c: function() {
        $(".add_to_cart_btn").unbind("click").bind("click", function(e) {
            if( !e ) e = window.event;
            var v = products.g($(this));
            if(!isNaN(v) && v > 0){
                var ele = $(this);
                var objLink = $(e.target) || $(e.srcElement); //$(event.currentTarget);
                var priceInput = objLink.parents(".amount-addcart-row").find(".grey");
                var price = priceInput.text();
                price = price.substring(price.indexOf("$")+1);
                cart.a($(this).attr("data-sku"), v, price, $(this).attr("data-itemlabel"));
            }
        });
    },
    d: function() {
        $(".prod-mobile-details").unbind("click").bind("click", function() {
            $(this).toggleText('Details +', 'Details -');
            var e = $("#prodCardWrapper_" + $(this).data("sku"));
            e.toggleClass("hidden-sm").toggleClass("hidden-xs");
        });
    },
    i: function() {
        $(".plus_minus").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $(".plus_minus").each(function() {
            $(this).focusout(function() { if($(this).val() == '') $(this).val("1"); });
        });
        if(window.location.pathname.toLowerCase() == "/shop-hmr-foods" || window.location.search.indexOf("shop-hmr-foods-admin") != -1) {
            products.p();
            products.m();
        }
        products.c();
        products.d();
    }
};
var foods = {
    i: function() {
        $(".hmr-foods-filter .hmr-allcaps").on("click", function() {
            $(".hmr-allcaps").removeClass("selected");
            $(this).addClass("selected");
            var s = $(this).attr("data-section");
            var ele = $("#" + s);
            $(".hmr-food-section").each(function() {
                if($(this).hasClass(s)) {
                    if($(this).hasClass("hidden")) {
                        $(this).removeClass("hidden");
                    }
                }
                else {
                    if(!$(this).hasClass("hidden")) {
                        $(this).addClass("hidden");
                    }
                }
            });
        });
    }
};
var cart = {
    prodSku: '',
    prodQty: 0,
    a: function(sku, quantity, price, itemlabel){ //Add to cart
        var request = {
            action: 'addToCart',
            userId: cart.uId(),
            isGuest: CMS.isGuest,
            encryptedCartId: $("#current_user_cart_encrypted_id").val(),
            sku: sku,
            quantity: quantity,
            price: price,
            itemlabel: itemlabel
        };
        cart.prodSku = sku;
        cart.prodQty = quantity;

        $(".add_to_cart_btn[data-sku='" + sku + "']").button('loading')
        cart.r(request, cart.a_c);
    },
    a_c: function(t, d){
        if(d.success){
            cart.log("Item added to cart");
            $("#current_user_cart_encrypted_id").val(d.encryptedCartId);

            var cartMajorItemCount = parseInt($(".hmr-cart-counter").html());

            if(isNaN(cartMajorItemCount))
                cartMajorItemCount = 0;

            //Set to 1 to auto apply header without having to do another call
            if(typeof(header) !== "undefined"){
                $("#current_user_cart_item_count").val(cartMajorItemCount + cart.prodQty);
                header.i();
            }
        }
        else {
            cart.log("Add Item to Cart Failed");
        }

        if($(".add_to_cart_btn").length > 0){
            $(".add_to_cart_btn").button('reset');
            //Set product quantity textbox back to 0
            $("#quantity_counter_" + cart.prodSku).val(1);
        }

        cart.prodSku = '';
        cart.prodQty = 0;

        cart.log(d);
    },
    ac: function(cc){ //Apply Coupon
        var request = {
            action : "applyCoupon",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val(),
            couponCode : cc
        };
        $(".addCouponBtn").button('loading');
        cart.r(request, cart.ac_c);
    },
    ac_c: function(t, d){
        if(d.success){
            $(".form-group-promo-code").removeClass("has-error");
            $(".promo-code-error").addClass("hidden");
            cart.i();
        }
        else {
            $(".addCouponBtn").button('reset');
            $(".form-group-promo-code").addClass("has-error");
            $(".promo-code-error").removeClass("hidden");
        }

        cart.log(d);
    },
    ad: function(a){ //Adjust Discount
        var request = {
            action : "adjustDiscount",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val(),
            adjustment : a
        };

        $(".adjustDiscountBtn").button('loading');
        cart.r(request, cart.ad_c);
    },
    ad_c: function(t, d){
        if(d.success){
            cart.log("Adjustment Applied");
            cart.i();
        }
        else{
            $(".adjustDiscountBtn").button('reset');
            cart.log("Adjustment Failed");
        }

        cart.log(d);
    },
    c: function(t, d) {
        if(d.success) {
            response = d;

            if(typeof(HMR) !== "undefined" && typeof(HMR.cartDetailModel) !== 'undefined')
                HMR.cartDetailModel.fetch(response);

            if(typeof(HMR) !== "undefined" && typeof(HMR.CartWidgetModel) !== 'undefined')
                HMR.CartWidgetModel.fetch(response);


            if($(".addCouponBtn").length > 0)
                $(".addCouponBtn").button('reset');
            if($(".removeCouponBtn").length > 0)
                $(".removeCouponBtn").button('reset');
            if($(".updateCartBtn").length > 0)
                $(".updateCartBtn").button('reset');
            if($(".adjustDiscountBtn").length > 0)
                $(".adjustDiscountBtn").button('reset');
            if($(".add_to_cart_btn").length > 0)
                $(".add_to_cart_btn").button('reset');
            if($(".removeCartItemBtn").length > 0){
                $(".removeCartItemBtn").button('reset');
                $(".removeCartItemBtn").removeClass("hmr-remove-button-placeholder");
            }

            cart.cac_l(false);

            cart.ic(d);

            if(d.redirectUrl && d.redirectUrl.length > 0)
                window.location.href = redirectUrl;
        }
        else {
            cart.log("Cart Fetch call Failed");
        }
        cart.log(d);
    },
    cac: function(redirectUrl){ //Check automatic coupon
        var request ={
            action : "checkForAutomaticCoupon",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val()
        };

        if(redirectUrl && redirectUrl.length > 0)
            request.redirectUrl = redirectUrl;

        cart.cac_l(true);

        cart.r(request, cart.cac_c);
    },
    cac_c: function(t, d){
        if(d.success){
            cart.log("Auto Coupon Check Successful");
            if(d.redirectUrl && d.redirectUrl.length > 0)
                window.location.href = d.redirectUrl;
            else
                cart.i();
        }
        else{
            cart.log("Auto Coupon Check Failed");
        }

        cart.log(d);
    },
    cac_l: function(isLoading){
        if(typeof(HMR) !== "undefined")
            HMR.calculatingAutoDiscounts = isLoading;

        if($(".order-total-row").length > 0 && $(".discount-check").length > 0){
            if(isLoading){
                $(".order-total-row").addClass("hidden");
                $(".discount-check-loader").button('loading');
                $(".discount-check").removeClass("hidden");
            }
            else{
                $(".discount-check").addClass("hidden");
                $(".discount-check-loader").button('reset');
                $(".order-total-row").removeClass("hidden");
            }
        }
    },
    cec: function(){
        var request = {
            action : "checkForExpiredCoupon",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val()
        }

        return cart.r(request, cart.cec_c);
    },
    cec_c: function(t, d){
        cart.p();
    },
    g: function() { //Get Cart
        if(CMS.isGuest)
            return cart.gg();
        else
            return cart.gu();
    },
    gg: function() { //Get Guest Cart
        var cartCookie = cookie.g('apex__currCartId');

        if(!cartCookie && typeof(HMR) !== "undefined")
            cartCookie = HMR.getQueryParam('cartID', true);

        if(cartCookie !== ""){
            var request = {
                action: 'getCartByEncryptedId',
                userId: cart.uId(),
                encryptedCartId: cartCookie,
                isGuest: true
            }
            return request;
        }
    },
    gu: function() { //Get User Active Cart
        var request = {
            action : "getCartByUser",
            userId : cart.uId(),
            isGuest : false
        };

        return request;
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
           'use strict';
            cart.r(cart.g(), cart.c);
        }(window, STG.CTLib.Idea || {}));
    },
    ic: function(d){
        if(d.cart){
            $("#current_user_cart_sfdc_id").val(d.cart.id);
            $("#current_user_cart_encrypted_id").val(d.cart.encryptedId);
            $("#current_user_cart_owner_id").val(d.cart.ownerId);
            $("#current_user_cart_total_amount").val(d.cart.totalAmount);
            //$("#current_user_cart_item_count").val(d.cart.cartItemList.length);
            $("#current_user_cart_item_count").val(d.cart.totalMajorQuantity);

            if(d.cart.containsKit && typeof(dupKitDialog) !== "undefined")
                dupKitDialog.show();

            if(typeof(header) !== "undefined")
                header.i();
        }
        else if(d.encryptedCartId)
            $("#current_user_cart_encrypted_id").val(d.encryptedCartId);
    },
    icac: function(){ //init with check auto coupon
        var request = {
            action : "checkForAutomaticCoupon",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val()
        };

        cart.cac_l(true);

        cart.r(request, cart.icac_c);
    },
    icac_c: function(t, d){
        if(!d.success)
            cart.log('Initial Check Auto Coupon Failed');

        cart.cec();
        cart.log(d);
    },
    iicac: function(){
       cart.r(cart.g(), cart.iicac_c);
    },
    iicac_c: function(t, d){
        cart.c(t, d);
        cart.icac();
    },
    p: function(){ //Price cart
        var request = {
            action : "priceCart",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val()
        }

        if(CMS.isGuest){
            var cartCookie = cookie.g('apex__currCartId');

            if(!cartCookie && typeof(HMR) !== "undefined")
                cartCookie = HMR.getQueryParam('cartID', true);

            if(cartCookie !== "")
                request.encryptedCartId = cartCookie;
        }

        return cart.r(request, cart.p_c);
    },
    p_c: function(t, d){
        if(!d.success)
            cart.log('Price cart Failed');

        cart.i();
    },
    r: function(ps, c) { //Remote call
        if(ps && window.doServiceRequest) {
            $.orchestracmsRestProxy.doAjaxServiceRequest("HMR_CMS_CCRZ_Cart_ServiceInterface", ps, c); // Not Read-only mode
        }
    },
    rc : function(){ //Remove Coupon
        var request = {
            action : "removeCoupon",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val()
        }
        $(".removeCouponBtn").button('loading');
        return cart.r(request, cart.rc_c);
    },
    rc_c: function(t, d){
        if(d.success){
            cart.log('Coupon Removed');
            cart.i();
        }
        else{
            cart.log("Remove Coupon Failed");
        }

        cart.log(d);
    },
    ri: function(sfId){ //Remove Item
        var request = {
            action : "removeFromCart",
            userId : cart.uId(),
            isGuest : CMS.isGuest,
            encryptedCartId : $("#current_user_cart_encrypted_id").val(),
            sfId : sfId
        }

        if($(".cart-item-row[data-sfid='" + sfId + "']").length > 0){
            $(".cart-item-row[data-sfid='" + sfId + "']").find(".removeCartItemBtn").addClass("hmr-remove-button-placeholder");
            $(".cart-item-row[data-sfid='" + sfId + "']").find(".removeCartItemBtn").button('loading');
        }

        cart.r(request, cart.ri_c);
    },
    ri_c: function(t, d) {
        if(d.success) {
            cart.log("Cart Item Removed");
            cart.p();
        }
        else {
            cart.log("Cart Item remove Failed");
        }
        cart.log(d);
    },
    uc: function() { //Update Cart
        var cartItemArray = [];

        HMR.currentCart.attributes.cartItems.models.forEach(function(ci){
            cartItemArray.push({'sfId':ci.attributes.sfid, 'quantity':ci.attributes.quantity});
        });

        if(cartItemArray.length > 0){
            var request = {
                action : "updateCart",
                userId : cart.uId(),
                isGuest : CMS.isGuest,
                encryptedCartId : $("#current_user_cart_encrypted_id").val(),
                itemList : JSON.stringify(cartItemArray)
            }
            $(".updateCartBtn").button('loading')
            cart.r(request, cart.uc_c);
        }
        else{
            cart.log("Cart is empty");
        }
    },
    uc_c: function(t, d){
        if(d.success) {
            cart.log("Cart Updated");
            cart.p();
        }
        else {
            cart.log("Cart Update Failed");
        }

        cart.log(d);
    },
    uId: function(){
        var userId = $("#current_user_id").val();

        if(typeof(HMR) !== "undefined" && HMR.getQueryParam('portalUser', true) !== "")
            userId = HMR.getQueryParam('portalUser', true);

        return userId;
    },
    log: function(logText){
        if(typeof(HMR) !== "undefined")
            HMR.console.log(logText);
    }
};
var cookie = {
    g: function(cname){
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
};
var filterBar = {
    i: function(){
        $(window).on("scroll touchmove", function() {
            if(($(window).scrollTop() > $('.bg-foodsnarihero').outerHeight(true)) &&
                ($(window).scrollTop() < ($('.kit-config-main').height() + $('.bg-foodsnarihero').height()))) {
                $('.hmr-foods-filter').addClass('hmr-foods-filter-fixed');
            }
            else {
                $('.hmr-foods-filter').removeClass('hmr-foods-filter-fixed');
            }
        });

        $(".hmr-foods-filter [data-section]").click(function(){
            if($('.bg-foodsnarihero').length > 0)
                window.scrollTo(0, $('.bg-foodsnarihero').outerHeight(true));
            filterBar.fc($(this).data("section"));
        });

        filterBar.fc('Shake/Soup');
    },
    fc: function(category){
        switch(category){
                case "entrees-section":
                    $(".info-wrapper-shakes").addClass("hidden");
                    $(".info-wrapper-bars").addClass("hidden");
                    $(".info-wrapper-entrees").removeClass("hidden");
                    $(".product-card[data-categories^='Entrees']").parent().show();
                    $(".product-card:not([data-categories^='Entrees'])").parent().hide();
                    dataLayer.push({
                       'event' : 'detailPushed',
                         'ecommerce': {
                           'currencyCode': 'USD',
                           'detail': {
                           'products': [{
                              'name': 'Entrees Page View',
                              'id': 'ENTREE'
                            }]
                           }
                          }
                       });
                break;
                case "bars-flavorings-section":
                    $(".info-wrapper-shakes").addClass("hidden");
                    $(".info-wrapper-entrees").addClass("hidden");
                    $(".info-wrapper-bars").removeClass("hidden");
                    $(".product-card[data-categories^='Bars']").parent().show();
                    $(".product-card:not([data-categories^='Bars'])").parent().hide();
                    $(".product-card[data-categories^='Flavorings']").parent().show();
                    $(".product-card[data-categories^='Materials']").parent().show();
                    dataLayer.push({
                       'event' : 'detailPushed',
                         'ecommerce': {
                           'currencyCode': 'USD',
                           'detail': {
                           'products': [{
                              'name': 'Bars Page View',
                              'id': 'BAR'
                            }]
                           }
                          }
                       });
                break;
                default:
                    $(".info-wrapper-entrees").addClass("hidden");
                    $(".info-wrapper-bars").addClass("hidden");
                    $(".info-wrapper-shakes").removeClass("hidden");
                    $(".product-card[data-categories^='Shake/Soup']").parent().show();
                    $(".product-card:not([data-categories^='Shake/Soup'])").parent().hide();
                    dataLayer.push({
                       'event' : 'detailPushed',
                         'ecommerce': {
                           'currencyCode': 'USD',
                           'detail': {
                           'products': [{
                              'name': 'Shakes Page View',
                              'id': 'SHAKE'
                            }]
                           }
                          }
                       });
                break;
        }
    }
};
var modal = {
    i: function() {
        $(".hmr-prod-desc-modal, .hmr-prod-nutrition-modal, .hmr-prod-ingredients-modal").on("hidden.bs.modal", function() {
            $(".hmr-prod-desc-modal, .hmr-prod-nutrition-modal, .hmr-prod-ingredients-modal").on("shown.bs.modal", function() {
                $("body").addClass("modal-open");
            });
        });
    }
};
var header = {
    g_u: function(page){
        return navigate.g_u(page);
    },
    i: function(callRef){
        if($(".hmr-header").length > 0){
            var cartItemCount = parseInt($("#current_user_cart_item_count").val());

            if(isNaN(cartItemCount))
                cartItemCount = 0;

            if(typeof(CMS) !== "undefined" && CMS.isGuest) {
                $(".hdr_user_fname").html("Sign In");
                $(".hmrSignIn").attr("href", header.g_u('sign-in'));
                $(".submenu").addClass("hidden");
                $(".hmrSignIn").removeClass("hidden");
            }
            else {
                var firstName = '';

                if(typeof(HMR) !== "undefined" && HMR.getQueryParam("portalUser",true) !== "")
                    firstName = $("#current_user_first_name").val();

                if(!firstName && typeof(CMS) !== "undefined" && CMS.first_name)
                    firstName = CMS.first_name;

                $(".hdr_user_fname").html(firstName);

                //$(".hmrSignIn").attr("href", header.g_u('my-account'));
                $(".submenu").removeClass("hidden");
                $(".hmrSignIn").addClass("hidden");
            }

            if(cartItemCount > 0){
                //$(".hmr-cart-empty").removeClass("hidden");
                //$(".hmr-cart-counter").removeClass("hidden");
                $(".hmr-cart-counter").html(cartItemCount);
                $("#cartli_wrapper a").attr("href", header.g_u('cart'));
            }
            else{
                //$(".hmr-cart-filled").addClass("hidden");
                //$(".hmr-cart-empty").removeClass("hidden");
                //$(".hmr-cart-counter").addClass("hidden");
                if(callRef !== undefined && callRef == 'cartPage'){
                    $(".hmr-cart-counter").html("&nbsp;");
                }else{
                    $(".hmr-cart-counter").html("0");
                }
                $("#cartli_wrapper a").attr("href", "javascript:void(0);");
            }

            // if(cartItemCount > 0){
            //     $(".hmr-cart-empty").addClass("hidden");
            //     $(".hmr-cart-filled").removeClass("hidden");
            //     $("#cartli_wrapper a").attr("href", header.g_u('cart'));
            // }
            // else{
            //     $(".hmr-cart-filled").addClass("hidden");
            //     $(".hmr-cart-empty").removeClass("hidden");
            //     $("#cartli_wrapper a").attr("href", "javascript:void(0);");
            // }
        }
    }
};
var footer = {
    g_u: function(page){
        return navigate.g_u(page);
    },
    i: function(){
        if($("footer.bg-typecover").length > 0){
            $("#link-shop-hmr-foods").attr("href", footer.g_u("shop-hmr-foods"));
        }
    }
};
var navigate = {
    g_u: function(page){
        var url = '';

        if(window.location.href.indexOf("/apex/Main") >= 0){
            page = page.replace('shopping-cart','cart').replace('?','&');
            url = "/apex/Main?name=" + page;
        }
        else
            url = "/" + page;

        url = navigate.q(url);

        return url;
    },
    q: function(url){ //Appends standard query string params
        if(typeof(HMR) !== "undefined"){
            var portalUser = HMR.getQueryParam('portalUser', true);
            var cartId = HMR.getQueryParam('cartID', true);

            if(!cartId)
                cartId = $("#current_user_cart_encrypted_id").val();

            if(portalUser){
                url = url.replace('shop-hmr-foods', 'shop-hmr-foods-admin');
                url += (url.indexOf('?') >= 0 ? "&" : "?") + "portalUser=" + portalUser;
            }

            if(cartId && url.toUpperCase().indexOf('CONFIRMATION') < 0)
                url += (url.indexOf('?') >= 0 ? "&" : "?") + "cartID=" + cartId;
        }

        return url;
    }
};
$.fn.toggleText = function(t1, t2){
    if (this.text() == t1)
        this.text(t2);
    else
        this.text(t1);
    return this;
};
$(document).ready(function() {
    foods.i();
    //pricing.i();
    products.i();
    modal.i();
});
$(document).ocmsDynamicLoadFinished(function() {
    //cart page handles separately
    if($(".cart-page").length <= 0)
        cart.i();
    header.i('cartPage');
    footer.i();
    pricing.i();
    if($(".hmr-foods-filter").length > 0) filterBar.i();
});
