/**
* Main JavaScript for the HMR Community
*
* @Date: 2016-12-06
* @Author Pranay Mistry (Magnet 360)
*/
var HMR = [];
(function(){
    'use strict';
    delegateEvent('[data-toggle="collapse"]', 'click', collapseToggleHandler);
    delegateEvent('[data-toggle="collapse-card"]', 'click', collapseCardToggleHandler);
    delegateEvent('.shakes', 'change', shakeFlavorSelectionHandler);

    function shakeFlavorSelectionHandler(event) {
        if ( this.checked ) {

            // Get currently selected SKU
            var currentSku = window.location.href.slice(window.location.href.indexOf('sku=') + 4);
            console.log('currentSku: ' + currentSku);

            // Remove flavor from currently selected SKU
            var skuWithoutFlavor =
                currentSku.substr(0, currentSku.length - (currentSku.endsWith('VC') ? 2 : 1));
            console.log('skuWithoutFlavor: ' + skuWithoutFlavor);

            // Calculate newly selected Flavor
            var selectedSkuFlavor;
            console.log('this.nextSibling.data: ' + this.nextSibling.data);
            if ( this.nextSibling.data == 'Vanilla' ) {
                selectedSkuFlavor = 'V';
            } else if ( this.nextSibling.data == 'Chocolate' ) {
                selectedSkuFlavor = 'C';
            } else {
                selectedSkuFlavor = 'VC';
            }
            console.log('selectedSkuFlavor = : ' + selectedSkuFlavor );

            // If user chose a new flavor, redirect with the new SKU
            if ( currentSku != (skuWithoutFlavor + selectedSkuFlavor) ) {
                var url =
                    window.location.href.substring(0, window.location.href.indexOf('?')) +
                        '?sku=' + skuWithoutFlavor + selectedSkuFlavor;
                console.log('url 1: ' + url);
                window.location.href = url;
            }
        }
    }
    function collapseToggleHandler(event) {
        var target = (this.dataset['target']) ? document.getElementById(this.dataset['target']) : null;
        if(target && target.firstElementChild){
            event.preventDefault();
            if(target.classList.contains('in')){
                target.style.height = null;
            } else {
                //target.style.height = target.firstElementChild.offsetHeight + 'px';
                target.style.height = '300px';
            }
            target.classList.toggle('in');
            this.classList.toggle('collapse-in');
        }
    }
    function collapseCardToggleHandler(event) {
        var target = (this.dataset['target']) ? document.getElementById(this.dataset['target']) : null;
        if (target && target.firstElementChild) {
            event.preventDefault();
            if (target.classList.contains('in')) {
                target.style.height = null;
            } else {
                //target.style.height = target.firstElementChild.offsetHeight + 'px';
                target.style.height = '420px';
            }
            target.classList.toggle('in');
            this.classList.toggle('collapse-in');
        }
    }
    /**
     * Handle events whose target matches a selector.
     * @param  {string} selector     CSS selector
     * @param  {string} eventType    The type of event to handle
     * @param  {function} handler    The event handler
     * @param  {object} elementScope (optional) Defaults to document
     * @return {void}
     */
    function delegateEvent(selector, eventType, handler, elementScope) {
        (elementScope || document).addEventListener(eventType, function(event) {
            var listeningTarget = closest(event.target, selector);
            if (listeningTarget) {
                handler.call(listeningTarget, event);
            }
        });
    }
    /**
     * Get an element's nearest parent element matching a selector
     * @param  {object} el           Element to compare with
     * @param  {object} selector     CSS selector
     * @return {object}              Returns a DOM element, or null if no match is found
     */
    var closest = (function() {
        var el = HTMLElement.prototype;
        var matches = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;

        return function closest(el, selector) {
            if(!el || !selector){
                return null;
            }
            return matches.call(el, selector) ? el : closest(el.parentElement, selector);
        };
    })();
    function checkWidth() {
        var windowsize = $(window).width();
        if (windowsize >= 768) {
            $('#primaryNav').css('height', '');
        }
    };
    $(window).resize(checkWidth);
})();
var ccContextItems = [];var loginUrl = "sign-in";var myAccountUrl = "my-account";var loginLander = "HMR_Community_LoginLander";
var isGuest = true;var site_prefix = "";var first_name = "";var welcomeUrl = "welcome";var plansUrl = "healthy-solutions-at-home-diet-plan";
var hmr_a = {
    r: function() {
        $("#hmr_register_btn").click(function(e) {
            window.location.href = "register";
        });
    },
    c: function() {
        $("#cartli_wrapper").find("input[type='hidden']").each(function() {
            if($(this)) {
                var k = $(this).attr("id");
                var v = $(this).val();
                ccContextItems.push({
                    k : v
                });
                if(typeof CCRZ != "undefined") {
                    if(hmr_u.g("portalUser") == null || hmr_u.g("portalUser") == "") {
                        cartDetails = function() {
                            document.location = "/shopping-cart?cartID=" + CCRZ.pagevars.currentCartID + getCSRQueryString();
                        }
                    }
                    $("#cartli_wrapper a").attr("href", "javascript:void(0);");
                    $("#cartli_wrapper").on("click", function() {
                        cartDetails(CCRZ.pagevars.currentCartID);
                    });
                }
            }
        });
    },
    f: function() {
        var l = $("#footer nav:eq(1)").find("li.first").find("a");
        if(l.length > 0) {
            hmr_u.a(l, l.attr("href"));
        }
    },
    e: function() {
        $(".eyeclosed").on("click", function(e) {
            var i = $(this).parent().parent().find("input");
            var v = i.val();
            if(i.attr("type").toLowerCase() == "password") {
                i.attr("type", "text");
            }
            else {
                i.attr("type", "password");
            }
            i.val(v);
            e.preventDefault();
        });
    },
    g: function() {
        if(typeof CMS == "undefined" && typeof CCRZ != "undefined") {
            isGuest = CCRZ.pagevars.isGuest;
            CCRZ.pubSub.on('view:myAccountHeaderView:refresh', function(theView) {
                if(CCRZ.currentUser != undefined) {
                   first_name = CCRZ.currentUser.FirstName;
                }
                w.n();

                var c = $(".shopping-cart-count-bubble").html();
                if(c != null && c != "" && parseInt(c) > 0) {
                    cc.b(parseInt(c));
                }
            });
            site_prefix = CCRZ.pagevars.currSiteURL;
            HMR['pageVars'] = { 'cartId': CCRZ.pagevars.currentCartID };
        }
        if(typeof CCRZ == "undefined" && typeof CMS != "undefined") {
            isGuest = CMS.isGuest;
            first_name = CMS.first_name;
            if(CMS.site_prefix != null) {
                site_prefix = CMS.site_prefix + "/";
            }
            else {
                site_prefix = "/";
            }
        }
        if(typeof CCRZ == "undefined" && typeof CMS == "undefined") {
            isGuest = false;
            first_name = $("#hmr_vf_first_name").val();
        }
    },
    l: function(){
        var portalUser = hmr_u.p("portalUser");
        var linkShopHMRFoods = $("#link-shop-hmr-foods");
        if(portalUser && portalUser.length > 0 && linkShopHMRFoods && linkShopHMRFoods.length > 0){
            if($(linkShopHMRFoods).attr('href').indexOf('portalUser') < 0)
                $(linkShopHMRFoods).attr('href', $(linkShopHMRFoods).attr('href') + '&portalUser=' + portalUser);
        }
    },
    i: function() {
        hmr_a.c();
        hmr_a.r();
        hmr_a.e();
        hmr_a.g();
        hmr_a.l();
    }
};
/* Welcome */
var w = {
    b: function() {
        window.location.href = site_prefix + myAccountUrl;
    },
    i: function() {
        if(window.location.pathname.indexOf("welcome") != -1 || window.location.pathname.indexOf("consent") != -1) {
            $("#welcome_user").html("Welcome " + first_name);
        }
        w.n();
    },
    n: function() {
        if(isGuest) {
            $(".hdr_user_fname").html("Sign In");
            $("#hmrSignIn").attr("href", site_prefix + loginUrl);
            $(".submenu").addClass("hidden");
            $(".hmrSignIn").removeClass("hidden");
        }
        else {
            $(".hdr_user_fname").html(first_name);
            $(".submenu").removeClass("hidden");
            $(".hmrSignIn").addClass("hidden");
            if(typeof CCRZ == "undefined" || typeof CMS != "undefined") {
                hmr_u.a($("#hmrSignIn"), site_prefix + myAccountUrl);
            }
            else {
                $("#hmrSignIn").attr("href", "javascript:void(0);");
                $(".hmrSignIn").on("click", function() {
                    //CCRZ.headerView.goToAccount();
                    if(typeof CCRZ != "undefined") {
                        window.location.href = '/my-account?cartId=' + CCRZ.pagevars.currentCartID + '&viewState=viewAccount' + getCSRQueryString() ;
                    }
                    else {
                        window.location.href = '/my-account';
                    }
                });
            }
        }
    }
};
/* Registration */
var r = {
    r: function() {
        if(!isGuest && window.location.pathname.toLowerCase().indexOf("register") != -1) {
            window.location.href = site_prefix;
        }
        else {
            $.validator.addMethod("alphanumeric", function(value, element) {
                    // allow any non-whitespace characters as the host part
                    return this.optional( element ) || /[a-z].*[0-9]|[0-9].*[a-z]/i.test( value );
                },
                'Please enter a valid email address.'
            );
            $("#hmr_registration").validate({
                errorLabelContainer: $("#hmr_registration div.form-error"),
                rules :{
                    r_fn : {
                        required : true,
                        maxlength: 255
                    },
                    r_ln : {
                        required : true,
                        maxlength: 255
                    },
                    r_email : {
                        required : true,
                        email: true
                    },
                    r_dn : {
                        maxlength: 40
                    },
                    r_password : {
                        required : true,
                        minlength: 6,
                        alphanumeric: true
                    },
                    r_cpassword : {
                        equalTo: "#hmr_reg_inputPassword"
                    },
                    r_zipcode : {
                        required : true,
                        minlength: 5,
                        maxlength: 5,
                        digits: true
                    },
                    r_termsprivacy : {
                        required : true
                    }
                },
                messages: {
                    r_fn : {
                        required : 'Please enter your First Name',
                        maxlength: 'Please enter less than 255 characters'
                    },
                    r_ln : {
                        required : 'Please enter your Last Name',
                        maxlength: 255
                    },
                    r_email : {
                        required : 'Please enter your Email Address',
                        email: 'Please enter a valid Email Address'
                    },
                    r_dn : {
                        maxlength: 'Please enter less than 40 characters'
                    },
                    r_password : {
                        required : 'Please enter a password',
                        minlength: 'Password should be a minimum of 6 characters',
                        alphanumeric: 'Password should contain at least 1 letter and 1 number'
                    },
                    r_cpassword : {
                        equalTo: 'Password and Confirm password do not match'
                    },
                    r_zipcode : {
                        required : 'Please enter your zip code',
                        minlength: 'Your Zip Code must be 5 numbers!',
                        maxlength: 'Your Zip Code must be 5 numbers!',
                        digits: 'You can only enter numbers for Zip Code'
                    },
                    r_termsprivacy : {
                        required : 'Please agree to terms and conditions'
                    }
                },
                invalidHandler: function(f, v) {
                    var e = v.numberOfInvalids();
                    if (e) {
                        $("html, body").animate({ scrollTop: 0 }, "fast");
                    }
                },
                submitHandler: function(f) {
                    waitingDialog.show();
                    r.s();
                }
            });
            //r.s();
        }
    },
    n: function(s, ac, ec, u, p, e, cp, n, f, l, t, tp, ep, z, c) {
        var ps = {
            action: 'register',
            ac : ac,
            ec: ec,
            u : u,
            p : p,
            e : e,
            cp : cp,
            n : n,
            f : f,
            l : l,
            t : t,
            tp : tp,
            ep : ep,
            z : z
        };
        waitingDialog.msg("Registering...");
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, ps, c); // Not Read-only mode
    },
    o: function(e) {
        var v = '';
        if($("#" + e).length != 0) {
            v = $("#" + e).val();
        }
        return v;
    },
    b: function(e) {
        var v = '';
        if ($("#" + e).length != 0 && $("#" + e).is(":checked")){
            v = 'true';
        }
        else {
            v = 'false';
        }
        return v;
    },
    c: function(t, d) {
        console.log(d);
        if(d.success) {
            waitingDialog.msg("Success...");
            window.location.href = d.redirectUrl;
        }
        else {
            //TODO: show failure message
            var errorStr = '';
            if(d.exceptionMsg == '[User already exists.]'){
                errorStr = 'There is already a user with this email.';
            }else{
                errorStr = d.exceptionMsg;
            }
            $("#hmr_reg_inputRegistration-error").html(errorStr);
            $("#hmr_reg_inputRegistration-error").show();
            $("#regErrorDiv").show();
            $("html, body").animate({ scrollTop: 0 }, "fast");

            console.log('Registration Failed');
            console.log(d);
            waitingDialog.rmsg();
            waitingDialog.hide();
        }
    },
    s: function() {
        var s = 'HMR_CMS_RegistrationServiceInterface';
        var ec = r.o('hmr_reg_inputEmployerCode');
        var u = $("#hmr_reg_inputEmail").val();
        var p = $("#hmr_reg_inputPassword").val();
        var e = u;
        var cp = $("#hmr_reg_inputConfirmPassword").val();
        var n = $("#hmr_reg_inputDisplayName").val();
        var f = $("#hmr_reg_inputFirstName").val();
        var l = $("#hmr_reg_inputLastName").val();
        var ref = hmr_u.p("startUrl");
        if(typeof site_prefix == "undefined" || site_prefix == null) {
            site_prefix = "";
        }
        //Check for force.com in case came from shared link or some outside source,then take to welcome
        var t = ref && (ref.indexOf('500shake') > 0 || ref.indexOf('hmr') > 0 || ref.indexOf('force.com') > 0 || ref == 'checkout') ? ref : site_prefix + welcomeUrl;
        var tp = site_prefix + plansUrl;
        var ep = r.b('hmr_reg_emailpreference');
        var z = r.o('hmr_reg_inputZipCode');
        var ac = hmr_u.p('cartID');
        if(!ac) ac = '';

        r.n(s, ac, ec, u, p, e, cp, n, f, l, t, tp, ep, z, r.c);
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            r.r();
        }(window, STG.CTLib.Idea || {}));
    }
};
/* url parameters */
var hmr_u = {
    g: function(n, u) {
        if (!u) {
            u = window.location.href;
        }
        n = n.replace(/[\[\]]/g, "\\$&");
        var r = new RegExp("[?&]" + n + "(=([^&#]*)|&|#|$)"), s = r.exec(u);
        if (!s) return null;
        if (!s[2]) return '';
        return decodeURIComponent(s[2].replace(/\+/g, " "));
    },
    q: function(l) {
        var r = {};
        if(l == null) {
            l = location.search;
        }
        if(l){
            var s = l.slice(1).split('&');
            var i;
            for(i=0;i<s.length;i++){
                var p = s[i].split('=');
                var n = p[0];
                var v = decodeURIComponent(p[1] || '');
                if(r[n]){
                    if(_.isArray(r[n])){
                        r[n].push(v);
                    }
                    else{
                        r[n] = [r[n],v];
                    }
                }
                else{
                    r[n] = v;
                }
            }
        }
        return r;
    },
    c: function(k) {
        var n = k + "=";
        var d = decodeURIComponent(document.cookie);
        var ca = d.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(n) == 0) {
                return c.substring(n.length, c.length);
            }
        }
        return "";
    },
    a: function(a, u) {
        if(a != null) {
            var h = "";
            if(typeof hmr_u.q() != "undefined") {
                $.each(hmr_u.q(), function(i, v) {
                    if(h.indexOf(i) == -1) {
                        h += i + "=" + v + "&";
                    }
                });
                if(u.indexOf("?") != -1) {
                    var l = u.substring(u.indexOf("?"));
                    u = u.substring(0, u.indexOf("?"));
                    $.each(hmr_u.q(l), function(i, v) {
                        if(h.indexOf(i) == -1) {
                            h += i + "=" + v + "&";
                        }
                    });
                }
                if(typeof HMR.pageVars != "undefined") {
                    $.each(HMR.pageVars, function(i, v) {
                        h += i + "=" + v + "&";
                    });
                }
                var l = u;
                if(h.length > 0) {
                    l += "?" + h.substring(0, h.length - 1);
                }
                a.attr("href", l);
            }
        }
    },
    p: function(n){
        var match = RegExp('[?&]' + n + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }
};

/* CC Context Service Interface */

var cc = {
    ps: {
        action: 'context',
        activeCartId: hmr_u.p('id'),
        portalUser: hmr_u.p('portalUser')
    },
    n: function(s, c) {
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, cc.ps, c); // Not Read-only mode
    },
    b: function(c) {
        //$(".shopping-cart-badge").find("span").html(c);
        //$(".shopping-cart-badge").removeClass("hidden");
        if(c > 0) {
            $(".hmr-cart-empty").addClass("hidden");
            $(".hmr-cart-filled").removeClass("hidden");
        }
        else{
            $(".hmr-cart-filled").addClass("hidden");
            $(".hmr-cart-empty").removeClass("hidden");
        }
    },
    p: function(d) {
        if(d.cartCount == 0) {
            HMR['pageVars'] = { 'cartId': hmr_u.c('apex__currCartId') };
        }
        if(d.cartCount > 0 && d.outputData != null) {
            HMR['pageVars'] = { 'cartId': d.outputData.cartList[0].encryptedId };
        }
    },
    l: function() {
        if(typeof CMS != "undefined") {
            hmr_u.a($("#cartli_wrapper a"), "shopping-cart");
            hmr_a.f();
        }
    },
    c: function(t, d) {
        if(d.success) {
            cc.b(d.cartCount);
            cc.p(d);
            cc.l();
        }
        else {
            console.log('HMR_CMS_CCContextServiceInterface call Failed');
            console.log(d);
        }
    },
    r: function() {
        var s = 'HMR_CMS_CCContextServiceInterface';
        if(window.doServiceRequest) {
            cc.n(s, cc.c);
        }
        else {
            // var l = $("#footer nav:eq(1)").find("li.first").find("a");
            // var h = l.attr("href");
            // var i = h.substring(h.indexOf("?"));
            // l.attr("href", "javascript:void(0);");
            // if(typeof CCRZ != "undefined") {
            //     l.on("click", function() {
            //         productList(hmr_u.q(i).categoryId);
            //     });
            // }
            // else {
            //   l.on("click", function() {
            //       //TODO: Go back to A la Carte shopping page for non CMS non CC Pages
            //       //document.location = "/apex/ccrz__Products?viewState=ListView&cartID=" + hmr_u.q(i).cartID + "&categoryId=" + categoryId + getCSRQueryString();
            //   });
            // }
            if(typeof CCRZ == "undefined" && typeof CMS == "undefined" && typeof HMR_CMS_CCContextServiceInterface != "undefined") {
                HMR_CMS_CCContextServiceInterface.getCartCountRemoteActionCall(
                    cc.ps,
                    function(t, d) {
                        cc.c(t, JSON.parse(d.result));
                    },
                    {
                        escape: false
                    }
                );
            }
        }
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            cc.r();
        }(window, STG.CTLib.Idea || {}));
    }
};

/* Login */
var l = {
    n: function(s, u, p, r, ac, c) {
        var ps = {
            action: 'login',
            u : u,
            p : p,
            r : r,
            ac: ac
        };
        $.orchestracmsRestProxy.doAjaxServiceRequest(s, ps, c); // Not Read-only mode
    },
    c: function(t, d) {
        console.log(d);
        if(d.success) {
            window.location.href = d.redirectUrl;
        }
        else {
            //TODO: show failure message
            var errorStr = '';
            // if(d.exceptionMsg == '[User already exists.]'){
            //     errorStr = 'There is already a user with this email.';
            // }else{
                errorStr = d.exceptionMsg;
            // }
            $("#hmr_reg_inputLogin-error").html(errorStr);
            $("#hmr_reg_inputLogin-error").show();
            $("#loginErrorDiv").show();
            $("html, body").animate({ scrollTop: 0 }, "fast");


            console.log('Login call Failed');
            console.log(d);
        }
    },
    r: function() {
        if(!isGuest && window.location.pathname.toLowerCase().indexOf("login") != -1) {
            window.location.href = site_prefix;
        }
        else {
            $("#hmr_loginform").validate({
                errorLabelContainer: $("#hmr_loginform div.form-error"),
                rules :{
                    l_email : {
                        required : true
                    },
                    l_password : {
                        required : true,
                        minlength: 6
                    }
                },
                messages: {
                    l_email : {
                        required : "Please enter your email!"
                    },
                    l_password : {
                        required : "Please enter your password!",
                        minlength : "Please enter a valid password!"
                    }
                },
                submitHandler: function(f) {
                    l.s();
                }
            });
        }
    },
    s: function() {
        var s = 'HMR_CMS_LoginServiceInterface';
        var u = $("#hmr_signin_inputEmail").val();
        var p = $("#hmr_signin_password").val();
        var ac = hmr_u.p("cartID"); //active cart
        var k = hmr_u.p("hasKitCart");

        //var r = site_prefix + myAccountUrl;
        var ref = k == null ? document.referrer : "checkout";
        //Check for force.com in case came from shared link or some outside source,then take to welcome
        var r = ref && (ref.indexOf('500shake') > 0 || ref.indexOf('hmr') > 0 || ref.indexOf('force.com') > 0 || ref == "checkout") ? ref : site_prefix + loginLander;
        if(!ac) ac = '';
        l.n(s, u, p, r, ac, l.c);
    },
    i: function() {
        window.STG = window.STG || {};
        window.STG.CTLib = window.STG.CTLib || {};
        window.STG.CTLib.Idea = (function(global, namespace, undefined) {
            'use strict';
            l.r();
        }(window, STG.CTLib.Idea || {}));
    }
};
$(document).ready(function(){
    cc.i();
    hmr_a.i();
    l.i();
    r.i();
    w.i();
});
