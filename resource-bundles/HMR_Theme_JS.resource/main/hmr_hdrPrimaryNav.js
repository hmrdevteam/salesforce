function createSimpleTextAttribute(inputId) {
    var $element = $('#' + inputId);
    return {
        name: inputId,
        value: $element.val(),
        type: 'Text',
        simple: true
    };
}
function createPageSelectorAttribute(str, inputObj) {
	var $element = $('#' + str + 'PageLink');
	return {
		name: str + 'LinkObj',
        value: $element.page_selector_input('getSerializedLink'),
        type: 'Link',
        lang: ''
	};
}
function getAttributes() {
    var attributes = [];
    attributes.push(createPageSelectorAttribute('Plans', PlansPageLinkObj));
    attributes.push(createPageSelectorAttribute('Recipe', RecipePageLinkObj));
    attributes.push(createPageSelectorAttribute('Resources', ResourcesPageLinkObj));
    attributes.push(createSimpleTextAttribute('PlansText'));
    attributes.push(createSimpleTextAttribute('RecipeText'));
    attributes.push(createSimpleTextAttribute('ResourcesText'));
    return attributes;
}
function init() {	
	ce.content_editor('registerSaveFunction', getAttributes);
}
$(document).ready(init);