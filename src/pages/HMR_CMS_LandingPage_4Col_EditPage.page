<apex:page controller="cms.CreateContentController" extensions="HMR_CMS_LandingPage_4Col_ContentTemplate" showHeader="false" standardStylesheets="false" sidebar="false" cache="false">
	<apex:stylesheet value="{!URLFOR($Resource.HMR_CC_Theme, 'css/ocms_fix.css')}"/>
	<script type="text/javascript" language="javascript">
		function createSimpleTextAttribute(inputId) {
			var $element = $('#' + inputId);
			return {
				name: inputId,
				value: $element.val(),
				type: 'Text',
				simple: true
			};
		}
		function createTextAttribute(inputId) {
			var $element = $('#' + inputId);
			return {
				name: inputId,
				value: $element.val(),
				type: 'Text',
				simple: false
			};
		}
		function createSelectAttribute(inputId, value) {
	    	var $element = $('#' + inputId);
		    return {
		        name: inputId,
		        value: $("#" + inputId + " :selected").val(),
		        type: 'Text',
		        simple: true
		    };
		}
		function createCheckboxAttribute(inputId) {
			var $element = $('#' + inputId);
			return {
		        name: inputId,
		        value: $element.is(':checked').toString(),
		        type: 'Boolean',
		        lang: ''
		    };
		}
		function getAttributes() {
			var attributes = [];
			attributes.push(createSimpleTextAttribute('Title'));
			attributes.push(createTextAttribute('Description'));
            attributes.push(createSelectAttribute('numberOfColumns', '{!numberOfColumns}'));
            attributes.push(createSimpleTextAttribute('image1Url'));
			attributes.push(createTextAttribute('image1Caption'));
            attributes.push(createSimpleTextAttribute('image2Url'));
			attributes.push(createTextAttribute('image2Caption'));
            attributes.push(createSimpleTextAttribute('image3Url'));
			attributes.push(createTextAttribute('image3Caption'));
            attributes.push(createSimpleTextAttribute('image4Url'));
			attributes.push(createTextAttribute('image4Caption'));
			attributes.push(createCheckboxAttribute('showButton'));
			attributes.push(createSimpleTextAttribute('buttonText'));
			attributes.push(createSimpleTextAttribute('buttonTargetUrl'));
			attributes.push(createSimpleTextAttribute('buttonMobileTargetUrl'));
			attributes.push(createSimpleTextAttribute('sectionGTMIDAttr'));
			attributes.push(createSimpleTextAttribute('name1'));
			attributes.push(createSimpleTextAttribute('name2'));
			attributes.push(createSimpleTextAttribute('name3'));
			return attributes;
		}
		function init() {
			ce.content_editor('registerSaveFunction', getAttributes);
		}
		$(document).ready(init);
	</script>
	<table class="ocmsContentEditor ocmsContentEditorNested">
		 <tbody>
		   	<tr class="ocmsEditorSubtitle">
 				<td colspan="2">Number of Columns</td>
 			</tr>
		   	<tr class="attribute-container">
                <td>
                    <label class = "ocmsLabel" > Number of Columns: </label>
                </td>
                <td>
                    <select class="numberOfColumns" id="numberOfColumns" name="numberOfColumns" >
                        <option value='3'> 3 </option>
                        <option value='4'> 4 </option>
                    </select>
                </td>
            </tr>
			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Title</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Title: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="Title" name="Title" type="text" value="{!Title}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="Title" name="Title" type="text" value="{!Title}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the title text for the Plans Details Section</p>
 				</td>
 			</tr>
			<tr class="ocmsEditorSubtitle">
 				<td colspan="2">Description</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Description: </label>
 				</td>
 				<td>
					<apex:outputText escape="false">
                        <textarea name="Description" id="Description" rows="10" cols="50">{!Description}</textarea>
                    </apex:outputText>
					<p class="ocmsHelpText">Please Enter the Description for Section</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Image 1 </td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 1 Url: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="image1Url" name="image1Url" type="text" value="{!image1Url}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="image1Url" name="image1Url" type="text" value="{!image1Url}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the URL for Image 1</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 1 Caption: </label>
 				</td>
 				<td>
					<apex:outputText escape="false">
                        <textarea name="image1Caption" id="image1Caption" rows="10" cols="10">{!image1Caption}</textarea>
                    </apex:outputText>
					<p class="ocmsHelpText">Please Enter the Caption for Image 1</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Image 2 </td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 2 Url: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="image2Url" name="image2Url" type="text" value="{!image2Url}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="image2Url" name="image2Url" type="text" value="{!image2Url}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the URL for Image 2</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 2 Caption: </label>
 				</td>
 				<td>
					<apex:outputText escape="false">
                        <textarea name="image2Caption" id="image2Caption" rows="10" cols="10">{!image2Caption}</textarea>
                    </apex:outputText>
					<p class="ocmsHelpText">Please Enter the Caption for Image 2</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Image 3 </td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 3 Url: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="image3Url" name="image3Url" type="text" value="{!image3Url}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="image3Url" name="image3Url" type="text" value="{!image3Url}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the URL for Image 3</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 3 Caption: </label>
 				</td>
 				<td>
					<apex:outputText escape="false">
                        <textarea name="image3Caption" id="image3Caption" rows="10" cols="10">{!image3Caption}</textarea>
                    </apex:outputText>
					<p class="ocmsHelpText">Please Enter the Caption for Image 3</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Image 4 </td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 4 Url: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="image4Url" name="image4Url" type="text" value="{!image4Url}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="image4Url" name="image4Url" type="text" value="{!image4Url}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the URL for Image 4</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Image 4 Caption: </label>
 				</td>
 				<td>
					<apex:outputText escape="false">
                        <textarea name="image4Caption" id="image4Caption" rows="10" cols="10">{!image4Caption}</textarea>
                    </apex:outputText>
					<p class="ocmsHelpText">Please Enter the Caption for Image 4</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
				<td colspan="2">Show Section Button</td>
			</tr>
			<tr class="attribute-container">
				<td>
					<label class = "ocmsLabel" > Show/Hide: </label>
				</td>
				<td>
					<apex:outputText escape="false" rendered="{!disableAll}">
	                    <input type="checkbox" id="showButton" disabled="disabled" />
						<label for="showButton">Show Section Button</label>
	                </apex:outputText>
	                <apex:outputText escape="false" rendered="{!!disableAll}">
	                    <input type="checkbox" id="showButton" />
						<label for="showButton">Show Section Button</label>
	                </apex:outputText>
	                <p class="ocmsHelpText">Please check this box if you would like to display button in Section</p>
				</td>
			</tr>
			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Button Text: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="buttonText" name="buttonText" type="text" value="{!buttonText}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="buttonText" name="buttonText" type="text" value="{!buttonText}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter Text for Button</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Button Target URL: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="buttonTargetUrl" name="buttonTargetUrl" type="text" value="{!buttonTargetUrl}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="buttonTargetUrl" name="buttonTargetUrl" type="text" value="{!buttonTargetUrl}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter Target URL for Button</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Button Mobile Target URL: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="buttonMobileTargetUrl" name="buttonMobileTargetUrl" type="text" value="{!buttonMobileTargetUrl}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="buttonMobileTargetUrl" name="buttonMobileTargetUrl" type="text" value="{!buttonMobileTargetUrl}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter Mobile Target URL for Button</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> CTA Tag </td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > GTM ID: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="sectionGTMIDAttr" name="sectionGTMIDAttr" type="text" value="{!sectionGTMIDAttr}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="sectionGTMIDAttr" name="sectionGTMIDAttr" type="text" value="{!sectionGTMIDAttr}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the GTM ID for CTA</p>
 				</td>
 			</tr>
 			<tr class="ocmsEditorSubtitle">
 				<td colspan="2"> Google Review Names</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Name 1: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="name1" name="name1" type="text" value="{!name1}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="name1" name="name1" type="text" value="{!name1}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the name for the first google review</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Name 2: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="name2" name="name2" type="text" value="{!name2}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="name2" name="name2" type="text" value="{!name2}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the name for the second google review</p>
 				</td>
 			</tr>
 			<tr class="attribute-container">
 				<td>
 					<label class = "ocmsLabel" > Name 3: </label>
 				</td>
 				<td>
 					<apex:outputText escape="false" rendered="{!disableAll}">
 	                    <input id="name3" name="name3" type="text" value="{!name2}" disabled="disabled"></input>
 	                </apex:outputText>
 	                <apex:outputText escape="false" rendered="{!!disableAll}">
 	                    <input id="name3" name="name3" type="text" value="{!name3}"></input>
 	                </apex:outputText>
 	                <p class="ocmsHelpText">Please Enter the name for the third google review</p>
 				</td>
 			</tr>
		</tbody>
	</table>
</apex:page>