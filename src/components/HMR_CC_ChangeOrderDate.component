<apex:component controller="HMR_CC_OrderView_Controller">
    <script type="text/javascript">
    	var orderType;
    	var changeOrderDate = {
            onChangeDietStartDate : function(){
                this.openDietStartDateWidget();
            },
			onChangeOrderDate : function(){
				if(orderType)
                    this.openDateWidget();
                else
                   this.getOrderType(); 
			},
            openDietStartDateWidget : function(){
                $("#dietStartDate").Zebra_DatePicker({
                    container: $("#calendarWidget"),
                    show_clear_date: 'FALSE',
                    onSelect: function(formattedDate, standardDate, dateObject, relatedElement){
                        changeOrderDate.updateDietStartDate(dateObject);
                    }
                });
                $("#dietStartDate").trigger("click");
            },
            openDateWidget : function(){
                //Get the Current Order Date
                var currentOrderDate = new Date(parseInt($("#orderDate").val()));
                var maxDays = 13; //14 but include the day after already

                //For P2 First Order - use the created date as the original date as they can only make changes from that date on
                if(orderType && orderType == "P2 1st Order" && $("#p2FirstOrderOriginalDate").length > 0){
                    currentOrderDate = new Date(parseInt($("#p2FirstOrderOriginalDate").val()));
                    currentOrderDate = currentOrderDate.setDate(currentOrderDate.getDate() - 1);
                }
                
                if(orderType && orderType.indexOf("P2") >= 0){
                    if(orderType == 'P2 1st Order')
                        maxDays = 30;
                    else
                        maxDays = 29; //30 but included the day after already
                }
                $("#orderDate").Zebra_DatePicker({
					direction: [this.getFormattedDate(currentOrderDate, "zebra"), maxDays],
                    container: $("#calendarWidget"),
                    show_clear_date: 'FALSE',
                    onSelect: function(formattedDate, standardDate, dateObject, relatedElement){
                        changeOrderDate.updateOrderDate(dateObject);
                    }
				});
				$("#orderDate").trigger("click");
            },
            updateDietStartDate : function(dateObject){
                var contactId = $("#contactId").val(); 
                var programMembershipId = $("#programMembershipId").val();
                var dietStartDate = this.getFormattedDate(dateObject);

                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.HMR_CC_OrderView_Controller.updateDietStartDate}',
                    contactId, programMembershipId, dietStartDate,
                    function(result, event){
                        if(result.success){
                            $("#dietStartDateDisplay").html(result.data);
                            $("#dietStartDateStatus").html("Success: Diet Start Date Updated");
                            $("#changeDietStartDate").hide();
                        }
                        else{
                            $("#dietStartDateError").html(result.data);
                        }
                    },
                    {escape: false}
                );
                
                $("#dietStartDate").val(dateObject.getTime());
            },
			updateOrderDate : function(dateObject){
                var orderDate = this.getFormattedDate(dateObject);
				var orderId = this.getParameterByName('o');
				var portalUser = this.getParameterByName('portalUser');
                //We cannot rely on the isCSRFlow Query String Parameter and must instead look at PortalUser
                var isCSRFlow = portalUser && portalUser != '';
                
				Visualforce.remoting.Manager.invokeAction(
					'{!$RemoteAction.HMR_CC_OrderView_Controller.updateOrderDate}',
					orderId, orderDate, isCSRFlow,
					function(result, event){
						if(result.success){
                            $("#orderDateDisplay").html(result.data);
                            if(!orderType || orderType != "P2 1st Order")
                                $("#orderDateError").html("");
                        }
                        else{
                            $("#orderDateError").html(result.data);
                        }
					},
					{escape: false}
				);
                
                $("#orderDate").val(dateObject.getTime());
			},
            getFormattedDate : function(date, format) {
                var d = new Date(date);
                if(format === "zebra")
                	d.setDate(d.getDate() + 1);
                var month = '' + (d.getUTCMonth() + 1);
                var day = '' + d.getUTCDate();
                var year = d.getUTCFullYear();
                
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                
                return format === "zebra" ? [year, month, day].join('-') : [month, day, year].join('/');
            },
            getOrderType : function() {
                var orderId = this.getParameterByName('o');
                Visualforce.remoting.Manager.invokeAction(
					'{!$RemoteAction.HMR_CC_OrderView_Controller.getOrderType}',
					orderId,
					function(result, event){
						if(result.success){
                            orderType = result.data;
                            changeOrderDate.openDateWidget();
                        }
                        else{
                            $("#orderDateError").html(result.data);
                        }
					},
					{escape: false}
				);
            },
            getParameterByName : function(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
			}
		}
    </script>
</apex:component>