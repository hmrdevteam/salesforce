/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/30/2017
 * Created By: Zach Engman
 * Last Modified Date:05/30/2017
 * Last Modified By:
 * Description: Handles Order Processing (Currently: Address Sync)
 * ****************************************************/
trigger HMR_CC_Order_Trigger on ccrz__E_Order__c (after insert, after update) {
	HMR_CC_Order_Handler orderHandler = new HMR_CC_Order_Handler(Trigger.isExecuting, Trigger.size);
	if(Trigger.isAfter){
		if(Trigger.isInsert)
			orderHandler.onAfterInsert(Trigger.new, Trigger.newMap);
		else if(Trigger.isUpdate)
			orderHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
}