/**
 * Trigger on ProgramMembership Object
 * 
 * @Date: 11/28/2016
 * @Author: Ali Pierre (HMR)
 * @JIRA: https://reside.jira.com/browse/HPRP-577
 * @Modified: 03/16/2018 - Z. Engman
*/
trigger ProgramMembership on Program_Membership__c (before insert, before update, after insert, after update){
                                                        
   if(trigger.isBefore){
       if(trigger.isInsert){
           ProgramMembershipTriggerHandler.OnBeforeInsert(trigger.new);
       }
       if(trigger.isUpdate){
           ProgramMembershipTriggerHandler.OnBeforeUpdate(trigger.new, Trigger.oldMap);
       }
   }
   else{
   	  ProgramMembershipTriggerHandler handler = new ProgramMembershipTriggerHandler(Trigger.isExecuting, Trigger.size);

   	  if(trigger.isInsert){
   	  	handler.onAfterInsert(trigger.new);
   	  }

   	  if(trigger.isUpdate){
   	  	handler.onAfterUpdate(trigger.old, trigger.new, trigger.oldMap, trigger.newMap);
   	  }
   }
}