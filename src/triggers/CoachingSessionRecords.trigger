/**
* Trigger on Class Object
* Managing Coaching Session records
* @Date: 2016-11-21
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-576
*/

trigger CoachingSessionRecords on Class__c (after insert, after update) {           
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        CoachingSessionRecordsTriggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);    
    }
}