/*****************************************************
 * Author: Zach Engman
 * Created Date: 11/07/2017
 * Created By: Zach Engman
 * Last Modified Date: 11/07/2017
 * Last Modified By:
 * Description: Trigger for Cart: Handles Default Address Assignments
 * ****************************************************/
trigger HMR_CC_Cart_Trigger on ccrz__E_Cart__c (before insert, before update) {
	HMR_CC_Cart_Handler cartHandler = new HMR_CC_Cart_Handler(Trigger.isExecuting, Trigger.size);
    if(Trigger.isBefore){
        if(Trigger.isInsert)
            cartHandler.onBeforeInsert(Trigger.new);
        else if(Trigger.isUpdate)
            cartHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
}