/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/24/2017
 * Created By: Zach Engman
 * Last Modified Date:05/24/2017
 * Last Modified By:
 * Description: Trigger for Account: Formats/Standardizes the Field Data
 * ****************************************************/
trigger HMR_Account_Trigger on Account (before insert, before update) {
	HMR_Account_Handler accountHandler = new HMR_Account_Handler(Trigger.isExecuting, Trigger.size);
    if(Trigger.isBefore){
        if(Trigger.isInsert)
            accountHandler.onBeforeInsert(Trigger.new);
        else if(Trigger.isUpdate)
            accountHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
}