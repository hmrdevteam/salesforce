/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/23/2017
 * Created By: Zach Engman
 * Last Modified Date:05/23/2017
 * Last Modified By:
 * Description: Trigger for Contact Address: Formats/Standardizes the Field Data
 * ****************************************************/
trigger HMR_CC_ContactAddress_Trigger on ccrz__E_ContactAddr__c (before insert, before update) {
	HMR_CC_ContactAddress_Handler contactAddressHandler = new HMR_CC_ContactAddress_Handler(Trigger.isExecuting, Trigger.size);
	if(Trigger.isBefore){
		if(Trigger.isInsert)
			contactAddressHandler.onBeforeInsert(Trigger.new);
		else if(Trigger.isUpdate)
			contactAddressHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
}