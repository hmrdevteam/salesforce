/**
* Trigger on Order Items Object
* If the Order Submitted has a Dynamic Kit associated with a Program Membership
*
* @Date: 2016-11-25
* @Author Pranay Mistry (Magnet 360)
* @JIRA: 
*/

trigger ProgramMembershipCreationOnOrder on ccrz__E_Order__c (before insert, after insert, before update, after update) {
	if(Trigger.isBefore) {
		if(trigger.isInsert){
			ProgramMembershipOnOrderTriggerHandler.handleOrderBeforeInsert(Trigger.new);			
		}
		if(Trigger.isUpdate){
			ProgramMembershipOnOrderTriggerHandler.handleOrderBeforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}		 	
	if(Trigger.isAfter) {
		if(Trigger.isInsert){                      
			ProgramMembershipOnOrderTriggerHandler.handleOrderInsert(Trigger.new);
		}
		if(Trigger.isUpdate){
			ProgramMembershipOnOrderTriggerHandler.handleOrderUpdate(Trigger.new, Trigger.oldMap);
		}			
	}
}