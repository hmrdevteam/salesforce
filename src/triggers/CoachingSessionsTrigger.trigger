/**
* Trigger on Coaching Sessiongs Object
* This trigger will call handler to verify if the coaching session date has a holiday conflict 
*
* @Date: 2016-12-08
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-692
*/

trigger CoachingSessionsTrigger on Coaching_Session__c (before insert, before update, after insert, after update) {
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			CoachingSessionsTriggerHandler.handleBeforeInsert(Trigger.new, 0);	
		}
		if(Trigger.isUpdate) {
			CoachingSessionsTriggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap, 1);		
		}
	}
	if(Trigger.isAfter) {
		if (Trigger.isInsert || Trigger.isUpdate)
			CoachingSessionsTriggerHandler.handleAfterEvents(Trigger.new);
	}
}