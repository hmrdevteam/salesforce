/**
* Trigger on Class Object
* Managing Address Book records
* @Date: 2017-5-11
* @Author Nathan Anderson (Magnet 360)
* Last Modified Date: 06/08/2017
 *Last Modified By: Zach Engman (Magnet 360) - Added Before Insert Handling
*/

trigger DefaultAddressTrigger on ccrz__E_AccountAddressBook__c (before insert, after insert, after update) {
    if(Trigger.isBefore && Trigger.isInsert){
        HMR_CC_AccountAddressBook_Handler handler = new HMR_CC_AccountAddressBook_Handler(Trigger.isExecuting, Trigger.size);
        handler.onBeforeInsert(Trigger.new);
    }

    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        DefaultAddressTriggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);
    }
}