/**
* Trigger on Order Items Object
* If the Order Submitted has a Dynamic Kit associated with a Program Membership
*
* @Date: 2016-11-25
* @Author Pranay Mistry (Magnet 360)
* @JIRA: 
*/

trigger ProgramMembershipCreationOnOrderItem on ccrz__E_OrderItem__c (after insert, after update) {		 	
	/*if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		ProgramMembershipOnOrderTriggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);	
	}*/
}