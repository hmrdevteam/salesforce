trigger HMR_CC_CartItem_Trigger on ccrz__E_CartItem__c (before insert, before delete) {
    HMR_CC_CartItem_Trigger_Handler handler = new HMR_CC_CartItem_Trigger_Handler(Trigger.isExecuting, Trigger.size);

    if (trigger.isBefore) {
        if(Trigger.isInsert)
            handler.onBeforeInsert(Trigger.new);
    	if(Trigger.isDelete)
        	HMR_CC_CartItem_Trigger_Handler.deleteCoupon(trigger.old);
    }
}