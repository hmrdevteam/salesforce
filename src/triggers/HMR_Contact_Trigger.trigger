/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/23/2017
 * Created By: Zach Engman
 * Last Modified Date:05/23/2017
 * Last Modified By:
 * Description: Trigger for Contact: Formats/Standardizes the Field Data
                Synchronizes changes to email
 * ****************************************************/
trigger HMR_Contact_Trigger on Contact (before insert, before update, after update) {
    HMR_Contact_Handler contactHandler = new HMR_Contact_Handler(Trigger.isExecuting, Trigger.size);
    if(Trigger.isBefore){
        if(Trigger.isInsert)
            contactHandler.onBeforeInsert(Trigger.new);
        else if(Trigger.isUpdate)
            contactHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
    else{
        if(Trigger.isUpdate)
            contactHandler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
}