/**
* Trigger on Class Object
* create Session Attendees records for Coaching Session 
* @Date: 2016-12-5
* @Author Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-189
*/

trigger CreateSessionAttendees on Class_Member__c (after insert, after update) {
	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		CreateSessionAttendeesTriggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);    
	}
}