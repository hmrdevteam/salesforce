/**
* Trigger on ccrz__E_Coupon__c Object
* 
* @Date: 2017-05-24
* @Author: Jay Zincone (HMR) 
* @JIRA: 
*/

trigger CC_CouponTrigger on ccrz__E_Coupon__c (after insert, after update) {
	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		CC_CouponTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
	}
}