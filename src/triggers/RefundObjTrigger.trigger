trigger RefundObjTrigger on Refund__c (after update) {
if(Trigger.isAfter && Trigger.isUpdate) {
		RefundObjTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);	
	}
}