/*****************************************************
 * Author: Joey Zhuang
 * Created Date: 11 May 2017
 * Created By: Joey Zhuang
 * Last Modified Date: 2017-06-05
 * Last Modified By: Zach Engman - Updated for Deletions
 * Description:
 When Stored Payment is updated
 Update all related Transaction Payment to this up to date one
 All template order and pending order.
 * ****************************************************/
trigger StoredPayment_Trigger on ccrz__E_StoredPayment__c (before delete, after update) {
	StoredPayment_TriggerHandler triggerHandler = new StoredPayment_TriggerHandler(Trigger.isExecuting, Trigger.size);

	if(Trigger.isBefore){
		if(Trigger.isDelete){
			triggerHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
		}
	}
	if(Trigger.isAfter){
		if(Trigger.isUpdate)
			triggerHandler.onAfterUpdate(Trigger.new, Trigger.NewMap, Trigger.old, Trigger.oldMap);  
	}

}