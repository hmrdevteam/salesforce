trigger HMRRecipeObjTrigger on hmr_Recipe__c (after insert, after update) {
	if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert))  {
		RecipeArticleCreationTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);	
	}
}