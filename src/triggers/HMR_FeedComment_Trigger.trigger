trigger HMR_FeedComment_Trigger on FeedComment (after insert) {
    //Create list of Ids to send to utility
    List<Id> feedCommentIds = new List<Id>();
    //Create set for parent feed item ids
    Set<Id> feedItemIds = new Set<Id>();

    //Loop through trigger.new and add records to arrays
    for(FeedComment fc : Trigger.new){
        feedItemIds.add(fc.FeedItemId);
        feedCommentIds.add(fc.Id);
    }

    //See if parent posts are Questions
    List<FeedItem> feedItems = [SELECT Id, Type FROM FeedItem WHERE Id IN : feedItemIds AND Type = 'QuestionPost'];

    //If the parent posts are Questions, call utility with list of comment Ids
    if(feedItems.size() > 0) {
        new HMR_ChatterCommentEmailUtility().questionReplyAlert(feedCommentIds);
    }
}