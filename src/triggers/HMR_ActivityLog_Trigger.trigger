/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/09/2018
 * Created By: Zach Engman
 * Last Modified Date: 03/09/2018
 * Last Modified By:
 * Description: Trigger for ActivityLog: Links to session attendee where necessary
 * ****************************************************/
trigger HMR_ActivityLog_Trigger on ActivityLog__c (before insert, before update) {
	HMR_ActivityLog_Handler activityLogHandler = new HMR_ActivityLog_Handler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            activityLogHandler.onBeforeInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
        	activityLogHandler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
    }
}