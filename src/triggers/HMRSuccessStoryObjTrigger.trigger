trigger HMRSuccessStoryObjTrigger on hmr_Success_Story__c (after insert, after update) {
    
  if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert))  {
    SuccessStoryCreationTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);  
  }

}