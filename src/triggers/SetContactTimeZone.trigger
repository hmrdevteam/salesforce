/**
* Trigger on Contact Object
* to set Time Zone for Contact and associated User
*
* @Date: 2017-3-19
* @Author Nathan Anderson  (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-2628
*/

trigger SetContactTimeZone on Contact (after insert, after update) {
    if(Trigger.isAfter) {
		if(Trigger.isInsert){
			setTimeZoneTriggerHandler.handleAfterInsert(Trigger.new);
		}
		if(Trigger.isUpdate){
            if(!system.isBatch() && !system.isFuture()) {
    			setTimeZoneTriggerHandler.handleAfterInsert(Trigger.new);
            }
            //Trigger Handler to handle updates to Account field
            ContactAccountChangeTriggerHandler.handleContactAfterUpdate(Trigger.new, Trigger.oldMap);
		}

	}
}