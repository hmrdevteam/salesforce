trigger TransactionPayment on ccrz__E_TransactionPayment__c (after insert) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            TransactionPaymentTriggerHandler.handlePaymentInsert(Trigger.new);
        }
    }
}