/**
* @Date: 3/26/2017
* @Author: Nathan Anderson (Magnet360)
* @JIRA: https://reside.jira.com/browse/HPRP-
* Modified: 3/26/2017 Nathan Anderson
*
* This test class covers CC_Ctrl_ProductCatExtension, CC_Ctrl_ProductDataExtension, & CC_MyAccount_DataExtensionController
* Uses cc_dataFActory for test data
*/


@isTest
private class CC_Ctrl_ProductCatExtension_Test {

	@isTest static void test_method_one() {
	
    	cc_dataFActory.setupTestUser();
    	cc_dataFActory.setupCatalog();
    	
    	
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
        System.runAs ( thisUser ) {
        
            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_Ctrl_ProductCatExtension.getCategoryDataForProducts(ctx);
            System.assert(rar != null);
            System.assert(rar.success);
        
                //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
                //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }
	}
	
	@isTest static void test_method_two() {
	
    	cc_dataFActory.setupTestUser();
    	cc_dataFActory.setupCatalog();
    	
    	
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
        System.runAs ( thisUser ) {
        
            final List<String> productIds = new List<String>();
            
            ccrz__E_Product__c prod = [SELECT Id, ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__ProductStatus__c = 'released' LIMIT 1];  
            
        	productIds.add(prod.Id);
        
            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_Ctrl_ProductDataExtension.getExtendedDataForProducts(ctx, productIds);
            System.assert(rar != null);
            System.assert(rar.success);
        
                //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
                //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }
	}
	
		@isTest static void test_method_three() {
	
    	cc_dataFActory.setupTestUser();
    	cc_dataFActory.setupCatalog();
    	cc_dataFActory.createOrders(2);
    	
    	
    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
        System.runAs ( thisUser ) {
        
            Contact c = [SELECT Id FROM Contact LIMIT 1];
        
            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_MyAccount_DataExtensionController.getExtendedDataForMyAccount(ctx, c.Id);
            System.assert(rar != null);
            System.assert(rar.success);
        
                //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
                //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }
	}
}