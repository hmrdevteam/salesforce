/*
Developer: Ali Pierre (HMR)
Date: 12/12/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_CCRZ_Utility
*/
@isTest(SeeAllData=true) 
private class HMR_CMS_CCRZ_Utility_Test {
	
	private static String returnValueOrEmptyString(String value) {
        if(value != null && value != '') {
            return value;
        }
        return '';
    }

	@isTest static void test_method_one() {
		String html = '';
		//Test overlayLoaderHMTML Method with and without filling colorHEXCode parameter
		HMR_CMS_CCRZ_Utility.overlayLoaderHMTML(50, 50, '#333333');
		HMR_CMS_CCRZ_Utility.overlayLoaderHMTML(50, 50, '');

    	//Get Product ID's for HMR Product Categories Start
        Map<String, Object> inputCategoryData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => 3,
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccAPICategory.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            }
        };

        Map<String, Object> outputCategoryData = ccrz.ccAPICategory.fetch(inputCategoryData);
        //Get Product ID's for HMR Product Categories End
        Set<String> product_sfidSet = HMR_CMS_CCRZ_Utility.getProduct_sfidSet_forCategory(outputCategoryData, 'HMR Products');
        //Declare and initialize Set of strings with SKUs selected for testing
		Set<String> product_SKUList = new Set<String>{'IBSN', 'ISOLMAT', 'I70C', 'IVSB'};
		//Retrieve products by SKU
		Map<String, Object> inputData = new Map<String, Object>{
		    ccrz.ccAPI.API_VERSION => 6,
	        ccrz.ccApiProduct.PRODUCTSKULIST => product_SKUList,
	        ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
			ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
			ccrz.ccAPIProduct.PARAM_BY_ASC => true,
	        ccrz.ccAPI.SIZING => new Map<String, Object>{
	            ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
	                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
	            }
	        }
		};

        Map<String, Object> outputProductData = ccrz.ccAPIProduct.fetch(inputData);
        if(outputProductData != null && outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
            List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);
            if(outputProductList.size() > 0) {
                //Price Lists Logic Start
                //Set the Product SKU's
                Set<String> productSKUSet = HMR_CMS_CCRZ_Utility.getProduct_keySet(outputProductList, 'SKU');
                //Price lists Logic Map Input setup
                Map<String, Object> inputPriceListsData = new Map<String, Object>{
                    ccrz.ccAPIPriceList.PRODUCTSKUS => productSKUSet,
                    ccrz.ccAPI.API_VERSION => 6,
                    ccrz.ccAPI.SIZING => new Map<String, Object>{
                        ccrz.ccAPIPriceList.ENTITYNAME => new Map<String, Object>{
                            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                        }
                    }
                };
                Map<String, Object> outputPriceListsData = ccrz.ccAPIPriceList.fetch(inputPriceListsData);
                Map<String, Map<String, String>> productPriceListsMap = HMR_CMS_CCRZ_Utility.getProductPriceListsMap(outputPriceListsData);
                Map<String, String> employeesPriceList = productPriceListsMap.get('employeesPriceListMap');
                Map<String, String> friendsandfamilyPriceList = productPriceListsMap.get('friendsandfamilyPriceListMap');
                Map<String, String> standardPriceList = productPriceListsMap.get('standardPriceListMap');
                //Price Lists Logic End

                //Get Product Ingredients Logic Start
                Set<String> product_sfid_Set = HMR_CMS_CCRZ_Utility.getProduct_keySet(outputProductList, 'sfid');
                Map<String, String> productIngredientsMap = HMR_CMS_CCRZ_Utility.getProductIngredients(product_sfid_Set);
                //Get Product Ingredients Logic End


                //Product Specs Logic Start
                Set<String> productSpecIDSet = HMR_CMS_CCRZ_Utility.getProductSpecIDSet(outputProductList);
                Map<String, Object> inputProductSpecData = new Map<String, Object>{
                    ccrz.ccAPISpec.SPECIDLIST => productSpecIDSet,
                    ccrz.ccAPI.API_VERSION => 3
                };
                Map<String, Object> outputProductSpectData = ccrz.ccAPISpec.fetch(inputProductSpecData);
                List<Map<String, Object>> productSpecsMainList = new List<Map<String, Object>>();
                if (outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST) != null) {
                    productSpecsMainList = (List<Map<String, Object>>) outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST);
                }
                String calorieSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Calories');
                String totalServingsSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Total Servings');
                String proteinSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Protein');
                String servingSizeSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Serving Size');
                //Product Specs Logic End
                
                //Declare and initialize Maps to store product quantity (for kits) and products' category
                 Map<String, Object> quantityMap = new Map<String, Object>{
                	'IBSN' => 8,
                	'I70C' => 4,
                	'ISOLMAT' => 1
                };

                Map<String, String> groupMap = new Map<String, String>{
                	'IBSN' => 'Entrees',
                	'I70C' => 'Shakes',
                	'IVSB' => 'Entrees',
                	'ISOLMAT' => 'Materials'
                };


                //Product HTML rendering Loop Start
                Integer tabIndex = 0;
                Map<String, List<String>> finalProductCategoryMapWithSKU = HMR_CMS_CCRZ_Utility.finalProductCategoryMap(outputCategoryData);
                //Test finalMaterialProductsWithSFid method
                Set<String> finalMaterialProductsWithSFid = HMR_CMS_CCRZ_Utility.finalMaterialProductsWithSFid(outputCategoryData);
                for(Map<String, Object> productObject: outputProductList) {
                    tabIndex++;
                    String productMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image');
                    String productThumbnailMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image Thumbnail');
                    //Spec Logic Start
                    String allergenHTML = HMR_CMS_CCRZ_Utility.getProductAllergenInfoHTML(productObject);
                    String calorieSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, calorieSpecId);
                    String totalServingsSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, totalServingsSpecId);
                    String protienSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, proteinSpecId);
                    String servingSizeSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, servingSizeSpecId);
                    //Spec Logic End
                   

                    //Set specification values to N/A is string is empty 
                    if(String.isEmpty(calorieSpecification))
                        calorieSpecification = 'N/A';

                    
                    if(String.isEmpty(totalServingsSpecification)){
                        totalServingsSpecification = 'N/A';
                    }
                    if(String.isEmpty(protienSpecification)){
                        protienSpecification = 'N/A';
                    }
                    //Get Product Nutritions Map Start
                    Map<String, String> productNutritionsMap = HMR_CMS_CCRZ_Utility.getProductNutritionsMap(productSpecsMainList, productObject);
                    //Get Product Nutritions Map End
                    String product_sfid = returnValueOrEmptyString(String.valueOf(productObject.get('sfid')));
                    String productSKU = returnValueOrEmptyString(String.valueOf(productObject.get('SKU')));
                    String productName = returnValueOrEmptyString(String.valueOf(productObject.get('sfdcName')));
                    String productDescription = returnValueOrEmptyString(String.valueOf(productObject.get('shortDesc')));
                    //String productIngredients = returnValueOrEmptyString(String.valueOf(productObject.get('longDesc')));
                    String productIngredients = returnValueOrEmptyString(String.valueOf(productIngredientsMap.get(product_sfid)));
                    String containerType = productSKU.contains('120') ? 'can' : 'box';

                    List<String> productCategoriesList = finalProductCategoryMapWithSKU.get(productSKU);
                    String prodCategory = String.join(productCategoriesList, ',');
                    //Only create modals for products with total serving Specs to filter out items like Materials
                    if(prodCategory != 'Materials' && returnValueOrEmptyString(prodCategory) != ''){
                        html += HMR_CMS_CCRZ_Utility.getProductDetails_IngredientsModalHTML(productIngredients, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
                        html += HMR_CMS_CCRZ_Utility.getProductDetails_NutritionsModalHTML(productNutritionsMap, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
                        html += HMR_CMS_CCRZ_Utility.getProductDetails_DescriptionModalHTML(productSKU, productThumbnailMediaURL, productName, productDescription, totalServingsSpecification,
                                                                                        containerType, calorieSpecification, protienSpecification, servingSizeSpecification);
                    }
                    //Test No eCommerce product card html method
                    html += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_NoeCommerce(productSKU, productMediaURL, productName, allergenHTML, calorieSpecification);
                    //Test Commerce product card html method
                    html += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_eCommerce(totalServingsSpecification, productSKU, tabIndex,
                                                                                          returnValueOrEmptyString(standardPriceList.get(productSKU)),
                                                                                          returnValueOrEmptyString(employeesPriceList.get(productSKU)),
                                                                                          returnValueOrEmptyString(friendsandfamilyPriceList.get(productSKU)), true);
                    //Test HSAH Kit product card html method - Admin user
                    html += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_Kit( productSKU,  productMediaURL,  productName,  allergenHTML,  calorieSpecification,
	 										totalServingsSpecification,  tabIndex, quantityMap,  returnValueOrEmptyString(groupMap.get(productSKU)), true);
                    //Test HSAH Kit product card html method - Client user
                    html += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_Kit( productSKU,  productMediaURL,  productName,  allergenHTML,  calorieSpecification,
	 										totalServingsSpecification,  tabIndex, quantityMap,  returnValueOrEmptyString(groupMap.get(productSKU)), false);
                    //Test Phase 2 Kit line item html method
                    html += HMR_CMS_CCRZ_Utility.phase2OrderItemHTML(product_sfid, returnValueOrEmptyString(groupMap.get(productSKU)), productName, productSKU, 
                    																	  returnValueOrEmptyString(standardPriceList.get(productSKU)),
                                                                                          returnValueOrEmptyString(employeesPriceList.get(productSKU)),
                                                                                          returnValueOrEmptyString(friendsandfamilyPriceList.get(productSKU)), tabIndex);
                    //Product List HTML rendering End
                }
                //Product HTML rendering Loop End
            }
        }
  	}	  	 	
}