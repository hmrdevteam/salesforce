/**
 * Controller for JoinAProgram Page
 *
 * @Date: 3/22/2017
 * @Author: Nathan Anderson (Magnet 360)
*/

public with sharing class JoinAProgramController {

	public String pId {get;set;}
	public String cId {get;set;}
	public Contact c {get;set;}

	public JoinAProgramController() {

		cId = ApexPages.currentPage().getParameters().get('cId');
		c = [SELECT Id, Community_User_ID__c FROM Contact WHERE Id = :cId];

	}

	public List<SelectOption> getPrograms() {

		List<SelectOption> options = new List<SelectOption>();

		//options.add(new SelectOption('', '--Select Program--'));

		List<Program__c> progs = [SELECT Id, Name FROM Program__c WHERE RemoteProgram__c = TRUE];

		for(Program__c p : progs) {
			options.add(new SelectOption(p.Id, p.Name));
		}

		return options;
	}

	public PageReference goToKitConfig() {

		Program__c selProg = new Program__c();

		try {

			selProg = [SELECT Id, Admin_Kit_Configuration_URL__c FROM Program__c WHERE Id = :pId];
		}
		catch(Exception e) {

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a valid Program: ' + e.getMessage()));
		}

		String adminKit = selProg.Admin_Kit_Configuration_URL__c;

		return new PageReference('/apex/cms__Main?name='+adminKit+'&isCSRFlow=true&portalUser='+c.Community_User_ID__c+'&store=DefaultStore');

	}

	public PageReference cancel() {
		return new PageReference('/'+c.Id);
	}
}