/*****************************************************
 * Author: Zach Engman
 * Created Date: 06/08/2017
 * Created By: Zach Engman
 * Last Modified Date: 1/22/2018 -- Added handling for both account fields being left blank (this happens from the new My Account page)
 * Last Modified By: N. Anderson
 * Description: Handler for the ccrz__E_AccountAddressBook__c object triggers
 * ****************************************************/
public with sharing class HMR_CC_AccountAddressBook_Handler {
	private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;


	public HMR_CC_AccountAddressBook_Handler(boolean executing, integer size) {
		isExecuting = executing;
        batchSize = size;
	}


    public void onBeforeInsert(List<ccrz__E_AccountAddressBook__c> accountAddressBookList){

    	//Keep the Account lookup and id fields in sync, sometimes one or the other is being left null.
    	for(ccrz__E_AccountAddressBook__c addressBookRecord : accountAddressBookList){
    		if(!String.isBlank(addressBookRecord.ccrz__Account__c) && String.isBlank(addressBookRecord.ccrz__AccountId__c)){
				addressBookRecord.ccrz__AccountId__c = addressBookRecord.ccrz__Account__c;
			} else if(!String.isBlank(addressBookRecord.ccrz__AccountId__c) && String.isBlank(addressBookRecord.ccrz__Account__c)) {
				addressBookRecord.ccrz__Account__c = addressBookRecord.ccrz__AccountId__c;
			}
    	}

		//Keep the Owner lookup and id fields in sync, sometimes one or the other is being left null.
		for(ccrz__E_AccountAddressBook__c addressBookRecord : accountAddressBookList){
    		if(!String.isBlank(addressBookRecord.ccrz__Owner__c) && String.isBlank(addressBookRecord.OwnerId))
    			addressBookRecord.OwnerId = addressBookRecord.ccrz__Owner__c;
    		else if(!String.isBlank(addressBookRecord.OwnerId) && String.isBlank(addressBookRecord.ccrz__Owner__c))
    			addressBookRecord.ccrz__Owner__c = addressBookRecord.OwnerId;
    	}

		//Keep the Account fields populated if left blank
		Set<Id> ownerIds = new Set<Id>();
		List<User> users = new List<User>();

		//get the owner Ids
		for(ccrz__E_AccountAddressBook__c addressBookRecord: accountAddressBookList) {
			ownerIds.add(addressBookRecord.OwnerId);
        }
		//query for the user records so we can use the account id
		users = [SELECT Id, AccountId FROM User WHERE Id IN :ownerIds];

		//if the account fields are blank, get the matching user record and set the account fields
		for(ccrz__E_AccountAddressBook__c addressBookRecord : accountAddressBookList){
			for(User u : users) {
	    		if(!String.isBlank(addressBookRecord.ccrz__Owner__c) && String.isBlank(addressBookRecord.ccrz__Account__c) && String.isBlank(addressBookRecord.ccrz__AccountId__c)){
					if(u.Id == addressBookRecord.ccrz__Owner__c){
						addressBookRecord.ccrz__Account__c = u.AccountId;
						addressBookRecord.ccrz__AccountId__c = u.AccountId;
					}
				}
			}
    	}
   	}
}