/*
Developer: Ali Pierre (HMR)
Date: 3/16/2018
Modified: 
Modified Date : 
Target Class: HMR_CMS_Land_2Col_ImgLeft_ContentTemp
*/
@isTest
private class HMR_CMS_Land_2Col_ImgLeft_Content_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_Land_2Col_ImgLeft_ContentTemp landing2ColImgConfigTemplate = new HMR_CMS_Land_2Col_ImgLeft_ContentTemp();
	    String propertyValue = landing2ColImgConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String LeftImageURL = landing2ColImgConfigTemplate.LeftImageURL;                
        string LeftImageClass = landing2ColImgConfigTemplate.LeftImageClass;
        String RightTitle = landing2ColImgConfigTemplate.RightTitle;                
        String RightDescription = landing2ColImgConfigTemplate.RightDescription;
        String LinkOrButton = landing2ColImgConfigTemplate.LinkOrButton;                
        String LinkOrButtonText = landing2ColImgConfigTemplate.LinkOrButtonText;
        String LinkOrButtonTargetUrl = landing2ColImgConfigTemplate.LinkOrButtonTargetUrl;                
        String sectionGTMIDAttr = landing2ColImgConfigTemplate.sectionGTMIDAttr;
        Boolean showBulletedList = landing2ColImgConfigTemplate.showBulletedList;

	    landing2ColImgConfigTemplate.testAttributes = new Map<String, String>  {
            'LeftImageURL' => 'testurl',
            'LeftImageClass' => 'Block',
            'RightTitle' => 'test Title',
            'RightDescription' => 'test desc',
            'showBulletedList' => 'true',
            'LinkOrButton' => 'Link',
            'LinkOrButtonText' => 'test label',
            'LinkOrButtonTargetUrl' => 'testtarget',
            'sectionGTMIDAttr' => 'testGTM'
        };

	    string s1 = landing2ColImgConfigTemplate.getPropertyWithDefault('LeftImageURL','testurl');
	    system.assertEquals(s1, 'testurl');

	    String renderHTML = landing2ColImgConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_Land_2Col_ImgLeft_ContentTemp kc = new HMR_CMS_Land_2Col_ImgLeft_ContentTemp(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_Land_2Col_ImgLeft_ContentTemp(null) ' + e);
	    }
  	}

	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_Land_2Col_ImgLeft_ContentTemp landing2ColImgConfigTemplate = new HMR_CMS_Land_2Col_ImgLeft_ContentTemp();
	    String propertyValue = landing2ColImgConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String LeftImageURL = landing2ColImgConfigTemplate.LeftImageURL;                
        string LeftImageClass = landing2ColImgConfigTemplate.LeftImageClass;
        String RightTitle = landing2ColImgConfigTemplate.RightTitle;                
        String RightDescription = landing2ColImgConfigTemplate.RightDescription;
        String LinkOrButton = landing2ColImgConfigTemplate.LinkOrButton;                
        String LinkOrButtonText = landing2ColImgConfigTemplate.LinkOrButtonText;
        String LinkOrButtonTargetUrl = landing2ColImgConfigTemplate.LinkOrButtonTargetUrl;                
        String sectionGTMIDAttr = landing2ColImgConfigTemplate.sectionGTMIDAttr;
        Boolean showBulletedList = landing2ColImgConfigTemplate.showBulletedList;

	    landing2ColImgConfigTemplate.testAttributes = new Map<String, String>  {
            'LeftImageURL' => 'testurl',
            'LeftImageClass' => 'Block',
            'RightTitle' => 'test Title',
            'RightDescription' => 'test desc',
            'showBulletedList' => 'true',
            'LinkOrButton' => 'Button',
            'LinkOrButtonText' => 'test label',
            'LinkOrButtonTargetUrl' => 'testtarget',
            'sectionGTMIDAttr' => 'testGTM'
        };

	    string s1 = landing2ColImgConfigTemplate.getPropertyWithDefault('LeftImageURL','testurl');
	    system.assertEquals(s1, 'testurl');

	    String renderHTML = landing2ColImgConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_Land_2Col_ImgLeft_ContentTemp kc = new HMR_CMS_Land_2Col_ImgLeft_ContentTemp(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_Land_2Col_ImgLeft_ContentTemp(null) ' + e);
	    }
  	}
}