/**
* CloudCraze Shipping and Handling API Extension
*
* @Date: 2017-03-24
* @Author Pranay Mistry (Magnet 360)
* @Modified by:Aslesha Kokate(Mindtree)
* @Modified for: added mannual discount to cart
* @JIRA:
*/
global class HMR_CC_PriceAdjustment_Extn  extends ccrz.cc_api_PriceAdjustment{
    global override Map<String, Object> computePricingCart(Map<String, Object> inputData) {
        //Account Id record type is not equal to Corporate Account
        ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary) inputData.get(ccrz.cc_api_PriceAdjustment.PARAM_CARTBEAN);
        //Boolean pricingSuccess = false;
        //Boolean containsKit = false;
        ////Verify that this is an authenticated user
        //String userId = UserInfo.getUserId();
        //String userProfileId = UserInfo.getProfileId();
        //Id authUserProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User'].Id;
        //List<Program_Membership__c> progMembership = new List<Program_Membership__c>();
        ////if authenticated user, search for existing program membership( would be Interim order)
        ////
        //if(userProfileId == authUserProfileId){
        //    Id contactId = [SELECT Id, ContactId FROM User WHERE Id = :userId].ContactId;
        //     progMembership = [SELECT Id, Name, hmr_Contact__c  FROM Program_Membership__c      
        //        WHERE hmr_Status__c = 'Active' AND hmr_Contact__c = :contactId];
        //}                  
        

        //Grab the Account based on the currently logged in user.
        //List<Account> corporateAccountList = [SELECT Id, Name, Standard_HSAH_Kit_Discount__c, Standard_HSS_Kit_Discount__c, hmr_HSAH_Lactose_Free_Kit_Price__c,
        //                            hmr_HSAH_Standard_Kit_Price__c, hmr_HSS_Lactose_Free_Kit_Price__c, hmr_HSS_Standard_Kit_Price__c, RecordType.DeveloperName FROM Account WHERE Id = :ccrz.cc_CallContext.currAccount.Id];
        
                                  
        
        //if(corporateAccountList.size() <= 0 || (corporateAccountList[0].RecordType.DeveloperName != 'Corporate_Account' && corporateAccountList[0].Name != 'HMR Employees')) {

        //    Set<Id> dynamicKitIds = new Set<Id>();
        //    List<Program__c> programList;
        //    for (ccrz.cc_bean_CartItem cartItem: cartData.cartItems) {
        //        if(cartItem.mockProduct.ProductType == 'Dynamic Kit') {
        //            //containsKit = true;
        //            dynamicKitIds.add(cartItem.mockProduct.Id);

        //        }
        //    }
        //    programList = new List<Program__c>([SELECT Id, Name, Default_Promotional_Discount__c, Standard_Kit__c FROM Program__c WHERE Standard_Kit__c In :dynamicKitIds]);
        //    for (ccrz.cc_bean_CartItem cartItem: cartData.cartItems) {
        //        for(Program__c program: programList) {
        //            if(cartItem.mockProduct.Id == program.Standard_Kit__c && program.Default_Promotional_Discount__c != null) {
        //                cartItem.adjustment = -1 * program.Default_Promotional_Discount__c;
        //                //cartData.adjustment = -1 * program.Default_Promotional_Discount__c;
        //            }
        //        }
        //    }
        //    pricingSuccess = true;
        //}
        ////If this is an Interim order with  no coupons applied, apply standard 10% interim order discount
        //if(!containsKit && cartData.hasCoupon == false && !progMembership.isEmpty()){
        //    Decimal totalDiscount = 0.00;
        //    for (ccrz.cc_bean_CartItem cartItem: cartData.cartItems) {                
        //        if(cartItem.mockProduct.ProductType != 'Dynamic Kit') {
        //            decimal lineTotal = cartItem.Price * cartItem.Quantity;
        //            decimal lineDiscount = lineTotal * .10;
        //            lineDiscount = lineDiscount.setScale(2, RoundingMode.HALF_UP);
                
        //            //cartItem.AbsoluteDiscount = lineDiscount;
        //            //cartItem.SubAmount = lineTotal - lineDiscount;
        //            //cartItem.PercentDiscount = .10 * 100;
        //            totalDiscount += lineDiscount;
        //        }
        //        cartData.adjustment = - 1 * totalDiscount;
        //    }
        //}

        // manual discount logic starts here

        List<ccrz__E_Cart__c> cartList = [SELECT Id, ccrz__AdjustmentAmount__c, ccrz__AdjustmentReason__c, ccrz__SubtotalAmount__c,ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE  ccrz__EncryptedId__c=:cartData.encryptedId];
        
        if(cartList.size() > 0){
            if(cartList[0].ccrz__AdjustmentAmount__c != null)
                cartData.adjustment= cartList[0].ccrz__AdjustmentAmount__c;
        }
        inputData.put(ccrz.cc_api_CartExtension.PARAM_CARTBEAN,cartData);

        return inputData;
    }
}