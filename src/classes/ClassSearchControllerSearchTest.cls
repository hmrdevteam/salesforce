/* Unit Test for ClassSearchController
*
* Objects referenced -->Profile, User,Account,Contact, Coach__c,  Class__c, Class_Member__c,Program__c, Program_Membership__c,Coaching_Session__c
*
* @Date: 11/21/2016
* @Author Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/


@isTest
private Class ClassSearchControllerSearchTest{

    static testMethod void firstTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stan321', Email='standuser321@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;


        Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;


        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;

        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;


        Program_Membership__c programMemObj = new Program_Membership__c(Program__c = programObj.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj.id);
        insert programMemObj;



        Test.setCurrentPageReference(new PageReference('Page.ClassSearch'));
        System.currentPageReference().getParameters().put('id', testContatObj.id);



        List<Coaching_Session__c> searchedClassListTest = new List<Coaching_Session__c>();
        searchedClassListTest .add(coachingSessObj);

        Set<Id> classWithMembershipSetTest = new Set<Id>();
        classWithMembershipSetTest.add(classObj.id);

        ClassSearchController controllerObj = new ClassSearchController();



        controllerObj.searchedClassList = searchedClassListTest;
        controllerObj.membership = programMemObj;
        controllerObj.contactRecord = testContatObj;
        controllerObj.classObject = classObj;
        controllerObj.moreRecordLink = TRUE;
        controllerObj.classDate = Date.today();
        controllerObj.classInputDate = Date.today();
        controllerObj.recordsToShow = 3;
        controllerObj.numberOfRecords = 3;
        // by Utkarsh
        controllerObj.selectedClassFirstClassDate = '2016-11-11' ;
        controllerObj.selectedClass = classObj.id;
        controllerObj.success = TRUE;
        controllerObj.success = FALSE;
        controllerObj.noRecords = FALSE;
        controllerObj.hideError = FALSE;
        controllerObj.noRecordMessage = 'No Records';
        controllerObj.classWithMembershipSet = classWithMembershipSetTest;
        controllerObj.searchResult();
        controllerObj.assignClass();
        controllerObj.previous();
        controllerObj.next();
        controllerObj.getprev();
        controllerObj.getnxt();
        controllerObj.clearSearchList();
        controllerObj.loadMoreResults();
        controllerObj.selectClassId();
        controllerObj.cancel();

        List<Class__c> searchClass = [select id from Class__c where hmr_Coach__c = :coachObj.id AND Program__c = :programObj.id ];
        System.assertEquals(1, searchClass.size());

        Test.stopTest();

    }
    static testMethod void secondTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stan3212', Email='standusers321@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing3212', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanusesr@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        ClassSearchController controllerObj = new ClassSearchController();

        List<Contact> contactList = [select id from Contact where LastName = 'test contact'];
        System.assertEquals(1, contactList.size());

        Test.stopTest();

    }

    static testMethod void thirdTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanUs2', Email='standusers3221@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing3212', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanusesr2@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Test.setCurrentPageReference(new PageReference('Page.ClassSearch'));
        System.currentPageReference().getParameters().put('id', '13534');

        ClassSearchController controllerObj = new ClassSearchController();

        List<Contact> contactList = [select id from Contact where LastName = 'test contact'];
        System.assertEquals(1, contactList.size());

        Test.stopTest();

    }

    static testMethod void fourthTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanUs1', Email='standusers3227@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing3217', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanusesr7@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Test.setCurrentPageReference(new PageReference('Page.ClassSearch'));
        System.currentPageReference().getParameters().put('id', '13534');

        String idFromUrl = apexpages.currentpage().getparameters().get('id');

        ClassSearchController controllerObj = new ClassSearchController();
        controllerObj.assignClass();
        System.assertEquals('13534', idFromUrl);

        Test.stopTest();

    }

    static testMethod void fifthTestMethod(){


        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanU21', Email='standuser3221@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='TestingU321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser212@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;


        Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 14, hmr_Has_Phone_Coaching__c = true);
        insert programObj;


        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;

        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;


        Program_Membership__c programMemObj = new Program_Membership__c(Program__c = programObj.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj.id);
        insert programMemObj;

        Class_Member__c classMemObj = new Class_Member__c(hmr_Contact_Name__c = testContatObj.id, hmr_Class__c = classObj.id);
        insert classMemObj;


        Test.setCurrentPageReference(new PageReference('Page.ClassSearch'));
        System.currentPageReference().getParameters().put('id', testContatObj.id);



        List<Coaching_Session__c> searchedClassListTest = new List<Coaching_Session__c>();
        searchedClassListTest .add(coachingSessObj);

        Set<Id> classWithMembershipSetTest = new Set<Id>();
        classWithMembershipSetTest.add(classObj.id);

        ClassSearchController controllerObj = new ClassSearchController();
        controllerObj.selectedClass = classObj.id;
        String cDate = String.valueOf(classObj.hmr_First_Class_Date__c);
        controllerObj.selectedClassFirstClassDate = cDate;
        controllerObj.classInputDate = System.Today();
        controllerObj.classDate = classObj.hmr_First_Class_Date__c;
        controllerObj.assignClass();

        System.assertEquals(1, classWithMembershipSetTest.size());

        Test.stopTest();
    }
}