@isTest(SeeAllData=false)
global class HMRPushNotificationJobTest {
    
    private static void setupData(Date startDate, Integer numbOfRecs) {
        List<Contact> cList = new List<Contact>();
        for(Integer i = 0; i < numbOfRecs; i++) {
            String emailAddress = String.valueOf(System.now().getTime() + String.valueOf(i) + '@cc-test.mail');            
            Contact c = new Contact(
                FirstName = 'bulk',
                LastName  = 'pushNotification',
                Email = emailAddress,
                MailingStreet = 'abc'
            );
            c.hmr_Diet_Tracker_Token__c = JSON.serialize( HMRDeviceTokenDTO.getTestTokens() );           
            cList.add(c);
        }
        insert cList;
        createPMRecs(cList, startDate);
    }
    
    public static void createPMRecs(List<Contact> cList, Date startDate) {        
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', 
                                                  Days_in_1st_Order_Cycle__c = 30,
												  hmr_Program_Type__c = 'Healthy Solutions',
                                                  RemoteProgram__c = true);
        insert programRecord;                
        Date enrollmentDate = startDate.addDays(10);        
        List<Program_Membership__c> pmList = new List<Program_Membership__c>();
        for(Contact c: cList) {
            Program_Membership__c pmr = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                  hmr_Active__c = true,
                                                                  hmr_Status__c = 'Active',
                                                                  hmr_Contact__c = c.Id,
                                                                  hmr_Diet_Start_Date__c = startDate,
                                                                  hmr_Enrollment_Date__c = enrollmentDate);
            pmList.add(pmr);
        }
        insert pmList;
    }
    
    /**
     * not used because we need to use the system SystemModstamp field and it can not be modified
     */
    public static void createDietAndPhysicalActivityRecs(List<Program_Membership__c> pmList) {
        List<ActivityLog__c> alList = new List<ActivityLog__c>();
        for(Program_Membership__c pm: pmList) {
	        ActivityLog__c al = new ActivityLog__c(Contact__c  = pm.hmr_Contact__c,
                                                   Program_Membership__c = pm.Id,
                                                   Water__c = 5);
        }
    }
    
    global class PushNotificationResponse implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"multicast_id":7104568048158642000,"success":2,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1520356540163747%4741163a4741163a"}]}');
            res.setStatusCode(200);
            return res;            
        }
    }
     
    @isTest
    public static void testWeighInReminder() {
        System.debug('===> start testWeighInReminder');
        Date startDate = Date.today();
        startDate.addDays(-5);
        setupData(startDate, 5);
        
        Test.setMock(HttpCalloutMock.class, new PushNotificationResponse());
        Test.startTest();
        
        HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.WEIGHIN_REMINDER);
        DataBase.executeBatch(job, 5); 
        
        Test.stopTest();        
        System.debug('===> end testWeighInReminder');
    }        
    
        /*
    @isTest
    public static void testDayBeforeReminder() { 
        Date startDate = Date.today() + 1;
        setupData(startDate, 5);
        
        Test.setMock(HttpCalloutMock.class, new PushNotificationResponse());
        Test.startTest();
        
        HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.DAY_BEFORE_START_DATE_REMIDER);
        DataBase.executeBatch(job, 5); 
        
        Test.stopTest();        
    }
    
 
    @isTest
    public static void firstDayOfDietReminder() { 
        Date startDate = Date.today();
        setupData(startDate, 10);
        
        Test.setMock(HttpCalloutMock.class, new PushNotificationResponse());
        Test.startTest();
        
        HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.FIRST_DAY_OF_DIET_REMINDER);
        DataBase.executeBatch(job, 10); 
        
        Test.stopTest();        
    }  
*/

}