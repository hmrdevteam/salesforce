/**
* Apex Selector Class for CC_Order
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2017-04-25
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-06-19
* @JIRA:
*/
public class HMR_CC_Order_Selector {
    public static ccrz__E_Order__c getById(string id){
        List<ccrz__E_Order__c> ccOrderList =
            [
                SELECT ccrz__ShipAmount__c, ccrz__ShipMethod__c, hmr_Order_Type_c__c, ccrz__TotalDiscount__c, ccrz__TotalAmount__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c,
                (
                    SELECT ccrz__Category__c
                          ,ccrz__Product__c
                          ,ccrz__ProductType__c
                          ,ccrz__Price__c
                          ,ccrz__Quantity__c
                          ,ccrz__AbsoluteDiscount__c
                          ,ccrz__PercentDiscount__c
                          ,ccrz__SubAmount__c
                          ,ccrz__OriginalQuantity__c
                          ,ccrz__RequestDate__c
                          ,ccrz__OrderLineType__c
                          ,ccrz__StoreId__c
                          ,ccrz__UnitOfMeasure__c 
                    FROM ccrz__E_OrderItems__r 
                )
                FROM ccrz__E_Order__c
                WHERE Id =: id
            ];

        return ccOrderList.size() > 0 ? ccOrderList[0] : null;
    }

    public static ccrz__E_Order__c getByEncryptedId(string encryptedId){
        List<ccrz__E_Order__c> ccOrderList =
            [
                SELECT ccrz__ShipAmount__c,
                       ccrz__TotalDiscount__c,
                       ccrz__SubtotalAmount__c,
                       ccrz__TotalAmount__c,
                       ccrz__Account__r.Name,
                       Order_Value_Discount_Amount__c,
                       Unadjusted_Subtotal__c,
                       ccrz__ShipMethod__c,
                       hmr_Order_Type_c__c,
                       CreatedDate,
                       Original_Order_Date__c,
                       ccrz__OrderDate__c
                FROM ccrz__E_Order__c
                WHERE ccrz__EncryptedId__c =: encryptedId
            ];

        return ccOrderList.size() > 0 ? ccOrderList[0] : null;
    }

    public static ccrz__E_Order__c getPendingByContactId(string contactId){
        List<ccrz__E_Order__c> ccOrderList =
            [
                SELECT ccrz__OrderDate__c,
                       ccrz__ShipAmount__c,
                       ccrz__TotalDiscount__c,
                       ccrz__TotalAmount__c,
                       ccrz__Account__r.Name,
                       Order_Value_Discount_Amount__c,
                       ccrz__ShipMethod__c,
                       hmr_Order_Type_c__c
                FROM ccrz__E_Order__c
                WHERE ccrz__Contact__c =: contactId
                    AND ccrz__OrderStatus__c = 'Pending'
            ];

        return ccOrderList.size() > 0 ? ccOrderList[0] : null;
    }

    public static List<ccrz__E_Order__c> getByProgramMembershipId(string programMembershipId){
        List<ccrz__E_Order__c> ccOrderList = [SELECT ccrz__OrderStatus__c,
                                                     hmr_Order_Type_c__c
                                              FROM ccrz__E_Order__c
                                              WHERE hmr_Program_Membership__c =: programMembershipId];

        return ccOrderList;
    }

    public static List<ccrz__E_Order__c> getPendingAndCompletedByContactIdAndProgramMembershipId(string contactId, string programMembershipId){
        List<ccrz__E_Order__c> ccOrderList = [SELECT ccrz__OrderStatus__c,
                                                     hmr_Order_Type_c__c
                                              FROM ccrz__E_Order__c
                                              WHERE ccrz__Contact__c =: contactId
                                                AND hmr_Program_Membership__c =: programMembershipId
                                                AND (ccrz__OrderStatus__c = 'Pending' OR ccrz__OrderStatus__c = 'Completed')];

        return ccOrderList;
    }

    public static ccrz__E_Order__c getTemplateByContactIdAndProgramMembershipId(string contactId, string programMembershipId){
        List<ccrz__E_Order__c> ccOrderTemplateList =
            [
                SELECT ccrz__OrderDate__c,
                       ccrz__ShipAmount__c,
                       ccrz__TotalDiscount__c,
                       ccrz__TotalAmount__c,
                       ccrz__Account__r.Name,
                       Order_Value_Discount_Amount__c,
                       hmr_Order_Type_c__c
                FROM ccrz__E_Order__c
                WHERE ccrz__Contact__c = : contactId
                    AND hmr_Program_Membership__c = : programMembershipId
                    AND ccrz__OrderStatus__c = 'Template'
            ];

        return ccOrderTemplateList.size() > 0 ? ccOrderTemplateList[0] : null;
    }
}