/**
* Apex Rest Service Class for Tutorial Related Operations for the Mobile App
* @Date: 2018-01-24
* @Author: Magnet 360 - Zach Engman
* @Modified: 2018-01-24
* @JIRA:
*/
@RestResource(urlMapping='/mobile/user/tutorial/*')
global with sharing class HMRMobTutorial {
	/**
     * update the the tutorial flag on the contact record
     * @param hasCompletedTutorial - boolean flag
     */
    @HttpPatch
    global static String updateTutorial(Boolean hasCompletedTutorial) {
		List<Contact> contactRecordList = [SELECT MobileTutorialComplete__c FROM Contact WHERE Community_User_ID__c =: UserInfo.getUserId()];
        
        if(contactRecordList.size() > 0){
            contactRecordList[0].MobileTutorialComplete__c = hasCompletedTutorial;
            update contactRecordList[0];

            RestContext.response.statuscode = 204;
            return 'no content';
        }
        else {
            RestContext.response.statuscode = 404;
            return 'unable to update flag';
        }
   	}       
}