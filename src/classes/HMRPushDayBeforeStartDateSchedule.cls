/**
 * convenience class to allow scheduling notifications using "Schedule Apex"
 */
global class HMRPushDayBeforeStartDateSchedule implements Schedulable{
    
    global void execute(SchedulableContext sc) {
        try {
            HMRPushNotificationJob bj = new HMRPushNotificationJob(HMRNotificationType.DAY_BEFORE_START_DATE_REMIDER);
            database.executebatch(bj, 200);
        }
        catch(Exception ex) {
            System.debug('The HMRPushDayBeforeStartDateSchedule failed ' + ex.getMessage());
        }       
    } 
}