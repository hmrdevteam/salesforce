/**
* Apex Content Template Controller for Consent Agreement
*
* @Date: 2017-04-04
* @Author Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/
global virtual with sharing class HMR_CMS_ConsentAgr_ContentTemplate extends cms.ContentTemplateController{
    //need two constructors, 1 to initialize CreateContentController a
    global HMR_CMS_ConsentAgr_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //no ARG constructor
    global HMR_CMS_ConsentAgr_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /*public List<ccrz__E_Term__c> getConsentData(){
        List<ccrz__E_Term__c> consentData =
            [SELECT Id,ccrz__Description__c FROM ccrz__E_Term__c WHERE Consent_Type__c = :consentType AND ccrz__Enabled__c = TRUE LIMIT 1];
        return consentData;
    }*/

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
    */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String consentType{
        get{
            return getAttribute('consentType');
        }
    }

    public String renderType{
        get{
            return getAttribute('renderType');
        }
    }

    public String showOnScreen{
        get{
            return getAttribute('showOnScreen');
        }
    }

    public String dataId;

    global virtual override String getHTML() {
        String html = '';
        List<ccrz__E_Term__c> consentDataOfSelectedConsent = new List<ccrz__E_Term__c>();
        //Check for consentype Health
        if(consentType == 'Health') {
            //get the logged in user
            User loggedInUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            //if guest then contact Id will always be null
            //if logged in users
            if(loggedInUser.ContactId != null) {
                Contact conUser = [SELECT Id, Account.RecordType.DeveloperName, AccountId FROM Contact
                                        WHERE Id = :loggedInUser.ContactId];
                //if logged in user is associated with a corporate account
                if(conUser.Account.RecordType.DeveloperName == 'Corporate_Account') {
                    consentDataOfSelectedConsent =
                        [SELECT ccrz__Description__c, ccrz__Title__c, Consent_Type__c,
                                 Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE
                                 Consent_Type__c = 'Health' AND Employer_Account__c = :conUser.AccountId
                                 AND ccrz__Enabled__c = TRUE LIMIT 1];
                }
                else {//any other associated account for e.g. HMR PortalAccount
                    consentDataOfSelectedConsent =
                        [SELECT ccrz__Description__c, ccrz__Title__c, Consent_Type__c,
                                 Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE
                                 Consent_Type__c = 'Health' AND Employer_Account__c = null
                                 AND ccrz__Enabled__c = TRUE LIMIT 1];
                }
            }
            else { //else for guest users
                consentDataOfSelectedConsent =
                    [SELECT ccrz__Description__c, ccrz__Title__c, Consent_Type__c,
                             Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE
                             Consent_Type__c = 'Health' AND Employer_Account__c = null
                             AND ccrz__Enabled__c = TRUE LIMIT 1];
            }
        }
        else {//else for everything but health
            consentDataOfSelectedConsent =
                [SELECT Id,ccrz__Description__c,ccrz__Title__c FROM ccrz__E_Term__c WHERE Consent_Type__c = :consentType AND ccrz__Enabled__c = TRUE LIMIT 1];
        }
        dataId = consentType.replaceAll('[-+.^:,&$\\//\\s]','').toLowerCase();
        if(!consentDataOfSelectedConsent.isEmpty()){
            if(renderType == 'Modal' && showOnScreen == 'show'){
                html = createConsentTypeMarkupForPopup(consentDataOfSelectedConsent[0].ccrz__Description__c, consentDataOfSelectedConsent[0].Id, consentDataOfSelectedConsent[0].ccrz__Title__c);
            }
            if(renderType == 'Content' && showOnScreen == 'show'){
                html = createConsentTypeMarkupContent(consentDataOfSelectedConsent[0].ccrz__Description__c,consentDataOfSelectedConsent[0].ccrz__Title__c);
            }
        }

        return html;
    }

    public String createConsentTypeMarkupForPopup(String description, String termId, String consentTitle){
        String html = '';
        html += '<p class="links-group"><span class="hidden cctrem_container" data-toggle="modal" data-backdrop="static" data-target="#' + dataId + 'popup" id="'+ dataId + '" data-id="'+ termId + '">'+ consentTitle +'</span></p>';
        html += '<div id="'+ dataId + 'popup" class="hmr-modal modal fade" role="dialog">';
            html += '<div class="modal-dialog">';
                html += '<div class="modal-content">';
                    html += '<div class="modal-header">';
                        html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                            html += '<span aria-hidden="true">&times;</span>';
                        html += '</button>';
                        html += '<h4 class="modal-title text-center">'+ consentTitle +'</h4>'; //'+ title +'
                    html += '</div>';
                    html += '<div class="modal-body">';
                        html += '<p>';
                            html += description;
                        html += '</p>';
                    html += '</div>';
                    html += '<div class="modal-footer">';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        return html;
    }

    public String createConsentTypeMarkupContent(String description,String consentTitle){
        String html = '';
        html += '<div class="">';
            html += '<div class="footer-pages">';
                html += '<h1 class="text-3">'+ consentTitle +'</h1>';  // '+ title +'
                html += '<div class="summary text-10">';
                    html += '<p>' + description + '<br /></p>';
                html += '</div>';
            html += '</div>';
        html += '</div> ';
        return html;
    }
}