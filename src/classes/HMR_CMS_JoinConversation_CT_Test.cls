/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_JoinConversation_ContentTemplate
*/

@isTest //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_JoinConversation_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_JoinConversation_ContentTemplate joinConTemplate = new HMR_CMS_JoinConversation_ContentTemplate();
    String propertyValue = joinConTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    joinConTemplate.testAttributes = tab;

    string s2 = joinConTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = joinConTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');


    String renderHTML = joinConTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    try{
        HMR_CMS_JoinConversation_ContentTemplate qc = new HMR_CMS_JoinConversation_ContentTemplate(null);
    }catch(Exception e){}
  }
  
}