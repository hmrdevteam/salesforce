/**
 * Trigger Handler class for CC_Coupon trigger
 * @Date: 2017-05-24
 * @Author: Jay Zincone (HMR)
 * @JIRA:
*/

public class CC_CouponTriggerHandler {

	public static void handleAfterUpdate(List<ccrz__E_Coupon__c> newUpdatedCoupons, Map<Id, ccrz__E_Coupon__c> oldCouponVersionsMap){
        // handler logic
        //Loop through trigger collection
        for(ccrz__E_Coupon__c coupon : newUpdatedCoupons) {
        	if(oldCouponVersionsMap != null && !oldCouponVersionsMap.isEmpty()) {
				ccrz__E_Coupon__c getOldCouponVersionsMap = oldCouponVersionsMap.get(coupon.Id);

				//check to see if discountAmount field has been changed
				if(coupon.ccrz__DiscountAmount__c != getOldCouponVersionsMap.ccrz__DiscountAmount__c || coupon.ccrz__CouponType__c != getOldCouponVersionsMap.ccrz__CouponType__c) {
					if(coupon.HMR_Coupon__c != NULL){
						HMR_Coupon__c HMRc = [SELECT Id, Default_Consumer_Coupon__c, hmr_Active__c, Account__c FROM HMR_Coupon__c WHERE Id = :coupon.HMR_Coupon__c];
				        if(HMRc.Default_Consumer_Coupon__c == True && coupon.ccrz__Enabled__c == True && HMRc.hmr_Active__c == True){
			        		if(HMRc.Account__c == NULL){
			        			//No Account so update the program record
			        			List<Program__c> prgrms = new List<Program__c>();
			        			if(coupon.hmr_Program__c != NULL){
				        			Program__c prgrm = [Select Id, Default_Promotional_Discount__c, hmr_Standard_Kit_Price__c FROM Program__c WHERE Id = :coupon.hmr_Program__c];
				        			if(coupon.ccrz__CouponType__c == 'Absolute'){
										prgrm.Default_Promotional_Discount__c = coupon.ccrz__DiscountAmount__c;
									}else if(coupon.ccrz__CouponType__c == 'Percentage'){
										prgrm.Default_Promotional_Discount__c = (prgrm.hmr_Standard_Kit_Price__c * coupon.ccrz__DiscountAmount__c)/100;
									}

									prgrms.add(prgrm);
								}
								try {
						            //check for order items list to update for empty and then update
						            if(!prgrms.isEmpty()){
						                update prgrms;
						            }
						        }
						        catch (Exception ex) {
						            System.debug('Exception in CC_CouponTriggerHandler handleAfterUpdate Program ' + ex.getMessage());
						        }
			        		}else{
			        			//Account so update the account record
			        			List<Account> accts = new List<Account>();
			    				Account acct = [Select Id, Standard_HSS_Kit_Discount__c, Standard_HSAH_Kit_Discount__c, hmr_HSAH_Standard_Kit_Price__c, hmr_HSS_Standard_Kit_Price__c FROM Account WHERE Id = :HMRc.Account__c];

								if(coupon.ccrz__CouponType__c == 'Absolute'){
				    				if(coupon.Coupon_Order_Type__c == 'HSS 1st Order'){
			    						acct.Standard_HSS_Kit_Discount__c = coupon.ccrz__DiscountAmount__c;
			    					}else{
			    						acct.Standard_HSAH_Kit_Discount__c = coupon.ccrz__DiscountAmount__c;
			    					}
								}else if(coupon.ccrz__CouponType__c == 'Percentage'){
									if(coupon.Coupon_Order_Type__c == 'HSS 1st Order'){
										if(acct.hmr_HSS_Standard_Kit_Price__c!= NULL){
											acct.Standard_HSS_Kit_Discount__c = (acct.hmr_HSS_Standard_Kit_Price__c * coupon.ccrz__DiscountAmount__c)/100;
										}else{
											acct.Standard_HSS_Kit_Discount__c = (0 * coupon.ccrz__DiscountAmount__c)/100;
										}
									}else{
										if(acct.hmr_HSAH_Standard_Kit_Price__c!= NULL){
											acct.Standard_HSAH_Kit_Discount__c = (acct.hmr_HSAH_Standard_Kit_Price__c * coupon.ccrz__DiscountAmount__c)/100;
										}else{
											acct.Standard_HSAH_Kit_Discount__c = (0 * coupon.ccrz__DiscountAmount__c)/100;
										}
									}
								}
								System.debug('@@@@@@@@@@@@@@ Account after: ' + acct);
								accts.add(acct);

								try {
					            	//check for order items list to update for empty and then update
					            	if(!accts.isEmpty()){
						                update accts;
					            	}
					        	}
					        	catch (Exception ex) {
					            	System.debug('Exception in CC_CouponTriggerHandler handleAfterUpdate Account ' + ex.getMessage());
					        	}
		        			}
						}
					}
				}
			}
		}
    }
}