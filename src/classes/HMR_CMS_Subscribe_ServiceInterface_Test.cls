/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 30th, 2017
Target Class(ses): HMR_CMS_Subscribe_ServiceInterface
*/

@isTest
private class HMR_CMS_Subscribe_ServiceInterface_Test{
  

  @isTest static void firstMethod() {
    // Implement test code
    HMR_CMS_Subscribe_ServiceInterface subscribeController = new HMR_CMS_Subscribe_ServiceInterface();
    
    Map<String, String> params = new Map<String, String>();
    params.put('action','createLead');
    params.put('firstName','testfirst');
    params.put('lastName','testlast');
    params.put('email','test@test.com');
    params.put('zipcode','123456');  
   
    String resp = subscribeController.executeRequest(params);
    
    Map<String, String> paramsBlank;
    
    String respExcp = subscribeController.executeRequest(paramsBlank);
    
    HMR_CMS_Subscribe_ServiceInterface.getType();
    
    System.assert(!String.isBlank(resp));
    
  }
  

  @isTest static void secondMethod() { 
  
        HMR_CMS_Subscribe_ServiceInterface subscribeController = new HMR_CMS_Subscribe_ServiceInterface();
      
        Map<String, String> params = new Map<String, String>();
        params.put('action','createLead');
        params.put('firstName','testfirst');
        params.put('lastName','testlast');
        params.put('email','123');
        params.put('zipcode','123456');
      
        String resp = subscribeController.executeRequest(params);
        
        params.put('lastName','');
        params.put('zipcode','');
        
        String respNeg = subscribeController.executeRequest(params);
        
        params.put('action','testac');
        
        String respExp = subscribeController.executeRequest(params);
        
        System.assert(!String.isBlank(respExp));
    
  }
    
    @isTest static void thirdMethod() { 
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_HomePage_PageTemplate'));
  
        HMR_CMS_Subscribe_ServiceInterface subscribeController = new HMR_CMS_Subscribe_ServiceInterface();
      
        Map<String, String> params = new Map<String, String>();
        params.put('action','createLead');
        params.put('firstName','testfirst');
        params.put('lastName','testlast');
        params.put('email','123');
        params.put('zipcode','123456');
      
        String resp = subscribeController.executeRequest(params);
        
        params.put('lastName','');
        params.put('zipcode','');
        
        String respNeg = subscribeController.executeRequest(params);
        
        params.put('action','testac');
        
        String respExp = subscribeController.executeRequest(params);
        
        System.assert(!String.isBlank(respExp));
    
  }
  
}