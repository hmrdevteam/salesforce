/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_CCContextServiceInterface
*/

@isTest
private class HMR_CMS_CCContextServiceInterface_Test{
  
  @isTest static void test_general_method() {
  
       HMR_CMS_CCContextServiceInterface.isGuestUser = TRUE;
      
        Id p = [select id from profile where name='HMR Program Community Profile'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
        
        Cookie cook=new Cookie('currCartId', 'testcartId', null, 1440, false);
        ApexPages.currentPage().setCookies(new Cookie[]{cook});
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test12wtw3@noemail.com',
                emailencodingkey='UTF-8', lastname='Testinggggggg', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                
                timezonesidkey='America/Los_Angeles', username='testewttwwtr@noemail.com');
       
        insert userTest;
        
         cc_dataFActory.setupTestUser();
         User testUser = [Select id from user Limit 1];
         
         System.runAs(userTest){
         
         HMR_CMS_CCContextServiceInterface serviceObj = new HMR_CMS_CCContextServiceInterface();
        
        Map<String, String> userDetMap = new Map<String,String>();
        userDetMap.put('userId', userTest.id);
        
        HMR_CMS_CCContextServiceInterface.executeRequest(userDetMap);
        
        userDetMap = new Map<String,String>();
        userDetMap.put('portalUser', testUser.id);
        
        HMR_CMS_CCContextServiceInterface.executeRequest(userDetMap);
        
        HMR_CMS_CCContextServiceInterface.getCartCountRemoteActionCall(userDetMap);
        
        HMR_CMS_CCContextServiceInterface.getType();
        
        
    }
        
        Id prof = [select id from profile where name='HMR Program Community Profile'].id;
        Contact cont = new Contact(FirstName = 'firstt', LastName ='ttestCon',AccountId = testAcc.Id);
        insert cont; 
        User newUserTest = new User(alias = 'tesy123', email='tesyt123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testying', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='testkier@noemail.com');
       
        insert newUserTest;
        
        System.runAs(newUserTest){
            HMR_CC_Reorder_Controller reObj = new HMR_CC_Reorder_Controller();
            HMR_CMS_CCContextServiceInterface serviceObj2 = new HMR_CMS_CCContextServiceInterface(reObj);
        }

    }
}