/**
* Apex Rest Resource for Diet Tracker Configuration
* @Author: Javier Arroyo
*/
@RestResource(urlMapping='/mobile/dietTracker/config/*')
global class HMRMobDietTrackerConfig {

    /**
     * get method can retrieve all the records in Mobile_Diet_Tracker_Configuration__mdt or filter by program and diet type
     * @return array of json representation of the Diet Tracker Configuration
     */
    @HttpGet
    global static void doGet() {
        String allRecords = RestContext.request.params.get('allRecords');
        RestContext.response.addHeader('Content-Type', 'application/json');
		
        if ( allRecords != null && allRecords == 'true') {
            RestContext.response.responseBody = Blob.valueOf( getAllRecords() );
            RestContext.response.statuscode = 200;
        }
        else {
            String program = RestContext.request.params.get('program');
            String dietType = RestContext.request.params.get('dietType');
	        RestContext.response.responseBody = Blob.valueOf( getRecord(program, dietType) );                     
            RestContext.response.statuscode = 200;
        }       
    }
  
    /**
     * format the json representation of the diet tracker configuration into an array
     * @return json string
     */
    private static String format(List<Mobile_Diet_Tracker_Configuration__mdt> recs) {
        String result = '[';
        for( Integer i = 0; i < recs.size(); i++ ) {
            result = result + recs[i].Tracker_Configuration__c;
            if ( i + 1 < recs.size() ) { result = result + ','; }
        }

        return result + ']';        
    }
    
    /**
     * query by program/dietType
     * @return json string
     */
    private static String getRecord(String program, String dietType) {        
        List<Mobile_Diet_Tracker_Configuration__mdt> recs = 
        	[SELECT Program_Name__c, Diet_Type__c, Tracker_Configuration__c 
             	FROM Mobile_Diet_Tracker_Configuration__mdt 
             	where Program_Name__c =: program and Diet_Type__c =: dietType];
        return format(recs);
    }
    
    /**
     * query all the records
     * @return json string
     */    
    private static String getAllRecords() {
    	List<Mobile_Diet_Tracker_Configuration__mdt> recs = 
        	[SELECT Program_Name__c, Diet_Type__c, Tracker_Configuration__c 
             	FROM Mobile_Diet_Tracker_Configuration__mdt ];
        return format(recs);
    }
    
}