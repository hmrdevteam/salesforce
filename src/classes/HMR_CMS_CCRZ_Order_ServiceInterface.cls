/**
* Apex Cart (Add to Cart) Service Interface for Cart
*
* @Date: 2017-10-19
* @Author: Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/
global with sharing class HMR_CMS_CCRZ_Order_ServiceInterface implements cms.ServiceInterface{
  /**
  *
  * @param params a map of parameters including at minimum a value for 'action'
  * @return a JSON-serialized response string
  */
    public String executeRequest(Map<String, String> params) {
     Map<String, Object> returnMap = new Map<String, Object>{'success' => true};

        if(params.get('userId') != null){

            String userId = String.escapeSingleQuotes(params.get('userId'));
            HMR_Order_Service orderService = new HMR_Order_Service(userId);

            try {
                String action = params.get('action');
                //Return the calling function/action
                returnMap.put('action', action);

                if(action == 'getOrder') {
                    string encryptedOrderId = params.get('encryptedOrderId');
                    HMR_Order_Service.OrderRecord orderRecord = orderService.getOrderByEncryptedId(encryptedOrderId);
                    returnMap.put('order', orderRecord);
                }
                else
                 if(action == 'getOrderByOwnerId') {
                    string ownerId = params.get('userId');
                    List<HMR_Order_Service.OrderRecord> orderList = orderService.getOrderByOwner(ownerId);
                    returnMap.put('orders', orderList);
                }

                else
                 if(action == 'getOrderByOwnerIdMA') {
                    string ownerId = params.get('userId');
                    List<HMR_Order_Service.OrderRecord> orderList = orderService.getOrderByOwnerMA(ownerId);
                    returnMap.put('orders', orderList);
                }

                else
                 if(action == 'changeDate') {
                    string ownerId = String.escapeSingleQuotes(params.get('userId'));
                    String orderId = String.escapeSingleQuotes(params.get('orderId'));
                    String orderDate = String.escapeSingleQuotes(params.get('orderDate'));
                    Boolean csrFlow = Boolean.valueOf(params.get('csrFlow'));
                    Map<String, Object> changeDateResponse = orderService.updateOrderDate(orderId, orderDate, csrFlow);
                    returnMap.put('changeDateResp', changeDateResponse);
                }

                else{
                    //Invalid Action
                    returnMap.put('success',false);
                    returnMap.put('message','Invalid Action');
                }
            }
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'UserId parameter required');
        }

        return JSON.serialize(returnMap);
    }

    public static Type getType() {
        return HMR_CMS_CCRZ_Order_ServiceInterface.class;
    }
}