/**
* Apex Service Class for Content Search
*
* @Date: 2017-04-26
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-04-26
* @JIRA: 
*/

public with sharing class HMR_Content_Search_Service {
	private final string DefaultLanguage = 'en_US';
    private final string DefaultPublishStatus = 'Online';
    private final string DefaultSnippetLength = '120';
    private string communityId = '';

    //This made the search a few milliseconds slower, if issue may need different types for Authored/User Submitted
    public Set<Id> SuccessStoryHMRAuthoredIdSet {
        get{
            if(SuccessStoryHMRAuthoredIdSet == null){
                SuccessStoryHMRAuthoredIdSet = new Set<Id>();
                HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
                
                for(HMR_Knowledge_Service.ArticleRecord articleRecord : knowledgeService.getByArticleType('Success_Story__kav', 0, 0, 'AND Publish_to_Success_Story_Page__c = true'))
                    SuccessStoryHMRAuthoredIdSet.add(articleRecord.KnowledgeArticleId);
            }
            
            return SuccessStoryHMRAuthoredIdSet;
        }
        set;
    }

    public Map<String, Set<String>> KnowledgeDataCategoryMap{
        get{
            if(KnowledgeDataCategoryMap == null){
                HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
                KnowledgeDataCategoryMap = knowledgeService.getKnowledgeDataCategoryMap();
            }

            return KnowledgeDataCategoryMap;
        }
        set;
    }

    public HMR_Content_Search_Service(){
        this.communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;
    }

    public List<QuestionAndAnswerResultRecord> getSearchSuggestions(string searchText, boolean includeArticles, integer maxResults, string filterCategory, string filterTopic){
        ConnectApi.FeedElementPage chatterFeedResults;
        List<QuestionAndAnswerResultRecord> questionAndAnswerResultList = new List<QuestionAndAnswerResultRecord>();
        
        //If a filter is specified, filter by the topic name (non-localized) for chatter questions and answers (feedElements)
        // may have issue with # of items returned and not being able to pre-filter by topic/category
        try{ //Incase Connect Api Limit has been reached, catch error and still search knowledge
            chatterFeedResults = ConnectApi.ChatterFeeds.searchFeedElements(communityId, searchText + '*', null, maxResults, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
            
            for(ConnectApi.FeedElement feedElement : chatterFeedResults.elements){
                if(String.isBlank(filterCategory))
                    questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(feedElement));
                else{
                    if(feedElement.capabilities.topics != null && feedElement.capabilities.topics.items != null){
                        for(integer index = 0; index < feedElement.capabilities.topics.items.size(); index++){
                            ConnectApi.Topic topic = feedElement.capabilities.topics.items[index];
                            if(topic.nonLocalizedName == filterTopic){
                                questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(feedElement));
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex){
            System.Debug(ex);
        }

        if(includeArticles){
            List<Search.SearchResult> articleResults = getKnowledgeSearchResults(searchText, filterCategory);

            for(Search.SearchResult searchResult : articleResults)
                questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(searchResult, KnowledgeDataCategoryMap, SuccessStoryHMRAuthoredIdSet));
           
           questionAndAnswerResultList = appendRecipeCategories(questionAndAnswerResultList);
        }
        
        questionAndAnswerResultList.sort();
        
        return questionAndAnswerResultList;
    }

    public List<QuestionAndAnswerResultRecord> appendRecipeCategories(List<QuestionAndAnswerResultRecord> questionAndAnswerResultList){
        Set<Id> recipeIdSet = new Set<Id>();

        //We need to get Recipe Categories for URL Rewrites
        for(QuestionAndAnswerResultRecord result : questionAndAnswerResultList){
            if(result.resultType.startsWith('Recipe__'))
                recipeIdSet.add(result.Id);
        }

        if(recipeIdSet.size() > 0){
            Map<Id, Recipe__kav> recipeMap = new Map<Id, Recipe__kav>();
            
            for(Recipe__kav recipe : [SELECT KnowledgeArticleId, Diet_Category__c FROM Recipe__kav WHERE KnowledgeArticleId IN: recipeIdSet AND PublishStatus = 'online' AND Language = 'en_US'])
                recipeMap.put(recipe.knowledgeArticleId, recipe);

            if(recipeMap.size() > 0){
                for(QuestionAndAnswerResultRecord result : questionAndAnswerResultList){
                    if(result.resultType.startsWith('Recipe__') && recipeMap.containsKey(result.Id)){
                        string dietCategory = recipeMap.get(result.Id).Diet_Category__c;
                        
                        if(!String.isBlank(dietCategory)){
                            List<String> categoryPartList = dietCategory.split(';');
                            dietCategory = categoryPartList[0].replaceAll(' ','-').toLowerCase() + '-diet';
                            if(dietCategory.containsIgnoreCase('phase-2'))
                                dietCategory = 'phase-2';
                        }
                        
                        result.category = dietCategory;
                    }
                }
            }
        }



        return questionAndAnswerResultList;
    }

    //Does NOT work for Guests
    public List<QuestionAndAnswerResultRecord> getQuestionAndAnswersSuggestions(string searchText, boolean includeArticles, integer maxResults, string filterCategory, string filterTopic){
        ConnectApi.QuestionAndAnswersSuggestions result = ConnectApi.QuestionAndAnswers.getSuggestions(communityId, searchText, null, includeArticles && String.isBlank(filterCategory), maxResults);
        List<QuestionAndAnswerResultRecord> questionAndAnswerResultList = new List<QuestionAndAnswerResultRecord>();
        
        //If a filter is specified, filter by the topic name (non-localized) for chatter questions and answers (feedElements)
        // may have issue with # of items returned and not being able to pre-filter by topic/category
        for(ConnectApi.FeedElement feedElement : result.questions){
            if(String.isBlank(filterCategory))
            	questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(feedElement));
            else{
                if(feedElement.capabilities.topics != null && feedElement.capabilities.topics.items != null){
                    for(integer index = 0; index < feedElement.capabilities.topics.items.size(); index++){
                        ConnectApi.Topic topic = feedElement.capabilities.topics.items[index];
                        if(topic.nonLocalizedName == filterTopic){
                            questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(feedElement));
                            break;
                        }
                    }
            	}
            }
        }
        
        if(String.isBlank(filterCategory)){
            for(ConnectApi.ArticleItem articleItem : result.articles)
                questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(articleItem));
        }
        else{
            List<Search.SuggestionResult> articleResults = getKnowledgeSuggestionResults(searchText, maxResults, filterCategory);
       		for(Search.SuggestionResult suggestionResult : articleResults)
                questionAndAnswerResultList.add(new QuestionAndAnswerResultRecord(suggestionResult, KnowledgeDataCategoryMap, SuccessStoryHMRAuthoredIdSet));
        }
        
        questionAndAnswerResultList.sort();
        
        return questionAndAnswerResultList;
    }
    
    public List<Search.SuggestionResult> getKnowledgeSuggestionResults(string searchText, integer maxResults, string filterCategory){
        Search.SuggestionOption options = new Search.SuggestionOption();
        Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
        
        filters.setLanguage(DefaultLanguage);
        filters.setPublishStatus(DefaultPublishStatus);
        if(!String.isBlank(filterCategory)){
            if(filterCategory == 'recipes')
                filters.addArticleType(Recipe__kav.sobjecttype.getDescribe().getKeyPrefix());
            else
        	   filters.addDataCategory('HMR_Knowledge', filterCategory);
        }
        
        options.setFilter(filters);
        options.setLimit(maxResults);
        
        Search.SuggestionResults suggestionResults = Search.suggest(searchText, 'KnowledgeArticleVersion', options);
        return suggestionResults.getSuggestionResults();
    }
    
    public List<Search.SearchResult> getKnowledgeSearchResults(string searchText, string filterCategory){
        String searchObject = (filterCategory == 'recipes' ? 'Recipe__kav' : 'KnowledgeArticleVersion');
        String searchQuery = 'FIND \'' + searchText + '\' IN ALL FIELDS RETURNING ' + searchObject + '(Id, ArticleType, KnowledgeArticleId, Title, UrlName WHERE PublishStatus = \'' + DefaultPublishStatus + '\' AND Language = \'' + DefaultLanguage + '\') ' + getDataCategorySearchClause(filterCategory);
        Search.SearchResults searchResults = Search.find(searchQuery);
        List<Search.SearchResult> articleList = searchResults.get(searchObject);
        
        return articleList;
    }

    private string getDataCategorySearchClause(string filterCategory){
        string dataCategoryClause = '';

        if(!String.isBlank(filterCategory) && filterCategory != 'recipes')
            dataCategoryClause = 'WITH DATA CATEGORY HMR_Knowledge__c AT ' + filterCategory + '__c '; 
        
        return dataCategoryClause;
    }
    
    public Map<String,String> getArticleTypes(){
        Map<String,String> describeMap = new Map<String, String>();
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
        Set<String> keySet = globalDescribeMap.keySet();
        
        for (String key : keySet) {
            Schema.SObjectType objectType = globalDescribeMap.get(key);
            if (key.endsWith('ka') || key.endsWith('kb')) {
                describeMap.put(objectType.getDescribe().getKeyPrefix(), objectType.getDescribe().getLabel());
            }
        }
        
        return describeMap;
    }

    public List<HMR_DataCategory_Service.DataCategoryHierarchyRecord> getDataCategoryGroupStructures(){
        HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();
        return dataCategoryService.getDataCategoryGroupStructures();
    }

    public class QuestionAndAnswerResultRecord implements Comparable{
        public string id {get; set;}
        public string resultType {get; set;}
        public decimal rating {get; set;}
        public string title {get; set;}
        public string urlLink {get; set;}
        public boolean hasBestAnswer{get; set;}
        public integer commentCount{get; set;}
        public string category {get; set;}
        
        public QuestionAndAnswerResultRecord(ConnectApi.ArticleItem articleItem){
            this.id = articleItem.id;
            this.resultType = 'article';
            this.rating = articleItem.rating;
            this.title = articleItem.title;
            this.urlLink = articleItem.urlLink;
            this.hasBestAnswer = false;
            this.commentCount = 0;
            this.category = '';
        }
        
        public QuestionAndAnswerResultRecord(ConnectApi.FeedElement feedElement){
            this.id = feedElement.id;
            this.resultType = 'question';
            this.title = feedElement.capabilities.questionAndAnswers.questionTitle;
            this.urlLink = feedElement.url;
            this.hasBestAnswer = feedElement.capabilities.questionAndAnswers.bestAnswer != null;
            this.category = '';
            if(feedElement.capabilities.topics != null && feedElement.capabilities.topics.items.size() > 0){
                this.category = feedElement.capabilities.topics.items[0].name;
                if(this.category == 'HMR Suggested' && feedElement.capabilities.topics.items.size() > 1)
                    this.category = feedElement.capabilities.topics.items[1].name;
            }

            if(feedElement.capabilities.comments != null && feedElement.capabilities.comments.page != null)
            	this.commentCount = feedElement.capabilities.comments.page.total;
        }
        
        public QuestionAndAnswerResultRecord(Search.SuggestionResult suggestionResult, Map<String, Set<String>> knowledgeCategoryMap, Set<Id> successStoryHMRAuthoredIdSet){
            KnowledgeArticleVersion articleRecord = (KnowledgeArticleVersion)suggestionResult.getSObject();
            this.id = articleRecord.KnowledgeArticleId;
            this.resultType = articleRecord.ArticleType == null ? ((Id)this.id).getSObjectType().getDescribe().getName() : articleRecord.ArticleType;
            this.title = articleRecord.Title;
            this.urlLink = articleRecord.UrlName;
            this.hasBestAnswer = false;
            this.commentCount = 0;
            this.category = getCategory(knowledgeCategoryMap, articleRecord);
            if(this.resultType == 'Success_Story__ka' || this.resultType == 'Success_Story__kav')
                this.resultType = this.resultType + ' - ' + (successStoryHMRAuthoredIdSet.contains(this.id) ? 'HMR Authored' : 'User Submitted');
        }
        
        public QuestionAndAnswerResultRecord(Search.SearchResult searchResult, Map<String, Set<String>> knowledgeCategoryMap, Set<Id> successStoryHMRAuthoredIdSet){
            KnowledgeArticleVersion articleRecord = (KnowledgeArticleVersion)searchResult.getSObject();
            this.id = articleRecord.KnowledgeArticleId;
            this.resultType = articleRecord.ArticleType == null ? ((Id)this.id).getSObjectType().getDescribe().getName() : articleRecord.ArticleType;
            this.title = articleRecord.Title;
            this.urlLink = articleRecord.UrlName;
            this.hasBestAnswer = false;
            this.commentCount = 0;
            this.category = getCategory(knowledgeCategoryMap, articleRecord);
            if(this.resultType == 'Success_Story__ka' || this.resultType == 'Success_Story__kav')
                this.resultType = this.resultType + ' - ' + (successStoryHMRAuthoredIdSet.contains(this.id) ? 'HMR Authored' : 'User Submitted');
        }

        public QuestionAndAnswerResultRecord(Recipe__kav articleRecord){
            this.id = articleRecord.KnowledgeArticleId;
            this.resultType = articleRecord.ArticleType == null ? ((Id)this.id).getSObjectType().getDescribe().getName() : articleRecord.ArticleType;
            this.title = articleRecord.Title;
            this.urlLink = articleRecord.UrlName;
            this.hasBestAnswer = false;
            this.commentCount = 0;
        }

        public Integer compareTo(Object compareTo){
            QuestionAndAnswerResultRecord compareToResultRecord = (QuestionAndAnswerResultRecord)compareTo;
            
            return resultType == 'article' && compareToResultRecord.resultType == 'article' ? 
                		rating < compareToResultRecord.rating ? 1 : -1 : 
            	   resultType == 'question' && compareToResultRecord.resultType == 'question' ?
                       commentCount < compareToResultRecord.commentCount ? 1 : -1 : 0;
        }

        private string getCategory(Map<String, Set<String>> knowledgeCategoryMap, KnowledgeArticleVersion articleRecord){
            string categoryResult = getDataCategoryDisplay(articleRecord.Id, knowledgeCategoryMap);
            return categoryResult;
        }

        public String getDataCategoryDisplay(Id articleId, Map<String, Set<String>> dataCategoryToArticleMap){
            String dataCategoryDisplay = '';

            if(dataCategoryToArticleMap != null)
            {
                for(String dataCategory : dataCategoryToArticleMap.keySet()){
                    Set<String> articleSet = dataCategoryToArticleMap.get(dataCategory);
                    if(articleSet.contains(articleId))
                        dataCategoryDisplay += dataCategory + ', ';
                }

                if(!String.isBlank(dataCategoryDisplay)){
                    dataCategoryDisplay = dataCategoryDisplay.replaceAll('_',' ');
                    dataCategoryDisplay = dataCategoryDisplay.removeEnd(', ');
                }
            }

            //We want to only display category based on our prioritization of:
            //(Getting Started, Eating Well, Being More Active, Staying On Track)
            if(dataCategoryDisplay.containsIgnoreCase('Getting Started'))
                dataCategoryDisplay = 'Getting Started';
            else if(dataCategoryDisplay.containsIgnoreCase('Eating Well'))
                dataCategoryDisplay = 'Eating Well';
            else if(dataCategoryDisplay.containsIgnoreCase('Being More Active'))
                dataCategoryDisplay = 'Being More Active';
            else if(dataCategoryDisplay.containsIgnoreCase('Staying On Track'))
                dataCategoryDisplay = 'Staying On Track';
                
            return dataCategoryDisplay;
        }
    }
}