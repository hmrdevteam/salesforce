/**
* Date Utility Class, get End Date, Get Recurring Dates
*
* @Date: 2016-11-18
* @Author Pranay Mistry (Magnet 360)
* Used in CoachingSessionRecordsTriggerHandler for now
* @JIRA: related https://reside.jira.com/browse/HPRP-576
*/

public with sharing class DateUtil {
	
	/**
	*  getEndDate
	*  Utility function to get an end date from a starting date
	*  to generate a date range
	*
	*  @param Date Startingdate and 3 integers to add years, months and days to starting date
	*  @return Date Enddate generated based on start date and addition of years, months and days 
	*/
	public static Date getEndDate(Date startingDate, Integer addYears, Integer addMonths, Integer addDays) {
		Date endDate = null;
		//check for null starting date
		if(startingDate != null) {
			endDate = startingDate.addYears(addYears).addMonths(addMonths).addDays(addDays);
		}		
		return endDate;
	}

	/**
	*  getRecurringDates
	*  Utility function to get recurring dates between
	*  a date range
	*
	*  @param Date Startingdate, 3 integers to add years, months and days to starting date
	*  @param Integer for frequency of the recurrence in days, flag to include starting date
	*  @return List of recurring Dates
	*/
	public static List<Date> getRecurringDates(Date startingDate, Integer noOfYears, Integer noOfMonths, Integer noOfDays, Integer frequency, Boolean includeStartingDate) {
		List<Date> recurringDates = new List<Date>();
		//condition to add starting date in the recurring list
		if(includeStartingDate) {
			recurringDates.add(startingDate);
		}
		Date endDate = DateUtil.getEndDate(startingDate, noOfYears, noOfMonths, noOfDays);
		//check for null end date
		if(endDate != null) {
			//loop to generate the recurring list
			while(startingDate <= endDate) {
				recurringDates.add(startingDate.addDays(frequency));
				startingDate = startingDate.addDays(frequency);				
			}
		}
		return recurringDates;
	}
}