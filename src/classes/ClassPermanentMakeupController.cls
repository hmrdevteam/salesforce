/**
* Controller for Permanent Class makeup
* Objects referenced --> Class__c, Session_Attendee__c, Class_Member__c
*
* @Date: 2016-12-21
* @Author Aslesha (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-930
*/

public class ClassPermanentMakeupController {

    public ClassPermanentMakeupController(ApexPages.StandardSetController controller) {

    }

    //variable list
    public List<Class__c> availableClassList{get;set;}
    public Boolean noRecords{get;set;}
    public String noRecordMessage{get;set;}
    public String selectedClass{get;set;}
    public Class_Member__c classMemberDetails {get; set;}
    public Id classMemberId{get;set;}
    public String NewCMUrl {get; set;}
    public Boolean allowTransition = false;
    public Contact contactRecord {get;set;}
    public Integer timeOffset {get;set;}
    private static final Map<String, Integer> getMilitaryHour = new Map<String, Integer>{
        '12 AM'=> 0,
        '1 AM'=> 1,
        '2 AM'=> 2,
        '3 AM'=> 3,
        '4 AM'=> 4,
        '5 AM'=> 5,
        '6 AM'=> 6,
        '7 AM'=> 7,
        '8 AM'=> 8,
        '9 AM'=> 9,
        '10 AM'=> 10,
        '11 AM'=> 11,
        '12 PM'=> 12,
        '1 PM'=> 13,
        '2 PM'=> 14,
        '3 PM'=> 15,
        '4 PM'=> 16,
        '5 PM'=> 17,
        '6 PM'=> 18,
        '7 PM'=> 19,
        '8 PM'=> 20,
        '9 PM'=> 21,
        '10 PM'=> 22,
        '11 PM'=> 23
    };
    private static final Map<Integer, String> getStandardHour = new Map<Integer, String>{
         0=> '12 AM',
         1=> '1 AM',
         2=> '2 AM',
         3=> '3 AM',
         4=> '4 AM',
         5=> '5 AM',
         6=> '6 AM',
         7=> '7 AM',
         8=> '8 AM',
         9=> '9 AM',
         10=> '10 AM',
         11=> '11 AM',
         12=> '12 PM',
         13=> '1 PM',
         14=> '2 PM',
         15=> '3 PM',
         16=> '4 PM',
         17=> '5 PM',
         18=> '6 PM',
         19=> '7 PM',
         20=> '8 PM',
         21=> '9 PM',
         22=> '10 PM',
         23=> '11 PM'
    };
    //[TODO: GET NEXT COACHING SESSION TO ASSIGN TO START DATE]

    // this method will display the list of available class other than the class on class member object
    public void showOtherClasses(){
        try {
            //Get Class member id from URL
            classMemberId = ApexPages.currentPage().getParameters().get('Id');
            System.debug('classMemberId' + ' ' + classMemberId);
            if(classMemberId != null){
                System.debug('inside if classMemberId' + ' ' + classMemberId);
                // fetch class member details
                classMemberDetails = [SELECT Id, Name, hmr_Class__c, hmr_Contact_Name__c, hmr_Contact_Name__r.Time_Zone_Offset__c, hmr_Class_Type__c, hmr_Class__r.Name, hmr_Class__r.hmr_Class_Name__c,
                                        hmr_Class__r.hmr_First_Class_Date__c, hmr_Class__r.Coach_Name__c, hmr_Class__r.hmr_Class_Type__c, hmr_Class__r.hmr_Class_Strength__c,
                                        hmr_Class__r.hmr_Client_Enrolled__c, hmr_Class__r.hmr_Start_Time__c, hmr_Allow_Transition__c, hmr_Active__c
                                        FROM Class_Member__c WHERE Id = :classMemberId LIMIT 1];
                System.debug('after classMemberDetails' + ' ' + classMemberDetails);
                if(ClassMemberDetails.hmr_Allow_Transition__c == false){
                    //Fetch the list of avilable classes where class id not equal to the class id form class member record & active =true
                    availableClassList = [SELECT Id, Name, hmr_Class_Name__c, hmr_Class_Type__c, isEasternTime__c, hmr_First_Class_Date__c, hmr_Class_Strength__c, hmr_Client_Enrolled__c, hmr_Start_Time__c
                                        FROM Class__c WHERE Id <> :ClassMemberDetails.hmr_Class__c AND hmr_Active__c = true
                                        AND hmr_Class_Type__c = :ClassMemberDetails.hmr_Class__r.hmr_Class_Type__c];
                }
                else{
                    //Fetch the list of avilable classes where class id not equal to the class id form class member record & active =true
                    availableClassList = [SELECT Id, Name, hmr_Class_Name__c, hmr_Class_Type__c, isEasternTime__c, hmr_First_Class_Date__c, hmr_Class_Strength__c, hmr_Client_Enrolled__c, hmr_Start_Time__c
                                        FROM Class__c WHERE Id <> :ClassMemberDetails.hmr_Class__c AND hmr_Active__c = true
                                        AND hmr_Class_Type__c <> :ClassMemberDetails.hmr_Class__r.hmr_Class_Type__c];
                }

                if(classMemberDetails.hmr_Contact_Name__c != null){
                    contactRecord = [SELECT Id, FirstName, LastName, Time_Zone_Offset__c FROM Contact WHERE Id = :classMemberDetails.hmr_Contact_Name__c];

                    if(contactRecord.Time_Zone_Offset__c != null){
                        timeOffset = Integer.valueOf(contactRecord.Time_Zone_Offset__c) - 5;
                    }
                }
                System.debug('time offset' + ' ' + timeOffset);
                if(!availableClassList.isEmpty() && timeOffset != null && timeOffset != 0){
                    for(Class__c clsTime : availableClassList){                
                        try {
                            String hour = clsTime.hmr_Start_Time__c.substringBefore(':');
                            String min = clsTime.hmr_Start_Time__c.substringBetween(':', ' ');

                            if(clsTime.hmr_Start_Time__c.contains('AM')){
                                hour = hour + ' AM';
                            }
                            else{
                                hour = hour + ' PM';
                            }

                            Integer adjustHour = getMilitaryHour.get(hour);
                            //Loop counter
                            Integer i = 0;
                             
                            if(timeOffset > 0){
                                while (timeOffset > i) {
                                    if(adjustHour == 0){
                                        //if time is 0(Midnight), reset hour to 23(11 PM)
                                        adjustHour = 23;
                                    }
                                    else{
                                        adjustHour -= 1;
                                    }
                                    i++;
                                }
                            }
                            else if(timeOffset < 0){
                                timeOffset *= -1;
                                while (timeOffset > i) {
                                    if(adjustHour == 23){
                                        //if time is 23(11 PM), reset hour to 0(Midnight)
                                        adjustHour = 0;
                                    }
                                    else{
                                        adjustHour += 1;
                                    }                            
                                    i++;
                                }
                            }

                            String newTime = getStandardHour.get(adjustHour);

                            clsTime.hmr_Start_Time__c = newTime.contains('AM') ? newTime.substringBefore(' ') + ':' + min + ' AM' : newTime.substringBefore(' ') + ':' + min + ' PM';
                        }
                        Catch (exception e) {
                              clsTime.isEasternTime__c = true;
                        }                
                    }
                }
                else if(!availableClassList.isEmpty() && timeOffset == null){
                    for(Class__c clsTime : availableClassList){
                        clsTime.isEasternTime__c = true;
                    }
                }

                
                

                //Set the message to display on page if no classes available or Group Membership record is inactive
                if(classMemberDetails.hmr_Active__c == false){
                    availableClassList.clear();
                    noRecords = TRUE;
                    noRecordMessage = 'Group Membership is Inactive';
                }
                else if(availableClassList.size() <= 0){
                    noRecords = TRUE;
                    noRecordMessage = 'No Sessions To Display';
                } 
            }//end if
        }
        catch(Exception ex) {
            System.debug('Error in showOtherClasses ' + ex.getMessage());
        }
    }//end method

    //method to set the selected class Id
    public void selectClassId(){
        //get class id parameter
        selectedClass = System.currentPagereference().getParameters().get('selectedClassID');
    }


    /* this method will change the class ,
       It will delete all the future session attendee records ,
       inactivate the class member record &
       will create new class member record with selected class
    */

    public PageReference changeClass(){
        PageReference cmPage;
        try {
            //Inactivate exist class member record
            Class_Member__c oldClassMember = [SELECT Id,Name,hmr_Contact_Name__c,hmr_CRM_ContactID__c,hmr_AD_Restart_Number__c, hmr_Program_Membership__c, hmr_Allow_Transition__c from Class_Member__c where Id=:classMemberId];

            //update old class
            oldClassMember.hmr_Active__c = false;

            if(oldClassMember.hmr_Allow_Transition__c == true){
                oldClassMember.hmr_Allow_Transition__c = false;
                allowTransition = true;
            }
            else{
                allowTransition = false;
            } 
                        
            update oldClassMember;
            

            //Delete future session attendess records for the class member
            
            List<Session_Attendee__c> sessionAttnList = [SELECT ID FROM Session_Attendee__c WHERE hmr_Class_Member__c=:classMemberId AND hmr_Class_Date__c >=: Date.today() AND hmr_Attendance__c = null];
            if(sessionAttnList.size() > 0){
                delete sessionAttnList;
            }
            

            //Get new class details
            Class__c newClass = [SELECT Id, hmr_Coach__c FROM Class__c WHERE Id = :selectedClass];

            //Get first class details
            Date firstClassDate;
            
            try{
                Coaching_Session__c firstClass = [SELECT Id, hmr_Class_Date__c FROM Coaching_Session__c WHERE hmr_Class__c = :newClass.Id AND hmr_Class_Date__c >= :Date.today() ORDER BY hmr_Class_Date__c LIMIT 1];
                firstClassDate = firstClass.hmr_Class_Date__c;
            }
            catch (exception e) {
                firstClassDate = Date.today();
            }
            

            Program_Membership__c pmDetail = new Program_Membership__c();

            if(allowTransition == false){
                //get Program Membership details to update
                pmDetail = [SELECT Id, hmr_Current_Health_Coach__c, hmr_Membership_Type__c FROM Program_Membership__c WHERE Id = :oldClassMember.hmr_Program_Membership__c];
            }
            else{
                //get Program Membership details to update
                pmDetail = [SELECT Id, hmr_Current_Health_Coach__c, hmr_Membership_Type__c FROM Program_Membership__c WHERE hmr_Contact__c = :oldClassMember.hmr_Contact_Name__c AND hmr_Status__c = 'Active' LIMIT 1];
            }
            
            //Create New class member record
            Class_Member__c newClassMember = new Class_Member__c();

            newClassMember.hmr_Active__c = true;
            newClassMember.hmr_Class__c = selectedClass;
            newClassMember.hmr_Start_Date__c = firstClassDate;
            newClassMember.hmr_Coach__c = newClass.hmr_Coach__c;

            if(oldClassMember.hmr_Contact_Name__c != null)
                newClassMember.hmr_Contact_Name__c = oldClassMember.hmr_Contact_Name__c;

            //if(oldClassMember.hmr_CRM_ID__c != null)
            //    newClassmember.hmr_CRM_ID__c = oldClassMember.hmr_CRM_ID__c;

            if(oldClassMember.hmr_CRM_ContactID__c != null)
                newClassmember.hmr_CRM_ContactID__c = oldClassMember.hmr_CRM_ContactID__c;

            if(oldClassMember.hmr_AD_Restart_Number__c != null)
                newClassmember.hmr_AD_Restart_Number__c = oldClassMember.hmr_AD_Restart_Number__c;

            if(oldClassMember.hmr_Program_Membership__c != null && allowTransition == false)
                newClassmember.hmr_Program_Membership__c = oldClassMember.hmr_Program_Membership__c;

            if(pmDetail != null && allowTransition == true)
                newClassmember.hmr_Program_Membership__c = pmDetail.Id;

            
            
            //insert created record object
            insert newClassMember;

            //update Program Membership record with new Coach value
            pmDetail.hmr_Current_Health_Coach__c = newClassMember.hmr_Coach__c;
            pmDetail.hmr_Membership_Type__c = 'Phone Program';
            update pmDetail;

            //set message on the page
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'Successfully created New Class Member record with selected Class'));

            //set Id for redirection to newly created class member record
            NewCMUrl = '/'+newClassMember.id;

            //Redirects the page to the new CM record that was created
            cmPage = new PageReference(NewCMUrl);
        }
        catch(Exception ex) {
            System.debug('Error is changeClass ' + ex.getMessage());
        }
        return cmPage;
    }

}