public with sharing class HMRCouponAdminExtension {

    //instantiate all public variables
    public HMR_Coupon__c master {get;set;}
    public String hcId {get;set;}
    public ccrz__E_Coupon__c ccCoupon {get;set;}
    public String program {get;set;}
    public Id p1 {get;set;}
    public Id p2 {get;set;}
    public Id hss {get;set;}
    //Create map of Coupon Order Type picklist values to shorter strings for use in CC Coupon Code & Name fields
    public Map<String, String> orderTypes = new Map<String,String>{
                                                    'P1 1st Order' => 'P1_1',
                                                    'P1 Re-Order' => 'P1_RO',
                                                    'HSS 1st Order' => 'HS_1',
                                                    'HSS Re-Order' => 'HS_RO',
                                                    'P2' => 'P2',
                                                    'P2 Re-Order' => 'P2_RO',
                                                    'A la Carte' => 'ALC',
                                                    'P1 Interim' => 'P1_I',
                                                    'P2 Interim' => 'P2_I',
                                                    'HSS Interim' => 'HSS_I'
                                                  };

    public HMRCouponAdminExtension(ApexPages.StandardController stdController) {

      //retrieve id parameter from URL passed in from button
      hcId = apexpages.currentpage().getparameters().get('hcId');

      //retrieve the HMR Coupon record
      master = [Select Id, Name FROM HMR_Coupon__c WHERE Id =:hcId];

      //instatiate the new CC Coupon record and assign the HMR Coupon id to the lookup field
      ccCoupon = new ccrz__E_Coupon__c ();

	    ccCoupon.HMR_Coupon__c = master.Id;

      //get IDs of each program for use in assigning to CC Coupon later
      p1 = [Select Id FROM Program__c WHERE hmr_Program_Type__c = 'Healthy Solutions' AND hmr_Phase_Type__c = 'P1' LIMIT 1].Id;
      hss = [Select Id FROM Program__c WHERE hmr_Program_Type__c = 'Healthy Shakes' LIMIT 1].Id;
      p2 = [Select Id FROM Program__c WHERE hmr_Program_Type__c = 'Healthy Solutions' AND hmr_Phase_Type__c = 'P2' LIMIT 1].Id;

	}

  //Save method to be used in page buttons
  public void save() {

        //Map values for hidden fields
        ccCoupon.ccrz__Storefront__c = 'DefaultStore';
        ccCoupon.ccrz__CurrencyISOCode__c = 'USD';
        ccCoupon.ccrz__DiscountType__c = ccCoupon.ccrz__CouponType__c;
        ccCoupon.ccrz__CouponCode__c = master.name + '_' + orderTypes.get(ccCoupon.Coupon_Order_Type__c);
        ccCoupon.ccrz__CouponName__c = master.name + '_' + orderTypes.get(ccCoupon.Coupon_Order_Type__c);
        ccCoupon.ccrz__TotalUsed__c = 1;

        //Check to make sure there is a value in Coupon Order Type, then get the correct Program Id
        if(ccCoupon.Coupon_Order_Type__c != null){
            if(ccCoupon.Coupon_Order_Type__c.contains('P1')) {
                ccCoupon.hmr_Program__c = p1;
            } else if (ccCoupon.Coupon_Order_Type__c.contains('HSS')) {
                ccCoupon.hmr_Program__c = hss;
            } else if (ccCoupon.Coupon_Order_Type__c.contains('P2')) {
                ccCoupon.hmr_Program__c = p2;
            } else {
                ccCoupon.hmr_Program__c = null;
            }
        }

        //DML operation to insert the new CC Coupon record
        insert ccCoupon;

	}

  //This method makes the Save call and redirects back to parent HMR Coupon, for use in Save & Close button
	public PageReference saveAndClose() {

            save();

            PageReference pageRef= new PageReference('/'+hcId);
            pageRef.setRedirect(true);

        return pageRef;
    }

    //This method makes the Save call and re-loads the HMRCoupon page to create another CC Coupon,
    //for use in Save & Add Another button
    public PageReference saveAndNext() {

            save();

            PageReference pageRef= new PageReference('/apex/HMRCoupon?hcId='+hcId);
            pageRef.setRedirect(true);

        return pageRef;
    }

    //Cancel method to close page and redirect back to original record
    public PageReference cancel() {

            PageReference pageRef= new PageReference('/'+hcId);
            pageRef.setRedirect(true);

        return pageRef;
    }

}