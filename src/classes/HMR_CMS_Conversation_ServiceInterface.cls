/**
* Apex Conversation Service Interface for Join the Conversation
*
* @Date: 2017-07-14
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-14
* @JIRA: 
*/
global with sharing class  HMR_CMS_Conversation_ServiceInterface implements cms.ServiceInterface{
	private final integer PopularQuestionCount = 3;
    private final integer HMRSuggestedCount = 3;
	/**
	*
	* @param params a map of parameters including at minimum a value for 'action'
	* @return a JSON-serialized response string
	*/
    public String executeRequest(Map<String, String> params) {
        Map<String, Object> returnMap = new Map<string, Object>();

        try {
            String action = params.get('action');
            if(action == 'getConversationData') {
            	List<HMR_ConnectApi_Service.QuestionResultRecord> mostPopularFeedElementList = getMostPopularQuestions();
    			List<HMR_ConnectApi_Service.QuestionResultRecord> hmrSuggestedFeedElementList = getHMRRecommendedQuestions();
                returnMap.put('popularData', mostPopularFeedElementList);
                returnMap.put('hmrSuggestedData', hmrSuggestedFeedElementList);
                returnMap.put('success', true);
            }
            else{
                //Invalid Action
                returnMap.put('success',false);
                returnMap.put('message','Invalid Action');
            } 
        } 
        catch(Exception e) {
            // Unexpected Error
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMap.put('success',false);
            returnMap.put('message',JSON.serialize(message));
        }
        
        return JSON.serialize(returnMap);
    }

    private List<HMR_ConnectApi_Service.QuestionResultRecord> getMostPopularQuestions(){
        HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
        List<HMR_ConnectApi_Service.QuestionResultRecord> mostPopularQuestionList = connectApiService.getMostPopularQuestions();
        List<HMR_ConnectApi_Service.QuestionResultRecord> resultList = new List<HMR_ConnectApi_Service.QuestionResultRecord>();
        //Reduce to the number we want
        mostPopularQuestionList.sort();
        for(HMR_ConnectApi_Service.QuestionResultRecord questionResultRecord : mostPopularQuestionList){
            resultList.add(questionResultRecord);
            if(resultList.size() ==  PopularQuestionCount)
            	break;
        }
        return resultList; 
    }
    
    private List<HMR_ConnectApi_Service.QuestionResultRecord> getHMRRecommendedQuestions(){
        HMR_Topic_Service topicService = new HMR_Topic_Service();
        HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
        string topicId = topicService.getTopicId('HMR Suggested');
        List<HMR_ConnectApi_Service.QuestionResultRecord> hmrRecommendedQuestionList = connectApiService.getQuestionsByTopicId(topicId);
        List<HMR_ConnectApi_Service.QuestionResultRecord> resultList = new List<HMR_ConnectApi_Service.QuestionResultRecord>();
        
        for(HMR_ConnectApi_Service.QuestionResultRecord questionResultRecord : hmrRecommendedQuestionList){
            resultList.add(questionResultRecord);
            if(resultList.size() == HMRSuggestedCount)
                break;
        }

        return resultList;
    }

    public static Type getType() {
        return HMR_CMS_Conversation_ServiceInterface.class;
    }
}