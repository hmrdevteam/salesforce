/**
* Test Class Coverage of the HMR_Order_Service
*
* @Date: 10/27/2017
* @Author Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/
@isTest
private class HMR_Order_Service_Test{

    @testSetup
    private static void setupTestData(){

        ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'test@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 12345,
                                                   ccrz__EncryptedId__c = '12345',
                                                   ccrz__TaxAmount__c = 123
                                                );
        insert orderTestData;


        ccrz__E_Product__c productTestData = new ccrz__E_Product__c(
                                                    Name = 'Test Product',
                                                    ccrz__SKU__c = 'SK123',
                                                    ccrz__StartDate__c = Date.today(),
                                                    ccrz__EndDate__c = Date.today().addDays(1),
                                                    ccrz__Quantityperunit__c = 12
                                                );

        insert productTestData;

        ccrz__E_OrderItem__c orderItemData = new ccrz__E_OrderItem__c(
                                                    ccrz__Order__c = orderTestData.Id,
                                                    ccrz__Quantity__c = 2,
                                                    ccrz__OrderLineType__c = 'Major',
                                                    ccrz__Price__c = 123,
                                                    ccrz__SubAmount__c = 123,
                                                    ccrz__Product__c = productTestData.Id ,
                                                    ccrz__ParentProduct__c = productTestData.Id,
                                                    ccrz__DisplayProduct__c =  productTestData.Id
                                                );
        insert orderItemData;

        ccrz__E_TransactionPayment__c transactionPayData = new ccrz__E_TransactionPayment__c(
                                                    ccrz__CCOrder__c = orderTestData.Id,
                                                    ccrz__Storefront__c = 'DefaultStore',
                                                    ccrz__Amount__c = 123,
                                                    ccrz__RequestAmount__c = 123
                                                );

        insert transactionPayData;

    }



    @isTest
    private static void testOrderRecordMethod(){

        HMR_Order_Service orderService = new HMR_Order_Service();
        HMR_Order_Service.OrderRecord  orderRec = orderService.getOrderByEncryptedId('12345');
    }


    @isTest
    private static void testOrderRecordWrapperClass(){

        Map<String, Object> outputMap = new Map<String,Object>();

        List<Map<String, Object>> obList = new List<Map<String, Object>>();

        Map<String, Object> transMap = new Map<String, Object>();
        transMap.put('test', 'test');

        obList.add(transMap);

        outputMap.put('EOrderItemsS', obList);


        HMR_Order_Service.OrderRecord orderRec = new HMR_Order_Service.OrderRecord(outputMap, new List<Map<String, Object>>(),new List<Map<String, Object>>());
        string id = orderRec.id;
        String email =  orderRec.buyerEmail;
        String buyerPhone = orderRec.buyerPhone;
        integer orderNum =  orderRec.orderNumber;
        string encId =  orderRec.encryptedId;
        string isoCode = orderRec.currencyISOCode;
        decimal subAmount =  orderRec.subtotalAmount;
        decimal totalAmnt = orderRec.totalAmount;
        integer totalQty = orderRec.totalQuantity;
        decimal taxAmnt =  orderRec.taxAmount;
        decimal shipAmnt = orderRec.shipAmount;
        decimal surcharge =  orderRec.totalSurcharge;
        String shipMethod = orderRec.shipMethod;
        String shipNote = orderRec.shipNote;
     //Date deliveryDate = orderRec.deliveryDate;
        String shipTrack = orderRec.shipTrackingNo;
       // List<HMR_Order_Service.AddressRecord> addRec = orderRec.addressList;
        List<HMR_Order_Service.OrderItem> orderItem = orderRec.orderItemList;
        List<Map<String, Object>> outputList = orderRec.outputProductList;
    }

     @isTest
    private static void testOrderRecordAddressWrapperClass(){

        Map<String, Object> outputMap = new Map<String,Object>();

        List<Map<String, Object>> obList = new List<Map<String, Object>>();

        Map<String, Object> transMap = new Map<String, Object>();
        transMap.put('test', 'test');

        obList.add(transMap);

        outputMap.put('EOrderItemsS', obList);


        HMR_Order_Service.AddressRecord addressRec = new HMR_Order_Service.AddressRecord(new Map<String, Object>());
        string firstName = addressRec.firstName ;
        String lastName =  addressRec.lastName ;
        String addressFirstline = addressRec.addressFirstline ;
        String city =  addressRec.city ;
        string stateISOCode =  addressRec.stateISOCode;
        string postalCode = addressRec.postalCode;

    }

    @isTest
    private static void testUpdateOrderDateMethod(){

        HMR_Order_Service orderService = new HMR_Order_Service();

         ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'tes3t@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 12348,
                                                   ccrz__EncryptedId__c = '12345344',
                                                   ccrz__TaxAmount__c = 123
                                                );
        insert orderTestData;

        Map<String, Object>  orderRec = orderService.updateOrderDate('12345344', '06/06/2017', TRUE);
    }


    @isTest
    private static void testUpdateOrderDateNoORderMethod(){

        HMR_Order_Service orderService = new HMR_Order_Service();

         ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'tes3t@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 12348,
                                                   ccrz__EncryptedId__c = '12345344',
                                                   ccrz__TaxAmount__c = 123
                                                );
        insert orderTestData;

        Map<String, Object>  orderRec = orderService.updateOrderDate('123453441244', '06/06/2017', TRUE);
    }

    @isTest
    private static void testOrderByOwnerMethod(){

        HMR_Order_Service orderService = new HMR_Order_Service();

         ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'tes3t@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 123451,
                                                   ccrz__EncryptedId__c = '12345344',
                                                   ccrz__TaxAmount__c = 123,
                                                   ownerId = userInfo.getUserId()
                                                );
        insert orderTestData;

        List<HMR_Order_Service.OrderRecord>  orderRec = orderService.getOrderByOwner(userInfo.getUserId());
    }

    @isTest
    private static void testOrderByOwnerMAMethod(){

        HMR_Order_Service orderService = new HMR_Order_Service();

         ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'tes3t@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 123451,
                                                   ccrz__EncryptedId__c = '12345344',
                                                   ccrz__TaxAmount__c = 123,
                                                   ownerId = userInfo.getUserId()
                                                );
        insert orderTestData;

        List<HMR_Order_Service.OrderRecord>  orderRec = orderService.getOrderByOwnerMA(userInfo.getUserId());
    }

}