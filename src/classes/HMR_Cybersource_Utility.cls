/**
* HMR Cybersource Utility
* Objects referenced -->
*
* @Date: 2017-04-10
* @Author Pranay Mistry (M360)
* @Modified
* @Modification Log
*
* @Modified
* @Modification Log
*
* @JIRA:
*/

public class HMR_Cybersource_Utility {
    public static final Date now = Date.today();
    public static final String access_key = System.Label.HMR_Cybersource_AccessKey;
    public static final String profile_id = System.Label.HMR_Cybersource_Profile;
    public static final String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
    
    public static String signedDtString { get; set; }
    public static String reorderAmt { get; set; }
    public static Map<String, Object> signedFields { get; set; }
    public static Map<String, Object> unsignedFields { get; set; }
    public static List<String> signedFieldNames { get; set; }
    public static List<String> unsignedFieldNames { get; set; }
    
    public static Map<String, String> signedFieldValues { get; set; }
    public static Map<String, String> unSignedFieldValues { get; set; }
    
    public static Map<String, Object> cartResponseMap { get; set; }
    public static Map<String, String> userAddressMap { get; set; }
    
    public static Map<String, String> params = new Map<String, String> {
        'action' => 'getCartItemCount'
            };
                
                public HMR_Cybersource_Utility() {
                    //constructor
                }
    
    //build the POST string
    public static string generatePOSTString(){
        String returnMe = '';
        String paymentTokenStr = '';
        
        //generate the assigned Fields Name value
        string signedFieldNameString = '';
        for(string sfvKey : signedFieldNames){
            //if(signedFieldValues.containsKey(sfvKey))
            signedFieldNameString += sfvKey+',';
        }
        signedFieldNameString = signedFieldNameString.subString(0,signedFieldNameString.length()-1);
        //System.debug('signedFieldNameString');
        //System.debug(signedFieldNameString);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_SIGNEDFIELDNAMES,signedFieldNameString);
        
        //generate the UNassigned Fields Name value
        if(unsignedFieldNames != null && unsignedFieldNames.size()>0){
            string unsignedFieldNameString = '';
            for(string sfvKey : unsignedFieldNames){
                //if(unSignedFieldValues.containsKey(sfvKey))
                unsignedFieldNameString += sfvKey+',';
            }
            unsignedFieldNameString = unsignedFieldNameString.subString(0,unsignedFieldNameString.length()-1);
            //System.debug('unsignedFieldNameString');
            //System.debug(unsignedFieldNameString);
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_UNSIGNEDFIELDNAMES,unsignedFieldNameString);
        }
        for(string s: signedFieldNames){
            returnMe += s+'='+signedFieldValues.get(s)+'&';
            paymentTokenStr += s+'='+signedFieldValues.get(s)+',';
        }
        
        
        /******************************************************************************************************/
        /*Till this point, send all singned fields, includes the UNSIGNED_FIELD_NAMES field and values to sign*/
        /*Even UNSIGNED fields name is null*/
        /******************************************************************************************************/
        //prepare sign data string to sign
        paymentTokenStr = paymentTokenStr.substring(0, paymentTokenStr.length()-1);
        //System.debug('paymentTokenStr');
        //System.debug(paymentTokenStr);
        string signatureStr = signData(paymentTokenStr);
        ////System.debug('Anydatatype_msg+++++'+signatureStr);
        //System.debug('signatureStr');
        //System.debug(signatureStr);
        if(unSignedFieldValues!=null){
            for(string s: unsignedFieldNames){
                returnMe += s+'='+unSignedFieldValues.get(s)+'&';
            }
        }
        //signatureStr = signatureStr.replace('+','%2B');
        signatureStr =EncodingUtil.urlEncode(signatureStr, 'UTF-8');
        returnMe += cc_pgcs_ApiConstants.CS_RES_SIGNATURE+'='+signatureStr;
        
        
        System.debug('UUID----------->'+cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONUUID+'='+signedFieldValues.get(cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONUUID));
        System.debug('profile_id----->'+cc_pgcs_ApiConstants.CS_REQ_PROFILEID+'='+signedFieldValues.get(cc_pgcs_ApiConstants.CS_REQ_PROFILEID));
        System.debug('Access_key----->'+cc_pgcs_ApiConstants.CS_REQ_ACCESSKEY+'='+signedFieldValues.get(cc_pgcs_ApiConstants.CS_REQ_ACCESSKEY));
        System.debug('Secret_key----->'+System.Label.HMR_Cybersource_SecretKey);
        System.debug('Signature------>'+cc_pgcs_ApiConstants.CS_RES_SIGNATURE+'='+signatureStr);
        System.debug('Date & Time------>'+cc_pgcs_ApiConstants.CS_RES_SIGNEDDATETIME+'='+signedFieldValues.get(cc_pgcs_ApiConstants.CS_RES_SIGNEDDATETIME));
        
        
        
        System.debug('POST String---->'+returnMe);
        return returnMe;
    }
    
    public static String getReferenceTime() {
        DateTime current = System.now();
        Long timeInMili = current.getTime();
        return String.valueOf(timeInMili);
    }
    
    public static String uuid() {
        final String hex = EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        //System.debug('Transaction UUID');
        //System.debug(hex.SubString(0,8)+ '-' + hex.SubString(8,12) + '-' + hex.SubString(12,16) + '-' + hex.SubString(16,20) + '-' + hex.substring(20));
        
        string returnMeUUID = hex.SubString(0,8)+ '-' + hex.SubString(8,12) + '-' + hex.SubString(12,16) + '-' + hex.SubString(16,20) + '-' + hex.substring(20);
        return returnMeUUID;
    }
    
    public static String signData(final String token) {
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(token), Blob.valueOf(System.Label.HMR_Cybersource_SecretKey)));
    }
    
    public static String getUTCDateTime(){
        DateTime oUTSDateTime = DateTime.now();
        signedDtString = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'GMT');
        String strUTCDateTime = signedDtString;
        return strUTCDateTime;
    }
    
    //assign all signed fields value.
    public static void initSignedFieldValues(String transaction_type, String payment_token) {
        signedFieldValues = new Map<String, String>();
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONTYPE, transaction_type);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_CURRENCY, 'USD');
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_LOCALE, 'en-us');
        
        //determine the amount
        //if create payment token only, amount should be zero
        string pAmount = '0';
        
        if(reorderAmt != null){
            pAmount = reorderAmt;
        }else if(transaction_type != cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTCREATE &&transaction_type != cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE && cartResponseMap != null){
            if(cartResponseMap.get('cartTotalAmount') != null && (Decimal)cartResponseMap.get('cartTotalAmount') >= 0.0) {
                pAmount=String.valueOf(cartResponseMap.get('cartTotalAmount'));
            }
        }
        //assign the amount value
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_AMOUNT, pAmount);
        
        Map<String, Object> cartOutputData;
        if(cartResponseMap != null && cartResponseMap.size()>0){
            cartOutputData = (Map<String, Object>)cartResponseMap.get('outputData');
            if(userAddressMap != null || (cartOutputData.get(ccrz.ccAPICart.CART_OBJLIST) != null && ((List<Object>)cartOutputData.get(ccrz.ccAPICart.CART_OBJLIST)).size() > 0)){
                //assign user value
                assignSignedFieldValuesUser(cartOutputData);
                
                //assign billing address info
                assignSignedFieldValuesAddressMap(cartOutputData);
            }
        }

        //if payment token != null, use payment token, if null, use Card
        //for auth+update, both parameters need to be set up
        
        if(payment_token != null && !String.isBlank(payment_token)) {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTTOKEN, payment_token);
            if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTUPDATE){
                //if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH_PAYMENTUPDATE){
                signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTMETHOD, 'card');
            }
        }else {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTMETHOD, 'card');
        }
        
        //if updating token or auth+updateToken
        if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE ||transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTUPDATE) {
            //if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE ||transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH_PAYMENTUPDATE) {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_ALLOWPAYMENT_TOKEN_UPDATE, 'true');
        }
        
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_ACCESSKEY, access_key);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PROFILEID, profile_id);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONUUID, uuid());
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_RES_SIGNEDDATETIME, getUTCDateTime());
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_REFERENCENUMBER, getReferenceTime());
        
        //signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_SIGNEDFIELDNAMES, '');
        //signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_UNSIGNEDFIELDNAMES, '');
    }
    
    
    public static void initSignedFieldValuesUsingReferenceTime(String transaction_type, String payment_token, String referenceTime, String uTCDateTime) {
        signedFieldValues = new Map<String, String>();
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONTYPE, transaction_type);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_CURRENCY, 'USD');
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_LOCALE, 'en-us');
        
        //determine the amount
        //if create payment token only, amount should be zero
        string pAmount = '0';
        if(transaction_type != cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTCREATE &&transaction_type != cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE && cartResponseMap != null){
            if(cartResponseMap.get('cartTotalAmount') != null && (Decimal)cartResponseMap.get('cartTotalAmount') >= 0.0) {
                pAmount=String.valueOf(cartResponseMap.get('cartTotalAmount'));
            }
        }
        //assign the amount value
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_AMOUNT, pAmount);
        
        //
        Map<String, Object> cartOutputData;
        if(cartResponseMap != null && cartResponseMap.size()>0){
            cartOutputData = (Map<String, Object>)cartResponseMap.get('outputData');
        }
        
        //assign user value
        assignSignedFieldValuesUser(cartOutputData);
        
        //assign billing address info
        assignSignedFieldValuesAddressMap(cartOutputData);
        
        
        //if payment token != null, use payment token, if null, use Card
        //for auth+update, both parameters need to be set up
        
        if(payment_token != null && !String.isBlank(payment_token)) {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTTOKEN, payment_token);
            if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTUPDATE){
                //if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH_PAYMENTUPDATE){
                signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTMETHOD, 'card');
            }
        }else {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PAYMENTMETHOD, 'card');
        }
        
        //if updating token or auth+updateToken
        if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE ||transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTUPDATE) {
            //if(transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE ||transaction_type == cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH_PAYMENTUPDATE) {
            signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_ALLOWPAYMENT_TOKEN_UPDATE, 'true');
        }
        
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_ACCESSKEY, access_key);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_PROFILEID, profile_id);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_TRANSACTIONUUID, uuid());
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_RES_SIGNEDDATETIME, uTCDateTime);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_REFERENCENUMBER, referenceTime);
        
        //signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_SIGNEDFIELDNAMES, '');
        //signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_UNSIGNEDFIELDNAMES, '');
    }
    
    //assign signed fields values, UserInfo+AddressInfo
    public static void assignSignedFieldValuesUser(Map<String, Object> cartOutputData) {
        //userinfo map
        Map<String, Object> cartResponseMapCartList = new Map<String, Object>();
        
        string buyerFirstName='';
        string buyerLastName='';
        string buyerEmail='';
        
        //check input firstname last name first, if no input, check cart data
        if(userAddressMap != null){
            buyerFirstName = userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_FORENAME);
            buyerLastName = userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_SURNAME);
            buyerEmail = userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_EMAIL);
        }else if(cartOutputData != null && cartOutputData.get(ccrz.ccAPICart.CART_OBJLIST) != null){
            List<Object> cartResponseMapCartListObj = (List<Object>) cartOutputData.get(ccrz.ccAPICart.CART_OBJLIST);
            
            cartResponseMapCartList = (Map<String, Object>)cartResponseMapCartListObj[0];

            if(cartResponseMapCartList.get('buyerFirstName') != null && !String.isBlank((String)cartResponseMapCartList.get('buyerFirstName'))) {
                buyerFirstName = (String)cartResponseMapCartList.get('buyerFirstName');
            }
            
            if(cartResponseMapCartList.get('buyerLastName') != null && !String.isBlank((String)cartResponseMapCartList.get('buyerLastName'))) {
                buyerLastName = (String)cartResponseMapCartList.get('buyerLastName');
            }
            
            if(cartResponseMapCartList.get('buyerEmail') != null && !String.isBlank((String)cartResponseMapCartList.get('buyerEmail'))) {
                buyerEmail = (String)cartResponseMapCartList.get('buyerEmail');
            }
        }
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_FORENAME, buyerFirstName);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_SURNAME, buyerLastName);
        
        if(buyerEmail != null && (buyerEmail.contains('+') && buyerEmail.contains('@'))){
            buyerEmail = buyerEmail.split('\\+').get(0)+'@'+buyerEmail.split('@').get(1);
        }
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_EMAIL, buyerEmail);
    }
    
    
    //assign address info from cartOutputData
    public static void assignSignedFieldValuesAddressMap(Map<String, Object> cartOutputData){
        
        
        //if the address map is initated from argument already, use that
        //if not, try cartOurPutData
        if(userAddressMap != null && !userAddressMap.isEmpty()) {
            signedFieldValuesAddressMap(userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_LINE1),
                                        userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_CITY),
                                        userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_STATE),
                                        userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_COUNTRY),
                                        userAddressMap.get(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_POSTAL));
        }
        else {
            //get the billTo value
            List<Object> cartResponseMapCartListObj = (List<Object>) cartOutputData.get(ccrz.ccAPICart.CART_OBJLIST);
            if(cartResponseMapCartListObj!=null){
                Map<String, Object> cartResponseMapCartList = (Map<String, Object>)cartResponseMapCartListObj[0];
                String cartBillToId = '';
                if(cartResponseMapCartList.get('billTo') != null && !String.isBlank((String)cartResponseMapCartList.get('billTo'))) {
                    cartBillToId = (String)cartResponseMapCartList.get('billTo');
                }
                
                //get the address info
                if (cartOutputData.get(ccrz.ccAPIAddress.ADDRESSLIST) != null) {
                    List<Object> cartResponseMapAddressList = (List<Object>) cartOutputData.get(ccrz.ccAPIAddress.ADDRESSLIST);
                    if(cartResponseMapAddressList.size() > 0) {
                        for(Object cartResponseMapAddressObj: cartResponseMapAddressList) {
                            Map<String, Object> cartResponseMapAddressMap = (Map<String, Object>)cartResponseMapAddressObj;
                            if(cartResponseMapAddressMap.size() > 0) {
                                if(cartBillToId != null && cartBillToId != '') {
                                    if(cartResponseMapAddressMap.get('sfid') != null && !String.isBlank((String)cartResponseMapAddressMap.get('sfid')) && (String)cartResponseMapAddressMap.get('sfid') == cartBillToId) {
                                        signedFieldValuesAddressMap((String)cartResponseMapAddressMap.get('addressFirstline'),
                                                                    (String)cartResponseMapAddressMap.get('city'),
                                                                    (String)cartResponseMapAddressMap.get('stateISOCode'),
                                                                    (String)cartResponseMapAddressMap.get('countryISOCode'),
                                                                    (String)cartResponseMapAddressMap.get('postalCode'));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    //if user Address map has NOT been passed through the argument, directly get the address from Cart Response
    //assign address info from cc Cart Fetch result
    public static void signedFieldValuesAddressMap(String address_line_1, String address_city, String address_state, String address_country, String address_postal) {
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_LINE1, address_line_1);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_LINE2, '');
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_CITY, address_city);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_STATE, address_state);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_COUNTRY, address_country);
        signedFieldValues.put(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_POSTAL, address_postal);
    }
    
    public static void initCartId(String userId) {
        params.put('userId', userId);
        
        if(!String.isBlank(ccrz.cc_CallContext.currCartId)){
            params.put('activeCartId', ccrz.cc_CallContext.currCartId);
        }
        
    }
    
    public static void initUserAddressMap(Map<String, String> userAddressMapArg) {
        if(userAddressMapArg != null && !userAddressMapArg.isEmpty()) {
            userAddressMap = userAddressMapArg;
        }
    }
    
    public static void initCartResponseMap() {
        cartResponseMap = (Map<String, Object>)JSON.deserializeUntyped(HMR_CMS_CCContextServiceInterface.executeRequest(params));
    }
    
    public static void initContext(String transaction_type, String payment_token, List<String> signed_field_names, List<String> unsigned_field_names, Map<String, String> unSigned_Field_Values) {
        initFields();
        initSignedFieldNames(signed_field_names);
        initSignedFieldValues(transaction_type, payment_token);
        if(unSigned_Field_Values!=null){
            initUnsignedFieldNames(unsigned_field_names);
            initUnsignedFieldValues(unSigned_Field_Values);
        }
    }
    
    public static void initContextUsingReferenceTime(String transaction_type, String payment_token,
                                                     List<String> signed_field_names, List<String> unsigned_field_names,
                                                     Map<String, String> unSigned_Field_Values, String referenceTime, String uTCDateTime) {
                                                         initFields();
                                                         initSignedFieldNames(signed_field_names);
                                                         initSignedFieldValuesUsingReferenceTime(transaction_type, payment_token, referenceTime, uTCDateTime);
                                                         if(unSigned_Field_Values!=null){
                                                             initUnsignedFieldNames(unsigned_field_names);
                                                             initUnsignedFieldValues(unSigned_Field_Values);
                                                         }
                                                     }
    
    public static void initFields() {
        signedFields = new Map<String, String>();
        unsignedFields = new Map<String, String>();
    }
    
    public static void initSignedFieldNames(List<String> signed_field_names) {
        signedFieldNames = signed_field_names;
        signedFieldNames.sort();
    }
    
    public static void initUnsignedFieldNames(List<String> unsigned_field_names) {
        unsignedFieldNames = unsigned_field_names;
        unsignedFieldNames.sort();
    }
    
    //build unsigned field map
    public static void initUnsignedFieldValues(Map<String, String> unSigned_Field_Values) {
        unSignedFieldValues = new Map<String, String>();
        for(string ufKey : unsignedFieldNames){
            unSignedFieldValues.put(ufKey, unSigned_Field_Values.get(ufKey));
        }
    }

    public static void authorizePayment(String userId, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        //initAuthorizeUsingCart(cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH, userId, userAddressMapArg, unSigned_Field_Values);
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        //initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH, null, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE, null, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
    }
    
    public static void authorizePayment(String userId, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values, Map<String, Object> cartDataMap) {
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        cartResponseMap = cartDataMap;

        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE, null, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
    }
    
    public static void authorizePaymentAndCreatePaymentToken(String userId, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTCREATE, null, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
    }
    
    public static void authorizeUsingPaymentToken(String userId, Map<String, String> userAddressMapArg,String payment_token) {
        //initUserAddressMap(userAddressMapArg);
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        //initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_USEPAYMENTTOKEN, null,null);
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_USEPAYMENTTOKEN, null,null);
    }
    
    public static void authorizeUsingPaymentTokenForReorder(String userId, Map<String, String> userAddressMapArg,String payment_token, string rAmt) {
        //initUserAddressMap(userAddressMapArg);
        reorderAmt = rAmt;
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        //initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_USEPAYMENTTOKEN, null,null);
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_USEPAYMENTTOKEN, null,null);
    }
    
    public static void createPaymentToken(String userId, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTCREATE, null, cc_pgcs_ApiConstants.CS_REQ_F_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
    }
    
    public static void createPaymentTokenUsingReferenceTime(String userId, Map<String, String> userAddressMapArg,
                                                            Map<String, String> unSigned_Field_Values, String referenceTime, String uTCDateTime) {
                                                                initCartId(userId);
                                                                initUserAddressMap(userAddressMapArg);
                                                                initCartResponseMap();
                                                                initContextUsingReferenceTime(cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTCREATE, null, cc_pgcs_ApiConstants.CS_REQ_F_CREATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values, referenceTime, uTCDateTime);
                                                            }
    
    public static void authorizePaymentAndUpdatePaymentToken(String userId, Map<String, String> userAddressMapArg, String payment_token, Map<String, String> unSigned_Field_Values) {
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        //initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_AUTH_PAYMENTUPDATE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_UPDATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
        initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_SALE_PAYMENTUPDATE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_AUTH_UPDATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED, unSigned_Field_Values);
    }
    
    //when update payment token,
    public static void updatePaymentToken(String userId, Map<String, String> userAddressMapArg,String payment_token, Map<String, String> unSigned_Field_Values) {
        initCartId(userId);
        initUserAddressMap(userAddressMapArg);
        initCartResponseMap();
        if(userAddressMapArg != null){
            initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_UPDATETOKEN, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED_UPDATETOKEN,unSigned_Field_Values);
        }else{
            //if no address argument, then dont pass address
            initContext(cc_pgcs_ApiConstants.CS_TRANSACTION_PAYMENTUPDATE, payment_token, cc_pgcs_ApiConstants.CS_REQ_F_UPDATETOKEN_NO_ADDRESS, cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED_UPDATETOKEN,unSigned_Field_Values);
        }
    }
    
    public static map<string,string> parseHTMLResponse(string respBody) {
        list<string> sdl = respBody.split('<input type="hidden" name="');
        
        map<string, string> returnMe = new map<string, string>();
        for(Integer i=1; i<sdl.size()-1;i++ ){
            string s = '';
            if(i != sdl.size()-1){
                s = sdl[i].replace('" />','');
            }else{
                s = sdl[i].split('" />').get(0);
            }
           
            returnMe.put(s.split('" id="').get(0),s.split('" value="').get(1).trim());
        }
        return returnMe;
    }
    
    
    public static void cancelPayment() {
        
    }
    
    public static void voidPayment() {
        
    }
    
    public static void issueRefund() {
        
    }
}