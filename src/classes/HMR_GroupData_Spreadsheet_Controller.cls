/**
* Controller group data for spreadsheet
* @Date: 2018-03-28
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA:
*/


public without sharing class HMR_GroupData_Spreadsheet_Controller{
    public string columnClass {get{
        return this.isHealthySolutions ? 'side-ways-hs' : 'side-ways-p2';
    }}
    public boolean isHealthySolutions {get{
        return this.groupSSheetData.programType != 'P2';
    }}

    public HMR_GroupSpreadsheet_Service.GroupSpreadsheet groupSSheetData {get; set;} 
    
    public HMR_GroupData_Spreadsheet_Controller(){  
        String classId = ApexPages.currentPage().getParameters().get('id');
       
        if(!String.isBlank(classId)){
            HMR_GroupSpreadsheet_Service dataService = new HMR_GroupSpreadsheet_Service();
            groupSSheetData = dataService.getGroupDataSpreadsheet(classId);
        }
    }

    public HMR_GroupData_Spreadsheet_Controller(ApexPages.StandardController controller){  
        this();
    }

}