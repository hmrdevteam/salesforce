/*
Developer: Utkarsh Goswami (Mindtree)
Date: June 07th, 2017
Modified: 
Modified Date : March 27th 2017
Target Class: HMR_CMS_HomePage_Hero_ContentTemplate
*/

@isTest
public class HMR_CMS_Home_Hero_ContentTmpl_Test{  

        
    @isTest static void test_general_method() { 

                
                HMR_CMS_HomePage_Hero_ContentTemplate homePageHero = new HMR_CMS_HomePage_Hero_ContentTemplate();
                String propertyValue = homePageHero.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                String TitleText = homePageHero.TitleText;                
                String SubTitleText = homePageHero.SubTitleText;
                String ButtonLabelText = homePageHero.ButtonLabelText;
                String OtherLinkLabelText = homePageHero.OtherLinkLabelText;                
                String HeroCTAButtonLinkGTMID  = homePageHero.HeroCTAButtonLinkGTMID;
                String HeroExistingMemberOtherLinkGTMID  = homePageHero.HeroExistingMemberOtherLinkGTMID;
                Boolean showBestDietsFastWeightLoss = homePageHero.showBestDietsFastWeightLoss;                
                Boolean showBestDietsWeightLoss = homePageHero.showBestDietsWeightLoss;
                
                homePageHero.testAttributes = new Map<String, String>  {
                    'PlansDetailsTitle' => 'title',
                    'SubTitleText' => 'desc',
                    'ButtonLabelText ' => 'button',
                    'OtherLinkLabelText ' => 'other',
                    'HeroCTAButtonLinkGTMID ' => 'test',
                    'HeroExistingMemberOtherLinkGTMID' => 'testgtmd',
                    'showBestDietsFastWeightLoss' => 'TRUE',
                    'showBestDietsWeightLoss' => 'TRUE'
                };
                
                String renderHTML = homePageHero.getHTML();
                
                System.assert(!String.isBlank(renderHTML));
                
    }
    
}