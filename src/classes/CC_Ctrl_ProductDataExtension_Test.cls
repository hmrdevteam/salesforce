/**
* @Date: 4/5/2017
* @Author: Mustafa Ahmed (HMR)
*
* This test class covers CC_Ctrl_ProductDataExtension
*/


@isTest
public class CC_Ctrl_ProductDataExtension_Test {

	@isTest static void test_method_one() {

    	cc_dataFActory.setupTestUser();
    	cc_dataFActory.setupCatalog();


    	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        System.runAs ( thisUser ) {

            final List<String> productIds = new List<String>();

            ccrz__E_Product__c prod = [SELECT Id, ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__ProductStatus__c = 'released' LIMIT 1];

        	productIds.add(prod.Id);

            final string productIds2 = prod.id;

            ccrz.cc_RemoteActionContext ctx2 = new ccrz.cc_RemoteActionContext();
            //ccrz.cc_RemoteActionResult rar2 = CC_Ctrl_ProductDataExtension.createProgramMembership(ctx2, productIds2);
            CC_Ctrl_ProductDataExtension objPageCntlr= new CC_Ctrl_ProductDataExtension();

            ccrz.cc_RemoteActionContext ctx1 = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar1 = CC_Ctrl_ProductDataExtension.getExtendedDataForProducts(ctx1, productIds);
            System.assert(rar1 != null);
            System.assert(rar1.success);
        }
	}
}