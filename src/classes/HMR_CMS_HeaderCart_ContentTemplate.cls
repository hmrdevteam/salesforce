/**
* Apex Content Template Controller for Cart inside header on OCMS Master
*
* @Date: 2017-03-206
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_HeaderCart_ContentTemplate extends cms.ContentTemplateController{
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_HeaderCart_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_HeaderCart_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String CartLabel{    
        get{
            return getPropertyWithDefault('CartLabel', 'Shopping Cart');
        }    
    }

    public String CartIconImage{
        get{
            return getProperty('CartIconImage');
        }
    }

    public Map<String, Object> outputData { get; set; }
    public Set<String> cartKeys { get; set; }
    public String exCartString { get; set; } 
    public String encryptedCartId { get; set; }

    public Integer getCartItemCount(Map<String, Object> inputData) {
        Integer cartCount = 0;
        Map<String, Object> outputCartData  = ccrz.ccAPICart.fetch(inputData);                
        if (outputCartData.get(ccrz.ccAPICart.CART_OBJLIST) != null) {
            // The cast to List<Map<String, Object>> is necessary...
            List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputCartData.get(ccrz.ccAPICart.CART_OBJLIST);
            if(outputCartList.size() > 0) {
                outputData = outputCartList[0];
                cartKeys = outputCartList[0].keySet(); 
                encryptedCartId =  (String)outputData.get('encryptedId');              
                List<Map<String, Object>> cartItems = (List<Map<String, Object>>)outputData.get('ECartItemsS');  
                if(cartItems != null) {                        
                    for(Integer i = 0; i < cartItems.size(); i++) {
                        if(cartItems[i].get('cartItemType') == 'Major') {
                            cartCount++;
                            if(cartItems[i].get('productType') == 'Product') {
                                cartCount--;
                                cartCount += (Integer)cartItems[i].get('quantity');
                            }
                        }
                    }
                }
            }                                                      
        }        
        return cartCount;
    }

    // This method generates the markup for the shopping cart in the header
    global virtual override String getHTML() {    
        String html = '';
        Integer cartCount = 0;
        String sitePrefix = Site.getBaseUrl();
        String userId = UserInfo.getUserId();
        String userProfileId = UserInfo.getProfileId();
        Id guestProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile'].Id;
        try {
            Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccApiCart.CARTSTATUS => 'Open',
                ccrz.ccApiCart.ACTIVECART => true,                   
                ccrz.ccAPI.API_VERSION => 3,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                        //ccrz.ccAPI.SZ_REL => new List<String>(),
                        //ccrz.ccAPI.SZ_ASSC => false
                    }
                }
            };
            if(userProfileId == guestProfileId) {              
                if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
                    String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();  
                    if(cartId != null) {
                        inputData.put(ccrz.ccApiCart.CART_ENCID, cartId);
                        cartCount = getCartItemCount(inputData);  
                    }
                }                  
            }
            else {
                inputData.put(ccrz.ccApiCart.BYOWNER, userId);//'0055C000000NCnc',        
                cartCount = getCartItemCount(inputData);
            }
        } 
        catch (Exception e) {
            // Error handling...
            exCartString = e.getMessage() + '<br/>' + e.getTypeName() + '<br/>' + e.getStackTraceString() + '<br/>' + e.getLineNumber();
            cartCount = -1;
            System.debug(e.getMessage());
        }        
        html += '<li id="cartli_wrapper">' + 
                    '<a href="#" id="hmr_cart_anchor">' + 
                        '<div class="shopping-cart-badge hidden">' + 
                            '<span class="shopping-cart-count-bubble">' + cartCount + '</span>' + 
                        '</div>' + 
                        '<span class="hmr-icon-cart" style= "background-image : url(\'' + sitePrefix + cartIconImage + '\')" aria-hidden="true">' + cartLabel.escapeHtml4() + '</span>' + 
                    '</a>' + 
                    '<input type="hidden" id="cartId" name="cartId" value="' + encryptedCartId + '" />' +                    
                '</li>';                
        return html;            
    } 
}