@RestResource(urlMapping='/mobile/notification/fire/*')
global class HMRMobFirePushNotificationManually {

    @HttpGet
    global static void doGet() {
        RestRequest request = RestContext.request;
        String sType = RestContext.request.params.get('type');
 		fire ( sType );
    }

    public static void fire(String sType) {
        if ( sType == 'DAY_BEFORE_START_DATE_REMIDER') {
            HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.DAY_BEFORE_START_DATE_REMIDER);
            DataBase.executeBatch(job);             
        }
        
        else if (sType == 'FIRST_DAY_OF_DIET_REMINDER') {
            HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.FIRST_DAY_OF_DIET_REMINDER);
            DataBase.executeBatch(job);             
        }
        
        else if (sType == 'WEIGHIN_REMINDER') {
            HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.WEIGHIN_REMINDER);
            DataBase.executeBatch(job);             
        }
        
        else if (sType == 'INACTIVE_REMINDER') {
            HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.INACTIVE_REMINDER);
            DataBase.executeBatch(job);             
        }               

        else if (sType == 'TEST_REMINDER') {
            HMRPushNotificationJob job = new HMRPushNotificationJob(HMRNotificationType.TEST_REMINDER);
            DataBase.executeBatch(job);             
        }
        
    }     
}