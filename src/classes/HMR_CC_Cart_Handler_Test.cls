/**
* Test Class Coverage of the HMR_CC_Cart_Handler Class
*
* @Date: 11/10/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 11/10/2017
* @JIRA:
*/
@isTest
private class HMR_CC_Cart_Handler_Test {
	@isTest
	private static void testDefaultAddressAssignmentOnInsert(){
		User testUserRecord = cc_dataFactory.testUser;
		string encryptedCartId;

		//Assign ownership to address records from data factory class
		ccrz__E_ContactAddr__c billToAddressRecord = cc_dataFactory.billToAddress;
		billToAddressRecord.OwnerId = testUserRecord.Id;
		ccrz__E_ContactAddr__c shipToAddressRecord = cc_dataFactory.shipToAddress;
		shipToAddressRecord.OwnerId = testUserRecord.Id;
		update new List<ccrz__E_ContactAddr__c>{billToAddressRecord, shipToAddressRecord};

		//Bill to address book default
		ccrz__E_AccountAddressBook__c defaultBillingAddressRecord = cc_dataFactory.defaultBillingAdressBook;

		//Ship to address book default
		ccrz__E_AccountAddressBook__c defaultShippingAddressBook = cc_dataFactory.defaultShippingAdressBook;

		ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
		cartRecord.ccrz__ShipTo__c = null;
		cartRecord.ccrz__BillTo__c = null;

		Test.startTest();

		insert cartRecord;

		Test.stopTest();
		
		//Verify the shipping and billing addresses were defaulted
		ccrz__E_Cart__c cartRecordResult = [SELECT ccrz__BillTo__c, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cartRecord.Id];
		System.assert(!String.isBlank(cartRecordResult.ccrz__ShipTo__c));
		System.assert(!String.isBlank(cartRecordResult.ccrz__BillTo__c));
	}

	@isTest
	private static void testDefaultAddressAssignmentOnUpdate(){
		User userRecord = cc_dataFactory.testUser1;

		cc_dataFactory.testUser = userRecord;

		//Create the cart first
		ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
		cartRecord.ccrz__ShipTo__c = null;
		cartRecord.ccrz__BillTo__c = null;
		insert cartRecord;

		//Assign ownership to address records from data factory class
		ccrz__E_ContactAddr__c billToAddressRecord = cc_dataFactory.billToAddress;
		billToAddressRecord.OwnerId = userRecord.Id;
		ccrz__E_ContactAddr__c shipToAddressRecord = cc_dataFactory.shipToAddress;
		shipToAddressRecord.OwnerId = userRecord.Id;
		update new List<ccrz__E_ContactAddr__c>{billToAddressRecord, shipToAddressRecord};

		//Setup default address book records
		ccrz__E_AccountAddressBook__c defaultBillingAddressRecord = cc_dataFactory.defaultBillingAdressBook;
		ccrz__E_AccountAddressBook__c defaultShippingAddressBook = cc_dataFactory.defaultShippingAdressBook;

		//Update the cart
		Test.startTest();

		ccrz__E_Cart__c cartRecordToUpdate = [SELECT ccrz__Note__c FROM ccrz__E_Cart__c WHERE Id =: cartRecord.Id];
		cartRecordToUpdate.ccrz__Note__c = 'Updated Cart';
		update cartRecordToUpdate;

		Test.stopTest();
		
		//Verify the shipping and billing addresses were defaulted
		ccrz__E_Cart__c cartRecordResult = [SELECT ccrz__BillTo__c, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cartRecord.Id];
		System.assert(!String.isBlank(cartRecordResult.ccrz__ShipTo__c));
		System.assert(!String.isBlank(cartRecordResult.ccrz__BillTo__c));
	}
}