/**
*
* @Date: 2017-07-25
* @Author Zach Engman (Magnet 360)/Utkarsh Goswami (Mindtree)
* @Modified: 2017-07-25
* @JIRA: HPRP-4118
*/
global virtual with sharing class HMR_CMS_CardList_ContentTemplate extends cms.ContentTemplateController{
    private final integer defaultLeftRight = 2;
    private final integer defaultTopBottom = 4;
    private Integer offset = 0;
    private Integer recordCount = 0;
    private String recordsShown = '';
    private String colorBlocksShown = '';
    //Ran into issues using the properties defined for the edit page
    private string localListTitle = '';
    private string localObjectType = '';
    private string localDataCategory = '';
    private string localRecipeCategory = '';
    private string localSubFilters = '';

    global HMR_CMS_CardList_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    global HMR_CMS_CardList_ContentTemplate() {
        super();
    }
    global HMR_CMS_CardList_ContentTemplate(string listTitle, string objectType, string dataCategory, string recipeCategory, string subFilters, integer offset, integer recordCount, string recordsShown, string colorBlocksShown){
        this.localListTitle = listTitle;
        this.localObjectType = objectType;
        this.localDataCategory = dataCategory;
        this.localRecipeCategory = recipeCategory;
        this.localSubFilters = subFilters;
        this.offset = offset;
        this.recordCount = recordCount;
        this.recordsShown = recordsShown;
        this.colorBlocksShown = colorBlocksShown;
    }

    public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
    }

    /**
* A shorthand to retrieve a default value for a property if it hasn't been saved.
*
* @param propertyName the property name, passed directly to getProperty
* @param defaultValue the default value to use if the retrieved property is null
*/
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();


    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
* map in a test context.
*/
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String ListTitle {
        get{
            return getPropertyWithDefault('ListTitle', 'Recipes');
        }
        set;
    }

    public String ObjectType{
        get{
            return getPropertyWithDefault('ObjectType', 'Recipes');
        }
        set;
    }

    public String DataCategory{
        get{
            return getPropertyWithDefault('DataCategory', 'Getting_Started');
        }
        set;
    }

    public String RecipeCategory{
        get{
            return getPropertyWithDefault('RecipeCategory', 'All');
        }
        set;
    }



    global virtual override String getHTML() {
        String html = '';
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        HMR_Knowledge_Service.LayoutCount layoutCount;
        List<CardWrapper> leftRightCardList = new List<CardWrapper>();
        List<CardWrapper> topBottomCardList = new List<CardWrapper>();
        Color_Block__mdt colorBlock;
        Integer totalRecordCount = 0;
        Integer topBottomRecordIncrease = 0;
        Integer leftRightRecordIncrease = 0;
        boolean usedColorBlock = false;
        string additionalWhereClause = '';
        Set<String> recordsShownSet = new Set<String>();

        if(!String.isBlank(recordsShown) && offset > 0)
            recordsShownSet.addAll(recordsShown.split(','));

        if(String.isBlank(localListTitle))
            localListTitle = ListTitle;

        if(String.isBlank(localObjectType))
            localObjectType = ObjectType;

        if(String.isBlank(localDataCategory))
            localDataCategory = DataCategory;

        if(String.isBlank(localRecipeCategory))
            localRecipeCategory = RecipeCategory;

        if(offset == 0){
            html += '<input id="cardList_listTitle" type="hidden" value="' + localListTitle + '" />' +
                    '<input id="cardList_objectType" type="hidden" value="' + localObjectType + '" />' +
                    '<input id="cardList_dataCategory" type="hidden" value="' + localDataCategory + '" />' +
                    '<input id="cardList_recipeCategory" type="hidden" value="' + localRecipeCategory + '" />' +
                    '<input id="cardList_subFilters" type="hidden" value="' + localSubFilters + '" />';

            //Begin main container
            html += '<div class="ka-card-bg"><div class="container">';
        }

        colorBlock = getColorBlock();

        //Query by ObjectType
        if(localObjectType == 'Recipes'){
            //Handle additional filters applied
            if(!String.isBlank(localRecipeCategory) && localRecipeCategory != 'All')
                additionalWhereClause = 'AND Diet_Category__c LIKE \'%' + localRecipeCategory + '%\' ';
            //Handle sub filters applied
            if(!String.isBlank(localSubFilters)){
                List<String> subFilters = localSubFilters.split(',');
                if(subFilters.size() > 0){
                    additionalWhereClause += 'AND (';
                    for(String subFilter : subFilters)
                        additionalWhereClause += 'Category__c LIKE \'%' + subFilter + '%\' OR ';
                    additionalWhereClause = additionalWhereClause.removeEnd('OR ') + ') ';
                }
            }

            layoutCount = knowledgeService.getLayoutCountByArticleType('Recipe__kav', additionalWhereClause);
            totalRecordCount = layoutCount.TotalCount;

            //If there are not a good amount of a certain layout type, increase the pull of the other so we are at max
            if(layoutCount.LeftRightCount < defaultLeftRight)
                topBottomRecordIncrease = defaultLeftRight - layoutCount.LeftRightCount;
            if(layoutCount.TopBottomCount < defaultTopBottom)
                leftRightRecordIncrease = defaultTopBottom - layoutCount.TopBottomCount;

            for(HMR_Knowledge_Service.ArticleRecord articleRecord : knowledgeService.getByArticleType('Recipe__kav', defaultLeftRight + leftRightRecordIncrease, offset * defaultLeftRight, additionalWhereClause + 'AND Template_Layout__c = \'LeftRight\'')){
                if(!recordsShownSet.contains(articleRecord.KnowledgeArticleId)){
                    leftRightCardList.add(new CardWrapper(articleRecord));
                    recordsShown += (String.isBlank(recordsShown) ? '' : ',') + articleRecord.KnowledgeArticleId;
                }
            }

            topBottomRecordIncrease = leftRightCardList.size() == 0 ? 2 : leftRightCardList.size() == 1 ? 1 : 0;

            for(HMR_Knowledge_Service.ArticleRecord articleRecord : knowledgeService.getByArticleType('Recipe__kav', defaultTopBottom + topBottomRecordIncrease, offset * defaultTopBottom, additionalWhereClause + 'AND Template_Layout__c = \'TopBottom\'')){
                if(!recordsShownSet.contains(articleRecord.KnowledgeArticleId)){
                    topBottomCardList.add(new CardWrapper(articleRecord));
                    recordsShown += (String.isBlank(recordsShown) ? '' : ',') + articleRecord.KnowledgeArticleId;
                }
            }
        }
        else{
            layoutCount = knowledgeService.getLayoutCountByCategory(localDataCategory);
            totalRecordCount = layoutCount.TotalCount;

            //If there are not a good amount of a certain layout type, increase the pull of the other so we are at max
            if(layoutCount.LeftRightCount < defaultLeftRight)
                topBottomRecordIncrease = defaultLeftRight - layoutCount.LeftRightCount;
            if(layoutCount.TopBottomCount < defaultTopBottom)
                leftRightRecordIncrease = defaultTopBottom - layoutCount.TopBottomCount;

            for(HMR_Knowledge_Service.ArticleRecord articleRecord : knowledgeService.getByCategory(localDataCategory, defaultLeftRight + leftRightRecordIncrease, offset * defaultLeftRight, 'AND Card_Layout__c = \'LeftRight\'')){
                if(!recordsShownSet.contains(articleRecord.KnowledgeArticleId)){
                    leftRightCardList.add(new CardWrapper(articleRecord));
                    recordsShown += (String.isBlank(recordsShown) ? '' : ',') + articleRecord.KnowledgeArticleId;
                }
            }

            topBottomRecordIncrease = leftRightCardList.size() == 0 ? 2 : leftRightCardList.size() == 1 ? 1 : 0;

            for(HMR_Knowledge_Service.ArticleRecord articleRecord : knowledgeService.getByCategory(localDataCategory, defaultTopBottom + topBottomRecordIncrease, offset * defaultTopBottom, 'AND Card_Layout__c = \'TopBottom\'')){
                if(!recordsShownSet.contains(articleRecord.KnowledgeArticleId)){
                    topBottomCardList.add(new CardWrapper(articleRecord));
                    recordsShown += (String.isBlank(recordsShown) ? '' : ',') + articleRecord.KnowledgeArticleId;
                }
            }
        }

        recordCount += leftRightCardList.size() + topBottomCardList.size();

        //Generate the HTML - first where we have matching numbers of left right to top bottom
        integer matchingIndex = 0;
        while(matchingIndex < leftRightCardList.size() && matchingIndex < topBottomCardList.size()){
            html += '<div class="row">';

            if(Math.mod(matchingIndex, 2) == 0)
                html += getLayoutHtml(leftRightCardList[matchingIndex], topBottomCardList[matchingIndex], true);
            else
                html += getLayoutHtml(topBottomCardList[matchingIndex], leftRightCardList[matchingIndex], false);

            html += '</div>';
            matchingIndex ++;
            //Add color block record
            if(Math.mod(matchingIndex, 2) == 0 && colorBlock != null
                && (topBottomCardList.size() > 2 || leftRightCardList.size() <= matchingIndex)){ //If more left/right then we handle down below
                html += '<div class="row">';
                if(Math.mod(offset, 2) == 1)
                    html += getColorBlockHtml(colorBlock);
                if(topBottomCardList.size() > 2) {
                    html += getTopBottomCardHtml(topBottomCardList[2]);
                    topBottomCardList.remove(2);
                }
                if(topBottomCardList.size() > 2){
                    html += getTopBottomCardHtml(topBottomCardList[2]);
                    topBottomCardList.remove(2);
                }
                if(Math.mod(offset, 2) == 0)
                    html += getColorBlockHtml(colorBlock);
                html += '</div>';
                usedColorBlock = true;
                colorBlocksShown += (String.isBlank(colorBlocksShown) ? '' : ',') + colorBlock.Id;
            }
        }

        //Render any unmatched records (non equal amount of left right and top bottom)
        for(integer i = matchingIndex; i < leftRightCardList.size(); i++){
            html += '<div class="row">';
            //Color Block (at beggining)
            if(!usedColorBlock && colorBlock != null && Math.mod(offset, 2) == 1){
                //Color Block
                html += getColorBlockHtml(colorBlock);
                usedColorBlock = true;
                colorBlocksShown += (String.isBlank(colorBlocksShown) ? '' : ',') + colorBlock.Id;
            }

            html += getLeftRightCardHtml(leftRightCardList[i]);

            //Color Block (at end)
            if(!usedColorBlock && colorBlock != null && Math.mod(offset, 2) == 0){
                html += getColorBlockHtml(colorBlock);
                usedColorBlock = true;
                colorBlocksShown += (String.isBlank(colorBlocksShown) ? '' : ',') + colorBlock.Id;
            }

            html += '</div>';
        }

        for(integer j = matchingIndex; j < topBottomCardList.size(); j++){
            html += '<div class="row">';
            //Try to inject color block for extra if we can at beginning
            if(!usedColorBlock && colorBlock != null && Math.mod(offset, 2) == 1){
                //Color Block
                html += getColorBlockHtml(colorBlock);
                usedColorBlock = true;
                colorBlocksShown += (String.isBlank(colorBlocksShown) ? '' : ',') + colorBlock.Id;
                //Second Card
                html += getTopBottomCardHtml(topBottomCardList[j]);

                //Third Card
                if(j + 1 < topBottomCardList.size()){
                  html += getTopBottomCardHtml(topBottomCardList[j + 1]);
                  j++;
                }
            }
            else  {
                //First Card
                html += getTopBottomCardHtml(topBottomCardList[j]);
                //Second Card
                if(j + 1 < topBottomCardList.size()){
                  html += getTopBottomCardHtml(topBottomCardList[j + 1]);
                  j++;
                }
                //Color Block or Third Card
                if(!usedColorBlock && colorBlock != null && Math.mod(offset, 2) == 0){
                    html += getColorBlockHtml(colorBlock);
                    usedColorBlock = true;
                    colorBlocksShown += (String.isBlank(colorBlocksShown) ? '' : ',') + colorBlock.Id;
                }
                else if(j + 1 < topBottomCardList.size()){
                  html += getTopBottomCardHtml(topBottomCardList[j + 1]);
                  j++;
                }
            }

            html += '</div>';
        }

        if(recordCount < totalRecordCount){
            html += '<div class="row show-more" style="padding-bottom: 60px;">' +
                        '<div class="col-xs-12 text-center">' +
                            '<div class="link-container">' +
                                '<div class="hmr-allcaps-container">' +
                                    '<a onclick="cardList.showMore(\'' + localListTitle + '\',\'' + localObjectType + '\',\'' + localDataCategory + '\',\'' + localRecipeCategory + '\',\'' + localSubFilters + '\',\'' + (offset + 1) + '\')" class="hmr-allcaps hmr-allcaps-blue card-list-more">SEE MORE</a>' +
                                    '<a onclick="cardList.showMore(\'' + localListTitle + '\',\'' + localObjectType + '\',\'' + localDataCategory + '\',\'' + localRecipeCategory + '\',\'' + localSubFilters + '\',\'' + (offset + 1) + '\')" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue card-list-more">&gt;</a>' +
                                    '<input type="hidden" class="cardlist-recordCount" value="' + recordCount + '" />' +
                                    '<input type="hidden" class="cardlist-recordsShown" value="' + recordsShown + '" />' +
                                    '<input type="hidden" class="cardlist-colorBlocksShown" value="' + colorBlocksShown + '" />' +
                                    '<input type="hidden" class="cardlist-totalRecordCount" value="' + totalRecordCount + '" />' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

        }

        if(offset == 0)
            html += '</div></div>';

        return html;
    }

    private String getLayoutHtml(CardWrapper cardRecord01, CardWrapper cardRecord02, boolean displayLandscapeCardFirst){
        String html = '';

        if(displayLandscapeCardFirst)
            html += getLeftRightCardHtml(cardRecord01) + (cardRecord02 != null ? getTopBottomCardHtml(cardRecord02) : '');
        else
            html += getTopBottomCardHtml(cardRecord01) + (cardRecord02 != null ? getLeftRightCardHtml(cardRecord02) : '');

        return html;
    }

    private string getLeftRightCardHtml(CardWrapper cardRecord){
        string html = '';

        html += '<div class="ka-lr-card" onclick="cardList.navigateToRecord(\'' + cardRecord.CardType + '\', \'' + cardRecord.CardCategory + '\', \'' + cardRecord.CardNavigationUrl + '\')" data-category="'+cardRecord.CardCategory+'">' +
                    '<div class="ka-card lr-card col-lg-8 col-md-6 col-sm-6 col-xs-12 visible-lg hidden-md hidden-sm hidden-xs">' +
                        '<div class="product-card">' +
                                '<div class="ka-col ka-image">' +
                                    '<img src="' + cardRecord.CardImage + '"/>' +
                                '</div>' +
                                '<div class="ka-col ka-card-details">' +
                                    '<div class="card-row">' +
                                        '<div class="hmr-info">' + (!String.isBlank(localListTitle) ? localListTitle : '') + '</div>' +
                                    '</div>' +
                                    '<div class="card-row ka-title-row">' +
                                        '<h4 class="big-label">' + cardRecord.CardTitle + '</h4>' +
                                    '</div>' +
                                    '<div class="card-row ka-card-info">' +
                                        '<div class="ka-time-img">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockIcon.svg" />' +
                                        '</div>' +
                                        '<div class="ka-info hmr-allcaps">' +
                                            cardRecord.CardTimeDisplay + ' MIN' +
                                        '</div>' +
                                        '<div class="ka-heart-img">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartIcon.svg" />' +
                                        '</div>' +
                                        '<div class="ka-heart-count hmr-allcaps">' +
                                            cardRecord.CardRatingDisplay +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="card-row ka-details col-xs-12">' +
                                        '<div class="ka-author-img">' +
                                            '<img src="' + cardRecord.AuthorPhotoUrl + '" />' +
                                        '</div>' +
                                    '<div class="ka-author">' +
                                        '<h3>' + cardRecord.AuthorName + '</h3>' +
                                    '</div>' +
                                    '<div class="ka-separator">' +
                                        '<h3 class="book">|</h3>' +
                                    '</div>' +
                                    '<div class="ka-location">' +
                                        cardRecord.AuthorLocation +
                                    '</div>' +
                                '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            //Right Card Mobile
            html += '<div class="ka-card tb-card col-lg-4 col-md-6 col-sm-6 col-xs-12 hidden-lg visible-md visible-sm visible-xs" onclick="cardList.navigateToRecord(\'' + cardRecord.CardType + '\', \'' + cardRecord.CardCategory + '\', \'' + cardRecord.CardNavigationUrl + '\')" data-category="'+cardRecord.CardCategory+'">' +
                        '<div class="product-card">' +
                            '<div class="product-img">' +
                                '<img src="' + cardRecord.CardMobileImage + '"/>' +
                            '</div>' +
                            '<div class="card-block">' +
                                '<div class="card-row">' +
                                    '<div class="hmr-info">' + (!String.isBlank(localListTitle) ? localListTitle : '') + '</div>' +
                                '</div>' +
                                '<div class="card-row ka-title-row">' +
                                    '<h4 class="big-label">' + cardRecord.CardTitle + '</h4>' +
                                '</div>' +
                                '<div class="card-row ka-card-info">' +
                                    '<div class="ka-time-img">' +
                                        '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockIcon.svg" />' +
                                    '</div>' +
                                    '<div class="ka-info hmr-allcaps">' +
                                        cardRecord.CardTimeDisplay + ' MIN' +
                                    '</div>' +
                                    '<div class="ka-heart-img">' +
                                        '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartIcon.svg" />' +
                                    '</div>' +
                                    '<div class="ka-heart-count hmr-allcaps">' +
                                        cardRecord.CardRatingDisplay +
                                    '</div>' +
                                '</div>' +
                                '<div class="card-row ka-details col-xs-12">' +
                                    '<div class="ka-author-img">' +
                                        '<img src="' + cardRecord.AuthorPhotoUrl + '" />' +
                                    '</div>' +
                                    '<div class="ka-author">' +
                                        '<h3>' + cardRecord.AuthorName + '</h3>' +
                                    '</div>' +
                                    '<div class="ka-separator">' +
                                        '<h3 class="book">|</h3>' +
                                    '</div>' +
                                    '<div class="ka-location">' +
                                        cardRecord.AuthorLocation +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

        html += '</div>'; //Closes off the Main L-R

        return html;
    }

    private string getTopBottomCardHtml(CardWrapper cardRecord){
        string html = '';
        html += '<div class="ka-card tb-card col-lg-4 col-md-6 col-sm-6 col-xs-12" onclick="cardList.navigateToRecord(\'' + cardRecord.CardType + '\', \'' + cardRecord.CardCategory + '\', \'' + cardRecord.CardNavigationUrl + '\')" data-category="'+cardRecord.CardCategory+'">' +
                    '<div class="product-card">' +
                        '<div class="product-img">' +
                            '<img src="' + cardRecord.CardMobileImage + '"/>' +
                        '</div>' +
                        '<div class="card-block">' +
                            '<div class="card-row">' +
                                '<div class="hmr-info">' + (!String.isBlank(localListTitle) ? localListTitle : '') + '</div>' +
                            '</div>' +
                            '<div class="card-row ka-title-row">' +
                                '<h4 class="big-label">' + cardRecord.CardTitle + '</h4>' +
                            '</div>' +
                            '<div class="card-row ka-card-info">' +
                                '<div class="ka-time-img">' +
                                    '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockIcon.svg" />' +
                                '</div>' +
                                '<div class="ka-info hmr-allcaps">' +
                                    cardRecord.CardTimeDisplay + ' MIN' +
                                '</div>' +
                                '<div class="ka-heart-img">' +
                                    '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartIcon.svg" />' +
                                '</div>' +
                                '<div class="ka-heart-count hmr-allcaps">' +
                                    cardRecord.CardRatingDisplay +
                                '</div>' +
                            '</div>' +
                            '<div class="card-row ka-details col-xs-12">' +
                                '<div class="ka-author-img">' +
                                    '<img src="' + cardRecord.AuthorPhotoUrl + '" />' +
                                '</div>' +
                                '<div class="ka-author">' +
                                    '<h3>' + cardRecord.AuthorName + '</h3>' +
                                '</div>' +
                                '<div class="ka-separator">' +
                                    '<h3 class="book">|</h3>' +
                                '</div>' +
                                '<div class="ka-location">' +
                                    cardRecord.AuthorLocation +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        return html;
    }

    private string getColorBlockHtml(Color_Block__mdt colorBlock){
        string html = '';
        string baseUrl = IsProduction() ? '' : '/DefaultStore';
        string linkUrl = colorBlock.Button_Link__c;

        if(!String.isBlank(linkUrl)){
            if(!linkUrl.startsWith('/'))
                linkUrl = '/' + linkUrl;
        }

        html += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' +
                    '<div class="ka-color-block-card text-center" style="background-color:' + colorBlock.Background_Color__c + '">' +
                        '<div class="white">' +
                            (!String.isBlank(colorBlock.Title__c) ?
                             '<span class="sub-header">' + colorBlock.Title__c + '</span>' : '') +
                             '<h6 class="hmr-info">' + colorBlock.Teaser__c + '</h6>' +
                            (!String.isBlank(colorBlock.Button_Text__c) ?
                             '<div class="btn-container">' +
                             (colorBlock.Button_Text__c.containsIgnoreCase('subscribe') ?
                                '<div class="bg-subscribe" style="background-image: none;"><a href="#" data-toggle="modal" data-target="#hmrSubscribeModal" class="btn btn-primary hmr-btn-blue hmr-btn-small" style="background-color:' + colorBlock.Background_Color__c + '">' + colorBlock.Button_Text__c + '</a></div>' :
                                '<a href="' + baseUrl + linkUrl + '" class="btn btn-primary hmr-btn-blue hmr-btn-small" style="background-color:' + colorBlock.Background_Color__c + '">' + colorBlock.Button_Text__c + '</a>') +
                             '</div>' : '' ) +
                        '</div>' +
                    '</div>' +
                '</div>';
        return html;
    }

    private Color_Block__mdt getColorBlock(){
        Color_Block__mdt colorBlock;
        List<String> colorBlocksUsedList = String.isBlank(colorBlocksShown) ? new List<String>() : colorBlocksShown.split(',');
        String classification = localObjectType == 'Recipes' ? localObjectType : localDataCategory.replace('_',' ');
        Integer sortOrder = getColorBlockSortOrderParameter(colorBlocksUsedList.size());
        List<Color_Block__mdt> colorBlockList = [SELECT MasterLabel
                                                 ,Background_Color__c
                                                 ,Button_Link__c
                                                 ,Button_Text__c
                                                 ,Teaser__c
                                                 ,Title__c
                                                 ,Sort_Order__c
                                                 FROM Color_Block__mdt
                                                 WHERE Classification__c =: classification
                                                 AND ID NOT IN: colorBlocksUsedList
                                                 AND Sort_Order__c =: sortOrder
                                                 ORDER BY Sort_Order__c];

        if(colorBlockList.size() > 0)
            colorBlock = colorBlockList[Math.round(Math.random() * (colorBlockList.size() - 1))];

        return colorBlock;
    }

    private Integer getColorBlockSortOrderParameter(Integer colorBlocksUsed){
        Integer sortOrder = 1;
        Integer blockRemainder;

        //Randomize by sort order, cards are arranged in overall blocks of 4 (i.e. show one of the cards with sort order of 2)
        if(colorBlocksUsed > 0){
            //We want to skip sort order 1 after the first one is used as there will always just be one of those
            if(colorBlocksUsed >= 4 && Math.mod(colorBlocksUsed, 4) == 0)
                colorBlocksUsed++;

            blockRemainder = Math.mod(colorBlocksUsed, 4);
            sortOrder = blockRemainder == 1 ? 2 : blockRemainder == 2 ? 3 : blockRemainder == 3 ? 4 : 1;
        }

        return sortOrder;
    }

    public class CardWrapper{
        public String CardType {get; set;}
        public String CardImage {get; set;}
        public String CardMobileImage {get; set;}
        public String CardTitle {get; set;}
        public String CardTimeDisplay {get; set;}
        public String CardRatingDisplay {get; set;}
        public String CardNavigationUrl {get; set;}
        public String CardCategory {get; set;}
        public String CardLayout {get; set;}
        public String AuthorName {get; set;}
        public String AuthorLocation {get; set;}
        public string AuthorPhotoUrl {get; set;}

        public CardWrapper(HMR_Knowledge_Service.ArticleRecord articleRecord){
            this.AuthorName = 'HMR Team';
            this.AuthorLocation = articleRecord.AuthorTitle;
            this.AuthorPhotoUrl = 'https://www.myhmrprogram.com/ContentMedia/RecipeHome/DefaultProfilePicture.png';
            this.CardImage = articleRecord.ImageUri;
            this.CardMobileImage = !String.isBlank(articleRecord.MobileImageUri) ? articleRecord.MobileImageUri : articleRecord.ImageUri;
            this.CardTitle = articleRecord.Title;
            this.CardType = articleRecord.ArticleType;
            this.CardRatingDisplay = String.valueOf(articleRecord.Rating);
            this.CardTimeDisplay = String.isBlank(String.valueOf(articleRecord.TotalMinutes)) ? articleRecord.ReadTime : String.valueOf(articleRecord.TotalMinutes);
            this.CardNavigationUrl = articleRecord.UrlName;
            this.CardCategory = articleRecord.DataCategoryDisplay;
            this.CardLayout = articleRecord.TemplateLayout;
        }
    }

}