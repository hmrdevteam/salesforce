/**
* 
* @Date: 2018-01-12
* @Author: Utkarsh Goswami (Magnet 360)
* @Modified: 2018-02-13 - Zach Engman
* @JIRA:
*/

public virtual class HMRActivityDTO{

    public Id recordId {get;set;}
    public Integer recordVersion {get; set;}
    public String contactId {get;set;}
    public String programMembershipId {get;set;}
    public Date activityDate {get;set;}

    public HMRActivityDTO(){}
    public HMRActivityDTO(Id recordId, String contactId, String programMembershipId, Date activityDate){ 
        this.recordId = recordId;
        this.contactId = contactId;
        this.programMembershipId = programMembershipId;
        this.activityDate = activityDate;
    }
}