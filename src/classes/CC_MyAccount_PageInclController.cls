global with sharing class CC_MyAccount_PageInclController {
    global static String extendedProdJSON {get;set;}
    global static Map<String,Object> jsonMap {get;set;}
    global CC_MyAccount_PageInclController() {
        //String contactId = ApexPages.currentPage().getParameters().get('contactId');
        Map<String, sObject> dataMap = new Map<String, sObject>();
        Map<String, List<string>> varsMap = new Map<String, List<string>>();
        // if portalUser exits use that as userId if not use UserInfo.get
        String contactId;
        String portalUserId = ApexPages.currentPage().getParameters().get('portalUser');
        FormattedAddress billingAddress = new FormattedAddress();

        Id userId = UserInfo.getUserId();

        if (!String.isBlank(portalUserId)) {
            contactId = [SELECT ContactId FROM User WHERE ID = :portalUserId].contactId;
        }
        else {
            contactId = [SELECT ContactId FROM User WHERE ID = :userId].contactId;
        }

        system.debug('contactId: ' + contactId);
        try {

            List<Program_Membership__c> programMembershipList = new List<Program_Membership__c>(
                [SELECT Name,
                        hmr_Status__c,
                        hmr_Contact__c,
                        hmr_Membership_Type__c,
                        Program__c,
                        Program__r.hmr_Program_Display_Name__c,
                        hmr_Diet_Start_Date__c,
                        hmr_Diet_Start_Date_Changed__c
                 FROM Program_Membership__c
                 WHERE hmr_Contact__c = :contactId AND hmr_Status__c = 'Active']);

            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();

            if(programMembershipList.size() > 0){
                if(programMembershipList[0].hmr_Diet_Start_Date__c == null)
                    programMembershipList[0].hmr_Diet_Start_Date__c = programMembershipService.getDietStartDate(programMembershipList[0].hmr_Contact__c);

                //Check if diet start date can be changed, if not, set changed to true to hide ability
                if(!programMembershipList[0].hmr_Diet_Start_Date_Changed__c){
                    boolean canUpdateDietStartDate = programMembershipService.canUpdateDietStartDate(programMembershipList[0].hmr_Contact__c, programMembershipList[0].Id);
                    programMembershipList[0].hmr_Diet_Start_Date_Changed__c = !canUpdateDietStartDate;
                }
            }
            else{
                //If they don't have a membership, don't allow change of date by setting flag field
                programMembershipList.add(new Program_Membership__c(hmr_Diet_Start_Date_Changed__c = true));
            }

            Map<Id, Contact> contactInfoMap = new Map<Id, Contact>(
                [SELECT AccountId, hmr_Date_Of_Birth__c, hmr_Gender__c, hmr_Height_Feet__c, hmr_Height_Inches__c, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE Id = :contactId]);
                //[SELECT Id, hmr_Notes__c, hmr_Date_Of_Birth__c, hmr_Gender__c, hmr_Height_Feet__c, hmr_Height_Inches__c, hmr_Weight_Loss_Goal__c, hmr_Start_Weight__c, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE Id = :contactId]);

            Map<Id, User> userInfoMap = new Map<Id, User>(
                [SELECT Id, CommunityNickname, SmallPhotoUrl FROM User WHERE ContactId = :contactId]);

            //There can/should only be one active program membership at a time
            if(programMembershipList.size() > 0)
                dataMap.put('_programInfo', programMembershipList[0]);

            for(Contact contactInfo: contactInfoMap.values()){
                dataMap.put('_contactInfo', contactInfo);
            }

            for(User userInfo: userInfoMap.values()){
                dataMap.put('_userInfo', userInfo);
            }

            //Billing Address - we should always just have one contact record
            if(contactInfoMap.size() > 0)
                billingAddress = getFormattedBillingAddress(contactInfoMap.values()[0].AccountId, 'Billing');

            List<String> yearList = new List<String>();
            for (Integer i = 0; i < 100; i++) {
                Integer tempYear = System.Today().year() - i;
                yearList.add(String.valueOf(tempYear));
            }

            List<String> daysList = new List<String>();
            for (Integer i = 1; i < 32; i++) {
                daysList.add(String.valueOf(i));
            }
            varsMap.put('_contactVars', yearList);
            varsMap.put('_daysOfMonth', daysList);

            if((!String.isBlank(portalUserId)) && contactInfoMap.size() > 0 ){
                list<Contact> conIdSet = [select (Select Id from Contacts) from Account where Id = : contactInfoMap.values()[0].AccountId].Contacts;
                set<string> ids = new set<string>();
                for(Contact c: conIdSet){
                    string cid = c.Id;
                    system.debug('2222++'+cid.length());
                    if(cid.length()>15){
                        cid = cid.subString(0,15);
                    }
                    ids.add(cid);
                }

                list<ccrz__E_ContactAddr__c> extAddrListCon = [Select Id from ccrz__E_ContactAddr__c where Contact_Id__c in : ids AND OwnerId !=: portalUserId];
                //if(extAddrListCon.size()>0){
                    curAddrString = JSON.serialize(extAddrListCon);
                //}else{
                //    curAddrString = '';
                //}
            }else{
                curAddrString = JSON.serialize(new list<ccrz__E_ContactAddr__c>());
            }

        }
        catch (Exception err){
            System.debug('Exception in getExtendedDataForMyAccount controller');
            System.debug(err);
            System.debug(err.getLineNumber());
            System.debug(err.getMessage());
            curAddrString = JSON.serialize(new list<ccrz__E_ContactAddr__c>());
        }



        jsonMap = new Map<String,Object>{
                 'extProdData' => dataMap,
                 'extContactVars' => varsMap,
                 'extBillingAddress' => billingAddress
                 //,
                 //'extAddressList' => extAddrList
        };

        extendedProdJSON = JSON.serialize(jsonMap);
    }
    public string curAddrString {get;set;}


    //new method for hero section first name
    global String getUserFirstname() {
        String contactId;
        String portalUserId = ApexPages.currentPage().getParameters().get('portalUser');

        Id userId = UserInfo.getUserId();

        String firstNameStr = '';

        if (portalUserId != '' && portalUserId != null) {
            contactId = [SELECT ContactId FROM User WHERE ID = :portalUserId].contactId;
        } else {
            contactId = [SELECT ContactId FROM User WHERE ID = :userId].contactId;
        }

        firstNameStr = [SELECT FirstName FROM Contact where Id = :contactId].FirstName;

        return firstNameStr;
    }

    public class FormattedAddress {
        public string Name {get; set;}
        public string AddressLine01 {get; set;}
        public string CityStateZip {get; set;}

        public FormattedAddress() {}
        public FormattedAddress(ccrz__E_AccountAddressBook__c addressRecord){
            if(addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c != null &&
                String.isBlank(addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c))
                addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c = '';
            if(addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c != null && 
                String.isBlank(addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c))
                addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c = '';

            Name = String.format('{0} {1}', new String[]{addressRecord.ccrz__E_ContactAddress__r.ccrz__FirstName__c,
                                                         addressRecord.ccrz__E_ContactAddress__r.ccrz__LastName__c});

            AddressLine01 = String.format('{0} {1} {2}', new String[]{addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
                                                                      addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
                                                                      addressRecord.ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c});

            CityStateZip = String.format('{0}, {1} {2}', new String[]{addressRecord.ccrz__E_ContactAddress__r.ccrz__City__c,
                                                                      addressRecord.ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
                                                                      addressRecord.ccrz__E_ContactAddress__r.ccrz__PostalCode__c});
        }
    }

    private static FormattedAddress getFormattedBillingAddress(Id accountId, String addressType){ 
        //Order by Default to Get Default Record First (If It Exists)
        List<ccrz__E_AccountAddressBook__c> billingAddressList = [SELECT ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__City__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__State__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__PostalCode__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__Country__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__CompanyName__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__FirstName__c,
                                                                        ccrz__E_ContactAddress__r.ccrz__LastName__c
                                                                    FROM ccrz__E_AccountAddressBook__c
                                                                    WHERE ccrz__AccountId__c =: accountId
                                                                          AND ccrz__AddressType__c =: addressType
                                                                    ORDER BY ccrz__Default__c DESC];

        return billingAddressList.size() > 0 ? new FormattedAddress(billingAddressList[0]) : new FormattedAddress();
    }


    @remoteAction
    global static String changePassword(string newPassword, string verifyNewPassword, string oldPassword){
        pageReference pr;
        string reMessage = '';
        pr = Site.changePassword(newPassword, verifyNewPassword, oldPassword);
        if(pr != null){
            reMessage = 'Success';
        }else {
            for (ApexPages.Message m : ApexPages.getMessages()) {
                // Add messages to your response string

                reMessage+=m.getDetail()+'';
            }
        }
        return reMessage;
    }
}