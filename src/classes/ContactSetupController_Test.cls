/**
* Unit Test for ContactSetupController
*
* Objects referenced --> Contact, User, Account, AccountGroup, Profile
*
* @Date: 2/14/2017, 3/21/2017
* @Author Nathan Anderson (M360)
* @Modified: Rafa Hernandez (M360)  3/21/2017
* @JIRA: Issue HPRP-2315
*/

@isTest
private class ContactSetupController_Test {
    @isTest static void testConsumerClient() {
	    // Testing Data creation
		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='PortalAccount');
		insert ag;

		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'hmrtest@test.com';
        controller.c.MailingPostalCode = '60707';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'Consumer';
		controller.createUser = true;

		Test.startTest();
			controller.getAccountTypes();
			controller.registerUser();
			//System.Debug('@@@@@@@@@@@@@@@@@@@@ Contact Id =' + c.Id);
			controller.cancel();
		Test.stopTest();

	}

    @isTest static void testConsumerInCatchmentClient() {
	    // Testing Data creation
		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];
        ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='PortalAccount');
		insert ag;

        Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'Professional_Account'].Id;
		Account a = new Account(Name='TestCA', hmr_Active__c=true, ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;
		Zip_Code_Assignment__c zca = new Zip_Code_Assignment__c(Name='19044', Account__c=a.Id);
		insert zca;

		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'hmrtest@test.com';
        controller.c.MailingPostalCode = '19044';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'Consumer';
		controller.createUser = true;

		Test.startTest();
			controller.getAccountTypes();
			controller.registerUser();
			//System.Debug('@@@@@@@@@@@@@@@@@@@@ Contact Id =' + c.Id);
			controller.cancel();
		Test.stopTest();

	}

    @isTest static void testLeadConvertClient() {
	    // Testing Data creation
		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='PortalAccount');
		insert ag;

        Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'Professional_Account'].Id;
		Account a = new Account(Name='TestCA', hmr_Active__c=true, ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;

        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Company='TestCA', Account__c = a.Id, Email = 'test@test.com', hmr_Lead_Type__c = 'Clinic');
        insert l;

		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'test@test.com';
        controller.c.MailingPostalCode = '60707';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'Consumer';
		controller.createUser = true;

		Test.startTest();
			controller.getAccountTypes();
			controller.registerUser();
			//System.Debug('@@@@@@@@@@@@@@@@@@@@ Contact Id =' + c.Id);
			controller.cancel();
		Test.stopTest();

	}

    @isTest static void testLeadConvertClientNoAccount() {
	    // Testing Data creation
		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='PortalAccount');
		insert ag;

        Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'Professional_Account'].Id;
		Account a = new Account(Name='TestCA', hmr_Active__c=true, ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;

        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Company='TestCA', Email = 'test@test.com', hmr_Lead_Type__c = 'Consumer');
        insert l;

		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'test@test.com';
        controller.c.MailingPostalCode = '60707';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'Consumer';
		controller.createUser = true;

		Test.startTest();
			controller.getAccountTypes();
			controller.registerUser();
			//System.Debug('@@@@@@@@@@@@@@@@@@@@ Contact Id =' + c.Id);
			controller.cancel();
		Test.stopTest();

	}

	@isTest static void testEmployerClient() {
		// Testing Data creation
		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='PortalAccount');
		insert ag;
		Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'Corporate_Account'].Id;
		Account a = new Account(Name='TestCA', hmr_Employer_Account_ID__c='TestCA', hmr_Active__c=true, ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;


		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'hmrtest@test.com';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'Employer';
		controller.eCode = a.Id;
		controller.createUser = true;

		Test.startTest();
			controller.getCorporateAccounts();
            controller.setTypeFlag();
			controller.getEmployerFlag();
			controller.getAccountTypes();
			controller.registerUser();
			controller.cancel();
		Test.stopTest();
	}

	@isTest static void testHMREmpContact() {
		// Testing Data creation
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='HMR Employees');
		insert ag;
		Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;
		Account a = new Account(Name='HMR Employees', ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;

		ContactSetupController controller = new ContactSetupController();
		controller.c.FirstName = 'Test';
		controller.c.LastName = 'Test';
		controller.c.Email = 'hmrtest@test.com';
		controller.c.hmr_Contact_Original_Source__c = 'Search';
		controller.c.hmr_Contact_Original_Source_Detail__c = 'Organic';
		controller.selectedRType = 'HMR Employee';
		controller.createUser = false;

		Test.startTest();
			controller.getAccountTypes();
            controller.setTypeFlag();
			controller.getEmpTypeFlag();
			controller.registerUser();
			controller.cancel();
		Test.stopTest();

	}

	static testmethod void textRemoteAction() {

		ContactSetupController contactController = new ContactSetupController();
		Account testAccount = new Account();
		testAccount.Name='Test Account';
		List<HMR_Coupon__c> valueFromCoupons = new List<HMR_Coupon__c>();
		insert testAccount;
		valueFromCoupons = ContactSetupController.reminderForAccount(testAccount.id);

	}

	@isTest static void testException() {
		// Testing Data creation
		ccrz__E_AccountGroup__c ag = new ccrz__E_AccountGroup__c(Name='HMR Employees');
		insert ag;
		Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;
		Account a = new Account(Name='HMR Employees', ccrz__E_AccountGroup__c=ag.Id, RecordTypeId=rtype);
		insert a;

		ContactSetupController controller = new ContactSetupController();
		controller.selectedRType = 'HMR Employee';
		controller.createUser = true;

		Test.startTest();
			controller.registerUser();
		Test.stopTest();

	}

}