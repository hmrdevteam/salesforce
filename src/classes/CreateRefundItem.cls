/**
* @Date: 2017-03-30
* @Author Mustafa Ahmed (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-3037
* @Modified By: Ali Pierre
* @Modified Date: 6/23/2017
* @JIRA Bug: https://reside.jira.com/browse/HPRP-3872
*/

public class CreateRefundItem {

	Public Id refundId;
	Public Refund__c refundRecord {get; set;}	
	Public List<ccrz__E_OrderItem__c> orderItemList{get;set;}
	Public ccrz__E_OrderItem__c selectedOrderItemList{get;set;}
	Public Id orderItemId;
	Public Id orderId;
	Public List<ccrz__E_OrderItem__c> orderItemCouponList{get;set;}
	Public ccrz__E_OrderItem__c coupon {get; set;}	

	Public String selectedSKU {get;set;}
	Public Boolean btnStatus {get;set;}
	
	Public String refundNumber {get;set;}
	Public String orderNumber {get;set;}
	Public String productName {get;set;}
	Public String orderItemNumber {get;set;}
    Public Decimal pricePaid {get;set;}
    Public Decimal refundQty {get;set;}
    Public Decimal remainingQty {get;set;}
    Public Decimal remainingPrice {get;set;}

    Public Refund_Item__c[] insertRefundItem = new List<Refund_Item__c>();  

	Public CreateRefundItem(ApexPages.StandardController ctrl) {
        try{  
            refundId = apexpages.currentpage().getparameters().get('id');
            
	            if(refundId != null){                
	                refundRecord = [SELECT Id, Name, hmr_Order_Number__c, hmr_Order_Number__r.ccrz__OrderStatus__c, hmr_Return_Status__c FROM Refund__c WHERE Id = :refundId LIMIT 1];
	                		if (refundRecord != null){

	                					if (refundRecord.hmr_Return_Status__c == 'Approved'){
	                							btnStatus = true;
	                							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
	                								'This Refund is already Approved and cant accept new items. Please create new Refund record.'));
	                					}else if(refundRecord.hmr_Order_Number__r.ccrz__OrderStatus__c != 'Completed'){
	                						ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
	                								'Only completed orders are eligible for refunds.'));
	                					}
	                					else{
	                								btnStatus = false;
		                							refundNumber = refundRecord.Name;
		                							orderNumber = refundRecord.hmr_Order_Number__c;
		                							pricePaid = 0.00;
		                							refundQty = 0;
		                							remainingPrice = 0.00;
		                							remainingQty = 0;
		                							productName = 'SELECT SKU';
		                							orderItemNumber = 'SELECT SKU';
		                							
				                					orderItemList = [SELECT Id, Name, ccrz__Order__c, ccrz__Product__r.ccrz__SKU__c, ccrz__ProductType__c FROM ccrz__E_OrderItem__c 
				                									WHERE ccrz__Order__c = :refundRecord.hmr_Order_Number__c AND ccrz__ProductType__c <> 'Coupon' AND hmr_Fully_Refunded__c = False];

				                					orderItemCouponList = [SELECT Id, Name, ccrz__Order__c, ccrz__Product__r.ccrz__SKU__c, ccrz__ProductType__c, ccrz__Product__r.Name, ccrz__ItemTotal__c FROM ccrz__E_OrderItem__c 
				                									WHERE ccrz__Order__c = :refundRecord.hmr_Order_Number__c AND ccrz__ProductType__c = 'Coupon' LIMIT 1];
														
				                						if(orderItemList.size() > 0){

				                							String[] productSKU = new String[]{'SELECT'};

				                							for(ccrz__E_OrderItem__c ordItem : orderItemList){
				                								productSKU.add(ordItem.ccrz__Product__r.ccrz__SKU__c);                                                                
				                							}
				                								this.SKUOptions = new SelectOption[]{};
				
																for (String c: productSKU) {
																	this.SKUOptions.add(new SelectOption(c,c));
																}  
				                						}

				                						if(!orderItemCouponList.isEmpty()){
				                							coupon = orderItemCouponList[0];
				                						}

				                						System.debug(coupon.ccrz__ItemTotal__c);
		                					}  
	                							}
	                            			}	
        }

        catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex);
        }
    }
    	
	public SelectOption[] SKUOptions {
		public get;
		private set;
	}
	
	public pagereference populateRefundDetailsBlock(){        
		try{
			If (selectedSKU <> 'SELECT'){
					selectedOrderItemList = [SELECT Id, Name, ccrz__Order__c, ccrz__Product__r.ccrz__SKU__c, ccrz__Product__r.Name, ccrz__Price__c, ccrz__Quantity__c, 
											ccrz__Order__r.Id, Calculated_Absolute_Discount__c, ccrz__ProductType__c, hmr_Refunded_Amount__c, hmr_Refunded_Quantity__c, ccrz__ItemTotal__c FROM ccrz__E_OrderItem__c 
						                    WHERE ccrz__Order__c = :refundRecord.hmr_Order_Number__c AND ccrz__ProductType__c <> 'Coupon' 
						                    AND ccrz__Product__r.ccrz__SKU__c = :selectedSKU LIMIT 1];

					      if(selectedOrderItemList != null){
					      	
					      	if(selectedOrderItemList.hmr_Refunded_Quantity__c > 0 && selectedOrderItemList.hmr_Refunded_Quantity__c < selectedOrderItemList.ccrz__Quantity__c){
					      		refundQty = selectedOrderItemList.ccrz__Quantity__c - selectedOrderItemList.hmr_Refunded_Quantity__c;
					      	}
					      	else{
					      		refundQty = selectedOrderItemList.ccrz__Quantity__c;
					      	}
					      	
					      	remainingQty = refundQty;

					      	if(selectedOrderItemList.hmr_Refunded_Amount__c > 0 && selectedOrderItemList.hmr_Refunded_Amount__c < selectedOrderItemList.ccrz__ItemTotal__c){
					      		pricePaid = selectedOrderItemList.ccrz__ItemTotal__c - selectedOrderItemList.hmr_Refunded_Amount__c;
					      	}
					      	else{
			                	pricePaid = selectedOrderItemList.ccrz__ItemTotal__c;
					      	}
					      	
					      	remainingPrice = pricePaid;

			                productName = selectedOrderItemList.ccrz__Product__r.Name;
			                orderItemNumber = selectedOrderItemList.Name;	
			                orderItemId = selectedOrderItemList.Id;	
			                orderId = selectedOrderItemList.ccrz__Order__r.Id;		      	
					      }
				}			
			      else{
			      		pricePaid = 0.00;
	                	refundQty = 0;
	                	productName = 'SELECT SKU';
	                	orderItemNumber = 'SELECT SKU';
	                	remainingQty = 0;
	                	remainingPrice =0;
			      }   
        return null;	       
	    	}		

        catch(Exception ex){
            System.debug('The following exception has occurred: ' + ex);
            return null;
        	}	
        }			

	public PageReference saveNewItem(){	

			if(refundQty > remainingQty){
				System.debug('!!');
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Quantity submitted is greater than remaining items.'));
			}
			else if( pricePaid > remainingPrice){
				System.debug('!!!!!');
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Price submitted is greater than remaining price paid.'));
			}
			else{
				Refund_Item__c newRefundItem = new Refund_Item__c(hmr_CC_Order__c = orderId, CC_Order_Item__c = orderItemId, Price_paid_by_client__c = pricePaid, 
																hmr_Refund__c = refundId, hmr_Refund_Product_Quanity__c = refundQty, 
																hmr_Refund_Product_Quantity__c = refundQty, hmr_Refund_Product_SKU__c = selectedSKU);
       		 insertRefundItem.add(newRefundItem);      		 

        try{        
		        if (refundQty != 0 && pricePaid != 0.0 && productName != 'SELECT SKU' && orderItemNumber != 'SELECT SKU') {
						Insert insertRefundItem; 						
						PageReference cmPage = new PageReference('/'+refundId);   
						return cmPage;						
		       				 }
		       	else{
		       		    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter values on all the fields.'));
		       			return null;
		       		} 		       			                                  
           }

	        catch(Exception ex){
	            System.debug('The following exception has occurred: ' + ex);
	            return null;
	        }
		}
		return null;			      
    }

	public PageReference saveANDcreateItem(){

		if(refundQty > remainingQty){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Quantity submitted is greater than remaining items.'));
		}
		else if( pricePaid > remainingPrice){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Price submitted is greater than remaining price paid.'));
		}
		else{
			Refund_Item__c newRefundItem = new Refund_Item__c(hmr_CC_Order__c = orderId, CC_Order_Item__c = orderItemId, Price_paid_by_client__c = pricePaid, 
															hmr_Refund__c = refundId, hmr_Refund_Product_Quanity__c = refundQty, 
															hmr_Refund_Product_Quantity__c = refundQty, hmr_Refund_Product_SKU__c = selectedSKU);
   		 	insertRefundItem.add(newRefundItem);

    		try{           
        	 	if (refundQty != 0 && pricePaid != 0.00 && productName != 'SELECT SKU' && orderItemNumber != 'SELECT SKU') {
					Insert insertRefundItem; 						
					PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
					pageRef.setRedirect(true);
					return pageRef;							
	       		}
	       		else{
	       			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter values on all the fields.'));
	       			return null;
	       		}                        
       		}
        	catch(Exception ex){
            	System.debug('The following exception has occurred: ' + ex);
            	return null;
        	}
		}
		return null;	       
	}
}