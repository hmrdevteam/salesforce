/**
* Apex Content Template Controller for rest of the content on the Professionals Page
*
* @Date: 07/26/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_ProfessionalDtls_ContentTemplate extends cms.ContentTemplateController {
	global HMR_CMS_ProfessionalDtls_ContentTemplate(cms.CoreController controller){
        super();
    }
	global HMR_CMS_ProfessionalDtls_ContentTemplate() {}

	//method to render hero HTML markup, this is the override of CMS getHTML method for Apex Content Template
	global virtual override String getHTML(){
		String html = '';
		html += '<section class="hmr-page-section hmr-data-section bg-grey">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h2>Proven Effective</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="col-sm-6">' +
				                '<div class="hmr-data-card bg-green">' +
				                    '<div class="row">' +
				                        '<div class="italic first text-capitalize">Decision Free<span>&reg;</span> plan</div>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<h1 class="white">43-66 lbs.</h1>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<div class="hmr-allcaps hmr-data-content">At 12-26 Weeks<sup>1</sup></div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-sm-6">' +
				                '<div class="hmr-data-card second bg-lightblue">' +
				                    '<div class="row">' +
				                        '<div class="italic second text-capitalize">Healthy Solutions<span>&reg;</span> plan</div>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<h1 class="white">28-37 lbs.</h1>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<div class="hmr-allcaps hmr-data-content">At 12-26 Weeks<sup>2</sup></div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p class="text-11b">' +
				                '<sup>1</sup><a href="https://www.ncbi.nlm.nih.gov/pubmed/19631049" target="_blank">J Am Diet Assoc 2009; 109:1417-1421;</a> <a href="http://www.nature.com/ijo/journal/v31/n3/full/0803423a.html" target="_blank">Int J Obes 2007; 31:488-493;</a> <a href="https://www.ncbi.nlm.nih.gov/pubmed/16192259" target="_blank">J Am Coll Nutr 2005; 24:347-353.</a>' +
				                '<br/>' +
				                '<sup>2</sup><a href="https://www.ncbi.nlm.nih.gov/pubmed/19631049" target="_blank">J Am Diet Assoc 2009;109:1417-1421;</a> <a href="https://www.ncbi.nlm.nih.gov/pubmed/21904103" target="_blank">Postgrad Med 2011;123:205-213;</a> <a href="http://www.nature.com/ijo/journal/v31/n8/full/0803568a.html" target="_blank">Int J Obes 2007;31:1270-1276;</a> <a href="http://onlinelibrary.wiley.com/doi/10.1002/oby.20334/full" target="_blank">Obes 2013;21:1951-1959.</a>' +
				                '<br/>' +
				                'These studies were supported and/or sponsored by HMR, including financial support in some cases. Some of the investigators in some of the studies were employees of HMR and/or members of the HMR Medical Advisory Board.' +
				            '</p>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section hmr-intensive-intervention-section">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h2>Intensive Lifestyle-Change Intervention</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p>' +
								'"Meaningful weight loss" is more than just pounds lost. ' +
								'Helping your overweight population lose weight, become more active, and live a healthier lifestyle can be an effective, sustainable way to reduce medical risk factors and associated costs. ' +
								'Learning and practicing healthy lifestyle skills is critical for long-term weight and health management. ' +
								'It can also help reduce the risk factors associated with the development of heart disease, stroke, diabetes, cancer, and other expensive, chronic diseases. ' +
								'Healthy lifestyle changes - average American versus HMR participant:' +
							'</p>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="col-md-6 border">' +
				                '<h3 class="hmr-allcaps text-center">The Average American</h3>' +
				                '<div class="row">' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-darkblue">' +
				                            '<div class="row">' +
				                                '<h1 class="white">12.6</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Cups</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Fruits &amp; Vegetables per person per week' + '<sup>1</sup>' + '</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-darkblue">' +
				                            '<div class="row">' +
				                                '<h1 class="white">83</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Million</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">No Physical Activity during the last year</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-md-6 border">' +
				                '<h3 class="hmr-allcaps text-center">HMR PARTICIPANT*</h3>' +
				                '<div class="row">' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-purple">' +
				                            '<div class="row">' +
				                                '<h1 class="white">44.6</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Full Cups</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Fruits &amp; Vegetables Weekly average over 12 weeks</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-purple">' +
				                            '<div class="row">' +
				                                '<h1 class="white">2,134</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Kcals</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Physical Activity Weekly average over 12 weeks</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p class="text-11b">' +
				                '<sup>1</sup> State of the Plate: 2010 Study of America\'s Consumption of Fruits and Vegetables-The Produce for Better Health Foundation. ' +
								'Reported as 1.8 cups of fruits and vegetables per day. <sup>2</sup>PA Council-4/22/15.  ' +
								'<sup>3</sup>Donnelly JE, et al. Int J Obes 2007. Data self-reported.' +
								'<br />' +
				                '<sup>*</sup>Enrolled in Healthy Solutions at Home with phone support.' +
				            '</p>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section bg-typecover bg-healthoutcomes">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<div class="col-sm-4">' +
				                '<div class="section-right-black">' +
				                    '<h2 class="title">Health Outcomes</h2>' +
				                    '<div class="blurb text-left">' +
				                        '<p>' +
				                            'Weight loss is associated with favorable changes in medical risk factors such as blood pressure, lipids, and blood glucose, and the medications commonly used to treat these risk factors. ' +
											'The following are changes associated with weight loss for HMR clinic patients who: ' +
											'enrolled in HMR\'s clinic-based Decision-Free&reg; or Healthy Solutions&reg; ' +
											'programs at one of 43 U.S.-based HMR clinics, completed a baseline health risk assessment (HRA), and a follow-up HRA during the weight-maintenance phase of the program (July/August 2013). ' +
				                        '</p>' +
				                        '<p class="text-11b">' +
				                            'Poster T-2083 presented at Obesity Week 2014; Boston, MA' +
				                        '</p>' +
				                    '</div>' +
				                    '<div class="link-container">' +
				                        '<div class="hmr-allcaps-container">' +
				                            '<a class="hmr-allcaps" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">Request Information</a>' +
				                            '<a class="hmr-allcaps hmr-allcaps-carat" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">></a>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-sm-8 section-right-first visible-lg visible-md">' +
				            '</div>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section bg-typecover bg-paloaltochart">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<div class="col-sm-8 section-right-first visible-lg visible-md">' +
				            '</div>' +
				            '<div class="col-sm-4">' +
				                '<div class="section-right-black">' +
				                    '<h3 class="heading">Case Study</h3>' +
				                    '<h2 class="title">Palo Alto Medical Foundation</h2>' +
				                    '<div class="blurb text-left">' +
				                        '<p>' +
				                            'Data from 76 patients who enrolled in HMR\'s clinic-based Decision-Free&reg; or Healthy Solutions&reg; programs. ' +
											'Patients completed a baseline Health Risk Assessment (HRA) and a follow-up HRA during the weight-maintenance phase of the program at an average of 2.5 years follow-up. ' +
											'Patients were excluded if they did not have complete biometric values.' +
				                        '</p>' +
				                        '<p class="text-11b">' +
				                            '<span><sup>1</sup></span>' + '<span> Stifler LTP (HMR Founder), Treating Obesity and Preventing Costly Chronic Conditions. Group Practice Journal, April 15, 2011, 26-30</span>' +
				                            '<br/>' +
				                            '<sup>2</sup> Data on file.' +
				                        '</p>' +
				                    '</div>' +
				                    '<div class="link-container">' +
				                        '<div class="hmr-allcaps-container">' +
				                            '<a class="hmr-allcaps" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">Request Information</a>' +
				                            '<a class="hmr-allcaps hmr-allcaps-carat" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">></a>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section bg-typecover bg-univkentuckychart">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<div class="col-sm-4">' +
				                '<div class="section-right-black">' +
				                    '<h3 class="heading">Case Study</h3>' +
				                    '<h2 class="title">University of Kentucky Medical School</h2>' +
				                    '<div class="blurb text-left">' +
				                        '<p>' +
											'This was a retrospective comparison study (chart review) between patients with diabetes who went through the HMR clinic program (n=29) or a usual care group in an endocrine specialty clinic (n=26) who completed lifestyle education classes by a certified diabetes educator. ' +
											'Average 30-day estimated cost of insulin from baseline to six months from baseline to six months decreased $197.28 for HMR clinic patients, vs. an increase of $131.91 for control patients. ' +
											'Insulin costs were estimated based upon scheduled insulin used and cost information from Drugs.com' +
				                        '</p>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-sm-8 section-right-first visible-lg visible-md">' +
				            '</div>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section hmr-aurorahealth-section">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h3 class="heading">Case Study</h3>' +
				            '<h2>Aurora Health Care</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p>' +
								'Data from employees who enrolled in HMR\'s Healthy Solutions  at Home Program (n=76), and completed at least 10 weeks (February 2013 - September 2013). ' +
								'Average time in the program was 16 weeks, and included participants in Phase 1 (weight loss) and Phase 2 (maintenance). ' +
							'</p>' +
				            '<p>' +
								'<span class="hmr-allcaps">PARTICIPANT SATISFACTION<span>†</span></span> 92% very satisfied or satisfied with rate of weight loss' +
							'</p>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="col-md-6">' +
				                '<div class="row">' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-purple">' +
				                            '<div class="row">' +
				                                '<h1 class="white">-24.1</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">LBS</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Mean Weight Loss at 12 Weeks</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-lightblue">' +
				                            '<div class="row">' +
				                                '<h1 class="white">-3.9</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">BMI</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Average BMI Change at 12 Weeks</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-md-6">' +
				                '<div class="row">' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-green">' +
				                            '<div class="row">' +
				                                '<h1 class="white">39</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Full Cups</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Fruits &amp; Vegetables *average per week</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                    '<div class="col-xs-6">' +
				                        '<div class="hmr-data-card bg-coral">' +
				                            '<div class="row">' +
				                                '<h1 class="white">1,896</h1>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-allcaps hmr-data-content">Kcals</div>' +
				                            '</div>' +
				                            '<div class="row">' +
				                                '<div class="hmr-data-content">Physical Activity *average per week</div>' +
				                            '</div>' +
				                        '</div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p class="text-11b">' +
				                '<sup>1</sup>Confare AS, May SH, Facing the Obesity Epidemic. Group Practice Journal, April 2014;63:42-48<br />' +
				                '<sup>†</sup>Satisfaction data from 40 participants<br />' +
				                '<sup>*</sup>Self-reported data.' +
				            '</p>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section hmr-behavioral-intervention-section bg-darkblue white">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h2>U.S. Preventive Services Task Force recommends the following behavioral interventions for weight loss, 100% supported by the HMR Program</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="col-sm-6">' +
				                '<ul>' +
				                    '<li>Individual or group sessions</li>' +
				                    '<li>Setting weight-loss goals</li>' +
				                    '<li>Improving diet and nutrition</li>' +
				                    '<li>Physical activity</li>' +
				                '</ul>' +
				            '</div>' +
				            '<div class="col-sm-6">' +
				                '<ul>' +
				                    '<li>Addressing barriers to change</li>' +
				                    '<li>Self-monitoring</li>' +
				                    '<li>Strategies to maintain lifestyle changes</li>' +
				                '</ul>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p class="text-11b">' +
				                'Moyer VA. Screening for and Management of Obesity in Adults: U.S. Preventive<br />' +
				                'Services Task Force recommendation statement. Ann Intern Med, 2012; 157; 373-378' +
				            '</p>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="btn-container">' +
				                '<a class="btn btn-primary hmr-btn-purple hmr-btn-small" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">Request Information</a>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="hmr-allcaps contact-phone">Or Call 877.501.9257</div>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="hmr-page-section hmr-clinicalstudyhighlights-section">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h2>A Selection of Clinical Study Highlights</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<h3>Publications</h3>' +
				            '<div class="title">' +
				                'Obesity Epidemic: Driving Down Weight, Medications and Cost. AMGA Group Practice Journal, April 2015 pgs 18—19' +
				            '</div>' +
				            '<div class="article">' +
				                '<span class="hmr-allcaps">' +
				                    'Highlight:' +
				                '</span>' +
				                'A retrospective study of 984 patients with obesity (mean BMI of 44 kg/m2) at one HMR clinic demonstrated a 19% reduction in body weight after 52 weeks in the program. ' +
								'Weight loss was associated with decreased use of medications to treat diabetes, hypertension, and dyslipidemia, which was estimated to achieve a 54% reduction in annual medication costs relative to baseline. ' +
								'Weight loss most significantly impacted the use of diabetes medications, which was associated with an estimated 82% reduction in annual costs.' +
							'</div>' +
				            '<div class="title">' +
				                'Facing the Obesity Epidemic: Optimizing Employee Weight Management. AMGA Group Practice Journal, April 2014 pgs 42—48' +
				            '</div>' +
				            '<div class="article">' +
				                '<span class="hmr-allcaps">' +
				                    'Highlight:' +
				                '</span>' +
				                'Both HMR clinic and remote programs were effective at providing clinically significant weight loss of 45.2 and 24.1 pounds, respectively.' +
				                '<br />' +
				                '<p class="text-11b">' +
				                'These studies were supported and/or sponsored by HMR, including financial support in some cases. Some of the investigators in some of the studies were employees of HMR and/or members of the HMR Medical Advisory Board.' +
				                '</p>' +
				            '</div>' +
				            '<h3>' +
				                'Obesity Week Poster Presentations' +
				            '</h3>' +
				            '<div class="title">High Intensity Lifestyle Intervention and Use of Meal Replacements is Associated with Clinically Meaningful Weight Loss</div>' +
				            '<div class="reference">Poster T-2083-P, 04Nov2014, Boston, MA</div>' +
				            '<div class="article">' +
								'<span class="hmr-allcaps">Highlight:</span>' +
								'A retrospective study of 721 obese patients participating in an HMR clinic program showed a mean decrease in initial body weight of nearly 19 percent. ' +
								'The mean duration of program participation was approximately two years, and more than 80 percent of patients in the study had at least a 10 percent reduction of their initial body weight at follow-up. ' +
								'Weight loss in this study was associated with decreases in total cholesterol, triglycerides, fasting plasma glucose, systolic and diastolic blood pressure, and decreased use of medications used to treat diabetes, high blood pressure, and lipid disorders in some patients.' +
							'</div>' +
				            '<div class="title">Patients Who are Eligible for Bariatric Surgery Achieve Clinically Significant Weight Loss Maintenance and Biometric Changes Through Lifestyle Intervention Using Meal Replacements</div>' +
				            '<div class="reference">Poster T-2569-P, 04Nov2014, Boston, MA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>A retrospective analysis of 441 obese adults with a BMI ≥ 40 kg/m2 participating in an HMR clinic program achieved clinically significant weight loss. The mean duration of program participation was approximately two years, and nearly 85 percent of patients had a weight loss of greater than 10 percent of their initial body weight at follow-up. Weight loss in some patients in this study was associated' + 'with favorable biometric changes and a reduction in use of medications for treatment of diabetes, high blood pressure, or lipid disorders.</div>' +
				            '<div class="title">Clinical Outcomes in Patients with Diabetes Mellitus using a Medically Supervised Commercial Weight Reduction Program Compared to Standard Care in Endocrine Specialty Clinic</div>' +
				            '<div class="reference">Poster T-2585-P, 04Nov2014, Boston, MA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>HMR patients saw a reduction of total medication usage of 28 percent, with at least one medication discontinued in 80 percent of patients, and reduction in A1C levels (7.5%±2;bsl 8.3±1.9). Preliminary data shows that when compared to CDE-led diabetic education emphasizing lifestyle change, patients in an intensive lifestyle program like HMR utilizing weekly coaching, meal replacements and exercise, had '+ 'a 6.9 point reduction in BMI and achieved a similar A1C with greater reduction in medication use.</div>' +
				            '<div class="title">Behaviors that Predict Weight Loss Maintenance at One Year</div>' +
				            '<div class="reference">Poster T-495-P, 04Nov2013, Atlanta, GA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span> A retrospective analysis of 934 adults participating in an HMR clinic program found that the participants who lost more than 10 percent of their initial body weight were five times as likely to keep the weight off at the one-year mark. This supports previous research that greater initial weight loss increases the likelihood of keeping the weight off long-term.</div>' +
				            '<div class="title">A 12-Week Low-Calorie Diet Plus Behavioral Modification Acutely Improves Glycemic Parameters in Type 2 Diabetes Mellitus</div>' +
				            '<div class="reference">Poster T-552-P, 04Nov2013, Atlanta, GA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>A review of 129 people with type 2 diabetes shows a favorable impact on fasting plasma glucose levels, glycated hemoglobin levels, medication use, and overall glycemic profile when combining a low-calorie diet with a behavior modification program</div>' +
				            '<div class="title">Weight Management Program Participants Achieve Substantial Risk Factor and Medication Changes with Weight Loss</div>' +
				            '<div class="reference">Poster T-430-P, 04Nov2013, Atlanta, GA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>A study of an HMR program demonstrated an average weight loss of 43 pounds over 3.6 years and an average BMI decrease of 10.5 points amongst 1,256 HMR participants. Overall, 93 percent of patients had reductions in risk factors for chronic disease. All patients had an initial health risk appraisal (HRA) on program entry and a follow-up HRA in 2012.</div>' +
				            '<div class="title">Weight Loss in a Health Care System: A Population Health Approach</div>' +
				            '<div class="reference">Poster T-883-P, 04Nov2013, Atlanta, GA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>Aurora Health Care, a Wisconsin-based integrated system, offered healthcare premium incentives if employees participated in one of five weight-loss options. This study evaluated two programs offered by HMR: an in-clinic and remote phone program. Both programs showed high average weekly weight loss, high compliance, increased consumption of fruits and vegetables, and increases in physical activity.' + 'Aurora projects the implementation of these weight management programs, combined with their other wellness programs, will save more than an estimated $97 million over six years, enabling the system to curb costs and meet employee health objectives.</div>' +
				            '<div class="title">Survey of Weight Program Participants Indicates Improvement in Quality of Life</div>' +
				            '<div class="reference">Poster T-772-P, 04Nov2013, Atlanta, GA</div>' +
				            '<div class="article"><span class="hmr-allcaps">Highlight:</span>Nearly 71 percent of 706 HMR program participants reported that their overall health is greater or somewhat better than before participating in HMR programs. Top areas where improvement was noted include self-esteem, level of energy, and ability to perform daily tasks such as climbing stairs and walking. The study demonstrates the positive impact of weight loss on quality of life — an important yet' + 'overlooked weight-loss outcome.</div>' +
				        '</div>' +
				    '</div>' +
				'</section>';
		return html;
	}
}