/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class ChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}

    public PageReference resetPassword() {


        //Get the user Id to query the Contact record
        String userId = UserInfo.getUserId();
        System.debug('@@@@@@@@@@@@@@@@@@@ userId: ' + userId);
        Id clientId = [SELECT ContactId FROM User WHERE Id = :userId LIMIT 1].ContactId;

        //Create a list of Consent Agreement records with status of notified
        List<Consent_Agreement__c> cList = [SELECT Id, Consent_Type__c FROM Consent_Agreement__c WHERE Status__c = 'Notified' AND Contact__c = :clientId];

        System.debug('@@@@@@@@@@@@@@@@@@@ userId: ' + userId);
        System.debug('@@@@@@@@@@@@@@@@@@@ client: ' + clientId);
        System.debug('@@@@@@@@@@@@@@@@@@@ cList: ' + cList);


        //Create variable for the main Site.changepassword method
        //This will run immediately and allow us to then set the redirect
        PageReference output = Site.changePassword(newPassword, verifyNewPassword, oldpassword);


        //Make sure the Site.Changepassword has run
        if(output != null) {

            //check to see if the list of Consent Agreement records has any rows
            //set the redirect to the consent page if it does
            //set the redirect to the welcom page if it is empty
            if(cList.size() > 0) {

                PageReference consentPage = new PageReference(Site.getBaseUrl() + '/consent');
        		consentPage.setRedirect(false);

                return consentPage;
            }

            PageReference welcomePage = new PageReference(Site.getBaseUrl() + '/');
            welcomePage.setRedirect(false);

            return welcomePage;

        } return null;

    }

   	public ChangePasswordController() {}
}