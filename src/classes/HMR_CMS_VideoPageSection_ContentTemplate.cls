/**
* Apex Content Template Controller for Join the Conversation Component
*
* @Date: 07/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 07/17/2017
* @JIRA: HPRP-3958
*/

global virtual with sharing class HMR_CMS_VideoPageSection_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_VideoPageSection_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_VideoPageSection_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String BackgroundImageClass {
    	get {
    		return getPropertyWithDefault('BackgroundImageClass', 'bg-whychoosecoaching');
    	}
    }

	public String SectionTitle{
        get{
            return getPropertyWithDefault('SectionTitle', 'Section Title');
        }
    }

    public String VideoIconUrl{
    	get{
    		return getPropertyWithDefault('VideoIconUrl', 'https://www.myhmrprogram.com/ContentMedia/CoachValueProp/PlaySquare.png');
    	}
    }

    public String VideoTitle{
    	get{
    		return getPropertyWithDefault('VideoTitle', 'HMR Video');
    	}
    }

    public String VideoUrl{
        get{
            return getPropertyWithDefault('VideoUrl', 'https://www.youtube.com/embed/XAsg0b0mioA');
        }
    }

	global virtual override String getHTML(){
		String modalId = 'modal' + String.valueOf(Crypto.getRandomInteger());
		String html = '<section class="hmr-page-section bg-typecover ' + BackgroundImageClass + '" />' +
					        '<div class="container">' +
					            '<div class="media hidden-xs">' +
					              '<a class="media-left media-middle" href="#' + modalId + '" data-toggle="modal">' +
					                '<img class="pull-left play-square-icon" src="' + VideoIconUrl + '" />' +
					              '</a>' +
					              '<div class="media-body">' +
					                '<h2>' + SectionTitle + '</h2>' +
					              '</div>' +
					            '</div>' +
					            '<div class="row visible-xs">' +
					                '<div class="col-xs-12">' +
					                    '<h2>' + SectionTitle + '</h2>' +
					                '</div>' +
					            '</div>' +
					            '<div class="row visible-xs">' +
					                '<div class="col-xs-12 text-center">' +
					                    '<a href="#' + modalId + '" data-toggle="modal">' +
					                        '<img class="play-square-icon" src="' + VideoIconUrl + '" />' +
					                    '</a>' +
					                '</div>' +
					            '</div>' +
					        '</div>' +
					   '</section>';

		//Append Modal Dialog to Play Video
		html += '<div id="' + modalId + '" class="modal fade video">' +
			        '<div class="modal-dialog">' +
                        '<div>' +
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
                        '</div>' +
			            '<div>' +
                            '<div class="fluidMedia">' +
			                    '<iframe data-modal="' + modalId + '" class="videoModalFrame" src="' + VideoUrl + '" frameborder="0" allowfullscreen></iframe>' +
			                '</div>'+
			            '</div>' +
			        '</div>' +
			    '</div>';

		return html;
	}
}