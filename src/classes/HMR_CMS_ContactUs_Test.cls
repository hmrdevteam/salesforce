/**
* Test Class for ContactUsService Service Class 
*
* @Date: 2016-01-18
* @Author Utkarsh Goswami (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-1746
*/


@isTest
private class HMR_CMS_ContactUs_Test{


    static testMethod void firstTestMethod(){
        
        cc_dataFActory.setupTestUser();
        
        User u = [SELECT Id FROM User LIMIT 1];
        
        String userId = u.Id;
        
        Test.startTest();
        Map<String, String> testParams  = new Map<String, String>();
        testParams.put('action', 'getContact');
        testParams.put('firstname', 'TestFirstName');
        testParams.put('lastname', 'TestLastName');
        testParams.put('issue', 'getLogin');
        testParams.put('issue', 'Problem');
        testParams.put('subject', 'Test Subject');
        testParams.put('description', 'Test Description');
        testParams.put('userId', userId);
        
        
        HMR_CMS_ContactUs_ServiceInterface contactUsObject = new HMR_CMS_ContactUs_ServiceInterface();
        contactUsObject.executeRequest(testParams);
        System.debug('@@@@@@@@@@@@@@@@@@' + testParams);
        HMR_CMS_ContactUs_ServiceInterface.getType();
        testParams.put('action', 'invalid');
        contactUsObject.executeRequest(testParams);
        testParams = null;
        contactUsObject.executeRequest(testParams);
        Test.stopTest();
    }
}