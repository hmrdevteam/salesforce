/**
* Test Class Coverage of the HMR_CMS_PlansFtrInfoSection_Content Class
*   Created last minute for coverage - TODO expand tests
* @Date: 06/06/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/06/2017
* @JIRA: 
*/

@isTest
private  class HMR_CMS_PlansFtrInfoSection_Content_Test {
	@isTest 
	private static void testGetFooterContent(){
		HMR_CMS_PlansFtrInfoSection_ContentTmpl controller = new HMR_CMS_PlansFtrInfoSection_ContentTmpl();

		Test.startTest();

		String htmlResult = controller.getHTML();

		Test.stopTest();

		//Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
	}
}