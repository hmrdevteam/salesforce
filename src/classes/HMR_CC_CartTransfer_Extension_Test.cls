/**
* Test Class Coverage of the HMR_CC_CartTransfer_Extension Class
*
* @Date: 06/19/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/
@isTest
private class HMR_CC_CartTransfer_Extension_Test{
    @isTest
    private static void testTransferCartToUser() {
        cc_dataFactory.setupTestUser();
        ccrz__E_Cart__c cartRecord = cc_dataFactory.createCart();

        Test.startTest();

        ccrz.cc_RemoteActionResult remoteActionResult = HMR_CC_CartTransfer_Extension.transferCartToUser(cartRecord.ccrz__encryptedId__c, cc_dataFactory.testUser.Id);

        Test.stopTest();

        System.assert(remoteActionResult.success);
    }

    @isTest
    private static void testTransferInvalidCartToUser() {
        cc_dataFactory.setupTestUser();
        ccrz__E_Cart__c cartRecord = cc_dataFactory.createCart();

        Test.startTest();

        ccrz.cc_RemoteActionResult remoteActionResult = HMR_CC_CartTransfer_Extension.transferCartToUser(cartRecord.Id, cc_dataFactory.testUser.Id);

        Test.stopTest();

        System.assert(!remoteActionResult.success);
    }

    @isTest
    private static void testTransferGuestCart() {
        cc_dataFactory.setupTestGuest();
        ccrz__E_Cart__c cartRecord = cc_dataFactory.createCart();
        Cookie currentCartId = new Cookie('currCartId', cartRecord.ccrz__EncryptedId__c, null, -1, false);

        Test.setCurrentPageReference(Page.CC_Cart_PageIncl);
        ApexPages.currentPage().setCookies(new Cookie[]{currentCartId});

        Test.startTest();

        ccrz.cc_RemoteActionResult remoteActionResult = HMR_CC_CartTransfer_Extension.transferGuestCart(cc_dataFactory.testUser.Username);

        Test.stopTest();

        System.assert(remoteActionResult.success);
    }

    @isTest
    private static void testTransferCartToCurrentUser() {
        cc_dataFactory.setupTestUser();
        ccrz__E_Cart__c cartRecord = cc_dataFactory.createCart();

        Test.startTest();

        ccrz.cc_RemoteActionResult remoteActionResult = HMR_CC_CartTransfer_Extension.transferCart(cartRecord.ccrz__encryptedId__c);

        Test.stopTest();

        System.assert(!remoteActionResult.success);
    }

    @isTest
    private static void testCartCouponDelete() {
        cc_dataFactory.setupTestUser();
        ccrz__E_Cart__c cartRecord = cc_dataFactory.createCart();

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Dynamic Kit',
            Program__c = testProgram.Id,
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        ccrz__E_CartItem__c cartItem = cc_dataFactory.addCartItem(cartrecord, prod.Id, 1, 301.65);
        cartItem.ccrz__ProductType__c = 'Dynamic Kit';
        update cartItem;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'TestC',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            hmr_Program__c = testProgram.Id,
            ccrz__CouponCode__c = 'TestCoupon_P1',
            ccrz__CouponName__c = 'TestCoupon_P1',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;

        ccrz__E_CartCoupon__c cartCoupon = new ccrz__E_CartCoupon__c(
            ccrz__Coupon__c = childCCCoupon.Id,
            ccrz__Cart__c = cartRecord.Id
        );

        insert cartCoupon;


        Test.startTest();

            delete cartItem;

        Test.stopTest();

    }
}