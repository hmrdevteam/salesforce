/*****************************************************
 * Author: Zach Engman
 * Created Date: 04/03/2018
 * Created By: Zach Engman
 * Last Modified Date: 04/03/2018
 * Last Modified By:
 * Description: Test Coverage for the HMR_GroupSpreadsheet_Service Class
 * ****************************************************/
@isTest
private class HMR_GroupSpreadsheet_Service_Test {
	@testSetup
	private static void setupTestData(){
	  User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = HMR_TestFactory.createProgram();
      insert programRecord;

      Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
      insert programMembershipRecord;

      //create coach
      User coachUserRecord = HMR_TestFactory.createUser('HMR Standard User', true);
      insert coachUserRecord;

      Coach__c coachRecord = HMR_TestFactory.createCoach(coachUserRecord.Id);
      insert coachRecord;

      //setup the group/class and sessions
      Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
      insert classRecord;

      Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
      insert classMemberRecord;

      //create last 4 coaching sessions
      List<Coaching_Session__c> coachingSessionList;
      
      Coaching_Session__c coachingSessionRecord01 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord01.hmr_Class_Date__c = Date.today().addDays(-7);
      Coaching_Session__c coachingSessionRecord02 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord02.hmr_Class_Date__c = Date.today().addDays(-14);
      Coaching_Session__c coachingSessionRecord03 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord03.hmr_Class_Date__c = Date.today().addDays(-21);
       Coaching_Session__c coachingSessionRecord04 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord04.hmr_Class_Date__c = Date.today().addDays(-28);
      
      coachingSessionList = new List<Coaching_Session__c>{coachingSessionRecord01, coachingSessionRecord02, coachingSessionRecord03, coachingSessionRecord04};
      insert coachingSessionList;

      Session_Attendee__c sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord01.Id, classMemberRecord.Id, contactRecord.Id);
      sessionAttendeeRecord.hmr_Attendance__c = 'Attended';
      insert sessionAttendeeRecord;

      //create activity log records
      List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
      
      for(integer i = 0; i < 7; i++){
        ActivityLog__c activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);
        
        activityLogRecord.DateDataEnteredFor__c = Date.today().addDays(i * -1);
        activityLogRecord.Group_Member__c = classMemberRecord.Id;
        activityLogRecord.Session_Attendee__c = sessionAttendeeRecord.Id;

        activityLogList.add(activityLogRecord);
      }

      insert activityLogList;
	}

  @isTest
  private static void testGetGroupDataSpreadsheet(){
    HMR_GroupSpreadsheet_Service.GroupSpreadsheet groupSpreadsheetResult;
    Class__c classRecord = [SELECT Id FROM Class__c];

    Test.startTest();

	HMR_GroupSpreadsheet_Service groupSpreadsheetService = new HMR_GroupSpreadsheet_Service();

	groupSpreadsheetResult = groupSpreadsheetService.getGroupDataSpreadsheet(classRecord.Id);

	//cover all the read-only properties
	Decimal attendancePercent = groupSpreadsheetResult.weekSummaryList[0].attendancePercent;
	Decimal daysInTheBoxAverage = groupSpreadsheetResult.weekSummaryList[0].daysInTheBoxAverage;
	Decimal daysInTheBoxPercent = groupSpreadsheetResult.weekSummaryList[0].daysInTheBoxPercent;
	Decimal endOfWeekPercent = groupSpreadsheetResult.weekSummaryList[0].endOfWeekPercent;
	Decimal entreeAverage = groupSpreadsheetResult.weekSummaryList[0].entreeAverage;
	Decimal entreePercent = groupSpreadsheetResult.weekSummaryList[0].entreePercent;
	Decimal fruitVegAverage = groupSpreadsheetResult.weekSummaryList[0].fruitVegAverage;
	Decimal fruitVegPercent = groupSpreadsheetResult.weekSummaryList[0].fruitVegPercent;
	Decimal mealReplacementAverage = groupSpreadsheetResult.weekSummaryList[0].mealReplacementAverage;
	Decimal mealReplacementPercent = groupSpreadsheetResult.weekSummaryList[0].mealReplacementPercent;
	Integer mealReplacementTotal = groupSpreadsheetResult.weekSummaryList[0].mealReplacementTotal;
	Decimal met325TotalAverage = groupSpreadsheetResult.weekSummaryList[0].met325TotalAverage;
	Decimal met325TotalPercent = groupSpreadsheetResult.weekSummaryList[0].met325TotalPercent;
	Decimal metTIPercent = groupSpreadsheetResult.weekSummaryList[0].metTIPercent;
	Decimal oneWeekWeightChangeAverage = groupSpreadsheetResult.weekSummaryList[0].oneWeekWeightChangeAverage;
	Decimal percentThreeWeekExpectedWeightLoss = groupSpreadsheetResult.weekSummaryList[0].percentThreeWeekExpectedWeightLoss;
	String sessionDateFormatted = groupSpreadsheetResult.weekSummaryList[0].sessionDateFormatted;
	Decimal shakePercent = groupSpreadsheetResult.weekSummaryList[0].shakePercent;
	Decimal shakeAverage = groupSpreadsheetResult.weekSummaryList[0].shakeAverage;
	Decimal threeWeekWeightChangeAverage = groupSpreadsheetResult.weekSummaryList[0].threeWeekWeightChangeAverage;
	Decimal totalPhysicalActivityCaloriesAverage = groupSpreadsheetResult.weekSummaryList[0].totalPhysicalActivityCaloriesAverage;
	Decimal totalPhysicalActivityCaloriesPercent = groupSpreadsheetResult.weekSummaryList[0].totalPhysicalActivityCaloriesPercent;

	
    Test.stopTest();

    System.assert(groupSpreadsheetResult != null);
    System.assert(groupSpreadsheetResult.weekSummaryList.size() > 0);
  }
}