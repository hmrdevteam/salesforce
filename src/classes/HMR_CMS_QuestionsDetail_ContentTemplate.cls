global virtual with sharing class HMR_CMS_QuestionsDetail_ContentTemplate extends cms.ContentTemplateController{

    global HMR_CMS_QuestionsDetail_ContentTemplate(cms.createContentController cc){
        super(cc);
    }

    global HMR_CMS_QuestionsDetail_ContentTemplate(){
        super();
    }

        //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup
    	string returnMe = '' +

		'<div class="container-fluid">' +
			'<div class="row questions-detail">' +
				'<div class="question-bg question">' +
					'<div class="container comment">' +
						'<div class="col-xs-12 expand">' +
							'<div class="row">' +
								'<div class="col-xs-8 col-sm-offset-2 col-md-offset-1 col-sm-4 col-md-5 date text-left"> </div>' +
								'<div class="col-xs-4 col-sm-6 button text-right">' +
                                '</div>' +
                            '</div>' +
							'<div class="row">' +
							'<div class="col-xs-2 col-sm-2 col-md-1 avatar-info text-right questionImg">' +
								'</div>' +
								'<div class="col-xs-9 col-sm-4 col-md-5 user-detail text-left">' +
									'<div class="content">' +
										'<div class="name">' +
											'' +
										'</div>' +
											'<div class="location">' +
											'' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="row">' +
								'<div class="col-sm-10 col-sm-offset-2 col-md-offset-1 reply questionTitle">' +
								'' +
								'</div>' +
							'</div>' +
							'<div class="row">' +
								'<div class="col-sm-10 col-sm-offset-2 col-md-offset-1">' +
									'<div class="like">	' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="clearfix"></div>' +
								'<div class="best-answer text-right">' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +


					'<div class="bestAnswerZone">' +

					'</div>' +

					'<div class="commentsZone">' +

					'</div>' +

					'<div class="ckeditorZone">' +

					'</div>' +

					'<div class="container">' +
						'<div class="col-sm-6 hidden-xs">' +
							'<a class="hmr-link backToConvLink" href="#">< BACK TO CONVERSATIONS</a>' +
						'</div>' +

					'<div class="modalZone">' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';

    	return returnMe;
    }
}