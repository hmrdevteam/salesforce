/*****************************************************
 * Author: Utkarsh Goswami
 * Created Date: 02/19/2018
 * Created By:
 * Last Modified Date: 
 * Last Modified By:
 * Description: Test Coverage for the HMRMobGoal REST Service
 * ****************************************************/
@isTest
private class HMRMobGoalTest {
  @testSetup
    private static void setupData(){
        User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;

        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;

        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;
        
        Account testAcc = new Account(Name = 'test');
        
        Contact testContact = new Contact( lastName = 'testcon',
                                            firstName = 'test',
                                            AccountId = testAcc.Id,
                                            Email = 'test@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000000'
                                    );
        insert testContact;
        
        
        Program_Membership__c programMembershipRecord2 = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = testContact.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord2;
        
        Contact testContact2 = new Contact( lastName = 'testcon2',
                                            firstName = 'test2',
                                            AccountId = testAcc.Id,
                                            Email = 'test2@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000002'
                                    );
        insert testContact2;
        
        Goal__c testGoal = new Goal__c(
                                        Contact__c = testContact.Id,
                                        FocusArea__c =  'Shakes',
                                        GoalAmount__c = 120,
                                        WeekStartDate__c = Date.today(),
                                        ProgramMembership__c = programMembershipRecord.Id
                                    );
        insert testGoal;
        
        Goal__c testGoal2 = new Goal__c(
                                        Contact__c = testContact2.Id,
                                        FocusArea__c =  'Shakes',
                                        ProgramMembership__c = programMembershipRecord.Id
                                    );
        insert testGoal2;
    }
    
    @isTest
    private static void testGetMethod(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/goal/';
        request.params.put('communityUserId', '000000000000000000');
        request.params.put('goalDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobGoal.doGet();

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
    }
    
    
    @isTest
    private static void testGetMethodNullDate(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/goal/';
        request.params.put('communityUserId', '000000000000000000');
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobGoal.doGet();

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
    }
    
    
     @isTest
    private static void testGoalPostMethod(){
        RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/goal/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;
      
      HMRMobGoal.MobGoalDTO mobileGoalDTO;

      Test.startTest();

          mobileGoalDTO = HMRMobGoal.getGoal('000000000000000000', Date.today());
          HMRMobGoal.doPost(mobileGoalDTO);

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
     
 }
 
  @isTest
    private static void testGoalPostMethodNegetive(){
        RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/goal/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;
      
   //   HMRMobGoal.MobGoalDTO mobileGoalDTO;

      Test.startTest();

       //   mobileGoalDTO = HMRMobGoal.getGoal('000000000000000000', Date.today());
          HMRMobGoal.doPost( new HMRMobGoal.MobGoalDTO(new Goal__c()));

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
     
 }
}