/**
* 
*
* @Date: 05/25/2017
* @Author Utkarsh Goswami (Mindtree)
* @
* @Class : HMR_CC_StoredPayment_Selector
*/

@isTest
private class HMR_CC_StoredPayment_Selector_Test{

    @isTest
    private static void testMethodForGetById(){
            cc_DataFactory dataFactory = new cc_DataFactory();
            cc_DataFactory.setupTestUser();
            Contact contactRecord = cc_DataFactory.testUser.Contact;
            
            ccrz__E_ContactAddr__c contactAdr = cc_DataFactory.getContactAddress(); 
            contactAdr.OwnerId = cc_DataFactory.testUser.Id;
            
            insert contactAdr;        
            insert new ccrz__E_StoredPayment__c(Name = 'teststor',ccrz__Token__c = 'testtoken', ccrz__DisplayName__c = 'testname',HMR_Contact_Address__c = contactAdr.Id);
            
            Test.startTest();

            List<ccrz__E_StoredPayment__c> storedPaymentList =  HMR_CC_StoredPayment_Selector.getByContactId(String.valueOf(contactRecord.Id).left(15));
            
            Test.stopTest();
            
            System.assert(storedPaymentList.size()>0);
        }
       
        
        @isTest
        private static void testMethodForGetUserId(){
        
            Id p = [select id from profile where name='HMR Customer Community User'].id;
            Account testAcc = new Account(Name = 'testAcc');
            insert testAcc; 
           
            Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
            insert con;  
                      
            User userTest = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert userTest;
            
            ccrz__E_StoredPayment__c storedPay = new ccrz__E_StoredPayment__c(Name = 'teststor',ccrz__Token__c = 'testtoken',
                                                 ccrz__DisplayName__c = 'testname', ccrz__User__c  = userTest.id);
            insert storedPay;
        
            List<ccrz__E_StoredPayment__c> storedPaymentList = HMR_CC_StoredPayment_Selector.getByUserId(userTest.id);
            
            System.assert(storedPaymentList.size()>0);
        }
        
        @isTest
        private static void testGetByToken(){
        
            ccrz__E_StoredPayment__c storedPay = new ccrz__E_StoredPayment__c(Name = 'teststor',ccrz__Token__c = 'testtoken', ccrz__DisplayName__c = 'testname');
            insert storedPay;
        
            ccrz__E_StoredPayment__c tokenSearch = HMR_CC_StoredPayment_Selector.getByToken('testtoken');
        
            System.assert(!String.isBlank(tokenSearch.ccrz__DisplayName__c));
        }
}