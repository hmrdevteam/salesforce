/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 21 June 2017
 * Created By: Nathan Anderson
 * Last Modified Date: 6/23/2017
 * Last Modified By: Ali Pierre (HMR)
 * Description:
 Invoked by Process Builder, this class calculates the total Lifetime Order amounts
 for contacts and Program members and updates those records.
 * ****************************************************/


public class HMR_CC_LifetimeOrdersCalculator {
	public HMR_CC_LifetimeOrdersCalculator() {

	}

	@InvocableMethod
	public static void calculateOrderTotals(List<Id> orderIds) {

		//From the list of updated order Ids passed from Process Builder, get the records including the Contacts and PMs
		List<ccrz__E_Order__c> updatedOrders = [SELECT Id, ccrz__TotalAmount__c, ccrz__Contact__c, hmr_Program_Membership__c
										FROM ccrz__E_Order__c
										WHERE Id IN :orderIds AND ccrz__OrderStatus__c = 'Completed'];

		//We need to declare Sets and assign Ids from the Orders that were updated, so we can find all of the other related Orders
		Set<Id> contactIds = new Set<Id>();
		Set<Id> pmIds = new Set<Id>();

		for(ccrz__E_Order__c o : updatedOrders){
			if(o.ccrz__Contact__c != null) {
				contactIds.add(o.ccrz__Contact__c);
			}
			if(o.hmr_Program_Membership__c != null) {
				pmIds.add(o.hmr_Program_Membership__c);
			}
		}

		//Get the full list of related orders for the Contacts
		List<ccrz__E_Order__c> orders = [SELECT Id, Name, ccrz__TotalAmount__c, ccrz__Contact__c, hmr_Program_Membership__c, hmr_Total_Amount_Refunded__c, hmr_Fully_Refunded__c, ccrz__OrderDate__c
										FROM ccrz__E_Order__c
										WHERE ccrz__Contact__c IN :contactIds AND ccrz__OrderStatus__c = 'Completed'];

		//We need to declare Sets and assign Ids from the Orders related to Contacts being processed
		Set<Id> contactOrderIds = new Set<Id>();

		for(ccrz__E_Order__c o : orders){
			if(o.ccrz__Contact__c != null) {
				contactOrderIds.add(o.Id);
			}
		}

		//Get the full list of related Refunds for the Orders
		List<Refund__c> refunds = [SELECT Id, Name, hmr_Order_Number__c,  hmr_Return_Status__c, hmr_Total_Refund_Amount__c
											FROM Refund__c
											WHERE hmr_Order_Number__c IN :contactOrderIds AND hmr_Return_Status__c = 'Approved'];

		//Get the full Contact records
		List<Contact> contacts = [SELECT Id, hmr_Total_Sales__c, hmr_Lifetime_Orders__c, hmr_Last_Order_Date__c FROM Contact WHERE Id IN :contactIds];
		//Get the full Program Membership records
		List<Program_Membership__c> members = [SELECT Id, Total_Order_Amount__c, Total_Orders__c FROM Program_Membership__c WHERE Id IN :pmIds];

		//New list to hold contacts to update
		List<Contact> contactsToUpdate = new List<Contact>();

		Decimal totalRefundAmount;
		//Loop through the contacts in the list and re-calculate the total number of orders and the total lifetime dollars
		if(contacts.size() > 0) {
			for(Contact c : contacts) {
				c.hmr_Total_Sales__c = 0;
				c.hmr_Lifetime_Orders__c = 0;
				Datetime dt = c.hmr_Last_Order_Date__c;

				for(ccrz__E_Order__c o : orders) {
					totalRefundAmount = 0;
					if(o.ccrz__Contact__c == c.Id) {
						if(o.ccrz__OrderDate__c != null && (o.ccrz__OrderDate__c > dt || dt == null)){
							dt = Datetime.newinstance(o.ccrz__OrderDate__c.year(), o.ccrz__OrderDate__c.month(), o.ccrz__OrderDate__c.day());
						}								
						for(Refund__c r : refunds){
							if(r.hmr_Order_Number__c == o.Id){
								totalRefundAmount += r.hmr_Total_Refund_Amount__c;
							}
						}
						//subtract refund amounts from each order total to calculate total sales accurately
						c.hmr_Total_Sales__c += (o.ccrz__TotalAmount__c - totalRefundAmount);
						//Increment total orders count only if order has not been fully refunded
						System.debug('Order Number '+ o.Name +'Total amt ' + o.ccrz__TotalAmount__c + '- totalRefundAmount ' + totalRefundAmount);
						if(o.ccrz__TotalAmount__c != totalRefundAmount){
							c.hmr_Lifetime_Orders__c += 1;
						}						
					}
				}
				c.hmr_Last_Order_Date__c = dt;
				//add the contact to list to update
				contactsToUpdate.add(c);
			}
		}
		//if there are contacts in the list, update the records
		if(contactsToUpdate.size() > 0) {
			try {
				update contactsToUpdate;
			} catch(Exception e) {
                System.debug('Exception in Calculate Contact Order Totals: ' + e);
            }

		}
		//New list to hold Program membership records to update
		List<Program_Membership__c> membersToUpdate = new List<Program_Membership__c>();

		//Loop through the PMs in the list and re-calculate the total number of orders and the total lifetime dollars
		if(members.size() > 0) {
			for(Program_Membership__c p : members) {
				p.Total_Order_Amount__c = 0;
				p.Total_Orders__c = 0;

				for(ccrz__E_Order__c o : orders) {
					totalRefundAmount = 0;
					for(Refund__c r : refunds){
						if(r.hmr_Order_Number__c == o.Id){
							totalRefundAmount += r.hmr_Total_Refund_Amount__c;
						}
					}
					if(o.hmr_Program_Membership__c == p.Id) {
						//subtract refund amounts from each order total to calculate total sales accurately
						p.Total_Order_Amount__c += (o.ccrz__TotalAmount__c - totalRefundAmount);
						//Increment total orders count only if order has not been fully refunded
						if(o.ccrz__TotalAmount__c != totalRefundAmount){
							p.Total_Orders__c += 1;
						}						
					}
				}
				//add the PM to list to update
				membersToUpdate.add(p);
			}
		}
		//if there are PMs in the list, update the records
		if(membersToUpdate.size() > 0) {
			try {
				update membersToUpdate;
			} catch(Exception e) {
				System.debug('Exception in Calculate PM Order Totals: ' + e);
			}

		}

	}

}