/**
* Apex Content Template Controller for the Success Stories Detail Component
*
* @Date: 08/04/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 08/04/2017
* @JIRA: HPRP-4014
*/
global virtual  class HMR_CMS_SSUserDetail_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SSUserDetail_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SSUserDetail_ContentTemplate() {}

    global virtual override String getHTML(){
    	String html = '';
  		String articleName = ApexPages.currentPage().getParameters().get('name');

  		if(!String.isBlank(articleName))
    		articleName = String.escapeSingleQuotes(articleName);

    	if(!String.isBlank(articleName)){
    		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    		HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);

    		if(articleRecord != null){
    			html = '<div class="user-ss">' +
						       '<div class="container">' +
							        '<div class="row">' +
								          '<div class="col-xs-12 quote">' +
								                articleRecord.LongTeaser +
								          '</div>' +
									      '<div class="col-xs-12 bio">' +
									            articleRecord.StoryBody +
									      '</div>' +
									'</div>' +
								'</div>' +
						'</div>';
    		}
    	}

    	return html;
    }
}