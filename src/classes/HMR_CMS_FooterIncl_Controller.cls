public class HMR_CMS_FooterIncl_Controller {
	public String defaultProductCategory { get; set; }
	public HMR_CMS_FooterIncl_Controller() {
		try {
			defaultProductCategory = getHMRProductCategory();
		}
		catch(Exception ex) {
			System.debug('Exception in HMR_CMS_FooterIncl_Controller');
			System.debug(ex.getMessage());
		}
	}
	public String getHMRProductCategory(){
		List<ccrz__E_Category__c> categoryList = [SELECT Id FROM ccrz__E_Category__c WHERE Name = 'HMR Products'];
		return categoryList.size() > 0 ? categoryList[0].Id : '';
	}
}