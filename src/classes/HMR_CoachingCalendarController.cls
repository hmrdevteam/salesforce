public with sharing class HMR_CoachingCalendarController {

    public list<calEvent> events {get;set;}

    //The calendar plugin is expecting dates is a certain format. We can use this string to get it formated correctly
    static String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';


    //public variables
    public string coachId {get;set;}
    public Boolean currentUserIsCoach {get;set;}
    public String currentUserId {get;set;}
    public String programId {get;set;}

    public HMR_CoachingCalendarController() {

        User currentUser = [SELECT Id, hmr_IsCoach__c FROM User WHERE Id=:UserInfo.getUserId()];

        coachId = Apexpages.currentpage().getparameters().get('coachId');

        if(coachId == null || coachId == '') {
            if(currentUser.hmr_IsCoach__c == true) {

                currentUserIsCoach = true;
                List<Coach__c> getCoachId = [SELECT Id FROM Coach__c WHERE hmr_Coach__c =:currentUser.Id];

                for(Coach__c coach : getCoachId) {
                  currentUserId = coach.id;
                }
                coachId = currentUserId;
            }
        }

        programId = Apexpages.currentpage().getparameters().get('programId');

        if(programId == null || programId == '') {
            programId = [SELECT Id FROM Program__c WHERE Name = 'P1 Healthy Solutions'].Id;
        }

        pageLoad();
    }

    public PageReference pageLoad() {
        events = new list<calEvent>();

        for(Coaching_Session__c session : [SELECT Id, Name, hmr_Class_Date__c,hmr_Start_Time__c, hmr_End_Time__c,hmr_Coach_Name__c,hmr_Conference_call_number__c, hmr_Host_Code__c,hmr_Participant_code__c
                                            FROM Coaching_Session__c WHERE hmr_Coach__c = :coachId And hmr_Class__r.Program__c = :programId
                                            AND hmr_Start_Time__c != null AND hmr_End_Time__c != null]){

             Datetime startDT=DateTime.now();
             Datetime endDT=DateTime.now();

             //Chekc if date & time are not null
             if(session.hmr_Class_Date__c != null && session.hmr_Start_Time__c != null){

                 //Covert date & time from string to datetime instance
                 startDT = DateTime.parse( session.hmr_Class_Date__c.month()+'/'+session.hmr_Class_Date__c.day()+'/'+session.hmr_Class_Date__c.year()+' '+session.hmr_Start_Time__c);
                 endDT = DateTime.parse( session.hmr_Class_Date__c.month()+'/'+session.hmr_Class_Date__c.day()+'/'+session.hmr_Class_Date__c.year()+' '+session.hmr_End_Time__c);

             }

            //Prepare Event object to disply on calendar
            calEvent myEvent = new calEvent();
               myEvent.title = session.Name+'  '+ session.hmr_Conference_call_number__c;
               myEvent.allDay = false;
               myEvent.startString = startDT.format(dtFormat);
               myEvent.endString = endDT.format(dtFormat);
               myEvent.url = '/' + session.Id;
               //myEvent.className = 'event-personal';
               events.add(myEvent);
        }

        return null;
    }

    //Class to hold calendar event data
    public class calEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
    }

    //get the coach options for coach list
    public List<SelectOption> getCoachList() {

        List<Coach__c> coachObjectList=[SELECT Name, hmr_Coach_First_Name__c,hmr_Coach_Last_Name__c,Id FROM Coach__c WHERE hmr_Active__c = true];
        List<SelectOption> options = new List<SelectOption>();

        for(Coach__c coach : coachObjectList){
            options.add(new SelectOption(coach.Id,coach.hmr_Coach_First_Name__c+' '+coach.hmr_Coach_Last_Name__c));
        }

        return options;
    }

    //get the coach options for coach list
    public List<SelectOption> getProgramList() {

        List<Program__c> programList=[SELECT Id, Name FROM Program__c WHERE hmr_Has_Phone_Coaching__c = true];
        List<SelectOption> options = new List<SelectOption>();

        for(Program__c program : programList){
            options.add(new SelectOption(program.Id,program.Name));
        }

        return options;
    }
}