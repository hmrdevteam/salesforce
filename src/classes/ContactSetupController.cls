/*
* @Date: 2017-02-12
* @Author Nathan Anderson (M360)
* @Modified:
* @JIRA: Issue https://reside.jira.com/browse/HPRP-2315
*/

public class ContactSetupController {

	public Contact c {get; set;}
	public Boolean createUser {get; set;}
	public String selectedRType {get; set;}
	public String eCode {get; set;}

	//create variable and getter method to use in page rerender when user selects the Employer type
	Boolean employerFlag;
	public Boolean getEmployerFlag() {
		return employerFlag;
	}
	Boolean empTypeFlag;
	public Boolean getEmpTypeFlag() {
		return empTypeFlag;
	}

	public ContactSetupController() {

		//Initialize new Contact record
		c = new Contact();

		//default the create user flag to true
		createUser = true;
	}

	////This setter method updates the flag used in the page rerender to display the Employer Account picklist
	//public PageReference setEmployerFlag() {

	//	if(selectedRType == 'Employer') {
	//		employerFlag = true;
	//	} else {
	//		employerFlag = false;
	//	}
	//	return null;
	//}

	////This setter method updates the flag used in the page rerender to display the Employer Account picklist
	//public PageReference setEmpTypeFlag() {

	//	if(selectedRType == 'HMR Employee') {
	//		empTypeFlag = true;
	//	} else {
	//		empTypeFlag = false;
	//	}
	//	return null;
	//}

	public PageReference setTypeFlag() {

		if(selectedRType == 'Employer') {
			employerFlag = true;
		} else {
			employerFlag = false;
		}

		if(selectedRType == 'HMR Employee') {
			empTypeFlag = true;
		} else {
			empTypeFlag = false;
		}
		return null;
	}

	//Construct picklist for Record Types
	public List<SelectOption> getAccountTypes() {

		List<SelectOption> options = new List<SelectOption>();

		options.add(new SelectOption('', '--Select Customer Type--'));
		options.add(new SelectOption('Consumer', 'Consumer'));
		options.add(new SelectOption('Employer', 'Employer Group'));
		options.add(new SelectOption('HMR Employee', 'HMR Employee'));

		return options;

	}

	//construct list of active Corporate Accounts
	public List<SelectOption> getCorporateAccounts() {

		List<SelectOption> options = new List<SelectOption>();
		List<Account> accounts = [SELECT Id, Name, hmr_Employer_Account_ID__c, hmr_Active__c FROM Account
								WHERE hmr_Active__c = true AND hmr_Employer_Account_ID__c != null
								ORDER BY Name ASC];

		options.add(new SelectOption('', '--Select Employer Account--'));
		for(Account acct : accounts) {
			options.add(new SelectOption(acct.Id, acct.Name + ' - ' + acct.hmr_Employer_Account_ID__c));
		}

		return options;
	}


	@RemoteAction
	public static List<HMR_Coupon__c> reminderForAccount(Id accountToSearch){

		Id accountToSearchPrivate = accountToSearch;
		String newReminders = '';
		List<HMR_Coupon__c> valuetoReturn = new List<HMR_Coupon__c>();
		if(accountToSearchPrivate != null) {

			valuetoReturn = [SELECT Id, Name, hmr_Description__c FROM HMR_Coupon__c WHERE Account__c = :accountToSearchPrivate];


		}else{
			newReminders = 'variable not found';
		}

		return valuetoReturn;

	}

	//This is the main method to collect all of the necessary data for Account, Account Group, Contact and User
	//Checks the selected Client Type and finds or creates the appropriate records
	public PageReference registerUser() {

		//Mark the place in time to return to if any of the operations fail, to preserve data quality
		Savepoint sp = Database.setSavepoint();

		//If an exisiting Lead has a matching email address, convert this lead into a contact
		List<Lead> existingLeads = new List<Lead>([SELECT Id, Account__c FROM Lead WHERE Email = :c.Email LIMIT 1]);
		if(!existingLeads.isEmpty()){

			Lead existingLead = existingLeads[0];

			//update the contact so we don't lose the values on the form first
			try {
				if(existingLead.Account__c != null){
					c.AccountId = existingLead.Account__c;
				} else {
					Account a = new Account();
					//get the PortalAccount Account Group Id
					Id agId = [SELECT Id FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount'].Id;
					//get the Customer Account Record Type
					Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;

					//Create the Account for the Client, assign the PortalAccount Group
					a.Name = c.FirstName + ' ' + c.LastName;
					a.ccrz__E_AccountGroup__c = agId;
					a.RecordTypeId = rtype;

					insert a;

					c.AccountId = a.Id;
				}

				insert c;
			}
			catch(Exception e) {
				//add error message to the Page
				ApexPages.addMessages(e);
				//roll back any completed operations upon failure to the previously marked place in time
				Database.rollback(sp);
				//reset the contact record to maintain the form entries but clear the Id
				this.c = this.c.clone();
			}

			Database.LeadConvert lc = new Database.LeadConvert();
			lc.setLeadId(existingLead.Id);
			// if(existingLead.Account__c != null){
			// 	lc.setAccountId(existingLead.Account__c);
			// }
			lc.setAccountId(c.AccountId);
			lc.setContactId(c.Id);

			LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
			lc.setConvertedStatus(convertStatus.MasterLabel);
			lc.setDoNotCreateOpportunity(TRUE);

			Database.LeadConvertResult lcr = Database.convertLead(lc);

			// if(lcr.isSuccess()){
			// 	c = [SELECT Id, FirstName, LastName, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode,
			// 				Phone, hmr_DOB_Declined__c, hmr_Date_of_Birth__c, hmr_Contact_Original_Source__c, hmr_Contact_Original_Source_Detail__c
			// 	 			FROM Contact WHERE Id = :lcr.getContactId()];
			// }

		} else {

			//Create new Account instance so we can either create a consumer type Account or
			//query for the correct Corporate or HMR Employee Account
			Account a = new Account();

			if(selectedRType == 'Consumer') {

				List<Zip_Code_Assignment__c> zipCodeAssigment = [SELECT ID, Name, Account__c FROM Zip_Code_Assignment__c WHERE Name = :c.MailingPostalCode AND Account__c <> null  AND Account__r.hmr_Active__c = TRUE AND Account__r.hmr_Referral__c != 'DNR' LIMIT 1];

				//get the PortalAccount Account Group Id
				Id agId = [SELECT Id FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount'].Id;
				//get the Customer Account Record Type
				Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;

				//Check to see if there is a matching zip code assignment record, and set the account to the matching account
				if(zipCodeAssigment.size() > 0) {
					a = [SELECT Id, Name FROM Account WHERE Id = :zipCodeAssigment[0].Account__c];
					c.Patient_Type__c = 'Clinic Patient';
				} else {
					//Create the consumer Account for the Client, assign the PortalAccount Group
					a.Name = c.FirstName + ' ' + c.LastName;
					a.ccrz__E_AccountGroup__c = agId;
					a.RecordTypeId = rtype;

					insert a;
				}

			}

			if(selectedRType == 'Employer') {
				//if(eCode != null && eCode != '') {
					//Query for the correct Employer Account for the Client
					try {
						a = [SELECT Id FROM Account WHERE Id = :eCode];
						c.Patient_Type__c = 'Employer Group Member';
					} catch(Exception e) {
						ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid Employer Group for this Client type');
						ApexPages.addMessage(myMsg);
					}
				//}
			}

			if(selectedRType == 'HMR Employee') {

				//Query for the correct Employer Account for the Client
				a = [SELECT Id FROM Account WHERE Name = 'HMR Employees'];

			}

			//Assign the new contact to the correct Account
			c.AccountId = a.Id;

			try {

				insert c;

			} catch(Exception e) {
				//add error message to the Page
				ApexPages.addMessages(e);
				//roll back any completed operations upon failure to the previously marked place in time
				Database.rollback(sp);
				//reset the contact record to maintain the form entries but clear the Id
				this.c = this.c.clone();
			}

		}

		try {

			//Get the Community User Profile, assign to String
			Profile p = [SELECT Id FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];
			String profileId = p.Id;

			//If Create User checkbox is marked,
			//Set up User with entered values and enable for the Community
			if(createUser == true) {

				User u = new User();
		        u.Username = c.Email;
		        u.Email = c.Email;
		        u.FirstName = c.FirstName;
		        u.LastName = c.LastName;
		        u.ProfileId = profileId;
				String alias = c.FirstName + c.LastName;
				u.Alias = alias.left(8);
				u.CommunityNickname = c.Email;
				u.TimeZoneSidKey = 'America/New_York';
				u.LocaleSidKey = 'en_US';
				u.EmailEncodingKey = 'ISO-8859-1';
				u.LanguageLocaleKey = 'en_US';
				u.ContactId = c.Id;

				insert u;

				//Add Site Viewer Permission Set to the new User
				assignPermissionSet(u.Id);

			}

			//Set owner if community user
			Id uId;
			if(createUser == true) {
				uId = [SELECT Id FROM User WHERE Username = :c.Email LIMIT 1].Id;

				//Add CC Contact Address for Contact
				if(c.MailingStreet != null && c.MailingStreet != '' &&
				   c.MailingCity != null && c.MailingCity != '' &&
				   c.MailingState != null && c.MailingState != '' &&
				   c.MailingPostalCode != null && c.MailingPostalCode != '') {

					ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c();
					addr.ccrz__FirstName__c = c.FirstName;
					addr.ccrz__LastName__c = c.LastName;
					addr.ccrz__Email__c = c.Email;
					addr.ccrz__AddressFirstline__c = c.MailingStreet;
					addr.ccrz__City__c = c.MailingCity;
					addr.ccrz__StateISOCode__c = c.MailingState;
					addr.ccrz__PostalCode__c = c.MailingPostalCode;
					addr.ccrz__Country__c = 'United States';
					addr.ccrz__CountryISOCode__c = 'US';
					addr.OwnerId = uId;

					insert addr;

					//Add Billing CC Account Address Book for Contact
					ccrz__E_AccountAddressBook__c ab = new ccrz__E_AccountAddressBook__c();
					ab.ccrz__E_ContactAddress__c = addr.Id;
					ab.ccrz__Account__c = c.AccountId;
					ab.ccrz__AccountId__c = c.AccountId;
					ab.ccrz__Default__c = true;
					ab.ccrz__AddressType__c = 'Billing';
					ab.OwnerId = uId;
					ab.ccrz__Owner__c = uId;

					//Add Shipping CC Account Address Book for Contact
					ccrz__E_AccountAddressBook__c abs = new ccrz__E_AccountAddressBook__c();
					abs.ccrz__E_ContactAddress__c = addr.Id;
					abs.ccrz__Account__c = c.AccountId;
					abs.ccrz__AccountId__c = c.AccountId;
					abs.ccrz__Default__c = true;
					abs.ccrz__AddressType__c = 'Shipping';
					abs.OwnerId = uId;
					abs.ccrz__Owner__c = uId;

					insert ab;
					insert abs;
				}
			}



			//redirect back to the newly created Contact record
			PageReference page = new PageReference('/'+c.Id);
	        page.setRedirect(true);

			return page;

		} catch(Exception e) {
			//add error message to the Page
			ApexPages.addMessages(e);
			//roll back any completed operations upon failure to the previously marked place in time
			Database.rollback(sp);

			this.c = this.c.clone();
		}

		return null;
    }

	@future
	public static void assignPermissionSet(Id userId) {

		//Add Site Viewer Permission Set to the new User
		PermissionSetAssignment psa = new PermissionSetAssignment();
		Id permSet = [SELECT Id FROM PermissionSet WHERE Name = 'ocms_autogen_SiteViewer'].Id;
		psa.PermissionSetId = permSet;
		psa.AssigneeId = userId;

		try{

			insert psa;

		} catch(Exception e) {

			System.Debug('Exception in Permission Set Assignment: ' + e);

		}

	}

	public PageReference cancel() {

		//redirect back to the Contact Tab
		PageReference page = new PageReference('/003');
		page.setRedirect(true);

		return page;
	}
}