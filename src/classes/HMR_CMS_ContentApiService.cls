/**
* Apex Service Class for the CMS Content Api Service
*
* @Date: 2018-01-23
* @Author: Zach Engman
* @Modified: 
* @JIRA: 
*/
public with sharing class HMR_CMS_ContentApiService extends HMR_CMS_API {
	private ContentAPIRequest contentAPIRequest = new ContentAPIRequest();
    
	public HMR_CMS_ContentApiService(){}
	public HMR_CMS_ContentApiService(string siteName, string apiVersion) {
		this.siteName = siteName;
		this.apiVersion = apiVersion;
	}

	/**
     * Retrieves content through the Orchestra CMS Content Service API by object/origin Id
     *
     * @param objectId - the object or origin Id of the content
     */
	public JSONMessage.APIResponse getContentByObjectId(string objectId){
		JSONMessage.APIResponse apiResponse;
		String response;

		this.contentAPIRequest.objectId = objectId;

		Map<String, String> parameters = new Map<String, String>{
			 'contentRequest' => JSON.serialize(this.contentAPIRequest)
			,'action' => 'getContent'
			,'service' => 'ContentAPI'
			,'sname' => this.siteName
			,'application' => 'runtime'
			,'apiVersion' => this.apiVersion
		};

        try {
        	response = cms.ServiceEndpoint.doActionApex(parameters);
        	apiResponse = (JSONMessage.APIResponse)JSON.deserialize(response, JSONMessage.APIResponse.class);
          
	        if (!apiResponse.isSuccess)
	            throw new ContentApiServiceException('Could not retrieve content for this id');
        }
        catch(Exception e) {
            throw new ContentApiServiceException(e.getMessage());
        }

		return apiResponse;
	}

	public class ContentApiServiceException extends Exception{}
}