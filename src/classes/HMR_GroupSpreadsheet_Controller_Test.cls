/*****************************************************
 * Author: Zach Engman
 * Created Date: 04/04/2018
 * Created By: Zach Engman
 * Last Modified Date: 04/04/2018
 * Last Modified By:
 * Description: Test Coverage for the HMR_GroupSpreadsheet_Controller Class
 * ****************************************************/
@isTest
private class HMR_GroupSpreadsheet_Controller_Test {
	@testSetup
	private static void setupTestData(){
	  User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = HMR_TestFactory.createProgram();
      insert programRecord;

      Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
      insert programMembershipRecord;

      //create coach
      User coachUserRecord = HMR_TestFactory.createUser('HMR Standard User', true);
      insert coachUserRecord;

      Coach__c coachRecord = HMR_TestFactory.createCoach(coachUserRecord.Id);
      insert coachRecord;

      //setup the group/class and sessions
      Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
      insert classRecord;

      Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
      insert classMemberRecord;

      //create last 4 coaching sessions
      List<Coaching_Session__c> coachingSessionList;
      
      Coaching_Session__c coachingSessionRecord01 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord01.hmr_Class_Date__c = Date.today().addDays(-7);
      Coaching_Session__c coachingSessionRecord02 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord02.hmr_Class_Date__c = Date.today().addDays(-14);
      Coaching_Session__c coachingSessionRecord03 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord03.hmr_Class_Date__c = Date.today().addDays(-21);
       Coaching_Session__c coachingSessionRecord04 = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord04.hmr_Class_Date__c = Date.today().addDays(-28);
      
      coachingSessionList = new List<Coaching_Session__c>{coachingSessionRecord01, coachingSessionRecord02, coachingSessionRecord03, coachingSessionRecord04};
      insert coachingSessionList;

      Session_Attendee__c sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord01.Id, classMemberRecord.Id, contactRecord.Id);
      sessionAttendeeRecord.hmr_Attendance__c = 'Attended';
      insert sessionAttendeeRecord;

      //create activity log records
      List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
      
      for(integer i = 0; i < 7; i++){
        ActivityLog__c activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);
        
        activityLogRecord.DateDataEnteredFor__c = Date.today().addDays(i * -1);
        activityLogRecord.Group_Member__c = classMemberRecord.Id;
        activityLogRecord.Session_Attendee__c = sessionAttendeeRecord.Id;

        activityLogList.add(activityLogRecord);
      }

      insert activityLogList;
	}

  	@isTest
  	private static void testGetGroupDataSpreadsheet(){
  		Class__c classRecord = [SELECT Id FROM Class__c LIMIT 1];
  		ApexPages.StandardController con = new ApexPages.StandardController(classRecord);

  		Test.setCurrentPageReference(new PageReference('Page.HMR_GroupData_Spreadsheet'));
  		System.currentPageReference().getParameters().put('id', classRecord.Id);

  		Test.startTest();

  		HMR_GroupData_Spreadsheet_Controller spreadSheetController = new HMR_GroupData_Spreadsheet_Controller(con);
  		String columnClass = spreadSheetController.columnClass;
  		boolean isHealthySolutions = spreadSheetController.isHealthySolutions;

  		Test.stopTest();

  		//verify, we setup test data as healthy solutions, should be true
  		System.assertEquals(isHealthySolutions, true);
  	}
}