/**
* Apex Service Class for the Goal Object
*
* @Date: 2018-02-13
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-02-14
* @JIRA: 
*/
public with sharing class HMR_Goal_Service {
	private string contactId;

	public HMR_Goal_Service(string contactId) {
		this.contactId = contactId;
	}

	/**
     * method to get the goal setting based on diet type
     * @param programName - the program to retrieve the settings for
     * @param dietType - the diet type to retrieve the settings for
     * @return Goal_Setting__mdt 
     * @return null if not found
    */
	public static GoalSetting getGoalSetting(string programName, string dietType){
		List<Goal_Setting__mdt> goalSettingList = [SELECT Entrees__c
														 ,Entree_Minimum__c
														 ,Fruits_and_Vegetables__c
														 ,Fruits_and_Vegetables_Minimum__c
														 ,HMR_Foods__c
														 ,HMR_Foods_Minimum__c
														 ,Physical_Activity__c
														 ,Physical_Activity_Minimum__c
														 ,Shakes__c
														 ,Shake_Minimum__c
												   FROM Goal_Setting__mdt
												   WHERE Program__c =: programName 
												   	AND Diet_Type__c =: dietType];

	    return goalSettingList.size() > 0 ? new GoalSetting(goalSettingList[0]) : null;
	}

	/**
     * method to get the contact's goal for a specific date
     * @param goalDate - the date to retrieve the goal data for
     * @return Goal__c - the goal record
     * @return null if not found
    */
	public Goal__c getGoalForDate(Date goalDate){
		Date minDate = goalDate.addDays(-7); 
		List<Goal__c> goalList = [SELECT Contact__c
										,FocusArea__c
										,GoalAmount__c
										,WeekStartDate__c
								  FROM Goal__c
								  WHERE Contact__c =: this.contactId
								  AND WeekStartDate__c <=: goalDate
								  AND WeekStartDate__c >=: minDate
								  ORDER BY WeekStartDate__c DESC];

		return goalList.size() > 0 && goalList[0].WeekStartDate__c.addDays(6) > goalDate ? goalList[0] : null;
	}

	/**
     * method to get the goal statistics by program membership and focus area
     * @param programMembershipId - the current program membership id for the user/contact
     * @param focusArea - the selected focus area (goal) for the contact
     * @return GoalStatistics - personal best and last week
     * @return null if not found
    */
	public GoalStatistics getGoalStatistics(string programMembershipId, string focusArea){
		GoalStatistics goalStatistics;
		Map<Integer, Integer> weeklyTotalMap = new Map<Integer, Integer>();
		Map<String, String> statisticFieldMap = new Map<String, String>{'Entrees' => 'Entrees__c',
																		'Shakes' => 'ShakesCereals__c',
																		'Physical Activity' => 'TotalPhysicalActivityCaloriesRollUp__c',
																		'Fruits & Vegetables' => 'FruitsVegetables__c',
																	    'HMR Foods' => 'TotalMealReplacements__c'};
		string field = statisticFieldMap.get(focusArea);
		integer personalBest = 0;
		integer lastWeek = 0;
		integer lastWeekTotal = 0;
		Program_Membership__c programMembershipRecord = [SELECT Actual_Diet_Start_Date__c FROM Program_Membership__c WHERE Id =: programMembershipId];
		
		if(programMembershipRecord.Actual_Diet_Start_Date__c != null){
			lastWeek = Integer.valueOf(Math.ceil(programMembershipRecord.Actual_Diet_Start_Date__c.daysBetween(Date.today()) / 7.0) - 1);
		}

		//We don't have a week object, so we need to iterate by formula (can't group on formula field)
		//	to create a totals map by week. 
		for(ActivityLog__c activityLogRecord : [SELECT Entrees__c
													   ,FruitsVegetables__c
													   ,ShakesCereals__c
													   ,TotalPhysicalActivityCaloriesRollUp__c
													   ,TotalMealReplacements__c
													   ,Diet_Week__c
													   ,Is_Current_Diet_Week__c
													   ,Is_Previous_Diet_Week__c
												 FROM ActivityLog__c
												 WHERE Contact__c =: this.contactId
												 	AND Program_Membership__c =: programMembershipId
												 ORDER BY Diet_Week__c DESC]){
			
			integer weeksInProgram = Integer.valueOf(activityLogRecord.Diet_Week__c);
			object fieldValue = activityLogRecord.get(field);
			if(fieldValue == null){
				fieldValue = 0;
			}

			if(weeklyTotalMap.containsKey(weeksInProgram)){
				weeklyTotalMap.put(weeksInProgram, weeklyTotalMap.get(weeksInProgram) + Integer.valueOf(fieldValue));
			}
			else{
				weeklyTotalMap.put(weeksInProgram, Integer.valueOf(fieldValue));
			}
		}
		
		//get personal best
		for(integer weeklyTotal : weeklyTotalMap.values()){
			if(weeklyTotal > personalBest){
				personalBest = weeklyTotal;
			}
		}
		//get total for previous week
		if(weeklyTotalMap.get(lastWeek) != null){
			lastWeekTotal = weeklyTotalMap.get(lastWeek);
		}
		
		goalStatistics = new GoalStatistics(focusArea, personalBest, lastWeekTotal);

		return goalStatistics;
	}

	/**
     * method to get the goal statistics by program membership
     * @param programMembershipId - the current program membership id for the user/contact
     * @return GoalStatistics - personal best and last week
     * @return null if not found
    */
	public Map<String, GoalStatistics> getAllGoalStatistics(string programMembershipId){
		Map<String, GoalStatistics> goalStatisticMap = new Map<String, GoalStatistics>{
			'Entrees__c' => new GoalStatistics('Entrees', 0, 0), 
			'ShakesCereals__c' => new GoalStatistics('Shakes', 0, 0),
			'TotalPhysicalActivityCaloriesRollUp__c' => new GoalStatistics('Physical Activity', 0, 0),
			'FruitsVegetables__c' => new GoalStatistics('Fruits & Vegetables', 0, 0),
			'TotalMealReplacements__c' => new GoalStatistics('HMR Foods', 0, 0)
		};
		Map<String, Map<Integer, Integer>> fieldToWeeklyTotalMap = new Map<String, Map<Integer, Integer>>{
			'Entrees__c' => new Map<Integer, Integer>(),
			'ShakesCereals__c' => new Map<Integer, Integer>(),
			'TotalPhysicalActivityCaloriesRollUp__c' => new Map<Integer, Integer>(),
			'FruitsVegetables__c' => new Map<Integer, Integer>(),
			'TotalMealReplacements__c' => new Map<Integer, Integer>()
		};
		Map<String, String> statisticFieldMap = new Map<String, String>{'Entrees' => 'Entrees__c',
																		'Shakes' => 'ShakesCereals__c',
																		'Physical Activity' => 'TotalPhysicalActivityCaloriesRollUp__c',
																		'Fruits & Vegetables' => 'FruitsVegetables__c',
																		'HMR Foods' => 'TotalMealReplacements__c'};

		integer lastWeek = 0;
		Program_Membership__c programMembershipRecord = [SELECT Actual_Diet_Start_Date__c FROM Program_Membership__c WHERE Id =: programMembershipId];

		if(programMembershipRecord.Actual_Diet_Start_Date__c != null){
			lastWeek = Integer.valueOf(Math.ceil(programMembershipRecord.Actual_Diet_Start_Date__c.daysBetween(Date.today()) / 7.0) - 1);
		}
		//We don't have a week object, so we need to iterate by formula (can't group on formula field)
		//	to create a totals map by week. 
		for(ActivityLog__c activityLogRecord : [SELECT Entrees__c
													   ,FruitsVegetables__c
													   ,ShakesCereals__c
													   ,TotalMealReplacements__c
													   ,TotalPhysicalActivityCaloriesRollUp__c
													   ,Diet_Week__c
													   ,Is_Current_Diet_Week__c
													   ,Is_Previous_Diet_Week__c
												 FROM ActivityLog__c
												 WHERE Contact__c =: this.contactId
												 	AND Program_Membership__c =: programMembershipId
												 ORDER BY Diet_Week__c DESC]){
			for(string field : statisticFieldMap.values()){
				Map<Integer, Integer> weeklyTotalMap = fieldToWeeklyTotalMap.get(field);
				integer weeksInProgram = Integer.valueOf(activityLogRecord.Diet_Week__c);
				object fieldValue = activityLogRecord.get(field);
				
				if(fieldValue == null){
					fieldValue = 0;
				}

				if(weeklyTotalMap.containsKey(weeksInProgram)){
					weeklyTotalMap.put(weeksInProgram, weeklyTotalMap.get(weeksInProgram) + Integer.valueOf(fieldValue));
				}
				else{
					weeklyTotalMap.put(weeksInProgram, Integer.valueOf(fieldValue));
				}
			}
		}

		//get personal best
		for(string field : statisticFieldMap.values()){
			Map<Integer, Integer> weeklyTotalMap = fieldToWeeklyTotalMap.get(field);
			GoalStatistics goalStatistics = goalStatisticMap.get(field);
			
			for(integer weeklyTotal : weeklyTotalMap.values()){
				if(weeklyTotal > goalStatistics.personalBest){
					goalStatistics.personalBest = weeklyTotal;
				}
			}

			//get total for previous week
			if(weeklyTotalMap.get(lastWeek) != null){
				goalStatistics.lastWeekTotal = weeklyTotalMap.get(lastWeek);
			}

			goalStatisticMap.put(field, goalStatistics);
		}
		
		return goalStatisticMap;
	}

	public class GoalSetting{
		public boolean entreesAvailable {get; set;}
		public boolean fruitsAndVegetablesAvailable {get; set;}
		public boolean hmrFoodsAvailable {get; set;}
		public boolean physicalActivityAvailable {get; set;}
		public boolean shakesAvailable {get; set;}
		public integer entreeMinimum {get; set;}
		public integer fruitsAndVegetablesMinimum {get; set;}
		public integer hmrFoodsMinimum {get; set;}
		public integer physicalActivityMinimum {get; set;}
		public integer shakesMinimum {get; set;}

		public GoalSetting(Goal_Setting__mdt goalSettingRecord){
			this.entreesAvailable = goalSettingRecord.Entrees__c;
			this.fruitsAndVegetablesAvailable = goalSettingRecord.Fruits_and_Vegetables__c;
			this.hmrFoodsAvailable = goalSettingRecord.HMR_Foods__c;
			this.physicalActivityAvailable = goalSettingRecord.Physical_Activity__c;
			this.shakesAvailable = goalSettingRecord.Shakes__c;
			this.entreeMinimum = Integer.valueOf(goalSettingRecord.Entree_Minimum__c);
			this.fruitsAndVegetablesMinimum = Integer.valueOf(goalSettingRecord.Fruits_and_Vegetables_Minimum__c);
			this.hmrFoodsMinimum = Integer.valueOf(goalSettingRecord.HMR_Foods_Minimum__c);
			this.physicalActivityMinimum = Integer.valueOf(goalSettingRecord.Physical_Activity_Minimum__c);
			this.shakesMinimum = Integer.valueOf(goalSettingRecord.Shake_Minimum__c);
		}
	}

	public class GoalStatistics{
		public string focusArea {get; set;}
		public integer personalBest {get; set;}
		public integer lastWeekTotal {get; set;}

		public GoalStatistics(string focusArea, integer personalBest, integer lastWeekTotal){
			this.focusArea = focusArea;
			this.personalBest = personalBest;
			this.lastWeekTotal = lastWeekTotal;
		}
	}
}