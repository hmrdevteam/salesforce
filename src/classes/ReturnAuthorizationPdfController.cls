public class ReturnAuthorizationPdfController{

    public ccrz__E_Order__c order {get;set;}
	public String refundId {get;set;}
	public Refund__c tempRefund {get;set;}
	public Refund__c refund {get;set;}
    public List<Refund_Item__c> refundItems{get;set;}
    public String contactPhone{get;set;}


    public ReturnAuthorizationPdfController(ApexPages.StandardController controller) {

		tempRefund = (Refund__c)controller.getRecord();
		refundId = tempRefund.Id;

		refund = [SELECT Id, Name, hmr_Order_Number__c, CreatedDate, hmr_Total_Refund_Amount__c, hmr_Return_Status__c
					FROM Refund__c WHERE Id = :refundId];

		order = [SELECT Id, Name, ccrz__BuyerPhone__c, ccrz__Contact__r.Name, ccrz__Contact__r.Phone, ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                        ccrz__ShipTo__r.ccrz__AddressSecondline__c, ccrz__ShipTo__r.ccrz__AddressThirdline__c, ccrz__ShipTo__r.ccrz__City__c,
                        ccrz__ShipTo__r.ccrz__State__c, ccrz__ShipTo__r.ccrz__PostalCode__c
                    FROM ccrz__E_Order__c
					WHERE Id = :refund.hmr_Order_Number__c];

        refundItems = [SELECT Id, Name, CC_Order_Item__r.ccrz__Product__r.Name, hmr_Refund_Product_Quantity__c, Price_paid_by_Client__c
                        FROM Refund_Item__c  WHERE hmr_Refund__c = :refund.Id];

       	if(order.ccrz__BuyerPhone__c != null){
       		contactPhone = order.ccrz__BuyerPhone__c;
       	} else if(order.ccrz__Contact__r.Phone != null){
       		contactPhone = order.ccrz__Contact__r.Phone;
       	} else{
       		contactPhone = '';
       	}

    }
}