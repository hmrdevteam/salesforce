/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 11 May 2017
 * Created By: Nathan Anderson
 * Last Modified Date: 16 Nov 2017
 * Last Modified By: Nathan Anderson
 * Description:
 When CC Account Address Book is updated to Default
 Clear previous defaults, update Contact and update Pending/template orders
 * ****************************************************/

public class DefaultAddressTriggerHandler {
	public DefaultAddressTriggerHandler() {

	}

	public static void handleAfterInsert(List<ccrz__E_AccountAddressBook__c> newUpdatedAddresses, Map<Id, ccrz__E_AccountAddressBook__c> oldAddressVersionsMap) {

		Set<Id> addBookIds = new Set<Id>();
		Set<Id> ownerIds = new Set<Id>();
		List<User> users = new List<User>();
		Set<Id> contactIds = new Set<Id>();
        //get all address Id's
        for(ccrz__E_AccountAddressBook__c newUpdatedAddress: newUpdatedAddresses) {
			addBookIds.add(newUpdatedAddress.Id);
			ownerIds.add(newUpdatedAddress.OwnerId);
        }

		//Query for the full records including ContactAddress fields from the set of ids passed in from trigger.new
		List<ccrz__E_AccountAddressBook__c> defaultAddresses = [SELECT Id, ccrz__Default__c, OwnerId, ccrz__AddressType__c,
																ccrz__E_ContactAddress__c,
																ccrz__E_ContactAddress__r.Contact_Id__c,
																ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
																ccrz__E_ContactAddress__r.ccrz__City__c,
																ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
																ccrz__E_ContactAddress__r.ccrz__PostalCode__c
																FROM ccrz__E_AccountAddressBook__c
																WHERE Id IN :addBookIds];


		//Create lists to hold all updated shipping addresses and billing addresses separately
		List<ccrz__E_AccountAddressBook__c> defaultShipToUpdates = new List<ccrz__E_AccountAddressBook__c>();
		List<ccrz__E_AccountAddressBook__c> defaultBillToUpdates = new List<ccrz__E_AccountAddressBook__c>();

		//Loop through all addresses passed in from Trigger.new and check to see if they were updated to Default = true from Default = false
		//then separate them by address type and add to lists
		for(ccrz__E_AccountAddressBook__c newUpdatedAddress: defaultAddresses) {
			//get old version of record for comparison to handle updates
			if(oldAddressVersionsMap != null && !oldAddressVersionsMap.isEmpty()) {
				ccrz__E_AccountAddressBook__c getOldVersionOfAddress = oldAddressVersionsMap.get(newUpdatedAddress.Id);

				//check to see if default field is updated to true from false
				if(newUpdatedAddress.ccrz__Default__c == true && newUpdatedAddress.ccrz__Default__c != getOldVersionOfAddress.ccrz__Default__c) {
					if(newUpdatedAddress.ccrz__AddressType__c == 'Shipping') {
						defaultShipToUpdates.add(newUpdatedAddress);
					}
					if(newUpdatedAddress.ccrz__AddressType__c == 'Billing') {
						defaultBillToUpdates.add(newUpdatedAddress);
					}
				}

			} else {
				//handle inserts where the old map is empty
				if(newUpdatedAddress.ccrz__Default__c == true) {
					if(newUpdatedAddress.ccrz__AddressType__c == 'Shipping') {
						defaultShipToUpdates.add(newUpdatedAddress);
					}
					if(newUpdatedAddress.ccrz__AddressType__c == 'Billing') {
						defaultBillToUpdates.add(newUpdatedAddress);
					}
				}
			}
		}


		/*****************************************************
		 Uncheck all previous default addresses
		 * ****************************************************/

		//new List to hold Address Books to uncheck
		List<ccrz__E_AccountAddressBook__c> addressesToUncheck = new List<ccrz__E_AccountAddressBook__c>();
		//Get the Integration User Id to exclude these records from the Query
		Id intUserId = [Select Id FROM User WHERE FirstName = 'Integration' LIMIT 1].Id;
		//Get list of all Address books owned by the same user NOT in list of updated addresses
		List<ccrz__E_AccountAddressBook__c> relatedAddresses = [SELECT Id, ccrz__AddressType__c, ccrz__Default__c, OwnerId FROM ccrz__E_AccountAddressBook__c
								WHERE OwnerId IN :ownerIds AND OwnerId != :intUserId AND Id NOT IN :addBookIds];

		if(relatedAddresses.size() > 0){
			//Loop through Shipping address updates and add previous shipping defaults to list to uncheck
			for(ccrz__E_AccountAddressBook__c defaultShipToUpdate : defaultShipToUpdates){

				for(ccrz__E_AccountAddressBook__c relatedAddress : relatedAddresses ){
					if(relatedAddress.ccrz__AddressType__c == defaultShipToUpdate.ccrz__AddressType__c &&
						relatedAddress.OwnerId == defaultShipToUpdate.OwnerId){

							relatedAddress.ccrz__Default__c = false;
							addressesToUncheck.add(relatedAddress);
					}
				}
			}

			//Loop through Billing address updates and add previous billing defaults to list to uncheck
			for(ccrz__E_AccountAddressBook__c defaultBillToUpdate : defaultBillToUpdates){

				for(ccrz__E_AccountAddressBook__c relatedAddress : relatedAddresses ){
					if(relatedAddress.ccrz__AddressType__c == defaultBillToUpdate.ccrz__AddressType__c &&
						relatedAddress.OwnerId == defaultBillToUpdate.OwnerId){

							relatedAddress.ccrz__Default__c = false;
							addressesToUncheck.add(relatedAddress);
					}
				}
			}
		}

		//if anything was added to list to uncheck, update records
		if(addressesToUncheck.size() > 0) {

			try {
				update addressesToUncheck;
			} catch(Exception e) {
				System.debug('Exception in DefaultAddress Uncheck: ' + e);
			}
		}

		/*****************************************************
		 Update Contact records with new defualt Addresses
		 * ****************************************************/

		//Create new list to hold contact records that we'll update
		List<Contact> contactsToUpdateShipping = new List<Contact>();
		//Get the user records from the set of Owner Ids from all incoming updated address books
		users = [SELECT Id, ContactId FROM User WHERE Id IN :ownerIds];
		//for each user in the list, add the ContactId to a set, then get the full contact record
		for(User u : users) {
			contactIds.add(u.ContactId);
		}
		List<Contact> contacts = [SELECT Id FROM Contact WHERE Id IN : contactIds];

		//Loop through new default Shipping addresses, get the matching contact records and update the mailing address field
		//then add to list to update
		for(ccrz__E_AccountAddressBook__c defaultShipToUpdate: defaultShipToUpdates) {
			for(Contact c : contacts) {
				if(defaultShipToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c != null && defaultShipToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c != '') {
					if(c.Id == defaultShipToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c) {
						c.MailingStreet = defaultShipToUpdate.ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c;
						c.MailingCity = defaultShipToUpdate.ccrz__E_ContactAddress__r.ccrz__City__c;
						c.MailingState = defaultShipToUpdate.ccrz__E_ContactAddress__r.ccrz__StateISOCode__c;
						c.MailingPostalCode = defaultShipToUpdate.ccrz__E_ContactAddress__r.ccrz__PostalCode__c;

						contactsToUpdateShipping.add(c);
					}
				}
			}
        }
		//Create new list to hold contact records that we'll update
		List<Contact> contactsToUpdateBilling = new List<Contact>();
		//Loop through new default Billing addresses, get the matching contact records and update the other address field
		//then add to list to update
		for(ccrz__E_AccountAddressBook__c defaultBillToUpdate: defaultBillToUpdates) {
			for(Contact c : contacts) {
				if(defaultBillToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c != null && defaultBillToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c != '') {
					if(c.Id == defaultBillToUpdate.ccrz__E_ContactAddress__r.Contact_Id__c) {
						c.OtherStreet = defaultBillToUpdate.ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c;
						c.OtherCity = defaultBillToUpdate.ccrz__E_ContactAddress__r.ccrz__City__c;
						c.OtherState = defaultBillToUpdate.ccrz__E_ContactAddress__r.ccrz__StateISOCode__c;
						c.OtherPostalCode = defaultBillToUpdate.ccrz__E_ContactAddress__r.ccrz__PostalCode__c;

						contactsToUpdateBilling.add(c);
					}
				}
			}
        }

		//if list is not empty, update the records
		if(contactsToUpdateShipping.size() > 0) {

			try {
				update contactsToUpdateShipping;
			} catch(Exception e) {
				System.debug('Exception in Default Shipping Address Contact Update: ' + e);
			}
		}

		//if list is not empty, update the records
		if(contactsToUpdateBilling.size() > 0) {

			try {
				update contactsToUpdateBilling;
			} catch(Exception e) {
				System.debug('Exception in Default Billing Address Contact Update: ' + e);
			}
		}

		/*****************************************************
		 Update pending/template orders' Bill To / Ship To
		 * ****************************************************/

		//New list for holding CC orders that need updated with address records
		List<ccrz__E_Order__c> ordersToUpdate = new List<ccrz__E_Order__c>();
		Boolean updateOrder = false;

		//get the list of all pending and template orders owned by users in the ownerIds set
		List<ccrz__E_Order__c> ownedOrders = [SELECT Id, ccrz__BillTo__c, ccrz__ShipTo__c, OwnerId FROM ccrz__E_Order__c
												WHERE OwnerId IN :ownerIds
												AND (ccrz__OrderStatus__c = 'Pending'
												OR ccrz__OrderStatus__c = 'Template')];


		for(ccrz__E_Order__c ownedOrder : ownedOrders){
			for(ccrz__E_AccountAddressBook__c defaultShipToUpdate: defaultShipToUpdates) {
				//System.debug('@@@@@@@@@@@@@@ defaultShipToUpdates loop: ' + defaultShipToUpdates);
				if(ownedOrder.OwnerId == defaultShipToUpdate.OwnerId) {
					ownedOrder.ccrz__ShipTo__c = defaultShipToUpdate.ccrz__E_ContactAddress__c;
					updateOrder = true;
				}
			}
			for(ccrz__E_AccountAddressBook__c defaultBillToUpdate: defaultBillToUpdates) {
				//System.debug('@@@@@@@@@@@@@@ defaultBillToUpdates loop: ' + defaultBillToUpdates);
				if(ownedOrder.OwnerId == defaultBillToUpdate.OwnerId) {
					ownedOrder.ccrz__BillTo__c = defaultBillToUpdate.ccrz__E_ContactAddress__c;
					updateOrder = true;
				}
			}
			if(updateOrder) {
				ordersToUpdate.add(ownedOrder);
			}
		}

		//if the list is not empty, update the records
		if(ordersToUpdate.size() > 0) {
			//System.debug('@@@@@@@@@@@@@@ ordersToUpdate inside IF: ' + ordersToUpdate);
			try {
				update ordersToUpdate;
			} catch(Exception e) {
				System.debug('Exception in DefaultAddress Order Update: ' + e);
			}
		}
	}
}