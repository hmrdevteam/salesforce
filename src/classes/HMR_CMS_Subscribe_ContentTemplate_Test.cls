/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 30th, 2017
Log:
Target Class(ses): HMR_CMS_Subscribe_ContentTemplate
*/

@isTest
private class HMR_CMS_Subscribe_ContentTemplate_Test{  
    @isTest static void test_general_method() {
    // Implement test code
    Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_HomePage_PageTemplate'));
    HMR_CMS_Subscribe_ContentTemplate subscribeController = new HMR_CMS_Subscribe_ContentTemplate();
    String propertyValue = subscribeController.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');    
    String heroImage = subscribeController.backgroundImage;
    String renderHTML = subscribeController.getHTML();  
        
    System.assert(!String.isBlank(renderHTML));    
  }
}