/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_CommentFeed_ContentTemplate
*/

@isTest(SeeAllData=true)  //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_CommentFeed_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_CommentFeed_ContentTemplate commentFeedTemplate = new HMR_CMS_CommentFeed_ContentTemplate();
    String propertyValue = commentFeedTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    commentFeedTemplate.testAttributes = tab;

    string s2 = commentFeedTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = commentFeedTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');


	ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
    List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
    testItemList.add(new ConnectApi.FeedItem());
    testItemList.add(new ConnectApi.FeedItem());
    testPage.elements = testItemList;

    String communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;

    HMR_Topic_Service topicService = new HMR_Topic_Service();
    string topicId = topicService.getTopicId('Eating Well');
    //system.debug('+++++^^'+communityId);
	HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
 	ConnectApi.FeedElement fe = connectApiService.postQuestionAndAnswer(topicId, 'subject', 'body');
 	ConnectApi.FeedElement fe2 = connectApiService.postQuestionAndAnswer(topicId, 'subject2', 'body2');

    ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(communityId, ConnectApi.FeedType.Record, topicId, testPage);

    String renderHTML = commentFeedTemplate.getHTML();
    

    string nowD = commentFeedTemplate.getFormatedDate(DateTime.now());

    HMR_CMS_CommentFeed_ContentTemplate.QuestionAnswerResult qar= new HMR_CMS_CommentFeed_ContentTemplate.QuestionAnswerResult(fe);
    System.assert(!String.isBlank(renderHTML));

    try{
        HMR_CMS_CommentFeed_ContentTemplate qc = new HMR_CMS_CommentFeed_ContentTemplate(null);
    }catch(Exception e){}

  }
  
}