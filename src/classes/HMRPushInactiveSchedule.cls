/**
 * convenience class to allow scheduling notifications using "Schedule Apex"
 */
global class HMRPushInactiveSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        try {
            HMRPushNotificationJob bj = new HMRPushNotificationJob(HMRNotificationType.INACTIVE_REMINDER);
            database.executebatch(bj, 200);
        }
        catch(Exception ex) {
            System.debug('The HMRPushInactiveSchedule failed ' + ex.getMessage());
        }       
    }
}