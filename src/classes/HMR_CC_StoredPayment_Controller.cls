/**
* Controller for CC My Account Stored Payment, Add/Edit Credit Card
* Objects referenced --> TBD
*
* @Date Created: 2017-05-13
* @Author Pranay Mistry (M360)
* @Modification Log
* @JIRA: Link to be Added
*/
global class HMR_CC_StoredPayment_Controller {
    global HMR_CC_StoredPayment_Controller() {}

    global static Map<String, Object> getAddressBookDetails(Contact contact) {
        Map<String, Object> outputData = new Map<String, Object>();
        List<ccrz__E_AccountAddressBook__c> addressBookList = new List<ccrz__E_AccountAddressBook__c>(
                [SELECT Id, ccrz__E_ContactAddress__c, ccrz__AddressType__c, ccrz__Default__c, ccrz__AccountId__c
                    FROM ccrz__E_AccountAddressBook__c WHERE OwnerId = :contact.Community_User_ID__c AND
                                                             ccrz__AddressType__c = 'Billing']);
        Set<Id> contactAddressIds = new Set<Id>();
        if(!addressBookList.isEmpty()) {
            outputData.put('addressBookList', addressBookList);
            for(ccrz__E_AccountAddressBook__c addressBook: addressBookList) {
                contactAddressIds.add(addressBook.ccrz__E_ContactAddress__c);
            }
        }
        List<ccrz__E_ContactAddr__c> contactAddressList = new List<ccrz__E_ContactAddr__c>(
            [SELECT Id, ccrz__AddressFirstline__c, ccrz__AddressSecondline__c, ccrz__City__c, ccrz__FirstName__c,
                ccrz__LastName__c, ccrz__Country__c, ccrz__CountryISOCode__c, ccrz__PostalCode__c,
                ccrz__State__c, ccrz__StateISOCode__c FROM ccrz__E_ContactAddr__c WHERE Id IN :contactAddressIds]);
        if(!contactAddressList.isEmpty()) {
            outputData.put('contactAddressList', contactAddressList);
        }
        return outputData;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult CreatePaymentToken(ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> responseMap = new Map<String, Object>();
        try {
            HMR_Cybersource_Utility.createPaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, unSigned_Field_Values);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);//postString
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in CreatePaymentToken---'+e.getMessage());
            system.debug('Exception line Number---'+e.getStackTraceString());
            responseMap.put('exception', e);
            res.data = responseMap;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult UpdatePaymentToken(ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, String paymentToken, Map<String, String> unSigned_Field_Values,
            String contactAddressId, String storedPaymentToUpdateId, String displayName, String contactFirstName, String contactLastName) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> responseMap = new Map<String, Object>();
        try {
            HMR_Cybersource_Utility.updatePaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, paymentToken, unSigned_Field_Values);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_Update_Token_EndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);//postString
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            if(storedPaymentToUpdateId != null && storedPaymentToUpdateId != '' && contactAddressId != null && contactAddressId != '') {

                if(contactFirstName != null || contactFirstName != '' || contactLastName != null || contactLastName != '') {
                    ccrz__E_ContactAddr__c contactAddressToUpdate =
                        [SELECT Id, ccrz__FirstName__c, ccrz__LastName__c FROM ccrz__E_ContactAddr__c
                            WHERE Id = :contactAddressId];
                    if(contactFirstName != null && contactFirstName != '') {
                        contactAddressToUpdate.ccrz__FirstName__c = contactFirstName;
                    }
                    if(contactLastName != null && contactLastName != '') {
                        contactAddressToUpdate.ccrz__LastName__c = contactLastName;
                    }
                    update contactAddressToUpdate;
                    responseMap.put('contactAddressToUpdate', contactAddressToUpdate);
                }

                ccrz__E_StoredPayment__c storedPaymentToUpdate =
                    [SELECT Id, ccrz__DisplayName__c, HMR_Contact_Address__c, ccrz__Token__c, ccrz__ExpMonth__c, ccrz__ExpYear__c
                        FROM ccrz__E_StoredPayment__c WHERE Id = :storedPaymentToUpdateId];

                Map<String, String> cybersourceResponse = HMR_Cybersource_Utility.parseHTMLResponse(response.getBody());
                if(cybersourceResponse != null && !cybersourceResponse.isEmpty()) {
                    storedPaymentToUpdate.ccrz__DisplayName__c = displayName;
                    storedPaymentToUpdate.HMR_Contact_Address__c = contactAddressId;
                    storedPaymentToUpdate.ccrz__Token__c = cybersourceResponse.get('req_payment_token');
                    String cybersourceExpirationDate = cybersourceResponse.get('req_card_expiry_date');
                    storedPaymentToUpdate.ccrz__ExpMonth__c = Decimal.valueOf(cybersourceExpirationDate.substringBefore('-'));
                    storedPaymentToUpdate.ccrz__ExpYear__c = Decimal.valueOf(cybersourceExpirationDate.substringAfter('-'));
                    update storedPaymentToUpdate;
                    responseMap.put('updatedStoredPayment', storedPaymentToUpdate);
                }
            }
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in CreatePaymentToken---'+e.getMessage());
            system.debug('Exception line Number---'+e.getStackTraceString());
            responseMap.put('exception', e);
            res.data = responseMap;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchAddressBook(ccrz.cc_RemoteActionContext ctx, String contactId) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> outputData = new Map<String, Object>();
        try {
            //TODO: Add Comments and write test coverage
            Contact contact = [SELECT Id, AccountId, Community_User_ID__c FROM Contact WHERE Id = :contactId];
            if(contact != null) {
                res.data = getAddressBookDetails(contact);
            }
            else {
                res.data = null;
            }
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in fetchAddressBook---' + e.getMessage());
            system.debug('Exception line Number---' + e.getStackTraceString());
            outputData.put('exception', e);
            res.data = outputData;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchContactAddressForStoredPayment(ccrz.cc_RemoteActionContext ctx, String storedPaymentIdArg) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> outputData = new Map<String, Object>();
        try {
            // TODO: Add Comments and write test coverage
            User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :ccrz.cc_CallContext.currUserId];
            Contact contact = [SELECT Id, AccountId,Community_User_ID__c FROM Contact WHERE Id = :currentUser.ContactId];  // Added this Community_User_ID__c field in query (Utkarsh)
            if(storedPaymentIdArg != null && storedPaymentIdArg != '') {
                ccrz__E_StoredPayment__c storedPaymentToUpdate =
                    [SELECT Id, HMR_Contact_Address__c, ccrz__AccountNumber__c, ccrz__ExpMonth__c, ccrz__ExpYear__c,
                        ccrz__PaymentType__c, ccrz__Token__c, ccrz__DisplayName__c FROM ccrz__E_StoredPayment__c WHERE Id = :storedPaymentIdArg];

                if(storedPaymentToUpdate != null) {
                    outputData.put('storedPaymentToUpdate', storedPaymentToUpdate);
                }

                ccrz__E_ContactAddr__c storedPaymentContactAddress =
                    [SELECT Id, ccrz__AddressFirstline__c, ccrz__AddressSecondline__c, ccrz__City__c, ccrz__FirstName__c,
                        ccrz__LastName__c, ccrz__Country__c, ccrz__CountryISOCode__c, ccrz__PostalCode__c,
                        ccrz__State__c, ccrz__StateISOCode__c FROM ccrz__E_ContactAddr__c WHERE Id = :storedPaymentToUpdate.HMR_Contact_Address__c];

                if(storedPaymentContactAddress != null) {
                    outputData.put('storedPaymentContactAddress', storedPaymentContactAddress);
                }
                if(contact != null) {
                    outputData.put('contactAddressBook', getAddressBookDetails(contact));
                }
                res.data = outputData;
            }
            else {
                res.data = null;
            }
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in fetchContactAddressForStoredPayment---' + e.getMessage());
            system.debug('Exception line Number---' + e.getStackTraceString());
            outputData.put('exception', e);
            res.data = outputData;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult addBillingAddress(ccrz.cc_RemoteActionContext ctx, String firstName, String lastName, String contactId, Boolean setAsDefault, Map<String, String> billingAddressMapArg) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> outputData = new Map<String, Object>();
        try {
            //TODO: Add Comments and write test coverage
            Contact contact = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];
            if(contact != null && !billingAddressMapArg.isEmpty()) {
                ccrz__E_ContactAddr__c contactAddressToInsert = new ccrz__E_ContactAddr__c();
                contactAddressToInsert.ccrz__FirstName__c = firstName;
                contactAddressToInsert.ccrz__LastName__c = lastName;
                contactAddressToInsert.ccrz__AddressFirstline__c = billingAddressMapArg.get('bill_to_address_line1');
                contactAddressToInsert.ccrz__AddressSecondline__c = billingAddressMapArg.get('bill_to_address_line2');
                contactAddressToInsert.ccrz__City__c = billingAddressMapArg.get('bill_to_address_city');
                contactAddressToInsert.ccrz__State__c = billingAddressMapArg.get('bill_to_address_state');
                contactAddressToInsert.ccrz__StateISOCode__c = billingAddressMapArg.get('bill_to_address_state_code');
                contactAddressToInsert.ccrz__PostalCode__c = billingAddressMapArg.get('bill_to_address_postal_code');
                contactAddressToInsert.ccrz__Country__c = 'United States of America';
                contactAddressToInsert.ccrz__CountryISOCode__c = 'US';
                contactAddressToInsert.OwnerId = ccrz.cc_CallContext.currUserId;
                insert contactAddressToInsert;

                ccrz__E_AccountAddressBook__c contactAccountAddressBookToInsert = new ccrz__E_AccountAddressBook__c();
                contactAccountAddressBookToInsert.ccrz__E_ContactAddress__c = contactAddressToInsert.Id;
                contactAccountAddressBookToInsert.ccrz__AddressType__c = 'Billing';
                contactAccountAddressBookToInsert.ccrz__Default__c = setAsDefault;
                contactAccountAddressBookToInsert.ccrz__AccountId__c = ccrz.cc_CallContext.currAccountId;
                contactAccountAddressBookToInsert.OwnerId = ccrz.cc_CallContext.currUserId;
                insert contactAccountAddressBookToInsert;

                outputData.put('contactAddress', contactAddressToInsert);
                outputData.put('contactAccountAddressBook', contactAccountAddressBookToInsert);
                res.data = outputData;
                res.success = true;
            }
            else {
                res.data = null;
            }
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in addBillingAddress---' + e.getMessage());
            system.debug('Exception line Number---' + e.getStackTraceString());
            outputData.put('exception', e);
            res.data = outputData;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult createStoredPayment(ccrz.cc_RemoteActionContext ctx, Map<String, String> paymentInformationMap) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> outputData = new Map<String, Object>();
        try {
            //TODO: Add Comments and write test coverage

            System.debug('In Class Map -> ' + paymentInformationMap);

            if(!paymentInformationMap.isEmpty()) {
                if((paymentInformationMap.get('contactAddressId') != null || paymentInformationMap.get('contactAddressId') != '') &&
                    (paymentInformationMap.get('contactFirstName') != null || paymentInformationMap.get('contactFirstName') != '' ||
                    paymentInformationMap.get('contactLastName') != null || paymentInformationMap.get('contactLastName') != '')) {
                    ccrz__E_ContactAddr__c contactAddressToUpdate =
                        [SELECT Id, ccrz__FirstName__c, ccrz__LastName__c FROM ccrz__E_ContactAddr__c
                            WHERE Id = :paymentInformationMap.get('contactAddressId')];
                    if(paymentInformationMap.get('contactFirstName') != null && paymentInformationMap.get('contactFirstName') != '') {
                        contactAddressToUpdate.ccrz__FirstName__c = paymentInformationMap.get('contactFirstName');
                    }
                    if(paymentInformationMap.get('contactLastName') != null && paymentInformationMap.get('contactLastName') != '') {
                        contactAddressToUpdate.ccrz__LastName__c = paymentInformationMap.get('contactLastName');
                    }
                    update contactAddressToUpdate;
                    outputData.put('contactAddressToUpdate', contactAddressToUpdate);
                }
                ccrz__E_StoredPayment__c storedPaymentToInsert = new ccrz__E_StoredPayment__c(
                    ccrz__Account__c = ccrz.cc_CallContext.currAccountId,
                    ccrz__User__c = ccrz.cc_CallContext.currUserId,
                    ccrz__Storefront__c = ccrz.cc_CallContext.storefront,
                    ccrz__DisplayName__c = paymentInformationMap.get('displayName'),
                    ccrz__AccountNumber__c = paymentInformationMap.get('accountNumber'),
                    ccrz__AccountType__c = 'cc',
                    HMR_Contact_Address__c = paymentInformationMap.get('contactAddressId'),
                    ccrz__Token__c = paymentInformationMap.get('token'),
                    ccrz__ExpMonth__c = Decimal.valueOf(paymentInformationMap.get('expirationMonth')),
                    ccrz__ExpYear__c = Decimal.valueOf(paymentInformationMap.get('expirationYear')),
                    ccrz__PaymentType__c = paymentInformationMap.get('paymentType'),
                    ccrz__Enabled__c = true,
                    OwnerId = ccrz.cc_CallContext.currUserId
                );
                insert storedPaymentToInsert;
                outputData.put('storedPayment', storedPaymentToInsert);
                res.data = outputData;
                res.success = true;
            }
            else {
                res.data = null;
            }
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in storedPaymentToInsert---' + e.getMessage());
            system.debug('Exception line Number---' + e.getStackTraceString());
            outputData.put('exception', e);
            res.data = outputData;
            res.success = false;
        }
        finally {}
        return res;
    }
}