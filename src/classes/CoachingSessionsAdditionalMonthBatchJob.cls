/**
* Batch Job to Create Coaching Sessions Records
* Insert additional month coaching session records for an active class based on
* the latest coaching session record class date, comment from Pranay Mistry added after project load
*
* Objects referenced --> Class__c, Coaching_Session__c
*
* @Date: 2016-11-23
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-627
*/

global class CoachingSessionsAdditionalMonthBatchJob implements Database.Batchable<sObject> {
	Date now = Date.today();
	
		    	
	global CoachingSessionsAdditionalMonthBatchJob() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query;
		//query to get all active classes and get only the latest coaching session record based on class date desc
		query = 'SELECT Id, hmr_Active__c, hmr_Class_Name__c, hmr_Start_Time__c, hmr_Coach__c, hmr_End_Time__c, hmr_First_Class_Date__c, hmr_Active_Date__c, ' +
                	'(SELECT Id, hmr_Class__c, hmr_Class_Name__c, hmr_Class_Date__c FROM Coaching_Sessions__r ' +
                    	'WHERE hmr_Class_Date__c >= TODAY ORDER BY hmr_Class_Date__c DESC LIMIT 1) ' +
                'FROM Class__c WHERE hmr_Active__c = true';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {   		
		//type cast scope list to Class list
		List<Class__c> activeClasses = (List<Class__c>)scope;
		//declare coaching sessions list to insert
		List<Coaching_Session__c> coachingSessionsToInsert = new List<Coaching_Session__c>();
		//looping through the parent Class records returned from the query
		for (Class__c activeClass: activeClasses) { 
			//looping through the child Coaching session records from parent child realationship 
		    for (Coaching_Session__c latestCoachingSession: activeClass.Coaching_Sessions__r) {
		        //get the date range (start date and end date to generate classes between)
		    	List<Date> coachingSessionDates = DateUtil.getRecurringDates(latestCoachingSession.hmr_Class_Date__c, 0, 1, 0, 7, false);
		    	if(!coachingSessionDates.isEmpty()) {
					//loop through the date range with a 7 day increment
					for(Date coachingSessionDate: coachingSessionDates) {
						//insert coaching session in future only, hence check against now (date.today())
						if(coachingSessionDate >= now) {
							Coaching_Session__c coachingSessionToInsert = new Coaching_Session__c(
	                            hmr_Class__c = activeClass.Id,
	                            hmr_Class_Date__c = coachingSessionDate,
	                            hmr_Coach__c = activeClass.hmr_Coach__c,
	                            hmr_Start_Time__c = activeClass.hmr_Start_Time__c,
	                            hmr_End_Time__c =  activeClass.hmr_End_Time__c                            
	                        );
	                        coachingSessionsToInsert.add(coachingSessionToInsert);
						}
						else {
	                        //no need to create a coaching session record in the past
	                        System.debug('This recurring Date occured in the Past, no need to create a coachingSession' + ' ' + coachingSessionDate);
	                    }
					}	
				}
				else {
					//can't genereate the date range
					System.debug('System was not able to generate the date range to generate additional month class for ' + activeClass.hmr_Class_Name__c);
				}	    
		    }
		}		
		try {
			//insert coaching session records
			if(!coachingSessionsToInsert.isEmpty())    
        		insert coachingSessionsToInsert;
		}
		catch(Exception ex) {
			System.debug(ex.getMessage());
		}		
	}
	
	global void finish(Database.BatchableContext BC) {
		//TODO: send email to Enrollment and/or Program Specialists
	}
	
}