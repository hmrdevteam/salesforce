global with sharing class CC_MyAccount_UpdatesController {

  @RemoteAction
    global static Map<String,String> updatePhoto (String imgData) {
        Map<string,String> res = new Map<string,string>();
        res.put('success', 'false');
        String communityId = null;
        String userId = UserInfo.getUserId();
        imgData = imgData.remove('data:');
        //Blob imgDataBlob = Blob.valueOf(imgData);
        Blob imgDataBlob = EncodingUtil.base64Decode(imgData);
        try {
          //ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(imgData, 'image/jpg', 'userImage.jpg');
         ConnectApi.Photo photo = ConnectApi.UserProfiles.setPhoto(communityId, userId, new ConnectApi.BinaryInput(imgDataBlob, 'image/jpg', 'userImage.jpg'));
          res.put('success', 'true');
        }catch(Exception e){
            res.put('message', e.getMessage());
        }
        return res;
    }

  @RemoteAction
    global static Map<String,String> removePhoto () {
        Map<string,String> res = new Map<string,string>();
        res.put('success', 'false');
        String communityId = null;
        String userId = UserInfo.getUserId();
        try {
          //ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(imgData, 'image/jpg', 'userImage.jpg');
          ConnectApi.UserProfiles.deletePhoto(communityId, userId);
          res.put('success', 'true');
        }catch(Exception e){
            res.put('message', e.getMessage());
        }
        return res;
    }

    @RemoteAction
    global static Map<String,String> doSomething (String contactInfo, String contactId){

        Map<string,String> res = new Map<string,string>();
        res.put('success', 'false');
        String query = contactInfo;
        try{
            Contact c = [SELECT Id, firstName FROM Contact WHERE ID = :contactId];
          User u = [SELECT Id FROM User WHERE ContactId = :contactId];
            //Execute business logic using whatToDo here
          System.PageReference pageReference = new System.PageReference('/?' + query);
                Map<String,String> parameters = pageReference.getParameters();

                //get values from form
                //String password = parameters.get('password');
                String firstName = parameters.get('firstName');
                String lastName = parameters.get('lastName');
                String email = parameters.get('email');
                String phone = parameters.get('phone');
                String displayName = parameters.get('displayName');
                //String currentPass = parameters.get('currentPass');
                //String newPass = parameters.get('newPass');
                //String confirmPass = parameters.get('confirmPass');
                String gender = parameters.get('gender');
                String feet = parameters.get('feet');
                String inches = parameters.get('inches');
                String month = parameters.get('month');
                String day = parameters.get('day');
                String year = parameters.get('year');
                String order = parameters.get('order');
                //String bio = parameters.get('bio');
                //String weightLoss = parameters.get('weightLoss');

                if (firstName != null && firstName != '') {
                    c.firstName = firstName;
                    u.FirstName = firstName;
                }
                if (lastName != null && lastName != '') {
                    c.lastName = lastName;
                    u.LastName = lastName;
                }
                if (email != null && email != '') {
                    c.email = email;
                    u.email = email;
                    
                }
                  if (phone != null && phone != '') {
                    c.phone = phone;
                    u.phone = phone;
                  }
                if (displayName != null && displayName != '') {
                    u.communityNickname = displayName;
                }
                if (gender != null && gender != '') {
                    c.hmr_Gender__c = gender;
                }
                if (feet != null && feet != '') {
                    c.hmr_Height_Feet__c = Decimal.valueOf(feet);
                }
                if (inches != null && inches != '') {
                    c.hmr_Height_Inches__c = Decimal.valueOf(inches);
                }
                if (month != null && month != '' && day != null && day != '' && year != null && year != '') {
                    c.hmr_Date_Of_Birth__c = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                }
                
        //         if (bio != '') {
        //             c.hmr_Notes__c = bio;
        //         }
        //   if (weightLoss != '') {
        //     c.hmr_Weight_Loss_Goal__c = Decimal.valueOf(weightLoss);
        //   }
                update c;
          update u;

          res.put('success', 'true');
        }catch(Exception e){
            res.put('message', e.getMessage()+e.getLineNumber());

        }finally{
       }
        return res;
    }
}