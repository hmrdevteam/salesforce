/**
* Test Class Coverage of the HMR_CMS_SuccessStoryList_ContentTemplate
*
* @Date: 08/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA: 
*/
@isTest
public class HMR_CMS_SuccessStoryList_ContntTmpl_Test{  
    @testSetup
    private static void setupTestData(){
        List<Success_Story__kav> successStoryList = new List<Success_Story__kav>();

        for(integer i = 0; i < 20; i++)
            successStoryList.add(new Success_Story__kav(Title = 'Test Success Story ' + i
                                                       ,UrlName = 'test-success-story-' + i
                                                       ,Language = 'en_US'
                                                       ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                       ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                       ,Before_Photo__c = 'http://www.hmrprogram.com'
                                                       ,After_Photo__c = 'http://www.hmrprogram.com'
                                                       ,Publish_to_Success_Story_Page__c = false));
        
        
        insert successStoryList;

        List<Success_Story__kav> successStoryToPublishList = [SELECT KnowledgeArticleId FROM Success_Story__kav WHERE Id IN: successStoryList];
        for(Success_Story__kav successStoryRecord : successStoryToPublishList)
            KbManagement.PublishingService.publishArticle(successStoryRecord.KnowledgeArticleId, true);
    }

    @isTest
    private static void testGetHtml(){
        HMR_CMS_SuccessStoryList_ContentTemplate controller = new HMR_CMS_SuccessStoryList_ContentTemplate();

        Test.startTest();
        
        String htmlResult = controller.getHTML();

        Test.stopTest();

        System.assert(!String.isBlank(htmlResult));
    }
    
    @isTest
    private static void testPageParameterControllerConstructor(){
        HMR_CMS_SuccessStoryList_ContentTemplate controller;
        
        Test.startTest();

        controller = new HMR_CMS_SuccessStoryList_ContentTemplate('1');

        Test.stopTest();
        
        System.assert(!String.isBlank(controller.getHTML()));
    }
    
    @isTest
    private static void testContentControllerConstructor(){
        HMR_CMS_SuccessStoryList_ContentTemplate controller;
        
        Test.startTest();
        //This always fails as no way to get context but here for coverage
        try{
            controller = new HMR_CMS_SuccessStoryList_ContentTemplate((cms.CreateContentController)null);
        }
        catch(Exception ex){}
        
        Test.stopTest();
        
        System.assert(controller == null);
    }
    
}