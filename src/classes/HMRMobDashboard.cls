/**
* Apex Rest Service Class for Dashboard Operations
* @Date: 2018-02-16
* @Author: Magnet 360 - Zach Engman
* @Modified: 2018-02-16
* @JIRA:
*/
@RestResource(urlMapping='/mobile/user/dashboard/*')
global with sharing class HMRMobDashboard {
	/**
     * method to retrieve the dashboard information
     * @param dashboardDate - the week start date
     * @param communityUserId (optional) - will use the current/calling user id if not passed 
     * @return returnMap success - true or false
     *                   weeklyDashboard - object representing the combined dashboard data
     *                   error - error message
    */
	@HttpGet
	global static void doGet(){
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
		Map<String, String> paramMap = RestContext.request.params;
		String communityUserId = paramMap.get('communityUserId');

		if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
		}

        if(paramMap.get('dashboardDate') != null){
            RestContext.response.statusCode = 200;
            HMR_Dashboard_Service dashboardService = new HMR_Dashboard_Service(communityUserId);
        	Date dashboardDate = Date.valueOf(paramMap.get('dashboardDate'));
        	

            returnMap.put('success', true);
            returnMap.put('dashboardData', dashboardService.getWeeklyDashboardView(dashboardDate));
    	}
    	else{
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
    		returnMap.put('error', 'Date parameter required');
    	}

        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));
	}
}