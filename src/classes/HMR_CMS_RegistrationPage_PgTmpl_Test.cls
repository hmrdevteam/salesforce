/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_RegistrationPage_PageTemplate
*/

@isTest
private class HMR_CMS_RegistrationPage_PgTmpl_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_RegistrationPage_PageTemplate registrationPageController = new HMR_CMS_RegistrationPage_PageTemplate();
   // String propertyValue = plansContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
    HMR_CMS_RegistrationPage_PageTemplate.PicklistOption pickVal;
    pickVal = new HMR_CMS_RegistrationPage_PageTemplate.PicklistOption('General','General');
    
    String GeneralOrEmployeeRegistration = registrationPageController.GeneralOrEmployeeRegistration;
    
    String GeneralOrEmployeeRegistrationPicklistOptions = registrationPageController.GeneralOrEmployeeRegistrationPicklistOptions;
    
    registrationPageController.getHTML();
    
    pickVal = new HMR_CMS_RegistrationPage_PageTemplate.PicklistOption('test','test');
    
    String respString = registrationPageController.getHTML();
    
    System.assert(!String.isBlank(respString));
  }
  
}