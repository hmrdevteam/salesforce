/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/07/2018
 * Last Modified Date: 03/07/2018
 * Last Modified By:
 * Description: Test factory for HMR easy object creation without insertion
 * 	ONLY use from within test classes
 * ****************************************************/
public with sharing class HMR_TestFactory {
	private static final Map<String, Schema.SObjectType> GlobalDescribe = Schema.getGlobalDescribe();

	public static ActivityLog__c createActivityLog(string programMembershipId, string contactId){
		ActivityLog__c activityLogRecord = (ActivityLog__c)createSObject('ActivityLog__c');

		activityLogRecord.BenefitBars__c = 1;
		activityLogRecord.Contact__c = contactId;
		activityLogRecord.CurrentWeight__c = 200;
		activityLogRecord.DateDataEnteredFor__c = Date.today();
		activityLogRecord.Entrees__c = 2;
		activityLogRecord.FruitsVegetables__c = 5;
		activityLogRecord.Program_Membership__c = programMembershipId;
		activityLogRecord.ShakesCereals__c = 3;
		activityLogRecord.Water__c = 8;
		
		return activityLogRecord;
	}

	public static Contact createContact(){
		Contact contactRecord = (Contact)createSObject('Contact');

		contactRecord.FirstName = 'Sample';
        contactRecord.LastName = 'Contact';
        contactRecord.MailingStreet = '111 Rodeo Dr.';
        contactRecord.MailingCity = 'Beverly Hills';
        contactRecord.MailingState = 'CA';
        contactRecord.MailingPostalCode = '90210';

		return contactRecord;
	}

	public static Class__c createClass(string programId, string coachId){
		Class__c classRecord = (Class__c)createSObject('Class__c');

		classRecord.hmr_Active__c = true;
		classRecord.hmr_Active_Date__c = Date.today();
		classRecord.hmr_Class_Day__c = 'Wednesday';
		classRecord.hmr_Class_Name__c = 'Wednesday 6:30p P2 Bill R';
		classRecord.hmr_Coach__c = coachId;
		classRecord.hmr_Start_Time__c = '11:00 AM';
        classRecord.hmr_End_Time__c ='12:00 PM';
        classRecord.hmr_First_Class_Date__c=date.newInstance(2016, 11, 11);                                       
        classRecord.hmr_Active_Date__c=date.newInstance(2016, 10, 11);
        classRecord.hmr_Class_Type__c='P1 PP';
        classRecord.hmr_Coference_Call_Number__c='1234';
        classRecord.hmr_Time_of_Day__c='Noon';
        classRecord.hmr_Participant_Code__c='Placeholder';
        classRecord.hmr_Host_Code__c='Placeholder';
        classRecord.Program__c = programId;

		return classRecord;
	}

	public static Class_Member__c createClassMember(string classId, string contactId, string coachId, string programMembershipId){
		Class_Member__c classMemberRecord = (Class_Member__c)createSObject('Class_Member__c');

		classMemberRecord.hmr_Active__c = true;
		classMemberRecord.hmr_Class__c = classId;
		classMemberRecord.hmr_Start_Date__c = Date.today();
		classMemberRecord.hmr_Coach__c = coachId;
		classMemberRecord.hmr_Contact_Name__c = contactId;
		classMemberRecord.hmr_Program_Membership__c = programMembershipId;

		return classMemberRecord;
	}

	public static Coach__c createCoach(string userId){
		User userRecord = new User(Id = userId, hmr_IsCoach__c = true);
		update userRecord;

		Coach__c coachRecord = (Coach__c)createSObject('Coach__c');

		coachRecord.hmr_Active__c = true;
		coachRecord.hmr_Coach__c = userRecord.Id;
		coachRecord.hmr_Email__c = 'samplecoach@hmr.com';
		coachRecord.hmr_Class_Strength__c = 20;

		return coachRecord;
	}

	public static Coaching_Session__c createCoachingSession(string classId, string coachId){
		Coaching_Session__c coachingSessionRecord = (Coaching_Session__c)createSObject('Coaching_Session__c');

		coachingSessionRecord.hmr_Class__c = classId;
		coachingSessionRecord.hmr_Coach__c = coachId;
		coachingSessionRecord.hmr_Class_Date__c = Date.today();
		coachingSessionRecord.hmr_End_Time__c = '12:00 PM';
		coachingSessionRecord.hmr_Start_Time__c = '11:00 AM';

		return coachingSessionRecord;
	}

	public static List<Handouts__c> createHandoutList(string programId, integer count){
		List<Handouts__c> handoutList = new List<Handouts__c>();

		for(integer i = 0; i < count; i++){
			Handouts__c handoutRecord = createHandout(programId);

			handoutRecord.Title__c += '_' + String.valueOf(i);
			handoutRecord.Order__c = handoutRecord.Order__c * i;

			handoutList.add(handoutRecord);
		}

		return handoutList;
	}

	public static Handouts__c createHandout(string programId){
		Handouts__c handoutRecord = (Handouts__c)createSObject('Handouts__c');

		handoutRecord.Active__c = true;
		handoutRecord.Program__c = programId;
		handoutRecord.Title__c = 'Transition Structure';
		handoutRecord.Order__c = 1000;

		return handoutRecord;
	}

	public static Program__c createProgram(){
		Program__c programRecord = (Program__c)createSObject('Program__c');

		programRecord.Name = 'P1 Healthy Solutions';
		programRecord.hmr_Has_Phone_Coaching__c = true;
		programRecord.hmr_Program_Type__c = 'Healthy Solutions';
		programRecord.Days_in_1st_Order_Cycle__c = 30;

		return programRecord;
	}

	public static Program_Membership__c createProgramMembership(string programId, string contactId){
		Program_Membership__c programMembershipRecord = (Program_Membership__c)createSObject('Program_Membership__c');

		programMembershipRecord.Program__c = programId; 
        programMembershipRecord.hmr_Active__c = true;
        programMembershipRecord.hmr_Status__c = 'Active';
        programMembershipRecord.hmr_Contact__c = contactId;
        programMembershipRecord.hmr_Diet_Start_Date__c = Date.today();
        programMembershipRecord.DietType__c = 'Healthy Solutions 3+2+5';

        return programMembershipRecord;
	}

	public static Session_Attendee__c createSessionAttendee(string coachingSessionId, string classMemberId, string contactId){
		Session_Attendee__c sessionAttendeeRecord = (Session_Attendee__c)createSObject('Session_Attendee__c');

		sessionAttendeeRecord.hmr_Class_Member__c = classMemberId;
		sessionAttendeeRecord.hmr_Client_Name__c = contactId;
		sessionAttendeeRecord.hmr_Coaching_Session__c = coachingSessionId;

		return sessionAttendeeRecord;
	}

	public static List<Tracker_Note__c> createTrackerNoteList(string contactId, integer count){
		List<Tracker_Note__c> trackerNoteList = new List<Tracker_Note__c>();

		for(integer i = 0; i < count; i++){
			Tracker_Note__c noteRecord = createTrackerNote(contactId);

			noteRecord.Name += '_' + String.valueOf(i);

			trackerNoteList.add(noteRecord);
		}

		return trackerNoteList;
	}

	public static Tracker_Note__c createTrackerNote(string contactId){
		Tracker_Note__c trackerNoteRecord = (Tracker_Note__c)createSObject('Tracker_Note__c');

		trackerNoteRecord.Contact__c = contactId;
		trackerNoteRecord.Name = 'Tracker Note';
		trackerNoteRecord.Date_Time__c = DateTime.now();
		trackerNoteRecord.Notes__c = 'Test Note';

		return trackerNoteRecord;
	}

	public static User createUser(string profileName, boolean isCoach){
        String emailAddress = String.valueOf(System.now().getTime() + '@hmr-test.mail');

        Profile profile = [SELECT Id
	        			   FROM Profile
	       				   WHERE Name =: profileName LIMIT 1];

	    User userOwner = [SELECT TimeZoneSidKey, 
	        				     UserRoleId
	        			  FROM User
	        			  WHERE Id = :UserInfo.getUserId()];

	    User userRecord = new User(
                Alias = 'sampusr',
                Email = emailAddress,
                EmailEncodingKey = 'UTF-8',
                LastName = 'TestUser',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = profile.Id,
                TimeZoneSidKey = userOwner.TimeZoneSidKey,
                Username = emailAddress,
                isActive = true,
                ccrz__CC_CurrencyCode__c = NULL,
                hmr_isCoach__c = isCoach
        );

        return userRecord;
	}

	public static List<SObject> createSObjectList(string objectType, integer count){
		List<SObject> sobjectList = new List<SObject>();

		for(Integer i=0; i<count; i++ ){
			sobjectList.add(createSObject(objectType));
		}

		return sObjectList;
	}

	public static SObject createSObject(string objectType){
		Schema.sObjectType objectTypeDescribe = GlobalDescribe.get(objectType);

		if(objectTypeDescribe == null){
			throw new HMR_TestFactoryException('Invalid ObjectType: ' + objectType);
		}

		SObject obj = objectTypeDescribe.newSObject();

		return obj;
	}

	public class HMR_TestFactoryException extends Exception{}
}