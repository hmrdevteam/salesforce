/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 29th, 2017
Target Class(ses): HMR_Cybersource_Utility
*/

@isTest
private class HMR_Cybersource_Utility_Test{
  
  @isTest static void test_general_method() {
  
          Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert userTest; 
         
         
          Map<String, Object> signedMap = new Map<String, Object>();
          Map<String, Object> unSignedMap = new Map<String, Object>();
          
          List<String> singedNameList = new List<String>();
          singedNameList.add('test');
          
          List<String> unSingedNameList = new List<String>();
          unSingedNameList.add('test');
          
           Map<String, String> signedFieldValuesMap = new  Map<String, String>();
           signedFieldValuesMap.put('test','test');
           
           Map<String, String> unSignedFieldValuesMap = new  Map<String, String>();
           unSignedFieldValuesMap.put('test','test');
           
           Map<String, Object> carRespMap = new  Map<String, Object>();
      //     carRespMap.put('cartTotalAmount', 124);
        //   carRespMap.put('outputData', adb);
           
           Map<String, String> userAddrMap = new  Map<String, String>();
           userAddrMap.put('test','test');
          
          HMR_Cybersource_Utility.signedDtString = 'testsinged';
          HMR_Cybersource_Utility.signedFields = signedMap;
          HMR_Cybersource_Utility.unsignedFields = unSignedMap;
          HMR_Cybersource_Utility.signedFieldNames = singedNameList;
          HMR_Cybersource_Utility.unsignedFieldNames = unSingedNameList;
          HMR_Cybersource_Utility.signedFieldValues = signedFieldValuesMap;
          HMR_Cybersource_Utility.unSignedFieldValues = unSignedFieldValuesMap;
          HMR_Cybersource_Utility.cartResponseMap = carRespMap;
          HMR_Cybersource_Utility.userAddressMap= userAddrMap;
          
          HMR_Cybersource_Utility controllerObj = new HMR_Cybersource_Utility();
          
          HMR_Cybersource_Utility.generatePOSTString();
          
          HMR_Cybersource_Utility.initSignedFieldValues('card', 'test');
          
          HMR_Cybersource_Utility.initSignedFieldValuesUsingReferenceTime('card', 'test', '01/01/2017','01/01/2017');
          HMR_Cybersource_Utility.authorizePayment(userTest.id,userAddrMap, unSignedFieldValuesMap);
          HMR_Cybersource_Utility.authorizePaymentAndCreatePaymentToken(userTest.id,userAddrMap, unSignedFieldValuesMap);
          HMR_Cybersource_Utility.authorizeUsingPaymentToken(userTest.id,userAddrMap,'testtoken');
          HMR_Cybersource_Utility.createPaymentToken(userTest.id,userAddrMap, unSignedFieldValuesMap);
          HMR_Cybersource_Utility.createPaymentTokenUsingReferenceTime(userTest.id,userAddrMap, unSignedFieldValuesMap, '01/01/2017','01/01/2017');
          HMR_Cybersource_Utility.authorizePaymentAndUpdatePaymentToken(userTest.id,userAddrMap, 'testtoken',unSignedFieldValuesMap);
          HMR_Cybersource_Utility.updatePaymentToken(userTest.id,userAddrMap, 'testtoken',unSignedFieldValuesMap);
          Map<String, String> respMap = HMR_Cybersource_Utility.parseHTMLResponse('<div><input type="hidden" name="test" id="test" value="test"/></div><div><input type="hidden" name="test" id="test1" value="test"/></div><div><input type="hidden" name="test"/></div>');
          HMR_Cybersource_Utility.cancelPayment();
          HMR_Cybersource_Utility.voidPayment();
          HMR_Cybersource_Utility.issueRefund();
          
          Map<String, String> userAddrMapBlank = new  Map<String, String>();
          HMR_Cybersource_Utility.userAddressMap= userAddrMapBlank;

          cc_dataFActory.createCart();
          HMR_Cybersource_Utility.initCartId(cc_dataFActory.testUser.Id);
          HMR_Cybersource_Utility.initCartResponseMap();
          HMR_Cybersource_Utility.initSignedFieldValuesUsingReferenceTime('card', 'test', '01/01/2017','01/01/2017');
          
          System.assert(respMap.size() > 0);

    }
}