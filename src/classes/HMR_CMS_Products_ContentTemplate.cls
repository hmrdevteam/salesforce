global virtual with sharing class HMR_CMS_Products_ContentTemplate extends cms.ContentTemplateController{
    global HMR_CMS_Products_ContentTemplate(){}
    global HMR_CMS_Products_ContentTemplate(cms.createContentController cc){
        super(cc);
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }
    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    public Boolean is_eCommerce {
        get {
            return getAttribute('is_eCommerce') == 'true';
        }
    }

    public Boolean is_eCommerceAdmin {
        get {
            return getAttribute('is_eCommerceAdmin') == 'true';
        }
    }

    global String returnNotNullAndNotEmptyString(String value) {
        if(value != null && value != '') {
            return value;
        }
        return '';
    }

    public String ShakesHeaderText{
         get{
            return getAttribute('ShakesHeaderText');
        }
     }

     public String ShakesDescriptionText{
         get{
            return getAttribute('ShakesDescriptionText');
        }
     }

     public String EntreesHeaderText{
         get{
            return getAttribute('EntreesHeaderText');
        }
     }

     public String EntreesDescriptionText{
         get{
            return getAttribute('EntreesDescriptionText');
        }
     }

     public String BarsHeaderText{
         get{
            return getAttribute('BarsHeaderText');
        }
     }

     public String BarsDescriptionText{
         get{
            return getAttribute('BarsDescriptionText');
        }
     }
   
    global virtual override String getHTML() {
        String html = '';
        try {
            html +='<div class="info-wrapper-shakes">' +
                       '<div class="col-xs-12 filter-title">' +
                           '<h2>' + ShakesHeaderText + '</h2>' +
                       '</div>' +
                       '<div class="col-xs-12 hmr-desc-text filter-desc">' +
                                ShakesDescriptionText +
                       '</div>' +
                   '</div>';
            html +='<div class="info-wrapper-entrees hidden">' +
                       '<div class="col-xs-12 filter-title">' +
                           '<h2>' + EntreesHeaderText + '</h2>' +
                       '</div>' +
                       '<div class="col-xs-12 hmr-desc-text filter-desc">' +
                                EntreesDescriptionText +
                       '</div>' +
                   '</div>';
            html +='<div class="info-wrapper-bars hidden">' +
                       '<div class="col-xs-12 filter-title">' +
                           '<h2>' + BarsHeaderText + '</h2>' +
                       '</div>' +
                       '<div class="col-xs-12 hmr-desc-text filter-desc">' +
                                BarsDescriptionText +
                       '</div>' +
                   '</div>';
            //Get Product ID's for HMR Product Categories Start
            Map<String, Object> inputCategoryData = new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => 3,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICategory.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                    }
                }
            };
            Map<String, Object> outputCategoryData = ccrz.ccAPICategory.fetch(inputCategoryData);
            //Declare set of string to store product Ids for API call
            Set<String> product_sfidSet = new Set<String>();
            if(Test.isRunningTest()){//If running a test, run alternate code to populate product_sfidSet so you can avoid exceeding CPU limit
                //(FOR TESTING PURPOSES)Declare a Set of string containing a limited amount of SKUs to avoid exceeding CPU limit
                Set<String> testSkuSet = new Set<String>{'IBSN', 'IH120V', 'LCBAR'};
                if (outputCategoryData.get(ccrz.ccAPICategory.CATEGORYLIST) != null) {
                    List<Map<String, Object>> outputCategoryList = (List<Map<String, Object>>) outputCategoryData.get(ccrz.ccAPICategory.CATEGORYLIST);
                    for(Map<String, Object> outputCategoriesMap: outputCategoryList) {
                        if(outputCategoriesMap.get('sfdcName') == 'HMR Products') {
                            List<Map<String, Object>> outputProductCategoriesList = (List<Map<String, Object>>) outputCategoriesMap.get('productCategoriesS');
                            if(outputProductCategoriesList != null && outputProductCategoriesList.size() > 0) {
                                for(Map<String, Object> outputProductCategoriesMap: outputProductCategoriesList) { 
                                    if(outputProductCategoriesMap.get('productR') != null) {
                                        Map<String, Object> outputProductInfoMap = (Map<String, Object>) outputProductCategoriesMap.get('productR');
                                        //If test is running add product Id only if the SKU is in the test SKU set
                                        if(testSkuSet.contains((String)outputProductInfoMap.get('SKU'))){
                                            product_sfidSet.add((String)outputProductCategoriesMap.get('product'));
                                        }                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                product_sfidSet = HMR_CMS_CCRZ_Utility.getProduct_sfidSet_forCategory(outputCategoryData, 'HMR Products');
            }
            //Get Product ID's for HMR Product Categories End
            
            Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => 6,
                ccrz.ccApiProduct.PRODUCTIDLIST => product_sfidSet,
                ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
                ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
                ccrz.ccAPIProduct.PARAM_BY_ASC => true,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                    }
                }
            };
            Map<String, Object> outputProductData = ccrz.ccAPIProduct.fetch(inputData);
            if(outputProductData != null && outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
                List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);
                if(outputProductList.size() > 0) {
                    //Price Lists Logic Start
                    //Set the Product SKU's
                    Set<String> productSKUSet = HMR_CMS_CCRZ_Utility.getProduct_keySet(outputProductList, 'SKU');
                    //Price lists Logic Map Input setup
                    Map<String, Object> inputPriceListsData = new Map<String, Object>{
                        ccrz.ccAPIPriceList.PRODUCTSKUS => productSKUSet,
                        ccrz.ccAPI.API_VERSION => 6,
                        ccrz.ccAPI.SIZING => new Map<String, Object>{
                            ccrz.ccAPIPriceList.ENTITYNAME => new Map<String, Object>{
                                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                            }
                        }
                    };
                    Map<String, Object> outputPriceListsData = ccrz.ccAPIPriceList.fetch(inputPriceListsData);
                    Map<String, Map<String, String>> productPriceListsMap = HMR_CMS_CCRZ_Utility.getProductPriceListsMap(outputPriceListsData);
                    Map<String, String> employeesPriceList = productPriceListsMap.get('employeesPriceListMap');
                    Map<String, String> friendsandfamilyPriceList = productPriceListsMap.get('friendsandfamilyPriceListMap');
                    Map<String, String> standardPriceList = productPriceListsMap.get('standardPriceListMap');
                    //Price Lists Logic End

                    //Get Product Ingredients Logic Start
                    Set<String> product_sfid_Set = HMR_CMS_CCRZ_Utility.getProduct_keySet(outputProductList, 'sfid');
                    Map<String, String> productIngredientsMap = HMR_CMS_CCRZ_Utility.getProductIngredients(product_sfid_Set);
                    //Get Product Ingredients Logic End


                    //Product Specs Logic Start
                    Set<String> productSpecIDSet = HMR_CMS_CCRZ_Utility.getProductSpecIDSet(outputProductList);
                    Map<String, Object> inputProductSpecData = new Map<String, Object>{
                        ccrz.ccAPISpec.SPECIDLIST => productSpecIDSet,
                        ccrz.ccAPI.API_VERSION => 3
                    };
                    Map<String, Object> outputProductSpectData = ccrz.ccAPISpec.fetch(inputProductSpecData);
                    List<Map<String, Object>> productSpecsMainList = new List<Map<String, Object>>();
                    if (outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST) != null) {
                        productSpecsMainList = (List<Map<String, Object>>) outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST);
                    }
                    String calorieSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Calories');
                    String totalServingsSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Total Servings');
                    String proteinSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Protein');
                    String servingSizeSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Serving Size');
                    //Product Specs Logic End


                    //Product HTML rendering Loop Start
                    Integer tabIndex = 0;
                    Map<String, List<String>> finalProductCategoryMapWithSKU = HMR_CMS_CCRZ_Utility.finalProductCategoryMap(outputCategoryData);
                    for(Map<String, Object> productObject: outputProductList) {
                        tabIndex++;
                        String productMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image');
                        String productThumbnailMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image Thumbnail');
                        //Spec Logic Start
                        String allergenHTML = HMR_CMS_CCRZ_Utility.getProductAllergenInfoHTML(productObject);
                        String calorieSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, calorieSpecId);
                        String totalServingsSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, totalServingsSpecId);
                        String protienSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, proteinSpecId);
                        String servingSizeSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, servingSizeSpecId);
                        //Spec Logic End

                        //Product List HTML rendering Start
                        //!String.isEmpty(calorieSpecification) &&
                        if(String.isEmpty(calorieSpecification))
                            calorieSpecification = 'N/A';

                        if(is_eCommerceAdmin){
                            if(String.isEmpty(totalServingsSpecification)){
                                totalServingsSpecification = 'N/A';
                            }
                            if(String.isEmpty(protienSpecification)){
                                protienSpecification = 'N/A';
                            }
                        }

                        if(productObject.get('productStatus') == 'Released' &&  !String.isEmpty(totalServingsSpecification)) {
                            //Get Product Nutritions Map Start
                            Map<String, String> productNutritionsMap = HMR_CMS_CCRZ_Utility.getProductNutritionsMap(productSpecsMainList, productObject);
                            //Get Product Nutritions Map End
                            String product_sfid = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfid')));
                            String productSKU = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('SKU')));
                            String productName = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfdcName')));
                            String productDescription = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('shortDesc')));
                            //String productIngredients = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('longDesc')));
                            String productIngredients = returnNotNullAndNotEmptyString(String.valueOf(productIngredientsMap.get(product_sfid)));
                            String containerType = productSKU.contains('120') ? 'can' : 'box';

                            List<String> productCategoriesList = finalProductCategoryMapWithSKU.get(productSKU);
                            String prodCategory = String.join(productCategoriesList, ',');
                                //Only create modals for products with total serving Specs to filter out items like Materials
                                if(prodCategory != 'Materials' && returnNotNullAndNotEmptyString(prodCategory) != ''){
                                    html += HMR_CMS_CCRZ_Utility.getProductDetails_IngredientsModalHTML(productIngredients, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
                                    html += HMR_CMS_CCRZ_Utility.getProductDetails_NutritionsModalHTML(productNutritionsMap, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
                                    html += HMR_CMS_CCRZ_Utility.getProductDetails_DescriptionModalHTML(productSKU, productThumbnailMediaURL, productName, productDescription, totalServingsSpecification,
                                                                                                    containerType, calorieSpecification, protienSpecification, servingSizeSpecification);
                                }

                                if(prodCategory == 'Materials' || prodCategory == 'Flavorings' || prodCategory == 'Bars'){
                                    html +='<div class="info-wrapper-bars hidden">';
                                }
                                if(prodCategory == 'Entrees'){
                                    html +='<div class="info-wrapper-entrees hidden">';
                                }
                                if(prodCategory.contains('Shake')){
                                    html +='<div class="info-wrapper-shakes">';
                                }

                                html += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' +
                                            '<div class="product-card" data-t="byCategory" data-categories="'+prodCategory+'">' +
                                                HMR_CMS_CCRZ_Utility.productDetailsCardHTML_NoeCommerce(productSKU, productMediaURL, productName, allergenHTML, calorieSpecification);
                                if(is_eCommerce) {
                                    html += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_eCommerce(totalServingsSpecification, productSKU, tabIndex,
                                                                                                  returnNotNullAndNotEmptyString(standardPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(employeesPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(friendsandfamilyPriceList.get(productSKU)), true);
                                    }

                            html += '</div>' + '</div>' + '</div>' + '</div>';
                        }
                        //Product List HTML rendering End
                    }
                    //Product HTML rendering Loop End
                }
            }
        }
        catch(Exception ex) {
            html += 'Something went wrong ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
        }
        return html;
    }
}