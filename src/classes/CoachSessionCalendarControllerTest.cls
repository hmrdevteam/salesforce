/**
* Test Class for Controller of coaching session calendar
* Objects referenced --> Coaching_Session__c,Coach__c,Program__c,
*
* @Date: 2016-12-08
* @Author Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-710
*/

@isTest
public with sharing class CoachSessionCalendarControllerTest {

static testMethod void TestCalendarFetchData() {


        Test.startTest();
        //Declare variable to store today's date
        Date dt = Date.today();

        //Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Declare a new list to store Users
        List<User> userList = new List<User>();

        //Create a Coach Users
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        userList.add(userObj);
        User userObj1 = new User(FirstName = 'Test', LastName='Coach1', Email='Test1@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1231@testing.com', Alias = 'Test1');
        userList.add(userObj1);
        User userObj2 = new User(FirstName = 'Test', LastName='Coach2', Email='Test2@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1232@testing.com', Alias = 'Test2');
        userList.add(userObj2);
        //Insert User List
        insert userList;


        // Create Coach Data

        List<Coach__c> coachList = new List<Coach__c>();
        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        coachList.add(coachObj);
        Coach__c coachObj1 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test1@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj1);
        Coach__c coachObj2 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test2@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj2);
        //Insert Coach
        insert coachList;


        //Declare a new list to store Programs
        List<Program__c> progList = new List<Program__c>();
        //Create new Programs and add them to conList to insert into the database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', hmr_Program_Display_Name__c = 'TestProg1HSS',
                                            hmr_Program_Description__c = 'P1 Healthy Shakes Description', hmr_Program_Type__c = 'Healthy Shakes',
                                            hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        progList.add(programObj1);
        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', hmr_Program_Display_Name__c = 'TestProg1HSAH',
                                            hmr_Program_Description__c = 'Test Prog 1 HSS Description', hmr_Program_Type__c = 'Healthy Solutions',
                                            hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj2);
        Program__c programObj3 = new Program__c(Name = 'Phase 2', hmr_Program_Display_Name__c = 'TestProg2',
                                            hmr_Program_Description__c = 'Test Prog Phase 2 Description', hmr_Program_Type__c = 'P2',
                                            hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj3);
        //insert List of Programs
        insert progList;


        //Declare a new List of Class object
        List<Class__c> clsList = new List<Class__c>();
        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Shakes Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj1);
        Class__c classObj = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Solutios Class', hmr_Coach__c = coachObj1.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Evening',hmr_Start_Time__c = '5:00 PM', hmr_End_Time__c = '6:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj);
        Class__c classObj2 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P2 Class', hmr_Coach__c = coachObj2.id, Program__c = programObj3.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P2 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Afternoon',hmr_Start_Time__c = '3:00 PM', hmr_End_Time__c = '4:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj2);
        //Insert Classes
        insert clsList;

        System.runAs(userObj){

            Test.setCurrentPageReference(new PageReference('Page.SiteLogin'));
            System.currentPageReference().getParameters().put('minDate', String.valueOf(Date.today()));
            System.currentPageReference().getParameters().put('maxDate', String.valueOf(Date.today().addDays(7)));

            CoachSessionCalendarController.calEvent calEve = new CoachSessionCalendarController.calEvent();
            calEve.title = 'testtitle';
            calEve.allDay = TRUE;
            calEve.startString  = '04/04/2017';
            calEve.endString  = '04/05/2017';
            calEve.url = 'test@test.com';
            calEve.className = 'testclass';

            String checkresult= CoachSessionCalendarController.eventdata(coachObj.id+'::Phase1' );//+programObj1.id
            String checkresult2= CoachSessionCalendarController.eventdata(coachObj.id+'::Phase2' );//+programObj1.id
            CoachSessionCalendarController obj=New CoachSessionCalendarController();
            obj.getCoachList();
            obj.GetCurrentUserDetails();
            obj.CoachId=coachObj.id;
            obj.CurrentUserIsCoach=false;
            //obj.CurrentUserId=coachObj1.id;
            obj.eventsJSON='test';

            system.assert(checkresult != null, 'Json String Created!');

            Test.stopTest();
        }
    }

    //Update testMethod for new Version of Calendar Page/Controller
    static testMethod void TestNewCalendarFetchData() {

        //Declare variable to store today's date
        Date dt = Date.today();

        //Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Declare a new list to store Users
        List<User> userList = new List<User>();

        //Create a Coach Users
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        userList.add(userObj);
        User userObj1 = new User(FirstName = 'Test', LastName='Coach1', Email='Test1@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1231@testing.com', Alias = 'Test1');
        userList.add(userObj1);
        User userObj2 = new User(FirstName = 'Test', LastName='Coach2', Email='Test2@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1232@testing.com', Alias = 'Test2');
        userList.add(userObj2);
        //Insert User List
        insert userList;


        // Create Coach Data

        List<Coach__c> coachList = new List<Coach__c>();
        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        coachList.add(coachObj);
        Coach__c coachObj1 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test1@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj1);
        Coach__c coachObj2 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test2@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj2);
        //Insert Coach
        insert coachList;


        //Declare a new list to store Programs
        List<Program__c> progList = new List<Program__c>();
        //Create new Programs and add them to conList to insert into the database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', hmr_Program_Display_Name__c = 'TestProg1HSS',
                                            hmr_Program_Description__c = 'P1 Healthy Shakes Description', hmr_Program_Type__c = 'Healthy Shakes',
                                            hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        progList.add(programObj1);
        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', hmr_Program_Display_Name__c = 'TestProg1HSAH',
                                            hmr_Program_Description__c = 'Test Prog 1 HSS Description', hmr_Program_Type__c = 'Healthy Solutions',
                                            hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj2);
        Program__c programObj3 = new Program__c(Name = 'Phase 2', hmr_Program_Display_Name__c = 'TestProg2',
                                            hmr_Program_Description__c = 'Test Prog Phase 2 Description', hmr_Program_Type__c = 'P2',
                                            hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj3);
        //insert List of Programs
        insert progList;


        //Declare a new List of Class object
        List<Class__c> clsList = new List<Class__c>();
        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Shakes Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj1);
        Class__c classObj = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Solutios Class', hmr_Coach__c = coachObj1.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Evening',hmr_Start_Time__c = '5:00 PM', hmr_End_Time__c = '6:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj);
        Class__c classObj2 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P2 Class', hmr_Coach__c = coachObj2.id, Program__c = programObj3.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P2 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Afternoon',hmr_Start_Time__c = '3:00 PM', hmr_End_Time__c = '4:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj2);
        //Insert Classes
        insert clsList;

        Test.startTest();

        System.runAs(userObj){

            Test.setCurrentPageReference(new PageReference('Page.HMR_CoachingCalendar'));

            HMR_CoachingCalendarController obj = new HMR_CoachingCalendarController();
            obj.getCoachList();
            obj.getProgramList();
            obj.coachId = coachObj1.Id;
            obj.programId = programObj3.Id;
            obj.pageLoad();

            Test.stopTest();
        }

    }
}