/**
* Test Class Coverage of the HMR_CMS_CCRZ_Order_ServiceInterface
*
* @Date: 11/23/2017
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA: 
*/
@isTest
private class HMR_CMS_CCRZ_Order_ServInt_Test{
  
    @isTest
    private static void firstTestMethod(){
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='test1122@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test1122@testorg.com');
        
        insert u;
          
       
       Map<String, String> params = new Map<String, String>();
       params.put('action','getOrder');
       params.put('userId',u.id);
               
        HMR_CMS_CCRZ_Order_ServiceInterface orderServiceInerface = new HMR_CMS_CCRZ_Order_ServiceInterface();   
        orderServiceInerface.executeRequest(params) ;       
        params.put('action','getOrderByOwnerId');
        orderServiceInerface.executeRequest(params) ;
        params.put('action','invalid');
        orderServiceInerface.executeRequest(params) ;
        params.put('action',null);
        orderServiceInerface.executeRequest(params) ;
        params.put('userId',null);
        orderServiceInerface.executeRequest(params);
    }
    
    @isTest
    private static void secondTestMethod(){
    
        HMR_CMS_CCRZ_Order_ServiceInterface.getType();
    
    }
    
}