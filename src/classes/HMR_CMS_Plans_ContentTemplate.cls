/**
* Apex Content Template Controller for Hero Sections on Plans Page
*
* @Date: 03/20/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Joey Zhuang 03/20/2017
* @Modified: Jay Zincone (Health Management Resources) - 2017-12-11 : https://hmrjira.atlassian.net/browse/HMP-55
* @JIRA:
*/

global virtual with sharing class HMR_CMS_Plans_ContentTemplate extends cms.ContentTemplateController{

    global HMR_CMS_Plans_ContentTemplate(cms.createContentController cc){
        super(cc);
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }



    //planName Property
    public String planName{
        get{
            return getAttribute('planName');
        }
    }

    //join now Link property
    //public cms.Link joinLinkLinkObj{
    //    get{
    //        return this.getPropertyLink('joinLinkLinkObj');
    //    }
    //}
    //Hero Background Image Property
    public String planImage1{
        get{
            return getProperty('planImage1');
        }
    }

    //Hero Background Image Property
    public String planImage2{
        get{
            return getProperty('planImage2');
        }
    }

    //Hero Background Image Property
    public String planImage3{
        get{
            return getProperty('planImage3');
        }
    }

    //Hero Background Image Property
    public String planImage4{
        get{
            return getProperty('planImage4');
        }
    }

    //Hero Background Image Property
    public String planImage5{
        get{
            return getProperty('planImage5');
        }
    }

    //Hero Background Image Property
    public String planImage6{
        get{
            return getProperty('planImage6');
        }
    }


    //list of programs
    public list<Program__c> pList {get;set;}

    //if current user is the guest user
    public Boolean isGuestUser {get;set;}

    //if healthy shake is an option
    public Boolean isShakeOption {get;set;}

    //price, sku for shake and solutions
    public Decimal disctPrice {get;set;}
    public Decimal stdPrice {get;set;}
    public Decimal pricePerDay {get;set;}
    public String sku {get;set;}

    public String numberOfEntress {get;set;}
    public String numberOfCerealServings {get;set;}
    public String numberOfShakesServings {get;set;}
    public String numberOfSampleEntress {get;set;}

    public String debugString {get;set;}
 //   public Integer disctPriceShake {get;set;}
 //   public Integer stdPriceShake {get;set;}
    //public String skuShake {get;set;}
    global HMR_CMS_Plans_ContentTemplate(){

    }


    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup
        //init all var
        isGuestUser = true;
        isShakeOption = true;
        disctPrice = 0;
        stdPrice = 0;
        pricePerDay = 0;
        sku = '';
        numberOfEntress = '';
        numberOfCerealServings = '';
        numberOfShakesServings = '';
        numberOfSampleEntress = '';

        debugString = '';

        //current user Id
        String userId = UserInfo.getUserId();
        //current user profile Id
        String userProfileId = UserInfo.getProfileId();

        //check if current user is the site guest user
        Profile guestProfile = [SELECT Id, Name FROM Profile WHERE Id =: userProfileId];
        isGuestUser = (guestProfile.Name == 'HMR Program Community Profile');

        //retrieve all programs and preset the value for guest user
        pList = new list<Program__c>();
        pList = [SELECT Id,Standard_Kit__r.ccrz__SKU__c, Default_Promotional_Discount__c, hmr_Program_Type__c,
                hmr_Standard_Kit_Price__c,hmr_Number_of_Cereal_Servings__c, Price_per_Day__c,
                hmr_Number_of_Entrees__c,hmr_Number_of_Sample_Entrees__c, hmr_Number_of_Shakes_Servings__c FROM Program__c where hmr_Phase_Type__c = 'P1' ];

        //system.debug('@@@@@@Anydatatype_msg  '+ planName+pList.size()+isGuestUser);
        if(pList.size()>0){
            for(Program__c p: pList){
                //debugString += '++'+p.hmr_Program_Type__c+(p.hmr_Program_Type__c == planName)+'***'+planName;
                //assign value from programs
                if(p.hmr_Program_Type__c == planName){//'Healthy Solutions'
                    if(p.Default_Promotional_Discount__c!=null)
                    disctPrice = p.Default_Promotional_Discount__c;
                    if(p.hmr_Standard_Kit_Price__c!=null)
                    stdPrice = p.hmr_Standard_Kit_Price__c - disctPrice;
                    pricePerDay = p.Price_per_Day__c;
                    if(p.Standard_Kit__r!=null)
                    sku = p.Standard_Kit__r.ccrz__SKU__c;
                    if(p.hmr_Number_of_Entrees__c!=null)
                    numberOfEntress = ''+p.hmr_Number_of_Entrees__c;
                    if(p.hmr_Number_of_Sample_Entrees__c!=null)
                    numberOfSampleEntress = ''+p.hmr_Number_of_Sample_Entrees__c;
                    if(p.hmr_Number_of_Cereal_Servings__c!=null)
                    numberOfCerealServings = ''+p.hmr_Number_of_Cereal_Servings__c;
                    if(p.hmr_Number_of_Shakes_Servings__c!=null)
                    numberOfShakesServings = ''+p.hmr_Number_of_Shakes_Servings__c;
                }
            }
        }

        //if current user is community user but not site user
        if(!isGuestUser){
            //get the contact
            User curUser = [Select Id, ContactId from User where Id =: UserInfo.getUserId()];
            if(curUser.ContactId != null){
                Id cId = curUser.ContactId;
                Contact con = [Select Account.RecordType.DeveloperName,
                Account.hmr_Is_Healthy_Shakes_an_Option__c,
                Account.Standard_HSAH_Kit_Discount__c,
                Account.Standard_HSS_Kit_Discount__c,
                Account.hmr_HSAH_Standard_Kit_Price__c,
                Account.hmr_HSS_Standard_Kit_Price__c From Contact where Id = : cId];//Corporate_Account

                //if cooperate account then use the value of hmr_Is_Healthy_Shakes_an_Option__c
                if(con.Account.RecordType.DeveloperName == 'Corporate_Account'){
                    isShakeOption = con.Account.hmr_Is_Healthy_Shakes_an_Option__c;
                }


                //get hte price from
                if(planName == 'Healthy Solutions'){
                    if(con.Account.Standard_HSAH_Kit_Discount__c!=null)
                    disctPrice = con.Account.Standard_HSAH_Kit_Discount__c;
                    if(con.Account.hmr_HSAH_Standard_Kit_Price__c!=null)
                    stdPrice = con.Account.hmr_HSAH_Standard_Kit_Price__c - disctPrice;
                }

                if(planName == 'Healthy Shakes'){
                    if(con.Account.Standard_HSS_Kit_Discount__c!=null)
                    disctPrice = con.Account.Standard_HSS_Kit_Discount__c;
                    if(con.Account.hmr_HSS_Standard_Kit_Price__c!=null)
                    stdPrice = con.Account.hmr_HSS_Standard_Kit_Price__c - disctPrice;
                }
                system.debug('######Anydatatype_msg  '+ isShakeOption+disctPrice+stdPrice);
            }
        }

        //loop all selected program record, assign value for each plan

        String html = '';
        String sitePrefix = Site.getBaseUrl();

        if(planName=='Healthy Solutions'){
            //select tabs content
            html +='<li href="#HealthySolutions" class="selected" id="healthSolutions_li" onclick="showHealthySolutions();">';
                html +='<h3 class="tab-product">'+planName+debugString+'</h3>';
                html +='<h3 class="tab-description">3-week Quick Start Kit</h3>';
                html +='<h3 class="tab-description" >$'+stdPrice+' ($'+pricePerDay+' per day)</h3>';
            html +='</li>';

            //mobile version menu tab
            html +='<div class="row mobile_row first-mobile-plan" id="healthSolutions_li_mob">';
                html +='<a href="#HealthySolutions">'+planName+'</a>';
                html +='<h3 class="mobile-kit">3-WEEK QUICK START KIT</h3>';
                html +='<h3 class="mobile-kit">$'+stdPrice+'</h3>';
            html +='</div>';


            html +='<main id="HealthySolutions" class="tab-content">';
                    html +='<section id="mobile" class="clearfix common-height">';
                        html +='<img src="'+sitePrefix+planImage1+'" class="full-width"/>';

                        html +='<div class="container mobile-blue-bar" >';
                        html +='<div class="row mobile_row">';
                                html +='<a href="#HealthySolutions">HEALTHY SOLUTIONS AT HOME</a>';
                                html +='<h3 class="mobile-kit">3-WEEK QUICK START KIT</h3>';
                                html +='<h3 class="mobile-kit">$'+stdPrice+'</h3>';
                            html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section class="landing-section section-padding">';
                        html +='<div class="container container-initial">';
                            html +='<h3 class="first-title text-left">Take Total Control of Your Eating Environment with Healthy Solutions  at Home</h3>';
                            html +='<p class="first-paragraph margin-top-50">Healthy Solutions® at Home is a comprehensive weight-loss plan that uses HMR foods and guidance from expert coaches to prepare you for a lifetime of healthier living. Thousands of people have already achieved their weight-loss goals with Healthy Solutions and left the program with all the tools and skills they need to maintain it.</p>';
                            html +='<div class="margin-top-50 text-center">';
                                html +='<button class="btn btn-primary" onclick="location.href=\'ccrz__ProductDetails?SKU='+sku+'\'">Join Today</button>';
                            html +='</div>';
                        html +='</div>';
                        html +='<div class="container">';
                        html +='<div id="nonMobilePhoto" class="row text-center" >';
                            html +='<img src="'+sitePrefix+planImage2+'" class="full-width"/> ';
                        html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section class="hero-area" style="background-color: #FF7349;">';
                        html +='<div class="container mobile-padding">';
                            html +='<div class="row">';
                                html +='<div class="col-md-6 text-center">';
                                    html +='<img src="'+sitePrefix+planImage3+'" class="img-responsive"/>';
                                html +='</div>';
                                html +='<div class="col-md-6">';
                                    html +='<div class="block">';
                                       html +=' <h3 class="section-header-white">Food</h3>';
                                            html +='<p class="section-text-white">With Healthy Solutions you’ll eat a minimum of 3 HMR Shakes, 2 HMR Entrees, and 5 servings of fruits and vegetables. This system allows you to take a break from your current eating habits and focus on losing weight while always giving you the option to eat more if you feel hungry. </p>';
                                    html +='</div>';
                                html +='</div>';
                            html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section class="hero-area full-picture-padding">';
                        html +='<div class="container mobile-padding">';
                            html +='<div class="row">';
                                html +='<div class="col-md-5 text-picture-padding">';
                                    html +='<div>';
                                        html +='<h3 class="section-header">Coaching</h3>';
                                            html +='<p class="section-text">Losing weight takes more than willpower. That’s why everyone is encouraged take advantage of the coaching included in the Healthy Solutions at Home plan. HMR health coaches host weekly 1 hour group coaching sessions by phone. Coaches provide accountability and personalized advice for things like planning your meals, getting physical activity, and dealing with challenging food environments. </p>';
                                    html +='</div>';
                                html +='</div>';
                                html +='<div class="col-md-7 text-center">';
                                    html +='<img src="'+sitePrefix+planImage4+'" class="img-responsive"/>';
                                html +='</div>';
                            html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section class="hero-area" style="background-color: #F5F5F5;">';
                        html +='<div class="container mobile-padding">';
                            html +='<div class="row">';
                                html +='<div class="col-md-5 col-md-push-7">';
                                    html +='<div >';
                                        html +='<h3 class="section-header">Lose More with Coaching</h3>';
                                            html +='<p class="section-text">People who participate in phone coaching lose almost twice as much weight as those who complete the program on there own. In one clinical study, those who completed the program on their own lost a mean of 13 lbs. over the first 12 weeks. (Smith BK et al. Obes Res Clin Pract 2009) In another study, those who completed the program and attended group phone coaching lost a median of 23 lbs over the first 12 weeks.* </p>';
                                    html +='</div>';
                                html +='</div> ';
                                html +='<div class="col-md-7 col-md-pull-6 text-center">';
                                    html +='<img src="'+sitePrefix+planImage5+'" class="img-responsive"/>';
                                html +='</div>';
                            html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section class="landing-section clearfix section-padding green" >';
                        html +='<div class="col-xs-12 col-sm-6 landing-block-lg plans-img-col" style="background-image: url(\''+sitePrefix+planImage6+'\');"> </div>';
                        html +='<div class="col-xs-12 col-sm-6 col-lg-4 landing-block-lg plans-text-col-white">';
                            html +='<h3 class="first-title">Maintenance</h3>';
                            html +='<p class="first-paragraph">This program is designed with long-term weight maintenance in mind. You can’t diet forever, and we don’t want you to. That’s why we put such a strong focus on discovering the strategies that work best for you. After the weight is gone, we want you to keep it off by using the healthier eating, physical activity, and environmental control skills you picked up along the way. 2 </p>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section id="healthyshakesdetails" class="landing-section" >';
                        html +='<div class="container container-initial" >';
                            html +='<h3 class="first-title text-left">Healthy Solutions 3-week Quick Start Kit</h3>';
                            html +='<a class="right-side" >with auto delivery</a>';
                            html +='<div class="row" >';
                            html +='</div>';
                            html +='<div class="text-left spacing-top padding-top border">';
                                html +='<ul class="kit-list">';
                                    html +='<li>Weekly group phone coaching with and experienced HMR Coach is FREE with your auto delivery order</li>';
                                    html +='<li>'+numberOfShakesServings+' servings HMR 120 Shakes (chocolate, vanilla, or a mix of each)</li>';
                                    html +='<li>'+numberOfEntress+' assorted HMR Entrees</li>';
                                    html +='<li>'+numberOfCerealServings+' servings HMR Multigrain Hot Cereal</li>';
                                    html +='<li>'+numberOfSampleEntress+' serving sample HMR 70 Plus Chocolate Shake/Pudding </li>';
                                    html +='<li>Support Guides and HMR Recipe Book </li>';
                                    html +='<li>HMR Tracking App and Online Tools</li>';

                                html +='</ul>';
                                html +='<div class="col-xs-12 col-sm-8">';
                                    html +='<h6 class="discount-text">save $'+disctPrice+'</h6>';
                                html +='</div>';
                                html +='<div class="col-xs-12 col-sm-4">';
                                    html +='<h6 class="kit-price" >$'+stdPrice+'</h6>';
                                html +='</div>';
                            html +='</div>';
                            html +='<div class="col-xs-12 border">';
                                html += '<div class="row">';
                                    html += '<div class="col-sm-4">';
                                        html += '<button class="btn btn-primary join-today" onclick="location.href=\'/DefaultStore/health-terms\'">Health and Privacy Acknowledgement</button>';
                                    html += '</div>';
                                    html += '<div class="col-sm-4 col-sm-offset-4 text-center">';
                                        html +='<button class="btn btn-primary join-today"  onclick="location.href=\'ccrz__ProductDetails?SKU='+sku+'\'">Build Your Kit</button>';
                                    html += '</div>' +
                                    '</div>';
                            html +='</div>';
                        html +='</div>';
                    html +='</section>';
                    html +='<section id="mobile" class="clearfix common-height">';
                        html +='<div class="container mobile-blue-bar">';
                            html +='<div class="row mobile_row">';
                                html +='<div class="col-xs-10">';
                                    html +='<a href="#HealthySolutions">'+planName+'</a>';
                                    html +='<h3 class="mobile-kit">2-WEEK QUICK START KIT</h3>';
                                    html +='<h3 class="mobile-kit">$'+stdPrice+'</h3>';
                                html +='</div>';
                                html +='<div class="col-xs-2">';
                                    html +='<i class="fa white-arrow">&#xf178;</i>';
                                html +='</div>';
                            html +='</div>';
                        html +='</div>';

                    html +='</section>';
                html +='</main>';


        }else if(planName=='Healthy Shakes' && isShakeOption){
            html +='<li href="#HealthySkakes" onclick="showHealthyShakes();" id="healthShakes_li" style="background-image: url(\''+sitePrefix+planImage6+'\');">';
                html +='<h3 class="tab-product">'+planName+'</h3>';
                html +='<h3 class="tab-description">2-week Quick Start Kit</h3>';
                html +='<h3 class="tab-description" >$'+stdPrice+' ($'+pricePerDay+' per day)</h3>';
            html +='</li>';

            //mobile version menu tab
            html +='<div class="row mobile_row last-mobile-plan" id="healthShakes_li_mob">';
                html +='<a href="#HealthyShakes">'+planName+'</a>';
                html +='<h3 class="mobile-kit">2-WEEK QUICK START KIT</h3>';
                html +='<h3 class="mobile-kit">$'+stdPrice+'</h3>';
            html +='</div>';



            html+='<main id="HealthyShakes" class="tab-content">';
                html+='<section id="mobile" class="clearfix common-height">';
                    html+='<img src="'+sitePrefix+planImage1+'" class="full-width"/>';

                    html+='<div class="container mobile-blue-bar">';
                        html+='<div class="row mobile_row">';
                            html+='<a href="#HealthySolutions">HEALTHY SHAKES</a>';
                            html+='<h3 class="mobile-kit">3-WEEK QUICK START KIT</h3>';
                            html+='<h3 class="mobile-kit">$'+stdPrice+'</h3>';
                        html+='</div>';
                    html+='</div>';

                html+='</section>';
                html+='<section class="landing-section section-padding">';
                    html+='<div class="container container-initial">';
                        html+='<h3 class="first-title text-left">Follow Your Own Path with Healthy Shakes </h3>';
                       html+=' <p class="first-paragraph margin-top-50">Healthy Shakes® is a self-guided weight-loss program that offers flexible eating options for dieters who prefer to choose their own ingredients. This plan empowers you achieve the healthier lifestyle you want and navigate a world full of unhealthy choices. We’ve done the research and we know what it takes to lose weight and keep it off. The Healthy Shakes plan gives you all the tools you need to get to work and start building a healthy sustainable lifestyle.</p>';
                        html+='<div class="margin-top-50 text-center">';
                            html+='<button class="btn btn-primary" onclick="location.href=\'ccrz__ProductDetails?SKU='+sku+'\'">Join Today</button>';
                        html+='</div>';
                    html+='</div>';
                    html+='<div id="nonMobilePhoto" class="row text-center">';
                        html+='<img src="'+sitePrefix+planImage2+'" class="full-width"/>';
                    html+='</div>';
                html+='</section>';

                html+='<section class="hero-area green">';
                    html+='<div class="container mobile-padding">';
                        html+='<div class="row">';
                            html+='<div class="col-md-6 col-md-push-6 text-center">';
                                html+='<img src="'+sitePrefix+planImage3+'" class="img-responsive"/>';
                            html+='</div>';
                            html+='<div class="col-md-6 col-md-pull-6 text-picture-padding">';
                                html+='<div>';
                                    html+='<h3 class="section-header-white">Food</h3>';
                                        html+='<p class="section-text-white">The Healthy Shakes eating plan is based on a 3+5+1 structure. That includes a minimum of 3 HMR shakes, 5 servings fruits and vegetables, and 1 healthy meal. You plan, prepare, and cook the healthy meal on your own using HMR guides and strategies. This gives you the flexibility to decide exactly what’s on the menu every day. </p>';
                                html+='</div>';

                            html+='</div>';
                        html+='</div>';
                    html+='</div>';
                html+='</section>';

                html+='<section class="hero-area full-picture-padding">';
                    html+='<div class="container mobile-padding">';
                        html+='<div class="row">';
                            html+='<div class="hidden-sm col-md-7 text-center">';
                                html+='<img src="'+sitePrefix+planImage4+'" class="img-responsive"/>';
                            html+='</div>';
                            html+='<div class="col-sm-12 col-md-5 text-picture-padding">';
                                html+='<div>';
                                    html+='<h3 class="section-header">Guidance</h3>';
                                        html+='<p class="section-text">This is the ideal plan for DIYers who just need the tools to get the job done. Healthy Shakes includes detailed program guides and access to online resources that provide winning strategies for low-calorie meal prep, getting physical activity, and dealing with challenging food environments. </p>';
                                html+='</div>';

                            html+='</div>';
                        html+='</div>';
                    html+='</div>';
                html+='</section>';

                html+='<section class="landing-section clearfix section-padding orange">';

                    html+='<div class="col-xs-12 col-sm-6 col-lg-4 col-lg-offset-2 landing-block-md plans-text-col-white">';
                        html+='<h3 class="first-title">Maintenance</h3>';
                        html+='<p class="first-paragraph">This program is designed with long-term weight maintenance in mind. You can’t diet forever, and we don’t want you to. That’s why we put such a strong focus on discovering the strategies that work best for you. After the weight is gone, we want you to keep it off by using the healthier eating, physical activity, and environmental control skills you picked up along the way. 2 </p>';
                    html+='</div>';
                    html+='<div class="col-xs-12 col-sm-6 landing-block-lg plans-img-col" style="background-image: url(\''+sitePrefix+planImage5+'\');"> </div>';
                html+='</section>';

                html+='<section id="healthyshakesdetails" class="landing-section border">';
                    html+='<div class="container container-initial">';
                        html+='<h3 class="first-title text-left">Healthy Shakes 2-week Quick Start® Kit</h3>';
                        html+='<a class="right-side">with auto delivery</a>';
                        html+='<div class="row" >';

                        html+='</div>';
                        html+='<div class="text-left spacing-top padding-top border">';
                            html+='<ul class="kit-list">';
                                html+='<li >'+numberOfShakesServings+' servings of HMR® low-calorie shakes/pudding mix - available in vanilla and chocolate,(HMR 120) and/or lactose-free (HMR 70 Plus)</li>';
                                html+='<li >'+numberOfSampleEntress+' serving sample HMR Entree</li>';
                                html+='<li >Support Guides, HMR Recipe Book, and weekly tracking charts</li>';
                                html+='<li>HMR App access</li>';
                            html+='</ul>';
                            html+='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">';

                            html+='</div>';
                            html+='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">';
                                html+='<h6 class="kit-price">$'+stdPrice+'</h6>';
                            html+='</div>';
                        html+='</div>';
                        html += '<div class="col-sm-6" border>';
                            html += '<button class="btn btn-primary join-today" onclick="location.href=\'/DefaultStore/health-terms\'">Health and Privacy Acknowledgement</button>';
                        html += '</div>';
                         html+='<div class="col-xs-6 border">';
                            html+='<button class="btn btn-primary join-today"  onclick="location.href=\'ccrz__ProductDetails?SKU='+sku+'\'">Build Your Kit</button>';
                        html+='</div>';
                    html+='</div>';
                html+='</section>';
                html+='<section id="mobile" class="clearfix common-height">';

                    html+='<div class="container mobile-blue-bar">';
                        html+='<div class="row mobile_row">';
                            html+='<div class="col-xs-10">';
                                html+='<a href="#HealthySolutions">'+planName+'</a>';
                                html+='<h3 class="mobile-kit">3-WEEK QUICK START KIT</h3>';
                                html+='<h3 class="mobile-kit">$'+stdPrice+' ($'+pricePerDay+' per day)</h3>';
                            html+='</div>';
                            html+='<div class="col-xs-2">';
                                html+='<i class="fa white-arrow">&#xf178;</i>';
                            html+='</div>';
                        html+='</div>';
                    html+='</div>';

                html+='</section>';
            html+='</main>';
        }
        return html;
    }

}