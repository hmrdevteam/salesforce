@isTest
public class HMRCouponAdminExtensionTest {

    static testMethod void firstTestMethod(){

        Test.startTest();
        HMR_Coupon__c couponObj = new HMR_Coupon__c(Name = 'test coupon', hmr_Type_of_Code__c = 'Email');
        insert couponObj;
        ccrz__E_Coupon__c eCouponObject = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'testcode',Coupon_Order_Type__c = 'A la Carte', ccrz__MaxUse__c
                                            = 12, ccrz__TotalUsed__c = 11, ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today());

        Program__c programObj1 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        insert programObj1;
        Program__c programObj2 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        insert programObj2;
        Program__c programObj3 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 14 );
        insert programObj3;

        Test.setCurrentPageReference(new PageReference('Page.HMRCoupon'));
        System.currentPageReference().getParameters().put('hcId', couponObj.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(couponObj);
        HMRCouponAdminExtension hmrCouponObj  = new HMRCouponAdminExtension(sc);

        hmrCouponObj.master = couponObj;
        hmrCouponObj.ccCoupon = eCouponObject;
        hmrCouponObj.p1 = couponObj.id;
        hmrCouponObj.p2 = couponObj.id;
        hmrCouponObj.hss = couponObj.id;
    //    hmrCouponObj.hcId = couponObj.id;

        hmrCouponObj.save();
        hmrCouponObj.cancel();

        List<Program__c> programList = [select id from Program__c where id = :programObj3.id];
        System.assertEquals(1, programList.size());

        Test.stopTest();
    }

    static testMethod void secondTestMethod(){

       Test.startTest();
        HMR_Coupon__c couponObj = new HMR_Coupon__c(Name = 'test coupon', hmr_Type_of_Code__c = 'Email');
        insert couponObj;
        ccrz__E_Coupon__c eCouponObject = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'testcode',ccrz__MaxUse__c
                                            = 12, ccrz__TotalUsed__c = 11, ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today());

        Program__c programObj1 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        insert programObj1;
        Program__c programObj2 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        insert programObj2;
        Program__c programObj3 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 14 );
        insert programObj3;

        Test.setCurrentPageReference(new PageReference('Page.HMRCoupon'));
        System.currentPageReference().getParameters().put('hcId', couponObj.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(couponObj);
        HMRCouponAdminExtension hmrCouponObj  = new HMRCouponAdminExtension(sc);

        hmrCouponObj.master = couponObj;
        hmrCouponObj.ccCoupon = eCouponObject;
        hmrCouponObj.p1 = couponObj.id;
        hmrCouponObj.p2 = couponObj.id;
        hmrCouponObj.hss = couponObj.id;
    //    hmrCouponObj.hcId = couponObj.id;

        hmrCouponObj.saveAndClose();

        List<Program__c> programList = [select id from Program__c where id = :programObj3.id];
        System.assertEquals(1, programList.size());
        Test.stopTest();
    }

    static testMethod void thirdTestMethod(){

        Test.startTest();
        HMR_Coupon__c couponObj = new HMR_Coupon__c(Name = 'test coupon', hmr_Type_of_Code__c = 'Email');
        insert couponObj;
        ccrz__E_Coupon__c eCouponObject = new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'testcode',ccrz__MaxUse__c
                                            = 12, ccrz__TotalUsed__c = 11, ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today());

        Program__c programObj1 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        insert programObj1;
        Program__c programObj2 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        insert programObj2;
        Program__c programObj3 = new Program__c(Name = 'testprog',hmr_Program_Type__c = 'Healthy Solutions', hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 14 );
        insert programObj3;

        Test.setCurrentPageReference(new PageReference('Page.HMRCoupon'));
        System.currentPageReference().getParameters().put('hcId', couponObj.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(couponObj);
        HMRCouponAdminExtension hmrCouponObj  = new HMRCouponAdminExtension(sc);

        hmrCouponObj.master = couponObj;
        hmrCouponObj.ccCoupon = eCouponObject;
        hmrCouponObj.p1 = couponObj.id;
        hmrCouponObj.p2 = couponObj.id;
        hmrCouponObj.hss = couponObj.id;
    //    hmrCouponObj.hcId = couponObj.id;

        hmrCouponObj.saveAndNext();
        List<Program__c> programList = [select id from Program__c where id = :programObj3.id];
        System.assertEquals(1, programList.size());

        Test.stopTest();

    }


}