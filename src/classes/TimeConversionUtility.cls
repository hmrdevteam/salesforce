/**
* Time Conversion Utility Class
* Contains methods to calculate time conversions and methods to determine if there are any time conflicts regarding class times
* 
*  
* 
* @Date: 2017-6-13
* @Author Ali Pierre (HMR)
* @JIRA:  Issue https://reside.jira.com/browse/HPRP-3832
*/

public class TimeConversionUtility {
	//Map used to convert Standard time hour to corresponding Military hour 
	private static final Map<String, Integer> getMilitaryHour = new Map<String, Integer>{
        '12 AM'=> 0,
        '1 AM'=> 1,
        '2 AM'=> 2,
        '3 AM'=> 3,
        '4 AM'=> 4,
        '5 AM'=> 5,
        '6 AM'=> 6,
        '7 AM'=> 7,
        '8 AM'=> 8,
        '9 AM'=> 9,
        '10 AM'=> 10,
        '11 AM'=> 11,
        '12 PM'=> 12,
        '1 PM'=> 13,
        '2 PM'=> 14,
        '3 PM'=> 15,
        '4 PM'=> 16,
        '5 PM'=> 17,
        '6 PM'=> 18,
        '7 PM'=> 19,
        '8 PM'=> 20,
        '9 PM'=> 21,
        '10 PM'=> 22,
        '11 PM'=> 23
    };
    //Map used to convert Military hour to corresponding Standard time hour 
    private static final Map<Integer, String> getStandardHour = new Map<Integer, String>{
         0=> '12 AM',
         1=> '1 AM',
         2=> '2 AM',
         3=> '3 AM',
         4=> '4 AM',
         5=> '5 AM',
         6=> '6 AM',
         7=> '7 AM',
         8=> '8 AM',
         9=> '9 AM',
         10=> '10 AM',
         11=> '11 AM',
         12=> '12 PM',
         13=> '1 PM',
         14=> '2 PM',
         15=> '3 PM',
         16=> '4 PM',
         17=> '5 PM',
         18=> '6 PM',
         19=> '7 PM',
         20=> '8 PM',
         21=> '9 PM',
         22=> '10 PM',
         23=> '11 PM'
    };
    //Method to return a Set<String> containing times within a 90 range(increments of 15 mins)
    private static Set<String> getTimeSet(String min, String prevHour, String prevHourAmPm, String currentHour, String currentAmPm, String nextHour, String nextHourAmPm, String orgTime){
        Set<String> timeSet;
        //Build set based on the minute value of the class time being checked 
        if(min == '00'){
            timeSet = new Set<String>{prevHour + ':15' + prevHourAmPm, prevHour + ':30' + prevHourAmPm,prevHour + ':45' + prevHourAmPm, orgtime, currentHour + ':15' + currentAmPm, currentHour + ':30' + currentAmPm, currentHour + ':45' + currentAmPm};
        }
        else if(min == '15'){
            timeSet = new Set<String>{prevHour + ':30' + prevHourAmPm, prevHour + ':45' + prevHourAmPm, currentHour + ':00' + currentAmPm, orgtime, currentHour + ':30' + currentAmPm, currentHour + ':45' + currentAmPm, nextHour + ':00' + nextHourAmPm};
        }
        else if(min == '30'){
            timeSet = new Set<String>{prevHour + ':45' + prevHourAmPm, currentHour + ':00' + currentAmPm, currentHour + ':15' + currentAmPm, orgtime, currentHour + ':45' + currentAmPm, nextHour + ':00' + nextHourAmPm, nextHour + ':15' + nextHourAmPm};
        }
        else if(min == '45'){
            timeSet = new Set<String>{currentHour + ':00' + currentAmPm, currentHour + ':15' + currentAmPm, currentHour + ':30' + currentAmPm, orgtime, nextHour + ':00' + nextHourAmPm, nextHour + ':15' + nextHourAmPm, nextHour + ':30' + nextHourAmPm};
        }
         return timeSet;
    }

    //Method used to add/subtract hours to standard format times
    //Returns String
    public static String addHours(String orgTime, Integer timeOffset){
        //declare and initialize regular expression to validate class time being passed in
    	Pattern regexPattern = Pattern.compile('\\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))');
    	Matcher regexMatcher = regexPattern.matcher(orgtime);
        //if orgTime matches regex, proceed with time adjustment
    	if(regexMatcher.matches()){
    		try {
            String hour = orgTime.substringBefore(':');
            String min = orgTime.substringBetween(':', ' ');

            if(orgTime.contains('AM')){
                hour = hour + ' AM';
            }
            else{
                hour = hour + ' PM';
            }

            Integer adjustHour = getMilitaryHour.get(hour);
            //Loop counter
            Integer i = 0;
            
            if(timeOffset < 0){ 
                //If time is negative, set value to postive and subtract hours               
                timeOffset *= -1;
                while (timeOffset > i) { //Currently limited to 10 hours
                    if(adjustHour == 0){
                        //if time is 0(Midnight), reset hour to 23(11 PM)
                        adjustHour = 23;
                    }
                    else{
                        adjustHour -= 1;
                    }
                    i++;
                }
            }
            else if(timeOffset > 0){
                while (timeOffset > i) {//Currently limited to 10 hours
                    if(adjustHour == 23){
                        //if time is 23(11 PM), reset hour to 0(Midnight)
                        adjustHour = 0;
                    }
                    else{
                        adjustHour += 1;
                    }                            
                    i++;
                }
            }
            

            String newTime = getStandardHour.get(adjustHour);

             return newTime.contains('AM') ? newTime.substringBefore(' ').removeStart('0') + ':' + min + ' AM' : newTime.substringBefore(' ').removeStart('0') + ':' + min + ' PM';
	        }
	        Catch (exception e) {
                  //if something went wrong during time adjustment, return orgTime
	              return orgTime;
	        }
    	}
    	else{
            //if something went wrong during time adjustment, return orgTime
    		return orgTime;
    	}    	
    }

    //Method used to determine if there is a time conflict between 2 times/class time
    //Returns Boolean
    public static Boolean isTimeConflict(String updatedTime, String existingTime){
        //declare and initialize regular expression to validate class times being passed in
        Pattern regexPattern = Pattern.compile('\\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))');
        Matcher regexMatcher = regexPattern.matcher(updatedTime);
        Matcher regexMatcher2 = regexPattern.matcher(existingTime);
        //Set to store times within conflict range
        Set<String> timeSet;
        //if updatedTime matches regex, proceed with time adjustment
        if(regexMatcher.matches() && regexMatcher2.matches()){
            try {
            //Extract the hours and minutes digits from updatedTime
            Integer hour = Integer.valueOf(updatedTime.substringBefore(':'));
            String min = updatedTime.substringBetween(':', ' ');

            //Previous hour from updated time
            String prevHour;
            //Next hour from updated time
            String nextHour;
            //Period for previous hour
            String prevHourAmPm;
            //Period for next hour
            String nextHourAmPm;

            //To account for 12 hour standard range, set prevHour, nextHour, prevHourAmPm, nextHourAmPm variables based on the hour
            if(hour == 1){
                prevHour = '12';
                nextHour = '2';
                prevHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM'; 
            }
            else if(hour == 12){
                prevHour = '11';
                nextHour = '1';
                prevHourAmPm = updatedTime.contains('AM') ? ' PM' : ' AM';
                nextHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM';
            }
            else if(hour == 11){
                prevHour = '10';
                nextHour = '12';
                prevHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = updatedTime.contains('AM') ? ' PM' : ' AM';

            }
            else{
                prevHour = String.valueOf(hour - 1);
                nextHour = String.valueOf(hour + 1);
                prevHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = updatedTime.contains('AM') ? ' AM' : ' PM'; 
            }

            //Initialize timeSet using the getTimeSet Method
            timeSet = getTimeSet(min, prevHour, prevHourAmPm, String.valueOf(hour), updatedTime.contains('AM') ? ' AM' : ' PM', nextHour, nextHourAmPm, updatedTime);                  

            System.debug(timeSet);
            
            //Return true if time being compared is within the timeSet, else return false
            return timeSet.contains(existingTime) ? true : false;
            }
            Catch (exception e) {
                //if something went wrong during time comparison, return false
                return false;
            }
        }
        else{
            //if something went wrong during time comparison, return false
            return false;
        }       
    }
    //Method used to determine if there is a time conflict between 2 times/class time
    //Returns Set<String> of times within conflict range
    public static Set<String> getSetOfTimes(String classTime){
        //declare and initialize regular expression to validate class time being passed in
        Pattern regexPattern = Pattern.compile('\\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))');
        Matcher regexMatcher = regexPattern.matcher(classTime);
        //Set to store times within conflict range
        Set<String> timeSet;
        //if classTime matches regex, proceed with time adjustment
        if(regexMatcher.matches()){
            try {
            Integer hour = Integer.valueOf(classTime.substringBefore(':'));
            String min = classTime.substringBetween(':', ' ');

            //Previous hour from updated time
            String prevHour;
            //Next hour from updated time
            String nextHour;
            //Period for previous hour
            String prevHourAmPm;
            //Period for next hour
            String nextHourAmPm;

            //To account for 12 hour standard range, set prevHour, nextHour, prevHourAmPm, nextHourAmPm variables based on the hour
            if(hour == 1){
                prevHour = '12';
                nextHour = '2';
                prevHourAmPm = classTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = classTime.contains('AM') ? ' AM' : ' PM'; 
            }
            else if(hour == 12){
                prevHour = '11';
                nextHour = '1';
                prevHourAmPm = classTime.contains('AM') ? ' PM' : ' AM';
                nextHourAmPm = classTime.contains('AM') ? ' AM' : ' PM';
            }
            else if(hour == 11){
                prevHour = '10';
                nextHour = '12';
                prevHourAmPm = classTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = classTime.contains('AM') ? ' PM' : ' AM';

            }
            else{
                prevHour = String.valueOf(hour - 1);
                nextHour = String.valueOf(hour + 1);
                prevHourAmPm = classTime.contains('AM') ? ' AM' : ' PM';
                nextHourAmPm = classTime.contains('AM') ? ' AM' : ' PM'; 
            }

            //Initialize timeSet using the getTimeSet Method
            timeSet = getTimeSet(min, prevHour, prevHourAmPm, String.valueOf(hour), classTime.contains('AM') ? ' AM' : ' PM', nextHour, nextHourAmPm, classTime);                  

            System.debug(timeSet);
            
            //Return set of times within conflict range
            return timeSet;
            }
            Catch (exception e) {
                //if something went wrong during process, return set containing classTime only
                return timeSet = new Set<String>{classTime};
            }
        }
        else{
            //if something went wrong during process, return set containing classTime only
            return timeSet = new Set<String>{classTime};
        }       
    }
}