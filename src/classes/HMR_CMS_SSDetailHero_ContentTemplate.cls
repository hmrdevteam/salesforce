/**
* Apex Content Template Controller for the Success Stories Detail Hero Section Component
*
* @Date: 07/31/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 07/31/2017
* @JIRA: HPRP-4188
*/
global virtual  class HMR_CMS_SSDetailHero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SSDetailHero_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SSDetailHero_ContentTemplate() {}
	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String SuccessStory {
        get{
            return getPropertyWithDefault('SuccessStory', '');
        }
        set;
    }

    global virtual override String getHTML(){
    	String html = '';
  		String articleName = SuccessStory;
    	
    	if(!String.isBlank(articleName)){
    		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    		HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);
    		if(articleRecord != null){
    			html = '<div class="slim-hero hidden-xs" style="background-image: url(' + articleRecord.HeroImage + ')"></div>' +
    			   	   '<div class="slim-hero visible-xs" style="background-image: url(' + articleRecord.HeroImageMobile + ')"></div>';
    		}
    			   
    	}
    	
    	return html;
    }
}