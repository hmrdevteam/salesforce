global class HMR_CMS_Questions_List_ServiceInterface implements cms.ServiceInterface {
	global HMR_CMS_Questions_List_ServiceInterface() {}
	global HMR_CMS_Questions_List_ServiceInterface(cms.CoreController controller){} 

    /**
     *
     * @param params a map of parameters including at minimum a value for 'action'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {     	
        map<String, Object> returnMe = new map<string, Object>();
        System.debug(params);
        try {
            String action = params.get('action');
            if(action == 'getFeedElements') {
            	returnMe = getFeedElements(params);
            }else{
                //invalid action
                returnMe.put('setfeed','');
                returnMe.put('success',false);
                returnMe.put('message','Invalid Action');
            } 
        } catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('setfeed','');
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
        }
        // No actions matched and no error occurred
        return JSON.serialize(returnMe);
    }

    public Map<String, Object> getFeedElements(Map<String, String> params){
    	map<String, Object> returnMe = new map<string, Object>();
    	try{
    		HMR_CMS_Questions_List_ContentTemplate questionListContentTemplate = new  HMR_CMS_Questions_List_ContentTemplate(integer.valueOf(params.get('pageNumber')), params.get('topicName'));
    		returnMe.put('setfeed', questionListContentTemplate.getHTML());
    		if(!String.valueOf(returnMe.get('setfeed')).contains('Exception in getting Topics for Questions')){
    			returnMe.put('success',true);          
            	returnMe.put('message','success');            
    		}else{
    			String message = String.valueOf(returnMe.get('setfeed'));
    			returnMe.put('setfeed','');
            	returnMe.put('success',false);
            	returnMe.put('message',message);
    		}            
    	} catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('setfeed','');
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
        }
        return returnMe;
    }

	public static Type getType() {
        return HMR_CMS_Questions_List_ServiceInterface.class;
    }
}