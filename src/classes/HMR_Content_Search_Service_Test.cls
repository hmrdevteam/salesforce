/**
* Test Class Coverage of the HMR_Content_Search_Service Class
*
* @Date: 08/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA:
*/
@isTest
private class HMR_Content_Search_Service_Test {
	@testSetup
    private static void setTestData(){
        Recipe__kav leftRightRecipeArticle01 = new Recipe__kav(Title='Test LeftRight Recipe 01'
                                                              ,UrlName='test-leftright-recipe-01'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');

        Blog_Post__kav blogArticle01 = new Blog_Post__kav(Title='Test Blog 01'
                                                         ,UrlName='test-blog-01'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 1'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'LeftRight');

        FAQ__kav faqArticle01 = new FAQ__kav(Title='Test FAQ 01'
                                            ,UrlName='test-faq-01'
                                            ,Language='en_US'
                                            ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                            ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                            ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com');
        Success_Story__kav successStoryArticle01 = new Success_Story__kav(Title='Test Success Story 01'
							                                             ,UrlName='test-success-story-01'
							                                             ,Language='en_US'
							                                             ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
							                                             ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
							                                             ,Publish_to_Success_Story_Page__c = true);

        List<Recipe__kav> recipeArticleList = new List<Recipe__kav>{leftRightRecipeArticle01};
        insert recipeArticleList;

        List<Recipe__kav> recipeArticleToPublishList = [SELECT KnowledgeArticleId FROM Recipe__kav WHERE Id IN: recipeArticleList];
        for(Recipe__kav recipeRecord : recipeArticleToPublishList)
            KbManagement.PublishingService.publishArticle(recipeRecord.KnowledgeArticleId, true);

        List<Blog_Post__kav> blogArticleList = new List<Blog_Post__kav>{blogArticle01};
        insert blogArticleList;

        List<Blog_Post__kav> blogArticleToPublishList = [SELECT KnowledgeArticleId FROM Blog_Post__kav WHERE Id IN: blogArticleList];
        for(Blog_Post__kav blogRecord : blogArticleToPublishList)
            KbManagement.PublishingService.publishArticle(blogRecord.KnowledgeArticleId, true);

        insert faqArticle01;

        List<FAQ__kav> faqArticleToPublishList = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id =: faqArticle01.Id];
        for(FAQ__kav faqRecord : faqArticleToPublishList)
            KbManagement.PublishingService.publishArticle(faqRecord.KnowledgeArticleId, true);

        insert successStoryArticle01;

        List<Success_Story__kav> successStoryArticleToPublishList = [SELECT KnowledgeArticleId FROM Success_Story__kav WHERE Id =: successStoryArticle01.Id];

        for(Success_Story__kav successStoryRecord : successStoryArticleToPublishList)
            KbManagement.PublishingService.publishArticle(successStoryRecord.KnowledgeArticleId, true);

        //Assign Data Categories
        DescribeDataCategoryGroupResult[] dataCategoryGroupList = Schema.describeDataCategoryGroups(new String[] { 'KnowledgeArticleVersion'});

        Recipe__DataCategorySelection recipeDataCategorySelection = new Recipe__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	         ,DataCategoryName = 'Getting_Started'
        																			     	         ,ParentId = leftRightRecipeArticle01.Id);


        Blog_Post__DataCategorySelection blogDataCategorySelection = new Blog_Post__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	   			 ,DataCategoryName = 'Getting_Started'
        																			     	   			 ,ParentId = blogArticle01.Id);

        FAQ__DataCategorySelection faqDataCategorySelection = new FAQ__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	,DataCategoryName = 'Getting_Started'
        																			     	,ParentId = faqArticle01.Id);

        Success_Story__DataCategorySelection successDataCategorySelection = new Success_Story__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     				     		,DataCategoryName = 'Getting_Started'
        																			     					 	    ,ParentId = successStoryArticle01.Id);

        insert recipeDataCategorySelection;
        insert blogDataCategorySelection;
        insert faqDataCategorySelection;
        insert successDataCategorySelection;
    }

    @isTest
    private static void testGetSuccessStoryHMRAuthoredIdSet(){
    	HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

    	Test.startTest();

    	Set<Id> resultSet = searchService.SuccessStoryHMRAuthoredIdSet;

    	Test.stopTest();

    	System.assert(resultSet.size() == 1);
    }

    @isTest
    private static void testGetKnowledgeDataCategoryMap(){
    	HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

    	Test.startTest();

    	Map<String, Set<String>> resultMap = searchService.KnowledgeDataCategoryMap;

    	Test.stopTest();

    	System.assert(resultMap.size() >= 1);
    }

    @isTest
	private static void testGetSearchSuggestions(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();
		string communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;
		ConnectApi.FeedElementPage feedElementPage = new ConnectApi.FeedElementPage();
		List<ConnectApi.FeedElement> feedElementList = new List<ConnectApi.FeedElement>();

		feedElementList.add(new ConnectApi.FeedItem());
		feedElementPage.elements = feedElementList;

		Test.startTest();

		ConnectApi.ChatterFeeds.setTestSearchFeedElements(communityId, 'Recipe*', null, 10, ConnectApi.FeedSortOrder.LastModifiedDateDesc, feedElementPage);
		List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList = searchService.getSearchSuggestions('Recipe', false, 10, '', '');

		Test.stopTest();

		System.assert(resultList.size() == 0);
	}

	@isTest
	private static void testGetQuestionAndAnswersSuggestionsNoResults(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();
		string communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;
		ConnectApi.QuestionAndAnswersSuggestions questionAndAnswerSuggestions = new ConnectApi.QuestionAndAnswersSuggestions();
		List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList;

		Test.startTest();
//QuestionAndAnswers.setTestGetSuggestions(String communityId, String q, String subjectId, Boolean includeArticles, Integer maxResults, ConnectApi.QuestionAndAnswersSuggestions result)
		ConnectApi.QuestionAndAnswers.setTestGetSuggestions(communityId, 'Test', null, false, 10, questionAndAnswerSuggestions);

        try{
        	resultList = searchService.getQuestionAndAnswersSuggestions('Test', false, 10, '', '');
        }
        catch(Exception ex){}

		Test.stopTest();

		System.assert(resultList == null);
	}

	@isTest
	private static void testAppendRecipeCategories(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();
		List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> parameterList = new List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord>();
		KnowledgeArticleVersion articleVersion = [SELECT Title, ArticleType, UrlName FROM KnowledgeArticleVersion WHERE UrlName = 'test-leftright-recipe-01' AND PublishStatus = 'Online' AND Language = 'en_US' LIMIT 1];
		Recipe__kav recipe = [SELECT KnowledgeArticleId, Title, ArticleType, UrlName FROM Recipe__kav WHERE Id =: articleVersion.Id AND PublishStatus = 'Online' AND Language = 'en_US' LIMIT 1];

		parameterList.add(new HMR_Content_Search_Service.QuestionAndAnswerResultRecord(recipe));

		Test.startTest();

		List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList = searchService.appendRecipeCategories(parameterList);

		Test.stopTest();

		System.assert(resultList.size() == 1);
	}

	@isTest
	private static void testGetKnowledgeSuggestionResultsForRecipes(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

		Test.startTest();

		List<Search.SuggestionResult> resultList = searchService.getKnowledgeSuggestionResults('Recipe', 10, 'recipes');

		Test.stopTest();

		System.assert(resultList.size() == 0);
	}

	@isTest
	private static void testGetKnowledgeSuggestionResultsForBlogs(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

		Test.startTest();

		List<Search.SuggestionResult> resultList = searchService.getKnowledgeSuggestionResults('Test', 10, 'Getting_Started');

		Test.stopTest();

		//System.assert(resultList.size() == 0);
	}

	@isTest
	private static void testGetKnowledgeSearchResultsForRecipes(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

		Test.startTest();

		List<Search.SearchResult> resultList = searchService.getKnowledgeSearchResults('Test', 'recipes');

		Test.stopTest();

		System.assert(resultList.size() == 0);
	}

	@isTest
	private static void testGetArticleTypes(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

		Test.startTest();

		Map<String,String> articleTypeMap = searchService.getArticleTypes();

		Test.stopTest();

		System.assert(articleTypeMap.size() > 0);
	}

	@isTest
	private static void testGetDataCategoryGroupStructures(){
		HMR_Content_Search_Service searchService = new HMR_Content_Search_Service();

		Test.startTest();

		List<HMR_DataCategory_Service.DataCategoryHierarchyRecord> dataCategoryList = searchService.getDataCategoryGroupStructures();

		Test.stopTest();

		System.assert(dataCategoryList.size() > 0);
	}
}