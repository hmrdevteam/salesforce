@isTest
/**
* Test class for Trigger on Class Object
* Test Managing Coaching Session records
* @Date: 2016-11-28
* @Author Aslesha(Mindtree)
* @JIRA: 
*/
public with sharing class CoachingSessionRecordsTest {

  static testMethod void TestClassIsActive() {
      Test.startTest();
      //Create test user 
      User sampleUser = new User();
      sampleUser.Username = 'testuser1@gmail.com.hmrtest';
      sampleUser.FirstName='FirstName';
      sampleUser.LastName = 'LastTestName';
      sampleUser.Email = 'testuser1@gmail.com';
      sampleUser.alias = 'testAl';
      sampleUser.TimeZoneSidKey = 'America/New_York';
      sampleUser.LocaleSidKey = 'en_us';
      sampleUser.EmailEncodingKey = 'ISO-8859-1';
      sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
      sampleUser.LanguageLocaleKey = 'en_us';
      sampleUser.IsActive = true;
      sampleUser.hmr_IsCoach__c = true;
      insert sampleUser;

      //Create Test Data for 'Coach__C' object 
      Coach__c coachRecord1 = new Coach__c (
                                        hmr_Active__c=True,
                                        hmr_Coach__c= sampleUser.Id,
                                        hmr_Email__c='sampleemail1@gmail.com',
                                        hmr_Class_Strength__c=20
      );
      insert coachRecord1;    

      //Create Test Data for 'Class' object
      Class__c classRecord1 = New Class__c (
                                      hmr_Active__c=true,
                                      hmr_Class_Name__c='Session on eCommerce',
                                      hmr_Coach__c=coachRecord1.id,
                                      hmr_Start_Time__c = '11:00 AM',
                                      hmr_End_Time__c ='12:00 PM',
                                      hmr_First_Class_Date__c=date.newInstance(2016, 11, 11),                                       
                                      hmr_Active_Date__c=date.newInstance(2016, 10, 11),
                                      hmr_Class_Type__c='P1 PP',
                                      hmr_Coference_Call_Number__c='1234',
                                      hmr_Time_of_Day__c='Noon',
                                      hmr_Participant_Code__c='Placeholder',
                                      hmr_Host_Code__c='Placeholder'
                                      
      );
      insert classRecord1; 

      List <Coaching_Session__c> LstCoachingSession=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord1.Id ];

      //system.assert(LstCoachingSession.size()>0,'Coaching Sessions created');
      Test.stopTest();

  }

  static testMethod void TestClassNotActive() {
      
      Test.startTest();
      
      User sampleUser = new User();
      sampleUser.Username = 'testuser1@gmail.com.hmrtest';
      sampleUser.FirstName='FirstName';
      sampleUser.LastName = 'LastTestName';
      sampleUser.Email = 'testuser1@gmail.com';
      sampleUser.alias = 'testAl';
      sampleUser.TimeZoneSidKey = 'America/New_York';
      sampleUser.LocaleSidKey = 'en_us';
      sampleUser.EmailEncodingKey = 'ISO-8859-1';
      sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
      sampleUser.LanguageLocaleKey = 'en_us';
      sampleUser.IsActive = true;
      sampleUser.hmr_IsCoach__c = true;
      insert sampleUser;

      //Create Test Data for 'Coach__C' object 
      Coach__c coachRecord1 = new Coach__c (
                                        hmr_Active__c=True,
                                        hmr_Coach__c= sampleUser.Id,
                                        hmr_Email__c='sampleemail1@gmail.com',
                                        hmr_Class_Strength__c=20
      );
      insert coachRecord1;    
           
      //Create Test Data for 'Class' object with Active date today & first date in future 
      Class__c classRecord2 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '8:00 AM',
                                            hmr_End_Time__c ='9:00 AM',
                                            hmr_First_Class_Date__c=Date.today()+2,                                         
                                            hmr_Active_Date__c=Date.today(),
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Morning',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder'
                                          
                                            
      );
      
     
      insert classRecord2;       
      
      classRecord2.hmr_Active__c=false;
      update classRecord2;
      
      
     
      List <Coaching_Session__c> LstCoachingSession=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord2.Id ];
      system.assert(LstCoachingSession.size()==0,'Coaching Sessions not created');
      
      classRecord2.hmr_Active__c=true;
      update classRecord2;
      
        
      //Create test data for class with Active date & first class date in past 
      Class__c classRecord3 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session2 ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '1:00 PM',
                                            hmr_End_Time__c ='2:00 PM',
                                            hmr_First_Class_Date__c=Date.today()-150,                                         
                                            hmr_Active_Date__c=Date.today()-150,
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Noon',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder'
                                            
                                            
      );
      insert classRecord3; 
      //Add past coaching session record to cover past records condition 
              
      classRecord3.hmr_Active__c=false;
      update classRecord3;
     
      List <Coaching_Session__c> LstCoachingSession1=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord3.Id ];
      system.assert(LstCoachingSession1.size()==0,'Coaching Sessions not created');
      Test.stopTest();
      
  }
    
      static testMethod void TestClassUpdateTime() {
      
      Test.startTest();
      
      User sampleUser = new User();
      sampleUser.Username = 'testuser1@gmail.com.hmrtest';
      sampleUser.FirstName='FirstName';
      sampleUser.LastName = 'LastTestName';
      sampleUser.Email = 'testuser1@gmail.com';
      sampleUser.alias = 'testAl';
      sampleUser.TimeZoneSidKey = 'America/New_York';
      sampleUser.LocaleSidKey = 'en_us';
      sampleUser.EmailEncodingKey = 'ISO-8859-1';
      sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
      sampleUser.LanguageLocaleKey = 'en_us';
      sampleUser.IsActive = true;
      sampleUser.hmr_IsCoach__c = true;
      insert sampleUser;

      //Create Test Data for 'Coach__C' object 
      Coach__c coachRecord1 = new Coach__c (
                                        hmr_Active__c=True,
                                        hmr_Coach__c= sampleUser.Id,
                                        hmr_Email__c='sampleemail1@gmail.com',
                                        hmr_Class_Strength__c=20
      );
      insert coachRecord1;    
           
      //Create Test Data for 'Class' object with Active date today & first date in future 
      Class__c classRecord2 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '8:00 AM',
                                            hmr_End_Time__c ='9:00 AM',
                                            hmr_First_Class_Date__c=Date.today()+2,                                         
                                            hmr_Active_Date__c=Date.today(),
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Morning',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder'
                                          
                                            
      );
      
     
      insert classRecord2;      
      
          
      classRecord2.hmr_Start_Time__c = '9:00 AM';
      classRecord2.hmr_End_Time__c ='10:00 AM';
      update classRecord2;
     	
      classRecord2.hmr_Active__c = false;
      update classRecord2;
      classRecord2.hmr_One_Time_Class__c = true;
      update classRecord2;
      
        
      //Create test data for class with Active date & first class date in past 
      Class__c classRecord3 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session2 ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '1:00 PM',
                                            hmr_End_Time__c ='2:00 PM',
                                            hmr_First_Class_Date__c=Date.today()-150,                                         
                                            hmr_Active_Date__c=Date.today()-150,
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Noon',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder'
                                            
                                            
      );
      insert classRecord3; 
      //Add past coaching session record to cover past records condition 
              
      classRecord3.hmr_Active__c=false;
      update classRecord3;
     
      List <Coaching_Session__c> LstCoachingSession1=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord3.Id ];
      system.assert(LstCoachingSession1.size()==0,'Coaching Sessions not created');
      Test.stopTest();
      
  }

  static testMethod void TestClassOneTime() {
      
      Test.startTest();
      
      User sampleUser = new User();
      sampleUser.Username = 'testuser1@gmail.com.hmrtest';
      sampleUser.FirstName='FirstName';
      sampleUser.LastName = 'LastTestName';
      sampleUser.Email = 'testuser1@gmail.com';
      sampleUser.alias = 'testAl';
      sampleUser.TimeZoneSidKey = 'America/New_York';
      sampleUser.LocaleSidKey = 'en_us';
      sampleUser.EmailEncodingKey = 'ISO-8859-1';
      sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
      sampleUser.LanguageLocaleKey = 'en_us';
      sampleUser.IsActive = true;
      sampleUser.hmr_IsCoach__c = true;
      insert sampleUser;

      //Create Test Data for 'Coach__C' object 
      Coach__c coachRecord1 = new Coach__c (
                                        hmr_Active__c=True,
                                        hmr_Coach__c= sampleUser.Id,
                                        hmr_Email__c='sampleemail1@gmail.com',
                                        hmr_Class_Strength__c=20
      );
      insert coachRecord1;    
           
      //Create Test Data for 'Class' object with Active date today & first date in future 
      Class__c classRecord2 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '8:00 AM',
                                            hmr_End_Time__c ='9:00 AM',
                                            hmr_First_Class_Date__c=Date.today()+2,                                         
                                            hmr_Active_Date__c=Date.today(),
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Morning',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder',
                                            hmr_One_Time_Class__c = false
                                          
                                            
      );
      
     
      insert classRecord2;
      
      classRecord2.hmr_Start_Time__c = '9:00 AM';
      classRecord2.hmr_End_Time__c ='10:00 AM';
      update classRecord2;
      
      classRecord2.hmr_Active__c =false;
      update classRecord2;
     
      
        
       
      Class__c classRecord3 = New Class__c (
                                            hmr_Active__c=true,
                                            hmr_Class_Name__c='Aslesha session2 ',
                                            hmr_Coach__c=coachRecord1.id,
                                            hmr_Start_Time__c = '1:00 PM',
                                            hmr_End_Time__c ='2:00 PM',
                                            hmr_First_Class_Date__c=Date.today()+3,                                         
                                            hmr_Active_Date__c=Date.today()+3,
                                            hmr_Class_Type__c='P1 PP',
                                            hmr_Coference_Call_Number__c='1234',
                                            hmr_Time_of_Day__c='Noon',
                                            hmr_Participant_Code__c='Placeholder',
                                            hmr_Host_Code__c='Placeholder',
          									hmr_One_Time_Class__c = true
                                            
                                            
      );
      insert classRecord3; 
              
      
      classRecord3.hmr_One_Time_Class__c = false;
      update classRecord3;
     
      List <Coaching_Session__c> LstCoachingSession1=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord3.Id ];
      system.assert(LstCoachingSession1.size()==0,'Coaching Sessions not created');
      Test.stopTest();
      
  }

}