/**
* Apex Content Template for the Article Rating Functionality
*
* @Date: 2017-07-24
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-24
* @JIRA: HPRP-4090
*/
global virtual class HMR_CMS_RateArticle_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_RateArticle_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_RateArticle_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML(){
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        string articleName = ApexPages.currentPage().getParameters().get('name');

        //Try to get from url if empty
        if(String.isBlank(articleName)){
            List<String> urlPartList = ApexPages.currentPage().getURL().split('/');
            if(urlPartList.size() > 0)
                articleName = urlPartList[urlPartList.size() - 1].split('\\?')[0];
        }

        articleName = String.escapeSingleQuotes(articleName);

        Vote voteRecord = knowledgeService.getVote(articleName);

    	string html = '<div class="container">' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<span class="article-like" style="' + (String.isBlank(voteRecord.Id) ? '' : 'display:none') + '">' +
			                                 '<img id="likeArticleAction" src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartIcon.svg" class="article-like-icon" onclick="knowledgeVote.doInit();" /><h6 class="article-like-text">Like</h6>' +
			                        '</span>' +
                                    '<span class="article-liked" style="' + (String.isBlank(voteRecord.Id) ? 'display:none' : '') + '">' +
                                             '<img id="likedArticleAction" src="https://www.myhmrprogram.com/ContentMedia/Resources/BlueFillHeart.svg" class="article-liked-icon" /><h6 class="article-liked-text">Like</h6>' +
                                    '</span>' +
			                    '</div>' +
			                '</div>' +
			           '</div>';

    	return html;
    }
}