/**
* Schedular class to run CoachingSessionsAdditionalMonthBatchJob 
* this will be schedule to run monthly via Schedule Apex 
* 
* @Date: 2016-11-24
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-628
*/

global class CoachingSessionsBacthJobSchedular implements Schedulable {
	global void execute(SchedulableContext sc) {
		try {
			CoachingSessionsAdditionalMonthBatchJob b = new CoachingSessionsAdditionalMonthBatchJob();
			database.executebatch(b);
		}
		catch(Exception ex) {
			System.debug('The CoachingSessionsBacthJobSchedular failed ' + ex.getMessage());
		}		
	}
}