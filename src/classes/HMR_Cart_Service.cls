/**
* Apex Service Class for the Cart
*
* @Date: 2017-10-03
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-10-05
* @JIRA: HPRP-4340
*/
public with sharing class HMR_Cart_Service {
	private boolean isGuest = false;
	private boolean repriceCart = true;
	private string userId;
	public Map<String, Object> ccrzAPISizing = new Map<String, Object>{ccrz.ccApiCart.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL}
																	   ,ccrz.ccApiProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL}};

	public HMR_Cart_Service(string userId, boolean isGuest){
		this(userId, isGuest, true);
	}

	public HMR_Cart_Service(string userId, boolean isGuest, boolean repriceCart){
		ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
    	contextCCRZ.portalUserId = userId;
		ccrz.cc_CallContext.initRemoteContext(contextCCRZ);

		this.isGuest = isGuest;
		this.repriceCart = repriceCart;
		this.userId = userId;
	}

	/**
	* Fetches cart data from cloudcraze using the encrypted cart id.
	* @param encryptedCartId the encrypted id of the cart to return the data for
	* @return the cart record containing cart items, address, products and product specs
	*/
	public CartRecord getCartByEncryptedId(string encryptedCartId){
		Map<String, Object> inputDataMap = new Map<String, Object>{
			ccrz.ccApi.API_Version => 6
		   ,ccrz.ccApiCart.ACTIVECART => true
		   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
		   ,ccrz.ccApi.SIZING => this.ccrzAPISizing
		};

		return getCart(inputDataMap);
	}

	/**
	* Fetches cart data from cloudcraze using the user id.
	* @param userId the user id to retrieve the active cart data for
	* @return the cart record containing cart items, address, products and product specs
	*/
	public CartRecord getCartByUserId(string userId){
		Map<String, Object> inputDataMap = new Map<String, Object>{
		    ccrz.ccApi.API_Version => 6
		   ,ccrz.ccApiCart.ACTIVECART => true
		   ,ccrz.ccApiCart.BYOWNER => userId
		   ,ccrz.ccApi.SIZING => this.ccrzAPISizing
		};

		return getCart(inputDataMap);
	}

	/**
	* Fetches cart data from cloudcraze
	* @param inputDataMap the cloud craze formatted request parameters
	* @return the cart record containing cart items, address, products and product specs
	*/
	private CartRecord getCart(Map<String, Object> inputDataMap){
		CartRecord cartRecord;

		try{
			Map<String, Object> outputDataMap = ccrz.ccApiCart.fetch(inputDataMap);
			if(outputDataMap.get(ccrz.ccApiCart.CART_OBJLIST) != null){
				List<Map<String, Object>> outputCartList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiCart.CART_OBJLIST);
				List<Map<String, Object>> outputAddressList;
				List<Map<String, Object>> outputProductList;
				List<Object> outputSpecList;

				if(outputDataMap.get(ccrz.ccApiAddress.ADDRESSLIST) != null)
					outputAddressList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiAddress.ADDRESSLIST);

				if(outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST) != null)
					outputProductList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST);

				if(outputDataMap.get('specList') != null)
					outputSpecList = (List<Object>)outputDataMap.get('specList');

				cartRecord = new CartRecord(outputCartList[0], outputSpecList, outputAddressList, outputProductList);
			}
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return cartRecord;
	}

	/**
	* Creates a new/empty cart
	* @param inputDataMap the cloud craze formatted request parameters
	* @return the encrypted cart id for the new cart record
	*/
	public string createEmptyCart(){
		Savepoint sp = Database.setSavepoint();
		boolean success = false;
		String encryptedCartId;

		try{
			Map<String, Object> createResults = ccrz.ccApiCart.addTo(new Map<String, Object>{
				ccrz.ccApi.API_Version => 6
			});

			encryptedCartId = (String)createResults.get(ccrz.ccApiCart.CART_ENCID);
			success = !String.isBlank(encryptedCartId);
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		if(!success)
			Database.rollback(sp);

		if(isGuest)
			addCartCookie(encryptedCartId);

		return encryptedCartId;
	}

	/**
	* Adds items/products to an existing cart
	* @param encryptedCartId the encrypted cart id
	* @param sku the related sku for the item being added to the cart
	* @param quantity the quantity of items being added
	* @return boolean indicating success
	*/
	public boolean addItemToExistingCart(string encryptedCartId, string sku, decimal quantity, decimal price){
		Savepoint sp = Database.setSavepoint();
		boolean success = false;
		List<ccrz.ccApiCart.LineData> cartLineItemList = new List<ccrz.ccApiCart.LineData>();
		ccrz.ccApiCart.LineData cartLineItem = new ccrz.ccApiCart.LineData();

		cartLineItem.sku = sku;
		cartLineItem.quantity = quantity;

		cartLineItemList.add(cartLineItem);


		try{
			Map<String, Object> addResultMap = ccrz.ccApiCart.addTo(new Map<String, Object>{
				ccrz.ccApi.API_VERSION => 6
			   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
			   ,ccrz.ccAPICart.ISSKIPPRICING => (price == 0)
			   ,ccrz.ccApiCart.LINE_DATA => cartLineItemList
			});

			success = (Boolean)addResultMap.get(ccrz.ccApi.SUCCESS);
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return success;
	}

	/**
	* Adds a dynamic kit with related items to a cart
	* @param encryptedCartId the encrypted cart id
	* @param dynamicKitSku the related sku for the dynamic kit being added to the cart
	* @param jsonItemList a json string of items added to the kit in the format: ‘[{“sku”:“value”,“quantity”:“value”}, {“sku”:“value”,“quantity”:“value”}]’
	* @return boolean indicating success
	*/
	public Map<String, Object> addDynamicKitToExistingCart(string encryptedCartId, string dynamicKitSku, string jsonItemList){
		Savepoint sp = Database.setSavepoint();
		Map<String, Object> returnMap = new Map<String, Object>{
			'success' => true,
			'kitPrice' => 0.00};
		boolean success = true;
		Decimal kitPrice = 0.00;
		ccrz__E_Product__c dynamicKitRecord = [SELECT Name FROM ccrz__E_Product__c WHERE ccrz__SKU__c =: dynamicKitSku LIMIT 1];
		List<ccrz.ccApiCart.LineData> minorItemList = new List<ccrz.ccApiCart.LineData>();
		List<Object> itemList = (List<Object>)System.JSON.deserializeUntyped(jsonItemList);
		//Create the parent kit item
		ccrz.ccApiCart.LineData kitLineItem = new ccrz.ccApiCart.LineData();
		kitLineItem.quantity = 1;
		kitLineItem.sku = dynamicKitSku;
		kitLineItem.label = dynamicKitRecord.Name;

		for(Object itemRecord : itemList){
			Map<String, Object> itemRecordMap = (Map<String, Object>)itemRecord;
			ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();

        	lineItem.quantity = Integer.valueOf(itemRecordMap.get('quantity'));
        	lineItem.sku = (String)itemRecordMap.get('sku');
        	lineItem.parentSku = dynamicKitSku;

        	minorItemList.add(lineItem);
		}

		//Add the minor items to the parent
        kitLineItem.minorItems = minorItemList;

        try{
        	Map<String, Object> inputDataMap = new Map<String, Object>{
        		ccrz.ccApiCart.CART_ENCID => encryptedCartId
        	   ,ccrz.ccApi.API_VERSION => 6
        	   ,ccrz.ccApiCart.LINE_DATA => new List<ccrz.ccApiCart.LineData>{kitLineItem}
        	};

        	Map<String, Object> addResultsMap = ccrz.ccApiCart.addTo(inputDataMap);

        	success = (Boolean)addResultsMap.get(ccrz.ccApi.SUCCESS);

        	if(success){
        		try{
        			AggregateResult sumOfKitItems = [SELECT SUM(ccrz__ItemTotal__c) kitItems FROM ccrz__E_CartItem__c WHERE ccrz__Cart__r.ccrz__EncryptedId__c = :encryptedCartId
        								AND ccrz__ParentCartItem__r.ccrz__ItemLabel__c = :dynamicKitRecord.Name];

        			kitPrice = (Decimal)sumOfKitItems.get('kitItems') != null ? (Decimal)sumOfKitItems.get('kitItems'): 0.00;
        		}
        		catch(Exception ex){
        			kitPrice = 0.00;
        		}
        	}

        	returnMap.put('success', success);
        	returnMap.put('kitPrice', kitPrice);
        }
        catch(Exception ex){
        	System.Debug(ex);
        }

        if(!success)
        	Database.rollback(sp);

        return returnMap;
	}

	/**
	* Adds a standard kit to a cart
	* @param encryptedCartId the encrypted cart id
	* @param kitSku the related sku for the kit being added to the cart
	* @return boolean indicating success
	*/
	public boolean addKitToExistingCart(string encryptedCartId, string kitSku){
		Savepoint sp = Database.setSavepoint();
		boolean success = true;
		List<ccrz.ccApiCart.LineData> minorItemList = new List<ccrz.ccApiCart.LineData>();
		ccrz__E_Product__c kitRecord = [SELECT Name FROM ccrz__E_Product__c WHERE ccrz__SKU__c =: kitSku LIMIT 1];

		//Create the parent kit item
		ccrz.ccApiCart.LineData kitLineItem = new ccrz.ccApiCart.LineData();
		kitLineItem.quantity = 1;
		kitLineItem.sku = kitSku;
		kitLineItem.label = kitRecord.Name;

		//Kit Items
		for(ccrz__E_CompositeProduct__c kitItem : [SELECT  ccrz__Component__c
														  ,ccrz__Component__r.ccrz__SKU__c
                                                          ,ccrz__Quantity__c
                                                     FROM ccrz__E_CompositeProduct__c
                                                     WHERE ccrz__Composite__c =: kitRecord.Id])
        {
        	ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();

        	lineItem.quantity = kitItem.ccrz__Quantity__c;
        	lineItem.sku = kitItem.ccrz__Component__r.ccrz__SKU__c;
        	lineItem.parentSku = kitSku;

        	minorItemList.add(lineItem);
        }

        //Add the minor items to the parent
        kitLineItem.minorItems = minorItemList;

        try{
        	Map<String, Object> inputDataMap = new Map<String, Object>{
        		ccrz.ccApiCart.CART_ENCID => encryptedCartId
        	   ,ccrz.ccApi.API_VERSION => 6
        	   ,ccrz.ccApiCart.LINE_DATA => new List<ccrz.ccApiCart.LineData>{kitLineItem}
        	};

        	Map<String, Object> addResultsMap = ccrz.ccApiCart.addTo(inputDataMap);

        	success = (Boolean)addResultsMap.get(ccrz.ccApi.SUCCESS);
        }
        catch(Exception ex){
        	System.Debug(ex);
        }

        if(!success)
        	Database.rollback(sp);

        return success;
	}

	/**
	* Adds/applies a coupon code to a cart
	* @param encryptedCartId the encrypted cart id
	* @param couponCode the related coupon code being applied
	* @return boolean indicating success
	*/
	public boolean addCouponCode(string encryptedCartId, string couponCode){
		boolean success = false;
		HMR_Coupon_Service couponService = new HMR_Coupon_Service();

		try{
			//Price the cart
			//Map<String, Object> priceCartResultMap = priceCart(encryptedCartId);

			//Can get medium cart
			this.ccrzApiSizing = new Map<String, Object>{ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
	            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M,
	            ccrz.ccAPI.SZ_REL => new List<String>{'E_CartItems__r'},
	            ccrz.ccAPI.SZ_ASSC => false
	        }};

			CartRecord cartRecord = getCartByEncryptedId(encryptedCartId);

			//Get respective cc coupon
			ccrz__E_Coupon__c ccCouponRecord;
			if(cartRecord != null)
				ccCouponRecord = couponService.getCouponFromHMRCouponCode(this.userId, cartRecord, couponCode);

			//Apply coupon to the cart
			Map<String, Object> couponResultsMap = ccrz.ccApiCart.addTo(new Map<String, Object>{
				ccrz.ccApi.API_VERSION => 6
			   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
			   ,ccrz.ccApiCart.COUPON_CODE => ccCouponRecord != null ? ccCouponRecord.ccrz__CouponCode__c : couponCode
			});

			//Price the cart
			priceCart(encryptedCartId);

			success = (Boolean)couponResultsMap.get(ccrz.ccApi.SUCCESS);
		}
		catch(Exception ex){
			success = false;
		}

		return success;
	}

	public boolean applyManualAdjustment(string encryptedCartId, decimal adjustmentAmount){
		boolean success = false;
		try{
			ccrz__E_Cart__c cartRecord = [SELECT ccrz__AdjustmentAmount__c
									  	  FROM ccrz__E_Cart__c
									  	  WHERE  ccrz__EncryptedId__c=: encryptedCartId ];

        	cartRecord.ccrz__AdjustmentAmount__c = adjustmentAmount * -1;

        	update cartRecord;

        	success = true;
    	}
    	catch(Exception ex){
    		success = false;
    	}

        return success;
	}

	/**
	* Removes the active coupon code from a cart
	* @param encryptedCartId the encrypted cart id
	* @return boolean indicating success
	*/
	public boolean removeCouponCode(string encryptedCartId){
		return removeCouponCode(encryptedCartId, true);
	}
	public boolean removeCouponCode(string encryptedCartId, boolean repriceCart){
		boolean success = false;

		//Remove coupon
		Map<String, Object> couponResultsMap = ccrz.ccApiCart.removeFrom(new Map<String, Object>{
			ccrz.ccApi.API_VERSION => 6
		   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
		   ,ccrz.ccApiCart.COUPON_CODE => 'true'  //recommended to send just true, code does not matter
		});

		//Price the cart
		if(repriceCart)
			priceCart(encryptedCartId);

		success = (Boolean)couponResultsMap.get(ccrz.ccApi.SUCCESS);

		return success;
	}

	/**
	* Checks for automatic coupons to apply to the cart (i.e. Program/Account Membership Discounts and Kit Orders) or whether existing
	*  coupon is no longer qualified and should be removed
	* @param encryptedCartId the encrypted cart id
	* @return Cart Record with updated coupon data from result of the check
	*/
	public CartRecord checkForAutomaticCoupon(string encryptedCartId){
		this.ccrzApiSizing = new Map<String, Object>{ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M,
            ccrz.ccAPI.SZ_REL => new List<String>{'E_CartItems__r'},
            ccrz.ccAPI.SZ_ASSC => false
        }};

		CartRecord cartRecord = this.getCartByEncryptedId(encryptedCartId);

		return this.checkForAutomaticCoupon(cartRecord);
	}

	/**
	* Checks for automatic coupons to apply to the cart (i.e. Program/Account Membership Discounts and Kit Orders) or whether existing
	*  coupon is no longer qualified and should be removed
	* @param encryptedCartId the encrypted cart id
	* @return Cart Record with updated coupon data from result of the check
	*/
	public CartRecord checkForAutomaticCoupon(CartRecord cartRecord){
		if(cartRecord != null){
			HMR_Coupon_Service couponService = new HMR_Coupon_Service();
			HMR_Coupon_Service.AutomaticCouponRecord automaticCouponResult = couponService.getAutomaticCoupon(this.userId, cartRecord);

			if(automaticCouponResult != null){
				//Remove existing coupon if we need to
	  			if(automaticCouponResult.removeCoupon){
	  				this.removeCouponCode(cartRecord.encryptedId, false);
	  			}

	  		   	//Apply new coupon
	  		   	if(automaticCouponResult.couponRecord != null &&
	  		   	  (cartRecord.couponRecord == null || automaticCouponResult.couponRecord.ccrz__CouponCode__c != cartRecord.couponRecord.couponCode)){
	  		   	   this.addCouponCode(cartRecord.encryptedId, automaticCouponResult.couponRecord.ccrz__CouponCode__c);
	  		   	}

	  		   	//Re-retrieve the cart for the auto-coupon updates - too process/SOQL intensive, recalled from js
				//cartRecord = this.getCartByEncryptedId(cartRecord.encryptedId);
			}
		}

		return cartRecord;
	}

	/**
	* Checks for expired coupon applied to cart to properly remove it (doesn't happen with repricing)
	* @param encryptedCartId the encrypted cart id
	* @return inidication of whether coupon was removed
	*/
	public boolean checkForExpiredCoupon(string encryptedCartId){
		boolean wasCouponRemoved = false;
		List<ccrz__E_CartCoupon__c> cartCouponList = [SELECT ccrz__Coupon__r.ccrz__EndDate__c
													  FROM ccrz__E_CartCoupon__c
													  WHERE ccrz__Cart__r.ccrz__EncryptedId__c =: encryptedCartId];
		if(cartCouponList.size() > 0){ //We only allow one coupon
			ccrz__E_CartCoupon__c couponRecord = cartCouponList[0];
			if(couponRecord.ccrz__Coupon__r.ccrz__EndDate__c != null && couponRecord.ccrz__Coupon__r.ccrz__EndDate__c < Date.today())
				wasCouponRemoved = this.removeCouponCode(encryptedCartId);
		}

		return wasCouponRemoved;
	}

	/**
	* Retreives the available shipping options for the address
	* @param encryptedCartId the encrypted cart id
	* @param jsonShippingAddress the related address json data (sfid, countryISOcode, postalCode and stateISOCode)
	* @return list of shipping options available
	*/
	public List<ShippingOption> getShippingOptions(string encryptedCartId, string jsonShippingAddress){
		List<ShippingOption> shippingOptionList = new List<ShippingOption>();
		List<ccrz__E_Cart__c> cartRecordList = [SELECT ccrz__ShipAmount__c, ccrz__ShipMethod__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encryptedCartId];

		if(cartRecordList.size() > 0){
			Map<String, Object> shippingAddressRecordMap = (Map<String, Object>)System.JSON.deserializeUntyped(jsonShippingAddress);

			Map<String, Object> inputDataMap = new Map<String, Object>{
					ccrz.ccApi.API_VERSION => 6
	        	   ,ccrz.ccApiCart.CART_ID => cartRecordList[0].Id
	        	   ,ccrz.ccApiCart.SHIPPING_ADDR => shippingAddressRecordMap
	        };

	        try{
				Map<String, Object> getShippingOptionsResultsMap = ccrz.ccApiCart.getShippingOptions(inputDataMap);
				List<Object> shippingOptionMapList = (List<Object>)getShippingOptionsResultsMap.get(ccrz.ccApiCart.SHIPPING_OPTIONS);

				for(Object shippingOptionMap : shippingOptionMapList)
					shippingOptionList.add(new ShippingOption((Map<String, Object>)shippingOptionMap));

				//Default Shipping Method
				if(shippingOptionList.size() > 0){
					if(String.isBlank(cartRecordList[0].ccrz__ShipMethod__c)){
						this.updateShippingMethod(encryptedCartId, shippingOptionList[0].serviceName, shippingOptionList[0].price);
						shippingOptionList[0].isSelected = true;
					}
					else{
						//Verify current shipping method still valid
						String currentShippingMethod = cartRecordList[0].ccrz__ShipMethod__c;
						Decimal currentShippingAmount = cartRecordList[0].ccrz__ShipAmount__c;
						Boolean isValid = false;
						for(ShippingOption shippingOptionRecord : shippingOptionList){
							if(shippingOptionRecord.serviceName == currentShippingMethod && shippingOptionRecord.price == currentShippingAmount){
								isValid = true;
								shippingOptionRecord.isSelected = true;
								break;
							}
						}

						if(!isValid){
							this.updateShippingMethod(encryptedCartId, shippingOptionList[0].serviceName, shippingOptionList[0].price);
							shippingOptionList[0].isSelected = true;
						}
					}
				}
			}
			catch(Exception ex){
				System.Debug(ex);
			}
		}

		return shippingOptionList;
	}

	/**
	* Removes an item/product from the cart
	* @param encryptedCartId the encrypted cart id
	* @param sfId the related salesforce record id for the cart item being removed
	* @return boolean indicating success
	*/
	public boolean removeItemFromCart(string encryptedCartId, string sfId){
		boolean success = false;
		Savepoint sp = Database.setSavepoint();
		ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();

		lineItem.sfId = sfId;

        try{
        	Map<String, Object> inputDataMap = new Map<String, Object>{
				ccrz.ccApi.API_VERSION => 6
        	   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
        	   ,ccrz.ccApiCart.LINE_DATA => new List<ccrz.ccApiCart.LineData>{lineItem}
        	};

        	Map<String, Object> removeResultsMap = ccrz.ccApiCart.removeFrom(inputDataMap);

        	success = (Boolean)removeResultsMap.get(ccrz.ccApi.SUCCESS);

        	if(success)
        		checkForAutomaticCoupon(encryptedCartId);
    	}
    	catch(Exception ex){
    		System.Debug(ex);
    	}

    	if(!success)
        	Database.rollback(sp);

		return success;
	}

	/**
	* Updates a cart with new/updated cart item quantities
	* @param encryptedCartId the encrypted cart id
	* @param jsonItemList a json string of updated cart items in the format: ‘[{“sfId”:“value”,“quantity”:“value”}, {“sfId”:“value”,“quantity”:“value”}]’
	* @return boolean indicating success
	*/
	public boolean updateCart(string encryptedCartId, string jsonItemList){
		boolean success = false;
		Savepoint sp = Database.setSavepoint();
		Map<String, Map<String, Object>> cartItemList = new Map<String, Map<String, Object>>();
		List<Object> itemList = (List<Object>)System.JSON.deserializeUntyped(jsonItemList);
		System.debug('itemList');
		System.debug(itemList);
		for(Object itemRecord : itemList){
			Map<String, Object> itemRecordMap = (Map<String, Object>)itemRecord;
			for(String itemRecordMapKey: itemRecordMap.keySet()) {
				System.debug('itemRecordMapKey ' + itemRecordMapKey);
				System.debug('itemRecordMap ' + String.valueOf(itemRecordMap.get(itemRecordMapKey)));
			}
			cartItemList.put(
				(String)itemRecordMap.get('sfId'),
				new Map<String, Object>{
					ccrz.ccApiCart.CIQTY => Integer.valueOf(itemRecordMap.get('quantity')),
					ccrz.ccApiCart.CICMTS => ''
				}
			);
		}

		Map<String, Object> inputDataMap = new Map<String, Object>{
		    ccrz.ccApi.API_Version => 6
		   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
           ,ccrz.ccAPICart.CILIST => cartItemList
		};

		try{
			Map<String, Object> priceResultsMap = ccrz.ccApiCart.price(inputDataMap);
			success = (Boolean)priceResultsMap.get(ccrz.ccApi.SUCCESS);

			if(success)
				checkForAutomaticCoupon(encryptedCartId);
		}
		catch(Exception ex){
			System.Debug('Something went wrong in HMR_Cart_Service.updateCart ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString());
		}

		if(!success)
			Database.rollback(sp);

		return success;
	}

	/**
	* Sets the coaching selection on the cart
	* @param encryptedCartId the encrypted cart id
	* @param coachingSelection a boolean indicating whether coaching was selected
	* @return boolean indicating success
	*/
    public boolean setCoachingSelection(string encryptedCartId, boolean coachingSelection){
    	boolean result = false;

    	if(!String.isBlank(encryptedCartId)){
    		ccrz__E_Cart__c cartToUpdate = [SELECT ccrz__EncryptedId__c
    											  ,Coaching_Selection__c
                                            FROM ccrz__E_Cart__c
                                            WHERE ccrz__EncryptedId__c =: encryptedCartId];
            if(cartToUpdate != null){
            	cartToUpdate.Coaching_Selection__c = coachingSelection;
            	update cartToUpdate;
            	result = true;
            }
    	}

    	return result;
    }

    /**
	* Places an order transferring the cart to an official order record
	* @param encryptedCartId the encrypted cart id
	* @param jsonPaymentData the json payment data
	* @return string of the encrypted order id (or empty string indicating failure)
	*/
	public string placeOrder(string encryptedCartId, string jsonPaymentData){
		boolean success = false;
		Savepoint sp = Database.setSavepoint();
		string orderId = '';
		string encryptedOrderId = '';

		try{
			if(priceCart(encryptedCartId)){
				ccrz__E_Cart__c cartRecord = [SELECT ccrz__TotalAmount__c, ccrz__ValidationStatus__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encryptedCartId];
				cartRecord.ccrz__ValidationStatus__c = 'CartAuthUserValidated';
				update cartRecord;

				ccrz.cc_hk_Payment hkPayment = new ccrz.cc_hk_Payment();
				Map<String, Object> paymentResultMap = new Map<String, Object>();
				paymentResultMap.put('transactionData', jsonPaymentData);
				paymentResultMap.put('cart', cartRecord);
				Map<String, Object> processPaymentResultMap = hkPayment.processPayment(paymentResultMap);
				Object transactionPaymentData = processPaymentResultMap.get(ccrz.cc_hk_Payment.PARAM_TRANSACTION_PROCESSED_DATA);

				Map<String, Object> inputDataMap = new Map<String, Object>{
				    	 ccrz.ccApi.API_VERSION => 6
				        ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
				        ,ccrz.ccApiCart.ACTIVECART => true
				        ,ccrz.ccApiCart.PAYMENTDATA => jsonPaymentData
				        ,ccrz.ccApiCart.TRANSPAYMENTDATA => transactionPaymentData
				        ,ccrz.ccAPICart.PAYMENTRESULT => processPaymentResultMap
				};

				Map<String, Object> placeResultsMap = ccrz.ccApiCart.place(inputDataMap);

				success = (Boolean)placeResultsMap.get(ccrz.ccApi.SUCCESS);
				orderId = (String)placeResultsMap.get(ccrz.ccApiCart.ORDER_ID);

				if(!String.isBlank(orderId)){
					ccrz__E_Order__c orderRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Order__c WHERE Id =: orderId];
					encryptedOrderId = orderRecord.ccrz__EncryptedId__c;
				}
			}
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		if(!success)
			Database.rollback(sp);

		return encryptedOrderId;
	}

	/**
	* Creates a cookie "currCartId" storing the encrypted cart id
	* @param encryptedCartId the encrypted cart id
	* @return null
	*/
	private void addCartCookie(string encryptedCartId){
		Cookie currentCartId = new Cookie('currCartId', encryptedCartId, null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[]{currentCartId});
	}

	/**
	* Re-prices the cart
	* @param encryptedCartId the encrypted cart id
	* @return boolean indicating successful pricing
	*/
	public boolean priceCart(string encryptedCartId){
		boolean success = false;

		//Price the cart
		try{
			Map<String, Object> priceResultMap = ccrz.ccApiCart.price(new Map<String, Object>{
				ccrz.ccApi.API_Version => 6
			   ,ccrz.ccApiCart.CART_ENCID => encryptedCartId
			});

			success = (Boolean)priceResultMap.get(ccrz.ccApi.SUCCESS);
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return success;
	}

	public CartRecord updateBuyerInformation(string encryptedCartId, string firstName, string lastName, string email, string phone){
		CartRecord revisedCartRecord;

		try{
			ccrz__E_Cart__c cartRecord = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encryptedCartId];

			Map<String, Object> inputData = new Map<String, Object>{
		 		ccrz.ccAPI.API_VERSION => 6,
		 		ccrz.ccAPI.SIZING => new Map<String, Object> {
			        ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
			            ccrz.ccAPI.SZ_REFETCH => true
			        }
		    	},
		 		ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>> {
		 			new Map<String, Object> {
		 				'sfid' => cartRecord.Id,
		 				'buyerFirstName' => firstName,
		 				'buyerLastName' => lastName,
		 				'buyerEmail' => email,
		 				'buyerPhone' => phone
		 			}
		 		}
			};

			Map<String, Object> reviseResultMap = ccrz.ccAPICart.revise(inputData);
			boolean success = (Boolean)reviseResultMap.get(ccrz.ccApi.SUCCESS);

			if(success){
				List<Map<String, Object>> cartResultMapList = (List<Map<String, Object>>)reviseResultMap.get(ccrz.ccApiCart.CART_OBJLIST);
				revisedCartRecord = new CartRecord(cartResultMapList[0]);
			}
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return revisedCartRecord;
	}

	public CartRecord updateShippingMethod(string encryptedCartId, string shippingMethod, decimal shippingPrice){
		CartRecord revisedCartRecord;

		try{
			ccrz__E_Cart__c cartRecord = [SELECT ccrz__ShipMethod__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encryptedCartId];
			//Temporary if we find a way to do this through revise
			cartRecord.ccrz__ShipMethod__c = shippingMethod;
			update cartRecord;

			Map<String, Object> inputData = new Map<String, Object>{
		 		ccrz.ccAPI.API_VERSION => 6,
		 		ccrz.ccAPI.SIZING => new Map<String, Object> {
			        ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
			            ccrz.ccAPI.SZ_REFETCH => true
			        }
		    	},
		 		ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>> {
		 			new Map<String, Object> {
		 				'sfid' => cartRecord.Id,
		 				'shipAmount' => shippingPrice
		 				//'shipMethod' => shippingMethod
		 			}
		 		}
			};

			Map<String, Object> reviseResultMap = ccrz.ccAPICart.revise(inputData);
			boolean success = (Boolean)reviseResultMap.get(ccrz.ccApi.SUCCESS);

			if(success){
				List<Map<String, Object>> cartResultMapList = (List<Map<String, Object>>)reviseResultMap.get(ccrz.ccApiCart.CART_OBJLIST);
				revisedCartRecord = new CartRecord(cartResultMapList[0]);
			}
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return revisedCartRecord;
	}

	//Wrapper class to hold cart record and related object data
	public class CartRecord{
		public string id {get; private set;}
		public decimal adjustmentAmount {get; private set;}
		public string billTo {get; private set;}
		public boolean containsKit {get; private set;}
		public string currencyISOCode {get; private set;}
		public string encryptedId {get; private set;}
		public string ownerId {get; private set;}
		public decimal shipAmount {get; private set;}
		public string shipTo {get; private set;}
		public decimal subtotalAmount {get; private set;}
		public decimal totalAmount {get; private set;}
		public decimal totalDiscount {get; private set;}
		public integer totalQuantity {get; private set;}
		public HMR_Coupon_Service.CouponRecord couponRecord {get; private set;}
		public List<CartItem> cartItemList {get; private set;}
		public List<Object> specList {get; private set;}
		public Map<String, Object> cartDataMap {get; private set;}
		public List<Map<String, Object>> addressList {get; private set;}
		public List<Map<String, Object>> productList {get; private set;}

		public CartRecord(Map<String, Object> cartDataMap){
			this(cartDataMap, null, null, null);
		}

		public CartRecord(Map<String, Object> cartDataMap, List<Object> specList, List<Map<String, Object>> addressList, List<Map<String, Object>> productList){
			this.id = (String)cartDataMap.get('sfid');
			this.adjustmentAmount = (Decimal)cartDataMap.get('adjustmentAmount');
			this.billTo = (String)cartDataMap.get('billTo');
			this.currencyISOCode = (String)cartDataMap.get('currencyISOCode');
			this.encryptedId = (String)cartDataMap.get('encryptedId');
			this.ownerId = (String)cartDataMap.get('ownerId');
			this.shipAmount = (Decimal)cartDataMap.get('shipAmount');
			this.shipTo = (String)cartDataMap.get('shipTo');
			this.subtotalAmount = (Decimal)cartDataMap.get('subtotalAmount');
			this.totalAmount = (Decimal)cartDataMap.get('totalAmount');
			this.totalDiscount = (Decimal)cartDataMap.get('totalDiscount');
			this.totalQuantity = (Integer)cartDataMap.get('totalQuantity');

			this.cartDataMap = cartDataMap;

			this.cartItemList = new List<CartItem>();

			List<Map<String, Object>> cartItemMapList = (List<Map<String, Object>>)cartDataMap.get('ECartItemsS');

			for(Map<String, Object> cartItemDataMap : cartItemMapList)
				cartItemList.add(new CartItem(cartItemDataMap));

			//Check if cart contains kit
			this.containsKit = false;
			for(CartItem item : cartItemList){
				if(item.productType == 'Dynamic Kit')
					this.containsKit = true;
			}

			//Check for coupon
			HMR_Coupon_Service couponService = new HMR_Coupon_Service();
			this.couponRecord = couponService.getByCartId(this.id);
			if(couponRecord != null && couponRecord.couponType == 'absolute')
				this.totalDiscount = couponRecord.discountAmount;

			//TODO: Determine if these will be separate wrapper classes once services are built
			this.specList = specList;
			this.addressList = addressList;
			this.productList = productList;
		}
	}

	public class CartItem implements Comparable{
		public string id {get; private set;}
		public decimal absoluteDiscount {get; private set;}
		public string cartItemType {get; private set;}
		public decimal itemTotal {get; private set;}
		public decimal price {get; private set;}
		public integer quantity {get; private set;}
		public string productId {get; private set;}
		public string productType {get; private set;}
		public string sku {get; private set;}

		public CartItem(Map<String, Object> cartItemDataMap){
			this.id = (String)cartItemDataMap.get('sfid');
			this.absoluteDiscount = (Decimal)cartItemDataMap.get('absoluteDiscount');
			this.cartItemType = (String)cartItemDataMap.get('cartItemType');
			this.itemTotal = (Decimal)cartItemDataMap.get('itemTotal');
			this.price = (Decimal)cartItemDataMap.get('price');
			this.quantity = (Integer)cartItemDataMap.get('quantity');

			Map<String, Object> productDataMap = (Map<String, Object>)cartItemDataMap.get('productR');

			this.productId = (String)productDataMap.get('sfid');
			this.productType = (String)productDataMap.get('productType');
			this.sku = (String)productDataMap.get('SKU');
		}

		public Integer compareTo(Object compareTo){
            CartItem compareToCartItem = (CartItem)compareTo;
            return sku == compareToCartItem.sku ? 0 :
            sku > compareToCartItem.sku ? 1 : -1;
        }
	}

	public class ShippingOption{
		public string currencyCode {get; private set;}
		public decimal discount {get; private set;}
		public decimal discountedShipCode {get; private set;}
		public decimal price {get; private set;}
		public string provider {get; private set;}
		public string serviceName {get; private set;}
		public string uniqueId {get; private set;}
		public boolean isSelected {get; private set;}

		public ShippingOption(Map<String, Object> shippingOptionDataMap){
			this.currencyCode = (String)shippingOptionDataMap.get('currencyCode');
			this.discount = (Decimal)shippingOptionDataMap.get('discount');
			this.discountedShipCode = (Decimal)shippingOptionDataMap.get('discountedShipCode');
			this.price = (Decimal)shippingOptionDataMap.get('price');
			this.provider = (String)shippingOptionDataMap.get('provider');
			this.serviceName = (String)shippingOptionDataMap.get('serviceName');
			this.uniqueId = (String)shippingOptionDataMap.get('uniqueId');
		}
	}
}