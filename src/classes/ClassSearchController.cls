public class ClassSearchController {

    public List<Coaching_Session__c> searchedClassList{get;set;}
    public Program_Membership__c membership {get;set;}
    public Contact contactRecord {get;set;}
    public Set<Id> classWithMembershipSet;
    public Class__c classObject {get;set;}
    public Boolean moreRecordLink {get;set;}
    public Id contactId ;
    public Date classDate;
    public Date classInputDate;
    public Integer recordsToShow = 10;
    public Integer numberOfRecords = recordsToShow;
    public String selectedClass{get;set;}
    public string selectedClassFirstClassDate {get; set;}
    public Boolean success {get;set;}
    public Boolean error {get;set;}
    public Boolean noRecords{get;set;}
    public Boolean hideError{get;set;}
    public String noRecordMessage{get;set;}
    public Integer timeOffset {get;set;}
    private static final Map<String, Integer> getMilitaryHour = new Map<String, Integer>{
        '12 AM'=> 0,
        '1 AM'=> 1,
        '2 AM'=> 2,
        '3 AM'=> 3,
        '4 AM'=> 4,
        '5 AM'=> 5,
        '6 AM'=> 6,
        '7 AM'=> 7,
        '8 AM'=> 8,
        '9 AM'=> 9,
        '10 AM'=> 10,
        '11 AM'=> 11,
        '12 PM'=> 12,
        '1 PM'=> 13,
        '2 PM'=> 14,
        '3 PM'=> 15,
        '4 PM'=> 16,
        '5 PM'=> 17,
        '6 PM'=> 18,
        '7 PM'=> 19,
        '8 PM'=> 20,
        '9 PM'=> 21,
        '10 PM'=> 22,
        '11 PM'=> 23
    };
    private static final Map<Integer, String> getStandardHour = new Map<Integer, String>{
         0=> '12 AM',
         1=> '1 AM',
         2=> '2 AM',
         3=> '3 AM',
         4=> '4 AM',
         5=> '5 AM',
         6=> '6 AM',
         7=> '7 AM',
         8=> '8 AM',
         9=> '9 AM',
         10=> '10 AM',
         11=> '11 AM',
         12=> '12 PM',
         13=> '1 PM',
         14=> '2 PM',
         15=> '3 PM',
         16=> '4 PM',
         17=> '5 PM',
         18=> '6 PM',
         19=> '7 PM',
         20=> '8 PM',
         21=> '9 PM',
         22=> '10 PM',
         23=> '11 PM'
    };

/* Class constructor.
*/
    public ClassSearchController() {

        try{
            contactId = apexpages.currentpage().getparameters().get('id');
            if(String.isBlank(contactId)){
                success = false;
                error = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Contact Not Found'));
            }
        }
        catch(Exception ex){
            success = false;
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Contact Id'));
        }

        try{
            classObject = new Class__c();
            classWithMembershipSet = new Set<Id>();
            searchedClassList  = new List<Coaching_Session__c>();
            contactRecord = [SELECT Id, FirstName, LastName,Time_Zone_Offset__c FROM Contact WHERE Id =: contactId];

            if(contactRecord.Time_Zone_Offset__c != null){
                timeOffset = Integer.valueOf(contactRecord.Time_Zone_Offset__c) - 5;
            }
            else{
                timeOffset = 0;
            }
            membership = [SELECT Program__c, Program__r.Name from Program_Membership__c where hmr_Status__c = 'Active' AND hmr_Contact__c = :contactId Limit 1];
            Id programMembershipId = membership.Program__c;
            moreRecordLink = FALSE;
            List<Class__c> selectedClass = [Select id, hmr_First_Class_Date__c from Class__c where hmr_Active__c = true AND Program__c = :programMembershipId];
            for(Class__c classObj : selectedClass){

                classWithMembershipSet.add(classObj.id);
            }
            System.debug('Active Ids -> ' + classWithMembershipSet);
        }
        catch(Exception ex){
        }
    }


    /*  Method called by Search button. This method calls another method
        to get all the Coaching Session records with given time interval.
    */
    public PageReference searchResult(){

        recordsToShow  = numberOfRecords;
        classDate = classObject.hmr_First_Class_Date__c;
        classInputDate = classDate;
        if(classDate == null){

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Week Starting Date'));
        } else {

            searchedClassList = getSelectedClassList(classDate);

        }
        return null;
    }

//BUG IS ON UAT
    /*  Method to get all the coaching session based on the input search
        values.
        Takes 'Date' as Parameter
    */
    public List<Coaching_Session__c> getSelectedClassList(Date firstClassDate){
        String timeOfDay = '';
        Date endDate = firstClassDate.addDays(7);
        /*
            RecordsToShow Limit on query has been removed in order to display all records at once
            7/6/2017 - Ali Pierre (HMR) 
         */
        List<Coaching_Session__c>  classList = Database.Query('Select Id,hmr_Class__r.hmr_First_Class_Date__c, hmr_Class__r.hmr_Class_Name__c,hmr_Class__r.hmr_Coach__r.hmr_Coach__r.Name, Do_Not_Offer__c, hmr_Class_Date__c,hmr_Start_Time__c,hmr_Class__r.hmr_Class_Day__c,' +
                                                                ' hmr_Class__r.hmr_Client_Enrolled__c,hmr_Class__r.hmr_Class_Strength__c, isEasternTime__c from Coaching_Session__c' +
                                                                ' where Do_Not_Offer__c= false AND hmr_Class_Date__c >= :firstClassDate AND hmr_Class_Date__c < :endDate AND ' +
                                                                ' hmr_Class__c IN :classWithMembershipSet'+
                                                                ' ORDER BY hmr_Class_Date__c ASC');
        //Set records to show equal to list size       
        recordsToShow = classList.size();

        if(!classList.isEmpty() && timeOffset != null && timeOffset!= 0){
            for(Coaching_Session__c clsTime : classList){                
                try {
                    String hour = clsTime.hmr_Start_Time__c.substringBefore(':');
                    String min = clsTime.hmr_Start_Time__c.substringBetween(':', ' ');

                    if(clsTime.hmr_Start_Time__c.contains('AM')){
                        hour = hour + ' AM';
                    }
                    else{
                        hour = hour + ' PM';
                    }

                    Integer adjustHour = getMilitaryHour.get(hour);
                    //Loop counter
                    Integer i = 0;
                    
                    if(timeOffset > 0){
                        while (timeOffset > i) {
                            if(adjustHour == 0){
                                //if time is 0(Midnight), reset hour to 23(11 PM)
                                adjustHour = 23;
                            }
                            else{
                                adjustHour -= 1;
                            }
                            i++;
                        }
                    }
                    else if(timeOffset < 0){
                        timeOffset *= -1;
                        while (timeOffset > i) {
                            if(adjustHour == 23){
                                //if time is 23(11 PM), reset hour to 0(Midnight)
                                adjustHour = 0;
                            }
                            else{
                                adjustHour += 1;
                            }                            
                            i++;
                        }
                    }
                    

                    String newTime = getStandardHour.get(adjustHour);

                    clsTime.hmr_Start_Time__c = newTime.contains('AM') ? newTime.substringBefore(' ') + ':' + min + ' AM' : newTime.substringBefore(' ') + ':' + min + ' PM';
                }
                Catch (exception e) {
                      clsTime.isEasternTime__c = true;
                }                
            }
        }        
        else if(!classList.isEmpty() && timeOffset == null){
            for(Coaching_Session__c clsTime : classList){
                clsTime.isEasternTime__c = true;
            }
        }

         Integer totalRecords = [SELECT count() from Coaching_Session__c where hmr_Class_Date__c >= :firstClassDate AND hmr_Class_Date__c < :endDate AND
                                                                hmr_Class__c IN :classWithMembershipSet];

        System.debug('Class List -> ' + classList );
        System.debug('Total Size -> ' + totalRecords + 'Records To Show -> ' + recordsToShow);

      //  totalRecords = classList.size();

        if(totalRecords == 0){
            noRecords = TRUE;
            noRecordMessage = 'No Records To Display';

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Records Found For This Week'));
        }
        else{
            noRecords = FALSE;
        }
        if(totalRecords <= recordsToShow){
            moreRecordLink = TRUE;
        }else{
            moreRecordLink = FALSE;
        }
        return classList;

    }

     /* Method to go to the previous page (pagination).
    */
    public void previous(){

        recordsToShow  = numberOfRecords;
        hideError = TRUE;
        classDate = classDate.addDays(-7);
        searchedClassList = getSelectedClassList(classDate);

    }

    /* Method to go to the next page (pagination).
    */
    public void next(){

        recordsToShow  = numberOfRecords;
        hideError = TRUE;
        classDate = classDate.addDays(7);
        searchedClassList = getSelectedClassList(classDate);
    }


    /* Method to enable OR disable previous week button.
    */
    public boolean getprev()
    {
        if(classDate == classInputDate  )
        return true;
        else
        return false;
    }



    /* Method to enable next week button.
    */
    public boolean getnxt(){

        if(searchedClassList.size() == 0 || searchedClassList == null)
            return true;
        else
            return false;
    }



    /* Method to clear the List
    */
    public void clearSearchList(){

        classObject.hmr_First_Class_Date__c = null;
        searchedClassList = new List<Coaching_Session__c>();
    }



    /* Method assign selected class Id
    */
    public void selectClassId(){

        selectedClass = System.currentPagereference().getParameters().get('classId');
        selectedClassFirstClassDate = System.currentPagereference().getParameters().get('selectedClassClassDate');
        System.debug('Class Id -> ' + selectedClass);
        System.debug('Class Date -> ' + selectedClassFirstClassDate);

    }



    /*  Method to load more results when
        'more' link is clicked.
    */
    public void loadMoreResults(){

        recordsToShow +=3;
        searchedClassList = getSelectedClassList(classDate);
    }



    /* Method to assign the client to
    the selected class
    */
    public PageReference assignClass(){

        System.debug('Class Id -> ' + selectedClass);
        String contactId = apexpages.currentpage().getparameters().get('id');
        // Here we are checking if the cliet has already registered for a class
        List<Class_Member__c> alreadyEnrolled = [SELECT Id from Class_Member__c where hmr_Contact_Name__c = :contactId AND hmr_Active__c = True];  
        if(alreadyEnrolled.size() > 0){
            success = false;
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Client is already enrolled in the Phone Program'));
            return null;
        }

        if(!String.isBlank(selectedClass)){

            Class__c newClassObject = [SELECT Id, hmr_Coach__c FROM Class__c WHERE Id = :selectedClass];
            String [] dateArray = selectedClassFirstClassDate.split('-');
            String year = dateArray[0];
            String month = dateArray[1];
            String day = dateArray[2];
            Date classStartDate = Date.newinstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
            System.debug(classStartDate);
            Class_Member__c classMemberObj = new Class_Member__c();
            classMemberObj.hmr_Class__c = selectedClass;
            classMemberObj.hmr_Contact_Name__c = contactId;
            classMemberObj.hmr_Active__c = true;
            classMemberObj.hmr_Start_Date__c = classStartDate;
            classMemberObj.hmr_Program_Membership__c = membership.Id;
            classMemberObj.hmr_Coach__c = newClassObject.hmr_Coach__c;
            insert classMemberObj;

            membership.hmr_Membership_Type__c = 'Phone Program';
            membership.hmr_Current_Health_Coach__c = newClassObject.hmr_Coach__c;
            update membership;

            searchedClassList = getSelectedClassList(classDate);
            success = true;
            error = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Client is successfully enrolled for this class'));
            PageReference cmPage = new PageReference('/'+classMemberObj.Id);
            return cmPage;
        }else{
            success = false;
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a class to register'));
            return null;
        }
    }

    public PageReference cancel(){

        PageReference clientPage = new PageReference('/'+contactId);

        return clientPage;

    }
}