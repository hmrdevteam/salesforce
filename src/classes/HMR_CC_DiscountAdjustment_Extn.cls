/**
* Mannual discount adjustment
*
* @Date: 2017-03-29
* @Author Aslesha Kokate(Mindtree)
* @Modified: 
* @JIRA: 
*/
global class HMR_CC_DiscountAdjustment_Extn {

@RemoteAction

global static ccrz.cc_RemoteActionResult adjustHMRDiscount(final ccrz.cc_RemoteActionContext ctx,final String cartId, Decimal discountAmount){
    
    ccrz.cc_CallContext.initRemoteContext(ctx);
    ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
    res.success = false; 
   
    ccrz__E_Cart__c cart = [ SELECT Id,ccrz__AdjustmentAmount__c,ccrz__AdjustmentReason__c,ccrz__SubtotalAmount__c,ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE Id =:cartId ];

    system.debug('******=======>'+cart);
    cart.ccrz__AdjustmentAmount__c=discountAmount;
    //cart.ccrz__SubtotalAmount__c=cart.ccrz__SubtotalAmount__c-discountAmount;
    
    //update cart;
    res.success = true; 
    
    return res;
}

}