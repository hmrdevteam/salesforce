/**
* Apex Content Template Controller for Hero Section on Clinic Landing Page
*
* @Date: 05/24/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified: 07/17/2017 --> added property to query for account directly (Pranay Mistry)
* @JIRA:
*/
global virtual class HMR_CMS_ClinicLanderHero_ContentTemplate extends cms.ContentTemplateController implements cms.ServiceInterface{
	global HMR_CMS_ClinicLanderHero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_ClinicLanderHero_ContentTemplate(cms.CoreController controller){
        super();
    }
	global HMR_CMS_ClinicLanderHero_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
 	*/
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
 	*/
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	//Account Information CSS Class Property
	//This is unique to the account record and hence we can query the account Information using this value
	//this property is associated with the Edit Page (HMR_CMS_ClinicLanderHero_EditPage)
    public String AccountCSSClass{
        get{
            return getPropertyWithDefault('AccountCSSClass', '');
		}
    }

	public String convertPhoneNumberToUseDots(String phoneNumber) {
		if(phoneNumber != null && phoneNumber != '') {
			phoneNumber = phoneNumber.replaceAll('[\\s\\-()]', '');
			return phoneNumber.substring(0, 3) + '.' + phoneNumber.substring(3, 6) + '.' + phoneNumber.substring(6, 10);
		}
		return '';
	}

	public String returnNotNullAndNotEmptyString(String value) {
		if(value != null && value != '') {
			return value;
		}
		return '';
	}

	public String returnCSSClassforOptionalValue(String optionalValue) {
		if(optionalValue != null && optionalValue != '') {
			return 'hmr-info';
		}
		return 'bold-header';
	}

	public String returnClinicAccountLocationImageCSS(String locationImageCSS) {
		if(locationImageCSS != null && locationImageCSS != '') {
			return locationImageCSS;
		}
		return 'bg-cliniclandinghero';
	}

	public String returnClinicInfoSingleModal(List<Account> accountsList) {
		String html = '';
		String orientationSessionWithDateTimeOptions = '';String orientationSessionWithDateTimeSelect = '';
		if(!String.isBlank(accountsList[0].hmr_Orientation_Day_1__c) && !String.isBlank(accountsList[0].hmr_Orientation_Time_1__c)) {
			orientationSessionWithDateTimeOptions += String.format('<option value="{0}">{0}</option>', new String[] {accountsList[0].hmr_Orientation_Day_1__c + ', ' + accountsList[0].hmr_Orientation_Time_1__c});
		}
		if(!String.isBlank(accountsList[0].hmr_Orientation_Day_2__c) && !String.isBlank(accountsList[0].hmr_Orientation_Time_2__c)) {
			orientationSessionWithDateTimeOptions += String.format('<option value="{0}">{0}</option>', new String[] {accountsList[0].hmr_Orientation_Day_2__c + ', ' + accountsList[0].hmr_Orientation_Time_2__c});
		}
		if(!String.isEmpty(orientationSessionWithDateTimeOptions)) {
			orientationSessionWithDateTimeSelect += '<label for="tc_orientation">Select preferred Information Session</label>' + '<select class="form-control hmr-formcontrol" required="" id="tc_orientation">' + orientationSessionWithDateTimeOptions + '</select>';
		}
		html += '<div class="hmr-modal modal fade" id="clinicInfoModalDialog" data-name="' + accountsList[0].Name + '" data-id="' + accountsList[0].Id + '" tabindex="-1" role="dialog" aria-labelledby="clinicSingleModalLabel" aria-hidden="true">' +
				    '<div class="hmr-clinic-modal-dialog modal-dialog" role="document">' +
				        '<div class="modal-content hmr-modal-content">' +
				            '<div class="modal-header hmr-modal-header">' +
				                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
				                    '<span aria-hidden="true">x</span>' +
				                '</button>' +
				            '</div>' +
				            '<div class="modal-body hmr-modal-body">' +
				                '<div class="row text-center">' +
				                    '<h2 id="clinicInfoModalHeader">Get Started</h2>' +
				                '</div>' +
				                '<div class="row hmr-info clinic-info-to-hide">' +
				                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
				                        'Complete the following form to receive more information about the HMR Program near you.' +
				                    '</div>' +
				                '</div>' +
				                '<div class="row form-cliniclanding clinic-info-to-hide">' +
				                    '<form id="clinicLandingInfoForm">' +
										'<label for="clinicFN">First Name *</label>' +
										'<input type="text" id="clinicFN" class="form-control hmr-formcontrol" name="clinicFN" autofocus="" placeholder="First Name">' +
										'<label for="clinicLN">Last Name *</label>' +
										'<input type="text" id="clinicLN" class="form-control hmr-formcontrol" name="clinicLN" placeholder="Last Name">' +
										'<label for="clinicEmail">Email *</label>' +
										'<input type="email" id="clinicEmail" class="form-control hmr-formcontrol" name="clinicEmail"  placeholder="myemail@example.com">' +
										'<label for="clinicPhone">Phone Number</label>' +
										'<input type="text" id="clinicPhone" class="form-control hmr-formcontrol" placeholder="888-888-8888">' +
										'<label for="clinicSource">How did you hear about us?</label>' +
										'<select class="form-control hmr-formcontrol" required="" id="clinicSource">';
										for(String leadSource : getLeadSourcePickListValues())
											 html += String.format('<option value="{0}">{0}</option>', new String[] {leadSource});
								html +=	'</select>' + orientationSessionWithDateTimeSelect +
				                    '</form>' +
				                '</div>' +
				                '<div class="row text-center clinic-info-to-hide">' +
				                    '<div class="btn-container">' +
				                        '<a id="clinicInfoRequestInformationBtn" class="btn btn-primary hmr-btn-blue hmr-btn-small">Request Information</a>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				    '</div>' +
				'</div>';
		return html;
	}

	public String returnClinicInfoDoubleModal(List<Account> clinicAccountsList) {
		String html = '';
		html += '<div class="clinicDoubleModal hmr-modal modal fade" id="clinicInfoModalDialog" tabindex="-1" role="dialog" aria-labelledby="clinicDoubleModalLabel" aria-hidden="true">' +
				    '<div class="hmr-clinic-modal-dialog modal-dialog modal-lg" role="document">' +
				        '<div class="modal-content hmr-modal-content">' +
				            '<div class="modal-header hmr-modal-header">' +
				                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
				                    '<span aria-hidden="true">x</span>' +
				                '</button>' +
				            '</div>' +
				            '<div class="modal-body hmr-modal-body">' +
				                '<div class="row text-center">' +
				                    '<h2 id="clinicInfoModalHeader">Get Started</h2>' +
				                '</div>' +
				                '<div class="row hmr-info clinic-info-to-hide">' +
				                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
				                        'Complete the following form to receive more information about the HMR Program near you.' +
				                    '</div>' +
				                '</div>' +
				                '<div class="row clinic-info-to-hide">' +
				                    '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">' +
									'<div class="clinic-info-radiobtn-error col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">Please select a clinic</div>';
		for(Account clinicAccount: clinicAccountsList) {
			html += '<div class="clinic-info-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
						'<div class="get-clinic-info hmr-form-control no-padding">' +
							'<input onclick="javascript:cldg.o();" data-name="' + clinicAccount.Name + '" id="' + clinicAccount.Id + '" type="radio" name="clinicInfoRadio" class="blue">' +
							'<label class="form-control__label blue" for="' + clinicAccount.Id + '">' +
							'</label>' +
						'</div>' +
						'<div class="get-clinic-info">' +
							'<div class="bold-header">' +
								returnNotNullAndNotEmptyString(clinicAccount.HMR_Account_Name_1_Optional__c) +
							'</div>' +
							'<div class="' + returnCSSClassforOptionalValue(clinicAccount.HMR_Account_Name_1_Optional__c) + '">' +
								 returnNotNullAndNotEmptyString(clinicAccount.Name) +
							'</div>' +
							'<div class="hmr-info">' +
								returnNotNullAndNotEmptyString(clinicAccount.BillingStreet) +
							'</div>' +
							'<div class="hmr-info">' +
								returnNotNullAndNotEmptyString(clinicAccount.BillingCity) + ',' + ' ' +
								returnNotNullAndNotEmptyString(clinicAccount.BillingState) + ',' + ' ' +
								returnNotNullAndNotEmptyString(clinicAccount.BillingPostalCode) +
							'</div>' +
							'<div class="hmr-info">' +
								convertPhoneNumberToUseDots(returnNotNullAndNotEmptyString(clinicAccount.Phone)) +
							'</div>' +
						'</div>' +
					'</div>';
		}
		html +=	'</div>' +
                '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">' +
                    '<div class="row form-cliniclanding">' +
                        '<form id="clinicLandingInfoForm">' +
                            '<label for="clinicFN">First Name *</label>' +
                            '<input type="text" id="clinicFN" class="form-control hmr-formcontrol" name="clinicFN" autofocus="" placeholder="First Name">' +
                            '<label for="clinicLN">Last Name *</label>' +
                            '<input type="text" id="clinicLN" class="form-control hmr-formcontrol" name="clinicLN" placeholder="Last Name">' +
                            '<label for="clinicEmail">Email *</label>' +
                            '<input type="email" id="clinicEmail" class="form-control hmr-formcontrol" name="clinicEmail"  placeholder="myemail@example.com">' +
                            '<label for="clinicPhone">Phone Number</label>' +
                            '<input type="text" id="clinicPhone" class="form-control hmr-formcontrol" placeholder="888-888-8888">' +
                            '<label for="clinicSource">How did you hear about us?</label>' +
							'<select class="form-control hmr-formcontrol" required="" id="clinicSource">';
							for(String leadSource : getLeadSourcePickListValues())
								 html += String.format('<option value="{0}">{0}</option>', new String[] {leadSource});
					html +=	'</select>' +
                        '</form>' +
						'<div class="btn-container text-center">' +
                            '<a id="clinicInfoRequestInformationBtn" class="btn btn-primary hmr-btn-blue hmr-btn-small">Request Information</a>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '</div>' +
	            '</div>' +
		        '</div>' +
			    '</div>' +
				'</div>';
		return html;
	}

	public List<String> getLeadSourcePickListValues(){
       	List<String> pickListValuesList= new List<String>();
		try {
			Schema.DescribeFieldResult fieldResult = Lead.How_did_you_hear_about_us__c.getDescribe();
	        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        for( Schema.PicklistEntry pickListVal : ple){
	            pickListValuesList.add(pickListVal.getLabel());
	        }
		}
		catch(Exception ex) {
			System.debug('@@@ Exception in getLeadSourcePickListValues' + ex.getMessage());
			pickListValuesList.clear();
		}
        return pickListValuesList;
    }

	global static String executeRequest(Map<String, String> params) {
		Map<String, Object> responseString = new Map<String, Object>();
		try {
			Lead clinicInfoLeadToInsert = new Lead(
				Account__c = params.get('Account__c'),
				Company = params.get('Company'),
				FirstName = params.get('FirstName'),
				LastName = params.get('LastName'),
				Email = params.get('Email'),
				Phone = params.get('Phone'),
				LeadSource = params.get('LeadSource'),
				hmr_Lead_Source_Detail__c = params.get('hmr_Lead_Source_Detail__c'),
				How_did_you_hear_about_us__c = params.get('How_did_you_hear_about_us__c'),
				hmr_Lead_Type__c = params.get('hmr_Lead_Type__c')
			);
			if(!String.isBlank(params.get('hmr_Orientation_Day__c'))) {
				clinicInfoLeadToInsert.hmr_Orientation_Day__c = params.get('hmr_Orientation_Day__c');
			}
			if(!String.isBlank(params.get('hmr_Orientation_Time__c'))) {
				clinicInfoLeadToInsert.hmr_Orientation_Time__c = params.get('hmr_Orientation_Time__c');
			}
			insert clinicInfoLeadToInsert;
			responseString.put('success', true);
			responseString.put('clinicInfoLeadToInsert', (Object)clinicInfoLeadToInsert);
		}
		catch(Exception ex) {
			System.debug('@@@ Exception in HMR_CMS_ClinicLanderHero_ContentTemplate executeRequest' + ex.getMessage());
			responseString.put('clinicInfoLeadToInsert', (String)ex.getMessage());
		}
		return JSON.serialize(responseString);
	}

	/**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_ClinicLanderHero_ContentTemplate.class;
    }

	global virtual override String getHTML(){
		String html = '';String accountsHTML = '';String clinicAccountLocationImageCSS = '';
		Map<String, String> pageParams = ApexPages.currentPage().getParameters();
		if((AccountCSSClass != null && AccountCSSClass != '') || (pageParams.get('z') != null && pageParams.get('z') != '')) {
			Set<Id> AccountIds = new Set<Id>();
			if(pageParams.get('z') != null && pageParams.get('z') != '') {
				List<Zip_Code_Assignment__c> zipCodeAssignmentsList =
					[SELECT Id, Account__c, Name FROM Zip_Code_Assignment__c WHERE Name = :pageParams.get('z') AND Account__r.hmr_Active__c = TRUE AND Account__r.hmr_Referral__c != 'DNR'];

				if(zipCodeAssignmentsList != null && !zipCodeAssignmentsList.isEmpty()) {
					for(Zip_Code_Assignment__c zipCodeAssignment: zipCodeAssignmentsList) {
						AccountIds.add(zipCodeAssignment.Account__c);
					}
				}
			}
			if(AccountCSSClass != null && AccountCSSClass != '') {
				List<Account> accountsList =
					[SELECT Id, hmr_Location_Image_CSS__c, hmr_Location_Type__c FROM Account
						WHERE hmr_Location_Image_CSS__c = :AccountCSSClass AND hmr_Location_Type__c = 'HMR Training Center'
						      AND hmr_Active__c = TRUE AND hmr_Referral__c != 'DNR'];
				if(accountsList != null && !accountsList.isEmpty()) {
					for(Account account: accountsList) {
						AccountIds.add(account.Id);
					}
				}
			}
			if(!AccountIds.isEmpty()) {
				List<Account> clinicAccountsList =
					[SELECT Id, Name, HMR_Account_Name_1_Optional__c, BillingStreet, BillingCity, BillingState, hmr_Location_Image_CSS__c,
						BillingPostalCode, Phone, hmr_Web_Site_HMR__c, hmr_Orientation_Day_1__c, hmr_Orientation_Day_2__c,
						hmr_Orientation_Time_1__c, hmr_Orientation_Time_2__c FROM Account WHERE Id IN :AccountIds];
				String modalClickHandler = '';
				if(AccountIds.size() == 1) {
					html += returnClinicInfoSingleModal(clinicAccountsList);
					//modalClickHandler = 'onclick="javascript:cldg.s(\'' + String.valueOf(clinicAccountsList[0].Id) + '\');" ';
					modalClickHandler = 'onclick="javascript:cldg.s(\'' + String.valueOf(clinicAccountsList[0].Id) + '\', \'' + String.valueOf(clinicAccountsList[0].Name) + '\');" ';
				}
				else {
					html += returnClinicInfoDoubleModal(clinicAccountsList);
					modalClickHandler = 'onclick="javascript:cldg.m_o_c(this);" ';
				}
				for(Account clinicAccount: clinicAccountsList) {
					accountsHTML += '<div class="clinic-info-wrapper">' +
										'<div class="bold-header">' +
											returnNotNullAndNotEmptyString(clinicAccount.HMR_Account_Name_1_Optional__c) +
										'</div>' +
										'<div class="' + returnCSSClassforOptionalValue(clinicAccount.HMR_Account_Name_1_Optional__c) + '">' +
											returnNotNullAndNotEmptyString(clinicAccount.Name) +
										'</div>' +
										'<div class="hmr-info">' +
					                        returnNotNullAndNotEmptyString(clinicAccount.BillingStreet) +
					                    '</div>' +
					                    '<div class="hmr-info">' +
					                        returnNotNullAndNotEmptyString(clinicAccount.BillingCity) + ',' + ' ' +
											returnNotNullAndNotEmptyString(clinicAccount.BillingState) + ',' + ' ' +
											returnNotNullAndNotEmptyString(clinicAccount.BillingPostalCode) +
					                    '</div>' +
					                    '<div class="hmr-info">' +
					                        convertPhoneNumberToUseDots(returnNotNullAndNotEmptyString(clinicAccount.Phone)) +
					                    '</div>';
					if(clinicAccount.hmr_Web_Site_HMR__c != null && clinicAccount.hmr_Web_Site_HMR__c != '') {
						if(!clinicAccount.hmr_Web_Site_HMR__c.startsWith('http'))
							clinicAccount.hmr_Web_Site_HMR__c = 'http://' + clinicAccount.hmr_Web_Site_HMR__c;

						accountsHTML += '<div class="hmr-info">' +
											'<a href="' + clinicAccount.hmr_Web_Site_HMR__c + '" target="_blank" class="clinic-website-link">visit website</a>' +
										'</div>';
					}
					accountsHTML += '<div>' +
							        '<a data-name="' + clinicAccount.Name + '" data-id="' + clinicAccount.Id + '" data-toggle="modal" ' + modalClickHandler + 'data-target="#clinicInfoModalDialog" class="hmr-allcaps blue clinic-req-info-link">Request Information</a>' +
							        '</div>' +
							        '</div>';
					if(String.isBlank(clinicAccountLocationImageCSS) && !String.isBlank(clinicAccount.hmr_Location_Image_CSS__c))
						clinicAccountLocationImageCSS = returnClinicAccountLocationImageCSS(returnNotNullAndNotEmptyString(clinicAccount.hmr_Location_Image_CSS__c));
				}
			}

			html += '<section class="hmr-cliniclanding-hero-section bg-typecover ' + clinicAccountLocationImageCSS + '">' +
				        '<div class="container visible-lg visible-md">' +
				            '<div class="hero-row">' +
				                '<div>' +
				                    '<h2>An HMR Program is Close By!</h2>' +
				                '</div>' +
								accountsHTML +
				            '</div>' +
				        '</div>' +
				    '</section>' +
					'<section class="hmr-cliniclanding-text-section">' +
				        '<div class="container visible-sm visible-xs">' +
				            '<div class="non-hero-row">' +
				                '<div>' +
				                    '<h2>An HMR Program is Close By!</h2>' +
				                '</div>' +
								accountsHTML +
				            '</div>' +
				        '</div>' +
				    '</section>';
		}
		return html;
	}
}