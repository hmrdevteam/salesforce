/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 31st, 2017
Target Class: HMR_CMS_RateArticle_ContentTemplate
*/

@isTest
private class HMR_CMS_RateArticle_ContentTemplate_Test {
    @isTest 
    private static void testGetHtml() {
        Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_Community_PageTemplate'));
        System.currentPageReference().getParameters().put('urlName', 'test');
        
        HMR_CMS_RateArticle_ContentTemplate rateArticle = new HMR_CMS_RateArticle_ContentTemplate();          
        String propertyValue = rateArticle.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');  
      
        String respHTML = rateArticle.getHTML();              
        
        System.assert(!String.isBlank(respHTML));
    }       
           
}