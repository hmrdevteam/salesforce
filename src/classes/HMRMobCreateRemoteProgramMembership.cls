@RestResource(urlMapping='/mobile/user/createRemoteProgram/*')
global class HMRMobCreateRemoteProgramMembership {

    @HttpGet
    global static Program_Membership__c doGet() {
        String contactId = RestContext.request.params.get('contactId');
        List<Program_Membership__c> pmList = 
            [SELECT hmr_Contact__c,hmr_Status__c FROM Program_Membership__c 
             	where hmr_Contact__c =: contactId
            	and hmr_Status__c = 'Active'];
        
        for(Program_Membership__c pm: pmList ) {
            pm.hmr_Status__c = 'Dropped';
        }
		
        update pmList;
        
        
        Program__c p = [SELECT Id, name FROM Program__c WHERE Name = 'Phase 2' limit 1];
        Program_Membership__c pm = new Program_Membership__c();
        pm.Program__c = p.Id;
        pm.hmr_Contact__c = contactId;
        pm.hmr_Status__c = 'Active';
        
        insert pm;
        return pm;
    }
    
}