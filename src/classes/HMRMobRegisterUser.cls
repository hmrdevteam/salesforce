/**
 * this class uses the HMR_Registration_Service to register a user
 */
@RestResource(urlMapping='/mobile/user/communityUser/*')
global class HMRMobRegisterUser {
       
    /**
     * method to check if user already exists
     * @param Email id
     * @return Map of status and boolean value for user existence  
     * @return Map with status equals to false if there is any error And True or false if user is present
     */
    @HttpGet
    global static void doesUserExist() {
    
        String emailId = RestContext.request.params.get('email');
        RestResponse response = RestContext.response;
        
        Map<String, Object> returnMap = new Map<String, Object>();
        List<User> userList = new List<User>();
        
        try{
            userList = [SELECT id FROM User WHERE email = :emailId];

            if(userList.size()>0){
                response.statusCode = 200;    
                returnMap.put('success', true);
                returnMap.put('doesExist', true);
            }
            else{
                response.statusCode = 200;    
                returnMap.put('success', true);
                returnMap.put('doesExist', false);
            }
        }
        catch(Exception ex){
            response.statusCode = 400;
            returnMap.put('success', false);
            returnMap.put('doesExist', 'Exception ->  ' + ex.getMessage() + '  At Line Number  ->  ' + ex.getStackTraceString());        
        }
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  
    } 
   
    
    /**
     * Post method to register a user
     * @param firstname
     * @param lastname
     * @param email
     * @param password
     * @param dob
     * @return Map containing registration results
    */
    @HttpPost
    global static void doPost(String firstname, String lastname, String email, String password, Date dob) {
        RestResponse response = RestContext.response;
        HMR_Registration_Service registrationService = new HMR_Registration_Service();
        Map<String, Object> registrationResultMap;
        
        try {
            registrationResultMap = registrationService.processRegistration(firstname, lastname, email, password, 'na', false, 'General');
            
            if (registrationResultMap.containsKey('userId')) {
                String userId = (String)registrationResultMap.get('userId');
                // if the registration is successful, we need to update the contact dob
                Contact c = [SELECT hmr_Date_Of_Birth__c FROM Contact WHERE Community_User_ID__c  =: userId];            
                c.hmr_Date_Of_Birth__c = dob;
                update c;

                registrationResultMap.put('contactId', c.Id);
                
                response.statusCode = 201;
            }
            else
                response.statusCode = 500;
        } catch (Exception e) {
            registrationResultMap = new Map<String, Object>();
            registrationResultMap.put('success', false);
            registrationResultMap.put('error', e.getMessage());
            
            response.statusCode = 500;
        }

        response.responseBody = Blob.valueOf(JSON.serialize(registrationResultMap));
    }
}