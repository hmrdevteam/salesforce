/**
* Service Class for Registration methods
* 	To consume service, call processRegistration and pass in:
* 	First Name, Last Name, Email Address, Password,
* 	Code (zip code for general, employer code for employer), preference (user selection for email opt in), and
* 	RegType ('General' for general registration, and 'Employer' when used for Employer registration)
* Service returns a Map<String, Object>. If successful, returns (success: true) and (userId: the new user's Id). If unsuccessful, returns (success: false) and any error messages
* @Date: 2017-11-29
* @Author Nathan Anderson (Magnet 360)
* @Modified:
* @JIRA:
*/

public with sharing class HMR_Registration_Service {

	public HMR_Registration_Service() {

	}

	public Map<String, Object> processRegistration(String firstname, String lastname, String email, String password, String code, Boolean preference, String regType) {

		Map<String, Object> returnMap = new Map<String, Object>();
		String PORTAL_ACCOUNT_ID = '';
		String patientType = '';
		Boolean invalidEmpCode = false;
		String userId;
		String zipcode = '';


		if(regType == 'Employer'){
			PORTAL_ACCOUNT_ID = registerEmployers(code);
			patientType = 'Employer Group Member';

			if(PORTAL_ACCOUNT_ID == '') {
				invalidEmpCode = true;
				returnMap.put('success', false);
                returnMap.put('errorMsg', 'Please enter a valid Employer Code.');
			} else {
				//register
				try {
					userId = siteRegister(firstname, lastname, email, PORTAL_ACCOUNT_ID, password, patientType, preference, zipcode);
				} catch(Exception ex) {
					returnMap.put('success', false);
					returnMap.put('exceptionIn', 'EmployerSiteRegister');
					returnMap.put('exceptionMsg', ex.getMessage());
				}

				if(userId != null) {
					returnMap.put('success', true);
	                returnMap.put('userId', userId);
				} else {
					returnMap.put('success', false);
	                returnMap.put('exceptionIn', 'EmployerSiteRegister');
				}
			}
		}

		if(regType == 'General') {
			//route existing leads
			//If an exisiting Lead has a matching email address, convert this lead into a contact
	        List<Lead> existingLeads = new List<Lead>([SELECT Id, Account__c FROM Lead WHERE Email = :email LIMIT 1]);
	        if(!existingLeads.isEmpty()){
	            PORTAL_ACCOUNT_ID = ConvertExistingLead(existingLeads);
	        } else {
				if(!String.isBlank(code)){
					PORTAL_ACCOUNT_ID = ZipCodeAssociation(code);
				}

				if(PORTAL_ACCOUNT_ID != null && PORTAL_ACCOUNT_ID != '') {
					patientType = 'Clinic Patient';
				} else {
					PORTAL_ACCOUNT_ID = createClientAccount(firstName, lastName);
				}
			}
			//register
			zipcode = code;

			try {
				userId = siteRegister(firstname, lastname, email, PORTAL_ACCOUNT_ID, password, patientType, preference, zipcode);
			} catch(Exception ex) {
				returnMap.put('success', false);
				returnMap.put('exceptionIn', 'GeneralSiteRegister');
				returnMap.put('exceptionMsg', ex.getMessage());
			}

			if(userId != null) {
				returnMap.put('success', true);
				returnMap.put('userId', userId);
			} else {
				returnMap.put('success', false);
				returnMap.put('exceptionIn', 'GeneralSiteRegister');
			}
		}

		return returnMap;

	}


	public String ZipCodeAssociation(String zip) {

		String PORTAL_ACCOUNT_ID = '';

		List<Zip_Code_Assignment__c> zipCodeAssigment = [SELECT ID, Name, Account__c FROM Zip_Code_Assignment__c
															WHERE Name = :zip AND Account__c <> null
															AND Account__r.hmr_Active__c = TRUE
															AND Account__r.hmr_Referral__c != 'DNR' LIMIT 1];

		if(!zipCodeAssigment.isEmpty()){
			PORTAL_ACCOUNT_ID = zipCodeAssigment[0].Account__c;
		}
		return PORTAL_ACCOUNT_ID;
	}

	public String ConvertExistingLead(List<Lead> existingLeads) {
        try {
            Lead existingLead = existingLeads[0];
            Database.LeadConvert lc = new Database.LeadConvert();
			Account a = new Account();

            lc.setLeadId(existingLead.id);

            if(existingLead.Account__c != null){
                lc.setAccountId(existingLead.Account__c);
            } else {
                //get the PortalAccount Account Group Id
                Id agId = [SELECT Id FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount'].Id;
                //get the Customer Account Record Type
                Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;

                Lead l = [Select FirstName, LastName FROM Lead WHERE Id = :existingLead.Id];

                //Create the Account for the Client, assign the PortalAccount Group
                User uOwner = [SELECT Id, Name, IsActive FROM USER Where Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];

				a.OwnerId = uOwner.Id;
                a.Name = l.firstname + ' ' + l.lastname;
                a.ccrz__E_AccountGroup__c = agId;
                a.RecordTypeId = rtype;

                insert a;

                lc.setAccountId(a.Id);
            }

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(TRUE);

            Database.LeadConvertResult lcr = Database.convertLead(lc);

			String PORTAL_ACCOUNT_ID = '';

            if(lcr.isSuccess()){

                if(existingLead.Account__c != null){
                    PORTAL_ACCOUNT_ID = existingLead.Account__c;

                } else {
                    PORTAL_ACCOUNT_ID = a.Id;
                }
            }

			return PORTAL_ACCOUNT_ID;
        }
        catch(Exception e) {
            System.debug('@@@@@@@@@@@@ Exception in Lead Conversion' + e);
			return null;
        }
    }

	public String registerEmployers(String eCode) {

		String PORTAL_ACCOUNT_ID = '';
		//query for Account field hmr_Employer_Account_ID__c and match it with employer code
        //get the account record
        //assign the Portal Id var to Account id
        List<Account> ecomAccountList = [Select Id, hmr_Employer_Account_ID__c, name FROM Account WHERE hmr_Employer_Account_ID__c = :eCode LIMIT 1];

        if(!ecomAccountList.isEmpty()){
            PORTAL_ACCOUNT_ID = ecomAccountList[0].id;
        }
		return PORTAL_ACCOUNT_ID;
    }

	public String createClientAccount(String firstname, String lastname) {

		String PORTAL_ACCOUNT_ID = '';

		ccrz__E_AccountGroup__c accountGroup = [SELECT ID, Name FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount' LIMIT 1];

		User uOwner = [SELECT Id, Name, IsActive FROM USER Where Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];

		Account a = new Account();
		a.Name = firstname + ' ' + lastname;
		a.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;
		a.ccrz__E_AccountGroup__c = accountGroup.id;
		a.OwnerId = uOwner.Id;
		try{
			insert a;
		} catch(Exception e) {

			return null;
		}
		if(a.Id != null) {
			PORTAL_ACCOUNT_ID = a.Id;
		}
		return PORTAL_ACCOUNT_ID;
	}

	public String siteRegister(String firstname, String lastname, String email, String PORTAL_ACCOUNT_ID, String password, String patientType, Boolean preference, String zipcode){

		String userId;
		User u = new User();
		u.FirstName = firstname;
		u.LastName = lastname;
		u.Username = email;
		u.Email = email;
		u.TimeZoneSidKey = 'America/New_York';
		string nickName = email.split('@').get(0);
		if(nickName.length()>34){
			nickName = nickName.substring(0,34);
		}
		nickName = nickName+'_'+String.valueOf(Math.random()).substring(2,7);
		u.CommunityNickname = nickName;
		// u.Alias = communityNickname.substring(0, communityNickname.length());
		Integer lastNameSubLength = lastname.length();
		String alias = (firstname.substring(0, 1) + lastname.substring(0, lastNameSubLength)).toLowerCase();
		u.Alias = alias.left(8);

		//String accountId = PORTAL_ACCOUNT_ID;

		System.debug('@@@@@@@@@ createExternalUser: ' + u + ',' + PORTAL_ACCOUNT_ID + ',' + password);
		userId = Site.createExternalUser(u, PORTAL_ACCOUNT_ID, password);

		if(userId != null) {

			//update Contact record with entered email preference and zip code
            Id contactId = [SELECT Id, ContactId FROM User WHERE Id = :userId].ContactId;
            Contact c = [SELECT Id, FirstName, LastName, HMR_Email_Preference__c, MailingPostalCode, Patient_Type__c, HasOptedOutOfEmail FROM Contact WHERE Id =:contactId];
            Boolean preference_b = Boolean.valueOf(preference);
			c.FirstName = firstname;
			c.LastName = lastname;
            c.HMR_Email_Preference__c = preference_b;
            c.HasOptedOutOfEmail = !preference_b;
            c.hmr_Contact_Original_Source__c = 'Website';
            c.hmr_Contact_Original_Source_Detail__c = 'Self-Registration';
            if(!String.isBlank(zipcode)){
                c.MailingPostalCode = zipcode;
            }
            if(patientType != null){
                c.Patient_Type__c = patientType;
            }

            try {
                update c;
            }
            catch(Exception ex) {
                // String error = ex.getMessage();
                // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
                return null;
                //Database.rollback(sp);
            }

			assingPermissionSet(userId);

		}
		return userId;
	}

	@future
    public static void assingPermissionSet(Id userIdp) {
        PermissionSetAssignment psa = new PermissionSetAssignment();
        Id permSet = [SELECT Id FROM PermissionSet WHERE Name = 'ocms_autogen_SiteViewer'].Id;
        psa.PermissionSetId = permSet;
        psa.AssigneeId = userIdp;
        try {
            insert psa;
        }
        catch(Exception ex) {
            System.debug('excpetionIn AssignPermissionSet');
            System.debug('exceptionMsg' + ex.getMessage());
            System.debug('exceptiongetTypeName' + ex.getTypeName());
            System.debug('exceptiongetLineNumber' + ex.getLineNumber());
        }
    }
}