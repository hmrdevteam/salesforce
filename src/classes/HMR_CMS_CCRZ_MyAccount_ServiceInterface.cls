/**
* Service Interface to get Account Details
*
* @Date: 2017-11-15
* @Author: Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/
global with sharing class HMR_CMS_CCRZ_MyAccount_ServiceInterface implements cms.ServiceInterface{
  /**
  *
  * @param params a map of parameters including at minimum a value for 'action'
  * @return a JSON-serialized response string
  */
    public String executeRequest(Map<String, String> params) {
     Map<String, Object> returnMap = new Map<String, Object>{'success' => true};

        if(params.get('userId') != null && params.get('accountId') != null){
            string userId = String.escapeSingleQuotes(params.get('userId'));
            HMR_Account_Service accountService = new HMR_Account_Service(userId);

            try {
                String action = params.get('action');
                //Return the calling function/action
                returnMap.put('action', action);

                if(action == 'getAccountAddresses') {
                    //string accountId = params.get('accountId');
                    //string addressType = params.get('addressType');
                    List<HMR_CMS_AddressService.AddressRecord> addressList = accountService.getAddressRecords(userId, null);
                    returnMap.put('addresses', addressList);
                }
                else if(action == 'confirmPassword') {
                    string oldPass = params.get('oldpassword');
                    string newPass = params.get('newPassword');
                    string cnfNewPass = params.get('cnfPassword');
                    String changePassStr = accountService.changePassword(oldPass, newPass, cnfNewPass);
                    returnMap.put('changepass', changePassStr);
                }
                else if (action == 'getStoredPayments'){
                    HMR_StoredPayment_Service storedPaymentService = new HMR_StoredPayment_Service(userId);
                    List<HMR_StoredPayment_Service.StoredPaymentRecord> storedPaymentList = storedPaymentService.getByOwnerId();
                    returnMap.put('storedPayments', storedPaymentList);
                }
                else if (action == 'getAboutMeInfo'){
                    String portalUserId = params.get('userId');
                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(portalUserId);
                    returnMap.put('contactInfo', conRec);
                }
                else if(action == 'updateAboutMeInfo') {
                    String portalUserId = params.get('userId');
                    String contactJSON = String.escapeSingleQuotes(params.get('contactData'));

                    Map<String, Object> contactInfoMap = (Map<String, Object>)JSON.deserializeUntyped(contactJSON);

                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    contactService.updateContactInfo(portalUserId, contactInfoMap);

                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(portalUserId);
                    returnMap.put('contactInfo', conRec);
                }
                else if (action == 'getProfileInfo'){
                    String portalUserId = params.get('userId');
                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(portalUserId);
                    List<String> motivationSelections = HMR_Picklist_Service.getPicklistValuesByObjectAndFieldApiName('Program_Membership__c', 'Motivation__c');
                    List<String> supportNeededSelections = HMR_Picklist_Service.getPicklistValuesByObjectAndFieldApiName('Program_Membership__c', 'SupportNeeded__c');

                    returnMap.put('contactInfo', conRec);
                    returnMap.put('motivationSelections', motivationSelections);
                    returnMap.put('supportNeededSelections', supportNeededSelections);
                }
                else if (action == 'updateProfileInfo'){
                    String portalUserId = params.get('userId');
                    String contactJSON = String.escapeSingleQuotes(params.get('contactData'));

                    Map<String, Object> contactInfoMap = (Map<String, Object>)JSON.deserializeUntyped(contactJSON);

                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    returnMap = contactService.updateProfileInfo(portalUserId, contactInfoMap);

                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(portalUserId);
                    List<String> motivationSelections = HMR_Picklist_Service.getPicklistValuesByObjectAndFieldApiName('Program_Membership__c', 'Motivation__c');
                    List<String> supportNeededSelections = HMR_Picklist_Service.getPicklistValuesByObjectAndFieldApiName('Program_Membership__c', 'SupportNeeded__c');
                    returnMap.put('contactInfo', conRec);
                    returnMap.put('motivationSelections', motivationSelections);
                    returnMap.put('supportNeededSelections', supportNeededSelections);
                }
                else if(action == 'createAddress') {
                    String addressJSON = String.escapeSingleQuotes(params.get('addressData'));
                    //string accountId = params.get('accountId');
                    accountService.addAddressToAccount(userId, addressJSON.unescapeJava());
                    List<HMR_CMS_AddressService.AddressRecord> addressList = accountService.getAddressRecords(userId, null);
                    returnMap.put('addresses', addressList);
                }
                else if(action == 'updateAddress') {
                    String addressJSON = String.escapeSingleQuotes(params.get('addressData'));
                    //string accountId = params.get('accountId');
                    accountService.updateAddress(userId, addressJSON.unescapeJava());
                    List<HMR_CMS_AddressService.AddressRecord> addressList = accountService.getAddressRecords(userId, null);
                    returnMap.put('addresses', addressList);
                }
                else if(action == 'deleteAddress') {
                    String addressJSON = String.escapeSingleQuotes(params.get('addressData'));
                    //string accountId = params.get('accountId');
                    accountService.deleteAddress(addressJSON.unescapeJava());
                    List<HMR_CMS_AddressService.AddressRecord> addressList = accountService.getAddressRecords(userId, null);
                    returnMap.put('addresses', addressList);
                }
                else if(action == 'updatePhoto') {
                    String photo = String.escapeSingleQuotes(params.get('photo'));
                    accountService.updatePhoto(userId, photo);
                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(userId);
                    returnMap.put('contactInfo', conRec);
                }
                else if(action == 'removePhoto') {
                    accountService.removePhoto(userId);
                    HMR_Contact_Service contactService = new HMR_Contact_Service();
                    List<HMR_Contact_Service.ContactRecord> conRec = contactService.getContactRecordList(userId);
                    returnMap.put('contactInfo', conRec);
                }
                else if(action == 'deleteStoredPayment') {
                		HMR_StoredPayment_Service storedPaymentService = new HMR_StoredPayment_Service(userId);
                		String storedPaymentId = params.get('storedPaymentId');

                		try {
                			Map<String, Object> result = storedPaymentService.deleteStoredPayment(storedPaymentId);
                			returnMap.put('result', result);
                			List<HMR_StoredPayment_Service.StoredPaymentRecord> storedPaymentList = storedPaymentService.getByOwnerId();
                    		returnMap.put('storedPayments', storedPaymentList);
                		} catch(DmlException e) {
                			returnMap.put('success', false);
                			returnMap.put('message', e.getDMLMessage(0));
                			returnMap.put('showMessage', true);
                		} catch(Exception e) {
                			String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                			returnMap.put('success',false);
                			returnMap.put('message',JSON.serialize(message));
                			returnMap.put('showMessage', true);
                		}
                }
                else{
                    //Invalid Action
                    returnMap.put('success',false);
                    returnMap.put('message','Invalid Action');
                }
            }
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'user and account id parameters required');
        }

        return JSON.serialize(returnMap);
    }

    public static Type getType() {
        return HMR_CMS_CCRZ_MyAccount_ServiceInterface.class;
    }
}