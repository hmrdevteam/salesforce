/**
* Apex Content Template Controller for Login Page login section
*
* @Date: 2017-03-18
* @Author Pranay Mistry (Magnet 360)
* @Modified: Zach Engman (Magnet 360) - 2017-06-12
* @JIRA:
*/
global virtual with sharing class HMR_CMS_LoginPage_ContentTemplate extends cms.ContentTemplateController{
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_LoginPage_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2. no ARG constructor
    global HMR_CMS_LoginPage_ContentTemplate() {
        super();
    }

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup
    	String html = '';
        string currentCartId = '';
        string startUrl = '../DefaultStore/welcome';
        boolean hasKitCart = false;

        if(ApexPages.currentPage().getHeaders().get('Referer') != null)
        	startUrl = ApexPages.currentPage().getHeaders().get('Referer');
        if(ApexPages.currentPage().getParameters().get('hasKitCart') != null){
            hasKitCart = Boolean.valueOf(ApexPages.currentPage().getParameters().get('hasKitCart'));
            startUrl = 'checkout';
        }
        if(ApexPages.currentPage().getCookies().get('currCartId') != null)
            currentCartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
        else if(ApexPages.currentPage().getParameters().get('cartID') != null)
        	currentCartId = ApexPages.currentPage().getParameters().get('cartID');


    	html += '<div class="row form-login">' +
    	'<div class="col-sm-4">' +
		                '<div class="new-to-hmr">' +
		                    'New?' +
		                '</div>' +
		                '<div class="join-hmr">' +
		                    'Join the <div>HMR</div> <div>Community!</div>' +
		                '</div>' +
		                '<div>' +
		                	(!String.isBlank(currentCartId) ?
		                		String.format('<div class="btn-container"><a id="create-account-now-btn" data-startUrl="{0}" data-cartId="{1}" class="btn btn-primary hmr-btn-blue hmr-btn-small">Create Account Now</a></div>', new String[]{startUrl, currentCartId}) :
		                		String.format('<div class="btn-container"><a id="create-account-now-btn" data-startUrl="{0}" class="btn btn-primary hmr-btn-blue hmr-btn-small">Create Account Now</a></div>', new String[]{startUrl})) +
		                '</div>';
            			if(!hasKitCart){
							html += '<div class="link-container continue-guest-wrapper">' +
										'<div class="hmr-allcaps-container">' +
											String.format('<a id="continue-as-guest" data-cartId="{0}" class="hmr-allcaps hmr-allcaps-blue">Continue as Guest</a>', new String[]{currentCartId}) +
											String.format('<a id="continue-as-guest-carat" data-cartId="{0}" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>', new String[]{currentCartId}) +
										'</div>' +
									'</div>';
    					}
		            html += '</div>' +
		            '<div class="col-sm-8">' +
		                '<form id="hmr_loginform">' +
		                    '<h2>Sign In</h2>' +
		                    '<div class="col-xs-12 form-error" id="loginErrorDiv">'+
	                            '<label id="hmr_reg_inputLogin-error" class="error" style="display: none"></label>'+
	                        '</div>' +
		                    '<label for="hmr_signin_inputEmail">Email address</label>' +
		                    '<input type="email" id="hmr_signin_inputEmail" name="l_email" class="form-control" autofocus="">' +
		                    '<label for="inputPassword">Password' +
		                        '<span></span>' +
		                    '</label>' +
		                    '<div class="input-group">' +
		                        '<input type="password" id="hmr_signin_password" name="l_password" class="form-control cbox-shadow">' +
		                        '<div class="input-group-addon">' +
		                            '<div class="eyeclosed"></div>' +
		                        '</div>' +
		                    '</div>' +
		                    '<div class="submit-wrapper">' +
								'<div class="btn-container">' +
									'<button id="hmr_signin_btn" class="btn btn-primary hmr-btn-blue hmr-btn-small">Sign In</button>' +
								'</div>' +
								'<div class="link-container">' +
									'<div class="hmr-allcaps-container">' +
										'<a href="ForgotPassword" class="hmr-allcaps hmr-allcaps-blue">Forgot your password?</a>' +
										'<a href="ForgotPassword" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>' +
									'</div>' +
								'</div>' +
									'<div class="hmr-support-login">' +
										'If you need any customer support, please' +
										'<br/>'+
										 'call us at 800-418-1367 Monday-Friday, 9am-6pm EST'+
									'</div>'+
		                    '</div>' +
            			'</form>' +
		            '</div>' +
       			'</div>';

        return html;
    }
}