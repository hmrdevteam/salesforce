/**
* Apex Rest Resource for Activity History
* @Date: 2018-01-12
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-02-12 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/activity/history/*')  
global with sharing class HMRMobActivityHistory {
    /**
     * method to get the activity history records for the range specified
     * @param contactId - the related contact record id 
     * @param programMembershipId - the related program membership record id
     * @param activityDateFrom - the starting date to pull the activity history for
     * @param activityDateTo - the ending date to pull activity history for
     * @return MobActivityHistoryDTO - the DTO containing the activity history for the contact for the date range (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if error is encountered
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        Map<String, String> paramMap = RestContext.request.params;
        
        if(paramMap.get('contactId') != null && paramMap.get('activityDateFrom') != null && paramMap.get('activityDateTo') != null){
            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
            String contactId = paramMap.get('contactId');
            Date activityDateFrom = Date.valueOf(paramMap.get('activityDateFrom'));
            Date activityDateTo = Date.valueOf(paramMap.get('activityDateTo'));
            Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(contactId, activityDateFrom);
            
            if(programMembershipRecord == null){
               programMembershipRecord = programMembershipService.getProgramMembership(contactId, activityDateTo);
            }

            if(programMembershipRecord != null){
              List<MobActivityHistoryDTO> activityHistoryDTOList = new List<MobActivityHistoryDTO>();

              List<ActivityLog__c> activityLogList = [SELECT Contact__c
                                                            ,Program_Membership__c
                                                            ,Program_Membership__r.Program__r.Name
                                                            ,DateDataEnteredFor__c
                                                            ,CurrentWeight__c
                                                            ,Diet_Type__c
                                                            ,Diet_Version__c
                                                            ,Entrees__c
                                                            ,InTheBox__c
                                                            ,ShakesCereals__c
                                                            ,FruitsVegetables__c
                                                            ,TotalPhysicalActivityCaloriesRollUp__c
                                                            ,Water__c
                                                            ,Weight_Version__c
                                                            ,BenefitBars__c                                                      		
                                                            ,(SELECT Activity_Log__r.Contact__c
                                                                    ,Activity_Log__r.Program_Membership__c
                                                                    ,Activity_Log__r.DateDataEnteredFor__c
                                                                    ,NameofPhysicalActivity__c
                                                                    ,TotalPhysicalActivityCalories__c
                                                              		  ,Intensity_Level__c
                                                              		  ,Minutes__c  
                                                                    ,Physical_Activity_Version__c
                                                              FROM Physical_Activities__r)
                                                       FROM ActivityLog__c 
                                                       WHERE Program_Membership__c =: programMembershipRecord.Id
                                                        AND DateDataEnteredFor__c >=: activityDateFrom
                                                        AND DateDataEnteredFor__c <=: activityDateTo];

              RestContext.response.statuscode = 200;
                                                       
              if(activityLogList.size() > 0){
                  for(ActivityLog__c activityLogRecord : activityLogList){
                      activityHistoryDTOList.add(new MobActivityHistoryDTO(activityLogRecord)); 
                  }
              }

              String startDay;
                                                      
              if(programMembershipRecord != null){
              
                   startDay = programMembershipRecord.hmr_Day_of_Week__c;
                   
                   if(String.isBlank(startDay) && programMembershipRecord.hmr_Diet_Start_Date__c != null){
                      DateTime startDateTime = DateTime.newInstance(programMembershipRecord.hmr_Diet_Start_Date__c , Time.newInstance(0,0,0,0));
                      startDay = startDateTime.format('EEEE');
                   }
              }

              returnMap.put('success', true);
              returnMap.put('startDay', startDay);
              returnMap.put('programMembership', new MobProgramMembershipHistoryDTO(programMembershipRecord));
              returnMap.put('activityHistoryLog', activityHistoryDTOList);
          }
          else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'No program membership for the range');
          }
        }
        else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'Parameters: contactId and activityDateFrom and activityDateTo are required');
        }
        
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  

    }

    /**
     * global DTO class that represents a historical structure of the
     * activity and related physical activity records for a date range
     */
    global class MobActivityHistoryDTO {
        public Date activityDate {get; set;}
        public HMRMobDietActivity.MobDietActivityDTO dietActivityLog {get; set;}
        public HMRMobWeightActivity.MobWeightActivityDTO weightActivityLog {get; set;}
        public List<HMRMobPhysicalActivity.MobPhysicalActivityDTO> physicalActivityLog {get; set;}

        /**
         * constructor
        */
        public MobActivityHistoryDTO(ActivityLog__c activityLogRecord){
            this.activityDate = activityLogRecord.DateDataEnteredFor__c;
            this.dietActivityLog = new HMRMobDietActivity.MobDietActivityDTO(activityLogRecord);
            this.weightActivityLog = new HMRMobWeightActivity.MobWeightActivityDTO(activityLogRecord);
            this.physicalActivityLog = new List<HMRMobPhysicalActivity.MobPhysicalActivityDTO>();

            if(activityLogRecord.Physical_Activities__r.size() > 0){
                for(PhysicalActivity__c physicalActivityRecord : activityLogRecord.Physical_Activities__r){
                    physicalActivityLog.add(new HMRMobPhysicalActivity.MobPhysicalActivityDTO(physicalActivityRecord));
                }
            }
        }
    }

    /**
     * global DTO class that represents a historical structure of the program membership data
    */
    global class MobProgramMembershipHistoryDTO {
      public string programName {get; set;}
      public string dietType {get; set;}

      /**
       * constructor
      */
      public MobProgramMembershipHistoryDTO(Program_Membership__c programMembershipRecord){
        this.dietType = programMembershipRecord.DietType__c;
        this.programName = programMembershipRecord.Program__r.Name;
      }
    }
}