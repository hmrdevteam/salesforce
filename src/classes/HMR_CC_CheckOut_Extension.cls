/**
* CloudCraze Extension for Checkout, gets called from CC Page Include for CheckOut Page
*
* @Date: 2017-03-27
* @Author Pranay Mistry (Magnet 360)
* @Modified: Nathan Anderson (Magnet 360)
* @JIRA:
*/
global with sharing class HMR_CC_CheckOut_Extension {
    public String  curAddrString{ get; set; }
    public Boolean isCybersourceReponse { get; set; }
    public String portalUser { get; set; }
    public String cartId { get; set; }
    public String cybersourceResponse { get; set; }
    public Map<String, String> currentPageParams { get; set; }


    public HMR_CC_CheckOut_Extension() {
        currentPageParams = ApexPages.currentPage().getParameters();

        if(currentPageParams.get('auth_code') != null && currentPageParams.get('decision') != '') {
            isCybersourceReponse = true;
            portalUser = currentPageParams.get(cc_pgcs_ApiConstants.CS_RES_REQMERCHANT_SECURE_DATA1);
            cartId = currentPageParams.get(cc_pgcs_ApiConstants.CS_RES_REQMERCHANT_SECURE_DATA2);
            cybersourceResponse = JSON.serialize(currentPageParams);
        }

        String portalUserId = ApexPages.currentPage().getParameters().get('portalUser');
        if((!String.isBlank(portalUserId))){
            User pu = [SELECT ContactId, Contact.AccountId FROM User WHERE ID = :portalUserId];
            if(pu.ContactId != null){
              Id psAccountId = pu.Contact.AccountId;
              list<Contact> conIdSet = [SELECT (SELECT Id FROM Contacts) FROM Account WHERE Id = : psAccountId].Contacts;
              set<string> ids = new set<string>();
              for(Contact c: conIdSet){
                  string cid = c.Id;

                  if(cid.length()>15)
                      cid = cid.subString(0,15);

                  ids.add(cid);
              }

              List<ccrz__E_ContactAddr__c> extAddrListCon = [SELECT Id FROM ccrz__E_ContactAddr__c WHERE Contact_Id__c IN : ids AND OwnerId !=: portalUserId];
              curAddrString = JSON.serialize(extAddrListCon);
            }
        }else{
          curAddrString = JSON.serialize(new List<ccrz__E_ContactAddr__c>());
        }
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateCartForP2(final ccrz.cc_RemoteActionContext ctx, String cartEncrypteId, Date P2ProcessDateObj){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.data = null;
        res.success = false;
        try {
            if(!String.isBlank(cartEncrypteId)) {
                ccrz__E_Cart__c cartToUpdate = [SELECT ccrz__EncryptedId__c, P2_1st_Order_Date__c, P2_1st_Order_Delay__c
                                                FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :cartEncrypteId];
                if(cartToUpdate != null && P2ProcessDateObj != null) {
                    cartToUpdate.P2_1st_Order_Date__c = P2ProcessDateObj;
                    cartToUpdate.P2_1st_Order_Delay__c = true;
                    update cartToUpdate;
                    res.success = true;
                    res.data = cartToUpdate;
                }
            }
        }
        catch(Exception ex) {
            res.data = ex.getMessage();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult checkOut(final ccrz.cc_RemoteActionContext ctx, List<String> CC_Term_Id_List, Id contactId, Boolean isCSRFlow){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = true;
        try {
            List<Consent_Agreement__c> clientCAList = new List<Consent_Agreement__c>();

            //if CC Term Ids are passed in from Checkout process, loop through Ids and set up Consent Agreement records
            if(!CC_Term_Id_List.isEmpty() && contactId != null) {
                for(String CC_Term_Id: CC_Term_Id_List) {
                    Consent_Agreement__c clientCA = new Consent_Agreement__c(
                        Contact__c = contactId,
                        Version__c = CC_Term_Id
                    );

                    //if in CSR context, set the CA Status to Notified
                    if(isCSRFlow) {
                        clientCA.Status__c = 'Notified';
                    }
                    //if not CSR, set status to consented
                    else {
                        clientCA.Agreement_Timestamp__c = DateTime.now();
                        clientCA.Status__c = 'Consented';
                    }
                    clientCAList.add(clientCA);
                }
            }


            //insert new Consent Agreement records
            if(!clientCAList.isEmpty()) {
                insert clientCAList;
            }

            res.data = clientCAList;
        }
        catch (Exception err){
            res.success = false;
            res.data = err.getMessage();
        }

        return null;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult createPaymentTokenForP2(ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> responseMap = new Map<String, Object>();
        try {
            HMR_Cybersource_Utility.createPaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, unSigned_Field_Values);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);//postString
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            Map<String, Object> billingAddressMap = HMR_CC_CheckOut_Extension.addBillingAddress(ccrz.cc_CallContext.currUserId, ccrz.cc_CallContext.currAccountId, userAddressMapArg);
            if(billingAddressMap.size() > 0 && billingAddressMap.containsKey('contactAddress'))
                responseMap.put('billingAddress', billingAddressMap.get('contactAddress'));
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in CreatePaymentToken from Checkout Page---'+e.getMessage());
            system.debug('Exception line Number---'+e.getStackTraceString());
            responseMap.put('exception', e);
            res.data = responseMap;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getStoredPaymentDetails(ccrz.cc_RemoteActionContext ctx, String storedPaymentId) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        Map<String, Object> responseMap = new Map<String, Object>();
        try {
            ccrz__E_StoredPayment__c storedPayment =
                [SELECT Id, ccrz__AccountNumber__c, ccrz__AccountType__c, ccrz__ExpMonth__c, ccrz__ExpYear__c, ccrz__PaymentType__c,
                            ccrz__Token__c FROM ccrz__E_StoredPayment__c WHERE Id = :storedPaymentId];
            responseMap.put('storedPayment', storedPayment);
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            system.debug('Exception in getStoredPaymentDetails from Checkout Page---'+e.getMessage());
            system.debug('Exception line Number---'+e.getStackTraceString());
            responseMap.put('exception', e);
            res.data = responseMap;
            res.success = false;
        }
        finally {}
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCCTerms(final ccrz.cc_RemoteActionContext ctx, Id contactId, List<String> cartProductIds){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = true;

        if(contactId != null) {
            Contact c = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];

            Boolean contactIsHealthConsented = false;
            Boolean contactIsADConsented = false;

            List<Consent_Agreement__c> contactCAs = new List<Consent_Agreement__c>(
                [SELECT Id, Version__c, Status__c, Consent_Type__c, Contact__c FROM Consent_Agreement__c
                 WHERE Contact__c = :c.Id AND Status__c = 'Consented']);

            for(Consent_Agreement__c contactCA: contactCAs) {
                if(contactCA.Consent_Type__c == 'Health') {
                    contactIsHealthConsented = true;
                }
                if(contactCA.Consent_Type__c == 'Auto-Delivery') {
                    contactIsADConsented = true;
                }
            }

            //Map of Ids and Products from the argument passed into the method
            Map<Id, ccrz__E_Product__c> cc_CartProducts = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, Program__c FROM ccrz__E_Product__c WHERE Id IN :cartProductIds] );

            //empty set to hold Program Ids of all Kits in the cart.
            //This will be used in more depth in R2 to split out MDC orders that should only see AD and NOT Health terms
            Set<Id> kitIds = new Set<Id>();
            //variable that we'll set if there is a valid kit in the cart
            Boolean kitOrder = false;

            //Loop through the Products in the cart to find Kits related to a program, and add them to the set
            for(ccrz__E_Product__c cartProd: cc_CartProducts.values()) {
                if(cartProd.Program__c != null) {
                    kitIds.add(cartProd.Program__c);
                }
            }

            //if the set is not empty, set flag to true
            if(kitIds.size() > 0) {
                kitOrder = true;
            }

            //empty list of ccTerms to fill in according to conditions below
            List<ccrz__E_Term__c> ccTermsList = new List<ccrz__E_Term__c>();

            try {
                //Get the full list of terms to parse through individually
                List<ccrz__E_Term__c> fullTermsList = new List<ccrz__E_Term__c>(
                    [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                     FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE]);

                //Get standard Health Acknowledgement record for CC Term
                ccrz__E_Term__c standardHealthTerm =
                    [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                     FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE AND Consent_Type__c = 'Health' AND Employer_Account__c = null];

                //if cart contains a Kit Order, add Health and AD terms to the list
                if(kitOrder) {
                    //If the Contact is associated to an account that has a specific Health Term, add it to the list
                    for(ccrz__E_Term__c term : fullTermsList) {
                        if(term.Consent_Type__c == 'Health' && term.Employer_Account__c == c.AccountId && !contactIsHealthConsented) {
                            ccTermsList.add(term);
                        }
                    }
                    //if no Employer specific Health Term was added to the list, add the standard Health Term
                    if(ccTermsList.isEmpty() && !contactIsHealthConsented) {
                        ccTermsList.add(standardHealthTerm);
                    }
                    //add AD terms to the list
                    for(ccrz__E_Term__c term : fullTermsList) {
                        if(term.Consent_Type__c == 'Auto-Delivery' && !contactIsADConsented) {
                            ccTermsList.add(term);
                        }
                    }
                }
                //Add Money Back Guarantee to List for all orders
                for(ccrz__E_Term__c term : fullTermsList) {
                    if(term.Consent_Type__c == 'Shipping') {
                        ccTermsList.add(term);
                    }
                }
                res.data = ccTermsList;
                return res;
            }
            catch (Exception err){
                res.success = false;
                res.data = err.getMessage();
            }
        }
        else {
            if(contactId == null) {
                try {
                    //Get the full list of terms to parse through individually
                    List<ccrz__E_Term__c> ccTermsListGuest = new List<ccrz__E_Term__c>(
                        [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                         FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE AND Consent_Type__c = 'Shipping']);

                    res.data = ccTermsListGuest;
                    return res;
                }
                catch (Exception err){
                    res.success = false;
                    res.data = err.getMessage();
                }
            }
        }

        return null;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult authorizePayment(final ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            HMR_Cybersource_Utility.authorizePayment(ccrz.cc_CallContext.currUserId, userAddressMapArg, unSigned_Field_Values);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            Map<String, Object> responseMap = new Map<String, Object>();
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);//postString
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult authorizePaymentAndCreatePaymentToken(final ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, Map<String, String> unSigned_Field_Values) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            HMR_Cybersource_Utility.authorizePaymentAndCreatePaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, unSigned_Field_Values);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            Map<String, Object> responseMap = new Map<String, Object>();
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);//postString
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            Map<String, Object> billingAddressMap = HMR_CC_CheckOut_Extension.addBillingAddress(ccrz.cc_CallContext.currUserId, ccrz.cc_CallContext.currAccountId, userAddressMapArg);
            if(billingAddressMap.size() > 0 && billingAddressMap.containsKey('contactAddress'))
                responseMap.put('billingAddress', billingAddressMap.get('contactAddress'));
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    public static Map<String, Object> addBillingAddress(String userId, String accountId, Map<String, String> billingAddressMapArg) {
        Map<String, Object> outputData = new Map<String, Object>();
        List<User> userRecordList = [SELECT ContactId FROM User WHERE Id =: userId];

        if(userRecordList.size() > 0 && !String.isBlank(userRecordList[0].ContactId)){
            String contactId = userRecordList[0].ContactId;
            Contact contact = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];

            if(contact != null && !billingAddressMapArg.isEmpty()) {
                ccrz__E_ContactAddr__c contactAddressToInsert = new ccrz__E_ContactAddr__c();
                contactAddressToInsert.ccrz__FirstName__c = billingAddressMapArg.get('bill_to_forename');
                contactAddressToInsert.ccrz__LastName__c = billingAddressMapArg.get('bill_to_surname');
                contactAddressToInsert.ccrz__AddressFirstline__c = billingAddressMapArg.get('bill_to_address_line1');
                contactAddressToInsert.ccrz__AddressSecondline__c = billingAddressMapArg.get('bill_to_address_line2');
                contactAddressToInsert.ccrz__City__c = billingAddressMapArg.get('bill_to_address_city');
                contactAddressToInsert.ccrz__State__c = billingAddressMapArg.get('bill_to_address_state');
                contactAddressToInsert.ccrz__StateISOCode__c = billingAddressMapArg.get('bill_to_address_state_code');
                contactAddressToInsert.ccrz__PostalCode__c = billingAddressMapArg.get('bill_to_address_postal_code');
                contactAddressToInsert.ccrz__Country__c = 'United States';
                contactAddressToInsert.ccrz__CountryISOCode__c = 'US';
                contactAddressToInsert.OwnerId = userId;
                //Check for Duplicate
                List<ccrz__E_ContactAddr__c> existingAddressList = [SELECT ccrz__AddressFirstline__c,
                                                                           ccrz__AddressSecondline__c,
                                                                           ccrz__City__c,
                                                                           ccrz__State__c,
                                                                           ccrz__StateISOCode__c,
                                                                           ccrz__PostalCode__c,
                                                                           ccrz__Country__c,
                                                                           ccrz__CountryISOCode__c
                                                                    FROM ccrz__E_ContactAddr__c
                                                                    WHERE OwnerId =: userId
                                                                      AND ccrz__FirstName__c =: contactAddressToInsert.ccrz__FirstName__c
                                                                      AND ccrz__LastName__c =: contactAddressToInsert.ccrz__LastName__c
                                                                      AND ccrz__AddressFirstline__c =: contactAddressToInsert.ccrz__AddressFirstline__c
                                                                      AND ccrz__City__c =: contactAddressToInsert.ccrz__City__c
                                                                      AND ccrz__PostalCode__c =: contactAddressToInsert.ccrz__PostalCode__c];
                if(existingAddressList.size() > 0)
                  contactAddressToInsert.Id = existingAddressList[0].Id;
                else{
                  insert contactAddressToInsert;
                  ccrz__E_AccountAddressBook__c contactAccountAddressBookToInsert = new ccrz__E_AccountAddressBook__c();
                  contactAccountAddressBookToInsert.ccrz__E_ContactAddress__c = contactAddressToInsert.Id;
                  contactAccountAddressBookToInsert.ccrz__AddressType__c = 'Billing';
                  contactAccountAddressBookToInsert.ccrz__Default__c = true;
                  contactAccountAddressBookToInsert.ccrz__AccountId__c = accountId;
                  contactAccountAddressBookToInsert.ccrz__Account__c = accountId;
                  contactAccountAddressBookToInsert.OwnerId = userId;
                  insert contactAccountAddressBookToInsert;
                }

                outputData.put('contactAddress', contactAddressToInsert);
            }
        }

        return outputData;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult authorizeUsingPaymentToken(final ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, string paymentToken) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            HMR_Cybersource_Utility.authorizeUsingPaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, paymentToken);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            Map<String, Object> responseMap = new Map<String, Object>();
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult authorizeUsingBillingAddressAndPaymentToken(final ccrz.cc_RemoteActionContext ctx, Map<String, String> userAddressMapArg, string paymentToken) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            HMR_Cybersource_Utility.authorizeUsingPaymentToken(ccrz.cc_CallContext.currUserId, userAddressMapArg, paymentToken);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            Map<String, Object> responseMap = new Map<String, Object>();
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();
            for(String postStringItem: postStringArray) {
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            }
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);
            Http http = new Http();
            HTTPResponse response = http.send(req);
            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', ccrz.cc_CallContext.currUserId);
            res.data = responseMap;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult createStoredPayment(final ccrz.cc_RemoteActionContext ctx, Map<String, String> paymentInformationMap) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            Map<String, Object> outputData = new Map<String, Object>();
            if(!paymentInformationMap.isEmpty()) {
                if((paymentInformationMap.get('contactAddressId') != null || paymentInformationMap.get('contactAddressId') != '') &&
                   (paymentInformationMap.get('contactFirstName') != null || paymentInformationMap.get('contactFirstName') != '' ||
                    paymentInformationMap.get('contactLastName') != null || paymentInformationMap.get('contactLastName') != '')) {
                        ccrz__E_ContactAddr__c contactAddressToUpdate =
                            [SELECT Id, ccrz__FirstName__c, ccrz__LastName__c FROM ccrz__E_ContactAddr__c
                             WHERE Id = :paymentInformationMap.get('contactAddressId')];
                        if(paymentInformationMap.get('contactFirstName') != null && paymentInformationMap.get('contactFirstName') != '') {
                            contactAddressToUpdate.ccrz__FirstName__c = paymentInformationMap.get('contactFirstName');
                        }
                        if(paymentInformationMap.get('contactLastName') != null && paymentInformationMap.get('contactLastName') != '') {
                            contactAddressToUpdate.ccrz__LastName__c = paymentInformationMap.get('contactLastName');
                        }
                        update contactAddressToUpdate;
                        outputData.put('contactAddressToUpdate', contactAddressToUpdate);
                    }

                ccrz__E_StoredPayment__c storedPaymentToInsert = new ccrz__E_StoredPayment__c(
                    ccrz__Account__c = ccrz.cc_CallContext.currAccountId,
                    ccrz__User__c = ccrz.cc_CallContext.currUserId,
                    ccrz__Storefront__c = ccrz.cc_CallContext.storefront,
                    ccrz__DisplayName__c = paymentInformationMap.get('displayName'),
                    ccrz__AccountNumber__c = paymentInformationMap.get('accountNumber'),
                    ccrz__AccountType__c = 'cc',
                    HMR_Contact_Address__c = paymentInformationMap.get('contactAddressId'),
                    ccrz__Token__c = paymentInformationMap.get('token'),
                    ccrz__ExpMonth__c = Decimal.valueOf(paymentInformationMap.get('expirationMonth')),
                    ccrz__ExpYear__c = Decimal.valueOf(paymentInformationMap.get('expirationYear')),
                    ccrz__PaymentType__c = paymentInformationMap.get('paymentType'),
                    ccrz__Enabled__c = true,
                    OwnerId = ccrz.cc_CallContext.currUserId
                );

                insert storedPaymentToInsert;

                outputData.put('storedPayment', storedPaymentToInsert);

                res.data = outputData;
                res.success = true;
            }
            else {
                res.data = null;
            }
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getStoredPaymentBillingAddressRecordByToken(final ccrz.cc_RemoteActionContext ctx, string token) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            ccrz__E_StoredPayment__c storedPaymentRecord = HMR_CC_StoredPayment_Selector.getByToken(token);
            res.data = storedPaymentRecord.HMR_Contact_Address__r;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getStoredPaymentRecords(final ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            List<ccrz__E_StoredPayment__c> storedPaymentList = HMR_CC_StoredPayment_Selector.getByUserId(ccrz.cc_CallContext.currUserId);
            res.data = storedPaymentList;
            res.success = true;
        }
        catch(Exception e) {
            res.data = e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString();
        }

        return res;
    }

}