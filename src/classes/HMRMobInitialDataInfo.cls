/**
* Apex Rest Service Class for Gathering the Initial Data Info for the Mobile App
* @Date: 2018-01-02
* @Author: Magnet 360 - Javier Arroyo and Zach Engman
* @Modified: 2018-03-17
* @JIRA:
*/
@RestResource(urlMapping='/mobile/initialDataInfo/*')
global class HMRMobInitialDataInfo {
    private static final string healthySolutionsProgram = 'P1 Healthy Solutions';
    private static final integer MaxJourneyDays = 14;
    
    @HttpGet
    global static InitialDataInfoResponse getInitialDataInfo() {
        String communityUserId = RestContext.request.params.get('communityUserId');
        String imageResolution = RestContext.request.params.get('imageResolution');

        if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
        }
        
        HMR_User_Selector userSelector = new HMR_User_Selector();
        User userRecord = userSelector.getById(communityUserId);
        InitialDataInfoResponse response = new InitialDataInfoResponse(userRecord);
        HMR_Goal_Service goalService = new HMR_Goal_Service(userRecord.ContactId);

		response.userProfile = HMRMobProfile.getProfile(userRecord.Contact.Community_User_ID__c); 
        //retrieve the weekly goal for the current date (or null if not set)
        response.weeklyGoal = HMRMobGoal.getGoal(userRecord.Contact.Community_User_ID__c, Date.today());
        response.goalOptions = HMR_Goal_Service.getGoalSetting(response.userProfile.programName, response.userProfile.dietType);

        response.dailyTipContent = getDailyTipContent(userRecord, response, imageResolution);
        
        //get the statistics based on the weekly goal
        if(!String.isBlank(response.userProfile.programMembershipId) && response.weeklyGoal != null){
            response.goalStatistics = goalService.getGoalStatistics(response.userProfile.programMembershipId, response.weeklyGoal.focusArea);
        }
        //the focus area was moved to separate goals object, but mobile app is sometimes referencing this yet so kept here for safety
        if(response.weeklyGoal != null && !String.isBlank(response.weeklyGoal.focusArea)){
            response.userProfile.focusArea = response.weeklyGoal.focusArea;
        }

        return response;
    }
    
    global static HMR_CMS_ApiService.DailyTipContent getDailyTipContent(User userRecord, InitialDataInfoResponse response, string imageResolution) {
        String taxonomyPath = '';
        Integer journeyDay = 0;
        HMR_CMS_ApiService cmsApiService = new HMR_CMS_ApiService();
        HMR_CMS_ApiService.DailyTipContent dailyTipContent = new HMR_CMS_ApiService.DailyTipContent();
        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Date dietStartDate = programMembershipService.getDietStartDate(userRecord.ContactId);

        //Determine how many days until diet start date
        if(dietStartDate != null){
            journeyDay = dietStartDate.daysBetween(Date.today()) + 1;
        }

        //Only healthy solutions program members receive quick start for their first 14 days
        if(response.userProfile.programName == healthySolutionsProgram && journeyDay > 0 && journeyDay <= MaxJourneyDays){
            taxonomyPath = '/Daily Tips/Quick Start/Day ' + String.valueOf(journeyDay);
        }
        else{
            // We need to check which day the client is in for their diet
            string startDay = response.userProfile.dayOfWeek;
            integer dayCount = 1;

            if(String.isBlank(startDay) && response.userProfile.dietStartDate != null){
                DateTime startDateTime = DateTime.newInstance(response.userProfile.dietStartDate, Time.newInstance(0,0,0,0));
                startDay = startDateTime.format('EEEE');
            }

            if(!String.isBlank(startDay)){
                // If start day is set, which it always should be, enter loop
                DateTime todayDateTime = DateTime.newInstance(Date.today(), Time.newInstance(0,0,0,0));
                
                while(todayDateTime.format('EEEE') != startDay && dayCount <= 7){
                    dayCount++;
                    todayDateTime = todayDateTime.addDays(-1);
                }
            }

            if(dayCount == 1){
                taxonomyPath = '/Daily Tips/Coaching Tips/Day 1';
            }
            else if(dayCount == 2){
                if(response.weeklyGoal != null && !String.isBlank(response.weeklyGoal.focusArea)){
                    taxonomyPath = '/Daily Tips/Coaching Tips/Challenge Card/' + response.weeklyGoal.focusArea;
                }
                else{ //Get random focus area card
                    taxonomyPath = '/Daily Tips/Coaching Tips/Challenge Card';
                }
            }
            else{// Check for decision free
                if(!String.isBlank(response.userProfile.dietType) && response.userProfile.dietType.toLowerCase().contains('decision free')){
                    taxonomyPath = '/Daily Tips/Coaching Tips/Daily/Decision Free'; 
                }
                else{
                    taxonomyPath = '/Daily Tips/Coaching Tips/Daily/All';
                }
            }
        }

        if(!String.isBlank(taxonomyPath)){
            try{
                dailyTipContent = cmsApiService.getDailyTipsContentByTaxonomyTag(taxonomyPath);
                dailyTipContent.backgroundImageUrl = getResolvedImagePath(imageResolution, dailyTipContent.backgroundImageUrl);
            }
            catch(Exception ex){
                System.Debug(ex.getMessage() + taxonomyPath);
            }
        }

        return dailyTipContent;
    }

    @testVisible
    private static string getResolvedImagePath(string imageResolution, string image){
        //Default image for now if it is blank to the one image we have setup on the server
        if(String.isBlank(image)){
            image = 'Day1Hero.jpg';
        } 

        if(String.isBlank(imageResolution)){
            imageResolution = '';
        }
        
        string urlBasePath = 'https://www.myhmrprogram.com/ContentMedia/AppPhotos/'; //Move to setting
        string resolvedImagePath;

        imageResolution = imageResolution.toLowerCase();

        if(imageResolution == 'hdpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-hdpi/' + image;
        }
        else if(imageResolution == 'ldpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-ldpi/' + image;
        }
        else if(imageResolution == 'mdpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-mdpi/' + image;
        }
        else if(imageResolution == 'xhdpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-xhdpi/' + image;
        }
        else if(imageResolution == 'xxhdpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-xxhdpi/' + image;
        }
        else if(imageResolution == 'xxxhdpi'){
            resolvedImagePath = urlBasePath + 'Android/drawable-xxxhdpi/' + image;
        }
        else if(imageResolution == 'x'){
            resolvedImagePath = urlBasePath + 'IOS/iPhone X/' + image;
        }
        else if(imageResolution == 'x@2x'){
            resolvedImagePath = urlBasePath + 'IOS/iPhone X/' + image.replace('.', '@2x.');
        }
        else if(imageResolution == 'x@3x'){
            resolvedImagePath = urlBasePath + 'IOS/iPhone X/' + image.replace('.', '@3x.');
        }
        else if(imageResolution == '@2x'){
            resolvedImagePath = urlBasePath + 'IOS/' + image.replace('.', '@2x.');
        }
        else if(imageResolution == '@3x'){
            resolvedImagePath = urlBasePath + 'IOS/' + image.replace('.', '@3x.');
        }
        else{
            resolvedImagePath = urlBasePath + 'IOS/' + image;
        }

        return resolvedImagePath;
    }

    global class InitialDataInfoResponse {
        
        public InitialDataInfoResponse(User u) {
            this.user = u;
        }
        
        public User user {get; set;}
        public HMRMobProfile.MobProfileDTO userProfile {get; set;}
        public HMR_CMS_ApiService.DailyTipContent dailyTipContent {get; set;}
        public HMRMobGoal.MobGoalDTO weeklyGoal {get; set;}
        public HMR_Goal_Service.GoalSetting goalOptions {get; set;}
        public HMR_Goal_Service.GoalStatistics goalStatistics {get; set;}
    }
}