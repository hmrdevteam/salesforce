/*
Developer: Utkarsh Goswami (Mindtree)
Date: June 07th, 2017
Modified: 
Modified Date : March 27th 2017
Target Class: HMR_CMS_PageSections_ContentTemplate
*/

@isTest
public class HMR_CMS_PageSec_ContentTmpl_Test{  

        
    @isTest static void test_general_method() { 

                
                HMR_CMS_PageSections_ContentTemplate pageSection = new HMR_CMS_PageSections_ContentTemplate();
                String propertyValue = pageSection.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                String SectionTitle = pageSection.SectionTitle;                
                String SectionDescription = pageSection.SectionDescription;
                String PickLinkOrButton = pageSection.PickLinkOrButton ;
                String LinkOrButtonURLText = pageSection.LinkOrButtonURLText;                
                String LinkColorBlueOrWhite = pageSection.LinkColorBlueOrWhite;
                String FontColorBlackOrWhite = pageSection.FontColorBlackOrWhite;
                String CSSClassSelector = pageSection.CSSClassSelector;                
                Boolean isLinkOrButtonForFoodPage = pageSection.isLinkOrButtonForFoodPage;
                String SectionGTMID = pageSection.SectionGTMID;
                
                
                ccrz__E_Category__c category = new ccrz__E_Category__c(Name = 'HMR Products', ccrz__CategoryID__c = 'test12', ccrz__Sequence__c = 500, ccrz__StartDate__c =
                                                                    date.today(), ccrz__EndDate__c = DAte.today().addDays(1));
                                                                    
                insert category;
                
                pageSection.testAttributes = new Map<String, String>  {
                    'SectionTitle' => 'title',
                    'SectionDescription' => 'desc',
                    'PickLinkOrButton' => 'Button',
                    'LinkOrButtonURLText' => 'other',
                    'LinkColorBlueOrWhite' => 'blue',
                    'FontColorBlackOrWhite' => 'white',
                    'CSSClassSelector' => 'show',
                    'isLinkOrButtonForFoodPage' => 'TRUE',
                    'SectionGTMID' => 'test'
                };
                
                String renderHMTL2 = pageSection.getHTML();
                
                pageSection.testAttributes = new Map<String, String>  {
                    'SectionTitle' => 'title',
                    'SectionDescription' => 'desc',
                    'PickLinkOrButton' => 'Link',
                    'LinkOrButtonURLText' => 'other',
                    'LinkColorBlueOrWhite' => 'blue',
                    'FontColorBlackOrWhite' => 'white',
                    'CSSClassSelector' => 'show',
                    'isLinkOrButtonForFoodPage' => 'TRUE',
                    'SectionGTMID' => 'test'
                };
                
                String renderHTML = pageSection.getHTML();
                
                System.assert(!String.isBlank(renderHTML));
                
    }
    
}