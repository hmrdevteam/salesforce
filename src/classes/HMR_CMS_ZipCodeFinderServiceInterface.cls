/**
* 
* @Date: 
* @Author: 
* @Modified: Jay Zincone (Health Management Resources) - 2017-12-11 : https://hmrjira.atlassian.net/browse/HMP-71
* @JIRA:
*/

global with sharing class HMR_CMS_ZipCodeFinderServiceInterface implements cms.ServiceInterface {
    private static Pattern brPattern = Pattern.compile('<br( )?(/)?>');
    private static Pattern tagPattern = Pattern.compile('<.*?>');

    /**
     *
     * @param params a map of parameters including at minimum a value for 'action'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {
     //   String action = params.get('action');
        map<String, Object> returnMe = new map<string, Object>();
        try {
            String action = params.get('action');
            if(action == 'getDetailsOfZipCode') {
                returnMe = zipcodeDetails(params);
            }else{
                //invalid action
                returnMe.put('setdata','');
                returnMe.put('success',false);
                returnMe.put('message','Invalid Action');
            } 
        } catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('setdata','');
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
            //return '{"setdata":"","success":false,"message":' + JSON.serialize(message) + '}';
        }

        // No actions matched and no error occurred
        return JSON.serialize(returnMe);
    }
    
    public map<String, Object> zipcodeDetails(Map<String, String> params){
        
        String zipCode = params.get('zipcode');        
        map<String, Object> returnMe = new map<string, Object>();


        String returnJSON;
        List<Account> accountList;
        
        if(String.isBlank(zipCode)){
            returnMe.put('setdata','');
            returnMe.put('success',false);
            returnMe.put('message','Please Enter Zipcode');
            //return '{"setdata":"","success":false,"message":"Please Enter Zipcode"}';
        }else{     
            try{
                Set<Id> accountIdList = new Set<Id>();
                List<Zip_Code_Assignment__c> zipCodeList = new List<Zip_Code_Assignment__c>(
                    [SELECT Name, Account__c FROM Zip_Code_Assignment__c WHERE Name = :zipCode AND Account__r.hmr_Active__c = TRUE AND Account__r.hmr_Referral__c != 'DNR']);

                if(!zipCodeList.isEmpty()) {
                    for(Zip_Code_Assignment__c zc: zipCodeList) {
                        accountIdList.add(zc.Account__c);
                    }
                }
                accountList = new List<Account>([SELECT Id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                                   BillingCountry, BillingLatitude, BillingLongitude,HMR_Account_Name_1_Optional__c,Phone,hmr_Location_Image_URI__c
                                   FROM Account WHERE Id IN :accountIdList]);

                if(accountList.isEmpty()){
                    returnMe.put('setdata','');
                    returnMe.put('success',true);
                    returnMe.put('message','set is empty');

                    //returnJSON = '{"setdata":"","success":true,"message":"set is empty"}';
                }
                else{
                    returnMe.put('setdata',accountList);
                    returnMe.put('success',true);
                    returnMe.put('message','set data');

                    //returnJSON = '{"setdata":'+ JSON.serialize(accountList) + ',"success":true,"message":"set data"}';    
                }
            }
            catch(Exception e){
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMe.put('setdata','');
                returnMe.put('success',false);
                returnMe.put('message',JSON.serialize(message));
            }
        }
        return returnMe;
    
    }

    public static Type getType() {
        return HMR_CMS_ZipCodeFinderServiceInterface.class;
    }
}