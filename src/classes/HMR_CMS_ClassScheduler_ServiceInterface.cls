/**
* Service Interface to get Program membership records
*
* @Date: 2018-02-13
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: 2018-02-20 - Zach Engman
* @JIRA:
*/
global with sharing class HMR_CMS_ClassScheduler_ServiceInterface implements cms.ServiceInterface{
  /**
  *
  * @param params a map of parameters including at minimum a value for 'action'
  * @return a JSON-serialized response string
  */
    public String executeRequest(Map<String, String> params) {
     Map<String, Object> returnMap = new Map<String, Object>{'success' => true};

        if(params.get('contactId') != null){
            String contactId = String.escapeSingleQuotes(params.get('contactId'));
            HMR_Class_Service classService = new HMR_Class_Service(contactId);
            try {
                String action = params.get('action');
                //Return the calling function/action
                returnMap.put('action', action);
 
                if(action == 'getSearchResult') {
                    Date classDate = Date.valueOf(params.get('searchDate'));
                    Map<String, Object> searchResultMap = classService.getSearchResult(classDate);
                    
                    returnMap.put('coachingSessions', searchResultMap.get('classList'));
                    returnMap.put('contactDetails', searchResultMap.get('contactDetails'));
                    returnMap.put('programMembershipDetails', searchResultMap.get('programMembershipDetails'));
                    returnMap.put('autoRedirect', searchResultMap.get('autoRedirect'));
                }
                else if (action == 'assignClass'){
                     String selectedClassId = params.get('selectedClassId');
                     String selectedClassDate = params.get('selectedClassDate');

                     returnMap = classService.assignClass(selectedClassId, selectedClassDate);
                }                
                else{
                    //Invalid Action
                    returnMap.put('success',false);
                    returnMap.put('message','Invalid Action');
                }
            }
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'please enter the contact id');
        }

        return JSON.serialize(returnMap);
    }

    public static Type getType() {
        return HMR_CMS_ClassScheduler_ServiceInterface.class;
    }
}