/**
* Apex Controller Class for Content Search Component
*
* @Date: 2017-05-15
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-05-15
* @JIRA: 
*/
global without sharing class HMR_Content_Search_Controller {
    private final integer DefaultResultMaximum = 10;
    
    global List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> responseObject {get; set;}
    global string result{set;}
    
    global void doSearch(){
        List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList = new List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord>();
        Map<string,string> params = ApexPages.currentPage().getParameters();
        string searchText = params.get('searchText');
        string searchType = params.get('searchType');
        integer maxResults = DefaultResultMaximum; 
        string filterCategory = params.get('filterCategory');
        string filterTopic = params.get('filterTopic');
        boolean includeArticles = searchType == 'both';

        if(!String.isBlank(searchText))
            searchText = String.escapeSingleQuotes(searchText);
        
        if(params.get('maxResults') != null)
            maxResults = Integer.valueOf(params.get('maxResults'));
        
        if(searchType == 'both' || searchType == 'questions')
            resultList = getQuestionAndAnswersSuggestions(searchText, includeArticles, maxResults, filterCategory, filterTopic);
        else if(searchType == 'recipes')
            resultList = getKnowledgeSuggestions(searchText, maxResults, 'recipes');
        else
            resultList = getKnowledgeSuggestions(searchText, maxResults, filterCategory);
        
        responseObject = resultList;
    }
    
    
	public HMR_Content_Search_Controller(){}
	
    @RemoteAction
    public static List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> getQuestionAndAnswersSuggestions(string searchText, boolean includeArticles, integer maxResults, string filterCategory, string filterTopic){
        HMR_Content_Search_Service contentService = new HMR_Content_Search_Service();
        List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList = contentService.getSearchSuggestions(searchText, includeArticles, maxResults, filterCategory, filterTopic);
        return resultList;
    }
    
	@RemoteAction
	public static List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> getKnowledgeSuggestions(string searchText, integer maxResults, string filterCategory){
		HMR_Content_Search_Service contentService = new HMR_Content_Search_Service();
		List<Search.SearchResult> searchResultList = contentService.getKnowledgeSearchResults(searchText, filterCategory);
        List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord> resultList = new List<HMR_Content_Search_Service.QuestionAndAnswerResultRecord>();
        
        if(filterCategory == 'recipes'){
            for(Search.SearchResult searchResult : searchResultList)
                resultList.add(new HMR_Content_Search_Service.QuestionAndAnswerResultRecord((Recipe__kav)searchResult.getSObject()));

            resultList = contentService.appendRecipeCategories(resultList);
        }
        else{
            for(Search.SearchResult searchResult : searchResultList)
                resultList.add(new HMR_Content_Search_Service.QuestionAndAnswerResultRecord(searchResult, contentService.KnowledgeDataCategoryMap, contentService.SuccessStoryHMRAuthoredIdSet));
        }

        return resultList;
	}

    public string getResult(){
        return JSON.serializePretty(responseObject);
    }
}