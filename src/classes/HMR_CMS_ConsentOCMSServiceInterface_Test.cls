/**
* @Date: 4/5/2017
* @Author: Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-
* @Modified: Utkarsh Goswami(Mindtree)
* This test class covers HMR_CMS_ConsentOCMSServiceInterface Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CMS_ConsentOCMSServiceInterface_Test{

  @isTest static void testMethodForSetConsent() {
  
        cc_dataFActory.setupTestUser();
    
        User thisUser = [Select Id,contactId FROM User WHERE Id = :UserInfo.getUserId()];
        
        ccrz__E_Term__c term1=new ccrz__E_Term__c(ccrz__Title__c='Health Consent v1');
        ccrz__E_Term__c term2=new ccrz__E_Term__c(ccrz__Title__c='HMR Terms & Conditions v1');
        ccrz__E_Term__c term3=new ccrz__E_Term__c(ccrz__Title__c='Auto-Delivery Terms v1');
        
        insert term1;
        insert term2;
        insert term3;
        
        HMR_CMS_ConsentOCMSServiceInterface servObj1 = new HMR_CMS_ConsentOCMSServiceInterface();
        Map<String, String> params = new Map<String, String>(); 
        System.runAs(thisUser){
        
            params.put('action','setConsent');
            String returnResponse = servObj1.executeRequest(params);  
            
            System.assert(!String.isBlank(returnResponse));
       }
   
   }
   
    @isTest static void testMethodForGetConsent() {
    
        cc_dataFActory.setupTestUser();
    
        User thisUser = [Select Id,contactId FROM User WHERE Id = :UserInfo.getUserId()];
        
        Id contactId = [ select Id from contact limit 1 ].id;
        
        ccrz__E_Term__c term1=new ccrz__E_Term__c(ccrz__Title__c='Health Consent v1');
        ccrz__E_Term__c term2=new ccrz__E_Term__c(ccrz__Title__c='HMR Terms & Conditions v1');
        ccrz__E_Term__c term3=new ccrz__E_Term__c(ccrz__Title__c='Auto-Delivery Terms v1');
        
        insert term1;
        insert term2;
        insert term3;
       
        Consent_Agreement__c conAgree1=new Consent_Agreement__c();
        conAgree1.Contact__c= thisUser.contactId ;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Health Consent v1' limit 1].Id;
        
        Consent_Agreement__c conAgree2=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='HMR Terms & Conditions v1' limit 1].Id;
        
        Consent_Agreement__c conAgree3=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Auto-Delivery Terms v1' limit 1].Id;
        
        insert conAgree1;
        insert conAgree2;
        insert conAgree3;
        
        System.runAs(thisUser){
       
            HMR_CMS_ConsentOCMSServiceInterface servObj1 = new HMR_CMS_ConsentOCMSServiceInterface();
            Map<String, String> params = new Map<String, String>(); 
            params.put('action','getConsent');
            String returnResponse = servObj1.executeRequest(params);      
            System.assert(!String.isBlank(returnResponse));
       }
   
   } 
   
   
   @isTest static void testMethodForNoGetNoSetConsent() {
    
        cc_dataFActory.setupTestUser();
    
        User thisUser = [Select Id,contactId FROM User WHERE Id = :UserInfo.getUserId()];
        
        Id contactId = [ select Id from contact limit 1 ].id;
        
        ccrz__E_Term__c term1=new ccrz__E_Term__c(ccrz__Title__c='Health Consent v1');
        ccrz__E_Term__c term2=new ccrz__E_Term__c(ccrz__Title__c='HMR Terms & Conditions v1');
        ccrz__E_Term__c term3=new ccrz__E_Term__c(ccrz__Title__c='Auto-Delivery Terms v1');
        
        insert term1;
        insert term2;
        insert term3;
       
        Consent_Agreement__c conAgree1=new Consent_Agreement__c();
        conAgree1.Contact__c= thisUser.contactId ;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Health Consent v1' limit 1].Id;
        
        Consent_Agreement__c conAgree2=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='HMR Terms & Conditions v1' limit 1].Id;
        
        Consent_Agreement__c conAgree3=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Auto-Delivery Terms v1' limit 1].Id;
        
        insert conAgree1;
        insert conAgree2;
        insert conAgree3;
        
        System.runAs(thisUser){
       
            HMR_CMS_ConsentOCMSServiceInterface servObj1 = new HMR_CMS_ConsentOCMSServiceInterface();
            Map<String, String> params = new Map<String, String>(); 
            params.put('action','test');
            String returnResponse = servObj1.executeRequest(params);      
            System.assert(returnResponse.contains('Invalid Action'));
       }
   
   }
   
   
   @isTest static void testMethodForPortalUserSetConsent() {
    
        cc_dataFActory.setupTestUser();
    
        User thisUser = [Select Id,contactId FROM User WHERE Id = :UserInfo.getUserId()];
        
        Id contactId = [ select Id from contact limit 1 ].id;
        
        ccrz__E_Term__c term1=new ccrz__E_Term__c(ccrz__Title__c='Health Consent v1');
        ccrz__E_Term__c term2=new ccrz__E_Term__c(ccrz__Title__c='HMR Terms & Conditions v1');
        ccrz__E_Term__c term3=new ccrz__E_Term__c(ccrz__Title__c='Auto-Delivery Terms v1');
        
        insert term1;
        insert term2;
        insert term3;
       
        Consent_Agreement__c conAgree1=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId ;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Health Consent v1' limit 1].Id;
        
        Consent_Agreement__c conAgree2=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='HMR Terms & Conditions v1' limit 1].Id;
        
        Consent_Agreement__c conAgree3=new Consent_Agreement__c();
        conAgree1.Contact__c=thisUser.contactId;
        conAgree1.Status__c='Notified';
        conAgree1.Version__c=[Select Id from ccrz__E_Term__c where ccrz__Title__c='Auto-Delivery Terms v1' limit 1].Id;
        
        insert conAgree1;
        insert conAgree2;
        insert conAgree3;
        
        System.runAs(thisUser){
       
            HMR_CMS_ConsentOCMSServiceInterface servObj1 = new HMR_CMS_ConsentOCMSServiceInterface();
            Map<String, String> params = new Map<String, String>(); 
            params.put('action','setConsent');
            servObj1.executeRequest(params);  
            
            String returnResponse = servObj1.executeRequest(params);      
            System.assert(!String.isBlank(returnResponse));     
      
       }
   
   } 
   
   @isTest static void testMethodForGetType() {
       
           Type classType =  HMR_CMS_ConsentOCMSServiceInterface.getType();       
            System.assertEquals(HMR_CMS_ConsentOCMSServiceInterface.class, classType);
       }

   @isTest static void testMethodForNegativeTest() {
    
        Map<String, String> paramsBlank;
       
        HMR_CMS_ConsentOCMSServiceInterface servObj2 = new HMR_CMS_ConsentOCMSServiceInterface();
        String response = servObj2.executeRequest(paramsBlank);
        System.assert(response.contains('false'));
   }
  
}