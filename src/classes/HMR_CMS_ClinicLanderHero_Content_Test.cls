/**
* Test Class Coverage of the HMR_CMS_ClinicLanderHeo_Content Class
*   Created last minute for coverage - TODO expand tests
* @Date: 06/06/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/06/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_ClinicLanderHero_Content_Test {
	@isTest 
	private static void testGetClinicInfoSingleModal(){
		HMR_CMS_ClinicLanderHero_ContentTemplate controller = new HMR_CMS_ClinicLanderHero_ContentTemplate();
		List<Account> accountList = new List<Account>{new Account(Name = 'Single Account',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111')};
		

		Test.startTest();

		String htmlResult = controller.returnClinicInfoSingleModal(accountList);

		Test.stopTest();

		//Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
	}

	@isTest 
	private static void testGetClinicInfoDoubleModal(){
		HMR_CMS_ClinicLanderHero_ContentTemplate controller = new HMR_CMS_ClinicLanderHero_ContentTemplate();
		 List<Account> accountList = new List<Account>{new Account(Name = 'First Account',
                                                                  HMR_Account_Name_1_Optional__c = 'First Account Program',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111'),
            										new Account(Name = 'Second Account',
                                                                  BillingStreet = '222 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '222-222-2222')};
		

		Test.startTest();

		String htmlResult = controller.returnClinicInfoDoubleModal(accountList);

		Test.stopTest();

		//Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
	}
    
    @isTest 
	private static void testGetHTML(){
		HMR_CMS_ClinicLanderHero_ContentTemplate controller = new HMR_CMS_ClinicLanderHero_ContentTemplate();
		List<Account> accountList = new List<Account>{new Account(Name = 'First Account',
                                                                  HMR_Account_Name_1_Optional__c = 'First Account Program',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111'),
            										 new Account(Name = 'Second Account',
                                                                  BillingStreet = '222 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '222-222-2222')};
		
		insert accountList;
        insert new Zip_Code_Assignment__c(Name = '90067', Account__c = accountList[0].Id);
        
        ApexPages.currentPage().getParameters().put('z', '90067');
        
		Test.startTest();

		String htmlResult = controller.getHTML();

		Test.stopTest();

		//Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
	}
    
    @isTest 
	private static void testExecuteRequest(){
        Map<String, String> leadParameterMap = new Map<String, String>{'Company' => 'Magnet 360', 'FirstName' => 'Lara', 'LastName' => 'Lead'};
        
        Test.startTest();
        
        string leadResult = HMR_CMS_ClinicLanderHero_ContentTemplate.executeRequest(leadParameterMap);
        
        Test.stopTest();
    }
}