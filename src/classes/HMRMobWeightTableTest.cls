/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/12/2018
 * Created By: 
 * Last Modified Date: 03/12/2018
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobWeightTable REST Service
 * ****************************************************/
 @isTest
private class HMRMobWeightTableTest {
	@testSetup
    private static void setupData(){
		User userRecord = cc_dataFactory.testUser;
		Contact contactRecord = cc_dataFactory.testUser.Contact;
    }

    @isTest
    private static void testGetAll(){
		User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestURI = '/services/apexrest/mobile/weightTable/';
		request.params.put('communityUserId', userRecord.Id);
		request.params.put('allRecords', 'true');
		request.httpMethod = 'GET';

		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();

		System.runAs(userRecord){
			HMRMobWeightTable.doGet();
		  }

		Test.stopTest();

		//Verify the weight table was successfully returned
		System.assertEquals(200, response.statuscode);
    }

	@isTest
	private static void testGetByWeight(){
		User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestURI = '/services/apexrest/mobile/weightTable/';
		request.params.put('communityUserId', userRecord.Id);
		request.params.put('weight', '200');
		request.httpMethod = 'GET';

		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();

		System.runAs(userRecord){
			HMRMobWeightTable.doGet();
		  }

		Test.stopTest();

		//Verify the weight table record was successfully returned
		System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetByWeightWithInvalidWeight(){
    	User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestURI = '/services/apexrest/mobile/weightTable/';
		request.params.put('communityUserId', userRecord.Id);
		request.params.put('weight', '200abc');
		request.httpMethod = 'GET';

		RestContext.request = request;
		RestContext.response = response;

		Test.startTest();

		System.runAs(userRecord){
			HMRMobWeightTable.doGet();
		  }

		Test.stopTest();

		//Verify the error status was returned
		System.assertEquals(404, response.statuscode);
    }

    @isTest
    private static void testGetWeightTableRecord(){
		User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
		HMRMobWeightTable.WeightTableRecordDTO weightTableRecordDTO;

		Test.startTest();

		System.runAs(userRecord){
			weightTableRecordDTO = HMRMobWeightTable.getWeightTableRecord(200.0);
		}

		Test.stopTest();

		//Verify the weight table record was successfully returned
		System.assert(weightTableRecordDTO != null);
    }

}