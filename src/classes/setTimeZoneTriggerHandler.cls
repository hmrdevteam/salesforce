/**
* Trigger Handler for SetContactTimeZone.trigger
* Updating Contact records with a Time Zone value, retrieved from 2 custom setting tables
* Objects referenced --> Contact, County__c, Zip_County__c
*
* @Date: 2017-03-19
* @Author Nathan Anderson (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-2628
*/

public with sharing class setTimeZoneTriggerHandler {

	public static void handleAfterInsert (List<Contact> newContacts) {

		//Create Set to hold zip codes to use in querying for County FIP Number
		Set<String> zipCodes = new Set<String>();
		//Create Set to hold Contact Ids, so we can pass to future method to avoid recursions and processing issues
		Set<Id> ContactIds = new Set<Id>();

		//Loop through Contacts in list passed in from Trigger.new
		//Add MailingPostalCode value to zip code set and add Id to Id set
		for(Contact c : newContacts) {

			if(c.MailingPostalCode != null) {
				zipCodes.add(c.MailingPostalCode);
				ContactIds.add(c.Id);
			}
		}

		//Using Zip Code set, query for County FIPS Number from Zip County table
		List<Zip_County__c> zipCounties = [SELECT Id, FIPS_Number__c, Zip_Code__c FROM Zip_County__c WHERE Zip_Code__c IN :zipCodes];
		//Create map for holding FIPS number key to Zip Code Value, then fill in map by looping through zipCounties list
		Map<String, String> zipFipMap = new Map<String, String>();

		for(Zip_County__c zc : zipCounties) {
			zipFipMap.put(zc.FIPS_Number__c, zc.Zip_Code__c);
		}

		//Create set to hold FIPs Numbers for use in County query
		Set<String> countyFIPs = new Set<String>();

		for(Zip_County__c zc : zipCounties) {

			countyFIPs.add(zc.FIPS_Number__c);

		}

		//Create list of counties and offset information using FIPs Number set
		List<County__c> counties = [SELECT Id, Name, TZ_Offset__c FROM County__c WHERE Name IN : countyFIPs];
		//Create map for holding Zip Code key to Offset Value, then fill in map by looping through counties list
		Map<String, Decimal> fipOffsetMap = new Map<String, Decimal>();

		for(County__c county : counties) {

			fipOffsetMap.put(zipFipMap.get(county.Name), county.TZ_Offset__c);

		}

		//call updateContacts method
		//pass set of Contact Ids, along with fipOffsetMap
		updateContacts(ContactIds, fipOffsetMap);
	}

	//set up future method to handle processing of contact updates to avoid recursion and other issues
	@future
	public static void updateContacts(Set<Id> ContactIds, Map<String, Decimal> fipOffsetMap) {

		//Create new list to update records in bulk
		List<Contact> contactToUpdate = new List<Contact>();

		//Loop through contacts in the passed-in Set
		for(Contact c : [SELECT Id, MailingPostalCode FROM Contact WHERE Id IN :ContactIds]) {
			if(c.MailingPostalCode != null && fipOffsetMap.get(c.MailingPostalCode) != null) {

				//get offset value using Zip Code key from the map
				c.Time_Zone_Offset__c = fipOffsetMap.get(c.MailingPostalCode);
				//add to list to be updated
				contactToUpdate.add(c);
			}
		}
		//process updates and catch exceptions
		try{
			update contactToUpdate;
		} catch (exception e) {
			System.debug('EXCEPTION IN UPDATE: ' + e);
		}
	}
}