/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 31st, 2017
Target Class: HMR_CMS_VideoPageSection_ContentTemplate
*/

@isTest
private class HMR_CMS_VideoPageSec_ContentTmpl_Test {
    @isTest 
    private static void testGetHtml() {
        HMR_CMS_VideoPageSection_ContentTemplate videoController = new HMR_CMS_VideoPageSection_ContentTemplate();          
        String propertyValue = videoController.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');  
        String BackgroundImageClass = videoController.BackgroundImageClass;  
        String SectionTitle = videoController.SectionTitle;
        String VideoIconUrl = videoController.VideoIconUrl;
        String VideoTitle = videoController.VideoTitle;  
        String VideoUrl = videoController.VideoUrl;
        
        String respHTML = videoController.getHTML();              
        
        System.assert(!String.isBlank(respHTML));
    }       
           
}