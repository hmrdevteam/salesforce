/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/08/2018
 * Last Modified Date: 03/08/2018
 * Last Modified By:
 * Description: Test coverage of the HMR_TestFactory
 * ****************************************************/
 @isTest
private class HMR_TestFactory_Test {
	@isTest
	private static void testCreateSingleObject(){
		Contact contactRecord;

		Test.startTest();

		contactRecord = (Contact)HMR_TestFactory.createSObject('Contact');

		Test.stopTest();

		System.assert(contactRecord != null);
	}

	@isTest
	private static void testCreateSingleObjectInvalid(){
		Contact contactRecord;

		Test.startTest();

		try{
			contactRecord = (Contact)HMR_TestFactory.createSObject('ContactInvalid__c');
		}
		catch(HMR_TestFactory.HMR_TestFactoryException ex){}

		Test.stopTest();

		System.assert(contactRecord == null);
	}

	@isTest
	private static void testCreateMultipleObjects(){
		List<Account> accountList;

		Test.startTest();

		accountList = (List<Account>)HMR_TestFactory.createSObjectList('Account', 10);

		Test.stopTest();

		System.assert(accountList.size() == 10);
	}

	@isTest
	private static void testCreateActivityLogObject(){
		ActivityLog__c activityLogRecord;

		Contact contactRecord = HMR_TestFactory.createContact();
		insert contactRecord;
		
		Program__c programRecord = HMR_TestFactory.createProgram();
		insert programRecord;

		Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
		insert programMembershipRecord;

		Test.startTest();

		activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);

		Test.stopTest();

		System.assert(activityLogRecord != null);
	}

	@isTest
	private static void testCreateClassObject(){
		Class__c classRecord;

		User userRecord = HMR_TestFactory.createUser('HMR Standard User', true);
		insert userRecord;

		Program__c programRecord = HMR_TestFactory.createProgram();
		insert programRecord;

		Coach__c coachRecord = HMR_TestFactory.createCoach(userRecord.Id);
		insert coachRecord;
		
		Test.startTest();

		classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);

		Test.stopTest();

		System.assert(classRecord != null);
	}

	@isTest
	private static void testCreateClassMemberObject(){
		Class_Member__c classMemberRecord;

		Contact contactRecord = HMR_TestFactory.createContact();
		insert contactRecord;
		
		Program__c programRecord = HMR_TestFactory.createProgram();
		insert programRecord;

		Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
		insert programMembershipRecord;

		User userRecord = HMR_TestFactory.createUser('HMR Standard User', true);
		insert userRecord;

		Coach__c coachRecord = HMR_TestFactory.createCoach(userRecord.Id);
		insert coachRecord;

		Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
		insert classRecord;
		
		Test.startTest();

		classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);

		Test.stopTest();

		System.assert(classMemberRecord != null);
	}

	@isTest
	private static void testCreateCoachObject(){
		Coach__c coachRecord;
		
		Test.startTest();

		User userRecord = HMR_TestFactory.createUser('HMR Standard User', true);
		insert userRecord;

		coachRecord = HMR_TestFactory.createCoach(userRecord.Id);

		Test.stopTest();

		System.assert(coachRecord != null);
	}

	@isTest
	private static void testCreateCoachingSessionObject(){
		Coaching_Session__c coachingSessionRecord;

		User userRecord = HMR_TestFactory.createUser('HMR Standard User', true);
		insert userRecord;

		Program__c programRecord = HMR_TestFactory.createProgram();
		insert programRecord;

		Coach__c coachRecord = HMR_TestFactory.createCoach(userRecord.Id);
		insert coachRecord;

		Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
		insert classRecord;

		Test.startTest();

		coachingSessionRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);

		Test.stopTest();

		System.assert(coachingSessionRecord != null);
	}

	@isTest
	private static void testCreateContactObject(){
		Contact contactRecord;

		Test.startTest();

		contactRecord = HMR_TestFactory.createContact();

		Test.stopTest();

		System.assert(contactRecord != null);
	}

	@isTest
	private static void testCreateMultipleHandoutObjects(){
		List<Handouts__c> handoutList;
		Program__c programRecord = HMR_TestFactory.createProgram();

		insert programRecord;

		Test.startTest();

		handoutList = HMR_TestFactory.createHandoutList(programRecord.Id, 10);

		Test.stopTest();

		System.assert(handoutList.size() == 10);
	}

	@isTest
	private static void testCreateProgramObject(){
		Program__c programRecord;

		Test.startTest();

		programRecord = HMR_TestFactory.createProgram();

		Test.stopTest();

		System.assert(programRecord != null);
	}

	@isTest
	private static void testCreateProgramMembershipObject(){
		Program_Membership__c programMembershipRecord;
		Contact contactRecord = HMR_TestFactory.createContact();
		Program__c programRecord = HMR_TestFactory.createProgram();

		insert contactRecord;
		insert programRecord;

		Test.startTest();

		programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);

		Test.stopTest();

		System.assert(programMembershipRecord != null);
	}

	@isTest
	private static void testCreateSessionAttendeeObject(){
		Session_Attendee__c sessionAttendeeRecord;

		Contact contactRecord = HMR_TestFactory.createContact();
		insert contactRecord;

		Program__c programRecord = HMR_TestFactory.createProgram();
		insert programRecord;

		Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
		insert programMembershipRecord;

		User userRecord = HMR_TestFactory.createUser('HMR Standard User', true);
		insert userRecord;

		Coach__c coachRecord = HMR_TestFactory.createCoach(userRecord.Id);
		insert coachRecord;

		Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
		insert classRecord;

		Coaching_Session__c coachingSessionRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
		insert coachingSessionRecord;

		Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
		insert classMemberRecord;
		
		Test.startTest();

		sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord.Id, classMemberRecord.Id, contactRecord.Id);

		Test.stopTest();

		System.assert(sessionAttendeeRecord != null);
	}

	@isTest
	private static void testCreateMultipleTrackerNoteObjects(){
		List<Tracker_Note__c> trackerNoteList;
		Contact contactRecord = HMR_TestFactory.createContact();

		insert contactRecord;

		Test.startTest();

		trackerNoteList = HMR_TestFactory.createTrackerNoteList(contactRecord.Id, 5);

		Test.stopTest();

		System.assert(trackerNoteList.size() == 5);
	}

}