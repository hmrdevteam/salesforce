/**
* Controller for HMR Community Login Page
* Extends the standard Community Site.Login method and processes the custom redirect logic
* * @Date: 2017-11-29
* @Author Nathan Anderson (Magnet 360)
* @Modified: 2018-01-16 Javier Arroyo and Zach Engman
* @JIRA:
*/
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public String currentCartId {get; set;}
    public String startUrl {get; set;}
    public String refURL {get; set;}
    public Boolean hasKitCart {get; set;}
    public String showContinueAsGuest {get;set;}
    public List<User> userRecordList {get; set;}
    public String cartId = '';
    public Boolean isMobileFlow {get; set;}
    private List<Connected_App_Setting__mdt> connectedAppSettingList{get{
        if(connectedAppSettingList == null)
            connectedAppSettingList = [SELECT DeveloperName, Setting_Value__c FROM Connected_App_Setting__mdt];

        return connectedAppSettingList;
    } set;}
    private string clientId{get{
        if(clientId == null){
            for(Connected_App_Setting__mdt appSetting : connectedAppSettingList){
                if(appSetting.DeveloperName == 'HMR_Diet_Tracker_Consumer_Key'){
                    clientId = appSetting.Setting_Value__c;
                    break;
                }
            }
        }

        return clientId;
    }set;}
    private string redirectUri{get{
        if(redirectUri == null){
            for(Connected_App_Setting__mdt appSetting : connectedAppSettingList){
                if(appSetting.DeveloperName == 'HMR_Diet_Tracker_Redirect_Uri'){
                    redirectUri = appSetting.Setting_Value__c;
                    break;
                }
            }
        }

        return redirectUri;
    }set;}

    global SiteLoginController () {
        this.isMobileFlow  =  ApexPages.currentPage().getUrl().containsIgnoreCase('RemoteAccessAuthorizationPage');


        //The constructor defines the startURL that will be passed to the login method
        //The default is My Account. If the user clicked on Sign-in from a page other than the home page, that will override My Account.
        //If the user got to Sign-in from cart on the way to checkout, that will override as well.
        currentCartId = '';
        startUrl = '/my-account';
        showContinueAsGuest = 'hide';

        refURL = ApexPages.currentPage().getHeaders().get('Referer');
        String baseURL = Site.getBaseUrl() + '/';

        if(refURL != null && !refURL.contains('sign-in') && refURL != baseURL) {
            startUrl = refURL;
        }

        if(ApexPages.currentPage().getParameters().get('hasKitCart') != null){
            hasKitCart = Boolean.valueOf(ApexPages.currentPage().getParameters().get('hasKitCart'));
            if(!hasKitCart) {
                showContinueAsGuest = 'show';
            }
            startUrl = 'checkout';
        }

        if(ApexPages.currentPage().getCookies().get('currCartId') != null)
            currentCartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
        else if(ApexPages.currentPage().getParameters().get('cartID') != null)
            currentCartId = ApexPages.currentPage().getParameters().get('cartID');

        if(this.isMobileFlow)
            startUrl = baseUrl + 'services/oauth2/authorize?response_type=token&client_id=' + clientId + '&redirect_uri=' + this.redirectUri;
    }

    global PageReference login() {

        //Log user in
        PageReference loginPage = Site.login(username, password, startUrl);

        //if login is successful, check if a guest cart needs converted
        //if not redirecting to checkout, also check if the user has open consent agreements
        if(loginPage != null) {
            string activeCartId = '';
            userRecordList = [SELECT ContactId, Contact.MobileLogin__c FROM User WHERE UserName =: username];

            //If we are in mobile flow and the mobile login flag has not been set, set it now
            if(this.isMobileFlow && userRecordList.size() > 0 && userRecordList[0].Contact != null && !userRecordList[0].Contact.MobileLogin__c){
                update new Contact(Id = userRecordList[0].ContactId, MobileLogin__c = true);
            }

            //Transfer a guest cart if one exists, if not passed in url, check cookie
            if(String.isBlank(cartId) && ApexPages.currentPage().getCookies().get('currCartId') != null)
                cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
            if(!String.isBlank(cartId)) {
                activeCartId = cartId;
                if(!String.isBlank(activeCartId)){
                    ccrz.cc_RemoteActionResult cartTransferResult = HMR_CC_CartTransfer_Extension.transferCartToUser(activeCartId, userRecordList[0].Id);
                    //If Cart was Transfered/Take to Cart, must pass in redirect here to login, not directly to response string or it won't login
                    if(cartTransferResult.success && startUrl == 'checkout'){
                        PageReference checkoutRedirect = Site.login(username, password, Site.getBaseUrl() + String.format('/checkout?cartID={0}', new String[]{String.valueOf(cartTransferResult.data)}));

                        return checkoutRedirect;
                    }
                }
            }

            //Check for open consent agreements and redirect to consent page if so, skip when in mobile application
            if(!this.isMobileFlow){
                Id clientId = userRecordList[0].ContactId;
                List<Consent_Agreement__c> cList = [SELECT Id, Consent_Type__c FROM Consent_Agreement__c WHERE Status__c = 'Notified' AND Contact__c = :clientId];

                if(cList.size() > 0) {
                    startUrl = '/consent';
                    PageReference consentRedirect = Site.login(username, password, startUrl);

                    return consentRedirect;
                }
            }

            //if not sending to checkout or consent, send to startURL as set in constructor
            return loginPage;
        }
        else {
             //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Incorrect password or email address entered. Please try again.');
             //ApexPages.addMessage(msg);
            return null;
        }

        return loginPage;

    }
}