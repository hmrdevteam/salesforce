/*****************************************************
 * Author: Utkarsh Goswami (Mindtree)
 * Created Date: 01/08/2018
 * Created By: Utkarsh Goswami (Mindtree)
 * Last Modified Date: 2018-01-10 
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobPicklist REST Service
 * ****************************************************/
@isTest
private class HMRMobPicklistTest {


    @isTest
    private static void testGetPickListValues(){
    
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/data/pickListValues';
      request.httpMethod = 'GET';
      request.params.put('objectApiName', 'Program__c');

      RestContext.request = request;
      RestContext.response = response;
      
      Test.startTest();

      HMRMobPicklist.getPickListValues();
      
      Test.stopTest();
      
      //Verify picklist values are returned
      System.assertEquals(200, response.statusCode);
      
    }

    @isTest
    private static void testGetPickListValuesWithInvalidObject(){
    
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/data/pickListValues';
      request.httpMethod = 'GET';
      request.params.put('objectApiName', 'ProgramBad__c');

      RestContext.request = request;
      RestContext.response = response;
      
      Test.startTest();

      HMRMobPicklist.getPickListValues();
      
      Test.stopTest();
      
      //Verify error is returned
      System.assertEquals(400, response.statusCode);
      
    }
}