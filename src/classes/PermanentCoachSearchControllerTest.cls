/**
* Unit Test for PermanentCoachSearchController 
*
* Objects referenced --> User, Account, Contact, Coach__c, Program__c, Class__c
*
* @Date: 1/23/2016 
* @Author Nathan Anderson (M360)
* @Modified: 
* @JIRA: Issue https://reside.jira.com/browse/HPRP-631, Task https://reside.jira.com/browse/HPRP-684
*/



@isTest
private Class PermanentCoachSearchControllerTest{

    static testMethod void firstTestMethod(){
        
        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userObj = new User(Alias = 'sSCTe', Email='standuser321_SCTest@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj;     
        
        
        Account testAccountObj = new Account(name = 'test Account SCTest');
        insert testAccountObj;
        
        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact SCTest', Account = testAccountObj, Email = 'test@test.com');
        insert testContatObj;           
        
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test_SCTest@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;
        
        Program__c programObj = new Program__c(Name = 'test program SCTest', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;   
        
        
        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class SCTest', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;
        
       
        
        Test.setCurrentPageReference(new PageReference('Page.PermanentCoachSearch'));
        System.currentPageReference().getParameters().put('Id', classObj.id);

        List<Coach__c> coachList = new List<Coach__c>();
        coachList.add(coachObj);

        ApexPages.StandardController controllerObj = new ApexPages.StandardController(classObj);
        PermanentCoachSearchController csObject = new PermanentCoachSearchController(controllerObj);
        
        csObject.selectedCoachId = coachObj.id;
        csObject.classDetail = classObj;
        csObject.newCoachList = coachList;
        csObject.selectedCoach();
        csObject.returnToPreviousPage();
        csObject.assignCoach();
        
        System.assertEquals(1, coachList.size());
        
        Test.stopTest();
    } 
    
}