/**
* @Date: 4/7/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
* This test class covers HMR_CC_DiscountAdjustment_Extn  Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CC_DiscountAdjustment_Extn_Test{

  @isTest static void test_method_one() {
   
   
   HMR_CC_Cybersource_RequestForm_Ctrl  cybrsorceReq = new HMR_CC_Cybersource_RequestForm_Ctrl();
            
        ccrz__E_Cart__c shpCart = new ccrz__E_Cart__c(ccrz__CartType__c = 'Cart');
        insert shpCart;
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        String cart = shpCart.Id;
        
        Decimal amt = 10;
        
        HMR_CC_DiscountAdjustment_Extn.adjustHMRDiscount(ctx,cart,amt);
                  

    }  

}