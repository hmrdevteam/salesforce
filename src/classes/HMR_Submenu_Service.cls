/**
* Service class for the submenu within the community
* @Date: 2018-01-30
* @Author Zach Engman (Magnet 360)
* @Modified: 2018-01-30 Zach Engman
* @JIRA:
*/
public with sharing class HMR_Submenu_Service {
	public HMR_Submenu_Service() {}

	public List<SubmenuItem> getCommunitySubmenu(string userId){
		List<SubmenuItem> submenuItemList = new List<SubmenuItem>();
		for(Community_Submenu_Setting__mdt submenuRecord : [SELECT MasterLabel, 
																 Community_Url__c, 
																 Is_Mobile_App_Enabled__c, 
																 Sequence__c 
													 	  FROM Community_Submenu_Setting__mdt 
													 	  ORDER BY Sequence__c]){
			submenuItemList.add(new SubmenuItem(submenuRecord));
		}
															 
	    //check if user is has logged in to mobile application
	    List<Contact> contactList = [SELECT Id FROM Contact WHERE Community_User_ID__c =: userId AND MobileLogin__c = true];
	    boolean mobileLogin = contactList.size() > 0;

	    //check if user is enrolled in class
	    List<Class_Member__c> classMemberList = [SELECT Id FROM Class_Member__c WHERE hmr_Contact_Name__r.Community_User_ID__c =: userId AND hmr_Active__c = true];
	    boolean isEnrolled = classMemberList.size() > 0;

	    if(!mobileLogin){
	    	for(SubmenuItem subMenuSetting : submenuItemList){
	    		if(subMenuSetting.label == 'Dashboard'){
	    			subMenuSetting.communityUrl = 'dashboard';
	    		}
	    	}
	    }

	    if(isEnrolled){
	    	for(SubmenuItem subMenuSetting : submenuItemList){
	    		if(subMenuSetting.label == 'Coaching'){
	    			subMenuSetting.communityUrl = 'group-data';
	    		}
	    	}
	    }

		return submenuItemList;
	}

	public class SubmenuItem{
		public string label {get; set;}
		public string communityUrl {get; set;}
		public boolean isMobileEnabled {get; set;}

		public SubmenuItem(Community_Submenu_Setting__mdt subMenuSetting){
			this.label = submenuSetting.MasterLabel;
			this.communityUrl = submenuSetting.Community_Url__c;
			this.isMobileEnabled = submenuSetting.Is_Mobile_App_Enabled__c;
		}
	}
}