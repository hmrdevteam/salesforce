/**
* Apex Service Class for the Order
*
* @Date: 2017-04-25
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-06-19
* @JIRA: 
*/
public with sharing class HMR_CC_Order_Service {
    @testVisible
    private static decimal FreeShippingMinimum;
    @testVisible
    private static decimal DiscountMinimum;
    @testVisible
    private static decimal DiscountAmount = .10;
    @testVisible
    private static decimal StandardShipping = 11.95;
    
    
    public ccrz__E_Order__c orderRecord {get; set;}
    public List<ccrz__E_OrderItem__c> orderItemsList {get; set;}
    public ccrz__E_Coupon__c reorderCoupon {get; set;}
    
    //Constructors
    public HMR_CC_Order_Service(String orderId){
        this(orderId, new ccrz__E_Coupon__c());
    }
    public HMR_CC_Order_Service(String orderId, List<ccrz__E_OrderItem__c> orderItemList){
        this(HMR_CC_Order_Selector.getById(orderId), orderItemList);
    }
    public HMR_CC_Order_Service(ccrz__E_Order__c orderRecord, List<ccrz__E_OrderItem__c> orderItemsList){
        this.orderRecord = orderRecord;
        this.orderItemsList = orderItemsList;
    }
     public HMR_CC_Order_Service(String orderId, ccrz__E_Coupon__c reorderCoupon){
        this.orderRecord = HMR_CC_Order_Selector.getById(orderId);
        if(this.orderRecord != null)
            this.orderItemsList = orderRecord.ccrz__E_OrderItems__r;
        this.reorderCoupon = reorderCoupon;
    }
    
    //Process the Business Rules for the Order
    //Shipping is Free for Orders over $50
    //10% Applied to Orders over $100
    public void processBusinessRules(boolean autoUpdate){
        if(this.orderRecord == null || this.orderItemsList == null)
            return;
        
        ccrz__E_OrderItem__c couponOrderItem;
        ccrz__E_OrderItem__c kitOrderItem;
        decimal calculatedTotal = 0;
        decimal shipping = orderRecord.ccrz__ShipAmount__c;
        decimal orderValueDiscount = 0;
        String shipMethod = orderRecord.ccrz__ShipMethod__c;
        String orderType = orderRecord.hmr_Order_Type_c__c;
        decimal discount = 0;
        boolean hasItemLineChanged = false;
        boolean usingReorderCoupon = false;
        boolean usingAbsoluteCoupon = false;
        List<Account> accountList = [ SELECT Name FROM Account WHERE Id =: orderRecord.ccrz__Account__c];
        String accountName = accountList.size() > 0 ? accountList[0].Name : null;

        if(discount == null)
            discount = 0;
        
        if(shipping == null)
            shipping = 0;

        //If order type is P2 Free Shipping Minimum is $50 & Discount Minimum is $100
        //All other phase types have no minimums 
        if(orderType != null && orderType.contains('P2')){
            FreeShippingMinimum = 50;
            DiscountMinimum = 100;
        }
        else{
            FreeShippingMinimum = 1;
            DiscountMinimum = 1;
        }

        if(reorderCoupon != null && !String.isBlank(reorderCoupon.Id)  && accountName != 'HMR Employees'){
            
            usingReorderCoupon = true;
            DiscountMinimum = reorderCoupon.ccrz__CartTotalAmount__c;
            
            if(reorderCoupon.ccrz__CouponType__c == 'Percentage'){                
                DiscountAmount = reorderCoupon.ccrz__DiscountAmount__c / 100;
                discount = 0;
            }
            else{
                usingAbsoluteCoupon = true;
                DiscountAmount = reorderCoupon.ccrz__DiscountAmount__c;
                discount = reorderCoupon.ccrz__DiscountAmount__c;
            }           
        }

        if(String.isBlank(shipMethod)){
            if(shipping <= StandardShipping){
                shipMethod = 'Standard';
            }
            else{
                shipMethod = 'Expedited';
            }            
        }

        //Get the Total of all the Line Items
        for(ccrz__E_OrderItem__c oi: orderItemsList){
            if(oi.ccrz__ProductType__c <> 'Coupon'){
                decimal lineTotal = oi.ccrz__Price__c * oi.ccrz__Quantity__c;
                calculatedTotal += lineTotal;
            }
        }

        if(calculatedTotal >= FreeShippingMinimum && shipping != 0 && !shipMethod.contains('Expedited'))//Expedited shipping orders don't ever get free shipping
            shipping = 0;
        else if(shipMethod.contains('Expedited')){ //Took logic from trigger to match for expedited shipping rates
            if(calculatedTotal <= 150.00){
                shipping = 35.00;
            }
            else if(calculatedTotal > 150.00 && calculatedTotal <= 299.99){
                shipping = 50.00;
            }
            else if(calculatedTotal >= 300.00 && calculatedTotal <= 499.99){
                shipping = 75.00;
            }
            else if(calculatedTotal >= 500.00){
                shipping = 100.00;
            }
            //If all conditions fail set shipping to 35.00
            else{
                shipping = 35.00;
            }
        }
        
        if(calculatedTotal >= DiscountMinimum && accountName != 'HMR Employees' && !usingAbsoluteCoupon){
            orderValueDiscount = 0.0;

            //Adjust the Discount on the Line Items
            for(ccrz__E_OrderItem__c oi : orderItemsList){
                if(oi.ccrz__ProductType__c <> 'Coupon' && oi.ccrz__ProductType__c <> 'Dynamic Kit' && oi.ccrz__ProductType__c <> 'Kit'){
                    decimal lineTotal = oi.ccrz__Price__c * oi.ccrz__Quantity__c;
                    decimal lineDiscount = lineTotal * DiscountAmount;
                    lineDiscount = lineDiscount.setScale(2, RoundingMode.HALF_UP);
                    orderValueDiscount += lineDiscount;
                    //Determine if line is impacted
                    if(oi.ccrz__AbsoluteDiscount__c != lineDiscount ||  oi.ccrz__SubAmount__c != lineTotal - lineDiscount || oi.ccrz__PercentDiscount__c != DiscountAmount * 100){
                        hasItemLineChanged = true;
                        //For P2 1st Order the discount is stored on the custom kit line so we do not want it here
                        if(orderType != 'P2 1st Order'){
                            oi.ccrz__AbsoluteDiscount__c = lineDiscount;
                            oi.ccrz__SubAmount__c = lineTotal - lineDiscount;
                            oi.ccrz__PercentDiscount__c = DiscountAmount * 100;
                        }
                        else{
                            oi.ccrz__AbsoluteDiscount__c = 0.0;
                            oi.ccrz__PercentDiscount__c = 0.0;
                        }
                    }
                }
                else if(oi.ccrz__ProductType__c == 'Coupon'){
                    couponOrderItem = oi;
                }
                else if(oi.ccrz__ProductType__c == 'Dynamic Kit' || oi.ccrz__ProductType__c == 'Kit'){
                    kitOrderItem = oi;
                }
            }
        }
        else{
            orderValueDiscount = 0;
            discount = 0;
            for(ccrz__E_OrderItem__c oi : orderItemsList){
                if(oi.ccrz__ProductType__c <> 'Coupon' && oi.ccrz__ProductType__c <> 'Dynamic Kit' && oi.ccrz__ProductType__c <> 'Kit'){
                    if(oi.ccrz__AbsoluteDiscount__c != 0 || oi.ccrz__SubAmount__c != oi.ccrz__Price__c * oi.ccrz__Quantity__c || oi.ccrz__PercentDiscount__c != 0){
                        hasItemLineChanged = true;
                        oi.ccrz__AbsoluteDiscount__c = 0;
                        oi.ccrz__SubAmount__c = oi.ccrz__Price__c * oi.ccrz__Quantity__c;
                        oi.ccrz__PercentDiscount__c = 0;
                    }
                }
                else if(oi.ccrz__ProductType__c == 'Coupon'){
                    couponOrderItem = oi;
                }
                else if(oi.ccrz__ProductType__c == 'Dynamic Kit' || oi.ccrz__ProductType__c == 'Kit'){
                    kitOrderItem = oi;
                }
            }
        }

        if(orderType == 'P2 1st Order'){
            if(couponOrderItem != null){
                couponOrderItem.ccrz__SubAmount__c = orderValueDiscount * -1.0;
            }
            else if(orderValueDiscount > 0 && kitOrderItem != null){
                //Need to attach coupon record to the template order if it didn't previously exist
                List<ccrz__E_Product__c> p2CouponProductList = [SELECT Id FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'ProgramDiscount_P2'];
                if(p2CouponProductList.size() > 0){
                    ccrz__E_OrderItem__c kitCouponOrderItemRecord = new ccrz__E_OrderItem__c();
                    kitCouponOrderItemRecord.ccrz__Order__c = orderRecord.Id;
                    kitCouponOrderItemRecord.ccrz__Product__c = p2CouponProductList[0].Id;
                    kitCouponOrderItemRecord.ccrz__ProductType__c = 'Coupon';
                    kitCouponOrderItemRecord.ccrz__Quantity__c = 1;
                    kitCouponOrderItemRecord.ccrz__Price__c = 0;
                    kitCouponOrderItemRecord.ccrz__SubAmount__c = orderValueDiscount * -1.0;
                    kitCouponOrderItemRecord.ccrz__ParentOrderItem__c = kitOrderItem.Id;
                    kitCouponOrderItemRecord.ccrz__RequestDate__c = Date.today();
                    kitCouponOrderItemRecord.ccrz__OrderLineType__c = 'Minor';
                    kitCouponOrderItemRecord.ccrz__StoreId__c = 'DefaultStore';
                    insert kitCouponOrderItemRecord;
                }
            }

            if(kitOrderItem != null){
                kitOrderItem.ccrz__AbsoluteDiscount__c = orderValueDiscount;
                kitOrderItem.ccrz__PercentDiscount__c = DiscountAmount * 100.0;
            }
        }

        if(shipping != orderRecord.ccrz__ShipAmount__c || shipMethod != orderRecord.ccrz__ShipMethod__c || orderValueDiscount != orderRecord.Order_Value_Discount_Amount__c || discount != orderRecord.ccrz__TotalDiscount__c || hasItemLineChanged){
            orderRecord.ccrz__ShipAmount__c = shipping;
            orderRecord.ccrz__TotalDiscount__c = orderValueDiscount > 0 ? orderValueDiscount: discount;
            orderRecord.ccrz__ShipMethod__c = shipMethod;
            orderRecord.Order_Value_Discount_Amount__c = orderValueDiscount;

            //For P2 1st Order, we do not store/update the ccrz__TotalDiscount__c or Order_Value_Discount_Amount__c fields
            if(orderType == 'P2 1st Order'){
                orderRecord.ccrz__TotalDiscount__c = null;
                orderRecord.Order_Value_Discount_Amount__c = 0;
            }

            if(autoUpdate){
                update orderRecord;
                if(orderItemsList.size() > 0)
                    update orderItemsList;
            }
        }
    }
    public void processBusinessRules(){
        processBusinessRules(true);
    }
}