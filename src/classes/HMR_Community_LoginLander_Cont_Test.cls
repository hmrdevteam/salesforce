/*
Developer: Utkarsh Goswami (Mindtree)
Date: 5/24/2017
Target Class: HMR_Community_LoginLander_Controller 
*/

@isTest
public class HMR_Community_LoginLander_Cont_Test{  

        
    @isTest static void test_general_method() { 
    
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert userTest;
        
        Consent_Agreement__c caObj = new Consent_Agreement__c(Status__c = 'Notified', Contact__c = con.Id);
        insert caObj;
        
        System.runAs(userTest){

            HMR_Community_LoginLander_Controller landingController = new HMR_Community_LoginLander_Controller();
            landingController.checkConsent();
        
        }

    }
    
}