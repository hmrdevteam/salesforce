/**
* Apex Selector Class for Contact
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2018-01-05
* @Author: Utkarsh Goswami (Magnet 360)
* @Modified: Zach Engman 2018-01-10 - Changed to get by object api name
* @JIRA:
*/
 
@RestResource(urlMapping='/mobile/data/pickListValues/*')
global with sharing class HMRMobPicklist {
    /**
     * method to get picklist values of the provided object
     * @param Object API Name
     * @return Map of status and picklist values if values are retireved 
     * @return Map with status equals to false if there is any error OR no picklist values is retrieved 
     */
    @HttpGet
    global static void getPickListValues() {
        String objectApiName = RestContext.request.params.get('objectApiName');
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        
        try{
            Schema.SObjectType targetObjectType = Schema.getGlobalDescribe().get(objectApiName);
            Sobject objectName = targetObjectType.newSObject();
            Schema.sObjectType sObjectType = objectName.getSObjectType(); 
            Schema.DescribeSObjectResult sObjectDescribe = sObjectType.getDescribe(); 
            Map<String, Schema.SObjectField> objectFieldMap = sObjectDescribe.fields.getMap(); 

            for(string field : objectFieldMap.keySet()){
                Schema.DescribeFieldResult describeFieldResult = objectFieldMap.get(field).getDescribe(); 
                //Check for picklist fields
                if(describeFieldResult.getType() == Schema.DisplayType.Picklist){
                    List<Schema.PicklistEntry> picklistEntryList = describeFieldResult.getPickListValues();  
                    List<String> picklistValueList=new List<String>();

                    for (Schema.PicklistEntry pe : picklistEntryList) 
                        picklistValueList.add(pe.getValue());

                    returnMap.put(describeFieldResult.getName(), picklistValueList);
                }
            }
            
            response.statusCode = 200;

            returnMap.put('success', true);
        }
        catch(Exception ex){
            response.statusCode = 400;

            returnMap.put('success', false);
            returnMap.put('picklistValues', 'Exception ->  ' + ex.getMessage() + '  At Line Number  ->  ' + ex.getStackTraceString());
        
        }
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  
    } 
    
    
}