/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/23/2017
 * Created By: Zach Engman
 * Last Modified Date:05/23/2017
 * Last Modified By:
 * Description: Handler for the Contact object triggers
 * ****************************************************/
public without sharing class HMR_Contact_Handler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;
    public static Boolean isBeforeUpdateFlag = false;
    public static Boolean isAfterUpdateFlag = false;
    
    public HMR_Contact_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }
    
    public void onBeforeInsert(List<Contact> contactList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;
            //Standardize/Format the Field Data
            contactList = standardizeFieldFormat(contactList);
        }
    }

    public void onBeforeUpdate(List<Contact> oldContactList, List<Contact> newContactList, Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap){
        if(!isBeforeUpdateFlag)
            isBeforeUpdateFlag = true;
        
        //Standardize/Format the Field Data
        newContactList = standardizeFieldFormat(newContactList);
    }

    public void onAfterUpdate(List<Contact> oldContactList, List<Contact> newContactList, Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap){
        if(!isAfterUpdateFlag)
            isAfterUpdateFlag = true;

        //Check for email updates
        Map<Id, Contact> emailUpdateMap = new Map<Id, Contact>();
        for(Contact newContact : newContactMap.values()){
            if(!String.isBlank(newContact.Community_User_ID__c)){
                string newEmail = newContact.Email;
                string oldEmail = oldContactMap.get(newContact.Id).Email;

                if(!String.isBlank(newEmail) && oldEmail != newEmail)
                    emailUpdateMap.put(newContact.Community_User_ID__c, newContact);
            }
        }

        if(emailUpdateMap.size() > 0)
            updateRelatedEmailData(emailUpdateMap, oldContactMap);
    }


    private void updateRelatedEmailData(Map<Id, Contact> emailUpdateMap, Map<Id, Contact> oldContactMap){
        List<User> userUpdateList = new List<User>();
        //Usernames need to be updated in a future context, since it only accepts primitives storing as such
        Map<Id, String> usernameUpdateMap = new Map<Id, String>();
        //Check for duplicate usernames/emails
        Set<String> emailSet = new Set<String>();
        Map<String, User> emailMap = new Map<String, User>();

        for(Contact contactRecord : emailUpdateMap.values())
            emailSet.add(contactRecord.Email);

        //Build duplicate map, to be safe checking email, username and community nickname
        for(User userRecord : [SELECT Email, Username, CommunityNickname FROM User WHERE Username IN: emailSet OR Email IN: emailSet or CommunityNickname IN: emailSet]){
            if(!emailMap.containsKey(userRecord.Email))
                emailMap.put(userRecord.Email, userRecord);
            if(!emailMap.containsKey(userRecord.Username))
                emailMap.put(userRecord.Username, userRecord);
            if(!emailMap.containsKey(userRecord.CommunityNickname))
                emailMap.put(userRecord.CommunityNickname, userRecord);
        }

        //Get the user data
        for(User userRecord : [SELECT ContactId, Email, Username, CommunityNickname FROM User WHERE Id IN: emailUpdateMap.keySet()]){
            //Get the old record to know what to update (i.e. if the fields matched the old email)
            Contact newContactRecord = emailUpdateMap.get(userRecord.Id);
            Contact oldContactRecord = oldContactMap.get(userRecord.ContactId);
            if(newContactRecord != null && oldContactRecord != null){
                if(emailMap.get(newContactRecord.Email) == null){
                    string oldEmail = oldContactRecord.Email;
                    userRecord.Email = newContactRecord.Email;
                    
                    if(userRecord.CommunityNickname == oldEmail)
                        userRecord.CommunityNickname = newContactRecord.Email;

                    if(userRecord.UserName == oldEmail)
                        usernameUpdateMap.put(userRecord.Id, newContactRecord.Email);

                    userUpdateList.add(userRecord);
                }
                else
                    throw new DuplicateEmailException('Email Already in Use: ' + newContactRecord.Email);
            }
            
        }

        if(userUpdateList.size() > 0)
            update userUpdateList;

        if(usernameUpdateMap.size() > 0)
            updateUsernames(usernameUpdateMap);
    }

    @future
    private static void updateUsernames(Map<Id, String> usernameUpdateMap){
        List<User> userUpdateList = new List<User>();

        for(Id userId : usernameUpdateMap.keySet())
            userUpdateList.add(new User(Id = userId, Username = usernameUpdateMap.get(userId)));
        
        if(userUpdateList.size() > 0)
            update userUpdateList;
    }

    private List<Contact> standardizeFieldFormat(List<Contact> contactList){
        for(Contact contactRecord : contactList){
            contactRecord.FirstName = HMR_Format_Utility.capitalize(contactRecord.FirstName);
            contactRecord.LastName = HMR_Format_Utility.capitalize(contactRecord.LastName);
        }

        return contactList;
    }

    public class DuplicateEmailException extends Exception {}
}