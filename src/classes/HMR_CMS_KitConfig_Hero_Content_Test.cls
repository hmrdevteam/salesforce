/*
Developer: Ali Pierre (HMR)
Date: 10/18/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_KitConfig_Hero_ContentTemplate
*/
@isTest(SeeAllData=false) 
private class HMR_CMS_KitConfig_Hero_Content_Test {
	@isTest static void test_method_one() {
	    // Implement test codeCMS
	    HMR_CMS_KitConfig_Hero_ContentTemplate kitConfigHeroTemplate = new HMR_CMS_KitConfig_Hero_ContentTemplate();
	    String propertyValue = kitConfigHeroTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    map<String, String> kitDetails = new Map<String, String>();
	    kitDetails.put('kitName','HSS Test Kit');
	    kitDetails.put('kitHeading','Build Your Test Kit');
	    kitDetails.put('heroImageURL','https://www.Test.com/HealthyShakes.png');
	    kitDetails.put('detailsLine1','<span>36</span> servings HMR Test Shakes (choose your flavor)');
	    kitDetails.put('detailsLine2','<span>18</span> servings of HMR Test Shakes (choose your flavor)');
	    kitConfigHeroTemplate.testAttributes = kitDetails;

	    string s2 = kitConfigHeroTemplate.getPropertyWithDefault('kitName','testValue2');
	    string s1 = kitConfigHeroTemplate.getPropertyWithDefault('kitHeading','testValue2');
	    system.assertEquals(s2, 'HSS Test Kit');

	    String renderHTML = kitConfigHeroTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_Hero_ContentTemplate kh = new HMR_CMS_KitConfig_Hero_ContentTemplate(null);
	    }catch(Exception e){}
  	}

	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_KitConfig_Hero_ContentTemplate kitConfigHeroTemplate = new HMR_CMS_KitConfig_Hero_ContentTemplate();
	    String propertyValue = kitConfigHeroTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    map<String, String> kitDetails = new Map<String, String>();
	    kitDetails.put('kitName','HSAH Test Kit');
	    kitDetails.put('kitHeading','Build Your Test Kit');
	    kitDetails.put('heroImageURL','https://www.Test.com/HealthySolutions.png');
	    kitDetails.put('detailsLine1','<span>36</span> servings HMR Test Shakes (choose your flavor)');
	    kitDetails.put('detailsLine2','<span>18</span> servings of HMR Test Shakes (choose your flavor)');
	    kitDetails.put('detailsLine3','Test line 3');
	    kitDetails.put('detailsLine4','Test line 4');
	    kitDetails.put('detailsLine5','Test line 5');
	    kitConfigHeroTemplate.testAttributes = kitDetails;

	    string s2 = kitConfigHeroTemplate.getPropertyWithDefault('kitName','testValue2');
	    string s1 = kitConfigHeroTemplate.getPropertyWithDefault('kitHeading','testValue2');
	    system.assertEquals(s2, 'HSAH Test Kit');

	    String renderHTML = kitConfigHeroTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_Hero_ContentTemplate kh = new HMR_CMS_KitConfig_Hero_ContentTemplate(null);
	    }catch(Exception e){}
  	}
}