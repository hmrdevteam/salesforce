/**
* Trigger Handler for CreateSessionAttendees.trigger
* Managing (Creating, Deleting) Coaching Session records based on class records
*
* Objects referenced --> Class_Member__c,Class__c, Coaching_Session__c, Contact, Session_Attendee__c
*
* @Date: 2016-12-05
* @Author Aslesha Kokate (Mindtree)
* @Modified: Pranay Mistry on 2016-12-05
* @JIRA: Issue https://reside.jira.com/browse/HPRP-189, Task https://reside.jira.com/browse/HPRP-676
*/

public class CreateSessionAttendeesTriggerHandler {
    //Start of Pranay's changes
    /**
    *  ProcessClassMemberAndSessionAttendeeRecords
    *  static function to process class member, coaching session, their parent class, related Contact
    *  and related session attendee records
    *  only called when there is a change in the class member Active status and when
    *  a new class member record is created. called from handler method
    *
    *  @param Class member record --> the newly created or updated class member record with change in Active Status
    *  @param Sessions Attendees Map --> any existing session attendee records related to the updated class member record
    *  @param Sessions Attendees List to insert --> invoked in the handler method
    *  @return void
    */
    public static void ProcessClassMemberAndSessionAttendeeRecords(Class_Member__c newUpdatedClassMember, Map<Id, Session_Attendee__c> existingSessionAttendees,
                                                                    List<Session_Attendee__c> sessionAttendeesListToInsert, Map<Id, Coaching_Session__c> classCoachingSessions,
                                                                    Map<Id, Class__c> parentClasses, Map<Id, Contact> classMemberContacts) {
        Date now = Date.today();
        //check for if the class member record is made inactive

        if(newUpdatedClassMember.hmr_Active__c == false){
            //check if there are any related session attendee records
            if(!existingSessionAttendees.values().isEmpty()) {
                //loop through the related session attendee records
                for(Session_Attendee__c futureSessionAttendeeToDelete: existingSessionAttendees.values()) {
                    //compare the session Attendee date with today
                    if(futureSessionAttendeeToDelete.hmr_Class_Date__c < now || (futureSessionAttendeeToDelete.hmr_Class_Date__c >= now && futureSessionAttendeeToDelete.hmr_Attendance__c != null)) {
                        //remove past session attendee records from the map that will be deleted
                        existingSessionAttendees.remove(futureSessionAttendeeToDelete.Id);
                    }
                }
            }
        }
        else {
            //this will run if the Class Member record is active, inserted or updated
            //loop through all the Coaching Session Records that are related to the parent class
            for(Coaching_Session__c classCoachingSession: classCoachingSessions.values()) {
                //both the coaching session and class member record should belong to the same class, hence the validation
                if(classCoachingSession.hmr_Class__c == newUpdatedClassMember.hmr_Class__c){
                    //the class member start date should be less than the coaching session class date, only then
                    //the user should have a session attendee record
                    if(classCoachingSession.hmr_Class_Date__c >= newUpdatedClassMember.hmr_Start_Date__c) {
                        //get the client contact record to check for the Consent Form Signed
                        //the contact record is feteched from the Contact Map, and from class member record
                        Contact classMemberContact = classMemberContacts.get(newUpdatedClassMember.hmr_Contact_Name__c);
                        //check if contact is not null, this will again validate if the consent form was signed by the client
                        //only then the contact cannot be null
                        if(classMemberContact != null) {
                            //initialize the session attendee sObject
                            Session_Attendee__c sessionAttendeeToInsert = new Session_Attendee__c(
                                hmr_Coaching_Session__c = classCoachingSession.Id,
                                hmr_Class_Member__c = newUpdatedClassMember.Id,
                                hmr_Client_Name__c = classMemberContact.Id
                            );
                            //add session attendee sObject to Session Attendees List to Insert
                            sessionAttendeesListToInsert.add(sessionAttendeeToInsert);
                        }
                    }
                }
            }
        }
    }


    /**
    *  handleAfterInsert
    *  Trigger Handler method for CreateSessionAttendees.trigger
    *  only called for After trigger, after class member record is created or updated
    *
    *  @param Trigger.new --> List of new or updated Class member records
    *  @param Trigger.OldMap --> List of old version of the class member records
    *  @return void
    */
    public static void handleAfterInsert(List<Class_Member__c> newUpdatedClassMembers, Map<Id, Class_Member__c> oldClasMemberVersionsMap) {

        //initialize all the Set of Id's

        //class member Id's, used to get existing session attendees map
        Set<Id> classMemberIds = new Set<Id>();
        //class Id's, used to get parent class records map
        Set<Id> parentClassIds = new Set<Id>();
        //contact Id's, used to get class member contacts map
        Set<Id> classMemberContactIds = new Set<Id>();

        //loop through Trigger.new of Class member records
        for(Class_Member__c newUpdatedClassMember: newUpdatedClassMembers) {
            classMemberIds.add(newUpdatedClassMember.Id);
            //only set parent class and contact id's if the class membership record is active
            if(newUpdatedClassMember.hmr_Active__c == true && newUpdatedClassMember.hmr_Consented__c == true) {
                parentClassIds.add(newUpdatedClassMember.hmr_Class__c);
                classMemberContactIds.add(newUpdatedClassMember.hmr_Contact_Name__c);
            }
        }

        //Map for existing Session Attendees records related to class member records, will be used in delete logic
        Map<Id, Session_Attendee__c> existingSessionAttendees = new Map<Id, Session_Attendee__c>(
            [SELECT Id, hmr_Coaching_Session__c, hmr_Class_Member__c, hmr_Client_Name__c, hmr_Class_Date__c, hmr_Attendance__c FROM Session_Attendee__c
                WHERE hmr_Class_Member__c In :classMemberIds AND hmr_Class_Date__c >= TODAY]);

        //Map for Parent Class record, used to get the coaching session records
        Map<Id, Class__c> parentClasses = new Map<Id, Class__c>(
            [SELECT Id, Name, hmr_Class_Name__c, hmr_Active__c, hmr_Start_Time__c, hmr_End_Time__c, hmr_Coach__c FROM Class__c
                WHERE Id In :parentClassIds AND hmr_Active__c = TRUE]);

        //Map for Class Member Contact records, to verify if the client has signed the consent
        Map<Id, Contact> classMemberContacts = new Map<Id, Contact>(
            [SELECT Id, Name, hmr_Consent_Form__c FROM Contact WHERE Id In :classMemberContactIds
                AND hmr_Consent_Form__c = TRUE]);

        //initalize the Coaching Session Set of Id's
        Set<Id> classCoachingSessionsParentIds = new Set<Id>();

        //loop through parent class records
        for(Class__c parentClass: parentClasses.values()) {
            //set the coaching session Id's based on parent class records
            classCoachingSessionsParentIds.add(parentClass.Id);
        }

        //Map for Coaching Session Records, the class date of which will be used in session attendee record
        Map<Id, Coaching_Session__c> classCoachingSessions = new Map<Id, Coaching_Session__c>(
            [SELECT Id, Name, hmr_Class_Date__c, hmr_Class__c FROM Coaching_Session__c
                WHERE hmr_Class__c In :classCoachingSessionsParentIds AND hmr_Class_Date__c >= TODAY]);

        //initialize the session attendee list to insert
        List<Session_Attendee__c> sessionAttendeesListToInsert = new List<Session_Attendee__c>();

        //loop through all the class member records
        for(Class_Member__c newUpdatedClassMember: newUpdatedClassMembers) {
            //check if the Trigger is an insert or an update trigger
            //by checking for old Map not null and have values
            if(oldClasMemberVersionsMap != null && !oldClasMemberVersionsMap.values().isEmpty()) {
                //get the older version of the Class member record
                Class_Member__c getOldVersionOfClassMember = oldClasMemberVersionsMap.get(newUpdatedClassMember.Id);
                //check if there is an update or change in any other field other than Active field in class member record
                if(getOldVersionOfClassMember.hmr_Active__c == newUpdatedClassMember.hmr_Active__c &&
                    getOldVersionOfClassMember.hmr_Consented__c == newUpdatedClassMember.hmr_Consented__c) {
                    /**
                    * Start of Mustafa Changes 5/30/2017
                    * @JIRA Issue: https://reside.jira.com/browse/HPRP-3646
                    */                  
                    //If start date has been changed, use new Class Member logic to create new session attendee records 
                    //based off the new Start Date field.
                    if(getOldVersionOfClassMember.hmr_Active__c == newUpdatedClassMember.hmr_Active__c &&
                        getOldVersionOfClassMember.hmr_Start_Date__c != newUpdatedClassMember.hmr_Start_Date__c){
                        //call method if there is a change in Start Date field
                        ProcessClassMemberAndSessionAttendeeRecords(newUpdatedClassMember, existingSessionAttendees, sessionAttendeesListToInsert, classCoachingSessions, parentClasses, classMemberContacts);
                    }
                    else{
                        //if the active flag is the same & Start Date has not been changed then clear out the existing Session attendees Map
                        //the below logic will prevent the session attendees from getting deleted in line 185 & 186
                        //if there is a change in any other field other than the Active field
                        for(Session_Attendee__c existingSessionAttendee: existingSessionAttendees.values()) {
                            //need to remove the existing session attendee record from the map, which will be deleted
                            //hence the map will not contain records if there is change in any other field
                            if(existingSessionAttendee.hmr_Class_Member__c == newUpdatedClassMember.Id) {
                                existingSessionAttendees.remove(existingSessionAttendee.Id);
                            }
                        }
                    }                    
                    //*End of Mustafa Changes                    
                }
                else {
                    //call method if there is a change in the Active status field
                    ProcessClassMemberAndSessionAttendeeRecords(newUpdatedClassMember, existingSessionAttendees, sessionAttendeesListToInsert, classCoachingSessions, parentClasses, classMemberContacts);
                }
            }
            else {
                //call method if a new class member record is inserted
                ProcessClassMemberAndSessionAttendeeRecords(newUpdatedClassMember, existingSessionAttendees, sessionAttendeesListToInsert, classCoachingSessions, parentClasses, classMemberContacts);
            }
        }
        try {
            //DML insert on the Session Attendees List
            if(!sessionAttendeesListToInsert.isEmpty())
                insert sessionAttendeesListToInsert;

            //DML delete if a class member is deactivated and if there are any future
            //related session attendee records in the existing session attendees Map
            if(!existingSessionAttendees.values().isEmpty())
                delete existingSessionAttendees.values();
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
    //End of Pranay's changes
}