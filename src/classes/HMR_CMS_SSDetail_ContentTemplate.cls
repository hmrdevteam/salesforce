/**
* Apex Content Template Controller for the Success Stories Detail Component
*
* @Date: 07/31/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 07/31/2017
* @JIRA: HPRP-4014
*/
global virtual  class HMR_CMS_SSDetail_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SSDetail_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SSDetail_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    @TestVisible
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String SuccessStory {
        get{
            return getPropertyWithDefault('SuccessStory', '');
        }
        set;
    }

    global virtual override String getHTML(){
    	String html = '';
 
    	if(!String.isBlank(SuccessStory)){
    		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    		HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(SuccessStory);
    		
    		if(articleRecord != null){
    			html = '<div class="success-detail">' +
					      '<div class="container">' +
					        '<div class="row">' +
					          '<div class="col-xs-12">' +
					            '<div class="col-xs-12 col-sm-7">' +
					              '<div class="min-header">' +
					                'SUCCESS STORIES' +
					              '</div>' +
					              '<div class="header">' +
					                articleRecord.ShortTeaser +
					              '</div>' +
					              '<div class="quote">' +
					                articleRecord.LongTeaser +
					              '</div>' +
					              '<div class="bio hidden-xs">' +
					                articleRecord.StoryBody +
					              '</div>' +
					            '</div>' +
					            '<div class="col-xs-12 col-sm-5 blocks text-center">';
					            if(!String.isBlank(articleRecord.BeforePhoto) && !String.isBlank(articleRecord.AfterPhoto)){
						              html += '<div class="block">' +
								                  '<img class="img-responsive" src="' + articleRecord.BeforePhoto + '" />' +
								                '<div class="name">' +
								                  'BEFORE' +
								                '</div>' +
								              '</div>' +
								              '<div class="block">' +
								                  '<img class="img-responsive" src="' + articleRecord.AfterPhoto + '" />' +
								                '<div class="name">' +
								                  'AFTER' +
								                '</div>' +
								              '</div>';
					          	}
					   html += '</div>' +
							        '<div class="col-xs-12 bio visible-xs-block">' +
							            articleRecord.StoryBody +
							        '</div>' +
							    '</div>' +
							'</div>' +
						'</div>';
    		}
    			   
    	}
    	
    	return html;
    }
}