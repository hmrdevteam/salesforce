/**
* Apex Content Template Controller for Recipe Home Hero Content
*
* @Date: 2017-03-26
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-03-26
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_RecipeHome_Hero_ContentTemplate extends cms.ContentTemplateController{
	//Constructors (With and Without Parameters)
	global HMR_CMS_RecipeHome_Hero_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }

    global HMR_CMS_RecipeHome_Hero_ContentTemplate() {
        super();
    }
    
    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    @TestVisible
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }
    
    //Hero Image Properties
    public String heroImage{
        get{
            return getProperty('heroImage');
        }
    }
    
    public String heroImageMobile{
        get{
            return getProperty('heroImageMobile');
        }
    }
    
    global virtual override String getHTML() { 
        String sitePrefix = Site.getBaseUrl();
        String html = '';
        
        html += '<div class="new-hero-layout">';
			html += '<div class="img-container">';
        		html += String.format('<img class="bg-img" src="{0}{1}">', new String[]{sitePrefix, heroImage});
        	html += '</div>';
        	html += '<div class="container">';
            	html += '<div class="col-sm-12">';
                	html += '<div class="mini-title">RECIPES</div>';
                	html += '<div class="title">Deliciously Filling</div>';
            	html += '</div>';
        	html += '</div>';
        
        	html += String.format('<img class="mini-hero-img" src="{0}{1}">', new String[] {sitePrefix, heroImageMobile});
        html += '</div>';
        
        //Dropdown Hero Content
        html += '<div class="container dropdown-hero-content">';
            html += '<div class="col-xs-12">';
                html += '<div class="col-xs-12">';
                    html += '<div class="mini-title">RECIPES</div>';
                    html += '<div class="title">Deliciously Filling</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';

        return html;
    }
}