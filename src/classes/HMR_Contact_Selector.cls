/**
* Apex Selector Class for Contact
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2018-01-05
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-01-05
* @JIRA:
*/
public class HMR_Contact_Selector {
	public HMR_Contact_Selector() {}

	public Contact getProfileByCommunityUserId(string communityUserId){
		List<Contact> contactRecordList = [SELECT Name
												 ,Email
												 ,External_Coach_Conference_Number__c
												 ,External_Coach_Email__c
												 ,External_Coach_Name__c
												 ,External_Coach_Participant_Code__c
												 ,FirstName
												 ,LastName
												 ,Community_User_ID__c
												 ,hmr_Date_Of_Birth__c
												 ,hmr_Gender__c
												 ,hmr_Height_Feet__c
												 ,hmr_Height_Inches__c
												 ,hmr_Current_Weight__c
                                                 ,hmr_Goal_Weight__c
												 ,DailyTipViewedDate__c
												 ,IsMigratedUser__c
												 ,MobileTutorialComplete__c
                                           		 ,ProfileBuildProcessComplete__c 
												 ,(SELECT Days_in_Program__c
												         ,DietType__c
												 		 ,hmr_Day_of_Week__c 
												 		 ,hmr_Diet_Start_Date__c
												 		 ,hmr_Enrollment_Date__c
													   	 ,hmr_Program_Membership_Day__c
													   	 ,hmr_Membership_Type__c 
												 		 ,InitialWeight__c
													     ,Motivation__c
													     ,Program__r.Name
													     ,Program__r.RemoteProgram__c
													     ,SupportNeeded__c
													     ,WeightLossGoal__c            			 
									             		FROM Program_Memberships__r WHERE hmr_Status__c = 'Active')
											FROM Contact WHERE Community_User_ID__c =: communityUserId];

        return contactRecordList.size() > 0 ? contactRecordList[0] : null;
	}
}