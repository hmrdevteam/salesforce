/**
* CloudCraze + Cybersource Request Form Controller for CSR Flow
*
* @Date: 2017-03-26
* @Author Pranay Mistry (Magnet 360)
* @Modified: 
* @JIRA: 
*/

global with sharing class HMR_CC_Cybersource_Request_CSR_Ctrl {
	
	public List<String> signedFields { get; set; }
    public List<String> unsignedFields { get; set; }
    public String signed { get; set; }
    public String unsigned { get; set; }
    public String formAction { get; set; }
    public String selector { get; set; }
    
    
    public HMR_CC_Cybersource_Request_CSR_Ctrl() {
        signedFields = new List<String>();
        if (System.currentPageReference().getParameters().get('joinedSignedFields') != null) {
            signed = System.currentPageReference().getParameters().get('joinedSignedFields').escapeHtml4();
            for(String s : signed.split(',')) signedFields.add(s);
        }
        unsignedFields = new List<String>();
        if (System.currentPageReference().getParameters().get('joinedUnsignedFields') != null) {
            unsigned = System.currentPageReference().getParameters().get('joinedUnsignedFields').escapeHtml4();
            for(String u : unsigned.split(',')) unsignedFields.add(u);
        }
    }
}