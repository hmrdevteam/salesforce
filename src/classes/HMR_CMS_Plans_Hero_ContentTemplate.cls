/**
* Apex Content Template Controller for Hero Sections on Plans Page
*
* @Date: 03/20/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Joey Zhuang 03/20/2017
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_Plans_Hero_ContentTemplate extends cms.ContentTemplateController{
    
    global HMR_CMS_Plans_Hero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
    
    global HMR_CMS_Plans_Hero_ContentTemplate(){
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }
    
    //Title Label Property
    public String titleText{    
        get{
            return getPropertyWithDefault('titleText', 'SOMETHING FOR EVERYONE');
        }    
    }

    //SubTitle Label Property
    public String subTitleText{    
        get{
            return getPropertyWithDefault('subTitleText', 'Choose a Plan');
        }    
    }

    //Hero Background Image Property
    public String backgroundImage{
        get{
            return getProperty('backgroundImage');
        }
    }

    //Hero Mobile Background Image Property
    public String backgroundImageMobile{
        get{
            return getProperty('backgroundImageMobile');
        }
    }

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        String sitePrefix = Site.getBaseUrl();
        html += '<section id="Menu" class="clearfix common-height" style= "display:none;">'+
            '<img src="'+sitePrefix+backgroundImageMobile+'" class="full-width"/>'+
            '<div class="container">'+
                '<div class="row mobile_row">'+
                    '<h6 class="mobile-first">SOMETHING FOR EVERYONE</h5>'+
                    '<h3 class="margin-top-0 mobile-second">Choose a Plan</h5>'+
                '</div>'+
            '</div>'+    
        '</section>';
        html += '<section id="web" class="hero clearfix plans" style= "display:none; background-image : url(\'' + sitePrefix + backgroundImage + '\')">'+
                    '<div class="row tabs-row">'+
                        '<div class="col-sm-1">'+
                        '</div>'+
                        '<div class="col-sm-10">'+
                            '<ul id="PlansTabs" class="tabrow">'+
                            '</ul>'+
                        '</div>'+
                        '<div class="col-sm-1">'+
                        '</div>'+
                    '</div>'+
                    '<div class="hero-content hero-content-offset" >'+
                        '<h6 class="h6">'+titleText+'</h6>'+
                        '<h3 class="h3 margin-top-15">'+subTitleText+'</h3>'+
                    '</div>'+
                '</section>';
        return html;
    }
}