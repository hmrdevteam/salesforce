/**
* Rest service to read/update customer profile information
* @Date: 2018-01-05
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2020-10-19 
* @JIRA: HMRAPP-121, HMRAPP-219
*/
@RestResource(urlMapping='/mobile/user/profile/*')
global without sharing class HMRMobProfile {
    
    @HttpPost
    global static void doPost(MobProfileDTO prof) {
        Savepoint sp = Database.setSavepoint();
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        
        Mobile_Service_Record__c serviceRecord = HMR_MobileServiceRecord_Utility.logRequest('HMRMobProfile', 'Post', RestContext.request.params, prof);

        try { 
            Contact contactRecord = new Contact(Id = prof.Id,
                                                hmr_Date_Of_Birth__c = prof.birthDate,
                                                hmr_Gender__c = prof.gender,
                                                hmr_Height_Feet__c = prof.heightFeet,
                                                hmr_Height_Inches__c = prof.heightInches,
                                                hmr_Current_Weight__c = prof.current_Weight,
                                                ProfileBuildProcessComplete__c = true,
												ProfileBuildProcessCompleteDate__c = System.today()); 

            if(!String.isBlank(prof.appVersion)){
              contactRecord.Diet_Tracker_App_Version__c = prof.appVersion;
            }

            if(!String.isBlank(prof.timeZone)){
                contactRecord.Diet_Tracker_Time_Zone__c = prof.timeZone;
            }

            update contactRecord;

            //Check if active program membership already exists
            List<Program_Membership__c> programMembershipList = [SELECT Id FROM Program_Membership__c WHERE hmr_Contact__c =: contactRecord.Id AND hmr_Status__c = 'Active'];

            if(programMembershipList.size() <= 0){
              Program_Membership__c programMembership = prof.mapToPM(false);
              programMembership.hmr_Contact__c = contactRecord.Id;
              programMembership.hmr_Status__c = 'Active';

              //Default to digital if program is not passed
              if(String.isBlank(programMembership.Program__c)){
                List<Program__c> programList = [SELECT Id FROM Program__c WHERE Name = 'Digital'];
                programMembership.Program__c = programList[0].Id;
                programMembership.Discount_Rule_Type__c = Constants.DefaultDiscountRuleType;
              }
              insert programMembership;

              response.statusCode = 201;
              
              resultMap.put('profile', getProfile(UserInfo.getUserId()));
              resultMap.put('userId', UserInfo.getUserId());
              resultMap.put('success', true);
            }
            else{
               response.statusCode = 500;

               resultMap.put('success', false);
               resultMap.put('errorCode', 'PROFILE_EXISTS');
               resultMap.put('error', 'An active program membership already exists for the user');
            }  
        } 
        catch (Exception e) {
            response.statusCode = 500;
            resultMap.put('success', false);
            resultMap.put('error', e.getMessage());

            Database.rollback(sp);
        }

        HMR_MobileServiceRecord_Utility.logResponse(serviceRecord, Boolean.valueOf(resultMap.get('success')), response.statusCode, resultMap);

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * update the the Contact and Program Membership sObjects
     * @param MobProfileDTO
     * @see MobProfileDTO
     */
    @HttpPatch
    global static void updateProfile(MobProfileDTO prof) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        List<SObject> objs = new List<SObject>();

        Mobile_Service_Record__c serviceRecord = HMR_MobileServiceRecord_Utility.logRequest('HMRMobProfile', 'Patch', RestContext.request.params, prof);

        try {
            Contact contactRecord = prof.mapToContact();
            //Be sure the profile build process complete flag is checked for remote users who may already have profile stub
            contactRecord.ProfileBuildProcessComplete__c = true;
            contactRecord.ProfileBuildProcessCompleteDate__c = System.today(); 
            objs.add(contactRecord);
            objs.add(prof.mapToPM(true));
            update objs;

            RestContext.response.statuscode = 204;

            resultMap.put('success', true);
        }
        catch(Exception e) {
            RestContext.response.statuscode = 404;
            String exceptionMessage = e.getMessage();

            exceptionMessage = exceptionMessage.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ' , ': [');

            resultMap.put('success', false);
            resultMap.put('error', exceptionMessage);
        }  

        HMR_MobileServiceRecord_Utility.logResponse(serviceRecord, Boolean.valueOf(resultMap.get('success')), response.statusCode, resultMap);

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));     
    }
    
    /**
     * method to get the customer's profile information
     * @param communityUserId - the id to match in the contact sObject (note: it's a request parameter)
     * @return MobProfileDTO - the DTO containing all the profile fields (flat structure) (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if the contact is not found
     */
    @HttpGet
    global static MobProfileDTO doGet() {
        MobProfileDTO profileRecord;
        RestRequest request = RestContext.request;
        String communityUserId = RestContext.request.params.get('communityUserId');
        HMR_Contact_Selector contactSelector = new HMR_Contact_Selector();
        Contact contactRecord; 
        Mobile_Service_Record__c serviceRecord = HMR_MobileServiceRecord_Utility.logRequest('HMRMobProfile', 'Get', RestContext.request.params, null);

        if(!String.isBlank(communityUserId)){
          contactRecord = contactSelector.getProfileByCommunityUserId(communityUserId);
        }

        if (contactRecord != null) {
            RestContext.response.statuscode = 200;
            profileRecord = getProfile(contactRecord);
        }
        else {
            RestContext.response.statuscode = 404;
        }

        HMR_MobileServiceRecord_Utility.logResponse(serviceRecord, profileRecord != null, RestContext.response.statuscode, profileRecord);
        return profileRecord;
    }
    
    /**
     * this method gets the profile information for the specified community user
     * and can be re-used to aggregate data in other services
     * @see HMRMobInitialDataInfo
     */
    global static MobProfileDTO getProfile(String communityUserId) {
        HMR_Contact_Selector contactSelector = new HMR_Contact_Selector();
        Contact contactRecord = contactSelector.getProfileByCommunityUserId(communityUserId);
        
        return getProfile(contactRecord);
    } 

    /**
     * this method gets the profile information for the specified community user
     * and can be re-used to aggregate data in other services
     * @see HMRMobInitialDataInfo
     */
    global static MobProfileDTO getProfile(Contact contactRecord) {
        MobProfileDTO profileRecord = new MobProfileDTO();

        if (contactRecord != null) {
            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
            profileRecord.mapFrom(contactRecord);
             
            if (contactRecord.Program_Memberships__r.size() > 0){
                profileRecord.mapFrom(contactRecord.Program_Memberships__r[0]);
            }

            profileRecord.dietStartDate = programMembershipService.getDietStartDate(contactRecord.Id);
            profileRecord.minimumLogDate = programMembershipService.getMinDietDate(contactRecord.Id);
            
            if(!String.isBlank(contactRecord.Community_User_ID__c)){
              //Get the photo url
              User userRecord = [SELECT SmallPhotoUrl FROM User WHERE Id =: contactRecord.Community_User_ID__c];
              profileRecord.photoUrl = userRecord.SmallPhotoUrl;

              //check for upcoming/active bootcamp
              profileRecord.isBootcampUser = [SELECT COUNT() 
                                              FROM Class_Member__c 
                                              WHERE hmr_Contact_Name__r.Community_User_ID__c =: contactRecord.Community_User_ID__c 
                                                AND hmr_Class__r.RecordTypeId =: SObjectType.Class__c.getRecordTypeInfosByDeveloperName().get('Bootcamp_Group').getRecordTypeId()
                                                AND hmr_Class__r.hmr_End_Date__c >= TODAY] > 0;

              //Get coach data - for digital it is customizable and populated from the contact, here we check for group users
              Class_Member__c activeGroupMemberRecord = new HMR_GroupMember_Selector().getActiveByContactId(contactRecord.Id);

              profileRecord.isCoachEditable = activeGroupMemberRecord == null;
              //excluded group types, they can still set diet start date
              final Set<String> excludedGroupTypeSet = new Set<String>{'P1 Clinic', 'P2 Clinic'};
              profileRecord.showInitialDietStartDateSelection = contactRecord.Program_Memberships__r.size() > 0 ? contactRecord.Program_Memberships__r[0].Program__r.RemoteProgram__c && (activeGroupMemberRecord == null || excludedGroupTypeSet.contains(activeGroupMemberRecord.hmr_Class__r.hmr_Class_Type__c)) : false;

              if(activeGroupMemberRecord != null){
                profileRecord.coachName = activeGroupMemberRecord.hmr_Coach__r.Display_Name__c;
                profileRecord.coachEmail = activeGroupMemberRecord.hmr_Coach__r.hmr_Email__c;
                profileRecord.groupConferenceNumber = activeGroupMemberRecord.hmr_Class__r.hmr_Coference_Call_Number__c;
                profileRecord.groupCode = !String.isBlank(activeGroupMemberRecord.hmr_Class__r.hmr_Participant_Code__c) ? activeGroupMemberRecord.hmr_Class__r.hmr_Participant_Code__c + '#' : '';
 				profileRecord.groupName = activeGroupMemberRecord.hmr_Class__r.hmr_Class_Name__c;
                profileRecord.groupDay = activeGroupMemberRecord.hmr_Class__r.hmr_Class_Day__c;
                profileRecord.groupFirstDate = activeGroupMemberRecord.hmr_Class__r.hmr_First_Class_Date__c;
                profileRecord.groupActiveDate = activeGroupMemberRecord.hmr_Class__r.hmr_Active_Date__c;
                profileRecord.zoomLink = activeGroupMemberRecord.hmr_Class__r.hmr_Video_Link__c;
                //for shared services, show the clinic coach (if it has been assigned)
                if(contactRecord.Account.Clinic_Platform_License__c == 'Shared Services'){
                    List<Clinic_Coach_to_Group_Bridge__c> coachRecordList = [SELECT User__c, 
                                                                                    User__r.Name, 
                                                                                    User__r.Email 
                                                                             FROM Clinic_Coach_to_Group_Bridge__c 
                                                                             WHERE Group__c =: activeGroupMemberRecord.hmr_Class__c
                                                                                AND User__r.Contact.AccountId =: contactRecord.AccountId];
                    if(coachRecordList.size() > 0){
                       profileRecord.coachName = coachRecordList[0].User__r.Name;
                       profileRecord.coachEmail = coachRecordList[0].User__r.Email;
                    }
                }
              }
            }

            //Get the current weight for the member
            List<ActivityLog__c> weightLogList = [SELECT CurrentWeight__c 
                                                    FROM ActivityLog__c 
                                                    WHERE Contact__c =: contactRecord.Id 
                                                      AND CurrentWeight__c != null 
                                                    ORDER BY DateDataEnteredFor__c DESC LIMIT 1];

            if(weightLogList.size() > 0) {                                       
                profileRecord.currentWeight = weightLogList[0].CurrentWeight__c;
            }
            else{
                profileRecord.currentWeight = profileRecord.initialWeight;
            }

            //HMRAPP-222 - Initial Weight should be first weight ever entered (see comments in the story)
            weightLogList = [SELECT CurrentWeight__c 
                                    FROM ActivityLog__c 
                              WHERE Contact__c =: contactRecord.Id AND CurrentWeight__c != null 
                              ORDER BY DateDataEnteredFor__c LIMIT 1];

            if(weightLogList.size() > 0) {                                       
                profileRecord.initialWeight = weightLogList[0].CurrentWeight__c;
            }
        }
        return profileRecord;
    }   
    
    /**
     * global DTO class that represents a flat structure of the
     * Contact and Program Membership sObjects
     */
    global class MobProfileDTO {
        
        /**
         * constructor
         */
        public MobProfileDTO() {
          this.isBootcampUser = false;
        }
        
        /**
         * utility method to map the fields from Contact to the fields of this object
         * @param Contact
         */        
        private void mapFrom(Contact c) {
            this.id = c.Id;
            this.name = c.Name;
            this.email = c.Email;
            this.firstName = c.FirstName;
            this.lastName = c.LastName;
            this.birthDate = c.hmr_Date_Of_Birth__c;
            this.gender = c.hmr_Gender__c;
            this.heightFeet = Integer.valueOf(c.hmr_Height_Feet__c);
            this.heightInches = Integer.valueOf(c.hmr_Height_Inches__c);
            this.current_Weight = Integer.valueOf(c.hmr_Current_Weight__c);
            this.dailyTipViewedDate = c.DailyTipViewedDate__c;
            this.isMigratedUser = c.IsMigratedUser__c;
            this.hasAcceptedConditionsOfUse = false;
            this.hasAllowedNotifications = c.Allow_Mobile_Notifications__c;
            this.hasAllowedDailyTips = c.Allow_Daily_Tips__c;
            this.hasCompletedTutorial = c.MobileTutorialComplete__c;
            this.profileBuildProcessComplete = c.ProfileBuildProcessComplete__c;
            this.showWeightCelebrations = c.Show_Weight_Celebrations__c;
            this.shownWeightCelebrationCard = c.Shown_Weight_Celebration_Card__c;
            this.appVersion = c.Diet_Tracker_App_Version__c;
            this.timeZone = c.Diet_Tracker_Time_Zone__c;
            this.isInternalEmployee = c.Account.Name == Constants.EmployeesAccountName;
            
            this.coachName = c.External_Coach_Name__c;
            this.coachEmail = c.External_Coach_Email__c;
            this.groupCode = c.External_Coach_Participant_Code__c;
            this.groupConferenceNumber = c.External_Coach_Conference_Number__c;

            //check consent
            HMR_ConsentAgreement_Service consentService = new HMR_ConsentAgreement_Service(c.Id);
            this.hasAcceptedConditionsOfUse = !consentService.doesRequirePrivacyAndTermsAcceptance();

            //send as 0 instead of null
            if(this.heightFeet != null && this.heightInches == null){
              this.heightInches = 0;
            }
        }
        
        /**
         * utility method to map the fields from Program_Membership__c to the fields of this object
         * @param Program_Membership__c
         */
        private void mapFrom(Program_Membership__c m) {
            this.clientStartDate = m.Client_Start_Date__c;
            this.daysInProgram = m.Days_in_Program__c;
            this.dayOfWeek = m.hmr_Day_of_Week__c;    
            this.dietStartDate = m.hmr_Diet_Start_Date__c;
            this.dietType = m.DietType__c;
            this.enrollmentDate = m.hmr_Enrollment_Date__c;
            this.initialWeight = m.InitialWeight__c != null && m.InitialWeight__c > 0 ? m.InitialWeight__c : null;
            this.isRemoteUser = m.Program__r.RemoteProgram__c || (m.Program__r.Name == 'Clinic P1' || m.Program__r.Name == 'Clinic P2' || m.Program__r.Name == 'Shared Services P1' || m.Program__r.Name == 'Shared Services P2');   
            this.membershipType = m.hmr_Membership_Type__c;
            this.motivation = m.Motivation__c;
            this.programId = m.Program__c;
            this.programName = (m.Program__r.Name == 'Clinic P1' || m.Program__r.Name == 'Clinic P2' || m.Program__r.Name == 'Shared Services P1' || m.Program__r.Name == 'Shared Services P2') ? 'Digital' : m.Program__r.Name;
            this.programMembershipDay = m.hmr_Program_Membership_Day__c;
            this.programMembershipId = m.Id;
            this.supportNeeded = m.SupportNeeded__c;
            this.weightLossGoal = m.WeightLossGoal__c != null && m.WeightLossGoal__c >= 0 ? m.WeightLossGoal__c : null;
            this.currentWeekStartDate = m.Current_Diet_Week_Start_Date__c;
        }  

        public Date getWeekStartDate(Date weekDate){
            DateTime weekStartDateTime = DateTime.newInstance(weekDate, Time.newInstance(0,0,0,0));
            integer dayCount = 0;
            string startDayOfWeek = this.dayOfWeek;

            //derive the start day of week from the diet start date if it is blank (non-remote)
            if(this.clientStartDate != null){
                DateTime startDateTime = DateTime.newInstance(this.clientStartDate, Time.newInstance(0,0,0,0));
                startDayOfWeek = startDateTime.format('EEEE');
            }
           
            if(!String.isBlank(startDayOfWeek)){
                // If start day is set, which it always should be, enter loop
                while(weekStartDateTime.format('EEEE') != startDayOfWeek && dayCount <= 6){
                    dayCount++;
                    weekStartDateTime = DateTime.newInstance(weekDate.addDays(dayCount * -1), Time.newInstance(0,0,0,0));
                }
            }

            return weekStartDateTime.date();
        }

        /**
         * utility method to map the fields from this object to a Contact object
         * @return Contact
         */        
        private Contact mapToContact() {
            Contact contactRecord = new Contact(
                Id = this.Id
               //,Email = this.Email
               ,FirstName = this.firstName
               ,LastName = this.lastName
               ,hmr_Date_Of_Birth__c = this.birthDate
               ,hmr_Gender__c = this.gender
               ,hmr_Height_Feet__c = (Decimal)this.heightFeet
               ,hmr_Height_Inches__c = (Decimal)this.heightInches
               ,hmr_Current_Weight__c = (Integer)this.current_Weight
            );

            if(!String.isBlank(this.appVersion)){
              contactRecord.Diet_Tracker_App_Version__c = this.appVersion;
            }

            if(!String.isBlank(this.timeZone)){
               contactRecord.Diet_Tracker_Time_Zone__c = this.timeZone;
            }

            List<Class_Member__c> groupList = [SELECT hmr_Coach__r.hmr_Email__c,
                                                      hmr_Coach__r.hmr_Coach_First_Name__c,
                                                      hmr_Coach__r.hmr_Coach_Last_Name__c,
                                                      hmr_Class__r.hmr_Coference_Call_Number__c,
                                                      hmr_Class__r.hmr_Host_Code__c,
                                                      hmr_Class__r.hmr_Participant_Code__c
                                               FROM Class_Member__c
                                               WHERE hmr_Contact_Name__c =: this.Id 
                                                     AND hmr_Active__c = true];

            if(groupList.size() == 0){
              contactRecord.External_Coach_Email__c = this.coachEmail;
              contactRecord.External_Coach_Name__c = this.coachName;
              contactRecord.External_Coach_Conference_Number__c = this.groupConferenceNumber;
              contactRecord.External_Coach_Participant_Code__c = this.groupCode;
            }
            
            return contactRecord;
        }
        
        /**
         * utility method to map the fields from this object to a Program_Membership__c object
         * @return Program_Membership__c
         */
        private Program_Membership__c mapToPM(boolean isUpdate) {
            Program_Membership__c programMembershipRecord = new Program_Membership__c(
                Id = this.programMembershipId
               //,InitialWeight__c = this.initialWeight == null || this.initialWeight >= 0 ? this.initialWeight : null
               ,Motivation__c = this.motivation
               ,Program__c = this.programId
               ,SupportNeeded__c = this.supportNeeded
               ,WeightLossGoal__c =this.weightLossGoal
            );

            //for updates of diet start date and/or day, we need to validate against specific business rules
            if(!isUpdate){
              programMembershipRecord.hmr_Diet_Start_Date__c = this.dietStartDate;
              programMembershipRecord.hmr_Day_of_Week__c = this.dayOfWeek;
            }
            else if(!String.isBlank(this.programMembershipId)){
              //retrieve the program membership record to be sure we are validating against the right data
              List<Program_Membership__c> programMembershipList = [SELECT DietType__c,
                                                                          hmr_Diet_Start_Date__c,
                                                                          hmr_Membership_Type__c,
                                                                          Program__r.Name,
                                                                          Program__r.RemoteProgram__c
                                                                    FROM Program_Membership__c
                                                                    WHERE Id =: this.programMembershipId];
              if(programMembershipList.size() > 0){
                Program_Membership__c serverProgramMembershipRecord = programMembershipList[0];

                //digital/non-remote users cannot edit/change their start date
                if(serverProgramMembershipRecord.Program__r.RemoteProgram__c){
                    //start date is editable up to the end of the existing start date
                    if((serverProgramMembershipRecord.hmr_Diet_Start_Date__c == null || Date.today() <= serverProgramMembershipRecord.hmr_Diet_Start_Date__c) && Date.today() <= this.dietStartDate){
                       programMembershipRecord.hmr_Diet_Start_Date__c = this.dietStartDate;
                    }

                    //self-directed users can edit their start day at any time while phone/coaching users cannot
                    if(serverProgramMembershipRecord.hmr_Membership_Type__c == 'Self-directed'){
                      programMembershipRecord.hmr_Day_of_Week__c = this.dayOfWeek;
                    }
                }
                else{
                  //digital/non-remote users can never change their start date, but can always change their start day
                  programMembershipRecord.hmr_Day_of_Week__c = this.dayOfWeek;
                }
              }
            }

            if(!String.isBlank(this.dietType)){
                programMembershipRecord.DietType__c = this.dietType;
            }
            
            return programMembershipRecord;
        }
        
        // Contact fields
        public Id id {get; set;}
        public String name {get; set;}
        public String email {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        public Date birthDate {get; set;}
        public String gender {get; set;}
        public Integer heightFeet {get; set;}
        public Integer heightInches {get; set;}
        public Decimal current_Weight {get; set;}
        public Date dailyTipViewedDate {get; private set;}
        public boolean isInternalEmployee {get; private set;}
        public boolean isMigratedUser {get; private set;}
        public boolean hasCompletedTutorial {get; private set;}
        public boolean hasAcceptedConditionsOfUse {get; private set;}
        public boolean profileBuildProcessComplete {get; private set;}
        public boolean hasAllowedNotifications {get; private set;}
        public boolean hasAllowedDailyTips {get; private set;}
        public boolean showWeightCelebrations {get; private set;}
        public boolean shownWeightCelebrationCard {get; private set;}
        public String timeZone {get; private set;}
        public String appVersion {get; private set;}

        
        //Program Membership fields
        public String dayOfWeek {get; set;}
        public Decimal daysInProgram {get; private set;}
        public Date dietStartDate {get; set;}
        public Date clientStartDate {get; set;}
        public String dietType {get; set;}
        public Date enrollmentDate {get; set;}
        public String focusArea {get; set;} // to delete
        public Decimal initialWeight {get; set;}
        public boolean isRemoteUser {get; protected set;}
        public String membershipType {get; set;}
        public Date minimumLogDate {get; set;}
        public String motivation {get; set;}
        public String programMembershipDay {get; set;}
        public String programMembershipId {get; set;}
        public String programId {get; set;}
        public String programName {get; set;}
        public String supportNeeded {get; set;}
        public Decimal weightLossGoal {get; set;}

        // Supplemental fields
        public Decimal currentWeight {get; private set;}
        public Date currentWeekStartDate {get; set;}
        public string photoUrl {get; set;}
        public boolean isBootcampUser {get; set;}
        //indicates if diet start selection should be displayed as part of profile build
        public boolean showInitialDietStartDateSelection{get; set;}
        public boolean isCoachEditable {get; set;}
        public string coachingUrl {get{
          return isCoachEditable ? ((programName == 'P1 Healthy Solutions' || programName == 'Phase 2') ? 'class-scheduler?MobileApp=true' : 'resources/getting-started/do-i-need-weight-loss-coach?MobileApp=true') : 'group-data?MobileApp=true';
        }}
        public boolean showReferFriendsMenu {get {
          return (!isInternalEmployee && (programName == 'P1 Healthy Solutions' || programName == 'Phase 2'));
        }}
        public string coachName {get; set;}
        public string coachEmail {get; set;}
        public string groupConferenceNumber {get; set;}
        public string groupCode {get; set;}
        public string groupName {get; set;}
        public string groupDay {get; set;}
        public Date groupFirstDate {get; set;}
        public Date groupActiveDate {get; set;}
        public string zoomLink {get; set;}
    }       
}