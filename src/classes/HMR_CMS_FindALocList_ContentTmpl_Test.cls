/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 19th, 2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_FindALocList_ContentTemplate
*/

@isTest
private class HMR_CMS_FindALocList_ContentTmpl_Test{  

        
    @isTest 
    private static void testGetHtmlWithAccount() { 
              
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Professional Account').getRecordTypeId();
        
        Account accTst = new Account(Name = 'test',BillingStreet = 'test street, test', BillingState = 'CA',hmr_Referral__c ='HSOnly', recordtypeid = recordTypeId,hmr_Location_Type__c ='Product & Services(P&S)');
        insert accTst;
        
        HMR_CMS_FindALocList_ContentTemplate findALocation = new HMR_CMS_FindALocList_ContentTemplate();
        String propertyValue = findALocation.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                        
        String renderHTML = findALocation.getHTML();
        
        System.assert(!String.isBlank(renderHTML));
                
    }
    
    
    @isTest 
    private static void testGetHtmlWithAccount02() { 
              
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Professional Account').getRecordTypeId();
        
        Account accTst = new Account(Name = 'test',BillingStreet = 'test street, test', hmr_Referral__c ='HSOnly', recordtypeid = recordTypeId,hmr_Location_Type__c ='Product & Services(P&S)');
        insert accTst;
        
        HMR_CMS_FindALocList_ContentTemplate findALocation = new HMR_CMS_FindALocList_ContentTemplate();
        String propertyValue = findALocation.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                        
        String renderHTML = findALocation.getHTML();
        
        System.assert(!String.isBlank(renderHTML));
                
    }
    
}