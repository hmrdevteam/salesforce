/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Log: Missing Meaningful Asserts (if needed) and test for parameter method.
Target Class(ses): HMR_CMS_Clinic_Hero_ContentTemplate
*/

@isTest
private class HMR_CMS_Clinic_Hero_Test {
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_Clinic_Hero_ContentTemplate clinicHeroTemplate = new HMR_CMS_Clinic_Hero_ContentTemplate();
    String propertyValue = clinicHeroTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
    
    String heroImage = clinicHeroTemplate.heroImage;

    String renderHMTL = clinicHeroTemplate.getHTML();
    
    System.assertEquals('<img class="top-img" src="null" width="100%" />', renderHMTL);
    
  }
  
  @isTest static void test_general_method_parameters() { //TODO 
    //cms.CreateContentController cc = new cms.CreateContentController();

   // init controller with parameter
    //HMR_CMS_Clinic_Hero_ContentTemplate clicnicHeroTemplatewithParam = new HMR_CMS_Clinic_Hero_ContentTemplate(cc);
  }
}