@isTest
private class TransactionPaymentTriggerHandlerTest{

    static testMethod void firstTestMethod(){

            Account testAcc = new Account(name='test');
            insert testAcc;
        
            Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
            insert con;
            
            Program__c programObj = new Program__c(Name='test program', hmr_Program_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30);
            insert programObj;
            
            Program_Membership__c programMemberObject = new Program_Membership__c(Program__c = programObj.Id, hmr_Status__c = 'Active');
            insert programMemberObject;
            
            ccrz__E_Order__c testOrderOld = new ccrz__E_Order__c(ccrz__Account__c = testAcc.Id, ccrz__Contact__c = con.Id,
                                                                ccrz__OrderStatus__c = 'Template', hmr_Program_Membership__c = programMemberObject.id, hmr_Program_Membership__r = programMemberObject);
            insert testOrderOld;
            
            ccrz__E_Order__c testOrder = new ccrz__E_Order__c(ccrz__Account__c = testAcc.Id, ccrz__Contact__c = con.Id, ccrz__Order__c = testOrderOld.id,
                                                                ccrz__OrderStatus__c = 'Template', hmr_Program_Membership__c = programMemberObject.id, hmr_Program_Membership__r = programMemberObject);
            insert testOrder;
 
           List<ccrz__E_TransactionPayment__c > transacList = new List<ccrz__E_TransactionPayment__c>();
             
           
            ccrz__E_TransactionPayment__c transPayment = new ccrz__E_TransactionPayment__c(ccrz__Account__c = testAcc.Id,ccrz__Amount__c=123,
                                                Card_Type__c = 12, ccrz__CCOrder__c=testOrder.id,ccrz__CCOrder__r = testOrder);
                                                
            transacList.add(transPayment);
            //insert transPayment;
            
            ccrz__E_TransactionPayment__c transPayment2 = new ccrz__E_TransactionPayment__c(ccrz__Account__c = testAcc.Id,ccrz__Amount__c=123,
                                                Card_Type__c = 12, ccrz__CCOrder__c=testOrderOld.id,ccrz__CCOrder__r = testOrderOld);
            transacList.add(transPayment2);                                    
           // insert transPayment2;
           insert transacList;
   
    }      
       
}