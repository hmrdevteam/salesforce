/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/23/2017
 * Created By: Zach Engman
 * Last Modified Date:07/17/2017
 * Last Modified By:
 * Description: Utility class for formatting/standardizing field data
 * ****************************************************/
public without sharing class HMR_Format_Utility {
	//Capitalizes the first letter of the string passed
    public static string capitalize(string value){
        if(!String.isBlank(value))
            value = value.capitalize();
        
        return value;
    }

    //Capitalizes the first letter of each string part passed
    public static string capitalizeAll(string value){
    	if(!String.isBlank(value)){
    		string[] stringParts = value.split(' ');
    		
            for(integer index = 0; index < stringParts.size(); index++)
    			stringParts[index] = stringParts[index].capitalize();
    		
            value = String.join(stringParts, ' ');
    	}

    	return value;
    }

    public static string uppercase(string value){
    	if(!String.isBlank(value))
    		value = value.toUpperCase();

    	return value;
    }

    public static string phone(string phone, string separator){
        String formattedPhone = !String.isBlank(phone) ? phone.replaceAll('[^0-9]','') : '';
        //If the length is 10, it is a number we can format nicely
        return formattedPhone.length() == 10 ? String.format('{0}{3}{1}{3}{2}', new String[] {formattedPhone.left(3), formattedPhone.mid(3,3), formattedPhone.right(4), separator}) : phone;
    }

}