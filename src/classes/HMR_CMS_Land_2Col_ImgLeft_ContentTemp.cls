/**
* Apex Content Template Controller for Landing Page Sections
*
* @Date: 03/13/2018
* @Author Ali Pierre (HMR)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_Land_2Col_ImgLeft_ContentTemp extends cms.ContentTemplateController{

	global HMR_CMS_Land_2Col_ImgLeft_ContentTemp(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_Land_2Col_ImgLeft_ContentTemp() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String LeftImageURL{
         get{
            return getPropertyWithDefault('LeftImageURL', '');
        }
    }
    public String LeftImageClass{
        get{
            return getPropertyWithDefault('LeftImageClass', '[]');
        }
    }

	public String RightTitle{
        get{
            return getPropertyWithDefault('RightTitle', '');
        }
    }    

	public String RightDescription{
        get{
            return getPropertyWithDefault('RightDescription', '');
        }
    }
    public Boolean showBulletedList {
        get {
            return getAttribute('showBulletedList') == 'true';
        }
    }
    public String LinkOrButton{
        get{
            return getPropertyWithDefault('LinkOrButton', '[]');
        }
    }

    public String LinkOrButtonText{
        get{
            return getPropertyWithDefault('LinkOrButtonText', '');
        }
    }
    public String LinkOrButtonTargetUrl{
        get{
            return getPropertyWithDefault('LinkOrButtonTargetUrl', '');
        }
    }
    public String LinkOrButtonMobileTargetUrl{
        get{
            return getPropertyWithDefault('LinkOrButtonMobileTargetUrl', '');
        }
    }
    public String sectionGTMIDAttr{
         get{
            return getPropertyWithDefault('sectionGTMIDAttr', 'considerBlock2');
        }
    }
    
	global virtual override String getHTML(){
		String html = ''; 
		String bulletedListHtml = '';
		String RightlinkOrButtonHTML = '';

		if(LinkOrButton == 'Link'){
			RightlinkOrButtonHTML += '<div class="row">' +
						                '<div class="link-container">' +
								            '<div class="hmr-allcaps-container">'+
								            	'<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + LinkOrButtonTargetUrl + '" class="hidden-sm hidden-xs hmr-allcaps hmr-allcaps-blue">' + LinkOrButtonText + '</a>'+
								            	'<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + LinkOrButtonTargetUrl + '" class="hidden-sm hidden-xs hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>'+
                                                '<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + LinkOrButtonMobileTargetUrl + '" class="hidden-lg hidden-md hmr-allcaps hmr-allcaps-blue">' + LinkOrButtonText + '</a>'+
                                                '<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + LinkOrButtonMobileTargetUrl + '" class="hidden-lg hidden-md hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>'+
								            '</div>'+
								         '</div>'+
						            '</div>';
		}else{
			RightlinkOrButtonHTML += '<div class="btn-container">'+
										'<a href="#" class="btn btn-primary hmr-btn-blue hmr-btn-small" data-toggle="modal" data-target="#hmrSubscribeModal">' + LinkOrButtonText + '</a>' +
									'</div>';
		}
		if(showBulletedList){
			bulletedListHtml += '<div class="row">' +
									'<ul>'+
									'<li>Lose weight quickly</li>' +
									'<li>Break free of unhealthy patterns</li>' +
									'<li>Make way for new, healthier habits</li>'+
									'<li>Learn a new eating plan for the long term</li>'+
									'</ul>' +
								'</div>';
		}
		html += '<section class="hmr-page-section hmr-2col-left-img-section bg-subscribe landing-clearbg">' +
			        '<div class="container">' +
			            '<div class="row landing-row">' +
			                '<div class="col-sm-6">' +
			                    '<img class="landing-left-img" src="' + LeftImageURL +'" />' +
			                '</div>' +
							'<div class="col-sm-6">' +
			                    '<div class="landing-right-section">' +
			                        '<h2 class="landing-right-title">' + RightTitle + '</h2>' +
			                        '<div class="landing-right-blurb">' +
			                            '<p>' + RightDescription + '</p>' +
			                        '</div>';
			                        if(showBulletedList){
			                        	html += bulletedListHtml;
			                        }
			                        html += RightlinkOrButtonHTML +
			                    '</div>' +
			                '</div>' +
			            '</div>' +
			        '</div>' +
			    '</section>';
		return html;
	}
}