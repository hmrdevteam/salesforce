/**
*
* @Date: 07/27/2017
* @Author Utkarsh Goswami(Mindtree)
* @Modified:
* @JIRA: HPRP-4109
*/
global virtual class HMR_CMS_CommentFeed_ContentTemplate extends cms.ContentTemplateController{
    global HMR_CMS_CommentFeed_ContentTemplate(cms.createContentController cc) {
        super(cc);
    }
    global HMR_CMS_CommentFeed_ContentTemplate() {}

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }


    global virtual override String getHTML(){
        
       
        String html = ''+  
        '<div class="container-fluid">' +
            '<div class="row questions-detail">' +
                    '<div class="container leave-comment">'+
                       
                    '</div>'+
                    '<div class="commentsZone">' +

                    '</div>' +

                    '<div class="container">' +
                        '<div class="col-sm-6 hidden-xs">' +
                            //'<a class="hmr-link" href="#">< BACK TO CONVERSATIONS</a>' +
                        '</div>' +
                    //'<div class="col-sm-6 text-right">' +
                    //    '<nav aria-label="Page navigation">' +
                    //        '<ul class="pagination">' +
                    //            '<li>' +
                    //                '<a href="#" aria-label="Previous">' +
                    //                    '<span aria-hidden="true">< Previous</span>' +
                    //                '</a>' +
                    //            '</li>' +
                    //            '<li><a href="#">1</a></li>' +
                    //            '<li>' +
                    //                '<a href="#" aria-label="Next">' +
                    //                    '<span aria-hidden="true">Next ></span>' +
                    //                '</a>' +
                    //            '</li>' +
                    //        '</ul>' +
                    //    '</nav>' +
                    //'</div>' +
                    '<div class="modalZone">' +
                    '</div>' +
            '</div>' +
        '</div>';

   
        return html;
    }

    //@testVisible
    //private List<ConnectApi.FeedElement> getFeedItems(){
    
    //    String articleName = 'Spectacular-Squash-Casserole';
        
    //    HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        
    //    HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);
        
        
    //    List<ConnectApi.FeedElement> feedItemList = new List<ConnectApi.FeedElement>();
    //      if(!String.isBlank(articleRecord.Id)){

    //        ConnectApi.FeedElementPage result = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, articleRecord.id);
            
    //        feedItemList = result.elements;
    //    }
        
    //    return feedItemList;
    //}
    @testVisible
    private string getFormatedDate(DateTime d){
        string returnMe = '';
        if(d!=null){
            returnMe = d.format('MMMM dd, yyyy hh:mm a');
        }
        return returnMe;
    }
    
    
    public class QuestionAnswerResult{
    
        public ConnectApi.FeedElement feedElement{get; set;}
        public ConnectApi.UserSummary userSummary {get; set;} 
        

        
        public QuestionAnswerResult(ConnectApi.FeedElement feedElement){
            this.feedElement = feedElement;
           this.userSummary = (ConnectApi.UserSummary)((ConnectApi.FeedItem)feedElement).actor;
        }
    
    }

}