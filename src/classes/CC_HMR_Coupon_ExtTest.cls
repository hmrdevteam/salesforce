/**
* @Date: 3/5/2017
* @Author: Mustafa Ahmed (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-
* Modified: 3/26/2017 Nathan Anderson
*/


@isTest
private class CC_HMR_Coupon_ExtTest {

    @isTest static void test_method_one() {
        // Implement test code

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Dynamic Kit',
            Program__c = testProgram.Id,
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'TestC',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            hmr_Program__c = testProgram.Id,
            ccrz__CouponCode__c = 'TestCoupon_P1',
            ccrz__CouponName__c = 'TestCoupon_P1',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;

        ccrz__E_AccountGroup__c accountGroup = new ccrz__E_AccountGroup__c(
            Name = 'TestAccountGroup',
            ccrz__PriceListSelectionMethod__c = 'Best Price'
        );
        insert accountGroup;

        Account testAccount = new Account(
            Name = 'Test Account',
            ccrz__E_AccountGroup__c = accountGroup.Id
        );

        insert testAccount;

        Contact testContact = new Contact(
            FirstName = 'TestFirstName',
          LastName = 'TestLastName',
          hmr_Contact_Original_Source__c = 'Referral',
          hmr_Contact_Original_Source_Detail__c = 'Friend',
            Account = testAccount
        );

        insert testContact;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User'];

        UserRole r = [Select Name, Id from UserRole where Name ='President' LIMIT 1];
       /*   User testUser = new User(
                        Alias = 'standt',
                        Email = 'standarduser@testorg.com',
                        EmailEncodingKey = 'UTF-8',
                        LastName = 'Testing',
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US',
                        ProfileId = p.Id,
                        IsActive = true,
                        UserRoleId = r.Id,
                        TimeZoneSidKey = 'America/New_York',
                        ContactId = testContact.Id,
                        UserName = 'standarduser@testorg.com');
        insert testUser;
*/

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        System.runAs ( thisUser ) {
            final List<String> productIds = new List<String>();

            productIds.add(prod.Id);

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.getCCCoupons(ctx, testCoupon.Name, productIds, testContact.Id);
            CC_HMR_Coupon_Ext.getExtendedDataForProducts(ctx, prod.Id);
            System.assert(rar != null);
            //System.assert(rar.success);

            //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
            //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }

    }


    @isTest static void test_method_two() {
        // Implement test code

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Product',
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'TestC',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'A La Carte',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            hmr_Program__c = testProgram.Id,
            ccrz__CouponCode__c = 'TestCoupon_P1',
            ccrz__CouponName__c = 'TestCoupon_P1',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;

        ccrz__E_AccountGroup__c accountGroup = new ccrz__E_AccountGroup__c(
            Name = 'TestAccountGroup',
            ccrz__PriceListSelectionMethod__c = 'Best Price'
        );
        insert accountGroup;

        Account testAccount = new Account(
            Name = 'Test Account',
            ccrz__E_AccountGroup__c = accountGroup.Id
        );

        insert testAccount;

        Contact testContact = new Contact(
            FirstName = 'TestFirstName',
            LastName = 'TestLastName',
            hmr_Contact_Original_Source__c = 'Referral',
            hmr_Contact_Original_Source_Detail__c = 'Friend',
            Account = testAccount
        );

        insert testContact;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'CloudCraze Customer Community User'];

        UserRole r = [Select Name, Id from UserRole where Name ='President' LIMIT 1];
       /*   User testUser = new User(
                        Alias = 'standt',
                        Email = 'standarduser@testorg.com',
                        EmailEncodingKey = 'UTF-8',
                        LastName = 'Testing',
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US',
                        ProfileId = p.Id,
                        IsActive = true,
                        UserRoleId = r.Id,
                        TimeZoneSidKey = 'America/New_York',
                        ContactId = testContact.Id,
                        UserName = 'standarduser@testorg.com');
        insert testUser;
*/

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            final List<String> productIds = new List<String>();

            productIds.add(prod.Id);

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.getCCCoupons(ctx, testCoupon.Name, productIds, testContact.Id);
            System.assert(rar != null);
            System.assert(rar.success);

            //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
            //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }

    }


    @isTest static void test_method_three() {
        // Implement test code
                //Declare variable to store today's date
        Date dt = Date.today();

        //Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Create a Coach User
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        //Insert User
        insert userObj;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');

        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();

        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj1);

        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj2);

        Contact testContatObj3 = new Contact(FirstName = 'P2', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj3);

        Contact testContatObj4 = new Contact(FirstName = 'P2R', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj4);

        Contact testContatObj5 = new Contact(FirstName = 'P1HSSR', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj5);

        Contact testContatObj6 = new Contact(FirstName = 'P1HSSRcls', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj6);

        //List of contacts
        insert conList;

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Dynamic Kit',
            Program__c = testProgram.Id,
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'TestC',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General',
            Account__c = testAccountObj.Id
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            hmr_Program__c = testProgram.Id,
            ccrz__CouponCode__c = 'TestCoupon_P1',
            ccrz__CouponName__c = 'TestCoupon_P1',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;


        system.RunAs(userObj)
        {
            final List<String> productIds = new List<String>();

            productIds.add(prod.Id);

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.getCCSavedCoupons(ctx, testAccountObj.Id, productIds,testContatObj1.id,'testcoupon');
            System.assert(rar != null);
            //System.assert(rar.success);
        }

    }

    @isTest static void test_method_four() {
        // Implement test code
                //Declare variable to store today's date
        Date dt = Date.today();

        //Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Create a Coach User
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        //Insert User
        insert userObj;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');

        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();

        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj1);

        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj2);

        Contact testContatObj3 = new Contact(FirstName = 'P2', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj3);

        Contact testContatObj4 = new Contact(FirstName = 'P2R', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj4);

        Contact testContatObj5 = new Contact(FirstName = 'P1HSSR', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj5);

        Contact testContatObj6 = new Contact(FirstName = 'P1HSSRcls', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj6);

        //List of contacts
        insert conList;

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Product',
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'TestC',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General',
            Account__c = testAccountObj.Id
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'A La Carte',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'TestCoupon',
            ccrz__CouponName__c = 'TestCoupon',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;


        system.RunAs(userObj)
        {
            final List<String> productIds = new List<String>();

            productIds.add(prod.Id);

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.getCCSavedCoupons(ctx, testAccountObj.Id, productIds,testContatObj1.id,'testcoupon');
            System.assert(rar != null);
            System.assert(rar.success);
        }
    }

      @isTest static void test_method_five() {

           cc_dataFActory.setupTestUser();
           User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

           System.runAs ( thisUser ) {

                cc_dataFActory.setupCatalog();
                ccrz__E_Product__c objProduct= [Select Id from ccrz__E_Product__c where ccrz__SKU__c ='test001' limit 1];
                ccrz__E_Cart__c objCart=cc_dataFActory.createCart();
                cc_dataFActory.addCartItem(objCart,objProduct.Id,3,150);

                Decimal discountAmount=10;


                ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
                ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.adjustHMRDiscount(ctx, objCart.ccrz__EncryptedId__c,discountAmount);
                System.assert(rar.success);



           }

      }

   @isTest static void test_method_six() {

       cc_dataFActory.setupTestUser();
     //  User thisUser = [ select Id, ContactId from User where ContactId != null LIMIT 1];
        User thisUser = [ select Id, ContactId from User LIMIT 1];

       System.runAs ( thisUser ) {

             Test.setCurrentPageReference(new PageReference('Page.HMRCoupon'));
              System.currentPageReference().getParameters().put('portalUser', 'test');

            ccrz__E_Category__c categoryObj = new ccrz__E_Category__c(Name ='HMR Products', ccrz__CategoryID__c = 'testcateg', ccrz__Sequence__c = 220,
                                                ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today().addDays(1));

            insert categoryObj;

            CC_HMR_Coupon_Ext ccoupExt = new CC_HMR_Coupon_Ext();

            ccoupExt.specialDiscFlag = 'Test';

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            CC_HMR_Coupon_Ext.getCCCoupons(ctx, 'test', (new List<String>()), thisUser.ContactId);
        }
    }

    @isTest static void test_method_seven() {
        // Implement test code

        Program__c testProgram = new Program__c(
            Name = 'Test Program',
            hmr_Has_Phone_Coaching__c = false,
            hmr_AD_Mandatory_For_Phone__c = false,
            hmr_Phase_Type__c = 'P1',
            hmr_Program_Description__c = 'Program Test Description',
            hmr_Program_Display_Name__c = 'P1 Test Program',
            hmr_Program_Type__c = 'Healthy Solutions',
            Days_in_1st_Order_Cycle__c = 21
        );

        insert testProgram;

        List<String> programIds = new List<String>();
        programIds.add(testProgram.Id);

        ccrz__E_Product__c prod=new ccrz__E_Product__c(
            ccrz__ProductId__c = 'ABC',
            ccrz__Quantityperunit__c = 2,
            ccrz__SKU__c = 'IMURI',
            ccrz__ProductType__c = 'Dynamic Kit',
            Program__c = testProgram.Id,
            ccrz__StartDate__c = Date.newInstance(2017, 11, 11),
            ccrz__EndDate__c = Date.newInstance(2017, 11, 12)
        );
        insert prod;

        HMR_Coupon__c testCoupon = new HMR_Coupon__c(
            Name = 'ProgramMember',
            hmr_Active__c = true,
            hmr_Description__c = 'Coupon Description',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert testCoupon;

        ccrz__E_Coupon__c childCCCoupon = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Percentage',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = testCoupon.Id,
            ccrz__DiscountAmount__c = 10,
            ccrz__Storefront__c = 'DefaultStore',
            hmr_Program__c = testProgram.Id,
            ccrz__CouponCode__c = 'ProgramMember_I',
            ccrz__CouponName__c = 'ProgramMember_I',
            ccrz__TotalUsed__c = 0
        );

        insert childCCCoupon;

        ccrz__E_AccountGroup__c accountGroup = new ccrz__E_AccountGroup__c(
            Name = 'TestAccountGroup',
            ccrz__PriceListSelectionMethod__c = 'Best Price'
        );
        insert accountGroup;

        Account testAccount = new Account(
            Name = 'Test Account',
            ccrz__E_AccountGroup__c = accountGroup.Id
        );

        insert testAccount;

        Contact testContact = new Contact(
            FirstName = 'TestFirstName',
          LastName = 'TestLastName',
          hmr_Contact_Original_Source__c = 'Referral',
          hmr_Contact_Original_Source_Detail__c = 'Friend',
            Account = testAccount
        );

        insert testContact;

        ccrz__E_Order__c testOrder = new ccrz__E_Order__c(ccrz__Contact__c = testContact.Id, ccrz__Account__c =  testAccount.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1 1st Order');
        
        insert testOrder;

        //Create Program Memberships to be added to prgMemList and inserted into the Database
        Program_Membership__c prgMem = new Program_Membership__c(Program__c = testProgram.id, hmr_Status__c = 'Active',hmr_Contact__c = testContact.id, hmr_Enrollment_Date__c = Date.today(), hmr_First_Order_Number__c = testOrder.Id );

        insert prgMem;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User'];

        UserRole r = [Select Name, Id from UserRole where Name ='President' LIMIT 1];
       /*   User testUser = new User(
                        Alias = 'standt',
                        Email = 'standarduser@testorg.com',
                        EmailEncodingKey = 'UTF-8',
                        LastName = 'Testing',
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US',
                        ProfileId = p.Id,
                        IsActive = true,
                        UserRoleId = r.Id,
                        TimeZoneSidKey = 'America/New_York',
                        ContactId = testContact.Id,
                        UserName = 'standarduser@testorg.com');
        insert testUser;
        */
       

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        System.runAs ( thisUser ) {
            final List<String> productIds = new List<String>();

            productIds.add(prod.Id);

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            ccrz.cc_RemoteActionResult rar = CC_HMR_Coupon_Ext.getCCCoupons(ctx, testCoupon.Name, productIds, testContact.Id);
            CC_HMR_Coupon_Ext.getExtendedDataForProducts(ctx, prod.Id);
            System.assert(rar != null);
            //System.assert(rar.success);

            //final Map<String,Object> dataMap = (Map<String,Object>) rar.data;
            //System.assertEquals(chilCoupons.size(), dataMap.keySet().size());
        }
    }
}