/*****************************************************
 * Author: Utkarsh Goswami
 * Created Date: 01/12/2018
 * Created By: 
 * Last Modified Date: 01/15/2018
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobDietActivity REST Service
 * ****************************************************/
@isTest
private class HMRMobDietActivityTest {

    @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
      insert programRecord;

      Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                hmr_Active__c = true,
                                                                                hmr_Status__c = 'Active',
                                                                                hmr_Contact__c = contactRecord.Id,
                                                                                hmr_Diet_Start_Date__c = Date.today());
      insert programMembershipRecord;
    }

    @isTest
    private static void testGetDietActivityForNewRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobDietActivity.doGet();

      Test.stopTest();

      //Verify the diet activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetDietActivityForExistingRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      
      insert new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                               ,Program_Membership__c = programMembershipRecord.Id
                               ,DateDataEnteredFor__c = Date.today()
                               ,CurrentWeight__c = 170 
                               ,Entrees__c = 3
                               ,FruitsVegetables__c = 8
                               ,ShakesCereals__c = 4
                               ,Water__c = 8);
      
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobDietActivity.doGet();

      Test.stopTest();

      //Verify the diet activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }
    
    @isTest
    private static void testGetDietActivityWithInvalidData(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.params.put('contactId', '');
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobDietActivity.doGet();

      Test.stopTest();

      //Verify invalid status code was returned
      System.assertEquals(404, response.statuscode);
    }

    @isTest
    private static void testPostDietActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8);

      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        dietActivityDTO = HMRMobDietActivity.getDietActivity(activityLogRecord);
        HMRMobDietActivity.doPost(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the activity log record was created
      ActivityLog__c activityLogResultRecord = [SELECT Contact__c FROM ActivityLog__c];
      System.assertEquals(userRecord.ContactId, activityLogResultRecord.Contact__c);
    }

    @isTest
    private static void testPostDietActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8
                                                           ,Program_Membership__c = programMembershipRecord.Id);
       ActivityLog__c activityLogRecord2 = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8
                                                           ,Program_Membership__c = programMembershipRecord.Id);
    
     insert activityLogRecord2;
      
      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        dietActivityDTO = HMRMobDietActivity.getDietActivity(activityLogRecord);
        HMRMobDietActivity.doPost(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(405, response.statusCode);
    }
    
    
    @isTest
    private static void testPostDietActivityLogNegative(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;
      
      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
       // dietActivityDTO = HMRMobDietActivity.getDietActivity(activityLogRecord);
        HMRMobDietActivity.doPost(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(500, response.statusCode);
    }
    
    

    @isTest
    private static void testPatchDietActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8);
      insert activityLogRecord;

      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        dietActivityDTO = HMRMobDietActivity.getDietActivity(activityLogRecord);
        dietActivityDTO.entrees = 10;
        HMRMobDietActivity.updateDietActivity(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the activity log record was updated
      ActivityLog__c activityLogResultRecord = [SELECT Entrees__c FROM ActivityLog__c];
      System.assertEquals(10, activityLogResultRecord.Entrees__c);
    }

    @isTest
    private static void testPatchDietActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8);
      insert activityLogRecord;

      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        dietActivityDTO = HMRMobDietActivity.getDietActivity(activityLogRecord);
        dietActivityDTO.recordId = null;
        HMRMobDietActivity.updateDietActivity(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(405, response.statusCode);
    }
    
    
    @isTest
    private static void testPatchDietActivityLogNegative(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;


      HMRMobDietActivity.MobDietActivityDTO dietActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobDietActivity.updateDietActivity(dietActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(404, response.statusCode);
    }

    @isTest
    private static void testDeleteDietActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8);
      insert activityLogRecord;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.params.put('id', activityLogRecord.Id);
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;


      Test.startTest();

      System.runAs(userRecord){
        HMRMobDietActivity.deleteActivity();
      }

      Test.stopTest();

      //Verify the activity log record was deleted
      List<ActivityLog__c> activityLogResultList = [SELECT Id FROM ActivityLog__c];
      System.assertEquals(0, activityLogResultList.size());
    }

    @isTest
    private static void testDeleteDietActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/dietData/';
      request.params.put('id', '00000000000000000000');
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobDietActivity.deleteActivity();
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(404, response.statusCode);
    }
}