/**
* Service Class to get the Dashboard Data
*
* @Date: 2018-02-14
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-04-09
* @JIRA: 
*/
public with sharing class HMR_Dashboard_Service {
	public HMRMobProfile.MobProfileDTO profile;

	public HMR_Dashboard_Service(string userId) {
		this.profile = HMRMobProfile.getProfile(userId);
	}

	public WeeklyDashboardView getWeeklyDashboardView(){
		return getWeeklyDashboardView(Date.today());
	}

	public WeeklyDashboardView getWeeklyDashboardView(Date weekDate){
		WeeklyDashboardView dashboardView;
		WeightMetric previousWeightMetric;

		//get the program membership and diet type
		if (profile != null){
			HMR_CC_ProgramMembership_Service membershipService = new HMR_CC_ProgramMembership_Service();
			Program_Membership__c programMembershipRecord = membershipService.getProgramMembership(profile.Id, weekDate);
            string programName = programMembershipRecord.Program__r.Name;
            string dietType = programMembershipRecord.DietType__c;
            Date dashboardWeekStartDate = profile.getWeekStartDate(weekDate);
            Date dashboardWeekEndDate = dashboardWeekStartDate.addDays(6);

            Integer daysIntoWeek = Date.today().daysBetween(dashboardWeekEndDate);

            //Calculate the number of days into the week
            if(daysIntoWeek <= 0){
            	daysIntoWeek = 7;
            }
            else{
            	daysIntoWeek = 7 - daysIntoWeek;
            }

			//get the program/diet type objectives
			HMR_Goal_Service.GoalSetting goalSettings = HMR_Goal_Service.getGoalSetting(programName, dietType);

			//get the current goal for the user
			HMR_Goal_Service goalService = new HMR_Goal_Service(this.profile.id);
			Goal__c goalRecord = goalService.getGoalForDate(weekDate);

			if(goalSettings != null){
				dashboardView = new WeeklyDashboardView(programName, dietType, daysIntoWeek, goalSettings, goalRecord);

				dashboardView.startDay = DateTime.newInstance(dashboardWeekStartDate, Time.newInstance(0,0,0,0)).format('EEEE');
				dashboardView.startWeight = profile.initialWeight;

				//get the activity and weight statistics
				for(ActivityLog__c activityLogRecord : [SELECT  BenefitBars__c
															   ,CurrentWeight__c
															   ,DateDataEnteredFor__c
															   ,Entrees__c
															   ,FruitsVegetables__c
															   ,InTheBox__c
															   ,ShakesCereals__c
															   ,TotalMealReplacements__c
															   ,TotalPhysicalActivityCaloriesRollUp__c
															   ,Water__c
															   ,WeeksInProgram__c
														 FROM ActivityLog__c
														 WHERE Contact__c =: profile.id
														 ORDER BY DateDataEnteredFor__c DESC]){

					//Check if the activity belongs to the current week
					if(activityLogRecord.DateDataEnteredFor__c >= dashboardWeekStartDate && activityLogRecord.DateDataEnteredFor__c <= dashboardWeekEndDate){
						string dayOfWeek = DateTime.newInstance(activityLogRecord.DateDataEnteredFor__c, Time.newInstance(0,0,0,0)).format('EEEE');

						if(dayOfWeek == 'Monday'){
							dashboardView.mondayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.mondayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Tuesday'){
							dashboardView.tuesdayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.tuesdayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Wednesday'){
							dashboardView.wednesdayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.wednesdayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Thursday'){
							dashboardView.thursdayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.thursdayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Friday'){
							dashboardView.fridayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.fridayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Saturday'){
							dashboardView.saturdayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.saturdayObjective.addTotals(activityLogRecord);
						}
						else if(dayOfWeek == 'Sunday'){
							dashboardView.sundayObjective.inTheBox = activityLogRecord.InTheBox__c;
							dashboardView.sundayObjective.addTotals(activityLogRecord);
						}
					}

					//weight history chart can span for all time so need the record histories outside of current week
					if(activityLogRecord.CurrentWeight__c != null){
						//we are sorted by date entered, so get most recent weight entry
						if(dashboardView.currentWeight == null){
							dashboardView.currentWeight = activityLogRecord.CurrentWeight__c;
						}

						WeightMetric currentWeightMetric = new WeightMetric(activityLogRecord.Id, activityLogRecord.DateDataEnteredFor__c, activityLogRecord.CurrentWeight__c);

						//store in our weight metrics for the line graph
						if(currentWeightMetric.weightDate != null){
							dashboardView.weightLogList.add(currentWeightMetric);
						}

						if(previousWeightMetric != null){
							previousWeightMetric.weightChangeFromPrevious = previousWeightMetric.weight - currentWeightMetric.weight;
						}

						previousWeightMetric = currentWeightMetric;
					}
				}

				dashboardView.barMetric.total = dashboardView.mondayObjective.barTotal + dashboardView.tuesdayObjective.barTotal + dashboardView.wednesdayObjective.barTotal +
												dashboardView.thursdayObjective.barTotal + dashboardView.fridayObjective.barTotal + dashboardView.saturdayObjective.barTotal +
												dashboardView.sundayObjective.barTotal;
				dashboardView.barMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.barTotal,
																				  'tuesday' => dashboardView.tuesdayObjective.barTotal,
																				  'wednesday' => dashboardView.wednesdayObjective.barTotal,
																				  'thursday' => dashboardView.thursdayObjective.barTotal,
																				  'friday' => dashboardView.fridayObjective.barTotal,
																				  'saturday' => dashboardView.saturdayObjective.barTotal,
																				  'sunday' => dashboardView.sundayObjective.barTotal};
				dashboardView.entreeMetric.total = dashboardView.mondayObjective.entreeTotal + dashboardView.tuesdayObjective.entreeTotal + dashboardView.wednesdayObjective.entreeTotal +
												   dashboardView.thursdayObjective.entreeTotal + dashboardView.fridayObjective.entreeTotal + dashboardView.saturdayObjective.entreeTotal +
												   dashboardView.sundayObjective.entreeTotal;
				dashboardView.entreeMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.entreeTotal,
																					 'tuesday' => dashboardView.tuesdayObjective.entreeTotal,
																					 'wednesday' => dashboardView.wednesdayObjective.entreeTotal,
																					 'thursday' => dashboardView.thursdayObjective.entreeTotal,
																					 'friday' => dashboardView.fridayObjective.entreeTotal,
																					 'saturday' => dashboardView.saturdayObjective.entreeTotal,
																					 'sunday' => dashboardView.sundayObjective.entreeTotal};
				dashboardView.fruitsAndVegetablesMetric.total = dashboardView.mondayObjective.fruitsAndVegetablesTotal + dashboardView.tuesdayObjective.fruitsAndVegetablesTotal + dashboardView.wednesdayObjective.fruitsAndVegetablesTotal +
												   				dashboardView.thursdayObjective.fruitsAndVegetablesTotal + dashboardView.fridayObjective.fruitsAndVegetablesTotal + dashboardView.saturdayObjective.fruitsAndVegetablesTotal +
												   				dashboardView.sundayObjective.fruitsAndVegetablesTotal;
				dashboardView.fruitsAndVegetablesMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.fruitsAndVegetablesTotal,
																				  			      'tuesday' => dashboardView.tuesdayObjective.fruitsAndVegetablesTotal,
																				  				  'wednesday' => dashboardView.wednesdayObjective.fruitsAndVegetablesTotal,
																				  				  'thursday' => dashboardView.thursdayObjective.fruitsAndVegetablesTotal,
																				  				  'friday' => dashboardView.fridayObjective.fruitsAndVegetablesTotal,
																				  				  'saturday' => dashboardView.saturdayObjective.fruitsAndVegetablesTotal,
																				  				  'sunday' => dashboardView.sundayObjective.fruitsAndVegetablesTotal};
				dashboardView.hmrFoodsMetric.total = dashboardView.mondayObjective.hmrFoodsTotal + dashboardView.tuesdayObjective.hmrFoodsTotal + dashboardView.wednesdayObjective.hmrFoodsTotal +
												   	 dashboardView.thursdayObjective.hmrFoodsTotal + dashboardView.fridayObjective.hmrFoodsTotal + dashboardView.saturdayObjective.hmrFoodsTotal +
												   	 dashboardView.sundayObjective.hmrFoodsTotal;
				dashboardView.hmrFoodsMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.hmrFoodsTotal,
																		  			   'tuesday' => dashboardView.tuesdayObjective.hmrFoodsTotal,
																		  			   'wednesday' => dashboardView.wednesdayObjective.hmrFoodsTotal,
																		  			   'thursday' => dashboardView.thursdayObjective.hmrFoodsTotal,
																		  			   'friday' => dashboardView.fridayObjective.hmrFoodsTotal,
																		  			   'saturday' => dashboardView.saturdayObjective.hmrFoodsTotal,
																		  			   'sunday' => dashboardView.sundayObjective.hmrFoodsTotal};
				dashboardView.physicalActivityMetric.total = dashboardView.mondayObjective.physicalActivityTotal + dashboardView.tuesdayObjective.physicalActivityTotal + dashboardView.wednesdayObjective.physicalActivityTotal +
												   			 dashboardView.thursdayObjective.physicalActivityTotal + dashboardView.fridayObjective.physicalActivityTotal + dashboardView.saturdayObjective.physicalActivityTotal +
												   			 dashboardView.sundayObjective.physicalActivityTotal;
				dashboardView.physicalActivityMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.physicalActivityTotal,
																				  			   'tuesday' => dashboardView.tuesdayObjective.physicalActivityTotal,
																				  				'wednesday' => dashboardView.wednesdayObjective.physicalActivityTotal,
																				  				'thursday' => dashboardView.thursdayObjective.physicalActivityTotal,
																				  				'friday' => dashboardView.fridayObjective.physicalActivityTotal,
																				  				'saturday' => dashboardView.saturdayObjective.physicalActivityTotal,
																				  				'sunday' => dashboardView.sundayObjective.physicalActivityTotal};
				dashboardView.shakeMetric.total = dashboardView.mondayObjective.shakesTotal + dashboardView.tuesdayObjective.shakesTotal + dashboardView.wednesdayObjective.shakesTotal +
												  dashboardView.thursdayObjective.shakesTotal + dashboardView.fridayObjective.shakesTotal + dashboardView.saturdayObjective.shakesTotal +
												  dashboardView.sundayObjective.shakesTotal;
				dashboardView.shakeMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.shakesTotal,
																				  	'tuesday' => dashboardView.tuesdayObjective.shakesTotal,
																				  	'wednesday' => dashboardView.wednesdayObjective.shakesTotal,
																				  	'thursday' => dashboardView.thursdayObjective.shakesTotal,
																				  	'friday' => dashboardView.fridayObjective.shakesTotal,
																				  	'saturday' => dashboardView.saturdayObjective.shakesTotal,
																				  	'sunday' => dashboardView.sundayObjective.shakesTotal};
				dashboardView.waterMetric.total = dashboardView.mondayObjective.waterTotal + dashboardView.tuesdayObjective.waterTotal + dashboardView.wednesdayObjective.waterTotal +
												  dashboardView.thursdayObjective.waterTotal + dashboardView.fridayObjective.waterTotal + dashboardView.saturdayObjective.waterTotal +
												  dashboardView.sundayObjective.waterTotal;
				dashboardView.waterMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.waterTotal,
																				  	'tuesday' => dashboardView.tuesdayObjective.waterTotal,
																				  	'wednesday' => dashboardView.wednesdayObjective.waterTotal,
																				  	'thursday' => dashboardView.thursdayObjective.waterTotal,
																				  	'friday' => dashboardView.fridayObjective.waterTotal,
																				  	'saturday' => dashboardView.saturdayObjective.waterTotal,
																				  	'sunday' => dashboardView.sundayObjective.waterTotal};
				

				dashboardView.inTheBoxMetric.total = dashboardView.daysInBox;
				dashboardView.inTheBoxMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.inTheBox ? 1 : 0,
																				  	   'tuesday' => dashboardView.tuesdayObjective.inTheBox ? 1 : 0,
																				  	   'wednesday' => dashboardView.wednesdayObjective.inTheBox ? 1 : 0,
																				  	   'thursday' => dashboardView.thursdayObjective.inTheBox ? 1 : 0,
																				  	   'friday' => dashboardView.fridayObjective.inTheBox ? 1 : 0,
																				  	   'saturday' => dashboardView.saturdayObjective.inTheBox ? 1 : 0,
																				  	   'sunday' => dashboardView.sundayObjective.inTheBox ? 1 : 0};
				dashboardView.metMinimumMetric.total = dashboardView.daysMetMinimum;
				dashboardView.metMinimumMetric.dailyObjective = new Map<String, Integer>{'monday' => dashboardView.mondayObjective.metMinimum ? 1 : 0,
																				  	     'tuesday' => dashboardView.tuesdayObjective.metMinimum ? 1 : 0,
																				  	     'wednesday' => dashboardView.wednesdayObjective.metMinimum ? 1 : 0,
																				  	     'thursday' => dashboardView.thursdayObjective.metMinimum ? 1 : 0,
																				  	     'friday' => dashboardView.fridayObjective.metMinimum ? 1 : 0,
																				  	     'saturday' => dashboardView.saturdayObjective.metMinimum ? 1 : 0,
																				  	     'sunday' => dashboardView.sundayObjective.metMinimum ? 1 : 0};
				dashboardView.calculateWeightCharts(Date.today());
			}
		}

		return dashboardView;
	}


	public class WeeklyDashboardView{
		public string startDay {get; set;}
		public Date webDisplayDate{get{
			return Date.today();
		}}
		public decimal startWeight {get; set;}
		public decimal currentWeight {get; set;}
		public decimal weightLoss {get{
			return currentWeight != null && startWeight != null ? currentWeight - startWeight : 0.0;
		}}
		public integer daysInBox{get{
			return (mondayObjective.inTheBox ? 1 : 0) +
				   (tuesdayObjective.inTheBox ? 1 : 0) +
				   (wednesdayObjective.inTheBox ? 1 : 0) +
				   (thursdayObjective.inTheBox ? 1 : 0) + 
				   (fridayObjective.inTheBox ? 1 : 0) + 
				   (saturdayObjective.inTheBox ? 1 : 0) + 
				   (sundayObjective.inTheBox ? 1 : 0);
		}}	
		public integer daysMetMinimum{get{
			return (mondayObjective.metMinimum ? 1 : 0) + 
				   (tuesdayObjective.metMinimum ? 1 : 0) +
				   (wednesdayObjective.metMinimum ? 1 : 0) +
				   (thursdayObjective.metMinimum ? 1 : 0) + 
				   (fridayObjective.metMinimum ? 1 : 0) + 
				   (saturdayObjective.metMinimum ? 1 : 0) + 
				   (sundayObjective.metMinimum ? 1 : 0);
		}}
		public List<DayObjective> dayObjectiveList{get{
			return new List<DayObjective>{mondayObjective, tuesdayObjective, wednesdayObjective, thursdayObjective, fridayObjective, saturdayObjective, sundayObjective};
		}}

		public DayObjective mondayObjective {get; set;}
		public DayObjective tuesdayObjective {get; set;}
		public DayObjective wednesdayObjective {get; set;}
		public DayObjective thursdayObjective {get; set;}
		public DayObjective fridayObjective {get; set;}
		public DayObjective saturdayObjective {get; set;}
		public DayObjective sundayObjective {get; set;}

		public MetricObjective barMetric {get; set;}
		public MetricObjective entreeMetric {get; set;}
		public MetricObjective fruitsAndVegetablesMetric {get; set;}
		public MetricObjective hmrFoodsMetric {get; set;}
		public MetricObjective inTheBoxMetric {get; set;}
		public MetricObjective metMinimumMetric {get; set;}
		public MetricObjective physicalActivityMetric {get; set;}
		public MetricObjective shakeMetric {get; set;}
		public MetricObjective waterMetric {get; set;}
		
		public List<WeightMetric> weightLogList{get; set;}

		//Weight chart specifics
		public WeightChart weightChartThreeWeek {get; set;}
		public WeightChart weightChartThreeMonth {get; set;}
		public WeightChart weightChartAllTime {get; set;}

		public WeeklyDashboardView(String programName, String dietType, Integer daysIntoWeek, HMR_Goal_Service.GoalSetting goalSettings, Goal__c goalRecord){
			//initialize the metric objectives
			this.barMetric = new MetricObjective(true, 0); // not tracked with goals
			this.entreeMetric = new MetricObjective(goalSettings.entreesAvailable, goalSettings.entreeMinimum);
			this.fruitsAndVegetablesMetric = new MetricObjective(goalSettings.fruitsAndVegetablesAvailable, goalSettings.fruitsAndVegetablesMinimum);
			this.hmrFoodsMetric = new MetricObjective(goalSettings.hmrFoodsAvailable, goalSettings.hmrFoodsMinimum);
			this.inTheBoxMetric = new MetricObjective(dietType != 'Phase 2', daysIntoWeek);
			this.metMinimumMetric = new MetricObjective(dietType != 'Phase 2', daysIntoWeek);
			this.physicalActivityMetric = new MetricObjective(goalSettings.physicalActivityAvailable, goalSettings.physicalActivityMinimum);
			this.shakeMetric = new MetricObjective(goalSettings.shakesAvailable, goalSettings.shakesMinimum);
			this.waterMetric = new MetricObjective(true, 56); // not tracked with goals

			//handle goal amount
			if(goalRecord != null && goalRecord.GoalAmount__c != null){
				if(goalRecord.FocusArea__c == 'Entrees'){
					this.entreeMetric.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
				}
				else if(goalRecord.FocusArea__c == 'Shakes'){
					this.shakeMetric.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
				}
				else if(goalRecord.FocusArea__c == 'Physical Activity'){
					this.physicalActivityMetric.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
				}
				else if(goalRecord.FocusArea__c == 'Fruits & Vegetables'){
					this.fruitsAndVegetablesMetric.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
				}
				else if(goalRecord.FocusArea__c == 'HMR Foods'){
					this.hmrFoodsMetric.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
				}
			}

			//initialize
			this.mondayObjective = new DayObjective(goalSettings, false);
			this.tuesdayObjective = new DayObjective(goalSettings, false);
			this.wednesdayObjective = new DayObjective(goalSettings, false);
			this.thursdayObjective = new DayObjective(goalSettings, false);
			this.fridayObjective = new DayObjective(goalSettings, false);
			this.saturdayObjective = new DayObjective(goalSettings, false);
			this.sundayObjective = new DayObjective(goalSettings, false);

			this.weightLogList = new List<WeightMetric>();

			this.weightChartThreeWeek = new WeightChart();
			this.weightChartThreeMonth = new WeightChart();
			this.weightChartAllTime = new WeightChart();
		}

		public void calculateWeightCharts(Date dietWeekStartDate){
			Date endingChartDate = Date.today();
			String officialWeightDay = DateTime.newInstance(dietWeekStartDate, Time.newInstance(0,0,0,0)).format('EEEE');

			if (this.weightLogList.size() > 0) {
				Date threeWeekStart = endingChartDate.addDays(-21);
				Date threeMonthStart = endingChartDate.addMonths(-2);
				Date nextWeeklyLabelDate = endingChartDate.addDays(-21);
	            Date nextMonthlyLabelDate = endingChartDate.addMonths(-2);
	            Date earliestWeightLogDate = this.weightLogList[this.weightLogList.size() - 1].weightDate;

				Map<Date, WeightMetric> dateWeightMap = new Map<Date, WeightMetric>();

				List<Date> threeWeekList = new List<Date>();
				List<Date> threeMonthList = new List<Date>();

				//last known metric for gaps in weight logs
				WeightMetric lastKnownMetric = this.weightLogList[0];

				//put our weightLogList into a map
				for(WeightMetric weightLog : this.weightLogList){
					dateWeightMap.put(weightLog.weightDate, weightLog);
				}

				//get our full date lists as we want to plot all intermediate points
				for(integer i = 0; endingChartDate.addDays(i * -1) >= threeWeekStart && endingChartDate.addDays(i * -1) >= earliestWeightLogDate; i++){
					if(threeWeekList.size() > 0){
						threeWeekList.add(0, endingChartDate.addDays(i * -1));	
					}
					else{
						threeWeekList.add(endingChartDate.addDays(i * -1));
					}
					
				}

				if(threeWeekStart < earliestWeightLogDate){
					threeWeekStart = earliestWeightLogDate;
				}

				for(integer i = 0; endingChartDate.addDays(i * -1) >= threeMonthStart && endingChartDate.addDays(i * -1) >= earliestWeightLogDate; i++){
					if(threeMonthList.size() > 0){
						threeMonthList.add(0, endingChartDate.addDays(i * -1));
					}
					else{
						threeMonthList.add(endingChartDate.addDays(i * -1));
					}
				}

				if(threeMonthStart < earliestWeightLogDate){
					threeMonthStart = earliestWeightLogDate;
				}

				//get last known weight metric for three week
				Date iterator = threeWeekStart;

				while(iterator >= earliestWeightLogDate){
					if(dateWeightMap.get(iterator) != null){
						lastKnownMetric = dateWeightMap.get(iterator);
						break;
					}

					iterator = iterator.addDays(-1);
				}

				//populate three week data
				for(Date weekDate : threeWeekList){
					if(dateWeightMap.get(weekDate) != null){
						lastKnownMetric = dateWeightMap.get(weekDate);
					}

					this.weightChartThreeWeek.weightList.add(lastKnownMetric.weight);

					if(weekDate == nextWeeklyLabelDate || weekDate == earliestWeightLogDate || weekDate == endingChartDate){
						this.weightChartThreeWeek.dateLabelList.add(DateTime.newInstance(weekDate, Time.newInstance(0,0,0,0)).format('MM-dd-YY'));
						this.weightChartThreeWeek.officialWeightDayList.add(DateTime.newInstance(weekDate, Time.newInstance(0,0,0,0)).format('EEEE') == officialWeightDay);
						nextWeeklyLabelDate = nextWeeklyLabelDate.addDays(7);
					}
					else{
						this.weightChartThreeWeek.dateLabelList.add('');
						this.weightChartThreeWeek.officialWeightDayList.add(officialWeightDay == DateTime.newInstance(weekDate, Time.newInstance(0,0,0,0)).format('EEEE'));
					}
				}

				//clean up
				while(this.weightChartThreeWeek.dateLabelList.size() > 0 && this.weightChartThreeWeek.dateLabelList[0] == ''){
					this.weightChartThreeWeek.dateLabelList.remove(0);
					this.weightChartThreeWeek.weightList.remove(0);
					this.weightChartThreeWeek.officialWeightDayList.remove(0);
				}


				lastKnownMetric = this.weightLogList[0];

				//get last known weight metric for three month
				iterator = threeMonthStart;

				while(iterator >= earliestWeightLogDate){
					if(dateWeightMap.get(iterator) != null){
						lastKnownMetric = dateWeightMap.get(iterator);
						break;
					}

					iterator = iterator.addDays(-1);
				}

				//populate three month data
				for(Date monthDate : threeMonthList){
					if(dateWeightMap.get(monthDate) != null){
						lastKnownMetric = dateWeightMap.get(monthDate);
					}

					this.weightChartThreeMonth.weightList.add(lastKnownMetric.weight);
		
					if(monthDate == nextMonthlyLabelDate || monthDate == earliestWeightLogDate || (monthDate == endingChartDate && (monthDate.month() == nextMonthlyLabelDate.month() || monthDate.month() == earliestWeightLogDate.month()))){
						this.weightChartThreeMonth.dateLabelList.add(DateTime.newInstance(monthDate, Time.newInstance(0,0,0,0)).format('MMMM'));
						this.weightChartThreeMonth.officialWeightDayList.add(DateTime.newInstance(monthDate, Time.newInstance(0,0,0,0)).format('EEEE') == officialWeightDay);
						nextMonthlyLabelDate = monthDate.month() == nextMonthlyLabelDate.month() ? nextMonthlyLabelDate.addMonths(1) : monthDate.addMonths(1);
					}
					else{
						this.weightChartThreeMonth.dateLabelList.add('');
						this.weightChartThreeMonth.officialWeightDayList.add(DateTime.newInstance(monthDate, Time.newInstance(0,0,0,0)).format('EEEE') == officialWeightDay);
					}
				}

				//clean up
				while(this.weightChartThreeMonth.dateLabelList.size() > 0 && this.weightChartThreeMonth.dateLabelList[0] == ''){
					this.weightChartThreeMonth.dateLabelList.remove(0);
					this.weightChartThreeMonth.weightList.remove(0);
					this.weightChartThreeMonth.officialWeightDayList.remove(0);
				}

				lastKnownMetric = this.weightLogList[0];

				//populate all time data
				for(Date allTimeDate : dateWeightMap.keySet()){
					lastKnownMetric = dateWeightMap.get(allTimeDate);

					this.weightChartAllTime.weightList.add(0, lastKnownMetric.weight);

					this.weightChartAllTime.officialWeightDayList.add(0, DateTime.newInstance(allTimeDate, Time.newInstance(0,0,0,0)).format('EEEE') == officialWeightDay);
					
					if(allTimeDate == this.weightLogList[0].weightDate){
						this.weightChartAllTime.dateLabelList.add(0, DateTime.newInstance(endingChartDate, Time.newInstance(0,0,0,0)).format('MM-dd-YYYY'));
					}
					else if(allTimeDate == this.weightLogList[this.weightLogList.size() - 1].weightDate){
						this.weightChartAllTime.dateLabelList.add(0, DateTime.newInstance(allTimeDate, Time.newInstance(0,0,0,0)).format('MM-dd-YYYY'));
					}
					else{
						this.weightChartAllTime.dateLabelList.add(0, '');
					}
				}

				//remove the dummy elements that were added so we could use add(0, item) above
				this.weightChartAllTime.weightList.remove(this.weightChartAllTime.weightList.size() - 1);
				this.weightChartAllTime.dateLabelList.remove(this.weightChartAllTime.dateLabelList.size() - 1);
				this.weightChartAllTime.officialWeightDayList.remove(this.weightChartAllTime.officialWeightDayList.size() - 1);

                if(this.weightChartThreeWeek.weightList.size() > 0){
                	//Show ending and beginning as a plot
                	if(this.weightChartThreeWeek.officialWeightDayList[0] == null){
                		this.weightChartThreeWeek.officialWeightDayList[0] = false;
                	}

                	if(this.weightChartThreeWeek.officialWeightDayList[this.weightChartThreeWeek.officialWeightDayList.size() - 1] == null){
                		this.weightChartThreeWeek.officialWeightDayList[this.weightChartThreeWeek.officialWeightDayList.size() - 1] = false;
                	}
                	
                    this.weightChartThreeWeek.weightChange = this.weightChartThreeWeek.weightList[this.weightChartThreeWeek.weightList.size() - 1] - this.weightChartThreeWeek.weightList[0];
                }
                if(this.weightChartThreeMonth.weightList.size() > 0){
                	if(this.weightChartThreeMonth.officialWeightDayList[0] == null){
                		this.weightChartThreeMonth.officialWeightDayList[0] = false;
                	}
                	
                	if(this.weightChartThreeMonth.officialWeightDayList[this.weightChartThreeMonth.officialWeightDayList.size() - 1] == null){
                		this.weightChartThreeMonth.officialWeightDayList[this.weightChartThreeMonth.officialWeightDayList.size() - 1] = false;
                	}

                    this.weightChartThreeMonth.weightChange = this.weightChartThreeMonth.weightList[this.weightChartThreeMonth.weightList.size() - 1] - this.weightChartThreeMonth.weightList[0];
                }
                if(this.weightChartAllTime.weightList.size() > 0){
                	if(this.weightChartAllTime.officialWeightDayList[0] == null){
                		this.weightChartAllTime.officialWeightDayList[0] = false;
                	}
                	
                	if(this.weightChartAllTime.officialWeightDayList[this.weightChartAllTime.officialWeightDayList.size() - 1] == null){
                		this.weightChartAllTime.officialWeightDayList[this.weightChartAllTime.officialWeightDayList.size() - 1] = false;
                	}

                    this.weightChartAllTime.weightChange = this.weightChartAllTime.weightList[this.weightChartAllTime.weightList.size() - 1] - this.weightChartAllTime.weightList[0];
                }
            }
		}
	}

	public class DayObjective{
		public integer barTotal {get; set;}
		public integer entreeMinimum {get; set;}
		public integer fruitsAndVegetablesMinimum {get; set;}
		public integer hmrFoodsMinimum {get; set;}
		public integer physicalActivityMinimum {get; set;}
		public integer shakesMinimum {get; set;}
		public integer entreeTotal {get; set;}
		public integer fruitsAndVegetablesTotal {get; set;}
		public integer hmrFoodsTotal {get; set;}
		public integer physicalActivityTotal {get; set;}
		public integer shakesTotal {get; set;}
		public integer waterTotal {get; set;}
		public boolean inTheBox {get; set;}
		public boolean metMinimum {get{
			return this.entreeTotal >= entreeMinimum &&
				   this.fruitsAndVegetablesTotal >= fruitsAndVegetablesMinimum &&
				   this.hmrFoodsTotal >= hmrFoodsMinimum &&
				   //this.physicalActivityTotal >= physicalActivityMinimum &&
				   this.shakesTotal >= shakesMinimum; 
		}}

		public DayObjective(HMR_Goal_Service.GoalSetting goalSettings, boolean inTheBox){
			this.barTotal = 0;
			this.entreeMinimum = goalSettings.entreeMinimum == null ? 0 : goalSettings.entreeMinimum / 7;
			this.entreeTotal = 0;
			this.fruitsAndVegetablesMinimum = goalSettings.fruitsAndVegetablesMinimum == null ? 0 : goalSettings.fruitsAndVegetablesMinimum / 7;
			this.fruitsAndVegetablesTotal = 0;
			this.hmrFoodsMinimum = goalSettings.hmrFoodsMinimum == null ? 0 : goalSettings.hmrFoodsMinimum;
			this.hmrFoodsTotal = 0;
			this.physicalActivityMinimum = goalSettings.physicalActivityMinimum == null ? 0 : 300;
			this.physicalActivityTotal = 0;
			this.shakesMinimum = goalSettings.shakesMinimum == null ? 0 : goalSettings.shakesMinimum / 7;
			this.shakesTotal = 0;
			this.waterTotal = 0;
			this.inTheBox = inTheBox;
		}

		public void addTotals(ActivityLog__c activityLog){
			if(activityLog.BenefitBars__c != null){
				this.barTotal += Integer.valueOf(activityLog.BenefitBars__c);
			}

			if(activityLog.Entrees__c != null){
				this.entreeTotal += Integer.valueOf(activityLog.Entrees__c);
			}

			if(activityLog.FruitsVegetables__c != null){
				this.fruitsAndVegetablesTotal += Integer.valueOf(activityLog.FruitsVegetables__c);
			}

			if(activityLog.TotalMealReplacements__c != null){
				this.hmrFoodsTotal += Integer.valueOf(activityLog.TotalMealReplacements__c);
			}

			if(activityLog.TotalPhysicalActivityCaloriesRollUp__c != null){
				this.physicalActivityTotal += Integer.valueOf(activityLog.TotalPhysicalActivityCaloriesRollUp__c);
			}

			if(activityLog.ShakesCereals__c != null){
				this.shakesTotal += Integer.valueOf(activityLog.ShakesCereals__c);
			}

			if(activityLog.Water__c != null){
				this.waterTotal += Integer.valueOf(activityLog.Water__c);
			}
		}
	}

	public class MetricObjective{
		public boolean isTracked {get; set;}
		public integer minimum {get; set;}
		public integer goalAmount {get; set;}
		public integer total {get; set;}
		public integer percent {get{ 
			return goalAmount > 0 ? Math.min((Integer.valueOf((total * 100.0 / goalAmount).round(RoundingMode.HALF_UP))),999) : minimum > 0 ? Math.min(Integer.valueOf((total * 100.0 / minimum).round(RoundingMode.HALF_UP)),999) : 100;
		}}
		public integer percentBasedOnMinimum {get{
			return minimum > 0 ? Math.min(Integer.valueOf((total * 100.0 / minimum).round(RoundingMode.HALF_UP)),999) : 100;
		}}
		public Map<String, Integer> dailyObjective {get; set;}

		public MetricObjective(boolean isTracked, integer minimum){
			this.isTracked = isTracked;
			this.minimum = minimum == null ? 0 : minimum;
			this.goalAmount = 0;
			this.total = 0;
			this.dailyObjective = new Map<String, Integer>();
		}
	}

	public class WeightMetric{
		public String activityLogId {get; set;}
		public Date weightDate {get; set;}
		public decimal weight {get; set;}
		public decimal weightChangeFromPrevious {get; set;}

		public WeightMetric(String id, Date weightDate, decimal weight){
			this.activityLogId = id;
			this.weightDate = weightDate;
			this.weight = weight;
			this.weightChangeFromPrevious = 0.0;
		}
	}

	public class WeightChart{
		public decimal weightChange {get; set;}
		public List<String> dateLabelList {get; set;}
		public List<Boolean> officialWeightDayList {get; set;}
		public List<Decimal> weightList {get; set;}
		public decimal maximumYAxis {get {
			if(this.weightList.size() > 0){
				List<Decimal> sortedList = this.weightList.clone();
				sortedList.sort();
				return sortedList[sortedList.size() - 1] + 5;
				
			}
			else {
				return 0;
			}
		}}
		public decimal minimumYAxis {get {
			if(this.weightList.size() > 0){
				List<Decimal> sortedList = this.weightList.clone();
				sortedList.sort();
				return sortedList[0] - 5;
			}
			else {
				return 0;
			}
		}}

		public WeightChart(){
			this.weightChange = 0.0;
			this.dateLabelList = new List<String>{''};
			this.weightList = new List<Decimal>{0.0};
			this.officialWeightDayList = new List<Boolean>{false};
		}
	}
}