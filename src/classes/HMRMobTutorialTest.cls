/**
* Apex test class for coverage of HMRMobTutorial
* @Date: 2018-01-24
* @Author: Magnet 360 - Zach Engman
* @Modified: 2018-01-24
* @JIRA:
*/
@isTest
private class HMRMobTutorialTest {
	@testSetup
	private static void setupTestData(){
		User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;
	}

	@isTest
	private static void testUpdateOfTutorialCompletedFlag(){
		User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
    	RestRequest request = new RestRequest();
    	RestResponse response = new RestResponse();

    	request.requestURI = '/services/apexrest/mobile/user/tutorial/';
        request.params.put('hasCompletedTutorial', 'true');
    	request.httpMethod = 'PATCH';

    	RestContext.request = request;
    	RestContext.response = response;

    	String updateResult;

    	Test.startTest();

    	System.runAs(userRecord){
    		updateResult = HMRMobTutorial.updateTutorial(true);
    	}

    	Test.stopTest();

    	//Verify success
    	System.assertEquals(204, RestContext.response.statusCode);
	}

    @isTest
    private static void testUpdateOfTutorialCompletedFlagWithInvalidUser(){
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        String emailAddress = String.valueOf(System.now().getTime() + '@bad-cc-test.mail');
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User testOwner = [SELECT TimeZoneSidKey, UserRoleId FROM User WHERE Id = :UserInfo.getUserId()];
        User userRecord = new User(
                Alias                    = 'cctestB',
                Email                    = emailAddress,
                EmailEncodingKey         = 'UTF-8',
                LastName                 = 'TestUser1',
                LanguageLocaleKey        = 'en_US',
                LocaleSidKey             = 'en_US',
                ProfileId                = profile.Id,
                TimeZoneSidKey           = testOwner.TimeZoneSidKey,
                Username                 = emailAddress,
                isActive                 = true,
                ccrz__CC_CurrencyCode__c = NULL
        );

        insert userRecord;

        request.requestURI = '/services/apexrest/mobile/user/tutorial/';
        request.params.put('hasCompletedTutorial', 'true');
        request.httpMethod = 'PATCH';

        RestContext.request = request;
        RestContext.response = response;

        String updateResult;

        Test.startTest();

        System.runAs(userRecord){
            updateResult = HMRMobTutorial.updateTutorial(true);
        }

        Test.stopTest();

        //Verify bad request
        System.assertEquals(404, RestContext.response.statusCode);
    }
}