/*****************************************************
 * Author: Joey Zhuang
 * Created Date: 08 May 2017
 * Created By: Joey Zhuang
 * Last Modified Date: 01 July 2017
 * Last Modified By: Nathan Anderson - Added check for P2 1st Orders so they don't get dropped for no consent
 * Description:

 	Processes all the Orders
    	Only when the Order.Contact.Consent is checked true
	    	with status as Pending and Order Date of today (You can only do 10 callouts per batch Job)
	        What exactly is processing an order
	            Process a Cybersource Transaction
	                update existing transaction Payment Record with appropriate status
	                    Success
	                        Change the status of Order itself to "Order Submittted"

	                    Failure
	                        There is a count of Credit Card declines/Failures (Roll Up summary on the order record)
	    else
	    	update the Program Membership record related to the Order. You just set the New_Phase__c field to 'Drop' and the trigger will take over.
	    	update: Change Reason = "No Consent"

 * ****************************************************/
global class HMR_CC_PlaceOrderBatchJob implements Database.AllowsCallouts,Database.Batchable<sObject>{



	global Database.QueryLocator start(Database.BatchableContext BC) {
		string query = 'select Id,ccrz__TotalAmount__c, ccrz__OrderStatus__c, hmr_Order_Type_c__c, ccrz__OrderDate__c,ccrz__Contact__c, ccrz__User__c, ccrz__Contact__r.hmr_Consent_Form__c, ccrz__Contact__r.Community_User_ID__c, hmr_Program_Membership__c, hmr_Program_Membership__r.hmr_Membership_Type__c, hmr_Program_Membership__r.New_Phase__c, hmr_Program_Membership__r.hmr_Status_Change_Reason__c, ';
		query += '(Select ccrz__Token__c From ccrz__TransactionPayments__r where ccrz__Token__c != null) ';
		query += 'from ccrz__E_Order__c ';
		query += 'where ccrz__OrderDate__c = TODAY ' ;
		query += ' and ccrz__OrderStatus__c =\'Pending\' ';
		//system.debug('0((((('+query);
		return Database.getQueryLocator(query);
	}

	public map<string,string> testResponse {get;set;}

   	global void execute(Database.BatchableContext BC, List<ccrz__E_Order__c> scope) {
   		list<ccrz__E_Order__c> orderList = new list<ccrz__E_Order__c>();
   		set<Program_Membership__c> pmList = new set<Program_Membership__c>();
   		list<ccrz__E_TransactionPayment__c> tpList = new list<ccrz__E_TransactionPayment__c>();
   		list<ccrz__E_TransactionPayment__c> newTPList = new list<ccrz__E_TransactionPayment__c>();

   		system.debug('1(((('+scope.size());
   		for(ccrz__E_Order__c o : scope){
			//Check to make sure the client is consented
			//Also process P2 1st orders with delayed dates -- consent not required for these orders
			//1-18-2018 NA removed the P2 Reorder SD exclusion from the below if -- || (o.hmr_Order_Type_c__c == 'P2 Reorder' && o.hmr_Program_Membership__r.hmr_Membership_Type__c == 'Self-Directed')
   			if(o.ccrz__Contact__r.hmr_Consent_Form__c || o.hmr_Order_Type_c__c == 'P2 1st Order'){
   				if(o.ccrz__TransactionPayments__r.size()>0){
   					try{
		   				//prepare parameter for
		   				string userId = o.ccrz__Contact__r.Community_User_ID__c;
		   				string token = o.ccrz__TransactionPayments__r[0].ccrz__Token__c;
		   				string endpoint = Label.HMR_Cybersource_SilentOrderPayEndPoint;
		   				string reorderAmt='0';
		   				if(o.ccrz__TotalAmount__c != null){
		   					reorderAmt = String.valueOf(o.ccrz__TotalAmount__c);
		   				}
		   				HMR_Cybersource_Utility.authorizeUsingPaymentTokenForReorder(userId, null,token,reorderAmt);
		   				string postString = HMR_Cybersource_Utility.generatePOSTString();

						system.debug('2(((('+postString);

						//start the post
						HttpRequest req = new HttpRequest();
						req.setEndpoint(endPoint);
						req.setMethod('POST');
						req.setBody(postString);//postString

						Http http = new Http();
						HTTPResponse res;
						if(!Test.isRunningTest()){
							res = http.send(req);
						}else{
							res = new HTTPResponse();
							res.setStatusCode(200);
						}


						//if submit payment successfull
						system.debug('Anydatatype_msg++_+_'+res.getStatusCode() );
						if(res.getStatusCode() == 200){
							map<string,string> responseMap;
							if(!Test.isRunningTest()){
								responseMap = HMR_Cybersource_Utility.parseHTMLResponse(res.getBody());
							}else{
								responseMap = testResponse;
							}
							for(string s : responseMap.keySet())
							system.debug('5(((('+s+'='+responseMap.get(s));

							system.debug('33(((('+responseMap.get('message'));
							system.debug('55(((('+responseMap.get('decision'));

							if(responseMap.get('decision') == 'ACCEPT'){
								//update the order
								o.ccrz__OrderStatus__c = 'Order Submitted';
								orderList.add(o);

								//update the transaction payment record
								ccrz__E_TransactionPayment__c tp = o.ccrz__TransactionPayments__r[0];
								tp.ccrz__AccountNumber__c = responseMap.get('req_card_number');
								tp.ccrz__AccountType__c = 'cc';
								tp.Card_Type__c = Decimal.valueOf(responseMap.get('req_card_type'));
								tp.ccrz__Amount__c = Decimal.valueOf(responseMap.get('auth_amount'));
								tp.ccrz__RequestAmount__c = Decimal.valueOf(responseMap.get('req_amount'));
								tp.ccrz__Comments__c = responseMap.get('message');
								tp.ccrz__ExpirationMonth__c = Decimal.valueOf(responseMap.get('req_card_expiry_date').substring(0, 2));
								tp.ccrz__ExpirationYear__c = Decimal.valueOf(responseMap.get('req_card_expiry_date').substring(3, 7));
								//tp.ccrz__PaymentType__c = responseMap.get('req_payment_method');
								if(responseMap.get('payment_token')!=null && responseMap.get('payment_token') != ''){
									tp.ccrz__Token__c = responseMap.get('payment_token');
								}
								tp.ccrz__TransactionCode__c = responseMap.get('req_transaction_uuid');
								tp.ccrz__transactionSubcode__c = responseMap.get('reason_code');
								tp.ccrz__transactionType__c = responseMap.get('req_transaction_type');
								tp.ccrz__verificationCode__c = responseMap.get('decision')+ ',' + responseMap.get('auth_code') + ',' + responseMap.get('auth_trans_ref_no') + ',' + responseMap.get('auth_avs_code');
								tpList.add(tp);
							}else if(responseMap.get('decision') != '' && responseMap.get('decision') != null){
								//insert a separate TP record, with decline
								ccrz__E_TransactionPayment__c newTP = new ccrz__E_TransactionPayment__c();
								newTP.ccrz__CCOrder__c = o.Id;
								newTP.ccrz__Contact__c = o.ccrz__Contact__c;
								newTP.ccrz__User__c = o.ccrz__User__c;
								newTP.ccrz__AccountNumber__c = responseMap.get('req_card_number');
								newTP.ccrz__AccountType__c = 'cc';
								//newTP.Card_Type__c = Decimal.valueOf(responseMap.get('req_card_type'));
								//newTP.ccrz__Amount__c = Decimal.valueOf(responseMap.get('auth_amount'));
								//newTP.ccrz__RequestAmount__c = Decimal.valueOf(responseMap.get('req_amount'));
								newTP.ccrz__Comments__c = responseMap.get('message');
								//newTP.ccrz__PaymentType__c = responseMap.get('req_payment_method');
								newTP.ccrz__transactionSubcode__c = responseMap.get('reason_code');
								newTP.ccrz__verificationCode__c = responseMap.get('decision');
								newTPList.add(newTP);

								o.ccrz__OrderDate__c = Date.today().addDays(1);
								orderList.add(o);
							}

						}else{

						}
					}catch(Exception e){
						system.debug('Exception 1message:'+ e.getMessage());
					}

				}
   			}else{
				//For reorders where client has not consented as of the processing date, drop the client from AD
				pmList.add(new Program_Membership__c(Id = o.hmr_Program_Membership__c, hmr_Status_Change_Reason__c = 'No Consent', New_Phase__c = 'Drop'));
   			}
   		}

		try{
			insert newTPList;
			update tpList;
			update orderList;
			list<Program_Membership__c> pml = new list<Program_Membership__c>();
			pml.addAll(pmList);
			update pml;
		}catch(Exception e){
			system.debug('Exception message:'+ e.getMessage());
		}

	}

	global void finish(Database.BatchableContext BC) {

	}

}