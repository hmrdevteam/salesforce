/**
*
*
* @Date: 2017-08-02
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: Zach Engman 2017-08-03
* @JIRA:
*/
global virtual with sharing class HMR_CMS_SuccessStory_ContentTemplate extends cms.ContentTemplateController{
  //Constructors (With and Without Parameters)
  global HMR_CMS_SuccessStory_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }

    global HMR_CMS_SuccessStory_ContentTemplate() {
        super();
    }

    global virtual override String getHTML() {
        String sitePrefix = Site.getBaseUrl();
        String html = '';

        html += '<div class="success-block">';
            html += '<div class="container-fluid block-container">';
                html += '<div class="row hide-sm">';
                    html += '<div class="blocks">';
                        html += '<div class="block blue">';
                            html += '<div class="quote">';
                                html += 'I was never hungry';
                                    html += '<div class="name">';
                                       html += ' ~ Brad';
                                    html += '</div>';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block">';
                            html += '<div class="success-container">';
                                html += '<div class="block">';
                                    html += '<div class="title">';
                                        html += 'Success Stories';
                                    html += '</div>';
                                    html += '<div class="desc">';
                                        html += 'From 33 lbs. to over 300 lbs., HMR\'s programs have helped hundreds and thousands of people lose weight and start living a healthier lifestyle. See a few of their stories here.';
                                    html += '</div>';
                                    html += '<div class="link">';
                                          html += '<div class="hmr-allcaps-container">';
                                              html += '<a data-gtm-id="homeBlock3" href="/success-stories-see-all" class="hmr-allcaps hmr-allcaps-blue">SEE ALL STORIES</a>';
                                              html += '<a data-gtm-id="homeBlock3" href="/success-stories-see-all" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>';
                                          html += '</div>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/KevinLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block blue hide-md">';
                            html += '<div class="quote">';
                                html += 'It\'s so easy to follow';
                                    html += '<div class="name">';
                                       html += ' ~ Jessica';
                                    html += '</div>';
                                html += '</div>';
                        html += '</div>';
                        html += '<div class="block hide-lg">';
                            html += '<div class="parent">';
                                html += '<img class="center-img"  src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/JessicaLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                html += '<div class="row hide-sm">';
                    html += '<div class="blocks">';
                        html += '<div class="block">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/BradLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block">';
                        html += '</div>';
                        html += '<div class="block">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/ChristieLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block hide-md">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/RichardLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block hide-lg">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/TerriLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                html += '<div class="row hide-sm">';
                    html += '<div class="blocks">';
                       html += ' <div class="block">';
                           html += ' <div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/KarenLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block blue">';
                            html += '<div class="quote">';
                                html += 'The weight came off fast';
                                    html += '<div class="name">';
                                        html += '~ Jenny';
                                    html += '</div>';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/JennyLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block hide-md">';
                            html += '<div class="parent">';
                                html += '<img class="center-img" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/JulieLarge.jpg" />';
                            html += '</div>';
                        html += '</div>';
                        html += '<div class="block blue hide-lg">';
                            html += '<div class="quote">';
                                html += 'Now I finally get it';
                                    html += '<div class="name">';
                                        html += '~ Terri';
                                    html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                html += '<div class="row show-sm">';
                    html += '<div class="blocks">';
                        html += '<img class="block" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/BradLarge.jpg" />';
                        html += '<img class="block" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/KevinLarge.jpg" />';
                    html += '</div>';
                html += '</div>';
                html += '<div class="row show-sm">';
                    html += '<div class="blocks">';
                        html += '<img class="block" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/KarenLarge.jpg" />';
                        html += '<img class="block" src="https://www.myhmrprogram.com/ContentMedia/SuccessStories/JennyLarge.jpg" />';
                    html += '</div>';
                html += '</div>';
                html += '<div class="row show-sm">';
                    html += '<div class="blocks">';
                        html += '<div class="block success">';
                            html += '<div class="success-container">';
                                html += '<div class="block">';
                                    html += '<div class="title">';
                                        html += 'Success Stories';
                                    html += '</div>';
                                    html += '<div class="desc">';
                                        html += 'From 33 lbs. to over 300 lbs., HMR\'s programs have helped hundreds and thousands of people lose weight and start living a healthier lifestyle. See a few of their stories here.';
                                    html += '</div>';
                                    html += '<div class="link">';
                                      html += '<div class="link-container">';
                                          html += '<div class="hmr-allcaps-container">';
                                              html += '<a data-gtm-id="homeBlock3" href="/success-stories-see-all" class="hmr-allcaps hmr-allcaps-blue">SEE ALL STORIES</a>';
                                              html += '<a data-gtm-id="homeBlock3" href="/success-stories-see-all" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&gt;</a>';
                                          html += '</div>';
                                      html += '</div>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';

        return html;
    }
}