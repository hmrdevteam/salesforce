/**
* CloudCraze Credit Card Payment Pay Page Controller
*
* @Date: 2017-03-26
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual with sharing class HMR_CC_Pmt_CreditCard_Pay_Ctrl {
    public String blank { get{ return ''; } set; }
	public String formAction { get; set; }
    public Map<String, String> responseCC { get; set; }
    public Set<String> responseKeys { get; set; }
    public Map<String, Object> signedFields { get; set; }
    public Map<String, Object> unsignedFields { get; set; }
    public List<String> signedFieldNames { get; set; }
    public List<String> unsignedFieldNames { get; set; }
    public String signedDtString { get; set; }
    global String iframe { get; set; }
    global String iframeParameters { get; set; }
    global String joinedSignedFields  { get; set; }
    global String joinedUnsignedFields  { get; set; }
    public String qa_access_key = '710f4c18b9e03cd5ab55efcf22e8ed80';
    public String qa_profile_id = '97E799F4-B5E4-4950-A9F1-49CAC4AFF158';
    public String access_key = 'b1b5fa1bfbef31a8a05a48d2e3931f44';
    public String profile_id = '3E88176B-03A7-42B1-81A1-47762F40559F';
    public String finalDataForSignatureCC {get; set;}
    global Boolean isCybersourceReponse { get; set; }
    public String currentPageURL { get; set; }

    public Map<String,String> currentPageURLPageRefParams { get; set; }

    public Boolean isCustomerLoggedIn { get; set; }
    public String portalUser { get; set; }

    public Map<String, String> signedFieldValues = new Map<String, String>{
        'transaction_type' => 'authorization',
        //'transaction_type' => 'create_payment_token',
        'amount' => '100.00',
        'currency' => 'USD',
        'payment_method' => 'card',
        'bill_to_forename' => 'John',
        'bill_to_surname' => 'Doe',
        'bill_to_email' => 'null@cybersource.com',
        'bill_to_phone' => '02890888888',
        'bill_to_address_line1' => '1 Card Lane',
        'bill_to_address_city' => 'My City',
        'bill_to_address_state' => 'CA',
        'bill_to_address_country' => 'US',
        'bill_to_address_postal_code' => '94043',
        'locale' => 'en-us'
    };

    public String getReferenceTime() {
        DateTime current = System.now();
        Long timeInMili = current.getTime();
        return String.valueOf(timeInMili);
    }

    public static String uuid() {
        final String hex = EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        return hex.SubString(0,8)+ '-' + hex.SubString(8,12) + '-' + hex.SubString(12,16) + '-' + hex.SubString(16,20) + '-' + hex.substring(20);
    }

    public static String signData(final String token) {
        String secretKey = '5f29e7ec7bfa4d43a049967662557b9612db95a31fad4c2caffbacc03e50e55dec7a7a3479de49e0811e2d4e30568f5ed51a30fdd3944fc1a3aec6bcd014f1619e46d562985f413693fb3ba14dabdafd6118b0adca334541a421886b5036fe8ffb277e97c6314e76934368bf0889c28634c616f921d94e4e91b8d9223b7edb51';
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(token), Blob.valueOf(secretKey)));
    }

    public static String signQAData(final String token) {
        String qa_secretKey = '38406743772b4efb936a5a09db79086f7e2fecb534e04889a166bb750bd8d6dfb8bc25103de84751be28de61301239e7c46cd19988bb48c290df4cdee58512e0e09951d815cd45aea1080410ed83c67ff6228c171dd94ef39d43426f550685d17f8ae0a9e09f4bccbe99b5fd7ed184837b366c2882e54668874b287d8a38b232';
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(token), Blob.valueOf(qa_secretKey)));
    }

    public String getUTCDateTime(){
        DateTime oUTSDateTime = DateTime.now();//.addMinutes(362);
        signedDtString = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'GMT');
        String strUTCDateTime = signedDtString;
        return strUTCDateTime;
    }

	public HMR_CC_Pmt_CreditCard_Pay_Ctrl() {
        signedFieldNames = cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN;
        /*
            Plesae do not delete this code
            if(ApexPages.currentPage().getHeaders().get('Referer') != null) {
                portalReferer = ApexPages.currentPage().getHeaders().get('Referer');
                PageReference pageReference = new PageReference(portalReferer);
                Map<String,String> parameters = pageReference.getParameters();
                if(parameters.get('portalUser') != null && parameters.get('portalUser') != '') {
                    String p = parameters.get('portalUser');
                    Id portalUserId = [SELECT Id, Name FROM User WHERE Id = :p].Id;
                    portalUser = String.valueOf(portalUserId);
                }
                else {
                    if(pageReference.getUrl().contains('visual.force.com')) {
                        portalUser = UserInfo.getUserId();
                    }
                }
            }
        */

        Map<String, String> currentPageParams = ApexPages.currentPage().getParameters();

        if(currentPageParams.get('currentPageURL') != null) {
            currentPageURL = currentPageParams.get('currentPageURL');
        }
        if(currentPageParams.get('portalUserId') != null && currentPageParams.get('portalUserId') != '') {
            portalUser = currentPageParams.get('portalUserId');
            signedFieldValues.put('merchant_secure_data1', portalUser);
        }
        else {
            if(currentPageURL.contains('visual.force.com')) {
                portalUser = UserInfo.getUserId();
            }
        }

        if(currentPageParams.get('currentCartId') != null && currentPageParams.get('currentCartId') != '') {
            signedFieldValues.put('merchant_secure_data2', currentPageParams.get('currentCartId'));
        }

        PageReference currentPageURLPageRef = new PageReference(currentPageURL);
        currentPageURLPageRefParams = currentPageURLPageRef.getParameters();

        /*if(currentPageURLPageRefParams.get('isCSRFlow') != null && currentPageURLPageRefParams.get('isCSRFlow') != '') {
            signedFieldValues.put('merchant_secure_data3', currentPageURLPageRefParams.get('isCSRFlow'));
        }*/

        if(currentPageURLPageRefParams.get('isCybersourceResponseFlow') != null) {
            isCybersourceReponse = true;
        }
        else {
            isCybersourceReponse = false;
        }

        if(portalUser == null) {
            isCustomerLoggedIn = true;
            signedFieldValues.put('access_key', access_key);
            signedFieldValues.put('profile_id', profile_id);
        }
        else {
            isCustomerLoggedIn = false;
            signedFieldValues.put('access_key', qa_access_key);
            signedFieldValues.put('profile_id', qa_profile_id);
            signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_MERCHANT_SECURE_DATA1);
            signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_MERCHANT_SECURE_DATA2);
            //signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_MERCHANT_SECURE_DATA3);
        }
		signedFieldValues.put('transaction_uuid', uuid());
        signedFieldValues.put('signed_date_time', getUTCDateTime());
        signedFieldValues.put('reference_number', getReferenceTime());
        this.signedFields = new Map<String, String>();
        this.unsignedFields = new Map<String, String>();

        // signed fields

        // optional fields: state(US)phone
        //signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_STATE);
        signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_BILLTO_PHONE);

        //for(Integer i=1;i<=19;i++) signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_MERCHANT_DEFINED_DATA + i);
        // sort for predictable order
        signedFieldNames.sort();
        // seed with blank values
        for (String s : signedFieldNames) {
            if(signedFieldValues.get(s) != null) {
                signedFields.put(s, (Object)signedFieldValues.get(s));
            }
            else {
                signedFields.put(s,'');
            }
        }

        // list fields
        signedFields.put(cc_pgcs_ApiConstants.CS_REQ_SIGNEDFIELDNAMES, String.join(signedFieldNames,','));
        //signedFields.put('signed_field_names', 'access_key,amount,bill_add_1,bill_to_phone')

        unsignedFieldNames = cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED;
        // optional fields: CVN
        unsignedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_CARDCVN);
        // sort for predictable order
        unsignedFieldNames.sort();
        // seed with blank values
        for (String u : unsignedFieldNames) {
            unsignedFields.put(u,'');
        }
        // list fields
        signedFields.put(cc_pgcs_ApiConstants.CS_REQ_UNSIGNEDFIELDNAMES, String.join(unsignedFieldNames,','));

        //signedFields.put('unsigned_field_names', 'card_number,card_cvn,card_expiry_date')

        List<String> signedFieldNamesArray = ((String)signedFields.get('signed_field_names')).Split(',');
        List<String> dataToSign = new List<String>();

        //access_key=key,amount=100.0,bill_add_1=9136con,

        for (String signedFieldName: signedFieldNamesArray){
            dataToSign.add(signedFieldName + '=' + signedFields.get(signedFieldName));
            //dataToSign.add('access_key=deb2f6ab757b3ad099322f4eea87446b');
            //dataToSign.add('amount=100.0');
        }

        this.finalDataForSignatureCC = String.join(dataToSign, ',');
        //access_key=deb2f6ab757b3ad099322f4eea87446b,amount=100.00,bill_to_address_city=My City,bill_to_address_country=US,bill_to_address_line1=1 Card Lane,bill_to_address_postal_code=94043,bill_to_address_state=CA,bill_to_email=null@cybersource.com,bill_to_forename=John,bill_to_phone=02890888888,bill_to_surname=Doe,currency=USD,locale=en-us,payment_method=card,profile_id=84E8F595-8136-4021-BD17-6BAB936483DC,reference_number=1491337944333,signed_date_time=2017-04-04T20:32:24Z,signed_field_names=access_key,amount,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_postal_code,bill_to_address_state,bill_to_email,bill_to_forename,bill_to_phone,bill_to_surname,currency,locale,payment_method,profile_id,reference_number,signed_date_time,signed_field_names,transaction_type,transaction_uuid,unsigned_field_names,transaction_type=authorization,transaction_uuid=d163d7b2-28f6-8002-5de6-8f4da5009fe8,unsigned_field_names=card_cvn,card_expiry_date,card_number,card_type

        formAction = 'https://testsecureacceptance.cybersource.com/silent/pay';

        this.iframe = Site.getBaseUrl() + '/c__HMR_CC_Cybersource_RequestForm';
        this.iframeParameters = '';
        this.iframeParameters += 'formAction=' + formAction + '&selector=sopccFields';
        this.iframeParameters += '&signed=' + String.join(signedFieldNames, ',');
        this.iframeParameters += '&unsigned=' + String.join(unsignedFieldNames, ',');

        //https://qa-hmrprogram.cs71.force.com/DefaultStore/c__HMR_CC_Cybersource_RequestForm?
        //formAction=https://testsecureacceptance.cybersource.com/silent/pay&selector=sopccFields
        //&signed=access_key,amount,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_postal_code,bill_to_address_state,bill_to_email,bill_to_forename,bill_to_phone,bill_to_surname,currency,locale,payment_method,profile_id,reference_number,signed_date_time,signed_field_names,transaction_type,transaction_uuid,unsigned_field_names
        //&unsigned=card_cvn,card_expiry_date,card_number,card_type

        joinedSignedFields = String.join(signedFieldNames, ',');
        joinedUnsignedFields = String.join(unsignedFieldNames, ',');

        System.currentPageReference().getParameters().put('joinedSignedFields', joinedSignedFields);
        System.currentPageReference().getParameters().put('joinedUnsignedFields', joinedUnsignedFields);
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult getSignature(final ccrz.cc_RemoteActionContext ctx, final String dataForSignatureCC, final Boolean isCustomerLoggedIn) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
	    ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
	    result.inputContext = ctx;
		ccrz.ccLog.log(System.LoggingLevel.INFO,'M:E','getSignature');
		//ccrz.ccLog.log(System.LoggingLevel.INFO,'D:jsonData',jsonData);
		try {
			String signature;
            if(isCustomerLoggedIn) {
                signature = signData(dataForSignatureCC);
            }
            else {
                signature = signQAData(dataForSignatureCC);
            }
			Map<String, Object> signResult = new Map<String,Object> {
				'data' => signature
			};
			result.data = signResult.get('data');
			result.success = true;
		}
		catch (Exception ex) {
			ccrz.ccLog.log(System.LoggingLevel.ERROR, 'ERR', ex);
			result.success = false;
		}
		finally {
			ccrz.ccLog.log(System.LoggingLevel.INFO, 'M:X', 'getSignature');
			ccrz.ccLog.close(result);
		}
		return result;
	}
}