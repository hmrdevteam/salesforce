global with sharing class HMR_CMS_CCRZ_Checkout_ServiceInterface implements cms.ServiceInterface {

	global String executeRequest(Map<String, String> params) {
		Map<String, Object> returnMap = new Map<String, Object>();

		if(params.get('userId') != null && params.get('isGuest') != null){
            HMR_CMS_Checkout_Service checkoutService;
            string userId = String.escapeSingleQuotes(params.get('userId'));
            Boolean isGuest = Boolean.valueOf(params.get('isGuest'));
            String encryptedCartId = '';

            if(params.get('encryptedCartId') != null){
                encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                checkoutService = new HMR_CMS_Checkout_Service(userId, isGuest, encryptedCartId);
            }
            else
                checkoutService = new HMR_CMS_Checkout_Service(userId, isGuest);

        	try {
                String action = params.get('action');
                returnMap.put('success', true);
                //Return the calling function/action
                returnMap.put('action', action);

                if(action == 'getTerms') {
                    returnMap.put('terms', checkoutService.fetchTerms());
                }
                else if (action == 'getCartByEncryptedId') {
                	returnMap.put('cart', checkoutService.getCartByEncryptedId(encryptedCartId));
                }
                else if(action == 'getCartByUser'){
                    HMR_Cart_Service.CartRecord cartRecord = checkoutService.getCartByUserId(userId);
                    returnMap.put('cart', cartRecord);
                }
                else if (action == 'getCartAddresses') {
                	returnMap.put('cartAddresses', checkoutService.getCartAddresses(encryptedCartId));
                }
                else if (action == 'getCartBillingAddress') {
                	returnMap.put('cartBillingAddress', checkoutService.getCartBillingAddress(encryptedCartId));
                }
                else if (action == 'getCartShippingAddress') {
                	returnMap.put('cartBillingAddress', checkoutService.getCartShippingAddress(encryptedCartId));
                }
                else if (action == 'checkForAutomaticCoupon'){
                    HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);
                    HMR_Cart_Service.CartRecord cartRecord = cartService.checkForAutomaticCoupon(encryptedCartId);
                    returnMap.put('cart', cartRecord);
                }
                else if (action == 'createCartAddress') {
                    Map<String, Object> addressData = (Map<String, Object>) JSON.deserializeUntyped(params.get('addressData'));
                    String addressType = params.get('addressType');
                    returnMap.put('creatCartAddressResult', checkoutService.createCartAddress(addressData, encryptedCartId, addressType));
                }
                else if (action == 'getUserAddresses') {
                	String addressType = params.get('addressType');
                	returnMap.put('addressBookResult', checkoutService.getUserAddresses(userId, addressType));
                }
                else if (action == 'getShippingOptions'){
                    HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);
                    String shippingAddressJSON = params.get('shippingAddressData');
                    returnMap.put('shippingOptions', cartService.getShippingOptions(encryptedCartId, shippingAddressJSON));
                }
                else if (action == 'getStoredPayments'){
                    HMR_StoredPayment_Service storedPaymentService = new HMR_StoredPayment_Service(userId);
                    returnMap.put('storedPayments', storedPaymentService.getByOwnerId(userId));
                }
                else if (action == 'placeOrder'){
                    HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);
                    string transactionPaymentJSON = String.escapeSingleQuotes(params.get('paymentData'));
                    string orderId = cartService.placeOrder(encryptedCartId, transactionPaymentJSON);

                    returnMap.put('success', !String.isBlank(orderId));
                    returnMap.put('orderId', orderId);
                }
                else if (action == 'updateAddress') {
                    Map<String, Object> addressData = (Map<String, Object>) JSON.deserializeUntyped(params.get('addressData'));
                    returnMap.put('addressUpdateResult', checkoutService.updateAddress(addressData));
                }
                else if (action == 'updateBuyerInformation'){
                    HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);
                    string buyerFirstName = String.escapeSingleQuotes(params.get('buyerFirstName'));
                    string buyerLastName = String.escapeSingleQuotes(params.get('buyerLastName'));
                    string buyerEmail = String.escapeSingleQuotes(params.get('buyerEmail'));
                    string buyerPhone = String.escapeSingleQuotes(params.get('buyerPhone'));

                    returnMap.put('cart', cartService.updateBuyerInformation(encryptedCartId, buyerFirstName.unescapeJava(), buyerLastName.unescapeJava(), buyerEmail, buyerPhone));
                }
                else if (action == 'updateShippingMethod'){
                    HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);
                    string shippingMethod = String.escapeSingleQuotes(params.get('shippingMethod'));
                    decimal shippingCharge = Decimal.valueOf(params.get('shippingCharge'));

                    returnMap.put('cart', cartService.updateShippingMethod(encryptedCartId, shippingMethod, shippingCharge));
                }
                else if(action == 'saveCCTerms'){
                    string contactId = String.escapeSingleQuotes(params.get('contactId'));
                    List<String> ccTermIdList = (List<String>)JSON.deserialize(params.get('ccTermIdList'), List<String>.class);
                    Boolean isCSRFlow = Boolean.valueOf(params.get('isCSRFlow'));

                    returnMap = checkoutService.saveCCTerms(contactId, ccTermIdList, isCSRFlow);
                }
                else if(action == 'authorizePayment'){
                    Map<String, String> userAddressMap = parseUserAddressMap(params);
                    Map<String, String> unsignedFieldMap = parseUnsignedFieldMap(params);
                    returnMap = checkoutService.authorizePayment(userAddressMap, unsignedFieldMap);
                }
                else if(action == 'authorizePaymentAndCreatePaymentToken'){
                    Map<String, String> userAddressMap = parseUserAddressMap(params);
                    Map<String, String> unsignedFieldMap = parseUnsignedFieldMap(params);
                    returnMap = checkoutService.authorizePaymentAndCreatePaymentToken(userAddressMap, unsignedFieldMap);
                }
                else if(action == 'authorizePaymentToken'){
                    Map<String, String> userAddressMap = parseUserAddressMap(params);
                    String paymentToken = String.escapeSingleQuotes(params.get('paymentToken'));
                    returnMap = checkoutService.authorizeUsingPaymentToken(userAddressMap, paymentToken);
                }
                else if(action == 'createPaymentTokenForP2'){
                    Map<String, String> userAddressMap = parseUserAddressMap(params);
                    Map<String, String> unsignedFieldMap = parseUnsignedFieldMap(params);
                    returnMap = checkoutService.createPaymentTokenForP2(userAddressMap, unsignedFieldMap);
                }
                else if(action == 'createStoredPayment'){
                    Map<String, String> paymentInformationMap = parsePaymentInformationMap(params);
                    returnMap = checkoutService.createStoredPayment(paymentInformationMap);
                }
                else if(action == 'getCCTerms'){
                    string contactId = String.escapeSingleQuotes(params.get('contactId'));
                    List<String> productIdList = (List<String>)JSON.deserialize(params.get('productIdList'), List<String>.class);
                    returnMap = checkoutService.getCCTerms(contactId, productIdList);
                }
                else if(action == 'getExtendedDataForProducts'){
                    List<String> productIdList = (List<String>)JSON.deserialize(params.get('productIdList'), List<String>.class);
                    returnMap = checkoutService.getExtendedDataForProducts(productIdList);
                }
                else if(action == 'getStoredPaymentDetails'){
                    string storedPaymentId = String.escapeSingleQuotes(params.get('paymentId'));
                    returnMap = checkoutService.getStoredPaymentDetails(storedPaymentId);
                }
                else if(action == 'updateCartForP2'){
                    Date p2ProcessDate = Date.valueOf(params.get('p2ProcessDate'));
                    returnMap = checkoutService.updateCartForP2(encryptedCartId, p2ProcessDate);
                }
            }
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'userId and isGuest parameters required');
        }

		return JSON.serialize(returnMap);
	}

    private static Map<String, String> parsePaymentInformationMap(Map<String, String> params){
        Set<String> propertySet = new Set<String>{'displayName'
                                                 ,'accountNumber'
                                                 ,'token'
                                                 ,'paymentToken'
                                                 ,'expirationMonth'
                                                 ,'expirationYear'
                                                 ,'paymentType'
                                                 ,'contactFirstName'
                                                 ,'contactLastName'
                                                 ,'contactAddressId'};

        return parseMapValues(propertySet, params);
    }

    private static Map<String, String> parseUnsignedFieldMap(Map<String, String> params){
        Set<String> propertySet = new Set<String>{'card_number'
                                                 ,'card_type'
                                                 ,'card_expiry_date'
                                                 ,'card_cvn'
                                                 ,'card_number'};

        return parseMapValues(propertySet, params);
    }

    private static Map<String, String> parseUserAddressMap(Map<String, String> params){
        Set<String> propertySet = new Set<String>{'buyerFirstName'
                                                 ,'buyerLastname'
                                                 ,'buyerEmail'
                                                 ,'bill_to_email'
                                                 ,'bill_to_forename'
                                                 ,'bill_to_surname'
                                                 ,'bill_to_address_line1'
                                                 ,'bill_to_address_line2'
                                                 ,'bill_to_address_city'
                                                 ,'bill_to_address_state'
                                                 ,'bill_to_address_state_code'
                                                 ,'bill_to_address_country'
                                                 ,'bill_to_address_postal_code'};
        return parseMapValues(propertySet, params);
    }

    private static Map<String, String> parseMapValues(Set<String> propertySet, Map<String, String> params){
        Map<String, String> returnMap = new Map<String, String>();

        for(String property : propertySet){
            if(params.get(property) != null)
                returnMap.put(property, (String.escapeSingleQuotes(params.get(property))).unescapeJava());
            else
                returnMap.put(property, null);
        }

        return returnMap;
    }

	global static Type getType() {
        return HMR_CMS_CCRZ_Checkout_ServiceInterface.class;
    }
}