/**
* Unit Test for SubstituteCoachSearchController 
*
* Objects referenced --> User, Account, Contact, Coach__c, Program__c, Class__c, Coaching_Session__c
*
* @Date: 12/8/2016 
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA: Issue https://reside.jira.com/browse/HPRP-631, Task https://reside.jira.com/browse/HPRP-684
*/



@isTest
private Class SubstituteCoachSearchControllerTest{

    static testMethod void firstTestMethod(){
        
        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userObj = new User(Alias = 'sSCTe', Email='standuser321_SCTest@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj;     
        
        
        Account testAccountObj = new Account(name = 'test Account SCTest');
        insert testAccountObj;
        
        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact SCTest', Account = testAccountObj);
        insert testContatObj;           
        
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test_SCTest@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;
        
        Program__c programObj = new Program__c(Name = 'test program SCTest', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c =  true);
        insert programObj;   
        
        
        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class SCTest', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;
        
        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;   
        
        Test.setCurrentPageReference(new PageReference('Page.SubstituteCoachSearch'));
        System.currentPageReference().getParameters().put('Id', coachingSessObj.id);

        List<Coach__c> coachList = new List<Coach__c>();
        coachList.add(coachObj);

        ApexPages.StandardController controllerObj = new ApexPages.StandardController(coachingSessObj);
        SubstituteCoachSearchController csObject = new SubstituteCoachSearchController(controllerObj);
        
        csObject.selectedCoachId = coachObj.id;
        csObject.coachingSessDetail = coachingSessObj;
        csObject.substituteCoachList = coachList;
        csObject.selectedCoach();
        csObject.returnToPreviousPage();
        csObject.assignSubstitute();
        
        System.assertEquals(1, coachList.size());
        
        Test.stopTest();
    } 
    
    static testMethod void secondTestMethod(){
    
        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userObj = new User(Alias = 'sSCT2', Email='standuser321_SCTestTwo@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing321SCTestTwo', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj; 
        
        User userObj2 = new User(Alias = 'sSC2', Email='staner321_SCTestTwo@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing32CTestTwo', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='staser@org.com');
        insert userObj2;    
        
        
        Account testAccountObj = new Account(name = 'test Account SCTestTwo');
        insert testAccountObj;
        
        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact SCTestTwo', Account = testAccountObj);
        insert testContatObj;           
        
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test_SCTestTwo@testing.com', hmr_Class_Strength__c = 20,hmr_Active__c = TRUE);
        insert coachObj;
        
        Coach__c coachObj2 = new Coach__c(hmr_Coach__c = userObj2.id, hmr_Email__c = 'test_SestTwo@testing.com', hmr_Class_Strength__c = 20,hmr_Active__c = TRUE);
        insert coachObj2;
        
        Program__c programObj = new Program__c(Name = 'test program SCTestTwo', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c =  true);
        insert programObj;   
        
        
        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class SCTestTwo', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;
        
        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:15 PM',
                                                                       hmr_End_Time__c = '1:15 PM', hmr_Coach__c = coachObj.id); 
        insert coachingSessObj;
        
        Coaching_Session__c coachingSessObj2 = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:15 PM',
                                                                       hmr_End_Time__c = '1:15 PM', hmr_Coach__c = coachObj2.id); 
        insert coachingSessObj2;
        
        
    //    Test.setCurrentPageReference(new PageReference('Page.SubstituteCoachSearch'));
    //    System.currentPageReference().getParameters().put('Id', programObj.id);
    
        Test.setCurrentPageReference(new PageReference('Page.SubstituteCoachSearch'));
        System.currentPageReference().getParameters().put('Id', coachingSessObj2.id);

        List<Coach__c> coachList = new List<Coach__c>();
        coachList.add(coachObj);

        ApexPages.StandardController controllerObj = new ApexPages.StandardController(coachingSessObj);
        SubstituteCoachSearchController csObject = new SubstituteCoachSearchController(controllerObj);
        
        
        delete coachingSessObj;
        
        Coaching_Session__c coachingSessObj3 = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:15 PM',
                                                                       hmr_End_Time__c = '1:15 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj3;
    
        SubstituteCoachSearchController csObject2 = new SubstituteCoachSearchController(controllerObj);
        System.assertEquals(1, coachList.size());
        
        Test.stopTest();
    }
      
}