/**
* Apex Selector Class for User
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2017-11-02
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-11-02
* @JIRA:
*/
public with sharing class HMR_User_Selector {
	public HMR_User_Selector() {}

	public User getById(string userId){
		List<User> userRecordList = [SELECT Contact.AccountId, 
								  			ContactId,
								  			Contact.Account.Name,
								  			Contact.Community_User_ID__c,
								  			Username
						   			 FROM User 
						   			 WHERE Id = :userId];

        return userRecordList.size() > 0 ? userRecordList[0] : null;
	}
}