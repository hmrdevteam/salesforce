/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/08/2018
 * Created By: 
 * Last Modified Date: 03/08/2018
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobTrackerNote REST Service
 * ****************************************************/
@isTest
private class HMRMobTrackerNoteTest {

    @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;
    }

    @isTest
    private static void testGet(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      List<Tracker_Note__c> trackerNoteList = HMR_TestFactory.createTrackerNoteList(userRecord.ContactId, 10);

      insert trackerNoteList;

	    RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.params.put('communityUserId', userRecord.Id);
      request.params.put('offset', '0');
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	HMRMobTrackerNote.doGet();
  	  }

      Test.stopTest();

      //Verify the diet activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetTrackerNotesWithNoExistingRecords(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

	    RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.params.put('communityUserId', userRecord.Id);
      request.params.put('offset', '0');
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	HMRMobTrackerNote.doGet();
  	  }

      Test.stopTest();

      //Verify the diet activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetTrackerNotesWithNoParameters(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      List<Tracker_Note__c> trackerNoteList = HMR_TestFactory.createTrackerNoteList(userRecord.ContactId, 10);

      insert trackerNoteList;

	    RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	HMRMobTrackerNote.doGet();
  	  }

      Test.stopTest();

      //Verify the diet activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testPost(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);
      HMRMobTrackerNote.MobTrackerNoteDTO trackerNoteDTO = new HMRMobTrackerNote.MobTrackerNoteDTO(trackerNote);

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobTrackerNote.doPost(trackerNoteDTO);
      }

      Test.stopTest();

      //Verify the tracker note record was created
      List<Tracker_Note__c> trackerNoteResultList = [SELECT Id FROM Tracker_Note__c];
      System.assertEquals(1, trackerNoteResultList.size());
    }

    @isTest
    private static void testPostInvalid(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);
      HMRMobTrackerNote.MobTrackerNoteDTO trackerNoteDTO = new HMRMobTrackerNote.MobTrackerNoteDTO(trackerNote);

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	trackerNoteDTO.contactId = '0000000000000000000';
        HMRMobTrackerNote.doPost(trackerNoteDTO);
      }

      Test.stopTest();

      //Verify the tracker note record was not created
      List<Tracker_Note__c> trackerNoteResultList = [SELECT Id FROM Tracker_Note__c];
      System.assertEquals(0, trackerNoteResultList.size());
    }

    @isTest
    private static void testPatch(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);

      insert trackerNote;

      HMRMobTrackerNote.MobTrackerNoteDTO trackerNoteDTO = new HMRMobTrackerNote.MobTrackerNoteDTO(trackerNote);

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	trackerNoteDTO.title = 'Updated Title';
        HMRMobTrackerNote.doPatch(trackerNoteDTO);
      }

      Test.stopTest();

      //Verify the tracker note record was updated
      List<Tracker_Note__c> trackerNoteResultList = [SELECT Name FROM Tracker_Note__c];
      System.assertEquals('Updated Title', trackerNoteResultList[0].Name);
    }

    @isTest
    private static void testPatchInvalid(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);

      insert trackerNote;

      HMRMobTrackerNote.MobTrackerNoteDTO trackerNoteDTO = new HMRMobTrackerNote.MobTrackerNoteDTO(trackerNote);

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	trackerNoteDTO.contactId = '000000000000000000';
      	trackerNoteDTO.title = 'Updated Title';
        HMRMobTrackerNote.doPatch(trackerNoteDTO);
      }

      Test.stopTest();

      //Verify the tracker note record was not updated
      List<Tracker_Note__c> trackerNoteResultList = [SELECT Name FROM Tracker_Note__c];
      System.assertNotEquals('Updated Title', trackerNoteResultList[0].Name);
    }

    @isTest
    private static void testDelete(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);

      trackerNote.OwnerId = userRecord.Id;

      insert trackerNote;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
      request.params.put('id', trackerNote.Id);
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobTrackerNote.doDelete();
      }

      Test.stopTest();

      //Verify the tracker note record was deleted
      List<Tracker_Note__c> trackerNoteResultList = [SELECT Id FROM Tracker_Note__c];
      System.assertEquals(0, trackerNoteResultList.size());
    }

    @isTest
    private static void testDeleteInvalid(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
  		Tracker_Note__c trackerNote = HMR_TestFactory.createTrackerNote(userRecord.ContactId);

  		insert trackerNote;

  		HMRMobTrackerNote.MobTrackerNoteDTO trackerNoteDTO = new HMRMobTrackerNote.MobTrackerNoteDTO(trackerNote);

  		RestRequest request = new RestRequest();
  		RestResponse response = new RestResponse();

  		request.requestURI = '/services/apexrest/mobile/user/trackerNote/';
  		request.params.put('id', '00000000000000000000');
  		request.httpMethod = 'DELETE';

  		RestContext.request = request;
  		RestContext.response = response;

  		Test.startTest();

  		System.runAs(userRecord){
  			HMRMobTrackerNote.doDelete();
  		}

  		Test.stopTest();

  		//Verify the error return status was set
        	System.assertEquals(404, response.statusCode);
      }
}