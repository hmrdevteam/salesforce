/**
* Service Interface class for header related items in the community
* @Date: 2018-01-31
* @Author Zach Engman (Magnet 360)
* @Modified: 2018-01-31 Zach Engman
* @JIRA:
*/
global with sharing class HMR_CMS_Header_ServiceInterface implements cms.ServiceInterface {
	global String executeRequest(Map<String, String> params) {
		Map<String, Object> returnMap = new Map<String, Object>();
        HMR_Submenu_Service submenuService = new HMR_Submenu_Service();

    	try {
            String action = params.get('action');
            returnMap.put('success', true);
            //Return the calling function/action
            returnMap.put('action', action);

            if(action == 'getSubmenu') {
                returnMap.put('submenuItems', submenuService.getCommunitySubmenu(UserInfo.getUserId()));
            }
        }
        catch(Exception e) {
            // Unexpected Error
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMap.put('success',false);
            returnMap.put('message',JSON.serialize(message));
        }

		return JSON.serialize(returnMap);
	}

	global static Type getType() {
        return HMR_CMS_Header_ServiceInterface.class;
    }
}