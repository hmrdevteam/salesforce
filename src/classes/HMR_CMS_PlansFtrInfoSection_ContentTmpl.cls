/**
* Apex Content Template Controller for the last Sections on Plans Healthy Solutions and Healthy Shakes page
*
* @Date: 05/24/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified: Jay Zincone (Health Management Resources) - 2017-12-11 : https://hmrjira.atlassian.net/browse/HMP-55
* @JIRA:
*/

global virtual class HMR_CMS_PlansFtrInfoSection_ContentTmpl extends cms.ContentTemplateController{
	global HMR_CMS_PlansFtrInfoSection_ContentTmpl(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_PlansFtrInfoSection_ContentTmpl() {}

	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
 	*/
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
 	*/
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	public String ProgramType{
        get{
            return getPropertyWithDefault('ProgramType', '[]');
        }
    }

	public Decimal disctPriceHSAH { get; set; }
    public Decimal stdPriceHSAH { get; set; }
	public Decimal disctPriceHSS { get; set; }
    public Decimal stdPriceHSS { get; set; }
	//list of programs
    public List<Program__c> pList { get; set; }

    //if current user is the guest user
    public Boolean isGuestUser { get; set; }

    //if healthy shake is an option
    public Boolean isShakeOption { get; set; }

	public String returnPlansFooterInfoSectionHTML(Program__c healthySolutionsProgram, Program__c healthyShakesProgram, Decimal disctPriceHSAH, Decimal stdPriceHSAH, Decimal disctPriceHSS, Decimal stdPriceHSS, String ProgramType) {
		String html = '';
		html += '<section class="hmr-page-section bg-healthysolutions-info">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<div class="col-sm-1"></div>' +
			                '<div class="col-sm-10">' +
			                    '<div class="row">';
		if(ProgramType == 'HSAH') {
			html += '<h2>Healthy Solutions 3-week Quick Start&reg; Kit</h2>';
		}
		if(ProgramType == 'HSS') {
			html += '<h2>Healthy Shakes 2-week Quick Start&reg; Kit</h2>';
		}
		html +=	'</div>' +
		        '<div class="row">' +
		            '<div class="link-container">' +
		                '<div class="hmr-allcaps-container">' +
		                    '<a href="autodeliveryterms" target="_blank" class="hmr-allcaps blue">With Auto Delivery</a>' +
		                '</div>' +
		            '</div>' +
		        '</div>' +
		        '<div class="row">' +
		            '<hr></hr>' +
		        '</div>' +
		        '<div class="row">' +
		            '<ul>';
		if(ProgramType == 'HSAH') {
			html += '<li>FREE weekly group phone coaching included with auto delivery</li>' +
					'<li>FREE HMR BeneFit® Bar Variety Pack (12 assorted full-sized bars)</li>' +
					'<li>FREE shipping</li>' +
                    '<li><span>' + healthySolutionsProgram.hmr_Number_of_Shakes_Servings__c + '</span> servings HMR 120 Shakes  (choose chocolate, vanilla or a mix of each)</li>' +
                    '<li><span>' + healthySolutionsProgram.hmr_Number_of_Entrees__c + '</span> HMR Entrees (customize your assortment)</li>' +
                    '<li><span>' + healthySolutionsProgram.hmr_Number_of_Cereal_Servings__c + '</span> servings HMR Multigrain Hot Cereal</li>' +
                    '<li>Support Guides and HMR Recipe Book</li>' +
                    '<li>HMR Tracking App and Online Tools</li>' +
                    '<li>10% discount on future auto delivery re-orders, with free shipping</li>';
		}
		if(ProgramType == 'HSS') {
			html += '<li><span>' + healthyShakesProgram.hmr_Number_of_Shakes_Servings__c + '</span> servings HMR 120/HMR 70 Plus Shakes (you can customize chocolate, vanilla, or a mix of each) </li>' +
					'<li>Support Guides, HMR Recipe Book, and weekly tracking charts</li>' +
					'<li>HMR App access</li>' +
					'<li>10% discount on future auto delivery re-orders, with free shipping</li>';
		}
		html += '</ul>' +
	                '</div>' +
	                '<div class="row">' +
	                    '<div class="col-sm-6 col-xs-6 no-left-padding">' +
	                        '<div class="italic orange">';
		if(ProgramType == 'HSAH' && disctPriceHSAH > 0) {
			html += 'save $' + disctPriceHSAH;
		}
		if(ProgramType == 'HSS' && disctPriceHSS > 0) {
			html += 'save $' + disctPriceHSS;
		}
	    html += '</div>' +
	                '</div>' +
	                '<div class="col-sm-6 col-xs-6">' +
	                    '<div class="sub-header">' +
	                        '$<span>';
		if(ProgramType == 'HSAH') {
			html += stdPriceHSAH;
		}
		if(ProgramType == 'HSS') {
			html += stdPriceHSS;
		}
		html += '</span>' +
	                    '</div>' +
	                '</div>' +
	            '</div>' +
	                '<div class="row">' +
	                    '<hr></hr>' +
	                '</div>' +
	                '<div class="row">' +
	                    '<div class="col-sm-8"></div>' +
	                    '<div class="col-sm-4">' +
	                        '<div class="btn-container">';
		List<HMR_Url_Mapping__mdt> HMRURLMappingsList = new List<HMR_Url_Mapping__mdt>();
		if(ProgramType == 'HSAH') {
			HMRURLMappingsList =
				[SELECT DeveloperName, New_URL__c FROM HMR_Url_Mapping__mdt WHERE DeveloperName = 'HSAH_Kit_Config'];
			html += '<a href="' + HMRURLMappingsList[0].New_URL__c + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">Join Today</a>';
		}
		if(ProgramType == 'HSS') {
			HMRURLMappingsList =
				[SELECT DeveloperName, New_URL__c FROM HMR_Url_Mapping__mdt WHERE DeveloperName = 'HSS_Kit_Config'];
			html += '<a href="' + HMRURLMappingsList[0].New_URL__c + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">Join Today</a>';
		}
	    html += '</div></div></div>' +
				'<div class="row hmr-small-info">' +
					'<span>Please review our </span>' +
					'<a href="health-terms" target="_blank" class="hmr-allcaps blue">Health & Privacy Acknowledgement</a>' +
				'</div>' +
				'</div><div class="col-sm-1"></div></div></div></section>' +
				'<div class="plans-mobile-footer-div hidden">' +
			    '<section class="plans-mobile-footer-section visible-sm visible-xs">';
		String gtm_name = '';String gtm_id = '';
		if(ProgramType == 'HSAH') {
			gtm_name = 'Healthy Shakes Page View';
			gtm_id = 'HealthyShakes';
			html += '<a href="healthy-shakes-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">';
		}
		if(ProgramType == 'HSS') {
			gtm_name = 'Healthy Solutions Page View';
			gtm_id = 'HealthySolutions';
			html += '<a href="healthy-solutions-at-home-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">';
		}
		html +=	'<div class="container">' +
	            '<div class="row">' +
	                '<div class="col-xs-10">' +
	                    '<div class="row">' +
	                        '<div class="col-xs-12">';
		if(ProgramType == 'HSAH') {
			html += '<h3 class="hmr-allcaps white no-book">HEALTHY Shakes&reg;</h3>';
		}
		if(ProgramType == 'HSS') {
			html += '<h3 class="hmr-allcaps white no-book">HEALTHY Solutions at Home&reg;</h3>';
		}
		html +=	'</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-12">';
		if(ProgramType == 'HSAH') {
			html += '<h3 class="hmr-allcaps white book">2-Week Quick Start Kit</h3>';
		}
		if(ProgramType == 'HSS') {
			html += '<h3 class="hmr-allcaps white book">3-Week Quick Start Kit</h3>';
		}
        html += '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-12">';
		if(ProgramType == 'HSAH') {
			html += '<h3 class="hmr-allcaps white book">' +
						'$<span>' + stdPriceHSS + '</span> ($<span>' + healthyShakesProgram.Price_per_Day__c + '</span> per day)' +
					'</h3>';
		}
		if(ProgramType == 'HSS') {
			html += '<h3 class="hmr-allcaps white book">' +
						'$<span>' + stdPriceHSAH + '</span> ($<span>' + healthySolutionsProgram.Price_per_Day__c + '</span> per day)' +
					'</h3>';
		}
        html += '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<img src="https://www.myhmrprogram.com/ContentMedia/HealthySolutionsMobile/ArrowLarge.png" class="plans-arrow"/>' +
                '</div>' +
            '</div>' +
        	'</div>' +
			'</a>' +
		    '</section>' +
			'</div>';
		return html;
	}

	global virtual override String getHTML(){
		String html = '';isShakeOption = true;
		disctPriceHSAH = 0; disctPriceHSS = 0;stdPriceHSAH = 0; stdPriceHSS = 0;
		try {
			//current user Id
	        String userId = UserInfo.getUserId();
	        //current user profile Id
	        String userProfileId = UserInfo.getProfileId();

	        //check if current user is the site guest user
	        Profile guestProfile = [SELECT Id, Name FROM Profile WHERE Id =: userProfileId];
	        isGuestUser = (guestProfile.Name == 'HMR Program Community Profile');
			//retrieve all programs and preset the value for guest user
	        pList = new List<Program__c>();

			pList = [SELECT Id, Name, Standard_Kit__r.ccrz__SKU__c, hmr_Number_of_Entrees__c, hmr_Standard_Kit_Price__c, Price_per_Day__c,
						Default_Promotional_Discount__c, hmr_Number_of_Shakes_Servings__c, hmr_Number_of_Cereal_Servings__c FROM Program__c];

			Program__c healthySolutionsProgram;
			Program__c healthyShakesProgram;
			for(Program__c program: pList) {
				if(program.Name.contains('Solutions')) {
					healthySolutionsProgram = program;
					if(program.Default_Promotional_Discount__c != null && program.Default_Promotional_Discount__c != 0) {
						disctPriceHSAH = program.Default_Promotional_Discount__c;
						stdPriceHSAH = program.hmr_Standard_Kit_Price__c - program.Default_Promotional_Discount__c;
					}
					else {
						stdPriceHSAH = program.hmr_Standard_Kit_Price__c;
					}
				}
				if(program.Name.contains('Shakes')) {
					healthyShakesProgram = program;
					if(program.Default_Promotional_Discount__c != null && program.Default_Promotional_Discount__c != 0) {
						disctPriceHSS = program.Default_Promotional_Discount__c;
						stdPriceHSS = program.hmr_Standard_Kit_Price__c - program.Default_Promotional_Discount__c;
					}
					else {
						stdPriceHSS = program.hmr_Standard_Kit_Price__c;
					}
				}
			}
			//if current user is community user but not site user
	        if(!isGuestUser){
				User curUser = [Select Id, ContactId from User where Id =: UserInfo.getUserId()];
	            if(curUser.ContactId != null){
	                Id cId = curUser.ContactId;
	                Contact con = [SELECT Account.RecordType.DeveloperName,
							              Account.hmr_Is_Healthy_Shakes_an_Option__c,
							              Account.Standard_HSAH_Kit_Discount__c,
							              Account.Standard_HSS_Kit_Discount__c,
							              Account.hmr_HSAH_Standard_Kit_Price__c,
							              Account.hmr_HSS_Standard_Kit_Price__c FROM Contact WHERE Id = :cId];//Corporate_Account

	                //if cooperate account then use the value of hmr_Is_Healthy_Shakes_an_Option__c
	                if(con.Account.RecordType.DeveloperName == 'Corporate_Account'){
	                    isShakeOption = con.Account.hmr_Is_Healthy_Shakes_an_Option__c;
	                }
	                //get the price from
	                if(ProgramType == 'HSAH'){
	                    if(con.Account.hmr_HSAH_Standard_Kit_Price__c != null) {
							if(con.Account.Standard_HSAH_Kit_Discount__c != null && con.Account.Standard_HSAH_Kit_Discount__c != 0) {
								disctPriceHSAH = con.Account.Standard_HSAH_Kit_Discount__c;
								stdPriceHSAH = con.Account.hmr_HSAH_Standard_Kit_Price__c - con.Account.Standard_HSAH_Kit_Discount__c;
							}
							else {
								stdPriceHSAH = con.Account.hmr_HSAH_Standard_Kit_Price__c;
							}
						}
	                }
	                if(ProgramType == 'HSS'){
	                    if(con.Account.hmr_HSS_Standard_Kit_Price__c != null) {
							if(con.Account.Standard_HSS_Kit_Discount__c != null && con.Account.Standard_HSS_Kit_Discount__c != 0) {
								disctPriceHSS = con.Account.Standard_HSS_Kit_Discount__c;
								stdPriceHSS = con.Account.hmr_HSS_Standard_Kit_Price__c - con.Account.Standard_HSS_Kit_Discount__c;
							}
							else {
								stdPriceHSS = con.Account.hmr_HSS_Standard_Kit_Price__c;
							}
						}
	                }
	            }
			}
			html = returnPlansFooterInfoSectionHTML(healthySolutionsProgram, healthyShakesProgram, disctPriceHSAH, stdPriceHSAH, disctPriceHSS, stdPriceHSS, ProgramType);
		}
		catch(Exception ex) {
			System.debug('Exception in Plans Footer Info Section Content Template');
			System.debug(ex);
			html += ex.getMessage();
		}
		return html;
	}
}