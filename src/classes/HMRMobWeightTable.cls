/**
* Apex Rest Service Class for the Weight Table
* @Date: 2018-02-26
* @Author: Magnet 360 - Zach Engman
* @Modified: 2018-02-26
* @JIRA:
*/
@RestResource(urlMapping='/mobile/weightTable/*')
global class HMRMobWeightTable {
	/**
     * Get method can retrieve all the records in Weight_Table_Setting__mdt or filter by weight passed
     * @return array of Weight_Table_Setting__mdt DTO records
     */
    @HttpGet
    global static void doGet() {
    	Map<String, Object> resultMap = new Map<String, Object>();
        String allRecords = RestContext.request.params.get('allRecords');
        String weightParameter = RestContext.request.params.get('weight');
        RestContext.response.addHeader('Content-Type', 'application/json');
		
		List<WeightTableRecordDTO> weightTableRecordList = new List<WeightTableRecordDTO>();

        if (allRecords != null && allRecords == 'true') {
        	for(Weight_Table_Setting__mdt record : HMR_WeightTable_Service.getAll()){
        		weightTableRecordList.add(new WeightTableRecordDTO(record));
        	}  

        	RestContext.response.statuscode = 200;
        	resultMap.put('success', true);
        	resultMap.put('weightTable', weightTableRecordList);
        }
        else {
        	try{
            	Decimal weight = Decimal.valueOf(weightParameter);
            	HMR_WeightTable_Service weightTableService = new HMR_WeightTable_Service(weight);

            	weightTableRecordList.add(new WeightTableRecordDTO(weightTableService.getWeightSetting()));

            	RestContext.response.statuscode = 200;
            	resultMap.put('success', true);
            	resultMap.put('weightTable', weightTableRecordList);
        	}
        	catch(Exception ex){
        		RestContext.response.statuscode = 404;
        		resultMap.put('error', ex.getmessage());
        	}  
        }    

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(resultMap));    
    }

    global static WeightTableRecordDTO getWeightTableRecord(decimal weight){
    	HMR_WeightTable_Service weightTableService = new HMR_WeightTable_Service(weight);

    	return new WeightTableRecordDTO(weightTableService.getWeightSetting());
    }

    global class WeightTableRecordDTO {
    	public decimal weightMinimum {get; set;}
    	public decimal weightMaximum {get; set;}
    	public integer lowIntensity {get; set;}
    	public integer mediumIntensity {get; set;}
    	public integer highIntensity {get; set;}
    	public integer veryHighIntensity {get; set;}

    	public WeightTableRecordDTO(Weight_Table_Setting__mdt weightTableSetting){
    		this.weightMinimum = weightTableSetting.Weight_Minimum__c;
    		this.weightMaximum = weightTableSetting.Weight_Maximum__c;
    		this.lowIntensity = Integer.valueOf(weightTableSetting.Low_Intensity__c);
    		this.mediumIntensity = Integer.valueOf(weightTableSetting.Medium_Intensity__c);
    		this.highIntensity = Integer.valueOf(weightTableSetting.High_Intensity__c);
    		this.veryHighIntensity = Integer.valueOf(weightTableSetting.Very_High_Intensity__c);
    	}

    }
}