/**
* Test Class Coverage of the HMR_DataCategory_Service
*
* @Date: 08/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/18/2017
* @JIRA: 
*/
@isTest
private class HMR_DataCategory_Service_Test {
	@isTest
	private static void testGetDataCategoryList(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<Category_Map__mdt> dataCategoryList = dataCategoryService.getDataCategoryList();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryList.size() >= 0);
	}

	@isTest
	private static void testGetTopicList(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<Category_Map__mdt> dataCategoryList = dataCategoryService.getTopicList();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryList.size() >= 0);
	}

	@isTest
	private static void testGetRecipeDataCategoryList(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<Category_Map__mdt> dataCategoryList = dataCategoryService.getRecipeDataCategoryList();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryList.size() >= 0);
	}

	@isTest
	private static void testGetTopicIconMap(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		Map<String, String> topicIconMap = dataCategoryService.getTopicIconMap();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(topicIconMap.size() >= 0);
	}

	@isTest
	private static void testGetPrimaryDataCategoryLabelList(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<String> dataCategoryLabelList = dataCategoryService.getPrimaryDataCategoryLabelList();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryLabelList.size() >= 0);
	}

	@isTest
	private static void testGetPrimaryDataCategoryList(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<DataCategory> dataCategoryList = dataCategoryService.getPrimaryDataCategoryList();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryList.size() >= 0);
	}

	@isTest
	private static void testGetDataCategoryGroupStructures(){
		HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

		Test.startTest();

		List<HMR_DataCategory_Service.DataCategoryHierarchyRecord> dataCategoryHierarchyList = dataCategoryService.getDataCategoryGroupStructures();

		Test.stopTest();

		//Verify results were returned if they exist or at least empty list (upon initial deployment they won't exist)
		System.assert(dataCategoryHierarchyList.size() >= 0);
	}
}