/**
* Apex Content Template Controller for Hero Section on Professionals Page
*
* @Date: 07/26/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

global virtual class HMR_CMS_ProfessionalHero_ContentTemplate extends cms.ContentTemplateController implements cms.ServiceInterface{
	global HMR_CMS_ProfessionalHero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_ProfessionalHero_ContentTemplate(cms.CoreController controller){
        super();
    }
	global HMR_CMS_ProfessionalHero_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
 	*/
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
 	*/
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	public List<String> getLeadHMRSettingPickListValues(){
       	List<String> pickListValuesList = new List<String>();
		try {
			Schema.DescribeFieldResult fieldResult = Lead.hmr_Setting__c.getDescribe();
	        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        for( Schema.PicklistEntry pickListVal : ple){
	            pickListValuesList.add(pickListVal.getLabel());
	        }
		}
		catch(Exception ex) {
			System.debug('@@@ Exception in getLeadHMRSettingPickListValues' + ex.getMessage());
			pickListValuesList.clear();
		}
        return pickListValuesList;
    }

	public String returnStatePickListOptions() {
		String html = '';
		html += '<option value="" selected="selected">State</option>' +
				'<option value="AA">AA</option>' +
				'<option value="AE">AE</option>' +
				'<option value="AL">Alabama</option>' +
				'<option value="AK">Alaska</option>' +
				'<option value="AP">AP</option>' +
				'<option value="AZ">Arizona</option>' +
				'<option value="AR">Arkansas</option>' +
				'<option value="CA">California</option>' +
				'<option value="CO">Colorado</option>' +
				'<option value="CT">Connecticut</option>' +
				'<option value="DE">Delaware</option>' +
				'<option value="DC">District of Columbia</option>' +
				'<option value="FL">Florida</option>' +
				'<option value="GA">Georgia</option>' +
				'<option value="GU">Guam</option>' +
				'<option value="HI">Hawaii</option>' +
				'<option value="ID">Idaho</option>' +
				'<option value="IL">Illinois</option>' +
				'<option value="IN">Indiana</option>' +
				'<option value="IA">Iowa</option>' +
				'<option value="KS">Kansas</option>' +
				'<option value="KY">Kentucky</option>' +
				'<option value="LA">Louisiana</option>' +
				'<option value="ME">Maine</option>' +
				'<option value="MD">Maryland</option>' +
				'<option value="MA">Massachusetts</option>' +
				'<option value="MI">Michigan</option>' +
				'<option value="MN">Minnesota</option>' +
				'<option value="MS">Mississippi</option>' +
				'<option value="MO">Missouri</option>' +
				'<option value="MT">Montana</option>' +
				'<option value="NE">Nebraska</option>' +
				'<option value="NV">Nevada</option>' +
				'<option value="NH">New Hampshire</option>' +
				'<option value="NJ">New Jersey</option>' +
				'<option value="NM">New Mexico</option>' +
				'<option value="NY">New York</option>' +
				'<option value="NC">North Carolina</option>' +
				'<option value="ND">North Dakota</option>' +
				'<option value="OH">Ohio</option>' +
				'<option value="OK">Oklahoma</option>' +
				'<option value="OR">Oregon</option>' +
				'<option value="PA">Pennsylvania</option>' +
				'<option value="PR">Puerto Rico</option>' +
				'<option value="RI">Rhode Island</option>' +
				'<option value="SC">South Carolina</option>' +
				'<option value="SD">South Dakota</option>' +
				'<option value="TN">Tennessee</option>' +
				'<option value="TX">Texas</option>' +
				'<option value="VI">US Virgin Islands</option>' +
				'<option value="UT">Utah</option>' +
				'<option value="VT">Vermont</option>' +
				'<option value="VA">Virginia</option>' +
				'<option value="WA">Washington</option>' +
				'<option value="WV">West Virginia</option>' +
				'<option value="WI">Wisconsin</option>' +
				'<option value="WY">Wyoming</option>';
		return html;
	}

	//return HTML for the Professionals Lead Submission Modal Dialog
	public String returnProfessionalsLeadCreationModal() {
		String html = '';
		html += '<div class="hmr-modal modal fade" id="professionalsModalDialog" tabindex="-1" role="dialog" aria-labelledby="professionalsModalDialog" aria-hidden="true">' +
					'<div class="hmr-clinic-modal-dialog modal-dialog" role="document">' +
						'<div class="modal-content hmr-modal-content">' +
							'<div class="modal-header hmr-modal-header">' +
								'<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
									'<span aria-hidden="true">x</span>' +
								'</button>' +
							'</div>' +
							'<div class="modal-body hmr-modal-body">' +
								'<div id="professionalModalHeader" class="row text-center hidden">' +
									'<h2>Thank you for your inquiry. We look forward to sharing more information with you.</h2>' +
								'</div>' +
								'<div id="professionalModalHeaderFailure" class="row text-center hidden">' +
									'<h2>Something weng wrong! Please refresh this page</h2>' +
								'</div>' +
								'<div class="row hmr-info clinic-info-to-hide">' +
									'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
										'Complete and submit form to receive more information about HMR.' +
									'</div>' +
								'</div>' +
								'<div class="row form-cliniclanding clinic-info-to-hide">' +
									'<form id="professionalsLeadForm">' +
										'<input type="text" id="professionalsFN" class="form-control hmr-formcontrol" name="professionalsFN" autofocus="" placeholder="First Name">' +
										'<input type="text" id="professionalsLN" class="form-control hmr-formcontrol" name="professionalsLN" placeholder="Last Name">' +
										'<input type="text" id="professionalsJT" class="form-control hmr-formcontrol" name="professionalsJT" placeholder="Job Title">' +
										'<input type="email" id="professionalsEmail" class="form-control hmr-formcontrol" name="professionalsEmail"  placeholder="Email: myemail@example.com">' +
										'<input type="text" id="professionalsPhone" class="form-control hmr-formcontrol" name="professionalsPhone" placeholder="888-888-8888">' +
										'<input type="text" id="professionalsCompany" class="form-control hmr-formcontrol" name="professionalsCompany" placeholder="Company">' +
										'<input type="text" id="professionalsAddress1" class="form-control hmr-formcontrol" name="professionalsAddress1" placeholder="Address 1">' +
										'<input type="text" id="professionalsAddress2" class="form-control hmr-formcontrol" name="professionalsAddress2" placeholder="Address 2">' +
										'<input type="text" id="professionalsCity" class="form-control hmr-formcontrol" name="professionalsCity" placeholder="City">' +
										'<select class="form-control hmr-formcontrol" required="" id="professionalsState" name="professionalsState">' +
											returnStatePickListOptions() +
										'</select>' +
										'<input type="text" id="professionalsZipCode" class="form-control hmr-formcontrol" name="professionalsZipCode" placeholder="Zip Code">' +
										'<select class="form-control hmr-formcontrol" required="" id="professionalsHMRSetting" name="professionalsHMRSetting">';
										html += String.format('<option value="">{0}</option>', new String[] {'Select a setting'});
										for(String leadHMRSetting : getLeadHMRSettingPickListValues())
											 html += String.format('<option value="{0}">{0}</option>', new String[] {leadHMRSetting});
										html +=	'</select>' +
									'</form>' +
								'</div>' +
								'<div class="row text-center clinic-info-to-hide">' +
									'<div class="btn-container">' +
										'<a id="professionalsLeadRequestInformationBtn" class="btn btn-primary hmr-btn-blue hmr-btn-small">Request Information</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';
		return html;
	}

	//method to render hero HTML markup, this is the override of CMS getHTML method for Apex Content Template
	global virtual override String getHTML(){
		String html = '';
		html += returnProfessionalsLeadCreationModal();
		html += '<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>';
		html += '<section class="hmr-professionals-section">' +
			        '<div class="col-sm-6 bg-professionals bg-typecover mobile-only"></div>' +
			        '<div class="col-sm-6 professionals-content">' +
			            '<div class="col-sm-3"></div>' +
			            '<div class="col-sm-7 section-right-black">' +
			                '<h2>Clinically Significant Weight Loss</h2>' +
			                '<p>' +
			                    'HMR offers proven solutions for healthcare payers and providers looking to address the problem of obesity. ' +
			                    'Obesity and its comorbidities are estimated to be responsible for 75% of healthcare spending.' +
			                    '<sup>1</sup> HMR can help your population lose weight, and learn how to incorporate the lifestyle changes needed to reduce risk factors and maintain weight loss long term.' +
			                '</p>' +
			                '<p class="text-11b">' +
			                    '<sup>1</sup>Finkelstein EA et al. Am J Prev Med. 2012;42(6):563-570' +
			                '</p>' +
			                '<div class="btn-container">' +
			                    '<a class="btn btn-primary hmr-btn-blue hmr-btn-small" data-toggle="modal" data-target="#professionalsModalDialog" onclick="javascript:profpg.v();">Request Information</a>' +
			                '</div>' +
			            '</div>' +
			            '<div class="col-sm-2"></div>' +
			        '</div>' +
			        '<div class="col-sm-6 bg-professionals bg-typecover desktop-only"></div>' +
			    '</section>';
		return html;
	}

	//CMS service interface method to execute JS request for creating Professionals Lead
	global static String executeRequest(Map<String, String> params) {
		Map<String, Object> responseString = new Map<String, Object>();
		try {
			Lead professionalsLeadToInsert = new Lead(
				FirstName = params.get('FirstName'),
				LastName = params.get('LastName'),
				Title = params.get('Title'),
				Email = params.get('Email'),
				Phone = params.get('Phone'),
				Company = params.get('Company'),
				Street = params.get('Street'),
				City = params.get('City'),
				State = params.get('State'),
				PostalCode = params.get('PostalCode'),
				hmr_Setting__c = params.get('hmr_Setting__c'),
				hmr_Lead_Type__c = 'Professional',
				LeadSource = 'Website',
				Status = 'New',
				hmr_Lead_Source_Detail__c = 'Obesity Solutions'
			);
			insert professionalsLeadToInsert;
			responseString.put('success', true);
			responseString.put('professionalsLeadToInsert', (Object)professionalsLeadToInsert);
		}
		catch(Exception ex) {
			System.debug('@@@ Exception in HMR_CMS_ProfessionalHero_ContentTemplate executeRequest' + ex.getMessage());
			responseString.put('professionalsLeadToInsert', (String)ex.getMessage());
		}
		return JSON.serialize(responseString);
	}

	/**
	 * getType --> global method to override cms.ServiceInterface
	 * @param
	 * @return a JSON-serialized response string
	 */
	public static Type getType() {
		return HMR_CMS_ProfessionalHero_ContentTemplate.class;
	}
}