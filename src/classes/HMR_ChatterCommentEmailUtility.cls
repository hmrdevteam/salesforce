/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 10 August 2017
 * Created By: Nathan Anderson
 * Last Modified Date:
 * Last Modified By:
 * Description:
 Called by Process Builder/Trigger, this class sends emails on various types of Chatter Posts by Community Users.
 TODO ---- Finalize copy for all Email
 * ****************************************************/
public class HMR_ChatterCommentEmailUtility {

	//This method returns the list of users who are set up as Community Moderator users, to be re-used as required for each email.
	public static List<User> getModerators(){

		//Get list of users who are assigned to moderator permission set and add them to a List of User records to use in sendEmail method
		List<PermissionSetAssignment> psa = [SELECT Assignee.Id, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Community_Moderator'];
		Set<Id> moderatorIds = new Set<Id>();

		for (PermissionSetAssignment p : psa) {
			moderatorIds.add(p.Assignee.Id);

		}

		List<User> moderators = [SELECT Id, Email FROM User WHERE Id IN :moderatorIds];

		return moderators;

	}

	//This method accepts details about the email, then constructs and sends the appropriate number of emails
	public static void sendEmail(List<User> users, String subject, String body) {

		for(User u : users ) {
			//Create instance of a Message
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			//Use the setTargetObjectId class to avoid email limit issues
			mail.setTargetObjectId(u.Id);
			//Need to set this to false or email will fail when using setTargetObjectId
			mail.setSaveAsActivity(false);
			//Set sender display name & sender email address
			Id emailAddressId = [SELECT Id, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = 'HMR Program' LIMIT 1].Id;
			mail.setOrgWideEmailAddressId(emailAddressId);

			//Set the email subject from the passed in value
			mail.setSubject(subject);

			//Set the email body from the passed in value
			mail.setHTMLBody(body);

			//Send the email, catch exceptions so we don't stop the post from finishing
			try {
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			} catch (Exception e) {
				System.debug('Exception in sendEmail method: ' + e);
			}
		}

	}

	//Use InvocableMethod to make this visible in Process Builder
	@InvocableMethod(label = 'ArticleCommentAlert')

	//Method for setting up email notifications to Community Moderators for a Community User's Chatter Post on Knowledge Articles
	//Accepts a FeedItem Id from PB then calls the sendEmail method passing user, subject and body
	public static void ArticleCommentAlert(List<Id> feedPostId) {

		//Get the FeedItem record passed in from the Process
		FeedItem feedPost = [SELECT Id, ParentId, CreatedById, Body, Type, Title FROM FeedItem WHERE Id IN :feedPostId LIMIT 1];

		//Get the user record for the Client who submitted the FeedItem Post
		User customer = [SELECT Id, FirstName, LastName FROM User WHERE Id =: feedPost.CreatedById LIMIT 1];

		//get the list of moderators to send to
		List<User> moderators = getModerators();

		//Get 3 character prefix of Recipe article type
	    Schema.DescribeSObjectResult rec = Recipe__kav.sObjectType.getDescribe();
	    String recipePrefix = rec.getKeyPrefix();

		//Get 3 character prefix of Article article type
	    Schema.DescribeSObjectResult art = Blog_Post__kav.sObjectType.getDescribe();
	    String articlePrefix = art.getKeyPrefix();

		//Get 3 character prefix of Success Story article type
	    Schema.DescribeSObjectResult ss = Success_Story__kav.sObjectType.getDescribe();
	    String storyPrefix = ss.getKeyPrefix();

		//Get 3 character prefix of FAQ article type
	    Schema.DescribeSObjectResult faq = FAQ__kav.sObjectType.getDescribe();
	    String faqPrefix = faq.getKeyPrefix();

		String subject;

		String body = '';

		//Create instance of the Knowledge Service to help get category info for URLs
		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

		//if Comment was posted to a Recipe, set up the email and call sendEmail method
		if(String.valueOf(feedPost.ParentId).left(3) == recipePrefix){
			//Get the Recipe Article record the FeedItem Post was made on
			Recipe__kav recArticle = [SELECT KnowledgeArticleId, Title, UrlName FROM Recipe__kav WHERE KnowledgeArticleId =: feedPost.ParentId AND PublishStatus = 'Online' LIMIT 1];
			//Call Knowledge Service to get the primary category for the url
			HMR_Knowledge_Service.ArticleRecord article = knowledgeService.getByName(recArticle.UrlName);

			//Create the email subject
			subject = 'New Recipe Comment';
			//Create the email body
			body += 'Dear HMR Program Community Moderator,<br/><br/>';
			body += 'There is a new comment on the following Recipe: <a href="' + Site.getBaseURL() + '/recipes/' + article.CategoryUrlName + '/' + recArticle.UrlName + '">' + recArticle.Title + '</a>. <br/><br/>';
			body += 'Comment Submitted by: ' + customer.FirstName + ' ' + customer.LastName + '<br/>';
			body += '"' + feedPost.Body + '" <br/><br/>';
			body += 'Thank You! <br/>';
			body += 'ChatterBot';

			//send details to sendEmail method
			sendEmail(moderators, subject, body);

		//if Comment was posted to a general Article, set up the email and call sendEmail method
		} else if(String.valueOf(feedPost.ParentId).left(3) == articlePrefix) {
			//Get the Article record the FeedItem Post was made on
			Blog_Post__kav blogArticle = [SELECT KnowledgeArticleId, Title, UrlName FROM Blog_Post__kav WHERE KnowledgeArticleId =: feedPost.ParentId AND PublishStatus = 'Online' LIMIT 1];
			//Call Knowledge Service to get the primary category for the url
			HMR_Knowledge_Service.ArticleRecord article = knowledgeService.getByName(blogArticle.UrlName);

			//Create the email subject
			subject = 'New Article Comment';
			//Create the email body
			body += 'Dear HMR Program Community Moderator,<br/><br/>';
			body += 'There is a new comment on the following Article: <a href="' + Site.getBaseURL() + '/resources/' + article.CategoryUrlName + '/' + blogArticle.UrlName + '">' + blogArticle.Title + '</a>. <br/><br/>';
			body += 'Comment Submitted by: ' + customer.FirstName + ' ' + customer.LastName + '<br/>';
			body += feedPost.Body + '<br/><br/>';
			body += 'Thank You! <br/>';
			body += 'ChatterBot';

			//send details to sendEmail method
			sendEmail(moderators, subject, body);

		//if Comment was posted to a Success Story, set up the email and call sendEmail method
		} else if(String.valueOf(feedPost.ParentId).left(3) == storyPrefix) {
			//Get the SS Article record the FeedItem Post was made on
			Success_Story__kav storyArticle = [SELECT KnowledgeArticleId, Title, UrlName, Publish_to_Success_Story_Page__c FROM Success_Story__kav WHERE KnowledgeArticleId =: feedPost.ParentId AND PublishStatus = 'Online' LIMIT 1];

			//Create the email subject
			subject = 'New Success Story Comment';
			//Create the email body
			body += 'Dear HMR Program Community Moderator,<br/><br/>';
			if(storyArticle.Publish_to_Success_Story_Page__c) {
				body += 'There is a new comment on the following Success Story: <a href="' + Site.getBaseURL() + '/success-stories/' + storyArticle.UrlName + '">' + storyArticle.Title + '</a>. <br/><br/>';
			} else {
				body += 'There is a new comment on the following Success Story: <a href="' + Site.getBaseURL() + 'resources/success-stories/' + storyArticle.UrlName + '">' + storyArticle.Title + '</a>. <br/><br/>';
			}
			body += 'Comment Submitted by: ' + customer.FirstName + ' ' + customer.LastName + '<br/>';
			body += feedPost.Body + '<br/><br/>';
			body += 'Thank You! <br/>';
			body += 'ChatterBot';

			//send details to sendEmail method
			sendEmail(moderators, subject, body);

		//if Comment was posted to an FAQ, set up the email and call sendEmail method
		} else if(String.valueOf(feedPost.ParentId).left(3) == faqPrefix) {
			//Get the faq Article record the FeedItem Post was made on
			FAQ__kav faqArticle = [SELECT KnowledgeArticleId, Title, UrlName FROM FAQ__kav WHERE KnowledgeArticleId =: feedPost.ParentId AND PublishStatus = 'Online' LIMIT 1];
			//Call Knowledge Service to get the primary category for the url
			HMR_Knowledge_Service.ArticleRecord article = knowledgeService.getByName(faqArticle.UrlName);

			//Create the email subject
			subject = 'New Article Comment';
			//Create the email body
			body += 'Dear HMR Program Community Moderator,<br/><br/>';
			body += 'There is a new comment on the following Article: <a href="' + Site.getBaseURL() + '/resources/' + article.CategoryUrlName + '/' + faqArticle.UrlName + '">' + faqArticle.Title + '</a>. <br/><br/>';
			body += 'Comment Submitted by: ' + customer.FirstName + ' ' + customer.LastName + '<br/>';
			body += feedPost.Body + '<br/><br/>';
			body += 'Thank You! <br/>';
			body += 'ChatterBot';

			//send details to sendEmail method
			sendEmail(moderators, subject, body);

		//if FeedItem is a new Question Post, set up the email and call sendEmail method
		} else if (feedPost.Type == 'QuestionPost') {
			//Create the email subject
			subject = 'New Question Posted';
			//Create the email body
			body += 'Dear HMR Program Community Moderator,<br/><br/>';
			body += 'A new Question has been posted, <a href="' + Site.getBaseURL() + '/questions-detail?feedId=' + feedPost.Id + '">click here</a> to view the Question Details.<br/><br/>';
			body += 'Question Submitted by: ' + customer.FirstName + ' ' + customer.LastName + '<br/><br/>';
			body += '<b>"' + feedPost.Title + '"</b> <br/>';
			body += feedPost.Body + ' <br/><br/>';
			body += 'Thank You! <br/>';
			body += 'ChatterBot';

			//send details to sendEmail method
			sendEmail(moderators, subject, body);

		}
	}

	//Called by FeedComment Trigger, handles sending alerts to Original Poster for new replies to Questions
	public void questionReplyAlert(List<Id> feedCommentIds) {

		//Get the FeedComment record passed in from the trigger
		FeedComment feedComment = [SELECT Id, FeedItemId, CreatedById, CommentBody FROM FeedComment WHERE Id IN :feedCommentIds LIMIT 1];

		//Get the FeedItem record that the Comment is replying to
		FeedItem feedPost = [SELECT Id, ParentId, CreatedById, Body, Type, Title FROM FeedItem WHERE Id = :feedComment.FeedItemId LIMIT 1];

		//Get the user record for the Client who submitted the FeedItem Post
		User customer = [SELECT Id, FirstName, LastName FROM User WHERE Id =: feedPost.CreatedById LIMIT 1];

		//sendEmail method accepts a List, so we need to add the single User record to a list
		List<User> users = new List<User>();
		users.add(customer);

		//get the list of moderators to send to
		List<User> moderators = getModerators();

		//Add moderators to user list that already includes original poster
		for(User u : moderators) {
			users.add(u);
		}

		String subject;

		String body = '';

		if(feedPost.Type == 'QuestionPost') {

			//Create the email subject
			subject = 'New Answer Posted!';
			//Create the email body
			body += 'Dear ' + customer.FirstName + ',<br/><br/>';
			body += 'A new answer has been posted to your question, <a href="' + Site.getBaseURL() + '/questions-detail?feedId=' + feedPost.Id + '">click here</a> to view your post.<br/><br/>';
			body += 'Question: ' + feedPost.Title + '<br/><br/>';
			body += 'Answer: ' + feedComment.CommentBody + '<br/><br/>';
			body += 'Thank You! <br/>';
			body += 'HMR Program Community';

			//send details to sendEmail method
			sendEmail(users, subject, body);

		}
	}
}