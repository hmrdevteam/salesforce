/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_FeaturedArticles_ContentTemplate
*/

@isTest(seealldata=true) //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_FeaturedArticles_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_FeaturedArticles_ContentTemplate faTemplate = new HMR_CMS_FeaturedArticles_ContentTemplate();
    String propertyValue = faTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    faTemplate.testAttributes = tab;

    string s2 = faTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = faTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');


    String renderHTML = faTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    try{
        HMR_CMS_FeaturedArticles_ContentTemplate qc = new HMR_CMS_FeaturedArticles_ContentTemplate(null);
    }catch(Exception e){}
  }
  
}