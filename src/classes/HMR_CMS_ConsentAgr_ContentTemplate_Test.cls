/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_ConsentAgr_ContentTemplate_Test
*/

@isTest
private class HMR_CMS_ConsentAgr_ContentTemplate_Test{
  
  @isTest static void testMethodForHealth() {
        // Implement test code
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
        insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Auto-Delivery', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
    
    
      consentAgrContentTmpl.testAttributes = new Map<String, String> {
                        'renderType' => 'Modal',
                        'consentType' => 'Health',
                        'showOnScreen' => 'show'
                    };   
    
    
     String respString = consentAgrContentTmpl.getHTML();
     
     System.assert(!String.isBlank(respString));
     
   }
  
   
    @isTest static void testMethodForAutoDelivery() {   
    
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
      //  insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Auto-Delivery', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
        
        
        consentAgrContentTmpl.testAttributes = new Map<String, String> {
                            'renderType' => 'Content',
                            'consentType' => 'Auto-Delivery',
                            'showOnScreen' => 'show'
                        }; 
                        
        String respString = consentAgrContentTmpl.getHTML();
        
        System.assert(!String.isBlank(respString));
    
    }
    
    @isTest static void testMethodForHMRTRerms() {   
    
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
    //    insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'HMR Terms & Conditions', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
            
        consentAgrContentTmpl.testAttributes = new Map<String, String> {
                            'renderType' => 'Content',
                            'consentType' => 'HMR Terms & Conditions',
                            'showOnScreen' => 'show'
                        }; 
                        
        String respString = consentAgrContentTmpl.getHTML();
        
        System.assert(!String.isBlank(respString));
    
    }
    
    @isTest static void testMethodForPrivacyTracking() {   
    
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
   //     insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Privacy/Tracking', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
            
        consentAgrContentTmpl.testAttributes = new Map<String, String> {
                            'renderType' => 'Content',
                            'consentType' => 'Privacy/Tracking',
                            'showOnScreen' => 'show'
                        }; 
                        
        String respString = consentAgrContentTmpl.getHTML();
        
        System.assert(!String.isBlank(respString));
    }
    
    
    @isTest static void testMethodForGurantee() {   
    
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
     //   insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Guarantee', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
            
    
        consentAgrContentTmpl.testAttributes = new Map<String, String> {
                            'renderType' => 'Content',
                            'consentType' => 'Guarantee',
                            'showOnScreen' => 'show'
                        }; 
                        
        String respString = consentAgrContentTmpl.getHTML();
        
        System.assert(!String.isBlank(respString));
    }
    
    
    @isTest static void testMethodForShipping() {   
    
        HMR_CMS_ConsentAgr_ContentTemplate consentAgrContentTmpl = new HMR_CMS_ConsentAgr_ContentTemplate();
        String propertyValue = consentAgrContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
      //  HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption pickVal = new HMR_CMS_RegistrationPage_ContentTemplate.PicklistOption('General','General');
        
        String consentType = consentAgrContentTmpl.consentType;
        String showOnScreen = consentAgrContentTmpl.showOnScreen;
        
        ccrz__E_Term__c termObj1 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Health', ccrz__Enabled__c = TRUE);
        
    //    insert termObj1;
        
        ccrz__E_Term__c termObj2 = new ccrz__E_Term__c(ccrz__Description__c = 'Test Descr', Consent_Type__c = 'Shipping', ccrz__Enabled__c = TRUE);
        
        insert termObj2;
            
        consentAgrContentTmpl.testAttributes = new Map<String, String> {
                            'renderType' => 'Modal',
                            'consentType' => 'Shipping',
                            'showOnScreen' => 'show'
                        }; 
                        
        String respString = consentAgrContentTmpl.getHTML();
        
        System.assert(!String.isBlank(respString));
  }
  
  @isTest static void test_general_method_parameters() { //TODO 
    //cms.CreateContentController cc = new cms.CreateContentController();

   // init controller with parameter
    //HMR_CMS_Clinic_Hero_ContentTemplate clicnicHeroTemplatewithParam = new HMR_CMS_Clinic_Hero_ContentTemplate(cc);
  }
}