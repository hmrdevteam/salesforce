/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_CoachValueHero_ContentTemplate
*/

@isTest //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_CoachValueHero_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_CoachValueHero_ContentTemplate coachTemplate = new HMR_CMS_CoachValueHero_ContentTemplate();
    String propertyValue = coachTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    coachTemplate.testAttributes = tab;

    string s2 = coachTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = coachTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');


    String renderHTML = coachTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    HMR_CMS_CoachValueHero_ContentTemplate.getType();

    try{
        HMR_CMS_CoachValueHero_ContentTemplate qc = new HMR_CMS_CoachValueHero_ContentTemplate(null);
    }catch(Exception e){}
  }
  
}