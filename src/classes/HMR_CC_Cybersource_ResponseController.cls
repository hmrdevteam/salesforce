/**
* This class is a controller for cyber source response  
* @Date: 
* @Author: Pranay (Magnet360) 2017-03-26
* @Updated 
* @JIRA:
*/
global with sharing class HMR_CC_Cybersource_ResponseController {
	public String responseCC { get; set; }
    
	public HMR_CC_Cybersource_ResponseController() {
		if(ApexPages.currentPage().getParameters() != null) {
            responseCC = JSON.serialize(ApexPages.currentPage().getParameters());            
        }
	}
}