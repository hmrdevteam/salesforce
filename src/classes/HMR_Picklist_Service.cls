/**
* Apex Utility Service Class for Picklists
*
* @Date: 2018-01-16
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-01-16
* @JIRA: 
*/
public with sharing class HMR_Picklist_Service {
	public static List<String> getPicklistValuesByObjectAndFieldApiName(string objectApiName, string fieldApiName){
        List<String> picklistValueList = new List<String>();
        Schema.SObjectType targetObjectType = Schema.getGlobalDescribe().get(objectApiName);
        Sobject objectName = targetObjectType.newSObject();
        Schema.sObjectType sObjectType = objectName.getSObjectType(); 
        Schema.DescribeSObjectResult sObjectDescribe = sObjectType.getDescribe(); 
        Map<String, Schema.SObjectField> objectFieldMap = sObjectDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> picklistEntryList = objectFieldMap.get(fieldApiName).getDescribe().getPickListValues();  
        
        for (Schema.PicklistEntry pe : picklistEntryList) { 
            picklistValueList.add(pe.getValue());
        }
        
        return picklistValueList; 
    }
}