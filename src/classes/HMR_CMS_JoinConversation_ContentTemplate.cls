/**
* Apex Content Template Controller for Join the Conversation Component
*
* @Date: 07/14/2017
* @Author Zach Engman (Magnet 360)
* @Modified:
* @JIRA: 
*/
global virtual  class HMR_CMS_JoinConversation_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_JoinConversation_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_JoinConversation_ContentTemplate() {}
	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML(){
    	String html =   '<div class="container success">' + 
                            '<div class="row">' +
                                '<div class="col-xs-12 text-center">' +
                                    '<img class="img-responsive section-img" src="https://www.myhmrprogram.com/ContentMedia/Resources/ChatColor.png" />' +
                                '</div>' +
                                '<div class="col-xs-12">' +
                                    '<div class="title">Join the Conversation</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-sm-6">' +
                                    '<div id="list_mostPopular" class="col-xs-12 sub-title">Most Popular</div>' +
                                '</div>' +
                                '<div class="col-sm-6 col-md-5 col-md-offset-1">' +
                                    '<div id="list_hmrSuggested" class="col-xs-12 sub-title">HMR Suggested</div>' +
                                '</div>' +
                                '<div class="col-xs-12 text-center hmr-link">' +
                                    '<a class="" href="resources/forum">ALL CONVERSATIONS ></a>' +
                                '</div>' +
                            '</div>' +
                        '</div>';

    	return html;
    }
}