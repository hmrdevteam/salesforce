/**
* Test Class for SetContactTimeZone.trigger/TriggerHandler
* Updating Contact records with a Time Zone value, retrieved from 2 custom setting tables
* Objects referenced --> Contact, County__c, Zip_County__c
*
* @Date: 2017-03-19
* @Author Nathan Anderson (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-2628
*/

@isTest
private class setTimeZoneTriggerHandler_Test {
	static testMethod void testMethod1() {

		//create test custom setting Records
		County__c county = new County__c();
		county.Name = '12345';
		county.TZ_Offset__c = 6;
		county.State__c = 'IL';
		county.County_Name__c = 'Test';

		insert county;

		County__c county2 = new County__c();
		county2.Name = '12346';
		county2.TZ_Offset__c = 6;
		county2.State__c = 'IL';
		county2.County_Name__c = 'Test';

		insert county2;


		Zip_County__c zc = new Zip_County__c();
		zc.Name = '001';
		zc.FIPS_Number__c = '12345';
		zc.Zip_Code__c = '11111';

		insert zc;

		Zip_County__c zc2 = new Zip_County__c();
		zc2.Name = '001';
		zc2.FIPS_Number__c = '12346';
		zc2.Zip_Code__c = '11112';

		insert zc2;

		//Create Contact record and insert it, then update it to test both loops
		test.startTest();

			Contact c = new Contact();
			c.FirstName = 'Test';
			c.LastName = 'TestLast';
			c.MailingPostalCode = '11111';
			c.hmr_Contact_Original_Source__c = 'Search';
			c.hmr_Contact_Original_Source_Detail__c = 'Organic';

			insert c;

			c.MailingPostalCode = '11112';

			update c;

		test.stopTest();
	}

	static testMethod void testMethod2() {

		//create test custom setting Records
		County__c county = new County__c();
		county.Name = '12345';
		county.TZ_Offset__c = 6;
		county.State__c = 'IL';
		county.County_Name__c = 'Test';

		insert county;

		Zip_County__c zc = new Zip_County__c();
		zc.Name = '001';
		zc.FIPS_Number__c = '12345';
		zc.Zip_Code__c = '11111';

		insert zc;

		test.startTest();

			Contact c = new Contact();
			c.FirstName = 'Test';
			c.LastName = 'TestLast';
			c.MailingPostalCode = '11115';
			c.hmr_Contact_Original_Source__c = 'Search';
			c.hmr_Contact_Original_Source_Detail__c = 'Organic';

			insert c;

		test.stopTest();
	}
}