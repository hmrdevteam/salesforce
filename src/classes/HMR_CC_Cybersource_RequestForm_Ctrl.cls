/**
* CloudCraze + Cybersource Request Form Controller
*
* @Date: 2017-03-26
* @Author Pranay Mistry (Magnet 360)
* @Modified: 
* @JIRA: 
*/
global with sharing class HMR_CC_Cybersource_RequestForm_Ctrl {
	public List<String> signedFields { get; set; }
    public List<String> unsignedFields { get; set; }
    public String signed { get; set; }
    public String unsigned { get; set; }
    public String formAction { get; set; }
    public String selector { get; set; }
    public String postback { get; set; }
    
    public HMR_CC_Cybersource_RequestForm_Ctrl() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        formAction = params.get('formAction');
        selector = params.get('selector');

        signedFields = new List<String>();
        signed = params.get('signed');
        if (String.isNotBlank(signed)) {
            signed = signed.escapeHtml4();
            for(String s : signed.split(',')) signedFields.add(s);
        }
        unsignedFields = new List<String>();
        unsigned = params.get('unsigned');
        if (String.isNotBlank(unsigned)) {
            unsigned = unsigned.escapeHtml4();
            for(String u : unsigned.split(',')) unsignedFields.add(u);
        }
        // check if bridge page is available - ccrz 4.6+
        postback = '';
    }
}