public class ADTransitionController {

    private final Program_Membership__c pm;

    public ADTransitionController() {
        pm = [SELECT Id, Name, hmr_Contact__c, Program__c, hmr_Membership_Type__c, New_Phase__c, hmr_Status_Change_Reason__c FROM Program_Membership__c 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }

    public Program_Membership__c getpm() {
        return pm;
    }

    public PageReference save() {
        update pm;

        String NewCMUrl;

        List<Program_Membership__c> newPm = [SELECT Id, Name FROM Program_Membership__c 
                   WHERE hmr_Contact__c = :pm.hmr_Contact__c AND Id <> :pm.Id AND hmr_Status__c = 'Active'];

        if(!newPm.isEmpty()){
        	NewCMUrl = '/'+newPm[0].Id;
        }
        else{
        	NewCMUrl = '/'+pm.hmr_Contact__c;        	
        }        
        
        PageReference cmPage = new PageReference(NewCMUrl);

        return cmPage;
    }

    public PageReference cancel() {
        PageReference pmPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        
        return pmPage;
    }
}