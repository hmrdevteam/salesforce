/**
* CMS Login Service Interface that logs user in to the community
*
* @Date: 2017-03-19
* @Author Pranay Mistry (Magnet 360)
* @Modified: 2017-04-19
* @JIRA: 
*/
global class HMR_CMS_LoginServiceInterface implements cms.ServiceInterface{
    public HMR_CMS_LoginServiceInterface() {}
    
    /**
* executeRequest --> method to execute request
* @param params a map of parameters including 
* @return a JSON-serialized response string
*/
    public String executeRequest(Map<String, String> params) {
        Map<String, Object> responseString = new Map<String, Object>();
        List<User> userRecordList = new List<User>();
        PageReference loginPageRedirect = new PageReference('temp');
        String cartId = '';
        String userName = '';
        String password = '';
        String redirUrl = '';

        try {
            if(!params.isEmpty()) {
                cartId = params.get('ac');
                username = params.get('u');
                password = params.get('p');
                redirUrl = params.get('r');
                
                loginPageRedirect = Site.login(username, password, redirUrl);
                
                if(loginPageRedirect != null) {
                    string activeCartId = '';
                    userRecordList = [SELECT ContactId FROM User WHERE UserName =: username];
                    responseString.put('success', true);
                    //Transfer a guest cart if one exists, if not passed in url, check cookie
                    if(String.isBlank(cartId) && ApexPages.currentPage().getCookies().get('currCartId') != null)
                        cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
                    if(!String.isBlank(cartId)) {
                        activeCartId = cartId;
                        if(!String.isBlank(activeCartId)){
                            ccrz.cc_RemoteActionResult cartTransferResult = HMR_CC_CartTransfer_Extension.transferCartToUser(activeCartId, userRecordList[0].Id);
                            //If Cart was Transfered/Take to Cart, must pass in redirect here to login, not directly to response string or it won't login
                            if(cartTransferResult.success && redirUrl == 'checkout')
                                loginPageRedirect = Site.login(username, password, Site.getBaseUrl() + String.format('/ccrz__CheckoutNew?cartID={0}&isCSRFlow=true&portalUser=&store=&cclcl=en_US', new String[]{String.valueOf(cartTransferResult.data)}));
                        }    
                    }

                    responseString.put('redirectUrl', loginPageRedirect.getUrl());
                }
                else {
                        responseString.put('success', false);                   
                        responseString.put('failure', 'login failed');              
                        responseString.put('exceptionMsg', 'Login error: Incorrect password or email address entered. Please try again.');
                        
                    }  
            }
        }
        catch(Exception e) {
            // Unexpected error
            responseString.put('success', false);
            responseString.put('exceptionMsg', e.getMessage());
            responseString.put('exceptiongetTypeName', e.getTypeName());
            responseString.put('exceptiongetLineNumber', e.getLineNumber());
            return JSON.serialize(responseString);
        }

        //Check for consent agreement
        if(userRecordList.size() > 0 && !String.isBlank(userRecordList[0].ContactId)){
            if(hasConsentAgreements(userRecordList[0].ContactId)){
                string startUrl = String.valueOf(responseString.get('redirectUrl'));
                loginPageRedirect = Site.login(username, password, Site.getBaseUrl() + '/consent?startUrl=' + startUrl);
                responseString.put('redirectUrl', loginPageRedirect.getUrl());
            }
        }
        // No actions matched and no error occurred
        return JSON.serialize(responseString);
    }

    private boolean hasConsentAgreements(string contactId){
        List<Consent_Agreement__c> cList = [SELECT Consent_Type__c FROM Consent_Agreement__c WHERE Status__c = 'Notified' AND Contact__c = :contactId];
        return cList.size() > 0;
    }
    
    //Checks if there is a cart within the cookies and if so transfers it to the user upon login
    /*
private static ccrz.cc_RemoteActionResult transferGuestCart(String username){
ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
result.success = false;

if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: username];

if(!String.isBlank(cartId) && userRecordList.size() > 0)
result = HMR_CC_CartTransfer_Extension.transferCartToUser(cartId, userRecordList[0].Id);
}

return result;
}
*/
    /**
* getType --> global method to override cms.ServiceInterface
* @param  
* @return a JSON-serialized response string
*/
    public static Type getType() {
        return HMR_CMS_LoginServiceInterface.class;
    }
}