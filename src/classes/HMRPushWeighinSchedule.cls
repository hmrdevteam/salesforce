/**
 * convenience class to allow scheduling notifications using "Schedule Apex"
 */
global class HMRPushWeighinSchedule implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        try {
            HMRPushNotificationJob bj = new HMRPushNotificationJob(HMRNotificationType.WEIGHIN_REMINDER);
            database.executebatch(bj, 200);
        }
        catch(Exception ex) {
            System.debug('The HMRPushWeighinSchedule failed ' + ex.getMessage());
        }       
    } 
    
}