/**
* 
*
* @Date: 2017-08-03
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: Zach Engman (Magnet 360) - 2017-08-04, Utkarsh Goswami - 2017-08-10
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_SuccessStoryList_ContentTemplate extends cms.ContentTemplateController{
  
   private Integer pageNumber = 1;
   private Integer recordPerPage = 10;
   public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
   }

   //Constructors (With and Without Parameters)
   global HMR_CMS_SuccessStoryList_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }

    global HMR_CMS_SuccessStoryList_ContentTemplate() {
        super();
    }
    global HMR_CMS_SuccessStoryList_ContentTemplate(String pageNumber){
          this.pageNumber = Integer.valueOf(pageNumber);         
    }
    
    global virtual override String getHTML() { 
        String html = '';
        String baseUrl = IsProduction() ? '' : Site.getBaseUrl();
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        List<HMR_Knowledge_Service.ArticleRecord> articleRecordList = knowledgeService.getByArticleType('Success_Story__kav', recordPerPage * pageNumber, 0, 'AND Publish_to_Success_Story_Page__c = false');
        Integer totalStories = knowledgeService.getCountByArticleType('Success_Story__kav', 'AND Publish_to_Success_Story_Page__c = false');

        html += '<div class="success-list">';                // extra div added 
            html += '<div class="container hmr all-stories">';
                html += '<div class="row">';
                html += '<div class="col-xs-12">';
                    html += '<div class="col-xs-6 col-sm-4 header">NAME</div>';
                    html += '<div class="col-sm-5 hidden-xs header">HOMETOWN</div>';
                    html += '<div class="col-xs-6 col-sm-3 header">WEIGHT LOSS</div>';
                    html += '<div class="col-xs-12 divider"></div>';
                html += '</div>';
                
                for(HMR_Knowledge_Service.ArticleRecord articleRecord : articleRecordList){
                
                    html += '<div class="col-xs-12 pointer" onclick="successStoryList.navigateToRecord(\'' + articleRecord.UrlName + '\')">';
                        html += '<div class="col-xs-6 col-sm-4 detail">'+ articleRecord.ClientFirstName + '</div>';
                        html += '<div class="col-sm-5 hidden-xs detail">' + articleRecord.Hometown + '</div>';
                        html += '<div class="col-xs-6 col-sm-3 detail">' + String.valueOf(articleRecord.PoundsLost).replace('.00', '') + ' lbs</div>';
                        html += '<div class="col-xs-12 divider"></div>';
                    html += '</div>';
                
                }
            html += '</div>';
            html += '</div>';
            
            html += '<div class="container hmr">';
                html += '<div class="row">';
                html += '<div class="col-sm-6 hidden-xs">';
                    html += '<div class="link-container">';
                        html += '<div class="hmr-allcaps-container">';
                            html += '<a href="' + baseUrl + '/resources" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue">&lt;</a>';
                            html += '<a href="' + baseUrl + '/resources" class="hmr-allcaps hmr-allcaps-blue left">BACK TO RESOURCES</a>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-12">';          
                    if(pageNumber < Math.ceil(Double.valueOf(totalStories)/recordPerPage)){
                        html += '<div class="row show-more" style="padding-bottom: 60px;">';
                            html += '<div class="col-xs-12 text-center">';
                                html += '<div class="link-container">';
                                    html += '<div class="hmr-allcaps-container">';
                                        html += '<a onclick="successStoryList.showNext(event,' + pageNumber + ')" class="hmr-allcaps hmr-allcaps-blue card-list-more">SEE MORE</a>';
                                        html += '<a onclick="successStoryList.showNext(event,' + pageNumber + ')" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue card-list-more">&gt;</a>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                        html += '</div>';
                    }  
                html += '</div>';
            html += '</div>';
            html += '</div>';
        html += '</div>';             // extra div ends

        return html;
    }
}