/*
* Apex Content Template Controller for the Questions List page
*
* @Date: 07/24/2017
* @Author Ali Pierre (HMR)
* @Modified: Zach Engman - 08/03/2017
* @JIRA: 
*/


global virtual class HMR_CMS_Questions_List_ContentTemplate extends cms.ContentTemplateController{
  public List<QuestionAnswerResult> displayFeed{get;set;}
  public String topicId {get; set;}
  ConnectApi.FeedElementPage result;
  public integer offset = 10;
  public Set<String> suggestedPostsSet = new Set<String>();
  public Integer numberlinks = 0;
  public String atlTopic = '';

  public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
  }

  global HMR_CMS_Questions_List_ContentTemplate(cms.createContentController cc) {
    super(cc);
  }
  global HMR_CMS_Questions_List_ContentTemplate() {}

  global HMR_CMS_Questions_List_ContentTemplate(Integer offset, String topic){
      this.offset = offset;
      this.atlTopic = topic;
    }

  /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

     public String topicName{
         get{
            return getPropertyWithDefault('topicName', '[]');
        }
     }


   global virtual override String getHTML(){
     String html = '';
     String baseUrl = IsProduction() ? '' : Site.getBaseUrl();
     integer postCount = 0;

     try {
 
       HMR_Topic_Service topicService = new HMR_Topic_Service();
        
       topicId = atlTopic == ''? topicService.getTopicId(topicName) : topicService.getTopicId(atlTopic);
        //calling this method on page load to display the list
        getFeedResult();
        createFeedItemList(topicService);

         html += '<div class="questions-topic-detail container">';
                  if(atlTopic == ''){
                    html +='<input id="questionListTitle" type="hidden" value="' + topicName + '">';
                  }
                  else{
                    html +='<input id="questionListTitle" type="hidden" value="' + atlTopic + '">';
                  }
                  
                           html +='<div class="row">'+
                               '<div class="col-xs-12 start-a-topic">'+
                                   '<a href="#" class="btn hmr-btn-blue hmr-btn-small white" id="startTopicBtn" onclick="postQuestion.postQuestionModal(); ">Post a Question</a>'+
                               '</div>'+
                           '</div>'+
                           '<div class="row">'+
                               '<div class="post-list-header hidden-xs col-xs-12">'+
                                   '<div class="row">'+
                                       '<div class="col-xs-1 col-xs-offset-10 text-center">'+
                                           '<span class="text-16">Replies </span>'+
                                       '</div>'+
                                       '<div class="col-xs-1 text-center ">'+
                                           '<span class="text-16">Views </span>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                           '</div> '+
                       '<div class="row">';
                       if(!displayFeed.isEmpty()){
                            for(QuestionAnswerResult feed : displayFeed){
                              if(postCount <= offset){   
                                 html += '<div class="post-container pointer" onclick="conversation.navigateToRecord(\'' + feed.feedElement.Id + '\', \'' + feed.relatedTopicList[0] + '\')" >';
                                 if(feed.suggested == true){
                                     html += '<div class="post-list active">';
                                 }
                                 else{
                                     html += '<div class="post-list">';
                                 }
                                         //<!-- Media middle -->
                                      html +='<div class="media">'+
                                             '<div class="media-left">';
                                             if(feed.suggested == true){
                                                 html +='<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ChatColor.png" class="media-object">';
                                             }
                                             else{
                                                 html +='<img src="https://www.myhmrprogram.com/ContentMedia/Resources/chat-icon.png" class="media-object">';
                                             }
                                             html += '</div>'+
                                             '<div class="media-body row">'+
                                                 '<p class="col-xs-12 col-sm-10">' + feed.feedElement.capabilities.questionAndAnswers.questionTitle + '<br />'+
                                                 'by ' + feed.userSummary.firstname + ' ' + feed.userSummary.lastname.substring(0, 1) + '. on ' + feed.feedElement.CreatedDate.Format('M-dd-yyy') +
                                                 '</p>'+
                                                 '<div class="media-footer row">'+
                                                     '<div class="hidden-xs col-sm-2 no-padding">'+
                                                         '<span class="text-20 topic-number text-left col-xs-6" id="">' + feed.feedElement.capabilities.comments.page.total + '</span>'+
                                                         '<span class="text-20 post-number col-xs-6 text-left" id="">' + feed.feedElement.capabilities.interactions.count + '</span>'+
                                                     '</div>'+
                                                     '<div class="visible-xs-block col-xs-12 text-left">'+
                                                         '<div class="col-xs-12">'+
                                                             '<span class="topic-number text-left" id="">'+
                                                                 '<span class="mobile-topic">Replies </span>' + feed.feedElement.capabilities.comments.page.total + '</span>'+
                                                             '<span class="post-number" id="">'+
                                                                 '<span class="mobile-post">Views </span>' + feed.feedElement.capabilities.interactions.count + '</span>'+
                                                         '</div>'+
                                                     '</div>'+
                                                 '</div>'+
                                             '</div>'+
                                         '</div>'+
                                     '</div>'+
                                 '</div>'; 
                                }                                
                              postCount++;       
                            }
                       }
                       else{
                        html += '<p>No Posts for ' + topicName + '</p>';
                       }                       

                      html += '<div class="col-xs-12 text-right">'+
                                '<div class="col-sm-6 hidden-xs">'+
                                    '<div class="link-container">'+
                                        '<div class="hmr-allcaps-container" style="text-align: left;">'+
                                            '<a style="padding-left:0px;" class="hmr-allcaps hmr-allcaps-blue left" href="' + baseUrl + '/resources/forum">BACK TO ALL CATEGORIES</a>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                       '</div>';
                       
                  html +='</div>';   
                  if(displayFeed.size() > offset){
                    html += '<div class="row show-more" style="padding-bottom: 60px;">' +
                                '<div class="col-xs-12 text-center">' +
                                    '<div class="link-container">' +
                                        '<div class="hmr-allcaps-container">' +
                                            '<a onclick="QuestionsList.doInit(\'' + (offset + 10) + '\')" class="hmr-allcaps hmr-allcaps-blue card-list-more">SEE MORE</a>' +
                                            '<a onclick="QuestionsList.doInit(\'' + (offset + 10) + '\')" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-blue card-list-more">&gt;</a>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                  }
                        
                   html += '<div id="postQuestionModal" class="hmr-modal modal question fade" role="dialog">'+
                                '<div class="modal-dialog">'+
                                   '<div class="modal-content">'+
                                        '<div class="modal-header">'+
                                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                                                '<span aria-hidden="true">&times;</span>'+
                                            '</button>'+
                                            '<h4 class="modal-title">Post a Question</h4>'+
                                        '</div>'+
                                        '<div class="modal-body">'+
                                            '<form>'+
                                                  '<div class="form-group">'+
                                                    '<label for="subject" class="small-title">Subject</label>'+
                                                    '<input type="text" class="form-control" id="subject" placeholder=""/>'+
                                                  '</div>'+
                                                  '<div class="form-group">'+
                                                    '<label for="message" class="small-title">Message</label>'+
                                                    '<textarea name="post-rich-text" id="message" class="form-control"></textarea>'+
                                                '</div>'+
                                                '<div class="col-xs-12 text-right btn-sequence">'+
                                                    '<span class="hmr-allcaps-container">' +
                                                      '<a class="hmr-allcaps hmr-allcaps-blue post-btn" data-dismiss="modal">CANCEL</a>' +
                                                    '</span>' + 
                                                    '<span class="hmr-allcaps-container">' +
                                                      '<a class="hmr-allcaps hmr-allcaps-blue post-btn" onclick="postQuestion.postNewQuestion();">POST</a>'+
                                                    '</span>' +
                                                '</span>'+
                                            '</form>'+
                                        '</div>'+
                                        '<div class="modal-footer text-right"></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
     }
     catch(Exception ex) {
       html += 'Exception in getting Topics for Questions ' + ex.getMessage();
     }
      return html;
    }

  //calling this method first time from the constructor, to get the list on page load
  public void getFeedResult(){
    String communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;

    result = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topicId);
  }

  public class QuestionAnswerResult{
    public ConnectApi.FeedElement feedElement{get; set;}
    public ConnectApi.UserSummary userSummary {get; set;}
    public Boolean suggested {get; set;}
    public List<String> relatedTopicList {get; set;}

    public QuestionAnswerResult(ConnectApi.FeedElement feedElement){
        this.feedElement = feedElement;
        this.userSummary = ((ConnectApi.UserSummary)feedElement.parent);
        this.suggested = false;
        this.relatedTopicList = new List<String>();
        if(feedElement.capabilities.topics != null && feedElement.capabilities.topics.items != null){
            for(ConnectApi.Topic topic : feedElement.capabilities.topics.items){
                if(topic.nonLocalizedName != 'HMR Suggested') //Skip HMR Suggested
                    relatedTopicList.add(topic.nonLocalizedName);
            }
        }
    }
  }

   //method to generate the feeditem list using internal class
   public void createFeedItemList(HMR_Topic_Service topicService){
        displayFeed = new List<QuestionAnswerResult>();
        List<QuestionAnswerResult> unsortedDisplayFeed = new List<QuestionAnswerResult>();
        Set<String> feedElementIdSet = new Set<String>();

        List<ConnectApi.FeedElement> questionList = result.elements;

        if(questionList.size() > 10){
          numberlinks = Math.mod(questionList.size(), 10) > 0 ? (questionList.size() / 10) + 1: (questionList.size() / 10);
        }else{
          numberlinks = 0;
        }

        for(ConnectApi.FeedElement fi : questionList){
          if(Test.isRunningTest()){
            HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
            ConnectApi.FeedElement fe = connectApiService.postQuestionAndAnswer(topicId, 'subject', 'body');
            unsortedDisplayFeed.add(new QuestionAnswerResult(fe));
            feedElementIdSet.add(fe.id);   
          }else if(fi.capabilities.questionAndAnswers != null){
              unsortedDisplayFeed.add(new QuestionAnswerResult(fi));
              feedElementIdSet.add(fi.id);                
          }                        
        }
        suggestedPostsSet = topicService.suggestedPosts(feedElementIdSet);

        for(QuestionAnswerResult qa : unsortedDisplayFeed){
            if(suggestedPostsSet.contains(qa.feedElement.id)){
                qa.suggested = true;
            
                if(displayFeed.size()==0){
                  displayFeed.add(qa);
                }
                else{
                  displayFeed.add(0,qa);
                }
            }
            else
            {
                displayFeed.add(qa);
            }
        }
    }

}