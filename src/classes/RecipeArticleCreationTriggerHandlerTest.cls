@isTest
public class RecipeArticleCreationTriggerHandlerTest {

     static testMethod void RecipeTestMethod(){



        Profile prof = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        Account communityAc = new Account(name ='testComunity') ;
        insert communityAc;

        Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
        insert con;

        User userTest = new User(alias = 'test123', email='testtest12345@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id, country='United States',IsActive =true,
                 UserPermissionsKnowledgeUser = TRUE,
                timezonesidkey='America/Los_Angeles', username='testtest1234@noemail.com');

        List<HMR_Recipe__c> approvedRecipe = new List<HMR_Recipe__c>();

        Map<Id, HMR_Recipe__c> oldrecipeMap = new Map<Id, HMR_Recipe__c>();

         Test.startTest();
        HMR_Recipe__c Recipe1 = new HMR_Recipe__c(Contact__c = con.id, Approval_Status__c = 'Approved', Recipe_Name__c = 'TEST',
                                                          VF_Servings__c=10, hmr_Recipe_Category__c='Shakes',Date_Published__c=Date.today());
        insert Recipe1;

        Recipe_Ingredients__c ingrd=new Recipe_Ingredients__c();
            ingrd.Name='Milk';
            ingrd.Ingredient_Type__c='Other';
            ingrd.Quantity__c='1';
            ingrd.Quantity_Type__c='Cup';
            ingrd.Recipe__c=Recipe1.id;
            insert ingrd;

        Recipe_Instruction__c instruct= new Recipe_Instruction__c();
            instruct.Step_Number__c=1;
            instruct.Instruction__c='Add milk';
            instruct.Recipe__c=Recipe1.id;
            insert instruct;

        Recipe1.Desciption__c='test';
        update Recipe1;

       // Recipe__kav chkKA=[Select Id from Recipe__kav where Recipe__c =: Recipe1.id];
        //system.assert(chkKA!=null,'Article created');

        HMR_Recipe__c Recipe2 = new HMR_Recipe__c(Contact__c = con.id, Approval_Status__c = 'Submitted', Recipe_Name__c = 'TEST2',
                                                          VF_Servings__c=10, hmr_Recipe_Category__c='Shakes',Date_Published__c=Date.today());
        insert Recipe2;

        Recipe2.Desciption__c='test';
        update Recipe2;

        System.runAs(userTest){
            HMR_Recipe__c Recipe3 = new HMR_Recipe__c(Contact__c = con.id, Approval_Status__c = 'Approved', Recipe_Name__c = 'TEST3',
                                                              VF_Servings__c=10, hmr_Recipe_Category__c='Shakes',Date_Published__c=Date.today());
            insert Recipe3;

            Recipe__kav recipeArticle = new Recipe__kav( Recipe__c =Recipe3.id ,Language = 'en_US', UrlName='recpytes' ,Title='Test',Submitted_By__c=con.id);
            insert recipeArticle;


            recipeArticle = [SELECT KnowledgeArticleId,PublishStatus,Recipe__c  FROM Recipe__kav WHERE Id = :recipeArticle.Id];

            try {
                KbManagement.PublishingService.publishArticle(recipeArticle.KnowledgeArticleId, true);
            }
            catch(exception e) {

              System.debug('EXception ->   ' + e.getMessage());

            }

            Recipe3.Desciption__c='test';
            update Recipe3;

            Test.stopTest();
        }
    }


}