/**
 *  A service for interfacing with the Cloud Craze API to perform address related operations
 */
public with sharing class HMR_CMS_AddressService {

    public static Map<String, Object> getAddressesByIds(Set<Id> addressIdSet){
        // populate input map object for CC API
        Map<String,Object> inputData = new Map<String,Object>{
         ccrz.ccApi.API_VERSION => 6,
         ccrz.ccApiAddress.ADDRESSIDLIST => addressIdSet
        };

        return ccrz.ccAPIAddress.fetch(inputData);
    }

    public static Map<String, Object> getAddressBookByOwnerId(Id ownerId, String addressType) {
        // populate input map object for CC API
        Map<String,Object> inputData = new Map<String,Object>{
         ccrz.ccApi.API_VERSION => 4,
         ccrz.ccAPIAddressBook.OWNERID => ownerId
        };

        // Add address type to CC API input object if given
        if(addressType != null) {
            inputData.put(ccrz.ccAPIAddressBook.TYPE, addressType);
        }

        return getAddressBook(inputData);
    }

    private static Map<String, Object> getAddressBook(Map<String, Object> inputData) {
        // Perform callout to CC API
        Map<String, Object> outputData = ccrz.ccAPIAddressBook.fetch(inputData);

        // Output API response data for debugging purposes
        System.debug('*** ccAPIAddressBook response: ' + JSON.serializePretty(outputData));

        // Return api resonse
        return outputData;
    }

    public static void createAddressWithAdressBookEntries(Id ownerId, HMR_CMS_AddressService.AddressRecord address) {
        Map<String, Object> addressDataMap = address.getDataMap(false);
        Map<String, Object> outputData = createAddress(addressDataMap);

        Id addressId;
        // Parse out and store the id of the new address
        if (outputData.get(ccrz.ccApiAddress.ADDRESSIDLIST) != null) {
            List<String> addressIdList = (List<String>) outputData.get(ccrz.ccApiAddress.ADDRESSIDLIST);
            addressId = addressIdList.get(0);
        } else {
            throw new CalloutException('An error occurred when parsing the Cloud Craze API response');
        }
        System.debug('*** Address Id: ' + addressId);

        if(address.isShippingAddress) {
            createAddressBookEntry(ownerId, addressId, address.isDefaultShippingAddress, 'Shipping');
        }
        if(address.isBillingAddress) {
            createAddressBookEntry(ownerId, addressId, address.isDefaultBillingAddress, 'Billing');
        }
    }


    private static Map<String, Object> createAddressBookEntry(Id ownerId, Id addressId, Boolean isDefault, String addressType) {
        List<Map<String, Object>> addressBookToInsert = new List<Map<String, Object>> {
            new Map<String, Object> {
                'owner' => ownerId,
                'addressType' => addressType,
                'default' => isDefault,
                'EContactAddress' => addressId
            }
        };

        // populate meta CC API information for address book creation callout
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccApi.API_VERSION => 4,
            ccrz.ccApiAddressBook.ADDRESSBOOKS => addressBookToInsert,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                ccrz.ccAPIAddressBook.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_REFETCH => true
                }
            }
        };

        // Perform callout to API to create address book record
        Map<String, Object> outputData = ccrz.ccAPIAddressBook.createAddressBook(inputData);
        System.debug('*** Addressbook response: ' + JSON.serializePretty(outputData));

        // return CC API response
        return outputData;
    }

    private static Map<String, Object> reviseAddressBookEntry(Id addressBookId, Id addressId, Boolean isDefault, String addressType) {
        List<Map<String, Object>> addressBookToInsert = new List<Map<String, Object>> {
            new Map<String, Object> {
                'sfid' => addressBookId,
                'addressType' => addressType,
                'default' => isDefault,
                'EContactAddress' => addressId
            }
        };

        // populate meta CC API information for address book creation callout
        Map<String, Object> inputData = new Map<String, Object> {
            ccrz.ccApi.API_VERSION => 4,
            ccrz.ccApiAddressBook.ADDRESSBOOKS => addressBookToInsert,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                ccrz.ccAPIAddressBook.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_REFETCH => true
                }
            }
        };

        // Perform callout to API to create address book record
        Map<String, Object> outputData = ccrz.ccAPIAddressBook.revise(inputData);
        System.debug('*** Addressbook response: ' + JSON.serializePretty(outputData));

        // return CC API response
        return outputData;
    }

    public static void updateAddressBook(Id ownerId, HMR_CMS_AddressService.AddressRecord address) {
        Map<String, Object> addressDataMap = address.getDataMap(true);
        Map<String, Object> outputData = reviseAddress(addressDataMap);

        // perform removals
        List<Id> addressBookEntriesToRemove = new List<Id>();
        if(!address.isShippingAddress && address.shippingAddressBookEntry != null) {
            addressBookEntriesToRemove.add(address.shippingAddressBookEntry);
        }
        if(!address.isBillingAddress && address.billingAddressBookEntry != null) {
            addressBookEntriesToRemove.add(address.billingAddressBookEntry);
        }
        removeAddressBook(addressBookEntriesToRemove);


        // perform updates
        if(address.isShippingAddress && address.shippingAddressBookEntry != null) {
            reviseAddressBookEntry(
                address.shippingAddressBookEntry,
                address.id,
                address.isDefaultShippingAddress,
                'Shipping'
            );
        }
        if(address.isBillingAddress && address.billingAddressBookEntry != null) {
            reviseAddressBookEntry(
                address.billingAddressBookEntry,
                address.id,
                address.isDefaultBillingAddress,
                'Billing'
            );
        }

        //Perform inserts
        if(address.isShippingAddress && address.shippingAddressBookEntry == null) {
            createAddressBookEntry(
                ownerId,
                address.id,
                address.isDefaultShippingAddress,
                'Shipping'
            );
        }
        if(address.isBillingAddress && address.billingAddressBookEntry == null) {
            createAddressBookEntry(
                ownerId,
                address.id,
                address.isDefaultBillingAddress,
                'Billing'
            );
        }

    }

    public static void removeAddressBook(HMR_CMS_AddressService.AddressRecord address) {
        List<Id> addressBookEntriesToRemove = new List<Id>();
        if(address.shippingAddressBookEntry != null) {
            addressBookEntriesToRemove.add(address.shippingAddressBookEntry);
        }
        if(address.billingAddressBookEntry != null) {
            addressBookEntriesToRemove.add(address.billingAddressBookEntry);
        }

        if(!addressBookEntriesToRemove.isEmpty()) {
            removeAddressBook(addressBookEntriesToRemove);
        }
    }

    public static Map<String, Object> removeAddressBook(List<Id> entriesToRemove) {
        Map<String,Object> inputData = new Map<String,Object> {
            ccrz.ccApi.API_VERSION => 4,
            ccrz.ccApiAddressBook.IDS => entriesToRemove
        };

        Map<String, Object> outputData = ccrz.ccAPIAddressBook.removeAddressBook(inputData);
        return outputData;
    }

    public static Map<String, Object> reviseAddress(Map<String, Object> addressData) {
        // populate input map object for cc API
        Map<String, Object> inputAddressData = new Map<String, Object> {
            ccrz.ccApi.API_VERSION => 4,
            ccrz.ccApiAddress.ADDRESSLIST => new List<Map<String, Object>> { addressData },
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                ccrz.ccApiAddress.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_REFETCH => true
                }
            }
        };

        // Perform callout to API to create address record
        Map<String, Object> outputData = ccrz.ccAPIAddress.revise(inputAddressData);

        // Output API response data for debugging purposes
        System.debug(JSON.serializePretty(outputData));

        // return CC API response
        return outputData;
    }

    public static Map<String, Object> createAddress(Map<String, Object> addressData) {

        return createAddresses(new List<Map<String, Object>> { addressData });
    }

    public static Map<String, Object> createAddresses(List<Map<String, Object>> addressDataList) {
        // populate input map object for cc API
        Map<String, Object> inputAddressData = new Map<String, Object> {
            ccrz.ccApi.API_VERSION => 4,
            ccrz.ccApiAddress.ADDRESSLIST => addressDataList,
            ccrz.ccAPI.SIZING => new Map<String, Object> {
                ccrz.ccApiAddress.ENTITYNAME => new Map<String, Object> {
                    ccrz.ccAPI.SZ_REFETCH => true
                }
            }
        };

        // Perform callout to API to create address records
        Map<String, Object> outputData = ccrz.ccAPIAddress.createAddress(inputAddressData);

        // Output API response data for debugging purposes
        System.debug(JSON.serializePretty(outputData));

        return outputData;
    }

    public class AddressRecord{
        public string id {get; private set;}
        public string ownerId {get; private set;}
        public string firstName {get; private set;}
        public string lastName {get; private set;}
        public string addressFirstline {get; private set;}
        public string addressSecondline {get; private set;}
        public string city {get; private set;}
        public string stateISOCode{get; private set;}
        public string postalCode{get; private set;}
        public string country{get; private set;}
        public string countryISOCode{get; private set;}

        public boolean isDefault {get;set;}
        public boolean isBillingShipping {get;set;}
        public String addressType {get;set;}
        public List<AddressBookRecord> addressBookList {get; set;}


        public boolean isShippingAddress {get; set;}
        public boolean isBillingAddress {get; set;}

        public Id shippingAddressBookEntry {get; set;}
        public Id billingAddressBookEntry {get; set;}

        public boolean isDefaultShippingAddress {get; set;}
        public boolean isDefaultBillingAddress {get; set;}


        public AddressRecord(Map<String, Object> addressMap){

            this.id = (String)addressMap.get('sfid');
            this.ownerId = (String)addressMap.get('ownerId');
            this.firstName = (String)addressMap.get('firstName');
            this.lastName = (String)addressMap.get('lastName');
            this.addressFirstline = (String)addressMap.get('addressFirstline');
            this.addressSecondline = (String)addressMap.get('addressSecondline');
            this.city = (String)addressMap.get('city');
            this.stateISOCode = (String)addressMap.get('stateISOCode');
            this.postalCode = (String)addressMap.get('postalCode');
            this.country = 'United States';
            this.countryISOCode = 'US';
            this.isBillingShipping = FALSE;
            this.addressBookList = new List<AddressBookRecord>();
            this.isShippingAddress = false;
            this.isBillingAddress = false;
            this.isDefaultShippingAddress = false;
            this.isDefaultBillingAddress = false;
        }

        public Map<String, Object> getDataMap(boolean includeId) {
            Map<String, Object> addressDataMap = new Map<String, Object> {
                'lastName' => this.lastName,
                'firstName' => this.firstName,
                'addressFirstline' => this.addressFirstline,
                'addressSecondline' => this.addressSecondline,
                'city' => this.city,
                'stateISOCode' => this.stateISOCode,
                'postalCode' => this.postalCode,
                'country' => this.country,
                'countryISOCode' => this.countryISOCode
            };

            if(includeId) {
                addressDataMap.put('sfid', this.Id);
            }

            return addressDataMap;
        }
    }

    public class AddressBookRecord{
        public string id {get; private set;}
        public string contactAddressId {get; private set;}
        public boolean isDefault {get; private set;}
        public string addressType {get; private set;}

        public AddressBookRecord(Map<String, Object> addressBookMap){
            this.id = (String)addressBookMap.get('sfid');
            this.contactAddressId = (String)addressBookMap.get('EContactAddress');
            this.isDefault = (Boolean)addressBookMap.get('default');
            this.addressType = (String)addressBookMap.get('addressType');
        }

    }
}