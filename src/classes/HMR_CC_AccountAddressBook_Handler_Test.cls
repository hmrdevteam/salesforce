/*****************************************************
 * Author: Zach Engman
 * Created Date: 06/08/2017
 * Created By: Zach Engman
 * Last Modified Date: 06/08/2017
 * Last Modified By:
 * Description: Test Coverage for Handler of the ccrz__E_AccountAddressBook__c object triggers
 * ****************************************************/
@isTest
private class HMR_CC_AccountAddressBook_Handler_Test {
	@testSetup
    private static void setupData(){
        insert new Account(Name = 'Test Account', 
        				   BillingStreet = '1111 Main Street',
        				   BillingCity = 'Boston',
        				   BillingState = 'MA',
        				   BillingPostalCode = '01841',
        				   BillingCountry = 'US');
        insert cc_dataFactory.getContactAddress();
    }

	@isTest
	private static void testBeforeInsertAccountLookupPopulation(){
		Account accountRecord = [SELECT Id FROM Account LIMIT 1];
		ccrz__E_ContactAddr__c contactAddressRecord = [SELECT Id FROM ccrz__E_ContactAddr__c LIMIT 1];

		ccrz__E_AccountAddressBook__c addressBookRecord  = new ccrz__E_AccountAddressBook__c(ccrz__AccountId__c = accountRecord.Id,
																						     ccrz__AddressType__c = 'Billing',
																						     ccrz__E_ContactAddress__c = contactAddressRecord.Id,
																						     ccrz__Default__c = true);
		Test.startTest();

		insert addressBookRecord;

		Test.stopTest();

		//Verify the account lookup field was populated and that it matches the original value assigned to the text field
		ccrz__E_AccountAddressBook__c addressBookResultRecord = [SELECT ccrz__Account__c FROM ccrz__E_AccountAddressBook__c LIMIT 1];
		System.assert(!String.isBlank(addressBookResultRecord.ccrz__Account__c));
		System.assertEquals(addressBookRecord.ccrz__AccountId__c, addressBookResultRecord.ccrz__Account__c);
	}

	@isTest
	private static void testBeforeInsertAccountTextPopulation(){
		Account accountRecord = [SELECT Id FROM Account LIMIT 1];
		ccrz__E_ContactAddr__c contactAddressRecord = [SELECT Id FROM ccrz__E_ContactAddr__c LIMIT 1];

		ccrz__E_AccountAddressBook__c addressBookRecord  = new ccrz__E_AccountAddressBook__c(ccrz__Account__c = accountRecord.Id,
																						     ccrz__AddressType__c = 'Billing',
																						     ccrz__E_ContactAddress__c = contactAddressRecord.Id,
																						     ccrz__Default__c = true);
		Test.startTest();

		insert addressBookRecord;

		Test.stopTest();

		//Verify the account text field was populated and that it matches the original value assigned to the lookup field
		ccrz__E_AccountAddressBook__c addressBookResultRecord = [SELECT ccrz__AccountId__c FROM ccrz__E_AccountAddressBook__c LIMIT 1];
		System.assert(!String.isBlank(addressBookResultRecord.ccrz__AccountId__c));
		System.assertEquals(addressBookRecord.ccrz__Account__c, addressBookResultRecord.ccrz__AccountId__c);
	}
}