public class HMRMobileAppCtlr {
    public String val {get; set;}
    
    public String u {get; set;}
    public String p {get; set;}
    public String body {get; set;}
    public String sid {get; set;}
    
    public PageReference login11() {

		val = u + p;
        return null;
    }
    
    public PageReference login1() {
        PageReference curPage = ApexPages.currentPage();
        Map<String,String> paramMap = curPage.getParameters();
        List<String> paramKeys = new List<String>(paramMap.keySet());

        body = '';
        if (!paramKeys.isEmpty() ) {
            
            
            for(String p: paramKeys) {
                body = body + 'key ' + p + ', val:' + paramMap.get(p) + ', ';
            }
            //body = paramMap.get('un');
            //body = body + paramMap.get('pw');
            //body = body + paramMap.get('startURL');            
            //return Site.login(paramMap.get('un'), paramMap.get('pw'), paramMap.get('startURL'));
            return null;
        }

        else {
            body = 'nothing in post body';
            // else request body is empty
            return null;             
        }        
    }
    
    public PageReference login3() {
        body = System.currentPageReference().getParameters().get('startURL');
        return null;
    }
    
    public PageReference login10() {
        String un = ApexPages.currentPage().getParameters().get('un');
        String pw = ApexPages.currentPage().getParameters().get('pw');
		val = u + p;
        
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        PageReference pr = Site.login(un, pw, '/apex/HMRViewSessionId');        
        if (pr != null) {
            sid = UserInfo.getSessionId();
        }
        //return pr;
        return new PageReference('/apex/HMRViewSessionId');
    }    
    
    public PageReference login() {
        String un = ApexPages.currentPage().getParameters().get('un');
        String pw = ApexPages.currentPage().getParameters().get('pw');
		val = u + p;
        
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(un, pw, startUrl);        
    }
}