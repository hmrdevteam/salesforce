/**
* Apex Service Class for the CMS Api Services
*
* @Date: 2018-01-23
* @Author: Zach Engman
* @Modified: 
* @JIRA: 
*/
global with sharing class HMR_CMS_ApiService extends HMR_CMS_API {
	public HMR_CMS_ApiService() {}
	public HMR_CMS_ApiService(string siteName, string apiVersion) {
		this.siteName = siteName;
		this.apiVersion = apiVersion;
	}

	/**
     * Retrieves the content for the day
     *
     * @param journeyDay - the day of content to retrieve data for
     */
	public DailyTipContent getDailyTipsContentByTaxonomyTag(string taxonomyTag){
		HMR_CMS_RenderingApiService renderingApiService = new HMR_CMS_RenderingApiService(this.siteName, this.apiVersion);
		HMR_CMS_ContentApiService contentApiService = new HMR_CMS_ContentApiService(this.siteName, this.apiVersion);
		DailyTipContent dailyTipContent = new DailyTipContent();

		try{
			//Retrieve the rendering content by taxonomy for the day
			JSONMessage.APIResponse response = renderingApiService.getRenderedContentByTaxonomyTag(
	                    new List<String>{taxonomyTag},
	                    new List<List<String>>{new List<String>{'HMR_DailyTip_ContentTemplate'}},
	                    new List<String>{'HMR_DailyTip_ContentType'}
	        );

	        //Retrieve the rendered list of content
	        List<Object> renderingList = (List<Object>)(((Map<String, Object>)JSON.deserializeUntyped(response.responseObject)).get('renderings'));
	        //Iterate over the content to retrieve the individual content items
	        if(renderingList.size() > 0){
	        	//If more than one, choose one at random
	        	Map<String, Object> renderingObjectMap = (Map<String, Object>)renderingList[Math.round(Math.random() * (renderingList.size() - 1))];
	        	
        		String originId = (String)renderingObjectMap.get('contentId');

        		//Retrieve the content values from Orchestra
        		JSONMessage.APIResponse contentApiResponse = contentApiService.getContentByObjectId(originId);
        		Map<String, Object> contentResponseMap = (Map<String, Object>)JSON.deserializeUntyped(contentApiResponse.responseObject);
        		Map<String, Object> contentAttributeMap = (Map<String, Object>)contentResponseMap.get('contentAttributes');
				//Attributes are returned in a language map to support multiple languages, right now we only use english
				List<Object> englishAttributeList = (List<Object>)contentAttributeMap.get('en_US');
				
				for(object attributeItem : englishAttributeList){
				    Map<String, Object> attributeMap = (Map<String, Object>)attributeItem;
				    String attributeName = (String)attributeMap.get('name');

				    if(attributeName == 'linkDestination')
				    	dailyTipContent.actionLink = (String)attributeMap.get('value');
				    else if(attributeName == 'backgroundImage')
				    	dailyTipContent.backgroundImageUrl = (String)attributeMap.get('value');
				    else if(attributeName == 'buttonCopy')
				    	dailyTipContent.buttonText = (String)attributeMap.get('value');
				    else if(attributeName == 'destinationType')
				    	dailyTipContent.destinationType = (String)attributeMap.get('value');
				    else if(attributeName == 'googleAnalyticsTag')
				    	dailyTipContent.googleAnalyticsTag = (String)attributeMap.get('value');
				    else if(attributeName == 'hasButton')
				    	dailyTipContent.hasButton = (Boolean)attributeMap.get('value');
				    else if(attributeName == 'summary')
				    	dailyTipContent.summary = (String)attributeMap.get('value');
				    else if(attributeName == 'title')
				    	dailyTipContent.title = (String)attributeMap.get('value');
				}

				if(!String.isBlank(dailyTipContent.summary))
					dailyTipContent.summary = dailyTipContent.summary.replaceAll('<[^>]+>',' ').replaceAll('&nbsp;', ' ');
	        }
		}
		catch(Exception ex){
			DailyTipContent dailyTipContentException = new DailyTipContent();
			dailyTipContentException.summary = ex.getMessage();
		}

        return dailyTipContent;
	}

	global class DailyTipContent{
		public string title {get; private set;}
		public string summary {get; private set;}
		public string backgroundImageUrl {get; set;}
		public boolean hasButton {get; private set;}
		public string buttonText {get; private set;}
		public string actionLink {get; private set;}
		public string destinationType {get; private set;}
		public string googleAnalyticsTag {get; private set;}

		public DailyTipContent(){
			this.hasButton = false;
		}
	}

	/*
	/**
     * Retrieves a random tip content for the day
     *
     */
	/*public DailyTipContent getRandomDailyTipContent(){
		HMR_CMS_RenderingApiService renderingApiService = new HMR_CMS_RenderingApiService(this.siteName, this.apiVersion);
		HMR_CMS_ContentApiService contentApiService = new HMR_CMS_ContentApiService(this.siteName, this.apiVersion);
		List<JourneyContent> journeyContentList = new List<JourneyContent>();

		try{
			//Retrieve the rendering content by taxonomy for the day
			JSONMessage.APIResponse response = renderingApiService.getRenderedContentByTaxonomyTag(
	                    new List<String>{'/Tip/Daily'},
	                    new List<List<String>>{new List<String>{'HMR_Journey_ContentTemplate'}},
	                    new List<String>{'HMR_Journey_ContentType'}
	        );

	        //Retrieve the rendered list of content
	        List<Object> renderingList = (List<Object>)(((Map<String, Object>)JSON.deserializeUntyped(response.responseObject)).get('renderings'));
	        //Iterate over the content to retrieve the individual content items
	        if(renderingList.size() > 0){
	        	for(Object renderingObject : renderingList){
	        		Map<String, Object> renderingObjectMap = (Map<String, Object>)renderingObject;
	        		String originId = (String)renderingObjectMap.get('originId');

	        		//Retrieve the content values from Orchestra
	        		JSONMessage.APIResponse contentApiResponse = contentApiService.getContentByObjectId(originId);
	        		Map<String, Object> contentResponseMap = (Map<String, Object>)JSON.deserializeUntyped(contentApiResponse.responseObject);
	        		Map<String, Object> contentAttributeMap = (Map<String, Object>)contentResponseMap.get('contentAttributes');
					//Attributes are returned in a language map to support multiple languages, right now we only use english
					List<Object> englishAttributeList = (List<Object>)contentAttributeMap.get('en_US');
					
					JourneyContent journeyContentItem = new JourneyContent();
					for(object attributeItem : englishAttributeList){
					    Map<String, Object> attributeMap = (Map<String, Object>)attributeItem;
					    String attributeName = (String)attributeMap.get('name');
					    if(attributeName == 'title')
					    	journeyContentItem.title = (String)attributeMap.get('value');
					    else if(attributeName == 'summary')
					    	journeyContentItem.summary = (String)attributeMap.get('value');
					    else if(attributeName == 'backgroundImage')
					    	journeyContentItem.backgroundImageUrl = (String)attributeMap.get('value');
					    else if(attributeName == 'action')
					    	journeyContentItem.actionLink = (String)attributeMap.get('value');
					}

					journeyContentList.add(journeyContentItem);
	        	}
	        }
		}
		catch(Exception ex){
			JourneyContent journeyContentException = new JourneyContent();
			journeyContentException.summary = ex.getMessage();
			journeyContentList.add(journeyContentException);
		}

        return journeyContentList[Math.round(Math.random() * (journeyContentList.size() - 1))];
	}*/
}