/**
* Apex Selector Class for ccrz__E_Stored_Payment__c
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2017-05-22
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-05-22
* @JIRA: 
*/
public class HMR_CC_StoredPayment_Selector {
	public static List<ccrz__E_StoredPayment__c> getByContactId(string contactId){
		List<ccrz__E_StoredPayment__c> storedPaymentList = [SELECT ccrz__AccountNumber__c,
																	ccrz__AccountType__c,
																	ccrz__DisplayName__c,
																	ccrz__EndDate__c,
																	ccrz__ExpMonth__c,
																	ccrz__ExpYear__c,
																	ccrz__PaymentType__c,
																	HMR_Contact_Address__r.ccrz__AddressFirstline__c,
																	HMR_Contact_Address__r.ccrz__AddressSecondline__c,
																	HMR_Contact_Address__r.ccrz__City__c,
																	HMR_Contact_Address__r.ccrz__State__c,
																	HMR_Contact_Address__r.ccrz__StateISOCode__c,
																	HMR_Contact_Address__r.ccrz__PostalCode__c,
																	HMR_Contact_Address__r.ccrz__Country__c,
																	HMR_Contact_Address__r.ccrz__FirstName__c,
																	HMR_Contact_Address__r.ccrz__LastName__c,
																	HMR_Contact_Address__r.ccrz__Email__c,
																	HMR_Contact_Address__r.ccrz__DaytimePhone__c,
																	HMR_Contact_Address__r.ccrz__HomePhone__c
																FROM ccrz__E_StoredPayment__c
																WHERE HMR_Contact_Address__r.Contact_Id__c =: contactId];
		return storedPaymentList;
    }

	public static List<ccrz__E_StoredPayment__c> getByUserId(string userId){
		List<ccrz__E_StoredPayment__c> storedPaymentList = [SELECT ccrz__AccountNumber__c,
																	ccrz__AccountType__c,
																	ccrz__DisplayName__c,
																	ccrz__EndDate__c,
																	ccrz__ExpMonth__c,
																	ccrz__ExpYear__c,
																	ccrz__PaymentType__c,
																	HMR_Contact_Address__r.ccrz__AddressFirstline__c,
																	HMR_Contact_Address__r.ccrz__AddressSecondline__c,
																	HMR_Contact_Address__r.ccrz__City__c,
																	HMR_Contact_Address__r.ccrz__State__c,
																	HMR_Contact_Address__r.ccrz__StateISOCode__c,
																	HMR_Contact_Address__r.ccrz__PostalCode__c,
																	HMR_Contact_Address__r.ccrz__Country__c,
																	HMR_Contact_Address__r.ccrz__FirstName__c,
																	HMR_Contact_Address__r.ccrz__LastName__c,
																	HMR_Contact_Address__r.ccrz__Email__c,
																	HMR_Contact_Address__r.ccrz__DaytimePhone__c,
																	HMR_Contact_Address__r.ccrz__HomePhone__c
																FROM ccrz__E_StoredPayment__c
																WHERE ccrz__User__c =: userId];
        return storedPaymentList;
	}

	public static ccrz__E_StoredPayment__c getByToken(string token){
		List<ccrz__E_StoredPayment__c> storedPaymentList = [SELECT ccrz__AccountNumber__c,
																	ccrz__AccountType__c,
																	ccrz__DisplayName__c,
																	ccrz__EndDate__c,
																	ccrz__ExpMonth__c,
																	ccrz__ExpYear__c,
																	ccrz__PaymentType__c,
																	HMR_Contact_Address__r.ccrz__AddressFirstline__c,
																	HMR_Contact_Address__r.ccrz__AddressSecondline__c,
																	HMR_Contact_Address__r.ccrz__City__c,
																	HMR_Contact_Address__r.ccrz__State__c,
																	HMR_Contact_Address__r.ccrz__StateISOCode__c,
																	HMR_Contact_Address__r.ccrz__PostalCode__c,
																	HMR_Contact_Address__r.ccrz__Country__c,
																	HMR_Contact_Address__r.ccrz__FirstName__c,
																	HMR_Contact_Address__r.ccrz__LastName__c,
																	HMR_Contact_Address__r.ccrz__Email__c,
																	HMR_Contact_Address__r.ccrz__DaytimePhone__c,
																	HMR_Contact_Address__r.ccrz__HomePhone__c
																FROM ccrz__E_StoredPayment__c
																WHERE ccrz__Token__c =: token];
        
		return storedPaymentList.size() > 0 ? storedPaymentList[0] : null;
	}
}