/**
* Apex Service Class for the CMS Rendering Api Service
*
* @Date: 2017-12-28
* @Author: Magnet 360 - Zach Engman
* @Modified: 
* @JIRA: 
*/
public with sharing class HMR_CMS_RenderingApiService extends HMR_CMS_API {
    private RenderingAPIRequest renderingRequest = new RenderingAPIRequest();
    
    public HMR_CMS_RenderingApiService(){}
    public HMR_CMS_RenderingApiService(string siteName, string apiVersion) {
        this.siteName = siteName;
        this.apiVersion = apiVersion;
    }
    
    public HMR_CMS_RenderingApiService(Boolean orderByAlpha) {
        if (orderByAlpha) {
            renderingRequest.parameters.put('order', 'alpha');
        }
    }

    public JSONMessage.APIResponse getRenderedContentByTaxonomyTag(List<String> tagPathList, List<List<String>> taxonomyLayoutList, List<String> contentTypeList){
        JSONMessage.APIResponse apiResponse;

        renderingRequest.parameters.put('renderType', 'taxonomy');
        
        if(!renderingRequest.parameters.containsKey('order'))
            renderingRequest.parameters.put('order', 'publish_date');
        
        renderingRequest.listParameters.put('tagPaths', tagPathList);
        renderingRequest.layoutsForTaxonomy = taxonomyLayoutList;
        renderingRequest.listParameters.put('contentTypes', contentTypeList);
        renderingRequest.requestFlags.put('withContentBundle', true);
        
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('renderingRequest', json.serialize(renderingRequest));
        parameters.put('action', 'getRenderedContent');
        parameters.put('service', 'OrchestraRenderingAPI');
        parameters.put('sname', this.siteName); 
        parameters.put('application', 'runtime');
        parameters.put('apiVersion', this.apiVersion);

        String response;
        try {
            response = cms.ServiceEndpoint.doActionApex(parameters);
            apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);
          
            if (!apiResponse.isSuccess)
                throw new RenderingApiServiceException('Could not retrieve renderings for this node');
            if (apiResponse.type != 'RenderResultBundle')
                throw new RenderingApiServiceException('Unexpected result from Rendering API');
        }
        catch(Exception e) {
            throw new RenderingApiServiceException(e.getMessage());
        }
        return apiResponse;
    }

    public class RenderingApiServiceException extends Exception{}
}