@isTest
/**
* Test class for Batch Job of Create Coaching Sessions Records
* @Date: 2016-11-29
* @Author Aslesha(Mindtree)
* @JIRA:
*/

public with sharing class CoachingSessionsAddMonthBatchJobTest {
static testMethod void TestBatchJob() {

      User sampleUser = new User();
      sampleUser.Username = 'testuser1@gmail.com.hmrtest';
      sampleUser.FirstName='FirstName';
      sampleUser.LastName = 'LastTestName';
      sampleUser.Email = 'testuser1@gmail.com';
      sampleUser.alias = 'testAl';
      sampleUser.TimeZoneSidKey = 'America/New_York';
      sampleUser.LocaleSidKey = 'en_us';
      sampleUser.EmailEncodingKey = 'ISO-8859-1';
      sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
      sampleUser.LanguageLocaleKey = 'en_us';
      sampleUser.IsActive = true;
      sampleUser.hmr_IsCoach__c = true;
      insert sampleUser;

      //Create Test Data for 'Coach__C' object
      Coach__c coachRecord1 = new Coach__c (
                                        hmr_Active__c=True,
                                        hmr_Coach__c= sampleUser.Id,
                                        hmr_Email__c='sampleemail1@gmail.com',
                                        hmr_Class_Strength__c=20
      );
      insert coachRecord1;
    //Create Test Data for 'Class' object
    Class__c classRecord1 = New Class__c (
                                          hmr_Active__c=true,
                                          hmr_Class_Name__c='Session on eCommerce',
                                          hmr_Coach__c=coachRecord1.id,
                                          hmr_Start_Time__c = '11:00 AM',
                                          hmr_End_Time__c ='12:00 PM',
                                          hmr_First_Class_Date__c=date.newInstance(2016, 11, 11),
                                          hmr_Active_Date__c=date.newInstance(2016, 10, 11),
                                          hmr_Class_Type__c='P1 PP',
                                          hmr_Coference_Call_Number__c='1234',
                                          hmr_Time_of_Day__c='Noon',
                                          hmr_Participant_Code__c='Placeholder',
                                          hmr_Host_Code__c='Placeholder'

    );
    insert classRecord1;
    Class__c classRecord2 = New Class__c (
                                          hmr_Active__c=true,
                                          hmr_Class_Name__c='Aslesha session ',
                                          hmr_Coach__c=coachRecord1.id,
                                          hmr_Start_Time__c = '8:00 AM',
                                          hmr_End_Time__c ='9:00 AM',
                                          hmr_First_Class_Date__c=Date.today()+15,
                                          hmr_Active_Date__c=Date.today()+12,
                                          hmr_Class_Type__c='P1 PP',
                                          hmr_Coference_Call_Number__c='1234',
                                          hmr_Time_of_Day__c='Morning',
                                          hmr_Participant_Code__c='Placeholder',
                                          hmr_Host_Code__c='Placeholder'


    );
    insert classRecord2;
        Class__c classRecord3 = New Class__c (
                                          hmr_Active__c=true,
                                          hmr_Class_Name__c='Aslesha session2 ',
                                          hmr_Coach__c=coachRecord1.id,
                                          hmr_Start_Time__c = '1:00 PM',
                                          hmr_End_Time__c ='2:00 PM',
                                          hmr_First_Class_Date__c=Date.today()-150,
                                          hmr_Active_Date__c=Date.today()-150,
                                          hmr_Class_Type__c='P1 PP',
                                          hmr_Coference_Call_Number__c='1234',
                                          hmr_Time_of_Day__c='Noon',
                                          hmr_Participant_Code__c='Placeholder',
                                          hmr_Host_Code__c='Placeholder'


    );
    insert classRecord3;

    Test.startTest();
    CoachingSessionsAdditionalMonthBatchJob b = new CoachingSessionsAdditionalMonthBatchJob();
    database.executebatch(b);
    List <Coaching_Session__c> LstCoachingSession=[Select Name from Coaching_Session__c where hmr_Class__c =:classRecord1.Id ];
    //system.assert(LstCoachingSession.size()>0,'Coaching Sessions created');
    Test.stopTest();



}
static testMethod void TestBatchSchedular() {
	 Test.StartTest();
		 CoachingSessionsBacthJobSchedular sh1 = new CoachingSessionsBacthJobSchedular();
		 String sch = '0 0 10 * * ?';
		 system.schedule('Test check', sch, sh1);
	 Test.stopTest();
}

}