/*
Developer: Ali Pierre (HMR)
Date: 10/18/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_KitConfig_ContentTemplate
*/
@isTest(SeeAllData=true) 
private class HMR_CMS_KitConfig_Content_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_KitConfig_ContentTemplate kitConfigTemplate = new HMR_CMS_KitConfig_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = kitConfigTemplate.kitSKU;                
        Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'SHAKEM',
            'isAdmin' => 'FALSE'
        };

	    string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'SHAKEM');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_ContentTemplate kc = new HMR_CMS_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}

	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_KitConfig_ContentTemplate kitConfigTemplate = new HMR_CMS_KitConfig_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = kitConfigTemplate.kitSKU;                
        Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'HSQSKRVC',
            'isAdmin' => 'FALSE'
        };

	    string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'HSQSKRVC');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_ContentTemplate kc = new HMR_CMS_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}

  	@isTest static void test_method_three() {
	    // Implement test code
	    HMR_CMS_KitConfig_ContentTemplate kitConfigTemplate = new HMR_CMS_KitConfig_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = kitConfigTemplate.kitSKU;                
        Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'HSQSKRVCADMIN',
            'isAdmin' => 'TRUE'
        };

	    string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'HSQSKRVCADMIN');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_ContentTemplate kc = new HMR_CMS_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}

  	@isTest static void test_method_four() {
	    // Implement test code
	    HMR_CMS_KitConfig_ContentTemplate kitConfigTemplate = new HMR_CMS_KitConfig_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = kitConfigTemplate.kitSKU;                
        Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'SHAKEMADMIN',
            'isAdmin' => 'TRUE'
        };

	    string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'SHAKEMADMIN');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_KitConfig_ContentTemplate kc = new HMR_CMS_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}
}