/**
* Public class to support generation of picklist options
* @Date: 2018-02-06
* @Author Utkarsh Goswami (Mindtree)/Zach Engman
* @Modified:
* @JIRA: 
*/
public class HMR_CMS_PicklistOption {
   String Label;
   String Value;

   public HMR_CMS_PicklistOption (String label, String value) {
      this.Label = label;
      this.Value = value;
    } 
}