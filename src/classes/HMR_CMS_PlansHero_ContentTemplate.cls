/**
* Apex Content Template Controller for Hero Sections on Plans Page
*
* @Date: 05/24/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_PlansHero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_PlansHero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_PlansHero_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	public String SectionTitle{
        get{
            return getPropertyWithDefault('SectionTitle', 'Choose a Plan');
        }
    }

	public String SectionSubTitle{
        get{
            return getPropertyWithDefault('SectionSubTitle', 'Something for Everyone');
        }
    }

	public String DesktopOrMobile{
        get{
            return getPropertyWithDefault('DesktopOrMobile', '[]');
        }
    }

	public String ProgramType{
        get{
            return getPropertyWithDefault('ProgramType', '[]');
        }
    }

	public Decimal stdPriceHSAH { get; set; }
	public Decimal stdPriceHSS { get; set; }
	//list of programs
    public List<Program__c> pList { get; set; }
    //if current user is the guest user
    public Boolean isGuestUser { get; set; }
    //if healthy shake is an option
    public Boolean isShakeOption { get; set; }

	public String returnDesktopHTML(Program__c healthySolutionsProgram, Program__c healthyShakesProgram, Decimal stdPriceHSAH, Decimal stdPriceHSS) {
		String html = '';
		html += '<section class="hmr-plans-hero-section bg-typecover bg-planshero visible-lg visible-md">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<h4 class="hmr-allcaps plans-hero-subtitle white">' + SectionSubTitle + '</h4>' +
			            '</div>' +
			            '<div class="row">' +
			                '<h2 class="white hero-title">' + SectionTitle + '</h2>' +
			            '</div>' +
			        '</div>' +
			        '<div class="plans-tabs-row">' +
			            '<div class="col-sm-1"></div>' +
			            '<div class="col-sm-10">';
		String gtm_name = '';String gtm_id = '';
		if(ProgramType == 'HSAH') {
			html += '<div class="plans-tab healthysolutions-tab selected">' +
						'<div>' +
							'<h3 class="hmr-allcaps blue">Healthy Solutions at Home</h3>' +
							'<h3 class="hmr-allcaps book">3-WEEK KIT + FREE BAR VARIETY PACK</h3>' +
							'<h3 class="hmr-allcaps book">' +
								'$<span>' + stdPriceHSAH + '</span> ($<span>' + healthySolutionsProgram.Price_per_Day__c + '</span> per day)' +
							'</h3>' +
						'</div>' +
					'</div>';
			//if(isShakeOption) {
			gtm_name = 'Healthy Shakes Page View';
			gtm_id = 'HealthyShakes';
			html += '<a class="hss_tab hidden" href="healthy-shakes-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">' +
						'<div class="plans-tab healthyshakes-tab">' +
							'<div>' +
								'<h3 class="hmr-allcaps white">Healthy Shakes</h3>' +
								'<h3 class="hmr-allcaps book white">2-WEEK KIT NOW 20% OFF</h3>' +
								'<h3 class="hmr-allcaps book white">' +
									'$<span>' + stdPriceHSS + '</span> ($<span>' + healthyShakesProgram.Price_per_Day__c + '</span> per day)' +
								'</h3>' +
							'</div>' +
						'</div>' +
					'</a>';
			//}
		}
		if(ProgramType == 'HSS') {
			gtm_name = 'Healthy Solutions Page View';
			gtm_id = 'HealthySolutions';
			html += '<a href="healthy-solutions-at-home-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">' +
						'<div class="plans-tab healthysolutions-tab">' +
							'<div>' +
								'<h3 class="hmr-allcaps white">Healthy Solutions at Home</h3>' +
								'<h3 class="hmr-allcaps book white">3-WEEK KIT + FREE BAR VARIETY PACK</h3>' +
								'<h3 class="hmr-allcaps book white">' +
									'$<span>' + stdPriceHSAH + '</span> ($<span>' + healthySolutionsProgram.Price_per_Day__c + '</span> per day)' +
								'</h3>' +
							'</div>' +
						'</div>' +
					'</a>' +
					'<div class="plans-tab healthyshakes-tab selected">' +
						'<div>' +
							'<h3 class="hmr-allcaps blue">Healthy Shakes</h3>' +
							'<h3 class="hmr-allcaps book">2-WEEK KIT NOW 20% OFF</h3>' +
							'<h3 class="hmr-allcaps book">' +
								'$<span>' + stdPriceHSS + '</span> ($<span>' + healthyShakesProgram.Price_per_Day__c + '</span> per day)' +
							'</h3>' +
						'</div>' +
					'</div>';
		}
		html += '</div>' +
			    '<div class="col-sm-1"></div>' +
			    '</div>' +
			    '</section>';

		if(ProgramType == 'HSAH') {
			html += '<section class="plans-mobile-header-section visible-sm visible-xs">' +
			            '<div class="container">' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white no-book">HEALTHY Solutions At Home&reg;</h3>' +
			                    '</div>' +
			                '</div>' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white book">3-WEEK KIT + FREE BAR VARIETY PACK</h3>' +
			                    '</div>' +
			                '</div>' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white book">$<span>' + stdPriceHSAH + '</span> (Save $<span>' + healthySolutionsProgram.Default_Promotional_Discount__c + '</span>)</h3>' +
			                    '</div>' +
			                '</div>' +
			            '</div>' +
				    '</section>';
		}
		if(ProgramType == 'HSS') {
			html += '<section class="plans-mobile-header-section visible-sm visible-xs">' +
			            '<div class="container">' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white no-book">HEALTHY SHAKES&reg;</h3>' +
			                    '</div>' +
			                '</div>' +
			               '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white book">2-WEEK KIT NOW 20% OFF</h3>' +
			                    '</div>' +
			                '</div>' +
			                '<div class="row">' +
			                    '<div class="col-xs-12">' +
			                        '<h3 class="hmr-allcaps white book">$<span>' + stdPriceHSS + '</span> (Save $<span>' + healthyShakesProgram.Default_Promotional_Discount__c + '</span>)</h3>' +
			                    '</div>' +
			                '</div>' +
			            '</div>' +
				    '</section>';
		}
		return html;
	}

	public String returnMobileHTML(Program__c healthySolutionsProgram, Program__c healthyShakesProgram, Decimal stdPriceHSAH, Decimal stdPriceHSS) {
		String html = '';String gtm_name = '';String gtm_id = '';
		gtm_name = 'Healthy Solutions Page View';
		gtm_id = 'HealthySolutions';
		html += '<section class="hmr-plans-hero-section bg-typecover bg-planshero visible-sm visible-xs">' +
			    '</section>' +
			    '<section class="hmr-plans-mobile-section visible-sm visible-xs">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<h4 class="hmr-allcaps plans-hero-subtitle">' + SectionSubTitle + '</h4>' +
			            '</div>' +
			            '<div class="row">' +
			                '<h2 class="hero-title">' + SectionTitle + '</h2>' +
			            '</div>' +
			            '<div class="row healthysolutions-row">' +
			                '<a href="healthy-solutions-at-home-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">' +
			                    '<h3 class="hmr-allcaps blue">Healthy Solutions at Home</h3>' +
			                    '<h3 class="hmr-allcaps book dark-blue">3-WEEK KIT + FREE BAR VARIETY PACK</h3>' +
			                    '<h3 class="hmr-allcaps book dark-blue">' +
			                        '$<span>' + stdPriceHSAH + '</span> ($<span>' + healthySolutionsProgram.Price_per_Day__c + '</span> per day)' +
			                    '</h3>' +
			                '</a>' +
			            '</div>';
		//if(isShakeOption) {
		gtm_name = 'Healthy Shakes Page View';
		gtm_id = 'HealthyShakes';
		html += '<div class="row healthyshakes-row hidden">' +
					'<a href="healthy-shakes-diet-plan" onclick="javascript:gtm_detailPushed(\''+ gtm_name +'\', \''+ gtm_id +'\');">' +
						'<h3 class="hmr-allcaps blue">Healthy Shakes</h3>' +
						'<h3 class="hmr-allcaps book dark-blue">2-WEEK KIT NOW 20% OFF</h3>' +
						'<h3 class="hmr-allcaps book dark-blue">' +
							'$<span>' + stdPriceHSS + '</span> ($<span>' + healthyShakesProgram.Price_per_Day__c + '</span> per day)' +
						'</h3>' +
					'</a>' +
				'</div>';
		//}
		html += '</div></section>';
		return html;
	}

	global virtual override String getHTML(){
		String html = '';stdPriceHSAH = 0; stdPriceHSS = 0;isShakeOption = true;
		try {
			//current user Id
	        String userId = UserInfo.getUserId();
	        //current user profile Id
	        String userProfileId = UserInfo.getProfileId();
	        //check if current user is the site guest user
	        Profile guestProfile = [SELECT Id, Name FROM Profile WHERE Id =: userProfileId];
	        isGuestUser = (guestProfile.Name == 'HMR Program Community Profile');
			//retrieve all programs and preset the value for guest user
	        pList = new List<Program__c>();
	        pList = [SELECT Id, Name, Standard_Kit__r.ccrz__SKU__c, Default_Promotional_Discount__c, hmr_Program_Type__c,
	                 	hmr_Standard_Kit_Price__c, Price_per_Day__c FROM Program__c WHERE hmr_Phase_Type__c = 'P1'];
			Program__c healthySolutionsProgram;
			Program__c healthyShakesProgram;
			for(Program__c program: pList) {
				if(program.Name.contains('Solutions')) {
					healthySolutionsProgram = program;
					if(program.Default_Promotional_Discount__c != null) {
						stdPriceHSAH = program.hmr_Standard_Kit_Price__c - program.Default_Promotional_Discount__c;
					}
					else {
						stdPriceHSAH = program.hmr_Standard_Kit_Price__c;
					}
				}
				if(program.Name.contains('Shakes')) {
					healthyShakesProgram = program;
					if(program.Default_Promotional_Discount__c != null) {
						stdPriceHSS = program.hmr_Standard_Kit_Price__c - program.Default_Promotional_Discount__c;
					}
					else {
						stdPriceHSS = program.hmr_Standard_Kit_Price__c;
					}
				}
			}
			//if current user is community user but not site user
	        if(!isGuestUser){
				User curUser = [Select Id, ContactId from User where Id =: UserInfo.getUserId()];
	            if(curUser.ContactId != null){
	                Id cId = curUser.ContactId;
	                Contact con = [SELECT Account.RecordType.DeveloperName,
							              Account.hmr_Is_Healthy_Shakes_an_Option__c,
							              Account.Standard_HSAH_Kit_Discount__c,
							              Account.Standard_HSS_Kit_Discount__c,
							              Account.hmr_HSAH_Standard_Kit_Price__c,
							              Account.hmr_HSS_Standard_Kit_Price__c FROM Contact WHERE Id = :cId];//Corporate_Account

	                //if cooperate account then use the value of hmr_Is_Healthy_Shakes_an_Option__c
	                if(con.Account.RecordType.DeveloperName == 'Corporate_Account'){
	                    isShakeOption = con.Account.hmr_Is_Healthy_Shakes_an_Option__c;
	                }
	                //get the price from
	                if(ProgramType == 'HSAH'){
	                    if(con.Account.hmr_HSAH_Standard_Kit_Price__c != null) {
							if(con.Account.Standard_HSAH_Kit_Discount__c != null) {
								stdPriceHSAH = con.Account.hmr_HSAH_Standard_Kit_Price__c - con.Account.Standard_HSAH_Kit_Discount__c;
							}
							else {
								stdPriceHSAH = con.Account.hmr_HSAH_Standard_Kit_Price__c;
							}
						}
	                }
	                if(ProgramType == 'HSS'){
	                    if(con.Account.hmr_HSS_Standard_Kit_Price__c != null) {
							if(con.Account.Standard_HSS_Kit_Discount__c != null && con.Account.Standard_HSS_Kit_Discount__c != 0) {
								stdPriceHSS = con.Account.hmr_HSS_Standard_Kit_Price__c - con.Account.Standard_HSS_Kit_Discount__c;
							}
							else {
								stdPriceHSS = con.Account.hmr_HSS_Standard_Kit_Price__c;
							}
						}
	                }
	            }
			}
			if(DesktopOrMobile == 'Desktop') {
				html = returnDesktopHTML(healthySolutionsProgram, healthyShakesProgram, stdPriceHSAH, stdPriceHSS);
			}
			else {
				html = returnMobileHTML(healthySolutionsProgram, healthyShakesProgram, stdPriceHSAH, stdPriceHSS);
			}
		}
		catch(Exception ex) {
			System.debug('Exception in Plans Content Template');
			System.debug(ex);
			html += ex.getMessage();
		}
		return html;
	}
}