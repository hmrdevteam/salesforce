/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_LoginServiceInterface_Test
*/

@isTest
private class HMR_CMS_LoginServiceInterface_Test{
  
  @isTest static void firstMethod() {
    // Implement test code
    HMR_CMS_LoginServiceInterface loginServiceController = new HMR_CMS_LoginServiceInterface();
    
    Map<String, String> params = new Map<String, String>();
    params.put('u','testuser@gml.com');
    params.put('p','testuserP!2');
    params.put('r','/testHomePage');
    
    String resp = loginServiceController.executeRequest(params);
    
    HMR_CMS_LoginServiceInterface.getType();
    
   // System.assertEquals('{"failure":"login failed","success":false}', resp);
    System.assert(!String.isBlank(resp));
    
  }
  
  @isTest static void secondMethod() { 
      
        HMR_CMS_LoginServiceInterface loginServiceController = new HMR_CMS_LoginServiceInterface();
    
        Map<String, String> params;
        String resp = loginServiceController.executeRequest(params); 
        
        System.assert(resp.contains('false'));  
      
  }
}