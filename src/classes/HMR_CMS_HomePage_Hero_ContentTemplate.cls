/**
* Apex Content Template Controller for Hero Section on Home Page
*
* @Date: 2017-03-206
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA:
*/
global virtual class HMR_CMS_HomePage_Hero_ContentTemplate extends cms.ContentTemplateController {

	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_HomePage_Hero_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_HomePage_Hero_ContentTemplate() {
        super();
    }

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	//Title Label Property
    public String TitleText{
        get{
            return getPropertyWithDefault('TitleText', 'Lifestyle Change You Can Live With');
		}
    }

	//SubTitle Label Property
    public String SubTitleText{
        get{
            return getPropertyWithDefault('SubTitleText', 'LOSING WEIGHT');
        }
    }

	//Learn More Button Label Property
    public String ButtonLabelText{
    	get{
            return getPropertyWithDefault('ButtonLabelText', 'Learn More');
        }
	}

	//Learn More Button Page Link Property
    public cms.Link ButtonLinkObj{
        get{
            return this.getPropertyLink('ButtonLinkObj');
        }
    }

	//Other Link (Already an HMR Member) Label Property
    public String OtherLinkLabelText{
    	get{
            return getPropertyWithDefault('OtherLinkLabelText', 'Already an HMR Member');
        }
    }

	//Learn More button click Google Tag manager Data ID
    public String HeroCTAButtonLinkGTMID{
    	get{
            return getPropertyWithDefault('HeroCTAButtonLinkGTMID', 'heroCTA');
        }
    }

	//Other link click Google Tag manager Data ID
    public String HeroExistingMemberOtherLinkGTMID{
    	get{
            return getPropertyWithDefault('HeroExistingMemberOtherLinkGTMID', 'heroExisting');
        }
    }

	//Other Link (Already an HMR Member) Page Link Property
    public cms.Link OtherLinkLinkObj{
        get{
            return this.getPropertyLink('OtherLinkLinkObj');
        }
    }

	//show US News Best Diets Fast Weight Loss Icon
	public Boolean showBestDietsFastWeightLoss {
        get {
            return getAttribute('showBestDietsFastWeightLoss') == 'true';
        }
    }

	//show US News Best Diets Weight Loss Icon
	public Boolean showBestDietsWeightLoss {
        get {
            return getAttribute('showBestDietsWeightLoss') == 'true';
        }
    }

	//global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup
		String html = '', buttonLinkUrl = '', otherLinkLinkUrl = '', sectionHeroCTAButtonLinkGTMIDAttr = ' ', sectionHeroExistingMemberOtherLinkGTMIDAttr = ' ';
		//check cms.Link properties for null and assign values to above var's
        buttonLinkUrl = (this.ButtonLinkObj != null) ? this.ButtonLinkObj.targetPage : '#';
        otherLinkLinkUrl = (this.OtherLinkLinkObj != null) ? this.OtherLinkLinkObj.targetPage : '#';
		if(HeroCTAButtonLinkGTMID != null && HeroCTAButtonLinkGTMID != '') {
			sectionHeroCTAButtonLinkGTMIDAttr = ' data-gtm-id="' + HeroCTAButtonLinkGTMID + '" ';
		}
		if(HeroExistingMemberOtherLinkGTMID != null && HeroExistingMemberOtherLinkGTMID != '') {
			sectionHeroExistingMemberOtherLinkGTMIDAttr = ' data-gtm-id="' + HeroExistingMemberOtherLinkGTMID + '" ';
		}
		html += '<section class="hmr-hero-section bg-typecover bg-homehero">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<h4 class="hmr-allcaps hero-subtitle home-hero-subtitle">' + SubTitleText + '</h4>' +
			            '</div>' +
			            '<div class="row">' +
			                '<h2 class="home-hero-title hero-title">' + TitleText + '</h2>' +
			            '</div>' +
			            '<div class="row text-center no-padding">' +
			                '<div class="btn-container">' +
			                    '<a' + sectionHeroCTAButtonLinkGTMIDAttr + 'href="' + buttonLinkUrl + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">' + ButtonLabelText + '</a>' +
			                '</div>' +
			            '</div>' +
			            '<div class="row no-padding">' +
			                '<div class="link-container text-center">' +
			                    '<div class="hmr-allcaps-container">' +
			                        '<a' + sectionHeroExistingMemberOtherLinkGTMIDAttr + 'href="' + otherLinkLinkUrl + '" class="hmr-allcaps home-hero-link">' + OtherLinkLabelText + '</a>' +
			                        '<a' + sectionHeroExistingMemberOtherLinkGTMIDAttr + 'href="' + otherLinkLinkUrl + '" class="hmr-allcaps hmr-allcaps-carat home-hero-link-carret">></a>' +
			                    '</div>' +
			                '</div>' +
			            '</div>';
		if(showBestDietsFastWeightLoss || showBestDietsWeightLoss) {
			html += '<div class="row best-diets-container-parent">';
		}
		if(showBestDietsFastWeightLoss) {
			html += '<div class="best-diets-container">' +
						'<a href="http://health.usnews.com/best-diet/hmr-diet" target="_blank">' +
							'<img src="https://www.myhmrprogram.com/ContentMedia/HomeDesktop/BestDietsFastWL2018.png" />' +
						'</a>' +
					'</div>';
		}
		if(showBestDietsWeightLoss) {
			html += '<div class="best-diets-container">' +
						'<a href="http://health.usnews.com/best-diet/hmr-diet" target="_blank">' +
							'<img src="https://www.myhmrprogram.com/ContentMedia/HomeDesktop/BestDietsWL.png" />' +
						'</a>' +
					'</div>';
		}
		if(showBestDietsFastWeightLoss || showBestDietsWeightLoss) {
			html += '</div>';
		}
		html += '</div>' + '</section>';
		return html;
	}
}