/**
* @Date: 4/7/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
* This test class covers HMR_CC_CyberSourceUtil Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CC_Cybersource_RequestForm_Ctrl_Test{

  @isTest static void test_method_one() {
   
   
   HMR_CC_Cybersource_RequestForm_Ctrl  cybrsorceReq = new HMR_CC_Cybersource_RequestForm_Ctrl();
            
        Test.setCurrentPageReference(new PageReference('Page.HMR_CC_Cybersource_RequestForm')); 
        
        System.currentPageReference().getParameters().put('formAction','testaction');
        System.currentPageReference().getParameters().put('selector', 'testselector');
        System.currentPageReference().getParameters().put('signed','testsigned');
        System.currentPageReference().getParameters().put('unsigned', 'testunsigned');
        
        List<String> signStr = new List<String>();
        List<String> unSignStr = new List<String>(); 
        
        cybrsorceReq.signedFields = signStr;
        cybrsorceReq.unsignedFields = unSignStr;
        cybrsorceReq.signed = 'sign';
        cybrsorceReq.unsigned = 'unsign';
        cybrsorceReq.formAction = 'frmaction';
        cybrsorceReq.selector = 'selector';
        cybrsorceReq.postback = 'postback';
                  

    }  

}