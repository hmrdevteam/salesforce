@isTest
private class HMR_CMS_AddressService_Test
{
	
	private enum AddressType { Shipping, Billing }
	
	@isTest
	private static void testGetAddressById() {
		Test.startTest();
		Map<String, Object> response = HMR_CMS_AddressService.getAddressesByIds(new Set<Id> {
			cc_dataFactory.billToAddress.Id
		});
		Test.stopTest();
		
		System.debug('*** response: ' + JSON.serializePretty(response));

		List<Map<String, Object>> addressList = (List<Map<String, Object>>) response.get('addressList');
		System.assertEquals(1, addressList.size(), 'Expected exactly one address to be returned');
	}
	
	@isTest
	private static void testCreateAddressWithAdressBookEntries() {
		HMR_CMS_AddressService.AddressRecord aRecord = new HMR_CMS_AddressService.AddressRecord(
			new Map<String, Object> {
					'addressFirstline' => '123 Test St',
					'city' => 'Test Town',
					'country' => 'United Tests of Testopia'
		});
		aRecord.isShippingAddress = true;
		aRecord.isBillingAddress = true;
		
		Test.startTest();
		HMR_CMS_AddressService.createAddressWithAdressBookEntries(UserInfo.getUserId(), aRecord);
		Test.stopTest();
	}
	
	@isTest
	private static void testUpdateAddressBook_removeBilling() {
		ccrz__E_ContactAddr__c address_sobj = cc_dataFactory.billToAddress;
		HMR_CMS_AddressService.AddressRecord aRecord = new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
			'sfid' => address_sobj.Id,
			'ownerId' => address_sobj.ownerId,
			'firstName' => address_sobj.ccrz__FirstName__c,
			'lastName' => address_sobj.ccrz__lastName__c,
			'addressFirstline' => address_sobj.ccrz__AddressFirstline__c,
			'city' => address_sobj.ccrz__City__c,
			'stateISOCode' => address_sobj.ccrz__StateISOCode__c,
			'postalCode' => address_sobj.ccrz__PostalCode__c
		});
		
		aRecord.billingAddressBookEntry = cc_dataFactory.defaultBillingAdressBook.Id;
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 1,'Billing');
		
		Test.startTest();
		HMR_CMS_AddressService.updateAddressBook(aRecord.ownerId, aRecord);
		Test.stopTest();
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 0,'Billing');
	}
	
	@isTest
	private static void testUpdateAddressBook_removeShipping() {
		ccrz__E_ContactAddr__c address_sobj = cc_dataFactory.shipToAddress;
		HMR_CMS_AddressService.AddressRecord aRecord = new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
			'sfid' => address_sobj.Id,
			'ownerId' => address_sobj.ownerId,
			'firstName' => address_sobj.ccrz__FirstName__c,
			'lastName' => address_sobj.ccrz__lastName__c,
			'addressFirstline' => address_sobj.ccrz__AddressFirstline__c,
			'city' => address_sobj.ccrz__City__c,
			'stateISOCode' => address_sobj.ccrz__StateISOCode__c,
			'postalCode' => address_sobj.ccrz__PostalCode__c
		});
		
		aRecord.ShippingAddressBookEntry = cc_dataFactory.defaultShippingAdressBook.Id;
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 1, 'Shipping');
		
		Test.startTest();
		HMR_CMS_AddressService.updateAddressBook(aRecord.ownerId, aRecord);
		Test.stopTest();
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 0, 'Shipping');
	}
	
	@isTest
	private static void testUpdateAddressBook_removeDefaultOnShippingAddress() {
		testUpdateAddressBook_removeDefaultOnAddress(AddressType.Shipping);
	}
	
	@isTest
	private static void testUpdateAddressBook_removeDefaultOnBillingAddress() {
		testUpdateAddressBook_removeDefaultOnAddress(AddressType.Billing);
	}
	
	@isTest
	private static void testUpdateAddressBook_addShippingAddress() {
		testUpdateAddressBook_addAddress(AddressType.Shipping);
	}
	
	@isTest
	private static void testUpdateAddressBook_addBillingAddress() {
		testUpdateAddressBook_addAddress(AddressType.Billing);
	}
	
	private static void testUpdateAddressBook_addAddress(AddressType aType) {
		HMR_CMS_AddressService.AddressRecord aRecord = getAddressRecord(aType);
		configureAddressToBeAdded(aRecord, aType);
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 0, aType.name());
		
		Test.startTest();
		HMR_CMS_AddressService.updateAddressBook(aRecord.ownerId, aRecord);
		Test.stopTest();
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 1, aType.name());
	}
	
	private static void configureAddressToBeAdded(HMR_CMS_AddressService.AddressRecord aRecord, AddressType aType) {
		if(aType == AddressType.Shipping) {
			aRecord.isShippingAddress = true;
		} else if (aType == AddressType.Billing) {
			aRecord.isBillingAddress = true;
		}
	}
	
	private static void testUpdateAddressBook_removeDefaultOnAddress(AddressType aType) {
		//ccrz__E_ContactAddr__c address_sobj = cc_dataFactory.shipToAddress;
		HMR_CMS_AddressService.AddressRecord aRecord = getAddressRecord(aType);
		
		configureAddressToRemoveAsDefault(aRecord, aType);
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 1, aType.name());
		
		Test.startTest();
		HMR_CMS_AddressService.updateAddressBook(aRecord.ownerId, aRecord);
		Test.stopTest();
		
		assertNumberOfAddressesForOwner(aRecord.ownerId, 1, aType.name());
	}
	
	private static HMR_CMS_AddressService.AddressRecord getAddressRecord(AddressType aType) {
		ccrz__E_ContactAddr__c address_sobj;
		if(aType == AddressType.Shipping) {
			address_sobj = cc_dataFactory.shipToAddress;
		} else if (aType == AddressType.Billing) {
			address_sobj = cc_dataFactory.billToAddress;
		}
		
		return new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
			'sfid' => address_sobj.Id,
			'ownerId' => address_sobj.ownerId,
			'firstName' => address_sobj.ccrz__FirstName__c,
			'lastName' => address_sobj.ccrz__lastName__c,
			'addressFirstline' => address_sobj.ccrz__AddressFirstline__c,
			'city' => address_sobj.ccrz__City__c,
			'stateISOCode' => address_sobj.ccrz__StateISOCode__c,
			'postalCode' => address_sobj.ccrz__PostalCode__c
		});
	}
	
	private static void configureAddressToRemoveAsDefault(HMR_CMS_AddressService.AddressRecord aRecord, AddressType aType) {
		if(aType == AddressType.Shipping) {
			aRecord.ShippingAddressBookEntry = cc_dataFactory.defaultShippingAdressBook.Id;
			aRecord.isShippingAddress = true;
			aRecord.isDefaultShippingAddress = false;
		} else if (aType == AddressType.Billing) {
			aRecord.BillingAddressBookEntry = cc_dataFactory.defaultBillingAdressBook.Id;
			aRecord.isBillingAddress = true;
			aRecord.isDefaultBillingAddress = false;
		}
	}
	
	private static void assertNumberOfAddressesForOwner(Id ownerId, Integer numberOfAddresses, String addressTypeFilter) {
		Map<String, Object> response = HMR_CMS_AddressService.getAddressBookByOwnerId(ownerId, addressTypeFilter);
		
		List<Map<String, Object>> addressList = new List<Map<String, Object>>((List<Map<String, Object>>) response.get('addressList'));
		System.assertEquals(numberOfAddresses, addressList.size(), 'Expected exactly ' + numberOfAddresses + ' address(es) to be returned');
	}
	
	@isTest
	private static void testCreateAddress() {
		Test.startTest();
		Map<String, Object> response = HMR_CMS_AddressService.createAddress(new Map<String, Object> {
				'addressFirstline' => '123 Test St',
				'city' => 'Test Town',
				'country' => 'United Tests of Testopia'
		});
		Test.stopTest();
		
		System.debug('*** response: ' + JSON.serializePretty(response));

		List<Map<String, Object>> addressList = (List<Map<String, Object>>) response.get('addressList');
		System.assertEquals(1, addressList.size(), 'Expected exactly one address to be returned');
	}

	@isTest
	private static void testGetAddressBookByOwnerId()
	{
		// This debug statement is necessary in order to lazy instantiate the data from the cc_dataFactory
		System.debug(cc_dataFactory.defaultBillingAdressBook);

		Test.startTest();
		//Map<String, Object> response = HMR_CMS_AddressService.fetchAddressesForAccount(cc_dataFactory.testAccount.Id, 'Billing', true);
		Map<String, Object> response = HMR_CMS_AddressService.getAddressBookByOwnerId(cc_dataFactory.defaultBillingAdressBook.ownerId, 'Billing');
		Test.stopTest();

		System.assertNotEquals(null, response, 'response should not be null');

		System.debug('*** response: ' + JSON.serializePretty(response));

		List<Map<String, Object>> addressList = (List<Map<String, Object>>) response.get('addressList');
		System.assertEquals(1, addressList.size(), 'Expected exactly one address to be returned');

		List<Map<String, Object>> addressBookList = (List<Map<String, Object>>) response.get('addressBookList');
		System.assertEquals(1, addressBookList.size(), 'Expected exactly one address book to be returned');
	}


	/*
	@isTest
	private static void testAddressCreation() {
		Test.startTest();
		Map<String, Object> response = HMR_CMS_AddressService.createAddressForAccount(
			cc_dataFactory.testAccount.Id,
			'Shipping',
			true,
			new Map<String, Object> {
				'addressFirstline' => '123 Test St',
				'city' => 'Test Town',
				'country' => 'United Tests of Testopia'
			}
		);
		Test.stopTest();

		List<Map<String, Object>> addressList = (List<Map<String, Object>>) response.get('addressList');
		System.assertEquals(1, addressList.size(), 'Expected exactly one address to be returned');

		List<Map<String, Object>> addressBookList = (List<Map<String, Object>>) response.get('addressBookList');
		System.assertEquals(1, addressBookList.size(), 'Expected exactly one address book to be returned');
	}
	*/
}