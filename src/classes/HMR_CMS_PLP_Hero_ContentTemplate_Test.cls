/*
Developer: Mustafa Ahmed (HMR)
Date: 10/19/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_PLP_Hero_ContentTemplate
*/
@isTest(SeeAllData=true) 
public class HMR_CMS_PLP_Hero_ContentTemplate_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_PLP_Hero_ContentTemplate productsTemplate = new HMR_CMS_PLP_Hero_ContentTemplate();
	    String propertyValue = productsTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String PLPHeroTitleText = productsTemplate.PLPHeroTitleText;
	    String PLPHeroSubTitleText = productsTemplate.PLPHeroSubTitleText;
	    String PLPHeroDescriptionText = productsTemplate.PLPHeroDescriptionText;
	    String PLPHeroPromoText = productsTemplate.PLPHeroPromoText;
	    Boolean is_eCommerce = productsTemplate.is_eCommerce;

	    productsTemplate.testAttributes = new Map<String, String>  {
            'PLPHeroTitleText' => 'PLP Hero Title',
            'PLPHeroSubTitleText' => 'PLP Hero SubTitle',
            'PLPHeroDescriptionText' => 'PLP Hero Description',
            'PLPHeroPromoText' => 'PLP Hero Promo',
            'is_eCommerce' => 'True'
        };

	    string s1 = productsTemplate.getPropertyWithDefault('PLPHeroTitleText','testValue1');
	    system.assertEquals(s1, 'PLP Hero Title');

	    String renderHTML = productsTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_PLP_Hero_ContentTemplate kc = new HMR_CMS_PLP_Hero_ContentTemplate(null);
	    }catch(Exception e){}
  	}
}