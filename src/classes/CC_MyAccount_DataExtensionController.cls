global with sharing class CC_MyAccount_DataExtensionController {
  @RemoteAction
    global static ccrz.cc_RemoteActionResult getExtendedDataForMyAccount(final ccrz.cc_RemoteActionContext ctx, Id contactId){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            Map<String, sObject> dataMap = new Map<String, sObject>();

            Map<Id, Program_Membership__c> contactProgMemMap = new Map<Id, Program_Membership__c>(
                [SELECT Id, Name, hmr_Status__c, hmr_Contact__c, hmr_Membership_Type__c, Program__r.hmr_Program_Display_Name__c FROM Program_Membership__c
                   WHERE hmr_Contact__c = :contactId AND hmr_Status__c = 'Active']);

            Map<Id, Contact> contactInfoMap = new Map<Id, Contact>(
                [SELECT Id, hmr_Date_Of_Birth__c, hmr_Gender__c, hmr_Height_Feet__c, hmr_Height_Inches__c, hmr_Weight_Loss_Goal__c, hmr_Start_Weight__c FROM Contact WHERE Id = :contactId]);

            for(Program_Membership__c contactProgMem: contactProgMemMap.values()){
                dataMap.put(String.valueOf(contactProgMem.hmr_Contact__c), contactProgMem);
            }

            for(Contact contactInfo: contactInfoMap.values()){
                dataMap.put(String.valueOf(contactInfo.Id) + '_contact', contactInfo);
            }

            res.success = true;
            res.data = dataMap;
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getExtendedDataForMyAccount ' + res + ' ' + err.getMessage());
        }
        res.success = true;
        return null;
    }

}