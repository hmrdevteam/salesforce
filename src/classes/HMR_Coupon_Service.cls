/**
* Apex Service Class for the Coupons
*
* @Date: 2017-10-27
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-10-27
* @JIRA: 
*/
public with sharing class HMR_Coupon_Service {
	public HMR_Coupon_Service() {}

	/**
	* Fetches coupon data from cloudcraze using the cart id.
	* @param cartId the salesforce id of the cart to return the coupon data for
	* @return the coupon record
	*/
	public CouponRecord getByCartId(string cartId){
		Map<String, Object> inputDataMap = new Map<String, Object>{
			ccrz.ccApi.API_Version => 6
		   ,ccrz.ccApiCoupon.CARTIDS => new Set<String>{cartId}
		};

		return getCoupon(inputDataMap);
	}

	/**
	* Fetches coupon data from cloudcraze
	* @param inputDataMap the cloud craze formatted request parameters
	* @return the coupon record
	*/
	private CouponRecord getCoupon(Map<String, Object> inputDataMap){
		CouponRecord couponRecord;

		try{
			Map<String, Object> outputDataMap = ccrz.ccApiCoupon.fetch(inputDataMap);
			List<Map<String, Object>> outputCouponList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiCoupon.COUPONLIST);
			
			if(outputCouponList != null && outputCouponList.size() > 0)
				couponRecord = new CouponRecord(outputCouponList[0]);
		}
		catch(Exception ex){
			System.Debug(ex);
		}

		return couponRecord;
	}

	/**
	* Fetches cc coupon data from cloudcraze for the hmr coupon code
	* @param userId the user id of the active cart user
	* @param cartRecord the formatted cart data from the HMR_Cart_Service
	* @param couponCode the related hmr coupon code being applied
	* @return the cc coupon record (ccrz__E_Coupon__c)
	*/
	public ccrz__E_Coupon__c getCouponFromHMRCouponCode(string userId, HMR_Cart_Service.CartRecord cartRecord, string couponCode){
		HMR_User_Selector userSelector = new HMR_User_Selector();
		User userRecord = userSelector.getById(userId);

		return getCouponFromHMRCouponCode(userRecord, cartRecord, couponCode);
	}

	/**
	* Fetches cc coupon data from cloudcraze for the hmr coupon code
	* @param userId the user id of the active cart user
	* @param cartRecord the formatted cart data from the HMR_Cart_Service
	* @param couponCode the related hmr coupon code being applied
	* @return the cc coupon record (ccrz__E_Coupon__c)
	*/
	public ccrz__E_Coupon__c getCouponFromHMRCouponCode(User userRecord, HMR_Cart_Service.CartRecord cartRecord, string couponCode){
		ccrz__E_Coupon__c couponRecord;
		boolean isHMREmployeeAccount = userRecord.Contact.Account.Name == 'HMR Employees'; 
        HMR_Coupon__c hmrCoupon;
        Set<Id> cartProductIdSet = new Set<Id>();
        String contactId = userRecord.ContactId;

        //Get set of product ids
        for(HMR_Cart_Service.CartItem cartItem : cartRecord.cartItemList)
            cartProductIdSet.add(cartItem.productId);
        //Retrieve HMR coupon 
        List<HMR_Coupon__c> hmrCouponList = [SELECT Name 
        									 FROM HMR_Coupon__c 
        									 WHERE Name =: couponCode 
        									 	AND hmr_Active__c = True 
        									 	AND Name NOT IN ('ProgramDiscount', 'ProgramMember') 
        									 LIMIT 1];

        if(!hmrCouponList.isEmpty()) {
             hmrCoupon = hmrCouponList[0];

             //Retrieve Standard Interim order coupon
            List<HMR_Coupon__c> interimHMRCouponList = [SELECT Name 
            										 	FROM HMR_Coupon__c 
            										 	WHERE Name = 'ProgramMember' 
            										 		AND hmr_Active__c = True 
            										 	LIMIT 1];
            HMR_Coupon__c interimHMRCoupon;
            if(!interimHMRCouponList.isEmpty()){
                interimHMRCoupon = interimHMRCouponList[0];
            }

            //Create Map to store cart contents
            Map<Id, ccrz__E_Product__c> cc_CartProducts = new Map<Id, ccrz__E_Product__c>(
                [SELECT Name, 
                		Program__c 
                 FROM ccrz__E_Product__c 
                 WHERE Id IN: cartProductIdSet]
            );

            //variable to store program id associated to a kit
            Id programId;
            //coupon order type
            String orderType;
            //coupon Interim order type
            String interimOrderType;
            Boolean isProgramMember = false;
            String programPhase;

            //If contactId parameter is filled check to see if client has an existing program membership
            if(!String.isBlank(contactId)){
                List<Program_Membership__c> programMembershipList = new List<Program_Membership__c>();
                programMembershipList = [SELECT Name, 
                						 	    hmr_Contact__c, 
                						 		Program__r.Name  
                				   FROM Program_Membership__c
                                   WHERE hmr_Status__c = 'Active' 
                                   		AND hmr_First_Order_Number__c <> null 
                                   		AND hmr_Contact__c = :contactId 
                                   LIMIT 1];
                if(!programMembershipList.isEmpty()){
                    isProgramMember = true;
                    //Get Name of program phase
                    programPhase = programMembershipList[0].Program__r.Name;
                }
            }

            //Set appropriate interim oder type based on Phase
            if(programPhase != null){
                if(programPhase == 'P1 Healthy Solutions'){
                    interimOrderType = 'P1 Interim';
                }
                else if(programPhase == 'P1 Healthy Shakes'){
                     interimOrderType = 'HSS Interim';
                }
                else if(programPhase == 'Phase 2'){
                    interimOrderType = 'P2 Interim';
                }
                else{
                    interimHMRCoupon = null;
                }
            }

            //Loop through cart items to determine if the cart contains a dynamic kit associated to a program
            for(ccrz__E_Product__c cartProduct : cc_CartProducts.values()) {
                if(!String.isBlank(cartProduct.Program__c)) {
                    programId = cartProduct.Program__c;
                    break;
                }
            }
            //If an associated program is found, set order type of coupon depending on the program phase
            if(!String.isBlank(programId)){
                List<Program__c> kitProgramList = new List<Program__c>([SELECT Name 
                													    FROM Program__c
                    												    WHERE Id = :programId]);
                if(!kitProgramList.isEmpty()){
                    if(kitProgramList[0].Name == 'P1 Healthy Shakes'){
                        orderType = 'HSS 1st Order';
                    }
                    else if(kitProgramList[0].Name == 'P1 Healthy Solutions'){
                        orderType = 'P1 1st Order';
                    }
                    else if(kitProgramList[0].Name == 'Phase 2'){
                        orderType = 'P2';
                    }
                }
            }

            //Query the associated CC Coupon to be applied to this order
            //if cart contains a kit and this client is not already a Program Member, apply the appropriate 1st order type coupon
            List<ccrz__E_Coupon__c> ccCouponList;
            if(orderType != null && isProgramMember == false) {
                ccCouponList = new List<ccrz__E_Coupon__c>(
                    [SELECT ccrz__CouponName__c, 
                    		ccrz__CouponCode__c, 
                    		HMR_Coupon__c, 
                    		hmr_Program__c 
                      FROM ccrz__E_Coupon__c
                      WHERE HMR_Coupon__c =: hmrCoupon.Id 
                      	AND ccrz__Enabled__c = True 
                      	AND hmr_Program__c = :programId 
                      	AND Coupon_Order_Type__c = :orderType]);
            }
            //if cart does not contain a kit, coupon order type will be Interim if client is already a program member and A La Carte if they are not
            else {
                if(isProgramMember == true && interimOrderType != null){
                    ccCouponList = new List<ccrz__E_Coupon__c>(
                        [SELECT ccrz__CouponName__c, 
                        		ccrz__CouponCode__c, 
                        		HMR_Coupon__c 
                          FROM ccrz__E_Coupon__c
                          WHERE HMR_Coupon__c =: hmrCoupon.Id 
                            	AND ccrz__Enabled__c = True 
                            	AND Coupon_Order_Type__c =: interimOrderType]);

                    if(ccCouponList.isEmpty()  && interimHMRCoupon != null ){
                        ccCouponList = new List<ccrz__E_Coupon__c>(
                            [SELECT ccrz__CouponName__c, 
                            		ccrz__CouponCode__c, 
                            		HMR_Coupon__c 
                              FROM ccrz__E_Coupon__c
                              WHERE HMR_Coupon__c = :interimHMRCoupon.Id 
                              	AND ccrz__Enabled__c = True 
                              	AND Coupon_Order_Type__c = :interimOrderType]);
                    }
                }
                else if(isProgramMember == true && interimHMRCoupon != null && interimOrderType != null){
                    ccCouponList = new List<ccrz__E_Coupon__c>(
                        [SELECT ccrz__CouponName__c, 
                        		ccrz__CouponCode__c, 
                        		HMR_Coupon__c 
                          FROM ccrz__E_Coupon__c
                          WHERE HMR_Coupon__c = :interimHMRCoupon.Id 
                          	AND ccrz__Enabled__c = True 
                          	AND Coupon_Order_Type__c = :interimOrderType]);
                }
                else {
                    ccCouponList = new List<ccrz__E_Coupon__c>(
                        [SELECT ccrz__CouponName__c, 
                        		ccrz__CouponCode__c, 
                        		HMR_Coupon__c 
                          FROM ccrz__E_Coupon__c
                          WHERE HMR_Coupon__c =: hmrCoupon.Id 
                            		AND ccrz__Enabled__c = True 
                            		AND Coupon_Order_Type__c = 'A la Carte']);
                }
            }

            if(ccCouponList.size() > 0)
            	couponRecord = ccCouponList[0];
        }

        return couponRecord;
	}

	/**
	* Checks for automatic coupons to apply to the cart (i.e. Program/Account Membership Discounts and Kit Orders) or whether existing
	*  coupon is no longer qualified and should be removed
	* @param userId the user id of the active cart user
	* @param cartRecord the formatted cart data from the HMR_Cart_Service
	* @return AutomaticCouponRecord result of the check
	*/
	public AutomaticCouponRecord getAutomaticCoupon(string userId, HMR_Cart_Service.CartRecord cartRecord){
		HMR_User_Selector userSelector = new HMR_User_Selector();
		User userRecord = userSelector.getById(userId);

		return getAutomaticCoupon(userRecord, cartRecord);
	}

	/**
	* Checks for automatic coupons to apply to the cart (i.e. Program/Account Membership Discounts and Kit Orders) or whether existing
	*  coupon is no longer qualified and should be removed
	* @param userRecord the user record with related account and contact ids of the active cart user
	* @param cartRecord the formatted cart data from the HMR_Cart_Service
	* @return AutomaticCouponRecord result of the check
	*/
	public AutomaticCouponRecord getAutomaticCoupon(User userRecord, HMR_Cart_Service.CartRecord cartRecord){
			AutomaticCouponRecord automaticCoupon;
			Map<String, ccrz__E_Coupon__c> dataMap = new Map<String, ccrz__E_Coupon__c>();
            Boolean existingDefaultCoupon = false;
            Boolean result = false;
            String contactId = userRecord.ContactId;
            String accountId = userRecord.Contact.AccountId;
            String currentCoupon = ''; 
            Set<Id> cartProductIdSet = new Set<Id>();
            boolean isHMREmployeeAccount = userRecord.Contact.Account.Name == 'HMR Employees'; 
            if(cartRecord.couponRecord != null)
            	currentCoupon = cartRecord.couponRecord.couponCode;

            //Get set of product ids
            for(HMR_Cart_Service.CartItem cartItem : cartRecord.cartItemList)
            	cartProductIdSet.add(cartItem.productId);

            //Retrieve current HMR coupon applied to cart if exists
            if(!String.isBlank(currentCoupon) && currentCoupon.containsIgnoreCase('ProgramMember')){
                existingDefaultCoupon = [SELECT Id FROM HMR_Coupon__c WHERE Name = :currentCoupon AND Default_Consumer_Coupon__c = true LIMIT 1].size() > 0;
        	}

            //If contact is an HMR Employee coupons can't be applied to their orders
            if(!isHMREmployeeAccount){
	            Id programId;             	 //variable to store program id associated to a kit
	            String programName;
	            String orderType;
	            String interimOrderType;
	            Boolean isProgramMember = false;
	            String programPhase;
            	HMR_Coupon__c hmrCoupon;
            	HMR_Coupon__c programInterimCoupon;
            	HMR_Coupon__c programDiscountCoupon;

                //Retrieve HMR coupon associated to client's account
            	for(HMR_Coupon__c hmrCouponRecord : [SELECT Name, 
            											    Account__r.RecordType.DeveloperName 
            									     FROM HMR_Coupon__c 
            									     WHERE ((Account__c = :accountId AND Account__c != null) OR Name = 'ProgramMember' OR (Name = 'ProgramDiscount' AND  Default_Consumer_Coupon__c = True))
            									  		AND hmr_Active__c = True 
            									     ]){
            		if(hmrCouponRecord.Name == 'ProgramMember')
            			programInterimCoupon = hmrCouponRecord;
            		else if(hmrCouponRecord.Name == 'ProgramDiscount')
            			programDiscountCoupon = hmrCouponRecord;
            		else
            			hmrCoupon = hmrCouponRecord;
            	}

	            //Check to see if client already has an active program membership
	            List<Program_Membership__c> programMembershipList = [SELECT Name, 
	            													 hmr_Contact__c, 
	            													 Program__r.Name  
	            											  FROM Program_Membership__c
	                										  WHERE hmr_Status__c = 'Active' 
	                										  	AND hmr_First_Order_Number__c <> null 
	                										  	AND hmr_Contact__c = :contactId];
	            if(!programMembershipList.isEmpty() && contactId != null){
	                isProgramMember = true;
	                //Get Name of program phase
	                programPhase = programMembershipList[0].Program__r.Name;
	            }

	            //Loop through cart items to determine if the cart contains a dynamic kit associated to a program
	            for(ccrz__E_Product__c cartProd: [SELECT Name, 
	            										 Program__c, 
	            										 Program__r.Name,
	            										 ccrz__ProductType__c 
	            								  FROM ccrz__E_Product__c 
	            								  WHERE Id IN : cartProductIdSet]) {
	                if(cartProd.Program__c != null && cartProd.ccrz__ProductType__c == 'Dynamic Kit') {
	                    programId = cartProd.Program__c;
	                    programName = cartProd.Program__r.Name;
	                    break;
	                }
	            }

	            //If an associated program is found, set order type of coupon depending on the program phase
	            if(programId != null && isProgramMember == false && !String.isBlank(programName)){
                    if(programName == 'P1 Healthy Shakes'){
                        orderType = 'HSS 1st Order';
                        if(programDiscountCoupon != null && hmrCoupon == null){
                            hmrCoupon = programDiscountCoupon;
                        }
                    }
                    else if(programName == 'P1 Healthy Solutions'){
                        orderType = 'P1 1st Order';
                        if(programDiscountCoupon != null && hmrCoupon == null){
                            hmrCoupon = programDiscountCoupon;
                        }
                    }
                    else if(programName == 'Phase 2'){
                        orderType = 'P2';
                        if(programDiscountCoupon != null && hmrCoupon == null){
                            hmrCoupon = programDiscountCoupon;
                        }
                    }
	            }
	           
	            //Set appropriate interim order type based on Phase
	            if(!String.isBlank(programPhase)){
	                if(programPhase == 'P1 Healthy Solutions'){
	                    interimOrderType = 'P1 Interim';
	                }
	                else if(programPhase == 'P1 Healthy Shakes'){
	                     interimOrderType = 'HSS Interim';
	                }
	                else if(programPhase == 'Phase 2'){
	                    interimOrderType = 'P2 Interim';
	                }
	                else{
	                    programInterimCoupon = null;
	                }
	            }
	            //Query the associated cc Coupon to be applied to this order
	            //if cart contains a kit and this client is not already a Program Member, apply the appropriate 1st order type coupon
	            List<ccrz__E_Coupon__c> ccCouponList;
	            if(orderType != null && hmrCoupon != null) {
	                ccCouponList = new List<ccrz__E_Coupon__c>(
	                    [SELECT ccrz__CouponName__c,
	                    		ccrz__CouponCode__c, 
	                    		HMR_Coupon__c, 
	                    		hmr_Program__c 
	                     FROM ccrz__E_Coupon__c
	                     WHERE HMR_Coupon__c = :hmrCoupon.Id 
	                     	AND ccrz__Enabled__c = true 
	                     	AND hmr_Program__c = :programId 
	                     	AND Coupon_Order_Type__c = :orderType]);
	            }
	            //if cart does not contain a kit, coupon order type will be Interim if client is already a program member and A La Carte if they are not
	            else {	            	
	                if(isProgramMember == true && interimOrderType != null){
	                	string hmrCouponId = '';
	                	if(hmrCoupon != null)
	                		hmrCouponId = hmrCoupon.Id;
	                	else if(programInterimCoupon != null)
	                		hmrCouponId = programInterimCoupon.Id;

	                	if(!String.isBlank(hmrCouponId))
	                		ccCouponList = HMR_CC_Coupon_Selector.getByHMRCouponIdAndOrderType(hmrCouponId, interimOrderType);
	                }
	                else if(hmrCoupon != null){
	                    ccCouponList = HMR_CC_Coupon_Selector.getByHMRCouponIdAndOrderType(hmrCoupon.Id, 'A la Carte');
	                }
	                else if(existingDefaultCoupon == true){
	                    automaticCoupon = new AutomaticCouponRecord(null, true);
	                }
	            }

	            //if ccCoupon with matching criteria was not found, remove existing coupon if exists
	            if(ccCouponList == null || ccCouponList.isEmpty()){
	                if(existingDefaultCoupon == true){
	                	automaticCoupon = new AutomaticCouponRecord(null, true);
	                }
	            }
	            else{
	            	automaticCoupon = new AutomaticCouponRecord(ccCouponList[0], false);
	            }
  			}

        return automaticCoupon;
	}

	//Wrapper class to hold coupon record and related data
	public class CouponRecord{
		public string id {get; private set;}
		public string displayName {get; private set;}
		public string couponCode {get; private set;}
		public string couponType {get; private set;}
		public decimal discountAmount {get; private set;}
		public string discountType {get; private set;}
		//public Date startDate {get; private set;}
		//public Date endDate {get; private set;}
		public Long maxUse {get; private set;}
		public Long totalUsed {get; private set;}

		public CouponRecord(Map<String, Object> couponDataMap){
			this.id = (String)couponDataMap.get('sfid');
			this.couponCode = (String)couponDataMap.get('couponCode');
			this.couponType = (String)couponDataMap.get('couponType');
			this.discountAmount = (Decimal)couponDataMap.get('discountAmount');
			this.discountType = (String)couponDataMap.get('discountType');
			//this.startDate = (Date)couponDataMap.get('startDate');
			//this.endDate = (Date)couponDataMap.get('endDate');
			this.maxUse = (Long)couponDataMap.get('maxUse');
			this.totalUsed = (Long)couponDataMap.get('totalUsed');
			this.displayName = !String.isBlank(couponCode) && couponCode.contains('_') ? 
							   this.couponCode.left(this.couponCode.indexOf('_')) :
							   this.couponCode;
		}
	}

	public class AutomaticCouponRecord{
		public ccrz__E_Coupon__c couponRecord {get; private set;}
		public boolean removeCoupon {get; private set;}

		public AutomaticCouponRecord(ccrz__E_Coupon__c couponRecord){
			this(couponRecord, false);
		}
		public AutomaticCouponRecord(ccrz__E_Coupon__c couponRecord, boolean removeCoupon){
			this.couponRecord = couponRecord;
			this.removeCoupon = removeCoupon;
		}
	}
}