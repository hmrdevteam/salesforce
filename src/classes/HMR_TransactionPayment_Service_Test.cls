/**
* Test Class Coverage of the HMR_TransactionPayment_Service
*
* @Date: 10/26/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 10/26/2017
* @JIRA: 
*/
@isTest
private class HMR_TransactionPayment_Service_Test {
	@testSetup
	private static void setupTestData(){
		User testUserRecord = cc_dataFactory.testUser;
		List<ccrz__E_Order__c> orderList = cc_DataFactory.createOrders(1);

		ccrz__E_TransactionPayment__c transactionPaymentRecord = new ccrz__E_TransactionPayment__c(
			ccrz__CCOrder__c = orderList[0].Id
		   ,ccrz__Account__c = cc_dataFactory.testAccount.Id
		   ,ccrz__AccountNumber__c = '4111111111111111'
		   ,ccrz__AccountType__c = 'cc'
		   ,ccrz__Amount__c = 100.0
		   ,ccrz__Contact__c = cc_dataFactory.testUser.ContactId
		   ,Card_Type__c = 1
		   ,ccrz__ExpirationMonth__c = 1
		   ,ccrz__ExpirationYear__c = Date.today().year() + 1
		   ,ccrz__RequestAmount__c = 100.0
		   ,ccrz__Storefront__c = 'DefaultStore'
		   ,ccrz__TransactionTS__c = DateTime.now().addDays(-1)
		   ,ccrz__TransactionType__c = 'sales'
		   ,ccrz__User__c = testUserRecord.Id
		   ,ccrz__VerificationCode__c = 'd2ae1b1-4d92-889b-0fed-01c63a039cb5'
		   ,OwnerId = testUserRecord.Id
		);

		insert transactionPaymentRecord;
	}

	@isTest
	private static void testGetByOrderId(){
		ccrz__E_Order__c orderRecord = [SELECT Id FROM ccrz__E_Order__c LIMIT 1];
		System.Debug([SELECT ccrz__CCOrder__c FROM ccrz__E_TransactionPayment__c]);
		User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
		HMR_TransactionPayment_Service.TransactionPaymentRecord transactionPaymentRecord;

		System.runAs(userRecord){
			Test.startTest();

			HMR_TransactionPayment_Service paymentService = new HMR_TransactionPayment_Service();
			transactionPaymentRecord =  paymentService.getByOrderId(orderRecord.Id);

			Test.stopTest();
		}

		//Verify the transaction payment record was returned
		System.assert(transactionPaymentRecord != null);
		System.assert(!String.isBlank(transactionPaymentRecord.id));
	}
}