public class TestRenderingApi {
    
    public static void x() {
		List<String> tagPaths = new List<String>();
        tagPaths.add('/JourneyTaxonomyV1/QuickStart/Day-1');         
        //tagPaths.add('/JourneyTaxonomyV1/PreStart/Day-X');  
        
          
        RenderingAPIRequest renderingRequest = new RenderingAPIRequest();
        renderingRequest.parameters.put('renderType', 'taxonomy');
        renderingRequest.listParameters.put('contentTypes', new List<String>{'Text'});
        renderingRequest.listParameters.put('tagPaths', tagPaths);
          
        List<String> layoutsForTaxonomy = new List<String>{'DocumentStyle1'};
        renderingRequest.layoutsForTaxonomy = new List<List<String>>{layoutsForTaxonomy};
          
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('renderingRequest', json.serialize(renderingRequest));
        parameters.put('action', 'getRenderedContent');
        parameters.put('service', 'OrchestraRenderingAPI');
        parameters.put('sname', 'HMR_Program_Community'); // Replace site_name with the name of your OrchestraCMS site 
        parameters.put('application', 'runtime');
        parameters.put('apiVersion', '5.0');
        
        try {
        String response = cms.ServiceEndpoint.doActionApex(parameters);
        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);
          
        if (!apiResponse.isSuccess)
            System.Debug('Could not retrieve renderings for this node');
        if (apiResponse.type != 'RenderResultBundle')
            System.Debug('Unexpected result from Rendering API');
          
        System.Debug(apiResponse.responseObject);
        }
        catch(Exception e) {
            System.debug('---> error:' + e.getMessage() );
        }
            
        
    }

    public static void test0() {
        System.debug('---> here 0');
        RenderingAPIRequest renderingRequest = new RenderingAPIRequest();
        renderingRequest.parameters.put('renderType', 'taxonomy');
        renderingRequest.parameters.put('targeted', 'true');
        
        //renderingRequest.listParameters.put('contentNames', new List<String>{'Test Api Text'});
        renderingRequest.listParameters.put('tagPaths', new List<String>{'/JourneyTaxonomyV1/QuickStart/Day-1'});
        System.debug('---> here 1');
        //List<String> contentLayouts = new List<String>{'ArticleDetail', 'ArticleSummary'};
        //    renderingRequest.listParameters.put('contentLayouts', contentLayouts);
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('renderingRequest', json.serialize(renderingRequest));
        parameters.put('action', 'getRenderedContent');
        parameters.put('service', 'OrchestraRenderingAPI');
        parameters.put('sname', 'HMR_Program_Community'); // Replace site_name with the name of your OrchestraCMS site 
        parameters.put('application', 'runtime');
        parameters.put('apiVersion', '5.0');
        System.debug('---> here 2');
        String response = cms.ServiceEndpoint.doActionApex(parameters);
        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);
        if (!apiResponse.isSuccess)
            System.debug('---> Could not retrieve renderings for this node');
        if (apiResponse.type != 'RenderResultBundle')
            System.debug('---> Unexpected result from Rendering API');
        System.debug('---> apiResponse:');
        System.Debug(apiResponse.responseObject);
        
    }

    
    public static void test() {
        System.debug('---> here 0');
        RenderingAPIRequest renderingRequest = new RenderingAPIRequest();
        renderingRequest.parameters.put('renderType', 'contentName');
        renderingRequest.listParameters.put('contentNames', new List<String>{'Test Api Text'});
        System.debug('---> here 1');
        //List<String> contentLayouts = new List<String>{'ArticleDetail', 'ArticleSummary'};
        //    renderingRequest.listParameters.put('contentLayouts', contentLayouts);
        Map<String, String> parameters = new Map<String, String>();
        parameters.put('renderingRequest', json.serialize(renderingRequest));
        parameters.put('action', 'getRenderedContent');
        parameters.put('service', 'OrchestraRenderingAPI');
        parameters.put('sname', 'HMR_Program_Community'); // Replace site_name with the name of your OrchestraCMS site 
        parameters.put('application', 'runtime');
        parameters.put('apiVersion', '5.0');
        System.debug('---> here 2');
        String response = cms.ServiceEndpoint.doActionApex(parameters);
        JSONMessage.APIResponse apiResponse = (JSONMessage.APIResponse) json.deserialize(response, JSONMessage.APIResponse.class);
        if (!apiResponse.isSuccess)
            System.debug('---> Could not retrieve renderings for this node');
        if (apiResponse.type != 'RenderResultBundle')
            System.debug('---> Unexpected result from Rendering API');
        System.debug('---> apiResponse:');
        System.Debug(apiResponse.responseObject);
        
    }
}