@isTest
private class ClassPermanentMakeupControllerTest{


    static testMethod  void firstTestMethod(){

         Test.startTest();

        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanU21', Email='standuser3221@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='TestingU321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser212@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test', LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;


        Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;


        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;

         Class__c classObjSelected = new Class__c(hmr_Class_Name__c = 'testing Class selected', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '12345671242', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObjSelected;

        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;


        Program_Membership__c programMemObj = new Program_Membership__c(Program__c = programObj.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj.id);
        insert programMemObj;

        Class_Member__c classMemObj = new Class_Member__c(hmr_Contact_Name__c = testContatObj.id, hmr_Class__c = classObj.id, hmr_Program_Membership__c = programMemObj.Id);
        insert classMemObj;

        Class_Member__c classMemObjNew = new Class_Member__c(hmr_Contact_Name__c = testContatObj.id, hmr_Class__c = classObj.id, hmr_Program_Membership__c = programMemObj.Id);
        insert classMemObjNew;

        List<Class_Member__c > cMList = new List<Class_Member__c >();
        cmList.add(classMemObj);

        List<Class__c> availableClass = new List<Class__c>();
        availableClass.add(classObj);

        Test.setCurrentPageReference(new PageReference('Page.ClassPermenantMakeup'));
        System.currentPageReference().getParameters().put('id', classMemObjNew.id);


        ApexPages.StandardSetController controllerObj = new ApexPages.StandardSetController(cmList);
        ClassPermanentMakeupController controller = new ClassPermanentMakeupController(controllerObj);

        controller.availableClassList = availableClass;
        controller.noRecords = FALSE;
        controller.noRecordMessage = 'No Records Found';
        controller.classMemberDetails = classMemObj;
        controller.classMemberId = classMemObj.id;
        controller.NewCMUrl = classMemObjNew.id;
        controller.showOtherClasses();
        controller.selectClassId();
        controller.selectedClass = classObjSelected.id;
        controller.changeClass();

        System.assertEquals(1, availableClass.size());

        Test.stopTest();

    }


    static testMethod  void secondTestMethod(){

         Test.startTest();

        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanU21', Email='standuser3221@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='TestingU321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser212@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName = 'test', LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;


        Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;


        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder', hmr_Active__c = FALSE);
        insert classObj;

         Class__c classObjSelected = new Class__c(hmr_Class_Name__c = 'testing Class selected', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '12345671242', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder', hmr_Active__c = FALSE);
        insert classObjSelected;

        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;


        Program_Membership__c programMemObj = new Program_Membership__c(Program__c = programObj.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj.id);
        insert programMemObj;

        Class_Member__c classMemObj = new Class_Member__c(hmr_Contact_Name__c = testContatObj.id, hmr_Class__c = classObj.id, hmr_Program_Membership__c = programMemObj.Id);
        insert classMemObj;

        Class_Member__c classMemObjNew = new Class_Member__c(hmr_Contact_Name__c = testContatObj.id, hmr_Class__c = classObj.id, hmr_Program_Membership__c = programMemObj.Id);
        insert classMemObjNew;

        List<Class_Member__c > cMList = new List<Class_Member__c >();
        cmList.add(classMemObj);

        List<Class__c> availableClass = new List<Class__c>();

        Test.setCurrentPageReference(new PageReference('Page.ClassPermenantMakeup'));
        System.currentPageReference().getParameters().put('id', classMemObj.id);


        ApexPages.StandardSetController controllerObj = new ApexPages.StandardSetController(cmList);
        ClassPermanentMakeupController controller = new ClassPermanentMakeupController(controllerObj);

        controller.availableClassList = availableClass;
        controller.noRecords = FALSE;
        controller.noRecordMessage = 'No Records Found';
        controller.classMemberDetails = classMemObj;
        controller.classMemberId = classMemObj.id;
        controller.NewCMUrl = classMemObjNew.id;
        controller.showOtherClasses();
        controller.selectClassId();
        controller.selectedClass = classObjSelected.id;
        controller.changeClass();
        System.assertEquals(0, availableClass.size());

        Test.stopTest();

    }

}