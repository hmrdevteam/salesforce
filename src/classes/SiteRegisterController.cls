/**
* Apex Controller for HMR Community Registration Pages
*
* @Date: 2017-11-29
* @Author Nathan Anderson (Magnet 360)
* @Modified: 2018-01-16 Javier Arroyo and Zach Engman
* @JIRA:
*/
public class SiteRegisterController {

    public Id PORTAL_ACCOUNT_ID;
    public String userId;
    public Account a = new Account();
    public String patientType;
    public String redirectUrl = '';
    public String empCode {get; set;}
    public Boolean invalidEmpCode = false;
    public Boolean redirectToCheckout = false;
    public String username {get; set;}
    public String email {get; set;}
    public String firstname {get; set;}
    public String lastname {get; set;}
    public String zipcode {get; set;}
    public String birthDate {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public Boolean preference {get; set;}
    public String cartId = '';
    public String tcTitle {get; set;}
    public String tcDesc {get; set;}
    public String privTitle {get; set;}
    public String privDesc {get; set;}
    public String refURL {get; set;}
    public Boolean hasKitCart {get; set;}
    public Boolean isMobileFlow {get; set;}

    private List<Connected_App_Setting__mdt> connectedAppSettingList{get{
        if(connectedAppSettingList == null)
            connectedAppSettingList = [SELECT DeveloperName, Setting_Value__c FROM Connected_App_Setting__mdt];

        return connectedAppSettingList;
    } set;}
    private string clientId{get{
        if(clientId == null){
            for(Connected_App_Setting__mdt appSetting : connectedAppSettingList){
                if(appSetting.DeveloperName == 'HMR_Diet_Tracker_Consumer_Key'){
                    clientId = appSetting.Setting_Value__c;
                    break;
                }
            }
        }

        return clientId;
    }set;}
    private string redirectUri{get{
        if(redirectUri == null){
            for(Connected_App_Setting__mdt appSetting : connectedAppSettingList){
                if(appSetting.DeveloperName == 'HMR_Diet_Tracker_Redirect_Uri'){
                    redirectUri = appSetting.Setting_Value__c;
                    break;
                }
            }
        }

        return redirectUri;
    }set;}

    private boolean isValidPassword() {
        return password == confirmPassword;
    }

    public SiteRegisterController () {
        //Terms & Conditions Data
        List<ccrz__E_Term__c> terms = [SELECT Id,ccrz__Description__c,ccrz__Title__c,Consent_Type__c
                                        FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE ];
        string baseURL = Site.getBaseUrl() + '/';                                
        this.isMobileFlow =(ApexPages.currentPage().getHeaders().get('Referer') != null && 
                           ApexPages.currentPage().getHeaders().get('Referer').containsIgnoreCase('RemoteAccessAuthorizationPage')) ||
                             ApexPages.currentPage().getUrl().containsIgnoreCase('RemoteAccessAuthorizationPage');
        

        if(terms.size() > 0){
            for(ccrz__E_Term__c term : terms){

                if(term.Consent_Type__c == 'HMR Terms & Conditions' ){
                    tcTitle = term.ccrz__Title__c;
                    tcDesc = term.ccrz__Description__c;
                }
                if(term.Consent_Type__c == 'Privacy/Tracking' ){
                    privTitle = term.ccrz__Title__c;
                    privDesc = term.ccrz__Description__c;
                }
            }
        }

        //get the startURL passed in from sign-in page
        if(!this.isMobileFlow)
            refURL = ApexPages.currentPage().getParameters().get('startURL');
        else
            refURL = baseUrl + 'services/oauth2/authorize?response_type=token&client_id=' + clientId + '&redirect_uri=' + redirectUri;
    }

    public void trimResponseMessage(Map<String, Object> rMap){
        string ss = string.valueOf(rMap.get('exceptionMsg'));
        ss = ss.replace('[','');
        ss = ss.replace(']','');
        rMap.put('exceptionMsg', ss);
    }

    public PageReference registerUser(){
        //Set My Account as the default redirect
        redirectUrl = '/my-account';

        //If the refURL is populated from the URL, set this as the redirect
        if(refURL != null && refURL != '') {
            redirectURL = refURL;
        }

        //If the user comes to registration page via cart, set redirect to checkout
        if(refURL != null && refURL.contains('/cart')) {
            redirectURL = 'checkout';
        }

        //check password entry and validity, throw errors if checks do not pass, otherwise enter registration process
        if(password != null && password != '') {
            if(!isValidPassword()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
                ApexPages.addMessage(msg);
                return null;
            } else {

                Savepoint sp = Database.setSavepoint();

                //Initialize map to hold return data and set up call to HMR Registration Service
                Map<String, Object> returnMap = new Map<String, Object>();

                HMR_Registration_Service regService = new HMR_Registration_Service();

                //Break up registration types by checking for employer code first
                if(empCode != null && empCode != '') {
                    String regType = 'Employer';
                    //send data to Registration Service for Employer registration attempts (from /sign-up)
                    try{
                        returnMap = regService.processRegistration(firstname, lastname, email, password, empcode, preference, regType);
                        //Set redirect to Plans page for successful Employer registrations
                        redirectURL = '/healthy-solutions-at-home-diet-plan';
                    } catch(Exception ex) {
                            String msg = ex.getMessage();
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                            Database.rollback(sp);
                            return null;
                        }

                    //this error message is returned if no matching account is found on an employer registration attempt
                    if(returnMap.get('errorMsg') != null) {
                        String msg = String.valueOf(returnMap.get('errorMsg'));
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                        Database.rollback(sp);
                        return null;
                    }

                    //these all other exceptions returned from the registration service
                    if(returnMap.get('exceptionMsg') != null){
                        trimResponseMessage(returnMap);
                        String msg = String.valueOf(returnMap.get('exceptionMsg'));
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                        Database.rollback(sp);
                        return null;
                    }

                } else {
                    //Set registration type to General for all other registration attempts (from /register)
                    String regType = 'General';
                    //Send data to HMR Registration Service
                    try{
                        returnMap = regService.processRegistration(firstname, lastname, email, password, zipcode, preference, regType);
                    } catch(Exception ex) {
                        System.debug('\n\n\n\n---> error in registration: ' + ex.getMessage());
                        String msg = ex.getMessage();
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                        Database.rollback(sp);
                        return null;
                    }

                    //this handles all exceptions returned from the registration service
                    if(returnMap.get('exceptionMsg') != null){
                        System.debug('\n\n\n\n---> error in registration: error caught in return map');
                        trimResponseMessage(returnMap);
                        String msg = String.valueOf(returnMap.get('exceptionMsg'));
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                        Database.rollback(sp);
                        return null;
                    }
                }

                //If Registration is successful and the service returns a user Id, we need to check to see if the user had a guest cart and transfer it before logging in
                if(returnMap.get('userId') != null) {

                    string activeCartId = '';
                    boolean transferedCart = false;

                    if(!String.isBlank(cartId))
                        activeCartId = cartId;
                    else if(ApexPages.currentPage().getParameters().get('cartID') != null)
                        activeCartId = ApexPages.currentPage().getParameters().get('cartID');
                    else if(ApexPages.currentPage().getCookies().get('currCartId') != null)
                        activeCartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();

                    if(!String.isBlank(activeCartId)) {
                        if(!String.isBlank(activeCartId)){
                            try{
                                List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: email];
                                ccrz.cc_RemoteActionResult cartTransferResult = HMR_CC_CartTransfer_Extension.transferCartToUser(activeCartId, userRecordList[0].Id);
                                //If Cart was Transfered/Take to Cart, must pass in redirect here to login, not directly to response string or it won't login
                                if(cartTransferResult.success && redirectURL == 'checkout'){
                                    transferedCart = true;
                                    redirectToCheckout = redirectURL == 'checkout';
                                    redirectUrl = Site.getBaseUrl() + String.format('/checkout?cartID={0}&isCSRFlow=true&portalUser=&store=&cclcl=en_US', new String[]{String.valueOf(cartTransferResult.data)});
                                }
                            } catch(Exception ex) {
                                String msg = 'There was an error transferring your Shopping Cart.';
                                System.Debug(String.valueOf(ex.getMessage()));
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
                                Database.rollback(sp);
                                return null;
                            }
                        }
                    }

                    //HPRP-3789: When creating a new account, we want to bring them to the MyAccount page if they would have been brought back home
                    if(!String.isBlank(redirectUrl)){
                        if(redirectUrl.containsIgnoreCase('/Home')){
                            //can't convert to lower case incase case-sensitive id is used to replace both variations
                            redirectUrl = redirectUrl.replace('/home', '/my-account');
                            redirectUrl = redirectUrl.replace('/Home', '/my-account');
                        }
                        else if(redirectUrl.containsIgnoreCase('/Welcome')){
                            redirectUrl = redirectUrl.toLowerCase().replace('/welcome', '/my-account');
                            redirectUrl = redirectUrl.toLowerCase().replace('/Welcome', '/my-account');
                        }
                        else if(redirectUrl.endsWith('/'))
                            redirectUrl = '/my-account';
                    }

                    //If we are in the mobile flow, we need to update the birthdate of the contact record
                    /*
                    if(this.isMobileFlow){
                        try{
                            Date clientBirthDate = Date.valueOf(this.birthDate);
                            List<User> userRecordList = [SELECT ContactId FROM User WHERE UserName =: email];
                            update new Contact(Id = userRecordList[0].ContactId, hmr_Date_Of_Birth__c = clientBirthDate); 
                        }
                        catch(Exception ex){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error updating the birthdate: ' + this.birthDate));
                            Database.rollback(sp);
                            return null;
                        }
                    }
					*/

                    //Finally, set up the login call and pass in the redirectURL and log the user in
                    PageReference loginPageRedirect = Site.login(email, password, redirectUrl);
                    return loginPageRedirect;

                } else {
                    return null;
                }
            }
        } else {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid password');
            ApexPages.addMessage(msg);
            return null;
        }
    }



    // method to transfer guest user cart to register or login user
    //Checks if there is a cart within the cookies and if so transfers it to the user upon login
   public static ccrz.cc_RemoteActionResult transferGuestCart(String username){
       ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
       result.success = false;

       if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
            String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
            List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: username];

            if(!String.isBlank(cartId) && userRecordList.size() > 0)
               result = HMR_CC_CartTransfer_Extension.transferCartToUser(cartId, userRecordList[0].Id);
       }

       return result;
   }
}