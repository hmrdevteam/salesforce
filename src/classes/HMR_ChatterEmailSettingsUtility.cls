/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 9 August 2017
 * Created By: Nathan Anderson
 * Last Modified Date:
 * Last Modified By:
 * Description:
 Called by User Trigger, this class disables native chatter emails for Community Users.
 * ****************************************************/

public class HMR_ChatterEmailSettingsUtility {
	public HMR_ChatterEmailSettingsUtility() {

	}

	public void updateSettings(List<User> users) {
		//Create Set to hold user Ids
		Set<Id> userIds = new Set<Id>();

		//Loop through Users passed in from Trigger.new
		for(User u : users){
			//Check to make sure the User is a Community User
			if(u.UserType == 'CspLitePortal'){
				//Add Id to Set for use in query below
				userIds.add(u.Id);
			}
		}

		//If there are valid community user Ids to process, get the NetworkMember record
		if(userIds.size() > 0){
			List<NetworkMember> netMembers = [SELECT Id, MemberId, NetworkId, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId IN :userIds];

			//New List to hold records we will update
			List<NetworkMember> nmToUpdate = new List<NetworkMember>();

			//Loop through the Network Member records
			for(NetworkMember nm : netMembers){
				//Set the Global Disable Email Preference to true to stop ALL chatter emails for this User
				//This can be updated to be more specific to allow certain types of emails if desired
				nm.PreferencesDisableAllFeedsEmail = TRUE;
				//Add the NetworkMembers to the list to update
				nmToUpdate.add(nm);
			}

			//Check to see if any NetworkMembers records are ready to be updated
			if(nmToUpdate.size() > 0){
				try {
					//Update the records
					update nmToUpdate;
				} catch(Exception e) {
					system.debug('Exception in update users email settings: ' + e);
				}
			}
		}
	}
}