/**
 * Controller for ReShip Clone Utility
 * Uses CloneUtils.cls
 * @Date: 3/22/2017
 * @Author: Nathan Anderson (Magnet 360)
*/


public with sharing class ReShipCloneController {

	public String oId {get;set;}

	public ReShipCloneController() {

		oId = ApexPages.currentPage().getParameters().get('oId');
		ccrz__E_Order__c order = [SELECT Id, Name FROM ccrz__E_Order__c WHERE Id =: oId];

	}

	public pageReference cloneOrder() {

        //Get the Id of the original quote and pass it to the SOQL generator, then query for all fields and clone the quote
        String soql = CloneUtils.getCreatableFieldsSOQL('ccrz__E_Order__c','id=\''+oId+'\'');
        ccrz__E_Order__c originalOrder = (ccrz__E_Order__c)Database.query(soql);
        ccrz__E_Order__c copyOrder = originalOrder.clone(false, true);

		//setup new field values here
		copyOrder.ccrz__EncryptedId__c = '';
		copyOrder.ccrz__OrderId__c = '';
		copyOrder.ccrz__OrderNumber__c = null;
		copyOrder.ccrz__OrderDate__c = System.TODAY();
		copyOrder.hmr_Order_Type_c__c = 'Re-Ship';
		copyOrder.ccrz__OrderStatus__c = 'Open';
		copyOrder.ccrz__AdjustmentAmount__c = 0;
		copyOrder.ccrz__ShipDiscountAmount__c = 0;
		copyOrder.ccrz__ShipAmount__c = 0;
		copyOrder.ccrz__TotalDiscount__c = 0;
        insert copyOrder;

		//Get related Order Item Records, use SOQL Generator to get all fields and clone each record
        List<ccrz__E_OrderItem__c> items = new List<ccrz__E_OrderItem__c>();

        String itemSoql = CloneUtils.getCreatableFieldsSOQL('ccrz__E_OrderItem__c','ccrz__Order__c=\''+oId+'\'');
        List<ccrz__E_OrderItem__c> originalItems = (List<ccrz__E_OrderItem__c>)Database.query(itemSoql);

        for(ccrz__E_OrderItem__c item : originalItems) {

            ccrz__E_OrderItem__c itemCopy = item.clone(false,true);
            itemCopy.ccrz__Order__c = copyOrder.Id;
			itemCopy.ccrz__Price__c = 0;
			itemCopy.ccrz__SubAmount__c = 0;
			itemCopy.ccrz__AbsoluteDiscount__c = 0;
			itemCopy.ccrz__AdjustmentAmount__c = 0;
			itemCopy.ccrz__OrderItemId__c = '';
            items.add(itemCopy);
        }

        insert items;

		return new PageReference('/apex/ReshipSetupList?id='+copyOrder.Id);
	}
}