/**
* Test Class Coverage of the HMR_Coupon_Service
*
* @Date: 11/10/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 11/10/2017
* @JIRA: 
*/
@isTest
private class HMR_Coupon_Service_Test {
    @testSetup
    private static void setupTestData(){
        User testUserRecord = cc_dataFactory.testUser;
        cc_dataFactory.setupCatalog();

         //Create kit product
        ccrz__E_Product__c ccrzKitProduct = new ccrz__E_Product__c(Name = 'Healthy Solutions® Quick Start® Kit'
                                                                   ,ccrz__SKU__c = 'HSQSKRVC'
                                                                   ,ccrz__ProductType__c = 'Dynamic Kit'
                                                                   ,ccrz__ProductStatus__c = 'Released'
                                                                   ,hmr_ProductActive__c = true
                                                                   ,ccrz__Storefront__c = 'DefaultStore'
                                                                   ,ccrz__QuantityPerUnit__c = 1
                                                                   ,ccrz__StartDate__c = Date.today()
                                                                   ,ccrz__EndDate__c = Date.today());

         //Create shake product
        ccrz__E_Product__c ccrzShakeProduct = new ccrz__E_Product__c(Name = 'HMR 120 Vanilla Shake'
                                                                   ,ccrz__SKU__c = 'IH120V'
                                                                   ,ccrz__ProductType__c = 'Product'
                                                                   ,hmr_ProductActive__c = true
                                                                   ,ccrz__ProductStatus__c = 'Released'
                                                                   ,ccrz__Storefront__c = 'DefaultStore'
                                                                   ,ccrz__QuantityPerUnit__c = 1
                                                                   ,ccrz__StartDate__c = Date.today()
                                                                   ,ccrz__EndDate__c = Date.today());

        insert new List<ccrz__E_Product__c>{ccrzKitProduct, ccrzShakeProduct};

        //Create kit category
        ccrz__E_Category__c ccrzKitCategory = new ccrz__E_Category__c(Name = 'Kits'
                                                                      ,ccrz__CategoryID__c = '800'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 700);
        //Create shake category
        ccrz__E_Category__c ccrzShakeCategory = new ccrz__E_Category__c(Name = 'Shake/Soup'
                                                                      ,ccrz__CategoryID__c = '400'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 400);
        insert new List<ccrz__E_Category__c>{ccrzKitCategory, ccrzShakeCategory};

        //Create kit product category
        ccrz__E_ProductCategory__c ccrzKitProductCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzKitProduct.Id
                                                                                            ,ccrz__Category__c = ccrzKitCategory.Id
                                                                                            ,ccrz__StartDate__c = Date.today()
                                                                                            ,ccrz__EndDate__c = Date.today());

        //Create shake product category
        ccrz__E_ProductCategory__c ccrzShakeProductCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzShakeProduct.Id
                                                                                            ,ccrz__Category__c = ccrzShakeCategory.Id
                                                                                            ,ccrz__StartDate__c = Date.today()
                                                                                            ,ccrz__EndDate__c = Date.today());
            
        insert new List<ccrz__E_ProductCategory__c>{ccrzKitProductCategory, ccrzShakeProductCategory};

        //Create program
        Program__c programRecord = new Program__c(Name = 'P1 Healthy Solutions'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Solutions'
                                                 ,hmr_Phase_Type__c = 'P1'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord;

        //Create program
        Program__c programRecord2 = new Program__c(Name = 'P1 Healthy Shakes'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Shakes'
                                                 ,hmr_Phase_Type__c = 'P1'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord2;

        //Create program
        Program__c programRecord3 = new Program__c(Name = 'Phase 2'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Solutions'
                                                 ,hmr_Phase_Type__c = 'P2'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord3;

        //Create program membership 
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id
                                                                                  ,hmr_Active__c = true
                                                                                  ,hmr_Status__c = 'Active'
                                                                                  ,hmr_Contact__c = testUserRecord.ContactId);
        insert programMembershipRecord;

        //Create coupon data
        HMR_Coupon__c hmrCouponRecord = new HMR_Coupon__c(
            Name = 'HMR_100',
            hmr_Active__c = true,
            hmr_Description__c = '100 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert hmrCouponRecord;

        ccrz__E_Coupon__c childCouponRecord = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'A la Carte',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = hmrCouponRecord.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'HMR_100',
            ccrz__CouponName__c = 'HMR_100',
            ccrz__TotalUsed__c = 0
        );
        insert childCouponRecord;

        //*******************************************************************
        //Create coupon data
        HMR_Coupon__c programHmrCoupon = new HMR_Coupon__c(
            Name = 'ProgramDiscount',
            hmr_Active__c = true,
            hmr_Description__c = '100 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert programHmrCoupon;

        //Create coupon data
        HMR_Coupon__c programMemberHmrCoupon = new HMR_Coupon__c(
            Name = 'ProgramMember',
            hmr_Active__c = true,
            hmr_Description__c = '20 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert programMemberHmrCoupon;

        List<ccrz__E_Coupon__c> childCouponList = new List<ccrz__E_Coupon__c>();


        
        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_P1_1',
            ccrz__CouponName__c = 'ProgramDiscount_P1_1',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'HSS 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_HS_1',
            ccrz__CouponName__c = 'ProgramDiscount_HS_1',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord2.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P2',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_P2',
            ccrz__CouponName__c = 'ProgramDiscount_P2',
            ccrz__TotalUsed__c = 0, 
            hmr_Program__c = programRecord3.Id
        ));


        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_P1_I',
            ccrz__CouponName__c = 'ProgramMember_P1_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'HSS Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_HSS_I',
            ccrz__CouponName__c = 'ProgramMember_HSS_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord2.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P2 Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_P2_I',
            ccrz__CouponName__c = 'ProgramMember_P2_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord3.Id
        ));

        insert childCouponList;              
    }

    @isTest
    private static void testGetByCartIdWithoutCoupon(){
        ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
        insert cartRecord;

        Test.startTest();

        HMR_Coupon_Service couponService = new HMR_Coupon_Service();
        HMR_Coupon_Service.CouponRecord couponRecord = couponService.getByCartId(cartRecord.Id);

        Test.stopTest();

        System.assert(couponRecord == null);
    }
    
    @isTest
    private static void testCouponRecordWrapperClass(){
        ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
        insert cartRecord;

        Test.startTest();

        HMR_Coupon_Service couponService = new HMR_Coupon_Service();
        HMR_Coupon_Service.CouponRecord couponRecord = new HMR_Coupon_Service.CouponRecord(new Map<String, Object>());
        
        String id = couponRecord.id;
        String displayName = couponRecord.displayName;
        String couponCode = couponRecord.couponCode;
        String couponType = couponRecord.couponType;
        Double discountAmount = couponRecord.discountAmount;
        String discountType = couponRecord.discountType;
        Long maxUse = couponRecord.maxUse;
        Long totalUsed = couponRecord.totalUsed;
        
        Test.stopTest();

        //System.assert(couponRecord == null);
    }
    
    @isTest
    private static void testAutomaticCouponRecord(){
        ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
        insert cartRecord;

        Test.startTest();

        HMR_Coupon_Service couponService = new HMR_Coupon_Service();
        HMR_Coupon_Service.AutomaticCouponRecord couponRecord = new HMR_Coupon_Service.AutomaticCouponRecord(new ccrz__E_Coupon__c());
        HMR_Coupon_Service.AutomaticCouponRecord couponRecord2 = new HMR_Coupon_Service.AutomaticCouponRecord(new ccrz__E_Coupon__c(), TRUE);
        
        ccrz__E_Coupon__c couponRec = couponRecord.couponRecord;
        Boolean removeCoupon = couponRecord.removeCoupon;
        
        Test.stopTest();

        //System.assert(couponRecord == null);
    }
 /*   
    @isTest
    private static void testGetCouponFromHMRCouponCode(){
        ccrz__E_Cart__c cartRecord = cc_dataFactory.getTestCart();
        insert cartRecord;
        
        Map<String, Object> cartItemRecMap = new Map<String, Object>();
        cartItemRecMap.put('price', 23);
        cartItemRecMap.put('quantity' , 2);
        
                
        ccrz__E_Product__c testPRod = new ccrz__E_Product__c(
                                                   Name = 'test',
                                                   ccrz__SKU__c = 'test-test',
                                                   ccrz__StartDate__c = Date.today(),
                                                   ccrz__EndDate__c = Date.today().addDays(1),
                                                   ccrz__ProductType__c = 'Kit',
                                                   ccrz__Quantityperunit__c = 2 );
        insert testProd;
        
        Map<String, Object> productMap = new Map<String, Object>();
        productMap.put('sfid',testProd.Id);
        productMap.put('productType', testProd.ccrz__ProductType__c);
        productMap.put('SKU', testProd.ccrz__SKU__c);
        
        Map<String, Object> cartItemMap = new Map<String, Object>();
        cartItemMap.put('itemTotal', 23);
        cartItemMap.put('price', 23);
        cartItemMap.put('quantity' , 2);
        cartItemMap.put('productR' , productMap);
        
        HMR_Cart_Service.CartItem cartItem = new HMR_Cart_Service.CartItem(cartItemMap);
        
        List<HMR_Cart_Service.CartItem> cartItemList = new List<HMR_Cart_Service.CartItem>{cartItem};
        
        Map<String, Object> cartDataMap = new Map<String, Object>();
        cartDataMap.put('cartItemList', cartItemList);
        cartDataMap.put('ECartItemsS', cartRecord);
               
        HMR_Cart_Service.CartRecord cartService = new HMR_Cart_Service.CartRecord(cartDataMap);    
        
        HMR_Coupon_Service couponService = new HMR_Coupon_Service();
        ccrz__E_Coupon__c result = couponService.getCouponFromHMRCouponCode(UserInfo.getUserId(), cartService, 'test-coupon');

        Test.startTest();
        //System.assert(couponRecord == null);
    } */
}