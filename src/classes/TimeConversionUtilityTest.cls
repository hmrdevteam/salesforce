/**
* @Date: 6/21/2017
* @Author: Ali Pierre (HMR)
* @JIRA: Issue https://reside.jira.com/browse/HPRP-3832
*/

@isTest(SeeAllData=False)
public with sharing class TimeConversionUtilityTest {
	static testMethod void testaddHours(){

        Test.startTest();

        System.assertEquals('3:00 PM', TimeConversionUtility.addHours('11:00 AM', 4));

        System.assertEquals('7:00 AM', TimeConversionUtility.addHours('11:00 AM', -4));

        System.assertEquals('11:00 AM', TimeConversionUtility.addHours('11:00 AM', -24));

        System.assertEquals('11:00 AM', TimeConversionUtility.addHours('11:00 AM', 24));

        System.assertEquals('11:00 PM', TimeConversionUtility.addHours('3:00 AM', -4));

        System.assertEquals('11:00 AM', TimeConversionUtility.addHours('3:00 PM', -4));

        System.assertEquals('13:00 PM', TimeConversionUtility.addHours('13:00 PM', 4), 'Test for incorrect format passed');

        Test.stopTest();
    }

    static testMethod void testisTimeConflict(){

        Test.startTest();

        System.assertEquals(True, TimeConversionUtility.isTimeConflict('11:45 AM', '12:15 PM'));

        System.assertEquals(True, TimeConversionUtility.isTimeConflict('12:00 AM', '11:15 PM'));

        System.assertEquals(False, TimeConversionUtility.isTimeConflict('3:15 PM', '3:15 AM'));

        System.assertEquals(False, TimeConversionUtility.isTimeConflict('3:45 PM', '7:00 PM'));

        System.assertEquals(False, TimeConversionUtility.isTimeConflict('1:30 PM', '12:45 AM'));

        System.assertEquals(False, TimeConversionUtility.isTimeConflict('13:00 PM', '13:00 PM'), 'Test for incorrect format passed');

        Test.stopTest();
    } 

    static testMethod void testgetSetOfTimes(){

        Test.startTest();

        Set<String> timeSet;

        timeSet = new Set<String>{'1:15 PM','1:30 PM','1:45 PM','2:00 PM','2:15 PM','2:30 PM', '2:45 PM'};

        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('2:00 PM'));

        timeSet = new Set<String>{'10:30 AM','10:45 AM','11:00 AM','11:15 AM','11:30 AM','11:45 AM', '12:00 PM'};

        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('11:15 AM'));

        timeSet = new Set<String>{'10:45 PM','11:00 PM','11:15 PM','11:30 PM','11:45 PM','12:00 AM', '12:15 AM'};

      	System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('11:30 PM'));

      	timeSet = new Set<String>{'12:00 AM','12:15 AM','12:30 AM','12:45 AM','1:00 AM','1:15 AM', '1:30 AM'};

        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('12:45 AM'));

        timeSet = new Set<String>{'7:00 AM','7:15 AM','7:30 AM','7:45 AM','8:00 AM','8:15 AM', '8:30 AM'};
       
        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('7:45 AM'));

        timeSet = new Set<String>{'13:45 PM'};

        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('13:45 PM'));

        timeSet = new Set<String>{'12:15 PM','12:30 PM','12:45 PM','1:00 PM','1:15 PM','1:30 PM', '1:45 PM'};

        System.assertEquals(timeSet, TimeConversionUtility.getSetOfTimes('1:00 PM'));

        Test.stopTest();
    }
}