/**
* Service Interface to get group data 
*
* @Date: 2018-02-27
* @Author: Zach Engman/Utkarsh Goswami (Mindtree)
* @Modified: 2018-02-28 - Zach Engman
* @JIRA:
*/
global with sharing class HMR_CMS_GroupData_ServiceInterface implements cms.ServiceInterface{
  /**
  *
  * @param params a map of parameters including at minimum a value for 'action'
  * @return a JSON-serialized response string
  */
    public String executeRequest(Map<String, String> params) {
     Map<String, Object> returnMap = new Map<String, Object>{'success' => true};
        String action = params.get('action');
        String userId = params.get('userId');
        if(!String.isBlank(userId)){
            HMR_GroupData_Service dataService = new HMR_GroupData_Service();
            try {
                
                //Return the calling function/action
                returnMap.put('action', action);
                
                if(action == 'getGroupDataForUser') {
                    
                    HMR_GroupData_Service.GroupData groupData = dataService.getGroupDataForUser(userId);

                    returnMap.put('success', groupData != null);
                    returnMap.put('groupData', groupData);
                }              
                else{
                    //Invalid Action
                    returnMap.put('success',false);
                    returnMap.put('message','Invalid Action');
                }
            }
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'No action provided');
        }

        return JSON.serialize(returnMap);
    }

    public static Type getType() {
        return HMR_CMS_GroupData_ServiceInterface.class;
    }
}