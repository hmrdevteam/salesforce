/**
* Apex Rest Resource for Program Selection
* @Date: 2018-01-16
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-01-29
* @JIRA:
*/
@RestResource(urlMapping='/mobile/program/*')
global with sharing class HMRMobProgram {
    /**
* method to get the program selections for the mobile application
* @return List<MobProgramDTO> - the list of DTO containing the program selection (flat structure) (HTTP 200 OK)
* @return HTTP/1.1 404 not found if the programs are unavailable
*/
    @HttpGet
    global static List<MobProgramDTO> doGet() {
        Integer count = 0;
        List<MobProgramDTO> programList = new List<MobProgramDTO>();
        RestRequest request = RestContext.request;
        List<String> dietTypeList = HMR_Picklist_Service.getPicklistValuesByObjectAndFieldApiName('Program_Membership__c', 'DietType__c');
        List<HMR_Mob_Program__mdt> hmrMobProgramList = [SELECT DietType__c, DisplayName__c, Message__c, ChildValues__c FROM HMR_Mob_Program__mdt];
        List<Program__c> remoteProgramList = [SELECT Name FROM Program__c WHERE RemoteProgram__c = false AND Name = 'Digital' LIMIT 1];
        
        if(dietTypeList.size() > 0 && remoteProgramList.size() > 0 && hmrMobProgramList.size() > 0){
            for (String dietType : dietTypeList) {
                for (HMR_Mob_Program__mdt hmrMobProgram : hmrMobProgramList) {
                    if (hmrMobProgram.DietType__c == dietType) {
                        programList.add(new MobProgramDTO(remoteProgramList[0], dietType, hmrMobProgram.DisplayName__c, hmrMobProgram.Message__c));
                    }else if(hmrMobProgram.DietType__c == 'Decision Free' && count == 0){
                        List<ChildValue> childValues = new List<ChildValue>();
                        String input = hmrMobProgram.ChildValues__c;
                        String[] valuePairs = input.split(',');
                        for (String pair : valuePairs) {
                            String[] parts = pair.split('-');
                            if (parts.size() == 2) {
								childValues.add(new ChildValue(parts[1].trim(), parts[0].trim()));
                            }
                        }
                        programList.add(new MobProgramDTO(
                            remoteProgramList[0], hmrMobProgram.DietType__c,
                            hmrMobProgram.DisplayName__c,
                            hmrMobProgram.Message__c,
                            childValues
                        ));
                        count++;
                    }
                }
            }
            for(Program_Diet_Type_Setting__mdt settingRecord : [SELECT Diet_Type__c, MasterLabel, Remote_Url__c, Sequence__c, Display_Name__c, Message__c FROM Program_Diet_Type_Setting__mdt ORDER BY Sequence__c]){
                if(String.isBlank(settingRecord.Remote_Url__c))
                    programList.add(new MobProgramDTO(remoteProgramList[0], settingRecord.Diet_Type__c, settingRecord.MasterLabel, settingRecord.Display_Name__c, settingRecord.Message__c));
                else
                    programList.add(new MobProgramDTO(settingRecord.MasterLabel, settingRecord.MasterLabel, settingRecord.Remote_Url__c));
            }
            RestContext.response.statuscode = 200;
            System.debug('programlist--'+programList);
            return programList;
        }
        else {
            RestContext.response.statuscode = 404;
            return null;
        }
    }
    
    /**
* global DTO class that represents a flat structure of the program records
*/
    global class MobProgramDTO {
        public String programId {get; set;}
        public String programName {get; set;}
        public String dietType {get; set;}
        public String displayName {get; set;}
        public string remoteUrl {get; set;}
        public string label{get;set;}
        public string message{get;set;}
        public List<ChildValue> childValues;
        
        public MobProgramDTO(Program__c programRecord, String dietType, String label, String message, List<ChildValue> childValues){
            this.programId = programRecord.Id;
            this.programName = programRecord.Name;
            this.dietType = dietType;
            this.label = label;
            this.message = message;
            this.childValues = childValues;
        }
        public MobProgramDTO(Program__c programRecord, String dietType, String label, String message){
            this.programId = programRecord.Id;
            this.programName = programRecord.Name;
            this.dietType = dietType;
            this.label = label;
            this.message = message;
        }
        public MobProgramDTO(Program__c programRecord, String dietType, string displayName, String label, String message){
            this.programId = programRecord.Id;
            this.programName = programRecord.Name;
            this.dietType = dietType;
            this.displayName = displayName;
            this.label = label;
            this.message = message;
        }
        public MobProgramDTO(String displayName, string label, string remoteUrl){
            this.displayName = displayName;
            this.label = label;
            this.remoteUrl = remoteUrl;
        }
    }
    public class ChildValue {
        public String dietType;
        public String label;
        public ChildValue(String dietType, String label) {
            this.dietType = dietType;
            this.label = label;
        }
    }
}