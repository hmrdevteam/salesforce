/**
* Apex Content Template Controller for rendering HMR Social Icons and Links inside Footer on OCMS Master
*
* @Date: 2017-03-12
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/

global virtual with sharing class HMR_CMS_FooterSocial_ContentTemplate extends cms.ContentTemplateController {	
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_FooterSocial_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_FooterSocial_ContentTemplate() {
        super();
    }
    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Title Label Property
    public String ConnectTitleText{    
        get{
            return getPropertyWithDefault('ConnectTitleText', 'Connect');
        }    
    }
    //Facebook Label Property
    public String FacebookText{    
        get{
            return getPropertyWithDefault('FacebookText', 'Facebook');
        }    
    }
    //Facebook Page Link Property
    public cms.Link FacebookLinkObj{
        get{
            return this.getPropertyLink('FacebookLinkObj');
        }
    }
    //Twitter Label Property
    public String TwitterText{    
        get{
            return getPropertyWithDefault('TwitterText', 'Twitter');
        }    
    }
    //Twitter Page Link Property
    public cms.Link TwitterLinkObj{
        get{
            return this.getPropertyLink('TwitterLinkObj');
        }
    }
    //YouTube Label Property
    public String YouTubeText{    
        get{
            return getPropertyWithDefault('YouTubeText', 'YouTube');
        }    
    }
    //YouTube Page Link Property
    public cms.Link YouTubeLinkObj{
        get{
            return this.getPropertyLink('YouTubeLinkObj');
        }
    }

    //global override getHTML - renders the social media list items for footer social nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        //init links URL var to hold Url's of the target page
        String facebookLinkUrl = '', twitterLinkUrl = '', youTubeLinkUrl = '';
        //check cms.Link properties for null and assign values to above var's
        facebookLinkUrl = (this.FacebookLinkObj != null) ? this.FacebookLinkObj.targetPage : '#';
        twitterLinkUrl = (this.TwitterLinkObj != null) ? this.TwitterLinkObj.targetPage : '#';
        youTubeLinkUrl = (this.YouTubeLinkObj != null) ? this.YouTubeLinkObj.targetPage : '#';
        html += '<h6 class="text-upper connect">' + ConnectTitleText + '</h6>' +            
                '<a href="' + facebookLinkUrl + '" class="social-first">' +
                    '<span class="hmr-icon-facebook" aria-hidden="true"></span>' +
                '</a>' +
                '<a href="' + twitterLinkUrl + '">' +
                    '<span class="hmr-icon-twitter" aria-hidden="true"></span>' +
                '</a>' +
                '<a href="' + youTubeLinkUrl + '">' +
                    '<span class="hmr-icon-youtube" aria-hidden="true"></span>' +
                '</a>' +
                '<hr class="visible-xs"/>';
        return html;
    }
}