/**
* Test Class Coverage of the HMR_CMS_RecipeHomeServiceInterface Class
*
* @Date: 03/26/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 03/26/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_RecipeHomeServiceInterface_Test {
    @isTest
    private static void testExecuteRequestWithRecipes(){
         HMR_Recipe__c hmrRecipe = new HMR_Recipe__c(Date_Published__c = Date.today(),
                                                     Recipe_Name__c = 'Test Recipe',
                                                     Fruit_Servings__c = 2,
                                                     VF_Servings__c = 1);
         insert hmrRecipe;
         List<Recipe__kav> recipeList = new List<Recipe__kav>{new Recipe__kav(Image_URI__c = 'https/assets/image.png',
                                                                              Template_Layout__c = 'TopBottom',
                                                                              UrlName = 'Test-Recipe01',
                                                                              Title = 'Test Recipe 01',
                                                                              Summary = 'Test Recipe 01 Summary',
                                                                              Language = 'en_US',
                                                                              Recipe__c = hmrRecipe.Id),
            												 new Recipe__kav(Image_URI__c = 'https/assets/image.png',
                                                                              Template_Layout__c = 'LeftRight',
                                                                              UrlName = 'Test-Recipe02',
                                                                              Title = 'Test Recipe 02',
                                                                              Summary = 'Test Recipe 02 Summary',
                                                                              Language = 'en_US',
                                                                              Recipe__c = hmrRecipe.Id)};
        insert recipeList;

        Test.startTest();
        
        String result = HMR_CMS_RecipeHomeServiceInterface.getCardListHTML(recipeList);
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testExecuteRequestWithSingleTopBottomRecipe(){
         HMR_Recipe__c hmrRecipe = new HMR_Recipe__c(Date_Published__c = Date.today(),
                                                     Recipe_Name__c = 'Test Recipe',
                                                     Fruit_Servings__c = 2,
                                                     VF_Servings__c = 1);
         insert hmrRecipe;
         List<Recipe__kav> recipeList = new List<Recipe__kav>{new Recipe__kav(Image_URI__c = 'https/assets/image.png',
                                                                              Template_Layout__c = 'TopBottom',
                                                                              UrlName = 'Test-Recipe01',
                                                                              Title = 'Test Recipe 01',
                                                                              Summary = 'Test Recipe 01 Summary',
                                                                              Language = 'en_US',
                                                                              Recipe__c = hmrRecipe.Id)};
        insert recipeList;

        Test.startTest();
        
        String result = HMR_CMS_RecipeHomeServiceInterface.getCardListHTML(recipeList);
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testExecuteRequestWithSingleLeftRightRecipe(){
         HMR_Recipe__c hmrRecipe = new HMR_Recipe__c(Date_Published__c = Date.today(),
                                                     Recipe_Name__c = 'Test Recipe',
                                                     Fruit_Servings__c = 2,
                                                     VF_Servings__c = 1);
         insert hmrRecipe;
         List<Recipe__kav> recipeList = new List<Recipe__kav>{new Recipe__kav(Image_URI__c = 'https/assets/image.png',
                                                                              Template_Layout__c = 'LeftRight',
                                                                              UrlName = 'Test-Recipe01',
                                                                              Title = 'Test Recipe 01',
                                                                              Summary = 'Test Recipe 01 Summary',
                                                                              Language = 'en_US',
                                                                              Recipe__c = hmrRecipe.Id)};
        insert recipeList;

        Test.startTest();
        
        String result = HMR_CMS_RecipeHomeServiceInterface.getCardListHTML(recipeList);
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testExecuteRequestWithNoRecipes(){
        HMR_CMS_RecipeHomeServiceInterface controller = new HMR_CMS_RecipeHomeServiceInterface();
 
        Test.startTest();
        
        String result = controller.executeRequest(new Map<String, String>{'action' => 'getRecipeCards'});
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
	@isTest
    private static void testExecuteRequestWithNoAction(){
        HMR_CMS_RecipeHomeServiceInterface controller = new HMR_CMS_RecipeHomeServiceInterface();
 
        Test.startTest();
        
        String result = controller.executeRequest(new Map<String, String>());
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testExecuteRequestWithException(){
        HMR_CMS_RecipeHomeServiceInterface controller = new HMR_CMS_RecipeHomeServiceInterface();
 
        Test.startTest();
        
        String result = controller.executeRequest(null);
        
        Test.stopTest();
        
        //Verify the string result was still returned
        System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testGetType(){
        Test.startTest();
        
        Type result = HMR_CMS_RecipeHomeServiceInterface.getType();

        Test.stopTest();
        
        //Verify the correct type was returned
        System.assertEquals(HMR_CMS_RecipeHomeServiceInterface.class, result);
    }
}