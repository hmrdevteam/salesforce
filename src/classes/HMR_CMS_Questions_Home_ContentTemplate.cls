/**
* Apex Content Template Controller for the Questions Home page
*
* @Date: 07/18/2017
* @Author Ali Pierre (HMR)
* @Modified:
* @JIRA: 
*/
global virtual class HMR_CMS_Questions_Home_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_Questions_Home_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_Questions_Home_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML(){
    	String sitePrefix = Site.getBaseUrl(); //Temporary Until Content Links Established
        
        List<TopicRecord> topicRecordList = getTopicRecordList();

        
    	String html =   '<div class="questions-home container">' +           
                            '<div class="row">' +
                                '<div class="post-list-header col-xs-12">' +
                                    '<div class="col-xs-8 categories">' +
                                        '<span class="text-16">Categories</span>' +
                                    '</div>' + 
                                    '<div class="col-xs-2 hidden-xs text-right no-padding">' +
                                        '<span class="text-16">Posts</span>' +
                                    '</div>' +
                                    '<div class="col-xs-2 hidden-xs text-right no-padding">' +
                                        '<span class="text-16">Replies</span>' +
                                    '</div>' +
                                    '<div class="clearfix"></div>' +
                                '</div>';            

			                    for(TopicRecord tr : topicRecordList){			                    				                    	
			                    	html += '<a href="' + sitePrefix + '/' + tr.topicURL + '">'+
                                        '<div class="post-list col-xs-12">' +
                                            //<!-- Media middle -->
                                            '<div class="media">' +
                                                '<div class="row">' +
                                                    '<h4 class=" col-xs-12">' + tr.name + '  <img class="icon" src="' + tr.img + '" /></h4>'+
                                                    '<p class="col-xs-12 col-sm-8">' + tr.description + '</p>'+
                                                    '<div class="media-footer row">' +
                                                        '<div class="hidden-xs col-sm-4">' +
                                                            '<span class="text-20 topic-number text-left col-xs-6" id="">' +
                                                                '<span class="mobile-topic">Posts</span>' + tr.talkingAbout + '</span>' +
                                                            '<span class="text-20 post-number col-xs-6" id="">' +
                                                                '<span class="mobile-post">Replies</span> ' + tr.commentCount + '</span>' +
                                                        '</div>' +
                                                        '<div class="visible-xs-block col-xs-12">' +
                                                            '<div class="col-xs-12">' +
                                                                '<span class="topic-number text-left" id="">' +
                                                                    '<span class="mobile-topic">Posts</span> ' + tr.talkingAbout + '</span>' +
                                                                '<span class="post-number" id="">' +
                                                                    '<span class="mobile-post">Replies</span> ' + tr.commentCount + '</span>' +
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</a>';
			                    } 
    			            html  += '</div>'+
                        '</div>';
    	return html;
    }

    public List<TopicRecord> getTopicRecordList(){
        List<TopicRecord> topicRecordList = new List<TopicRecord>();
        HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();
        HMR_Topic_Service topicService = new HMR_Topic_Service();
        Map<String, String> imgURLMap = dataCategoryService.getTopicIconMap();
        
        //getFirstLevelCategoryLabelList
        //We have to do multiple calls as we just want our specific parent topics: can't use ConnectApi.Topics.getTopics(null).topics
        for(Category_Map__mdt cat : dataCategoryService.getTopicList()){
            //Our DataCategories Match our Topics
            ConnectApi.Topic topic = topicService.getTopicByName(cat.Topic_Name__c);
            if(topic != null){
                Integer postCount = 5;
                if(!Test.isRunningTest()){
                    postCount = topicService.getTopicPostsCount(topic.Id);
                }
                topicRecordList.add(new TopicRecord(topic, imgURLMap, cat.Topic_Description__c, cat.Url_Resource_Name__c, postCount));     
            }
        }  
        
        return topicRecordList;
    }

    public with sharing class TopicRecord{
        public string id {get; set;}
        public string name {get; set;}
        public string description {get; set;}
        public integer commentCount {get; set;}
        public integer talkingAbout {get; set;}
        public string img {get; set;}
        public string topicURL {get; set;}
        
        public TopicRecord(ConnectApi.Topic topic, Map<String, String> imgURLMap, String description, String topicResourceURL, Integer posts){
                        
            this.id = topic.Id;
            this.name = topic.Name;
            this.description = description != null ? description : topic.Name;
            this.talkingAbout = posts;
            this.commentCount = getCommentCount(topic.Name);
            this.img = imgURLMap.get(topic.Name) != null ? imgURLMap.get(topic.Name) : imgURLMap.get('Getting Started');
            this.topicURL = topicResourceURL != null ? 'resources/forum/' + topicResourceURL.replace('Questions-','').toLowerCase() : 'forum';
        }

        public Integer getCommentCount(String topicAssignName){
            List<TopicAssignment> ta = [SELECT EntityId, CreatedDate FROM TopicAssignment WHERE Topic.Name = :topicAssignName ORDER BY CreatedDate DESC LIMIT 999];
            
            Set<String> tIds = new Set<String>();

            for(TopicAssignment fItem : ta){
                tIds.add(fItem.EntityId);
            }

            List<FeedItem> fiList = [SELECT Id, CommentCount FROM FeedItem WHERE Id IN :tIds];
            
            Integer comments = 0;

            if(!fiList.isEmpty()){                
                for(FeedItem fi : fiList){
                    if(fi.CommentCount > 0){
                        comments += fi.CommentCount;
                    }
                }
            }
            return comments;
        }        
    }
}