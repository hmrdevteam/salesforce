/**
* Controller for the Article Preview page
* used from a hyperlink formula field on the Blog_Post__kav article type
* @Date: 2017-08-18
* @Author: Nathan Anderson (Magnet 360)
* @Modified: Nathan Anderson 2017-08-24
* @JIRA:
*/

public with sharing class HMR_ArticlePreviewController {

	public String articleName {get;set;}

	public HMR_ArticlePreviewController() {

		//get the Article name from the page
		articleName = ApexPages.currentPage().getParameters().get('name');

        //Try to get from url if empty
        if(String.isBlank(articleName)){
            List<String> urlPartList = ApexPages.currentPage().getURL().split('/');
            if(urlPartList.size() > 0)
                articleName = urlPartList[urlPartList.size() - 1].split('\\?')[0];
        }

        articleName = String.escapeSingleQuotes(articleName);


	}

	//Call the HMR Knowledge Service to retrieve full article details
	public HMR_Knowledge_Service.ArticleRecord getArticleRecord() {
		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

		if(!String.isBlank(articleName)){
			//set the publishStatus variable to draft to make sure that we retrieve the current working draft version first
			knowledgeService.publishStatus = 'draft';

			HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);

			//if no draft was returned, set the status to online and retrieve the published version
			if(articleRecord == null) {
				knowledgeService.publishStatus = 'online';

				articleRecord = knowledgeService.getByName(articleName);
			}

			return articleRecord;

		}

		return null;

	}
}