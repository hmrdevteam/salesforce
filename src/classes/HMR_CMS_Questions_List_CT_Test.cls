/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_Questions_List_ContentTemplate
*/

@isTest(SeeAllData=true)  //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_Questions_List_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_Questions_List_ContentTemplate recipeDetailTemplate = new HMR_CMS_Questions_List_ContentTemplate();
    String propertyValue = recipeDetailTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    recipeDetailTemplate.testAttributes = tab;

    string s2 = recipeDetailTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = recipeDetailTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');

    string topicName = recipeDetailTemplate.topicName;

	ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
    List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
    testItemList.add(new ConnectApi.FeedItem());
    testItemList.add(new ConnectApi.FeedItem());
    testPage.elements = testItemList;

    String communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;

    HMR_Topic_Service topicService = new HMR_Topic_Service();
    string topicId = topicService.getTopicId('Eating Well');
    //system.debug('+++++^^'+communityId);
	HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
 	ConnectApi.FeedElement fe = connectApiService.postQuestionAndAnswer(topicId, 'subject', 'body');
 	ConnectApi.FeedElement fe2 = connectApiService.postQuestionAndAnswer(topicId, 'subject2', 'body2');

    ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topicId, testPage);

    String renderHTML = recipeDetailTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));
	HMR_CMS_Questions_List_ContentTemplate.QuestionAnswerResult qar= new  HMR_CMS_Questions_List_ContentTemplate.QuestionAnswerResult(fe);

    try{
        HMR_CMS_Questions_List_ContentTemplate qc = new HMR_CMS_Questions_List_ContentTemplate(null);
    }catch(Exception e){}
    try{
        HMR_CMS_Questions_List_ContentTemplate qc2 = new HMR_CMS_Questions_List_ContentTemplate(1, 'tpc');
    }catch(Exception e){}
  }
  
}