global class HMR_CMS_CCRZ_Cart_ServiceInterface implements cms.ServiceInterface{
	public HMR_CMS_CCRZ_Cart_ServiceInterface() {}

	/**
     * executeRequest --> method to execute request
     * @param params a map of parameters including
     * @return a JSON-serialized response string
     */
    global String executeRequest(Map<String, String> params) {
		Map<String, Object> returnMap = new Map<String, Object>();

		if(params.get('userId') != null && params.get('isGuest') != null){
            string userId = String.escapeSingleQuotes(params.get('userId'));
            Boolean isGuest = Boolean.valueOf(params.get('isGuest'));
            HMR_Cart_Service cartService = new HMR_Cart_Service(userId, isGuest);

        	try {
                String action = params.get('action');
                returnMap.put('success', true);
                //Return the calling function/action
                returnMap.put('action', action);

                if(action == 'createCart') {
                    string encryptedCartId = String.escapeSingleQuotes(cartService.createEmptyCart());
                    returnMap.put('encryptedCartId', encryptedCartId);
                }
                else if(action == 'addDynamicKitToCart'){
                    string encryptedCartId = params.get('encryptedCartId');
                    string sku = String.escapeSingleQuotes(params.get('sku'));
                    string jsonItemList = params.get('itemList');
                    boolean coachingSelection = Boolean.valueOf(params.get('coachingSelection'));                    

                    if(String.isBlank(encryptedCartId))
                        encryptedCartId = cartService.createEmptyCart();
                    else
                        encryptedCartId = String.escapeSingleQuotes(encryptedCartId);

                    Map<String, Object> kitReturnMap = cartService.addDynamicKitToExistingCart(encryptedCartId, sku, jsonItemList);

                    returnMap.put('success', (boolean) kitReturnMap.get('success'));
                    returnMap.put('kitPrice', (decimal) kitReturnMap.get('kitPrice'));
                    returnMap.put('encryptedCartId', encryptedCartId);

                    if(coachingSelection){
                       returnMap.put('coachingAdded', cartService.setCoachingSelection(encryptedCartId, coachingSelection)); 
                    }                    
                    else{
                       returnMap.put('coachingAdded', false);  
                    }

                }
                else if(action == 'addToCart'){
                    string encryptedCartId = params.get('encryptedCartId');
                    string sku = String.escapeSingleQuotes(params.get('sku'));
                    decimal quantity = Decimal.valueOf(params.get('quantity'));
                    decimal price = Decimal.valueOf(params.get('price'));

                    if(String.isBlank(encryptedCartId))
                        encryptedCartId = cartService.createEmptyCart();
                    else
                        encryptedCartId = String.escapeSingleQuotes(encryptedCartId);
        
                    returnMap.put('success', cartService.addItemToExistingCart(encryptedCartId, sku, quantity, price));
                    returnMap.put('encryptedCartId', encryptedCartId);
                }
                else if(action == 'addKitToCart'){
                    string encryptedCartId = params.get('encryptedCartId');
                    string sku = String.escapeSingleQuotes(params.get('sku'));

                    if(String.isBlank(encryptedCartId))
                        encryptedCartId = cartService.createEmptyCart();
                    else
                        encryptedCartId = String.escapeSingleQuotes(encryptedCartId);

                    cartService.addKitToExistingCart(encryptedCartId, sku);
                    returnMap.put('encryptedCartId', encryptedCartId);
                    returnMap.put('success', !String.isBlank(encryptedCartId));
                }
                else if(action == 'adjustDiscount'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    decimal adjustmentAmount = Decimal.valueOf(params.get('adjustment'));

                    returnMap.put('success', cartService.applyManualAdjustment(encryptedCartId, adjustmentAmount));
                }
                else if(action == 'applyCoupon'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    string couponCode = String.escapeSingleQuotes(params.get('couponCode'));
                    boolean result = cartService.addCouponCode(encryptedCartId, couponCode);
                    returnMap.put('success', result);
                }
                else if(action == 'checkForAutomaticCoupon'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    HMR_Cart_Service.CartRecord cartRecord = cartService.checkForAutomaticCoupon(encryptedCartId);
                    
                    if(params.get('redirectUrl') != null)
                        returnMap.put('redirectUrl', String.escapeSingleQuotes(params.get('redirectUrl')));

                    returnMap.put('cart', cartRecord);
                    returnMap.put('success', cartRecord != null);
                }
                else if(action == 'checkForExpiredCoupon'){
                    string encryptedCartId = params.get('encryptedCartId');
                    boolean result = cartService.checkForExpiredCoupon(encryptedCartId);
                    returnMap.put('success', result);
                }
                else if(action == 'getCartByEncryptedId'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    HMR_Cart_Service.CartRecord cartRecord = cartService.getCartByEncryptedId(encryptedCartId);

                    returnMap.put('cart', cartRecord);
                    returnMap.put('encryptedCartId', encryptedCartId);
                    returnMap.put('success', cartRecord != null);
                }
                else if(action == 'getCartByUser'){
                    HMR_Cart_Service.CartRecord cartRecord = cartService.getCartByUserId(userId);
                    returnMap.put('cart', cartRecord);
                    if(cartRecord != null)
                        returnMap.put('encryptedCartId', cartRecord.encryptedId);
                    returnMap.put('success', cartRecord != null);
                }
                else if(action == 'priceCart'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    returnMap.put('success', cartService.priceCart(encryptedCartId));
                }
                else if(action == 'removeCoupon'){
                    string encryptedCartId = params.get('encryptedCartId');
                    boolean result = cartService.removeCouponCode(encryptedCartId);
                    returnMap.put('success', result);
                }
                else if(action == 'removeFromCart'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    string sfId = String.escapeSingleQuotes(params.get('sfId'));

                    cartService.removeItemFromCart(encryptedCartId, sfId);
                }
                else if(action == 'setCoachingSelection'){
                    string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                    boolean coachingSelection = Boolean.valueOf(params.get('coachingSelection'));

                    returnMap.put('success', cartService.setCoachingSelection(encryptedCartId, coachingSelection));
                }
                else if(action == 'updateCart'){
                	string encryptedCartId = String.escapeSingleQuotes(params.get('encryptedCartId'));
                	string jsonItemList = params.get('itemList');

                	returnMap.put('success', cartService.updateCart(encryptedCartId, jsonItemList)); 
                }
                else{
                    //Invalid Action
                    returnMap.put('success',false);
                    returnMap.put('message','Invalid Action');
                } 
            } 
            catch(Exception e) {
                // Unexpected Error
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMap.put('success',false);
                returnMap.put('message',JSON.serialize(message));
                returnMap.put('stack', e.getStackTraceString());
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('message', 'userId and isGuest parameters required');
        }

		return JSON.serialize(returnMap);
	}

	/**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_CCRZ_Cart_ServiceInterface.class;
    }
}