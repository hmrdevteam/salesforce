/**
 * Test class for ProgramMembershipTriggerHandler class
 *
     * @Date: 11/28/2016
     * @Author: Ali Pierre (HMR)
     * @JIRA: https://reside.jira.com/browse/HPRP-577
	*/
//SetAllData = False in order to use 100% test data
@isTest(SeeAllData=False)
Private class ProgramMembershipTriggerHandlerTest {
	//Create a new list to store program memberships
	public static List<Program_Membership__c> prgMembersList;

    //init() method creates and processes test data
    static void init(){
        //Call method to create test data
    	CreateRecords();
        //Retrieve Program Memberships with an 'Active' status that were created today
    	prgMembersList = new List<Program_Membership__c>([SELECT Id,Program__c, hmr_Contact__c,ProgramName__c, hmr_Client_Type__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active' AND hmr_Enrollment_Date__c = :Date.today()]);
        	//If prgMembersList is not empty loop through list and update each records' New Phase value according to the conditions
        	if(!prgMembersList.isEmpty()){
               	//Loop counter
            	Integer times = 1;
           		for(Program_Membership__c p : prgMembersList){
                    //First Phase 2 record within list will be placed in P1 HSAH
               		If(p.ProgramName__c == 'Phase 2' && times == 1){
                  		p.New_Phase__c = 'P1 Healthy Solutions';
                    //All other Phase 2 records within list will be placed in P1 HSS
               		}else if(p.ProgramName__c == 'Phase 2'){
               	   	 	p.New_Phase__c = 'P1 Healthy Shakes';
                    //P1 HSS will be dropped
               		}else if(p.ProgramName__c == 'P1 Healthy Shakes'){
               	 	  	p.New_Phase__c = 'Drop';
                    //P1 HSAH will be placed in Phase 2
               		}else if(p.ProgramName__c == 'P1 Healthy Solutions'){
               	    	p.New_Phase__c = 'Phase 2';
                    //P1 HSS will be sent to P1 HSAH
              	 	}else if(p.ProgramName__c == 'P1 Healthy Shakes' ){
               	   	 	p.New_Phase__c = 'P1 Healthy Solutions';
               		}
                    //add 1 to loop counter
               		times = times + 1;
        		}

        	}


    }
    //Method to create all test Data
    static testMethod void CreateRecords() {

        //Declare variable to store today's date
        Date dt = Date.today();

        //Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Create a Coach User
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        //Insert User
        insert userObj;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');

        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();

        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj1);

        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj2);

        Contact testContatObj3 = new Contact(FirstName = 'P2', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj3);

        Contact testContatObj4 = new Contact(FirstName = 'P2R', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj4);

        Contact testContatObj5 = new Contact(FirstName = 'P1HSSR', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj5);

        Contact testContatObj6 = new Contact(FirstName = 'P1HSSRcls', LastName = 'contact', Account = testAccountObj);
        conList.add(testContatObj6);

        //List of contacts
        insert conList;

		//Insert a valid CC Term record
		ccrz__E_Term__c ccTerm = new ccrz__E_Term__c(ccrz__Title__c='HMR Terms', ccrz__Enabled__c=true, Consent_Type__c='HMR Terms & Conditions',
                                                     Version_Number__c=1, ccrz__Locale__c='en_US', ccrz__Description__c='Test', ccrz__Storefront__c='DefaultStore');

		insert ccTerm;

		//Declare a new list to store contacts
		List<Consent_Agreement__c> consentList = new List<Consent_Agreement__c>();

        Consent_Agreement__c consentObj1 = new Consent_Agreement__c(Contact__c=testContatObj1.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj1);

		Consent_Agreement__c consentObj2 = new Consent_Agreement__c(Contact__c=testContatObj2.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj2);

		Consent_Agreement__c consentObj3 = new Consent_Agreement__c(Contact__c=testContatObj3.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj3);

		Consent_Agreement__c consentObj4 = new Consent_Agreement__c(Contact__c=testContatObj4.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj4);

		Consent_Agreement__c consentObj5 = new Consent_Agreement__c(Contact__c=testContatObj5.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj5);

		Consent_Agreement__c consentObj6 = new Consent_Agreement__c(Contact__c=testContatObj6.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
		consentList.add(consentObj6);

        insert consentList;

        //Create Chatter Group
		CollaborationGroup Enrollment = new CollaborationGroup(Name = 'Enrollment',CollaborationType = 'Public');

        insert Enrollment;

        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);

        //Insert Coach
        insert coachObj;

        //Declare new List of Programs
        List<Program__c> prgList = new List<Program__c>();

        //Create HMR Programs and add them to prgList to be inserted into Database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(programObj1);

        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        prgList.add(programObj2);

        Program__c programObj3 = new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c =  true);
        prgList.add(programObj3);

        //Insert Programs
        insert prgList;

        //Declare a new List of Class object
        List<Class__c> clsList = new List<Class__c>();
        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Class_Name__c = 'testing P1 Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj1);

        Class__c classObj2 = new Class__c(hmr_Class_Name__c = 'testing P2 Class', hmr_Coach__c = coachObj.id, Program__c = programObj3.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P2 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Afternoon',hmr_Start_Time__c = '3:00 PM', hmr_End_Time__c = '4:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj2);

        //Insert Classes
        insert clsList;

        //Declare a new List of Coaching Session object
        List<Coaching_Session__c> cSessionList = new List<Coaching_Session__c>();

        //Create Coaching Sessions to be added to cSessionList and inserted into the Database
        Coaching_Session__c coachingSessObj1 = new Coaching_Session__c(hmr_Class__c = classObj1.id, hmr_Coach__c = coachObj.Id, hmr_Class_Date__c = dt.addDays(1));
        cSessionList.add(coachingSessObj1);

        Coaching_Session__c coachingSessObj2 = new Coaching_Session__c(hmr_Class__c = classObj2.id, hmr_Coach__c = coachObj.Id, hmr_Class_Date__c = dt.addDays(1));
        cSessionList.add(coachingSessObj2);
        //Insert Coaching Sessions
        insert cSessionList;

        //Declare a new List of Program Membership object
        List<Program_Membership__c> prgMemList = new List<Program_Membership__c>();

        //Create Program Memberships to be added to prgMemList and inserted into the Database
        Program_Membership__c prgMemObj1 = new Program_Membership__c(Program__c = programObj1.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj1.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj1);

        Program_Membership__c prgMemObj2 = new Program_Membership__c(Program__c = programObj2.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj5.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj2);

        Program_Membership__c prgMemObj3 = new Program_Membership__c(Program__c = programObj3.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj3.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj3);

        Program_Membership__c prgMemObj4 = new Program_Membership__c(Program__c = programObj2.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj3.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj4);

        Program_Membership__c prgMemObj5 = new Program_Membership__c(Program__c = programObj3.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj4.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj5);

        Program_Membership__c prgMemObj6 = new Program_Membership__c(Program__c = programObj1.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj5.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj6);

        Program_Membership__c prgMemObj7 = new Program_Membership__c(Program__c = programObj1.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj6.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj7);

        Program_Membership__c prgMemObj8 = new Program_Membership__c(Program__c = programObj1.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj5.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj8);

        Program_Membership__c prgMemObj9 = new Program_Membership__c(Program__c = programObj3.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj3.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj9);

        Program_Membership__c prgMemObj10 = new Program_Membership__c(Program__c = programObj3.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj4.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj10);

        Program_Membership__c prgMemObj11 = new Program_Membership__c(Program__c = programObj1.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj1.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj11);

        Program_Membership__c prgMemObj12 = new Program_Membership__c(Program__c = programObj2.id, hmr_Status__c = 'Active',hmr_Contact__c = testContatObj2.id, hmr_Enrollment_Date__c = Date.today());
        prgMemList.add(prgMemObj12);

        Program_Membership__c prgMemObj13 = new Program_Membership__c(Program__c = programObj2.id, hmr_Status__c = 'Transitioned',hmr_Contact__c = testContatObj3.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj13);

        //Insert Program Memberships
        insert prgMemList;

        //Declare a new List of Class Member object
        List<Class_Member__c> clsMemList = new List<Class_Member__c>();

        //Create Class Memberships to be added to clsMemList and inserted into the Database
        Class_Member__c clsMemObj1 = new Class_Member__c(hmr_Class__c = classObj1.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj2.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj1);

        Class_Member__c clsMemObj2 = new Class_Member__c(hmr_Class__c = classObj2.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj3.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj2);

        Class_Member__c clsMemObj3 = new Class_Member__c(hmr_Class__c = classObj1.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj4.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj3);

        Class_Member__c clsMemObj4 = new Class_Member__c(hmr_Class__c = classObj1.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj5.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj4);

        Class_Member__c clsMemObj5 = new Class_Member__c(hmr_Class__c = classObj1.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj1.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj5);

        Class_Member__c clsMemObj6 = new Class_Member__c(hmr_Class__c = classObj1.id,  hmr_Active__c = True ,hmr_Contact_Name__c = testContatObj6.id, hmr_Start_Date__c = Date.today());
        clsMemList.add(clsMemObj6);

        //Insert Class Memberships
        insert clsMemList;

        //Declare a new List of Session Attendee object
        List<Session_Attendee__c> AttList = new List<Session_Attendee__c>();

        //Create Session Atendee records to be added to AttList and inserted into the Database
        Session_Attendee__c AttObj1 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj1.Id, hmr_Coaching_Session__c = coachingSessObj1.Id ,hmr_Client_Name__c = testContatObj2.id);
        AttList.add(AttObj1);

        Session_Attendee__c AttObj2 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj2.Id, hmr_Coaching_Session__c = coachingSessObj2.Id ,hmr_Client_Name__c = testContatObj3.id);
        AttList.add(AttObj2);

        Session_Attendee__c AttObj3 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj3.Id, hmr_Coaching_Session__c = coachingSessObj1.Id ,hmr_Client_Name__c = testContatObj4.id);
        AttList.add(AttObj3);

        Session_Attendee__c AttObj4 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj4.Id, hmr_Coaching_Session__c = coachingSessObj1.Id ,hmr_Client_Name__c = testContatObj5.id);
        AttList.add(AttObj4);

        Session_Attendee__c AttObj5 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj5.Id, hmr_Coaching_Session__c = coachingSessObj1.Id ,hmr_Client_Name__c = testContatObj6.id);
        AttList.add(AttObj5);

        Session_Attendee__c AttObj6 = new Session_Attendee__c(hmr_Class_Member__c = clsMemObj6.Id, hmr_Coaching_Session__c = coachingSessObj1.Id ,hmr_Client_Name__c = testContatObj1.id);
        AttList.add(AttObj6);

        //Insert Sesson Attendee records
        insert AttList;
    }

    // Main Test Method
    static testMethod void testProgram_Membership() {
        //init() will create and process updates to created Program Membership records
        init();
        //Start Test
        Test.startTest();
        //Update prgMembersList which will now contain new and updated records
        update prgMembersList;
        //Declare Id set for program membership records
        Set<Id> pMem = new set<Id>();
       	//Loop through prgMembersList and add Ids to pMem set
        for(Program_Membership__c p : prgMembersList){
            pMem.add(p.Id);
        }
        //Query the updated Program Memberships into a new list
        List<Program_Membership__c> MembersList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c, hmr_Status_Changed_Date__c FROM Program_Membership__c WHERE Id IN :pMem]);

        //Loop through the Program Memberships to check that their status is no longer 'Active' and their Status Changed Date = Today
        for(Program_Membership__c m : MembersList){
            System.assertNotEquals(m.hmr_Status__c,'Active');
            System.assertEquals(m.hmr_Status_Changed_Date__c,Date.today());
        }
        //Stop Test
        Test.stopTest();

    }


}