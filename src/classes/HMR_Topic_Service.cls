/**
* Apex Service Class for Topics
*
* @Date: 2017-05-08
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-08-01
* @JIRA: 
*/
public with sharing class HMR_Topic_Service {
    @testVisible
    private string communityId = '';
    
    public HMR_Topic_Service(){
        this.communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;
    }
    
    public List<Category_Map__mdt> getTopicList(){
        List<Category_Map__mdt> dataTopicList = [SELECT MasterLabel
                                                 ,DeveloperName
                                                 ,Data_Category_Name__c
                                                 ,Topic_Name__c
                                                 ,Icon1__c
                                                 ,Icon2__c 
                                                 ,Url_Resource_Name__c
                                                 ,Topic_Description__c
                                                 ,Sort_Order__c
                                                 FROM Category_Map__mdt 
                                                 WHERE Topic_Name__c  <> '' 
                                                 ORDER BY Sort_Order__c];
        return dataTopicList;
    }
    
    public Map<String, Category_Map__mdt> getTopicMap(){
        Map<String, Category_Map__mdt> topicMap = new Map<String, Category_Map__mdt>();
        
        for(Category_Map__mdt cm: getTopicList())
            topicMap.put(cm.Topic_Name__c, cm);
        
        return topicMap;
    }
    
    public ConnectApi.Topic getTopicByName(string topicName){
        ConnectApi.TopicPage topicResultPage = ConnectApi.Topics.getTopics(communityId, topicName, true);
        ConnectApi.Topic topicResult;
        
        if(topicResultPage.topics.size() > 0)
            topicResult = topicResultPage.topics[0];
        
        return topicResult;
    }
    
    public Set<String> suggestedPosts(Set<String> feedIdSet){
        Set<String> suggestedPostIds = new Set<String>();
        List<TopicAssignment> taList = [SELECT EntityId FROM TopicAssignment WHERE EntityId IN :feedIdSet AND Topic.Name = 'HMR Suggested'];
        
        if(!taList.isEmpty()){
            for(TopicAssignment ta : taList){
                suggestedPostIds.add(ta.EntityId);
            }
        }        
        return suggestedPostIds;
    }
    
    public Integer getTopicPostsCount(string topicId){
        ConnectApi.FeedElementPage resultPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(communityId,ConnectApi.FeedType.Topics, topicId, '', 10, null);          
        
        return resultPage.elements.size() > 0 ? resultPage.elements.size() : 0;
    }
    
    public string getTopicId(string topicName){
        ConnectApi.Topic topicResult = getTopicByName(topicName);
        
        return topicResult == null ? null : topicResult.Id;
    }
}