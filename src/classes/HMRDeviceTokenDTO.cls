/**
 * This class represents a user's device token.  The token is stored in an array in json format in 
 * the Contact.hmr_Diet_Tracker_Token__c field. This token is used for push notifications using 
 * firebase cloud messaging REST api
 * @author Javier Arroyo
 * @see HMRPushNotificationJob
 * @see HMRMobDeviceToken
 */
global class HMRDeviceTokenDTO {
    
    public String os {get; set;}
    public String deviceType {get; set;}
    public String token {get; set;}
    
    public HMRDeviceTokenDTO(String os, String deviceType, String token) {
        this.os = os;
        this.deviceType = deviceType;
        this.token = token;
    }
    
    public static List<HMRDeviceTokenDTO> getTestTokens() {
        List<HMRDeviceTokenDTO> tokens = new List<HMRDeviceTokenDTO>();
        tokens.add( new HMRDeviceTokenDTO('iOS', 'iPad', 't1') );
        tokens.add( new HMRDeviceTokenDTO('android', 'Galaxy', 't2') );
		return tokens;
    } 
    public static List<HMRDeviceTokenDTO> getTestTokenIos() {
        List<HMRDeviceTokenDTO> tokens = new List<HMRDeviceTokenDTO>();
        tokens.add( new HMRDeviceTokenDTO('iOS', 'iPad', 't1') );
		return tokens;
    }  
    public static List<HMRDeviceTokenDTO> getTestTokenAndroid() {
        List<HMRDeviceTokenDTO> tokens = new List<HMRDeviceTokenDTO>();
        tokens.add( new HMRDeviceTokenDTO('android', 'Galaxy', 'tokeng') );
		return tokens;
    }      
}


    /*
    [
      {
        "token": "t1",
        "os": "iOS",
        "deviceType": "iPad"
      },
      {
        "token": "t2",
        "os": "android",
        "deviceType": "Galaxy"
      }
    ]
    */