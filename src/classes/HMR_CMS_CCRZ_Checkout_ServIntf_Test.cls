/**
* Test Class Coverage of the HMR_CMS_CCRZ_Checkout_ServiceInterface Class
*
* @Date: 01/03/2018
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 01/24/2018 - Z. Engman
* @JIRA:
*/
@isTest
private class HMR_CMS_CCRZ_Checkout_ServIntf_Test {
    
    @testSetup
    private static void setupTestData(){
        cc_dataFactory.setupTestUser();
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.getContactAddress();
        insert contactAddressRecord;
        cc_dataFactory.setupCatalog();
        cc_dataFactory.createCart();
        
        List<ccrz__E_Term__c> termList = new List<ccrz__E_Term__c>{new ccrz__E_Term__c(ccrz__Description__c = 'Consent Agreement',
                                                                                       ccrz__Enabled__c = true,
                                                                                       ccrz__Title__c = 'Consent',
                                                                                       ccrz__Type__c = 'Digital',
                                                                                       Consent_Type__c = 'Auto-Delivery',
                                                                                       Version_Number__c = 1),
                                                                    new ccrz__E_Term__c(ccrz__Description__c = 'Consent Agreement',
                                                                                        ccrz__Enabled__c = true,
                                                                                        ccrz__Title__c = 'Consent',
                                                                                        ccrz__Type__c = 'Digital',
                                                                                        Consent_Type__c = 'Health',
                                                                                        Version_Number__c = 1)};
        
        insert termList;
        
        Program__c programRecord = new Program__c(hmr_Phase_Type__c = 'P1',
                                                  hmr_Program_Type__c = 'Healthy Solutions',
                                                  hmr_Program_Display_Name__c = 'P1 Healthy Solutions',
                                                  Days_in_1st_Order_Cycle__c = 30,
                                                  Default_Promotional_Discount__c = 0.0,
                                                  hmr_Standard_Kit_Price__c = 301.65);
        insert programRecord;
    }
    
    @isTest
    private static void testAddBillingAddressWithNewAddress(){
        Map<String, Object> resultMap;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName, Email FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        
        Map<String, String> params = new Map<String, String>{
            'buyerFirstName' => userRecord.Contact.FirstName,
            'buyerLastName' => userRecord.Contact.LastName,
            'buyerEmail' => userRecord.Email,
            'bill_to_email' => userRecord.Email,
            'bill_to_forename' => userRecord.Contact.FirstName,
            'bill_to_surname' => userRecord.Contact.LastName,
            'bill_to_address_line1' => contactAddressRecord.ccrz__AddressFirstline__c,
            'bill_to_address_line2' => contactAddressRecord.ccrz__AddressSecondline__c,
            'bill_to_address_city' => contactAddressRecord.ccrz__City__c,
            'bill_to_address_state' => contactAddressRecord.ccrz__State__c,
            'bill_to_address_state_code' => contactAddressRecord.ccrz__StateISOCode__c,
            'bill_to_address_country' => contactAddressRecord.ccrz__CountryISOCode__c,
            'bill_to_address_postal_code' => '90067' 
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_Checkout_Service checkoutService = new HMR_CMS_Checkout_Service(userRecord.Id, false);
            resultMap = checkoutService.addBillingAddress(userRecord.Id, params);
        }

        Test.stopTest();   

        //Verify success
        System.assert((Boolean)resultMap.containsKey('contactAddress')); 
    }

    @isTest
    private static void testAddBillingAddressWithExistingAddress(){
        Map<String, Object> resultMap;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName, Email FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        
        Map<String, String> params = new Map<String, String>{
            'buyerFirstName' => userRecord.Contact.FirstName,
            'buyerLastName' => userRecord.Contact.LastName,
            'buyerEmail' => userRecord.Email,
            'bill_to_email' => userRecord.Email,
            'bill_to_forename' => userRecord.Contact.FirstName,
            'bill_to_surname' => userRecord.Contact.LastName,
            'bill_to_address_line1' => contactAddressRecord.ccrz__AddressFirstline__c,
            'bill_to_address_line2' => contactAddressRecord.ccrz__AddressSecondline__c,
            'bill_to_address_city' => contactAddressRecord.ccrz__City__c,
            'bill_to_address_state' => contactAddressRecord.ccrz__State__c,
            'bill_to_address_state_code' => contactAddressRecord.ccrz__StateISOCode__c,
            'bill_to_address_country' => contactAddressRecord.ccrz__CountryISOCode__c,
            'bill_to_address_postal_code' => contactAddressRecord.ccrz__PostalCode__c 
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_Checkout_Service checkoutService = new HMR_CMS_Checkout_Service(userRecord.Id, false);
            resultMap = checkoutService.addBillingAddress(userRecord.Id, params);
        }

        Test.stopTest();   

        //Verify success
        System.assert((Boolean)resultMap.containsKey('contactAddress')); 
    }

    @isTest
    private static void testGetTerms() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getTerms');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetCartByEncryptedId() {
        String result;
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        Map<String, String> params = new Map<String, String>{
            'userId' => userRecord.Id,
            'isGuest' => 'false',
            'encryptedCartId' => cartRecord.ccrz__EncryptedId__c,
            'action' => 'getCartByEncryptedId'
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
            result = checkoutServExt.executeRequest(params);  
        }

        Test.stopTest();

        //Verify success
        Map<String, Object> resultMap = (Map<String, Object>)JSON.deserializeUntyped(result);
        System.assert((Boolean)resultMap.get('success'));
    }
    
    @isTest
    private static void testGetCartByUser() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getCartByUser');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetCartAddresses() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getCartAddresses');
        checkoutServExt.executeRequest(params);  

        Test.stopTest(); 
    }
    
    @isTest
    private static void testGetCartBillingAddress() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getCartBillingAddress');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetCartShippingAddress() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getCartShippingAddress');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testCheckForAutomaticCoupon() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','checkForAutomaticCoupon');
        checkoutServExt.executeRequest(params);  
        
    }

    @isTest
    private static void testCreateCartAddress(){
        Map<String, Object> resultMap;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName, Email FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        
        Map<String, String> userAddressMap = new Map<String, String>{
            'firstName' => userRecord.Contact.FirstName,
            'lastName' => userRecord.Contact.LastName,
            'buyerEmail' => userRecord.Email,
            'addressFirstline' => contactAddressRecord.ccrz__AddressFirstline__c,
            'addressSecondline' => contactAddressRecord.ccrz__AddressSecondline__c,
            'city' => contactAddressRecord.ccrz__City__c,
            'state' => contactAddressRecord.ccrz__State__c,
            'state_code' => contactAddressRecord.ccrz__StateISOCode__c,
            'country' => contactAddressRecord.ccrz__CountryISOCode__c,
            'postalCode' => contactAddressRecord.ccrz__PostalCode__c
        };


        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_Checkout_Service checkoutService = new HMR_CMS_Checkout_Service(userRecord.Id, false);
            resultMap = checkoutService.createCartAddress(userAddressMap, cartRecord.ccrz__EncryptedId__c, 'Shipping');
        }

        Test.stopTest();   

        //Verify success
        System.assert((Boolean)resultMap.get('success')); 
    }
    
    @isTest
    private static void testCreateCartAddressInvalid() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('addressData','{"test","test"}');
        params.put('addressType','test');
        params.put('action','createCartAddress');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetUserAddresses() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getUserAddresses');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetShippingOptions() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getShippingOptions');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testGetStoredPayments() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','getStoredPayments');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testPlaceOrder() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('paymentData','test-payment-data');
        params.put('action','placeOrder');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testUpdateAddress() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('addressData','[{"test","test"}]');
        params.put('action','updateAddress');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testUpdateBuyerInformation() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('buyerFirstName','first');
        params.put('buyerLastName','last');
        params.put('buyerEmail','test@em.com');
        params.put('buyerPhone','1234567890');
        params.put('action','updateBuyerInformation');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testUpdateShippingMethod() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('shippingMethod','test');
        params.put('shippingCharge','123');
        params.put('action','updateShippingMethod');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testSaveCCTerms() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('contactId','test');
        params.put('ccTermIdList','["test"]]');
        params.put('isCSRFlow','TRUE');
        params.put('action','saveCCTerms');
        checkoutServExt.executeRequest(params);  
        
    }

    @isTest
    private static void testAuthorizePayment(){
        String result;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName, Email FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        
        Map<String, String> params = new Map<String, String>{
            'userId' => userRecord.Id,
            'isGuest' => 'false',
            'encryptedCartId' => cartRecord.ccrz__EncryptedId__c,
            'card_number' => '41111111111111111',
            'card_type' => '01',
            'card_expiry_date' => '01/' + String.valueOf(Date.today().Year() + 1),
            'card_cvn' => '911',
            'buyerFirstName' => userRecord.Contact.FirstName,
            'buyerLastName' => userRecord.Contact.LastName,
            'buyerEmail' => userRecord.Email,
            'bill_to_email' => userRecord.Email,
            'bill_to_forename' => userRecord.Contact.FirstName,
            'bill_to_surname' => userRecord.Contact.LastName,
            'bill_to_address_line1' => contactAddressRecord.ccrz__AddressFirstline__c,
            'bill_to_address_line2' => contactAddressRecord.ccrz__AddressSecondline__c,
            'bill_to_address_city' => contactAddressRecord.ccrz__City__c,
            'bill_to_address_state' => contactAddressRecord.ccrz__State__c,
            'bill_to_address_state_code' => contactAddressRecord.ccrz__StateISOCode__c,
            'bill_to_address_country' => contactAddressRecord.ccrz__CountryISOCode__c,
            'bill_to_address_postal_code' => contactAddressRecord.ccrz__PostalCode__c,
            'action' => 'authorizePayment'
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
            result = checkoutServExt.executeRequest(params);
        }

        Test.stopTest();   

        Map<String, Object> resultMap = (Map<String, Object>) JSON.deserializeUntyped(result);
        //Verify success
        System.assert(!(Boolean)resultMap.get('success')); 
    }
    
    @isTest
    private static void testAuthorizePaymentInvalid() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','authorizePayment');
        checkoutServExt.executeRequest(params);  
        
    }
    
    @isTest
    private static void testAuthorizePaymentAndCreatePayment() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        
        params.put('action','authorizePaymentAndCreatePaymentToken');
        checkoutServExt.executeRequest(params);
        
        params.put('action','authorizePaymentToken');
        checkoutServExt.executeRequest(params);    
        
    }

    @isTest
    private static void testCreatePaymentToken(){
        Map<String, Object> resultMap;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName, Email FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        
        Map<String, String> userAddressMap = new Map<String, String>{
            'buyerFirstName' => userRecord.Contact.FirstName,
            'buyerLastName' => userRecord.Contact.LastName,
            'buyerEmail' => userRecord.Email,
            'bill_to_email' => userRecord.Email,
            'bill_to_forename' => userRecord.Contact.FirstName,
            'bill_to_surname' => userRecord.Contact.LastName,
            'bill_to_address_line1' => contactAddressRecord.ccrz__AddressFirstline__c,
            'bill_to_address_line2' => contactAddressRecord.ccrz__AddressSecondline__c,
            'bill_to_address_city' => contactAddressRecord.ccrz__City__c,
            'bill_to_address_state' => contactAddressRecord.ccrz__State__c,
            'bill_to_address_state_code' => contactAddressRecord.ccrz__StateISOCode__c,
            'bill_to_address_country' => contactAddressRecord.ccrz__CountryISOCode__c,
            'bill_to_address_postal_code' => contactAddressRecord.ccrz__PostalCode__c
        };

        Map<String, String> unsignedFieldMap = new Map<String, String>{
            'card_number' => '41111111111111111',
            'card_type' => '01',
            'card_expiry_date' => '01/' + String.valueOf(Date.today().Year() + 1),
            'card_cvn' => '911'
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_Checkout_Service checkoutService = new HMR_CMS_Checkout_Service(userRecord.Id, false);
            resultMap = checkoutService.createPaymentToken(userAddressMap, unsignedFieldMap);
        }

        Test.stopTest();   

        //Verify unsuccessful - it will be unsuccessful as we can't actually callout to cybersource
        System.assert(!(Boolean)resultMap.get('success')); 
    }
    
    @isTest
    private static void testCreatePaymentTokenP2() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','createPaymentTokenForP2');
        checkoutServExt.executeRequest(params);    
        
    }
    
    @isTest
    private static void testAuthorizePaymentToken() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','authorizePaymentToken');
        checkoutServExt.executeRequest(params);    
        
    }

    @isTest
    private static void testCreateStoredPayment(){
        String result;
        User userRecord = [SELECT ContactId, Contact.FirstName, Contact.LastName FROM User WHERE Alias = 'ccTest'];
        
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.billToAddress;
        contactAddressRecord.OwnerId = userRecord.Id;
        update contactAddressRecord;

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];

        Map<String, String> params = new Map<String, String>{
            'userId' => userRecord.Id,
            'isGuest' => 'false',
            'encryptedCartId' => cartRecord.ccrz__EncryptedId__c,
            'displayName' => userRecord.Contact.FirstName + ' ' + userRecord.Contact.LastName,
            'accountNumber' => '4111111111111111',
            'token' => 'testToken',
            'paymentToken' => 'testToken',
            'expirationMonth' => '01',
            'expirationYear' => String.valueOf(Date.today().Year() + 1),
            'paymentType' => '03',
            'contactFirstName' => userRecord.Contact.FirstName,
            'contactLastName' => userRecord.Contact.LastName,
            'contactAddressId' => contactAddressRecord.Id,
            'action' => 'createStoredPayment'
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
            result = checkoutServExt.executeRequest(params);
        }

        Test.stopTest();   

        Map<String, Object> resultMap = (Map<String, Object>) JSON.deserializeUntyped(result);
        //Verify success
        System.assert((Boolean)resultMap.get('success')); 
    }
    
    @isTest
    private static void testCreateStoredPaymentWithInvalidData() {
        Map<String, String> params = new Map<String, String>();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('action','createStoredPayment');

        Test.startTest();

        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        
        checkoutServExt.executeRequest(params);   

        Test.stopTest(); 
        
    }

    @isTest
    private static void testGetCCTerms() {
        String result;
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];
        ccrz__E_Product__c productRecord = [SELECT Id FROM ccrz__E_Product__c LIMIT 1];

        Map<String, String> params = new Map<String, String>{
            'userId' => userRecord.Id,
            'isGuest' => 'false',
            'encryptedCartId' => cartRecord.ccrz__EncryptedId__c,
            'contactId' => userRecord.ContactId,
            'productIdList' => '["' + productRecord.Id + '"]',
            'action' => 'getCCTerms'
        };

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
            result = checkoutServExt.executeRequest(params);  
        }

        Test.stopTest();

        Map<String, Object> resultMap = (Map<String, Object>) JSON.deserializeUntyped(result);
        //Verify success
        System.assert((Boolean)resultMap.get('success'));
        
    }
    @isTest
    private static void testGetExtendedDataForProducts() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('productIdList','["test"]');
        params.put('action','getExtendedDataForProducts');
        checkoutServExt.executeRequest(params);    
        
    }
    @isTest
    private static void testGetStoredPaymentDetails() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('paymentId','testpaymentlist');
        params.put('action','getStoredPaymentDetails');
        checkoutServExt.executeRequest(params);    
        
    }

    @isTest
    private static void testUpdateCartForP2() {
        Map<String, Object> resultMap;
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c LIMIT 1];

        Test.startTest();

        System.runAs(userRecord){
            HMR_CMS_Checkout_Service checkoutService = new HMR_CMS_Checkout_Service(userRecord.Id, false);
            resultMap = checkoutService.updateCartForP2(cartRecord.ccrz__EncryptedId__c, Date.today());  
        }

        Test.stopTest();

        //Verify success
        System.assert((Boolean)resultMap.get('success'));
    }

    @isTest
    private static void testUpdateCartForP2Invalid() {
        Test.startTest();
        
        Map<String, String> params = new Map<String, String>();
        
        
        HMR_CMS_CCRZ_Checkout_ServiceInterface checkoutServExt = new HMR_CMS_CCRZ_Checkout_ServiceInterface();
        params.put('userId',UserInfo.getUserId());
        params.put('isGuest','FALSE');
        params.put('encryptedCartId','wertte-awrdaf');
        params.put('paymentToken','test');
        params.put('p2ProcessDate','2017-12-12 12:20:20');
        params.put('action','updateCartForP2');
        checkoutServExt.executeRequest(params);    
        
    }
    
    
    @isTest
    private static void testGetType() {
        Test.startTest();
        HMR_CMS_CCRZ_Checkout_ServiceInterface.getType();
    }
    
}