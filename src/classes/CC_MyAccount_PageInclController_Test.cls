/**
* @Date: 3/27/2017
* @Author: Aslesha Kokate(Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-
*
* This test class covers CC_MyAccount_PageInclController 
* Uses cc_dataFActory for test data
*/


@isTest
private class CC_MyAccount_PageInclController_Test {
    
    @isTest static void test_method_one() {
    
        cc_dataFActory.setupTestUser();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
         Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert userTest;
        
        

        
        Test.setCurrentPageReference(new PageReference('Page.CC_MyAccount_PageIncl')); 
        System.currentPageReference().getParameters().put('portalUser', userTest.id);
        
        CC_MyAccount_PageInclController objPageCntlr= new CC_MyAccount_PageInclController();
            
           //system.assert(objPageCntlr.dataMap.get('_programInfo')!=null);
           objPageCntlr.getUserFirstname();
                       
    }
}