/**
* Apex Content Template Controller for the Success Stories Resource Home Section Component
*
* @Date: 07/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 07/18/2017
* @JIRA: 
*/
global virtual  class HMR_CMS_SuccessStories_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SuccessStories_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SuccessStories_ContentTemplate() {}
	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML(){
    	String html =  '<div class="success-stories-bg">' +
					        '<div class="container">' +
					            '<div class="row">' +
					                '<div class="col-xs-12">' +
					                   '<div class="title text-center">Success Stories</div>' +
					                '</div>' +
					                '<div class="text-center">' +
					                    '<img class="visible-xs-block img-responsive" src="https://www.myhmrprogram.com/ContentMedia/Resources/SuccessStories.jpg" />' +
					                    '<img class="col-xs-12 hidden-xs" src="https://www.myhmrprogram.com/ContentMedia/Resources/SuccessStoriesLarge.jpg" />' +
					                '</div>' +
					                '<div class="col-xs-12 text-center hmr-link">' +
					                    '<a class="" href="resources/community-success-stories">SEE ALL ></a>' +
					                '</div>' +
					            '</div>' +
					        '</div>' +
					    '</div>';
    	return html;
    }
}