/**
* Logic to Handle/Process Cart Transfers between Guest/Anonymous Login and Authenticated User
*
* @Date: 2017-04-18
* @Author: Zach Engman (Magnet 360)
* @Modified: 2017-06-19
* @JIRA: 
*/
global virtual with sharing class HMR_CC_CartTransfer_Extension {
    public static final String USER_TYPE_GUEST = 'Guest';

    @RemoteAction
    global static ccrz.cc_RemoteActionResult transferCart(string cartId){
        return transferCartToUser(cartId, UserInfo.getUserId());
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult transferCartToUser(String cartId, String userId){
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        ccrz__E_Cart__c cartRecord = getCart(cartId);
        HMR_User_Selector userSelector = new HMR_User_Selector();
        User userRecord = userSelector.getById(userId);
        List<ccrz__E_ContactAddr__c> contactAddressListToUpdate = new List<ccrz__E_ContactAddr__c>();
        HMR_Cart_Service cartService = new HMR_Cart_Service(userId, false);

        result.success = cartRecord != null && userRecord.Contact != null && !String.isBlank(userRecord.Contact.AccountId);

        if(result.success){
            cartRecord.ccrz__ActiveCart__c = true;
            cartRecord.OwnerId = userRecord.Id;
            cartRecord.ccrz__User__c = userRecord.Id;
            cartRecord.ccrz__AnonymousID__c = false;
            cartRecord.ccrz__Account__c = userRecord.Contact.AccountId;
            cartRecord.ccrz__Contact__c = userRecord.ContactId;
            
            update cartRecord;

            if(!String.isBlank(cartRecord.ccrz__BillTo__c) && cartRecord.ccrz__BillTo__r != null){
                cartRecord.ccrz__BillTo__r.OwnerId = userRecord.Id;
                contactAddressListToUpdate.add(cartRecord.ccrz__BillTo__r);
            }

            if(!String.isBlank(cartRecord.ccrz__ShipTo__c) && cartRecord.ccrz__ShipTo__r != null){
                cartRecord.ccrz__ShipTo__r.OwnerId = userRecord.Id;
                contactAddressListToUpdate.add(cartRecord.ccrz__ShipTo__r);
            }

            if(contactAddressListToUpdate.size() > 0)
                update contactAddressListToUpdate;

            result.data = cartId;
        }
        else
            result.data = getErrorMessage(userRecord, cartRecord);
        
        return result;
    }
   

    private static string getErrorMessage(User userRecord, ccrz__E_Cart__c cartRecord){
        string errorMessage = '';

         if(cartRecord == null)
            errorMessage = 'Unable to retrieve cart record, it may not be active or open';
         else if(userRecord == null || String.isBlank(userRecord.Contact.AccountId))
            errorMessage = 'User is not propertly associated with contact and account group';

         return errorMessage;
    }

    private static ccrz__E_Cart__c getCart(String encryptedId) {
        if(!String.isBlank(encryptedId)){
            List<ccrz__E_Cart__c> cartList = [SELECT OwnerId,
                                                     ccrz__ActiveCart__c,
                                                     ccrz__Account__c,
                                                     ccrz__AnonymousID__c, 
                                                     ccrz__Contact__c,
                                                     ccrz__User__c,
                                                     ccrz__BillTo__c,
                                                     ccrz__ShipTo__c,
                                                     ccrz__BillTo__r.OwnerId,
                                                     ccrz__ShipTo__r.OwnerId
                                              FROM ccrz__E_Cart__c 
                                              WHERE ccrz__encryptedId__c =: encryptedId
                                                AND ccrz__ActiveCart__c = true
                                                AND ccrz__CartStatus__c = 'Open'];
            if(cartList.size() > 0)
                return cartList[0];
        }
        return null;
    }

    public static ccrz.cc_RemoteActionResult transferGuestCart(String username){
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.success = false;
        
        if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
             String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
             List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: username];
 
             if(!String.isBlank(cartId) && userRecordList.size() > 0)
                result = HMR_CC_CartTransfer_Extension.transferCartToUser(cartId, userRecordList[0].Id);
        }
        
        return result;
    }
}