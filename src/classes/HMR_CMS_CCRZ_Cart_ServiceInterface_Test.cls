/**
* Test Class Coverage of the HMR_CMS_CCRZ_Cart_ServiceInterface
*
* @Date: 10/12/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 10/17/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_CCRZ_Cart_ServiceInterface_Test {
    @testSetup
    private static void setupTestData(){
        User testUserRecord = cc_dataFactory.testUser;
        User guestUserRecord = cc_dataFactory.testUserGuest;
        cc_dataFactory.setupCatalog();

         //Create kit product
        ccrz__E_Product__c ccrzKitProduct = new ccrz__E_Product__c(Name = 'Healthy Solutions® Quick Start® Kit'
                                                                   ,ccrz__SKU__c = 'HSQSKRVC'
                                                                   ,ccrz__ProductType__c = 'Dynamic Kit'
                                                                   ,ccrz__ProductStatus__c = 'Released'
                                                                   ,hmr_ProductActive__c = true
                                                                   ,ccrz__Storefront__c = 'DefaultStore'
                                                                   ,ccrz__QuantityPerUnit__c = 1
                                                                   ,ccrz__StartDate__c = Date.today()
                                                                   ,ccrz__EndDate__c = Date.today());
         insert ccrzKitProduct;

        //Create kit category
        ccrz__E_Category__c ccrzKitCategory = new ccrz__E_Category__c(Name = 'Kits'
                                                                      ,ccrz__CategoryID__c = '800'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 700);
        insert ccrzKitCategory;

        //Create kit product category
        ccrz__E_ProductCategory__c ccrzKitProductCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzKitProduct.Id
                                                                                            ,ccrz__Category__c = ccrzKitCategory.Id
                                                                                            ,ccrz__StartDate__c = Date.today()
                                                                                            ,ccrz__EndDate__c = Date.today());
        insert ccrzKitProductCategory;

        //Create program
        Program__c programRecord = new Program__c(Name = 'P1 Healthy Solutions'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Solutions'
                                                 ,hmr_Phase_Type__c = 'P1'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord;

        //Create program
        Program__c programRecord2 = new Program__c(Name = 'P1 Healthy Shakes'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Shakes'
                                                 ,hmr_Phase_Type__c = 'P1'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord2;

        //Create program
        Program__c programRecord3 = new Program__c(Name = 'Phase 2'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Solutions'
                                                 ,hmr_Phase_Type__c = 'P2'
                                                 ,Days_in_1st_Order_Cycle__c = 21);

        insert programRecord3;

        //Create coupon data
        HMR_Coupon__c hmrCouponRecord = new HMR_Coupon__c(
            Name = 'HMR_100',
            hmr_Active__c = true,
            hmr_Description__c = '100 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert hmrCouponRecord;

        ccrz__E_Coupon__c childCouponRecord = new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'A la Carte',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = hmrCouponRecord.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'HMR_100',
            ccrz__CouponName__c = 'HMR_100',
            ccrz__TotalUsed__c = 0
        );
        //*******************************************************************
        //Create coupon data
        HMR_Coupon__c programHmrCoupon = new HMR_Coupon__c(
            Name = 'ProgramDiscount',
            hmr_Active__c = true,
            hmr_Description__c = '100 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert programHmrCoupon;

        //Create coupon data
        HMR_Coupon__c programMemberHmrCoupon = new HMR_Coupon__c(
            Name = 'ProgramMember',
            hmr_Active__c = true,
            hmr_Description__c = '20 Dollars Off',
            hmr_Does_Not_Expire__c = false,
            hmr_End_Date__c = Date.today().addDays(100),
            hmr_Start_Date__c = Date.today(),
            hmr_Type_of_Code__c = 'General'
        );

        insert programMemberHmrCoupon;

        List<ccrz__E_Coupon__c> childCouponList = new List<ccrz__E_Coupon__c>();


        
        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_P1_1',
            ccrz__CouponName__c = 'ProgramDiscount_P1_1',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'HSS 1st Order',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_HS_1',
            ccrz__CouponName__c = 'ProgramDiscount_HS_1',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord2.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P2',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramDiscount_P2',
            ccrz__CouponName__c = 'ProgramDiscount_P2',
            ccrz__TotalUsed__c = 0, 
            hmr_Program__c = programRecord3.Id
        ));


        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P1 Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_P1_I',
            ccrz__CouponName__c = 'ProgramMember_P1_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'HSS Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_HSS_I',
            ccrz__CouponName__c = 'ProgramMember_HSS_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord2.Id
        ));

        childCouponList.add(new ccrz__E_Coupon__c(
            ccrz__RuleType__c = 'General',
            ccrz__CouponType__c = 'Absolute',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today().addDays(100),
            ccrz__Enabled__c = true,
            Coupon_Order_Type__c = 'P2 Interim',
            ccrz__MaxUse__c = 100,
            HMR_Coupon__c = programHmrCoupon.Id,
            ccrz__DiscountAmount__c = 100,
            ccrz__Storefront__c = 'DefaultStore',
            ccrz__CouponCode__c = 'ProgramMember_P2_I',
            ccrz__CouponName__c = 'ProgramMember_P2_I',
            ccrz__TotalUsed__c = 0,
            hmr_Program__c = programRecord3.Id
        ));

        insert childCouponList;
    }

    @isTest
    private static void testCreateCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testCreateCartRequestAsGuest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'ctgu'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'true');
        parameterMap.put('action', 'createCart');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testAddItemToNewCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addToCart');
        parameterMap.put('sku', 'test001');
        parameterMap.put('quantity', '1');
        parameterMap.put('price', '10');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
    }

    @isTest
    private static void testAddItemToExistingCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;
        String encryptedCartId;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');

        //Create cart
        result = service.executeRequest(parameterMap);
        encryptedCartId = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c].ccrz__EncryptedId__c;
        
        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addToCart');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('sku', 'test001');
        parameterMap.put('quantity', '1');
        parameterMap.put('price', '1.00');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
    }

    @isTest
    private static void testAdjustDiscountRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;
        String encryptedCartId;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');

        //Create cart
        result = service.executeRequest(parameterMap);
        encryptedCartId = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c].ccrz__EncryptedId__c;
        
        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addToCart');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('sku', 'test001');
        parameterMap.put('quantity', '1');
        parameterMap.put('price', '100.00');
        result = service.executeRequest(parameterMap);

        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'adjustDiscount');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('adjustment', '10.00');

        Test.startTest();

        result = service.executeRequest(parameterMap);

        Test.stopTest();
        
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__AdjustmentAmount__c, ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(cartRecord.ccrz__AdjustmentAmount__c < 0);
    }

    @isTest
    private static void testRemoveItemFromCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        List<ccrz__E_CartItem__c> cartItemList;
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;
        String encryptedCartId;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addToCart');
        parameterMap.put('sku', 'test001');
        parameterMap.put('quantity', '1');
        parameterMap.put('price', '10');
        //Add item to cart
        result = service.executeRequest(parameterMap);
        encryptedCartId = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c].ccrz__EncryptedId__c;

        cartItemList = [SELECT Id FROM ccrz__E_CartItem__c WHERE ccrz__Cart__r.ccrz__EncryptedId__c =: encryptedCartId];
        System.assert(cartItemList.size() > 0);
        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'removeFromCart');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('sfId', cartItemList[0].Id);

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        cartItemList = [SELECT Id FROM ccrz__E_CartItem__c WHERE ccrz__Cart__r.ccrz__EncryptedId__c =: encryptedCartId];

        System.assert(cartItemList.size() == 0);
    }

    @isTest
    private static void testAddKitToNewCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addKitToCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
    }
    
    
    @isTest
    private static void testCheckForAutomaticCouponRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'checkForAutomaticCoupon');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');
        parameterMap.put('encryptedCartId', 'test-test-123414-ertcyy-12334');
        parameterMap.put('redirectUrl', 'test');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

      //  ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

     //   System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
     //   System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
     System.assert(!String.isBlank(result));
    }
    
    
    @isTest
    private static void testCheckForExpiredCouponRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'checkForExpiredCoupon');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

     //   ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

     //   System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
      //  System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
      System.assert(!String.isBlank(result));
    }
    
    
    @isTest
    private static void testPriceCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'priceCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');
        parameterMap.put('encryptedCartId', 'test-test-123414-ertcyy-12334');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

       // ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

       // System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
       // System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
       System.assert(!String.isBlank(result));
    }
    
    
    @isTest
    private static void testSetCoachingSelectionRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'setCoachingSelection');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');
        parameterMap.put('encryptedCartId', 'test-test-123414-ertcyy-12334');
        parameterMap.put('coachingSelection', 'test');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

       // ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

       // System.assert(!String.isBlank(result));
       // //Verify the cart was auto-created
     //   System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
     System.assert(!String.isBlank(result));
    }
    
    @isTest
    private static void testUpdateCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'updateCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('quantity', '1');
        parameterMap.put('encryptedCartId', 'test-test-123414-ertcyy-12334');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        //ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

       // System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
      //  System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
      System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testAddKitToExistingCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String encryptedCartId;
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');

        //Create cart
        result = service.executeRequest(parameterMap);
        encryptedCartId = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c].ccrz__EncryptedId__c;

        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addKitToCart');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('sku', 'HSQSKRVC');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        System.assertEquals(encryptedCartId, cartRecord.ccrz__EncryptedId__c);
    }

    @isTest
    private static void testAddDynamicKitToNewCartRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addDynamicKitToCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('itemList', '[{"sku":"test001","quantity":"1"}]');
        parameterMap.put('coachingSelection', 'false');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        //System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
    }
    
    
    @isTest
    private static void testAddDynamicKitToNewCartEncIdRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addDynamicKitToCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('itemList', '[{"sku":"test001","quantity":"1"}]');
        parameterMap.put('coachingSelection', 'false');
        parameterMap.put('encryptedCartId', 'test-test-123-54342525-123134');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

      //  ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

    //    System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        //System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testAddDynamicKitToNewCartRequestWithCoaching(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'addDynamicKitToCart');
        parameterMap.put('sku', 'HSQSKRVC');
        parameterMap.put('itemList', '[{"sku":"test001","quantity":"1"}]');
        parameterMap.put('coachingSelection', 'true');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        System.assert(!String.isBlank(result));
        //Verify the cart was auto-created
        //System.assert(result.containsIgnoreCase(cartRecord.ccrz__EncryptedId__c));
    }

    @isTest
    private static void testApplyCouponRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String encryptedCartId;
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');
        //Create Cart
        service.executeRequest(parameterMap);
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        parameterMap.clear();
        parameterMap.put('action', 'applyCoupon');
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('encryptedCartId', cartRecord.ccrz__EncryptedId__c);
        parameterMap.put('couponCode', 'HMR_100');
        parameterMap.put('repriceCart', 'false');

        Test.startTest();

        result = service.executeRequest(parameterMap);

        Test.stopTest();

        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testRemoveCouponRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String encryptedCartId;
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');
        //Create cart
        service.executeRequest(parameterMap);
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        //Add coupon to cart
        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'applyCoupon');
        parameterMap.put('encryptedCartId', encryptedCartId);
        parameterMap.put('couponCode', 'HMR_100');

        service.executeRequest(parameterMap);

        parameterMap.put('action', 'removeCoupon');

        //Remove coupon from cart
        Test.startTest();

        result = service.executeRequest(parameterMap);

        Test.stopTest();

        //System.assert(result.containsIgnoreCase('true'));
        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testGetCartByEncryptedIdRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');
        //Create Cart
        service.executeRequest(parameterMap);
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'getCartByEncryptedId');
        parameterMap.put('encryptedCartId', cartRecord.ccrz__EncryptedId__c);

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testGetCartByUserRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest'];
        String result;

        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'createCart');
        //Create Cart
        service.executeRequest(parameterMap);
        ccrz__E_Cart__c cartRecord = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c];

        parameterMap.clear();
        parameterMap.put('userId', userRecord.Id);
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'getCartByUser');

        System.runAs(userRecord){
            Test.startTest();

            result = service.executeRequest(parameterMap);

            Test.stopTest();
        }

        System.assert(!String.isBlank(result));
    }

    @isTest
    private static void testInvalidActionRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();

        parameterMap.put('userId', UserInfo.getUserId());
        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'invalidMethod');

        Test.startTest();

        String result = service.executeRequest(parameterMap);

        Test.stopTest();

        System.assert(result.containsIgnoreCase('Invalid Action'));
    }

    @isTest
    private static void testMissingUserIdParameterRequest(){
        HMR_CMS_CCRZ_Cart_ServiceInterface service = new HMR_CMS_CCRZ_Cart_ServiceInterface();
        Map<String, String> parameterMap = new Map<String, String>();


        parameterMap.put('isGuest', 'false');
        parameterMap.put('action', 'invalidMethod');

        Test.startTest();

        String result = service.executeRequest(parameterMap);

        Test.stopTest();

        System.assert(result.containsIgnoreCase('userId and isGuest parameters required'));
    }

    @isTest
    private static void testGetType(){
        Type typeResult;

        Test.startTest();

        typeResult = HMR_CMS_CCRZ_Cart_ServiceInterface.getType();

        Test.stopTest();

        System.assertEquals(typeResult, HMR_CMS_CCRZ_Cart_ServiceInterface.class);
    }
}