/**
* Apex Content Template Controller for Primary Nav inside header on OCMS Master
*
* @Date: 2017-03-06
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_HeaderCallHMR_ContentTemplate extends cms.ContentTemplateController{
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_HeaderCallHMR_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_HeaderCallHMR_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }   
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }
    //Call HMR 1800 # Label Property
    public String CallHMRLabel{    
        get{
            return getPropertyWithDefault('CallHMRLabel', '');
        }    
    }
    //Call HMR 1800 # Number Property
    public String CallHMRNumber{    
        get{
            return getPropertyWithDefault('CallHMRNumber', '');
        }    
    }
    //Call HMR 1800 # Number Property
    public String NavSelect{    
        get{
            return getPropertyWithDefault('NavSelect', '');
        }    
    }

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        if(NavSelect == 'primary') {
        	html += '<li id="call">';	
        }
        else {
        	html += '<li class="hidden-xs">';
        }
        html +=	'<a href="tel:' + CallHMRNumber.escapeHtml4() + '">' + 
	                'CALL ' + CallHMRLabel.escapeHtml4() + 
	            '</a>' +
	        '</li>';
        return html;
    }
}