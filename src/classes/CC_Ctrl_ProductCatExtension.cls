/**
*This is controller for Product List Page fetching product category info
* @Date: 2017-3-16
* @Author Jay Zincone(HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-2511
*/
global with sharing class CC_Ctrl_ProductCatExtension {

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCategoryDataForProducts(final ccrz.cc_RemoteActionContext ctx){        
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false; 
        try { 
            Map<String,ccrz__E_ProductCategory__c > dataMap = new Map<String, ccrz__E_ProductCategory__c>();
            Map<Id, ccrz__E_ProductCategory__c> cc_ProductsMap = new Map<Id, ccrz__E_ProductCategory__c>(
                [SELECT ccrz__Product__r.Id, ccrz__Product__r.ccrz__SKU__c, ccrz__Category__r.Name  FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__r.Name != 'HMR Products'] );
            
            for(ccrz__E_ProductCategory__c productItem: cc_ProductsMap.values()){               
                dataMap.put(String.valueOf(productItem.ccrz__Product__r.Id), productItem);                
            }

            res.success = true;
            res.data = dataMap;
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getCategoryDataForProducts ' + res + ' ' + err.getMessage());
        }
        res.success = true;
        return null;
    }
}