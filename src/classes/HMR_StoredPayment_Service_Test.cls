/**
* Test Class Coverage of the HMR_StoredPayment_Service
*
* @Date: 8/12/2017
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA: 
*/
@isTest
private class HMR_StoredPayment_Service_Test{
  
    @testSetup
    private static void setupTestData(){
    
        ccrz__E_Order__c orderTestData = new ccrz__E_Order__c(
                                                   ccrz__BuyerEmail__c = 'test@hm.com',
                                                   ccrz__BuyerPhone__c = '12345667788',
                                                   ccrz__OrderNumber__c = 12345,
                                                   ccrz__EncryptedId__c = '12345',
                                                   ccrz__TaxAmount__c = 123                                              
                                                );    
        insert orderTestData;
        
        
        ccrz__E_Product__c productTestData = new ccrz__E_Product__c(
                                                    Name = 'Test Product',
                                                    ccrz__SKU__c = 'SK123',
                                                    ccrz__StartDate__c = Date.today(),
                                                    ccrz__EndDate__c = Date.today().addDays(1),
                                                    ccrz__Quantityperunit__c = 12
                                                );
                                                
        insert productTestData;
        
        ccrz__E_OrderItem__c orderItemData = new ccrz__E_OrderItem__c(
                                                    ccrz__Order__c = orderTestData.Id,
                                                    ccrz__Quantity__c = 2,
                                                    ccrz__OrderLineType__c = 'Major',
                                                    ccrz__Price__c = 123, 
                                                    ccrz__SubAmount__c = 123,
                                                    ccrz__Product__c = productTestData.Id ,
                                                    ccrz__ParentProduct__c = productTestData.Id,
                                                    ccrz__DisplayProduct__c =  productTestData.Id                                          
                                                );
        insert orderItemData;
        
        ccrz__E_StoredPayment__c strPay = new ccrz__E_StoredPayment__c(Name = 'test stored payment',
                                                                       ccrz__Enabled__c = TRUE,
                                                                       ownerId = UserInfo.getUserId());
       insert strPay;
        
        ccrz__E_TransactionPayment__c transactionPayData = new ccrz__E_TransactionPayment__c(
                                                    ccrz__CCOrder__c = orderTestData.Id,
                                                    ccrz__Storefront__c = 'DefaultStore',
                                                    ccrz__Amount__c = 123,
                                                    ccrz__RequestAmount__c = 123,
                                                    ccrz__StoredPayment__c = strPay.Id
                                                );
                                                
        insert transactionPayData;
     
    }

 
    
    @isTest
    private static void testMethodGetByOwnerId(){
    
        HMR_StoredPayment_Service storedPaymentRec = new HMR_StoredPayment_Service();  
        HMR_StoredPayment_Service storedPaymentRec2 = new HMR_StoredPayment_Service(UserInfo.getUserId()); 
        List<HMR_StoredPayment_Service.StoredPaymentRecord> strUserId =  storedPaymentRec.getByOwnerId();
   
    }
    
    @isTest
    private static void testMethodStoredPaymentRecordWrapper(){
    
        Map<String, Object> paramsMap = new Map<String, Object>();
        
        HMR_StoredPayment_Service.StoredPaymentRecord storedPaymentRecWrapper = new HMR_StoredPayment_Service.StoredPaymentRecord(paramsMap);  
        String Id = storedPaymentRecWrapper.Id;
        String account = storedPaymentRecWrapper.account;
        String accNum = storedPaymentRecWrapper.accountNumber;
        String accType = storedPaymentRecWrapper.accountType;
        String addId = storedPaymentRecWrapper.addressId;
        String dispName = storedPaymentRecWrapper.displayName;
        Boolean enbl = storedPaymentRecWrapper.enabled;
        String accNumLastFour = storedPaymentRecWrapper.accountNumberLastFour;
        String paymentType = storedPaymentRecWrapper.paymentType;
        String token = storedPaymentRecWrapper.token;
        Integer expMonth =storedPaymentRecWrapper.expirationMonth;
        Integer expYear = storedPaymentRecWrapper.expirationYear;
        Integer expSeq = storedPaymentRecWrapper.sequence;
    }
    

}