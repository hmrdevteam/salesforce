/*
Date: 01/18/2018
Author: Jay Zincone (HMR)
Modified: 
Target Class: HMR_CMS_Marketing_2Col_ContentTemplate
*/
@isTest
public class HMR_CMS_Markt_2Col_ContTemp_Test{  

        
    @isTest static void test_general_method() { 

                
                HMR_CMS_Marketing_2Col_ContentTemplate pageSection = new HMR_CMS_Marketing_2Col_ContentTemplate();
                String propertyValue = pageSection.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                String LeftTitle = pageSection.LeftTitle;                
                String LeftDescription = pageSection.LeftDescription;
                String LeftImageURL = pageSection.LeftImageURL;
                String LeftPickLinkOrButton = pageSection.LeftPickLinkOrButton ;
                String LeftLinkOrButtonURLText = pageSection.LeftLinkOrButtonURLText;                
                String LeftLinkColorBlueOrWhite = pageSection.LeftLinkColorBlueOrWhite;
                String LeftFontColorBlackOrWhite = pageSection.LeftFontColorBlackOrWhite;
                String LeftCSSClassSelector = pageSection.LeftCSSClassSelector;                
                Boolean LeftisLinkOrButtonForFoodPage = pageSection.LeftisLinkOrButtonForFoodPage;
                String LeftSectionGTMID = pageSection.LeftSectionGTMID;
                String RightTitle = pageSection.RightTitle;                
                String RightDescription = pageSection.RightDescription;
                String RightImageURL = pageSection.RightImageURL;
                String RightPickLinkOrButton = pageSection.RightPickLinkOrButton ;
                String RightLinkOrButtonURLText = pageSection.RightLinkOrButtonURLText;                
                String RightLinkColorBlueOrWhite = pageSection.RightLinkColorBlueOrWhite;
                String RightFontColorBlackOrWhite = pageSection.RightFontColorBlackOrWhite;
                String RightCSSClassSelector = pageSection.RightCSSClassSelector;                
                Boolean RightisLinkOrButtonForFoodPage = pageSection.RightisLinkOrButtonForFoodPage;
                String RightSectionGTMID = pageSection.RightSectionGTMID;
                
                ccrz__E_Category__c category = new ccrz__E_Category__c(Name = 'HMR Products', ccrz__CategoryID__c = 'test12', ccrz__Sequence__c = 500, ccrz__StartDate__c =
                                                                    date.today(), ccrz__EndDate__c = DAte.today().addDays(1));
                                                                    
                insert category;
                
                pageSection.testAttributes = new Map<String, String>  {
                    'LeftTitle' => 'left title',
                    'LeftDescription' => 'left desc',
                    'LeftImageURL' => 'left URL',
                    'LeftPickLinkOrButton' => 'Button',
                    'LeftLinkOrButtonURLText' => 'other',
                    'LeftLinkColorBlueOrWhite' => 'blue',
                    'LeftFontColorBlackOrWhite' => 'white',
                    'LeftCSSClassSelector' => 'show',
                    'LeftisLinkOrButtonForFoodPage' => 'TRUE',
                    'LeftSectionGTMID' => 'Lefttest',
                    'RightTitle' => 'Right title',
                    'RightDescription' => 'Right desc',
                    'RightImageURL' => 'Right URL',
                    'RightPickLinkOrButton' => 'Button',
                    'RightLinkOrButtonURLText' => 'other',
                    'RightLinkColorBlueOrWhite' => 'blue',
                    'RightFontColorBlackOrWhite' => 'white',
                    'RightCSSClassSelector' => 'show',
                    'RightisLinkOrButtonForFoodPage' => 'TRUE',
                    'RightSectionGTMID' => 'Righttest'
                };
                
                String renderHMTL2 = pageSection.getHTML();
                
                pageSection.testAttributes = new Map<String, String>  {
                    'LeftTitle' => 'left title',
                    'LeftDescription' => 'left desc',
                    'LeftImageURL' => 'left URL',
                    'LeftPickLinkOrButton' => 'Link',
                    'LeftLinkOrButtonURLText' => 'other',
                    'LeftLinkColorBlueOrWhite' => 'blue',
                    'LeftFontColorBlackOrWhite' => 'white',
                    'LeftCSSClassSelector' => 'show',
                    'LeftisLinkOrButtonForFoodPage' => 'TRUE',
                    'LeftSectionGTMID' => 'Lefttest',
                    'RightTitle' => 'Right title',
                    'RightDescription' => 'Right desc',
                    'RightImageURL' => 'Right URL',
                    'RightPickLinkOrButton' => 'Link',
                    'RightLinkOrButtonURLText' => 'other',
                    'RightLinkColorBlueOrWhite' => 'blue',
                    'RightFontColorBlackOrWhite' => 'white',
                    'RightCSSClassSelector' => 'show',
                    'RightisLinkOrButtonForFoodPage' => 'TRUE',
                    'RightSectionGTMID' => 'Righttest'
                };
                
                String renderHTML = pageSection.getHTML();
                
                System.assert(!String.isBlank(renderHTML));
                
    }
    
}