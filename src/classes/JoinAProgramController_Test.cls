/*
 * JoinAProgramController_Test.cls
  * @Date: 2017-3-22
  * @Author Nathan Anderson (Magnet 360)
  * @JIRA:
 * Usage:
 *   Test class for JoinAProgramController_Test.cls
 *   Uses cc_dataFActory for data creation
 */

@isTest
private class JoinAProgramController_Test {
	static testmethod void testMethod1() {
		//create test data
		cc_dataFActory.setupTestUser();
		cc_dataFActory.setupCatalog();

		Contact c = [SELECT Id, Community_User_ID__c FROM Contact WHERE Community_User_ID__c != null];

		ccrz__E_Product__c kit = [SELECT Id, ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__ProductStatus__c = 'released' LIMIT 1];

		Program__c p = new Program__c();
		p.Name = 'Phase 1 Healthy Solutions';
		p.Admin_Kit__c = kit.Id;
		p.Admin_Kit_Configuration_URL__c = 'HSAH-Kit-Config-Admin';
		p.Days_in_1st_Order_Cycle__c = 21;
		insert p;


		ApexPages.currentPage().getParameters().put('cId', c.Id);
		JoinAProgramController controller = new JoinAProgramController();
		controller.pId = p.Id;
		controller.getPrograms();
		controller.goToKitConfig();
		controller.cancel();
	}
}