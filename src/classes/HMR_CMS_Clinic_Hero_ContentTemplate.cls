/**
* Apex Content Template Controller for Hero Section on Home Page
*
* @Date: 2017-03-23
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-03-23
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_Clinic_Hero_ContentTemplate extends cms.ContentTemplateController{	
	//Need two contructors: 1 to initialize CreateContentController
	global HMR_CMS_Clinic_Hero_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    
    global HMR_CMS_Clinic_Hero_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Hero Image Property
    public String heroImage{
        get{
            return getProperty('heroImage');
        }
    }


    //global override getHTML - renders hero section image
    global virtual override String getHTML() { 
        String html = '';
        String sitePrefix = Site.getBaseUrl();       
        
        html += String.format('<img class="top-img" src="{0}{1}" width="100%" />', 
                              new String[]{sitePrefix, heroImage});
        
        return html;
    }
}