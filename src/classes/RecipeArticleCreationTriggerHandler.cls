public class RecipeArticleCreationTriggerHandler {

    //updateRecipeKAV(ingredients,instructions,approvedRecipe,recipeArticle)
    public static Recipe__kav upsertRecipeKAV(Map<Id, Recipe_Ingredients__c> recipeIngredients,
                              Map<Id, Recipe_Instruction__c> recipeInstructions,
                              HMR_Recipe__c approvedRecipe, Recipe__kav recipeArticle) {

     	recipeArticle.Recipe__c = approvedRecipe.Id;
        recipeArticle.Title = approvedRecipe.Recipe_Name__c;
        recipeArticle.URLName = approvedRecipe.Recipe_Name__c.replace(' ', '-');
        recipeArticle.Submitted_By__c = approvedRecipe.Contact__c;
        recipeArticle.Summary = approvedRecipe.Desciption__c;

        //for each child ingredient record, construct a string row
        //then add it to the Ingredients field on the Article record (appending each row)
        String IngrdRow = '';

        for(Recipe_Ingredients__c Ingred: recipeIngredients.values()){
            if(Ingred.Quantity_Type__c != null) {
                IngrdRow += Ingred.Quantity__c + ' ' + Ingred.Quantity_Type__c + ' ' + Ingred.Name + '<br />';
            } else {
                IngrdRow += Ingred.Quantity__c + ' ' + Ingred.Name + '<br />';
            }
        }
        recipeArticle.Ingredients__c = IngrdRow;
        //for each child instruction record, construct a string row
        //then add it to the Instructions field on the Article record (appending each row)
        String InstrRow = '';
        for(Recipe_Instruction__c Instruct: recipeInstructions.values()){
            if(Instruct.Instruction__c != null){
                InstrRow += Instruct.Step_Number__c + '. ' + Instruct.Instruction__c + '<br />';
            }
        }
        recipeArticle.Instructions__c = InstrRow;
        return recipeArticle;
    }

    public static void handleAfterUpdate(List<HMR_Recipe__c> approvedRecipes, Map<Id, HMR_Recipe__c> oldRecipesMap) {

        //Approval_Status__c is Approved
        // handler logic
        Set<Id> recipeIds = new Set<Id>();
        //get all Recipe Id's
		for(HMR_Recipe__c approvedRecipe: approvedRecipes) {
            if(approvedRecipe.Approval_Status__c == 'Approved') {
                recipeIds.add(approvedRecipe.Id);
            }
        }
        Map<Id, HMR_Recipe__c> approvedRecipesMap = new Map<Id, HMR_Recipe__c>(
            [SELECT Id, Recipe_Name__c, Desciption__c, Contact__c FROM HMR_Recipe__c WHERE Id In :recipeIds]);

        Set<Id> approvedRecipeIds = new Set<Id>();

        for(HMR_Recipe__c approvedRecipe: approvedRecipesMap.values()) {
            approvedRecipeIds.add(approvedRecipe.Id);
        }

        //get child Ingredients records
        Map<Id, Recipe_Ingredients__c> ingredients = new Map<Id, Recipe_Ingredients__c>(
            [SELECT Id, HMR_Product__r.ccrz__SKU__c,Name, Quantity__c, Quantity_Type__c, Ingredient_Type__c, Description__c  FROM Recipe_Ingredients__c
             WHERE Recipe__c In :approvedRecipeIds]);

        //get child Instructions records
        Map<Id, Recipe_Instruction__c> instructions = new Map<Id, Recipe_Instruction__c>(
            [SELECT Id, Instruction__c, Step_Number__c  FROM Recipe_Instruction__c
             WHERE Recipe__c In :approvedRecipeIds ORDER BY Step_Number__c ASC]);

        //instantiate list of Articles to insert
        List<Recipe__kav> recipeArticlesToInsertUpdate = new List<Recipe__kav>();
		List<Id> recipeKAVIds = new List<Id>();
        List<String> recipeKAVArticleNumbers = new List<String>();

        //Retrieve map of already Published Recipe Articles that match the HMR Recipe objects in the trigger
        Map<Id, Recipe__kav> recipeKAVMap = new Map<Id, Recipe__kav>(
            [SELECT Id, Recipe__c, ArticleNumber, KnowledgeArticleId, Language, PublishStatus  FROM Recipe__kav
             	WHERE Recipe__c IN :recipeIds AND PublishStatus = 'Online' AND Language = 'en_US']);

        //Check the map to see if the recipe in the loop has an existing published Article
        //if there are none in the list, insert fresh Knowledge Articles
        if(recipeKAVMap.values().size() == 0) {
            //Loop through appoved Recipes and create new Recipe Article Versions
            for(HMR_Recipe__c approvedRecipe: approvedRecipesMap.values()) {
                //new Article record
                Recipe__kav recipeArticle = new Recipe__kav();
                recipeArticlesToInsertUpdate.add(upsertRecipeKAV(ingredients, instructions, approvedRecipe, recipeArticle));
            }
        }
        //If there are existing Articles that match the HMR Recipe, call the PublishingService class
        //to create a new version of a published Article
        //then map the updated values from the HMR Recipe object and prepare this version for updating
        else {
            if(recipeKAVMap.values().size() > 0) {
                for(Recipe__kav recipeKAV: recipeKAVMap.values()) {
                    String recipeKAVId = KbManagement.PublishingService.editOnlineArticle(recipeKAV.KnowledgeArticleId, true);
                    if(recipeKAVId == null) {
                        System.debug('Recipe not published yet!');
                    }
                    recipeKAVArticleNumbers.add(recipeKAV.ArticleNumber);
                }
            }

            //Retrieve the newly create draft Article Version
            Map<Id, Recipe__kav> draftRecipeKAVMap = new Map<Id, Recipe__kav>(
                [SELECT Id, Recipe__c, KnowledgeArticleId, ArticleNumber, Language, PublishStatus  FROM Recipe__kav
                    WHERE Recipe__c IN :recipeIds AND PublishStatus = 'Draft' AND Language = 'en_US']);

            //Loop through Recipes and prepare data
            if(draftRecipeKAVMap.values().size() > 0) {
                for(Recipe__kav draftRecipeKAV: draftRecipeKAVMap.values()) {
                    for(HMR_Recipe__c approvedRecipe: approvedRecipesMap.values()) {
                        if(approvedRecipe.Id == draftRecipeKAV.Recipe__c) {

                            draftRecipeKAV.Submitted_By__c = approvedRecipe.Contact__c;

                            String IngrdRow = '';

                            for(Recipe_Ingredients__c Ingred:ingredients.values()){
                                if(Ingred.Quantity_Type__c != null) {
                                    IngrdRow += Ingred.Quantity__c + ' ' + Ingred.Quantity_Type__c + ' ' + Ingred.Name + '<br />';
                                } else {
                                    IngrdRow += Ingred.Quantity__c + ' ' + Ingred.Name + '<br />';
                                }
                            }
                            draftRecipeKAV.Ingredients__c = IngrdRow;

                            String InstrRow = '';

                            for(Recipe_Instruction__c Instruct:instructions.values()){
                                if(Instruct.Instruction__c!=null){
                                    InstrRow += Instruct.Step_Number__c + '. ' + Instruct.Instruction__c + '<br />';
                                }
                            }

                            draftRecipeKAV.Instructions__c = InstrRow;

                            //Add Articles to list to Update
                            recipeArticlesToInsertUpdate.add(draftRecipeKAV);
                        }
                    }
                }
            }

        }

        try {

            if(!recipeArticlesToInsertUpdate.isEmpty()) {
            	//DML operation to upsert Knowledge articles
            	upsert recipeArticlesToInsertUpdate;
            }
        }

        catch(Exception ex) {
            System.debug('@@@@@@@@@@');
            System.debug(ex.getMessage());
            System.debug('@@@@@@@@@@');
        }
    }
}