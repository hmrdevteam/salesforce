/**
* Test Class Coverage of the HMR_CMS_RecipeHome_Hero_ContentTemplate
*
* @Date: 03/26/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 03/26/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_RecipeHome_Hero_Test {
	@isTest
    private static void testGetHTML(){
        HMR_CMS_RecipeHome_Hero_ContentTemplate controller = new HMR_CMS_RecipeHome_Hero_ContentTemplate();

        Test.startTest();
        
        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }
    
    @isTest
    private static void testGenericPropertyMethods(){
        HMR_CMS_RecipeHome_Hero_ContentTemplate controller = new HMR_CMS_RecipeHome_Hero_ContentTemplate();
        
        Test.startTest();
        
        String propertyDefaultValue = controller.getPropertyWithDefault('heroImage', 'image');
        String attributeDefaultValue = controller.getAttribute('heroImage');
        
        Test.stopTest();
        
        //Verify Values
        System.assert(!String.isBlank(propertyDefaultValue));
        System.assert(String.isBlank(attributeDefaultValue));
        System.assertEquals(propertyDefaultValue, 'image');
    }
}