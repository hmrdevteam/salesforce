/*
Developer: Jay Zincone (HMR)
Date: 2018-01-29
Modified: 
Modified Date : 
Target Class: HMR_CMS_Home_Hero_Ver2_ContentTemplate
*/

@isTest
public class HMR_CMS_Home_Hero_Ver2_ContTemp_Test{  

        
    @isTest static void test_general_method() { 

                
                HMR_CMS_Home_Hero_Ver2_ContentTemplate homePageHero = new HMR_CMS_Home_Hero_Ver2_ContentTemplate();
                String propertyValue = homePageHero.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                String TitleText = homePageHero.TitleText;                
                String SubTitleText = homePageHero.SubTitleText;
                Boolean showBestDietsFastWeightLoss = homePageHero.showBestDietsFastWeightLoss;                
                Boolean showBestDietsWeightLoss = homePageHero.showBestDietsWeightLoss;
                
                homePageHero.testAttributes = new Map<String, String>  {
                    'PlansDetailsTitle' => 'title',
                    'SubTitleText' => 'desc',
                    'showBestDietsFastWeightLoss' => 'TRUE',
                    'showBestDietsWeightLoss' => 'TRUE'
                };
                
                String renderHTML = homePageHero.getHTML();
                
                System.assert(!String.isBlank(renderHTML));
                
    }
    
}