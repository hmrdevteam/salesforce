/**
* Apex controller for reorder page
* Mockup HTML: Categories
* @Date: 03/29/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Zach Engman (Magnet 360)
* @requirements:
*   1. From the Order Id, you need all Order items. These are represented by the items that have a qty != 0 in the screenshot.
*
*   2. Next, you’ll need to do some work to gather the rest of the available options.
*
*       a. From the Order Id, you’ll get the Program Membership Id, and from there you’ll get the Standard Kit.
*       b. Once you have the kit, you’ll read from the Composite Products to get the full list of the other products that should be options (basically all SKUs that aren’t already in the Order Item List)
*       c. Create these products as Order Items and add them to the above list
*
*   3. Next, you’ll need to look at each Order Item —> Product —> Product Category where the Category != HMR Products. This will give you the categorization that you need.
*
*   4. Finally, you’ll need to get all Products from the Bars Category and add them to the list with 0 qty. These are not part of the kit, but can find their way onto orders, and should be available to add to a re-order.
*/


public class HMR_CC_Reorder_Controller {

    public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
    }

    //program memeber ship of this order
    public string programMembershipId {get;set;}
    //contat ID
    public string contactId {get;set;}
    /****those two above are for retrieving template order****/

    public List<String> categoryKeyList {
        get{
            return categoryKeyList = new List<String>{'Shakes/Cereal','Entrees','Bars/Flavoring'};
                }
        set;
    }

    public string orderId {get;set;}

    public string standardKitSKU {get;set;}

    public string encOrderId {get;set;}

    public string isCSRFlow {get;set;}

    public string portalUser {get;set;}

    public boolean oneTimeOrder {get;set;}

    public String userFirstName { get; set; }

    public String programType { get; set; }

    //map<sku, oderItem> current order in cart
    public map<string, ccrz__E_OrderItem__c> orderItemMap {get;set;}
    //map<sku, product> current order in cart
    public map<string, ccrz__E_Product__c> productMap {get;set;}
    public map<string, ccrz__E_Category__c> categoryMap {get;set;}

    //final front end output
    public OrderWrapper orderWrap {get;set;}

    //for front end use
    //key: Category.Name
    //value: Category with Itemlist
    public map<string, CategoryWrapper> wrapMap {get;set;}

    //save button clicked
    public PageReference saveAdjustedOrder(){
        List<ccrz__E_OrderItem__c> upsertMe = new List<ccrz__E_OrderItem__c>();
        List<ccrz__E_OrderItem__c> deleteMe = new List<ccrz__E_OrderItem__c>();
        List<ccrz__E_Order__c> templateOrderUpdate = new List<ccrz__E_Order__c>();
        String templateOrderId = '';

        //update this order anyway
        Map<String, List<ccrz__E_OrderItem__c>> m = orderWrap.orderType == 'P2 1st Order' ?
                                                    adjustP2FirstOrderItems(wrapMap, orderItemMap, orderId) :
                                                    adjustOrderItems(wrapMap, orderItemMap, orderId);
        upsertMe.addAll(m.get('upsert'));
        deleteMe.addAll(m.get('delete'));

        //P2 1st Order should always updated the template
        if(orderWrap.orderType == 'P2 1st Order')
            oneTimeOrder = false;

        //not one time thing, so update the template order as well.
        if(!oneTimeOrder){
            //query out the template order and pass it to util method to process
            List<ccrz__E_Order__c> templateOrder = [SELECT Template_Updated__c
                                                          ,hmr_Program_Membership__c
                                                          ,hmr_Program_Membership__r.Program__r.Standard_Kit__c,
                                                    (
                                                        SELECT Id
                                                        ,ccrz__Price__c
                                                        ,ccrz__Quantity__c
                                                        ,ccrz__Product__c
                                                        ,ccrz__Product__r.ccrz__SKU__c
                                                        ,ccrz__OrderLineType__c
                                                        FROM ccrz__E_OrderItems__r WHERE ccrz__ProductType__c <> 'Coupon'
                                                    )
                                                    FROM ccrz__E_Order__c
                                                    WHERE ccrz__Contact__c = : contactId
                                                    AND hmr_Program_Membership__c = : programMembershipId
                                                    AND ccrz__OrderStatus__c = 'Template'
                                                   ];
            if(templateOrder.size() > 0){
                templateOrderId = templateOrder[0].Id;
                //compose the template order map
                Map<String, ccrz__E_OrderItem__c> templateOrderItemMap = new Map<String, ccrz__E_OrderItem__c>();

                for(ccrz__E_OrderItem__c im: templateOrder[0].ccrz__E_OrderItems__r){
                    templateOrderItemMap.put(im.ccrz__Product__r.ccrz__SKU__c, im);
                }

                m = adjustOrderItems(wrapMap, templateOrderItemMap, templateOrderId);

                upsertMe.addAll(m.get('upsert'));
                deleteMe.addAll(m.get('delete'));
                templateOrder[0].Template_Updated__c = true;
                templateOrderUpdate.add(templateOrder[0]);
            }
        }
        //Prevent Duplicates
        Set<ccrz__E_OrderItem__c> upsertSet = new Set<ccrz__E_OrderItem__c>();
        Set<ccrz__E_OrderItem__c> deleteSet = new Set<ccrz__E_OrderItem__c>();
        List<ccrz__E_OrderItem__c> upsertList = new List<ccrz__E_OrderItem__c>();
        List<ccrz__E_OrderItem__c> deleteList = new List<ccrz__E_OrderItem__c>();

        upsertSet.addAll(upsertMe);
        deleteSet.addAll(deleteMe);

        upsertList.addAll(upsertSet);
        deleteList.addAll(deleteSet);

        upsert upsertList;
        delete deleteList;

        //Adjust Order for Business Rules
        applyBusinessRules(templateOrderId);

        if(templateOrderUpdate.size()>0){
            update templateOrderUpdate;
        }

        String baseURL = 'https://' + ApexPages.currentPage().getHeaders().get('Host');
        if(ApexPages.currentPage().getParameters().get('portalUser') != null && ApexPages.currentPage().getParameters().get('portalUser') != '') {
            baseURL += '/apex/cms__Main?name=order-view&o=';
        } else {
            baseURL += '/order-view?o=';
        }

        PageReference pr = new PageReference(baseUrl+encOrderId+'&store=DefaultStore&cclcl=en_US&portalUser='+portalUser+'&isCSRFlow='+isCSRFlow);
        pr.setRedirect(true);
        return pr;
    }

    private void applyBusinessRules(String templateOrderId){
        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderId, orderWrap.reorderCoupon);
        orderService.processBusinessRules();
        if(!oneTimeOrder && !String.isBlank(templateOrderId)){
            orderService = new HMR_CC_Order_Service(templateOrderId, orderWrap.reorderCoupon);
            orderService.processBusinessRules();
        }
    }

    private Map<String, List<ccrz__E_OrderItem__c>> adjustP2FirstOrderItems(Map<String, CategoryWrapper> orderItemMap, Map<String, ccrz__E_OrderItem__c> existingOrderItemMap, string orderId){
        Map<String, List<ccrz__E_OrderItem__c>> orderOperationToOrderItemListMap = new Map<String, List<ccrz__E_OrderItem__c>>();
        Map<String, ccrz__E_OrderItem__c> kitOrderItemMap = new Map<String, ccrz__E_OrderItem__c>();
        List<ccrz__E_OrderItem__c> parentKitOrderItemList = [SELECT ccrz__Category__c
                                                             FROM ccrz__E_OrderItem__c
                                                             WHERE ccrz__Order__c =: orderId
                                                                AND ccrz__OrderLineType__c = 'Major'];

        if(parentKitOrderItemList.size() > 0){
            ccrz__E_OrderItem__c parentKitOrderItemRecord = parentKitOrderItemList[0];

            //Get existing kit item records
            for(ccrz__E_OrderItem__c kitOrderItem : [SELECT ccrz__Category__c
                                                           ,ccrz__OriginalQuantity__c
                                                           ,ccrz__RequestDate__c
                                                           ,ccrz__OrderLineType__c
                                                           ,ccrz__StoreId__c
                                                           ,ccrz__UnitOfMeasure__c
                                                           ,ccrz__Product__c
                                                           ,ccrz__Quantity__c
                                                           ,ccrz__Product__r.ccrz__SKU__c
                                                           ,ccrz__ParentOrderItem__c
                                                     FROM ccrz__E_OrderItem__c
                                                     WHERE ccrz__ParentOrderItem__c =: parentKitOrderItemRecord.Id])
            {
                kitOrderItemMap.put(kitOrderItem.ccrz__Product__r.ccrz__SKU__c, kitOrderItem);
            }


            //Instantiate the return map
            orderOperationToOrderItemListMap.put('upsert', new List<ccrz__E_OrderItem__c>());
            orderOperationToOrderItemListMap.put('delete', new List<ccrz__E_OrderItem__c>());

            for(string key : orderItemMap.keySet()){
                for(ItemDetailWrapper orderItem : orderItemMap.get(key).itemList){
                    ccrz__E_OrderItem__c existingItem = kitOrderItemMap.get(orderItem.sku);


                    if(existingItem != null){
                        if(orderItem.quantity == 0)
                            orderOperationToOrderItemListMap.get('delete').add(existingItem);
                        else if(orderItem.quantity != existingItem.ccrz__Quantity__c){
                            existingItem.ccrz__Quantity__c = orderItem.quantity;
                            existingItem.ccrz__OriginalQuantity__c = orderItem.quantity;
                            existingItem.ccrz__Price__c = productMap.get(orderItem.sku).ccrz__E_PriceListItems__r[0].ccrz__Price__c;
                            existingItem.ccrz__SubAmount__c = existingItem.ccrz__Price__c * existingItem.ccrz__Quantity__c;
                            orderOperationToOrderItemListMap.get('upsert').add(existingItem);
                        }
                    }
                    else if(orderItem.quantity != 0){
                        ccrz__E_OrderItem__c kitOrderItemRecord = new ccrz__E_OrderItem__c();
                        kitOrderItemRecord.ccrz__Order__c = orderId;
                        kitOrderItemRecord.ccrz__Product__c = productMap.get(orderItem.sku).Id;
                        kitOrderItemRecord.ccrz__ProductType__c = 'Product';
                        kitOrderItemRecord.ccrz__Quantity__c = orderItem.quantity;
                        kitOrderItemRecord.ccrz__Price__c = productMap.get(orderItem.sku).ccrz__E_PriceListItems__r[0].ccrz__Price__c;
                        kitOrderItemRecord.ccrz__SubAmount__c = kitOrderItemRecord.ccrz__Price__c * kitOrderItemRecord.ccrz__Quantity__c;
                        kitOrderItemRecord.ccrz__ParentOrderItem__c = parentKitOrderItemRecord.Id;
                        kitOrderItemRecord.ccrz__Category__c = parentKitOrderItemRecord.ccrz__Category__c;
                        kitOrderItemRecord.ccrz__OriginalQuantity__c = orderItem.quantity;
                        kitOrderItemRecord.ccrz__RequestDate__c = Date.today();
                        kitOrderItemRecord.ccrz__OrderLineType__c = 'Minor';
                        kitOrderItemRecord.ccrz__StoreId__c = 'DefaultStore';
                        kitOrderItemRecord.ccrz__UnitOfMeasure__c = 'Each';
                        orderOperationToOrderItemListMap.get('upsert').add(kitOrderItemRecord);
                    }
                }
            }
        }

        return orderOperationToOrderItemListMap;
    }

    //util method for adjusting the order/items
    private Map<String, List<ccrz__E_OrderItem__c>> adjustOrderItems(map<string, CategoryWrapper> newItems, map<string, ccrz__E_OrderItem__c> existingOrders, string orderIdPara){

        //init the returm map
        map<string, list<ccrz__E_OrderItem__c>> returnMe = new map<string, list<ccrz__E_OrderItem__c>>();
        returnMe.put('upsert',new list<ccrz__E_OrderItem__c>());
        returnMe.put('delete',new list<ccrz__E_OrderItem__c>());
        for(string k : newItems.keySet()){
            for(ItemDetailWrapper item: newItems.get(k).itemList){
                //try to get this item for existing order
                ccrz__E_OrderItem__c existingItem = existingOrders.get(item.sku);

                if(existingItem != null){
                    //existing item found.
                    if(item.quantity== 0){
                        //this orderItem is being deleted
                        returnMe.get('delete').add(existingItem);
                    }else if(item.quantity != existingItem.ccrz__Quantity__c){
                        //this order item has been changed quantity
                        existingItem.ccrz__Quantity__c = item.quantity;
                        //price from price list item of that product
                        if(productMap.get(item.sku).ccrz__E_PriceListItems__r.size()>0){
                            existingItem.ccrz__Price__c = productMap.get(item.sku).ccrz__E_PriceListItems__r[0].ccrz__Price__c;
                            existingItem.ccrz__SubAmount__c = existingItem.ccrz__Price__c * existingItem.ccrz__Quantity__c;
                        }
                        returnMe.get('upsert').add(existingItem);
                    }
                }else{
                    //new item not in existing order
                    if(item.quantity != 0){
                        if(productMap.get(item.sku)!=null){
                            //create a new order item and add to that cart.
                            ccrz__E_OrderItem__c newItem = new ccrz__E_OrderItem__c();
                            newItem.ccrz__ProductType__c = 'Product';
                            //newItem.ccrz__OrderItemStatus__c = 'Pending';
                            //order ID form URL
                            newItem.ccrz__Order__c = orderIdPara;
                            //product form Product Map
                            newItem.ccrz__Product__c = productMap.get(item.sku).Id;
                            //quantity form wrapper
                            newItem.ccrz__Quantity__c = item.quantity;
                            //Order Line Type required for Order view
                            newItem.ccrz__OrderLineType__c = 'Major';
                            //price from price list item of that product
                            if(productMap.get(item.sku).ccrz__E_PriceListItems__r.size()>0){
                                newItem.ccrz__Price__c = productMap.get(item.sku).ccrz__E_PriceListItems__r[0].ccrz__Price__c;
                                newItem.ccrz__SubAmount__c = newItem.ccrz__Price__c * newItem.ccrz__Quantity__c;
                            }
                            returnMe.get('upsert').add(newItem);
                        }
                    }
                }
            }
        }
        return returnMe;
    }



    public HMR_CC_Reorder_Controller() {
        //get Id from URL
        portalUser = ApexPages.currentPage().getParameters().get('portalUser');

        if(String.isBlank(portalUser))
            portalUser = '';
        else
            portalUser = String.escapeSingleQuotes(portalUser);

        isCSRFlow = ApexPages.currentPage().getParameters().get('isCSRFlow');
        encOrderId = ApexPages.currentPage().getParameters().get('Id');
        oneTimeOrder = false;

        if(!String.isBlank(encOrderId)){
            encOrderId = String.escapeSingleQuotes(encOrderId);

            try{
                //init category map
                categoryMap = new map<string, ccrz__E_Category__c>();

                for(ccrz__E_Category__c c: [Select Name, Id From ccrz__E_Category__c])
                    categoryMap.put(c.Name, c);

                //get contact ID
                Id userId = String.isBlank(portalUser) ? UserInfo.getUserId() : portalUser;

                User u = [SELECT FirstName, ContactID from User WHERE Id =: userId];
                userFirstName = u.FirstName;
                Id conId = u.ContactId;

                //get Account Group from Account From Contact
                Id accGrouId = [SELECT Account.ccrz__E_AccountGroup__c FROM Contact WHERE Id=: conId].Account.ccrz__E_AccountGroup__c;
                //get the price list from Account Group Price List
                Id priceListId = [SELECT ccrz__Pricelist__c from ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c =: accGrouId].ccrz__Pricelist__c;

                //Query all order Item and query the program for standard Kit
                ccrz__E_Order__c ccOrder = [
                    SELECT ccrz__Contact__c,
                    ccrz__Account__c,
                    ccrz__Account__r.Name,
                    hmr_Program_Membership__c,
                    hmr_Program_Membership__r.Program__c,
                    hmr_Program_Membership__r.Program__r.Standard_Kit__c,
                    hmr_Program_Membership__r.Program__r.Standard_Kit__r.ccrz__SKU__c,
                    hmr_Program_Membership__r.Program__r.hmr_Program_Type__c,
                    ccrz__ShipMethod__c,
                    ccrz__ShipAmount__c,
                    ccrz__SubtotalAmount__c,
                    ccrz__TaxAmount__c,
                    ccrz__TaxSubTotalAmount__c,
                    ccrz__TotalAmount__c,
                    ccrz__TotalDiscount__c,
                    ccrz__TotalSurcharge__c,
                    hmr_Order_Type_c__c,
                    Order_Value_Discount_Amount__c,
                    (
                        SELECT Id, ccrz__Price__c, ccrz__Quantity__c, ccrz__Product__c,ccrz__Product__r.ccrz__SKU__c, ccrz__OrderLineType__c
                        FROM ccrz__E_OrderItems__r WHERE ccrz__ProductType__c <> 'Coupon'
                    )
                    FROM ccrz__E_Order__c
                    WHERE ccrz__EncryptedId__c = : encOrderId
                ];
                programType = ccOrder.hmr_Program_Membership__r.Program__r.hmr_Program_Type__c;
                orderId = ccOrder.Id;
                //init for future use, if needed update template order
                contactId = ccOrder.ccrz__Contact__c;
                programMembershipId = ccOrder.hmr_Program_Membership__c;

                //build a existing order item map. save for later use
                orderItemMap = new map<string, ccrz__E_OrderItem__c>();
                for(ccrz__E_OrderItem__c oi: ccOrder.ccrz__E_OrderItems__r){
                    orderItemMap.put(oi.ccrz__Product__r.ccrz__SKU__c, oi);
                }

                if(ccOrder.hmr_Program_Membership__r.Program__r.Standard_Kit__c != null){

                    //set of all components ID
                    Set<Id> componentsIdSet = new Set<Id>();
                    //all bars Id
                    Set<Id> barsAndEntreesIdSet = new Set<Id>();
                    //For validation usage on front end
                    standardKitSKU = ccOrder.hmr_Program_Membership__r.Program__r.Standard_Kit__r.ccrz__SKU__c;

                    //query against CompositeProduct object and get all the components product ID
                    for(ccrz__E_CompositeProduct__c cp:[SELECT ccrz__Component__c FROM ccrz__E_CompositeProduct__c WHERE ccrz__Composite__c = : ccOrder.hmr_Program_Membership__r.Program__r.Standard_Kit__c]){
                        componentsIdSet.add(cp.ccrz__Component__c);
                    }

                    //query product category to get all products available to reorder incuding flavorings
                    for(ccrz__E_ProductCategory__c cp:[Select ccrz__Product__c, ccrz__Category__c From ccrz__E_ProductCategory__c WHERE ccrz__Category__c=: categoryMap.get('Bars').Id
                                                                                                                                        OR ccrz__Category__c=: categoryMap.get('Entrees').Id
                                                                                                                                        OR ccrz__Category__c=: categoryMap.get('Shake/Soup').Id
                                                                                                                                        OR ccrz__Category__c=: categoryMap.get('Flavorings').Id]){
                        barsAndEntreesIdSet.add(cp.ccrz__Product__c);
                    }

                    //query product category to get all Materials products IF in a CSR Flow
                    if(portalUser != '' && portalUser != null){
                        for(ccrz__E_ProductCategory__c cp:[Select ccrz__Product__c, ccrz__Category__c From ccrz__E_ProductCategory__c WHERE ccrz__Category__c=: categoryMap.get('Materials').Id]){
                            barsAndEntreesIdSet.add(cp.ccrz__Product__c);
                        }
                    }

                    //get the standard kit and all the Composite Products and price list
                    List<ccrz__E_Product__c> standardKitComponentsAndBars = new List<ccrz__E_Product__c>();
                    standardKitComponentsAndBars =  [
                        SELECT ccrz__SKU__c, Name, ccrz__ProductType__c, Accessory_Product__c,
                        (
                            SELECT ccrz__Product__c, ccrz__Category__c,ccrz__Category__r.Name
                            From ccrz__Product_Categories__r //query category
                            WHERE ccrz__Category__c != :categoryMap.get('HMR Products').Id
                        ),
                        (
                            SELECT ccrz__Price__c
                            From ccrz__E_PriceListItems__r //query Price
                            WHERE ccrz__Pricelist__c = :priceListId
                        )
                        FROM ccrz__E_Product__c
                        WHERE ID in: componentsIdSet
                        OR (ID in: barsAndEntreesIdSet
                            AND ccrz__ProductStatus__c = 'Released'
                            AND (ccrz__StartDate__c = null OR ccrz__StartDate__c <= TODAY)
                            AND (ccrz__EndDate__c = null OR ccrz__EndDate__c >= TODAY))
                    ];

                    productMap = new map<string, ccrz__E_Product__c>();
                    //start building the category wrapper
                    wrapMap = new map<string, CategoryWrapper>();
                    for(string keyName: categoryKeyList){
                        wrapMap.put(keyName, new CategoryWrapper(keyName,'instruction place holder+++'));
                    }
                    for(ccrz__E_Product__c p: standardKitComponentsAndBars){
                        if(p.ccrz__Product_Categories__r.size()>0 && p.ccrz__Product_Categories__r[0].ccrz__Category__c!=null){

                            string catName = '';
                            string iName = p.ccrz__Product_Categories__r[0].ccrz__Category__r.Name;

                            if(iName.containsIgnoreCase('shake') || iName.containsIgnoreCase('120') || iName.containsIgnoreCase('cereal')){
                                catName= 'Shakes/Cereal';
                            }else if(iName.containsIgnoreCase('entree')){
                                catName = 'Entrees';
                            }else{
                                catName = 'Bars/Flavoring';
                            }

                            //if this product is already in previous order
                            ItemDetailWrapper wr = new ItemDetailWrapper(p);
                            if(orderItemMap.get(wr.sku)!=null){
                                wr.quantity = orderItemMap.get(wr.sku).ccrz__Quantity__c;
                            }
                            wrapMap.get(catName).itemList.add(wr);

                        }

                        //compose a map of all product
                        productMap.put(p.ccrz__SKU__c, p);
                    }

                    //**********************************
                    //final front end output creation
                    orderWrap = new OrderWrapper(ccOrder, wrapMap);
                }
            }
            catch(Exception e){
                System.Debug(e);
            }
        }
    }

    public integer entreeMinimum{get{
        if(entreeMinimum == null){
            for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                if(orderRequirement.DeveloperName == 'Entree_Minimum'){
                    entreeMinimum = Integer.valueOf(orderRequirement.Requirement_Value__c);
                    break;
                }
            }
        }

        return entreeMinimum;
    }set;}

    public integer shakeMinimum{get{
        if(shakeMinimum == null){
            for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                if(orderRequirement.DeveloperName == 'Shake_Minimum'){
                    shakeMinimum = Integer.valueOf(orderRequirement.Requirement_Value__c);
                    break;
                }
            }
        }

        return shakeMinimum;
    }set;}


    public integer lactoseFreeShakeMinimum{get{
        if(lactoseFreeShakeMinimum == null){
            for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                if(orderRequirement.DeveloperName == 'Lactose_Free_Shake_Minimum'){
                    lactoseFreeShakeMinimum = Integer.valueOf(orderRequirement.Requirement_Value__c);
                    break;
                }
            }
        }

        return lactoseFreeShakeMinimum;
    }set;}

    public integer orderTotalP2{get{
        if(orderTotalP2 == null){
            for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                if(orderRequirement.DeveloperName == 'P2_Order_Total'){
                    orderTotalP2 = Integer.valueOf(orderRequirement.Requirement_Value__c);
                    break;
                }
            }
        }

        return orderTotalP2;
    }set;}

    public integer csrOrderTotalP2{get{
        if(csrOrderTotalP2 == null){
            for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                if(orderRequirement.DeveloperName == 'CSR_P2_Order_Total'){
                    csrOrderTotalP2 = Integer.valueOf(orderRequirement.Requirement_Value__c);
                    break;
                }
            }
        }

        return csrOrderTotalP2;
    }set;}

    //Order Validations
    @testVisible
    private List<Order_Requirement__mdt> orderRequirementList{get{
        if(orderRequirementList == null)
            orderRequirementList = [SELECT DeveloperName, Requirement_Value__c FROM Order_Requirement__mdt WHERE IsActive__c = true];

        return orderRequirementList;
    } set;}


    public integer csrShakeMinimum{
        get{
            if(csrShakeMinimum == null){
                for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                    if(orderRequirement.DeveloperName == 'CSR_Shake_Minimum'){
                        csrShakeMinimum = Integer.valueOf(orderRequirement.Requirement_Value__c);
                        break;
                    }
                }
            }
            return csrShakeMinimum;
        }
        set;
    }

    public integer csrEntreeMinimum{
        get{
            if(csrEntreeMinimum == null){
                for(Order_Requirement__mdt orderRequirement : orderRequirementList){
                    if(orderRequirement.DeveloperName == 'CSR_Entree_Minimum'){
                        csrEntreeMinimum = Integer.valueOf(orderRequirement.Requirement_Value__c);
                        break;
                    }
                }
            }
            return csrEntreeMinimum;
        }
        set;
    }

    //Order and all order items with category
    public class OrderWrapper{
        public decimal shipAmount {get; set;}
        public decimal subtotalAmount {get; set;}
        public decimal taxAmount {get; set;}
        public decimal taxTotalAmount {get; set;}
        public decimal totalAmount {get; set;}
        public decimal totalDiscount {get; set;}
        public decimal totalSurcharge {get; set;}
        public decimal orderValueDiscount {get; set;}
        //public boolean allowOrderValueDiscount {get; set;}
        public string orderType {get; set;}
        public Map<string, CategoryWrapper> wrapMap {get;set;}
        public string accountName {get; set;}
        public ccrz__E_Coupon__c reorderCoupon {get;set;}
        public string couponType {get; set;}
        public decimal couponOrderTotalAmount {get; set;}
        public decimal couponAmount {get; set;}
        public string shipMethod {get; set;}
        public boolean isP2 {get; set;}
        public OrderWrapper(ccrz__E_Order__c p, map<string, CategoryWrapper> w){
            this.accountName = p.ccrz__Account__r.Name;
            this.shipMethod = p.ccrz__ShipMethod__c;
            this.shipAmount = p.ccrz__ShipAmount__c;
            this.subtotalAmount = p.ccrz__SubtotalAmount__c;
            this.taxAmount = p.ccrz__TaxAmount__c;
            this.taxTotalAmount = p.ccrz__TaxSubTotalAmount__c;
            this.totalAmount = p.ccrz__TotalAmount__c;
            this.totalDiscount = p.ccrz__TotalDiscount__c;
            this.totalSurcharge = p.ccrz__TotalSurcharge__c;
            this.orderValueDiscount = p.Order_Value_Discount_Amount__c;
            this.orderType = p.hmr_Order_Type_c__c;
            this.isP2 = p.hmr_Order_Type_c__c.containsIgnoreCase('P2');
            this.wrapMap = w;

            if(!String.isBlank(p.hmr_Program_Membership__r.Program__c)){
                String hmrCouponId = getReorderHMRCouponId(p.ccrz__Contact__c, p.ccrz__Account__c, p.hmr_Program_Membership__c, p.hmr_Order_Type_c__c);
                if(!String.isBlank(hmrCouponId)){
                    List<ccrz__E_Coupon__c> reorderCouponList = HMR_CC_Coupon_Selector.getCouponListByHMRCouponIdAndProgramIdAndOrderType(hmrCouponId, p.hmr_Program_Membership__r.Program__c, p.hmr_Order_Type_c__c);
                    if(reorderCouponList.size() > 0){
                        this.reorderCoupon = reorderCouponList[0];
                        this.couponType = reorderCouponList[0].ccrz__CouponType__c;
                        this.couponOrderTotalAmount = reorderCouponList[0].ccrz__CartTotalAmount__c;
                        this.couponAmount = reorderCouponList[0].ccrz__DiscountAmount__c;
                    }
                }
            }

            for(string k : wrapMap.keySet()){
                wrapMap.get(k).itemList.sort();
            }
        }

        private string getReorderHMRCouponId(string contactId, string accountId, string programMembershipId, string orderType){
            string hmrCouponId = '';
            //Get the template order
            List<ccrz__E_Order__c> templateOrder = [SELECT ccrz__Order__c
                                                    FROM ccrz__E_Order__c
                                                    WHERE ccrz__Contact__c = : contactId
                                                    AND hmr_Program_Membership__c = : programMembershipId
                                                    AND ccrz__OrderStatus__c = 'Template'
                                                   ];

            if(templateOrder.size() > 0){
                //Get the Coupon from the 1st Order
                List<ccrz__E_OrderItem__c> orderItemCoupon = [SELECT ccrz__Coupon__r.HMR_Coupon__c
                                                              FROM ccrz__E_OrderItem__c
                                                              WHERE ccrz__Order__c =: templateOrder[0].ccrz__Order__c
                                                              AND ccrz__ProductType__c = 'Coupon'];
                if(orderItemCoupon.size() > 0)
                    hmrCouponId = orderItemCoupon[0].ccrz__Coupon__r.HMR_Coupon__c;
            }

            //Updated 2017-07-13: If coupon does not exist on Template, derive from account if coupon exists
            if(String.isBlank(hmrCouponId) && !String.isBlank(accountId)){
                List<HMR_Coupon__c> hmrCouponList = [SELECT Id FROM HMR_Coupon__c WHERE Account__c =: accountId];
                if(hmrCouponList.size() > 0)
                    hmrCouponId = hmrCouponList[0].Id;
            }

            return hmrCouponId;
        }

    }

    //category for the kit
    public class CategoryWrapper implements Comparable{

        public String name {get; set;}

        public String instruction {get; set;}

        public list<ItemDetailWrapper> itemList {get;set;}

        public CategoryWrapper(string name, string instruction){
            this.name = name;
            this.instruction = instruction;
            itemList = new list<ItemDetailWrapper>();
        }

        public CategoryWrapper(string name, string instruction, List<ItemDetailWrapper> itemList){
            this.name = name;
            this.instruction = instruction;
            this.itemList = itemList;
            this.itemList.sort();
        }

        public Integer compareTo(Object compareTo){
            CategoryWrapper compareToCategory = (CategoryWrapper)compareTo;
            return name == compareToCategory.name ? 0 :
            name > compareToCategory.name ? 1 : -1;
        }

    }

    //SUBclass to hold item information
    //wrapper class for product and the
    public class ItemDetailWrapper implements Comparable{
        public string productId {get;set;}

        public string productType {get; set;}

        public string sku {get;set;}

        public string name {get;set;}

        public decimal price {get;set;}

        public decimal quantity {get;set;}

        public boolean accessoryProduct {get;set;}


        //empty constructor
        public ItemDetailWrapper(){

        }

        public ItemDetailWrapper(string sku, string name, decimal price, decimal quantity, boolean accessoryProduct){
            this.productId = sku;
            this.sku = sku;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
            this.accessoryProduct = accessoryProduct;
        }

        public ItemDetailWrapper(ccrz__E_Product__c p){
            productId = p.Id;
            productType = p.ccrz__ProductType__c;
            accessoryProduct = p.Accessory_Product__c;
            if(p.ccrz__SKU__c != null) sku = p.ccrz__SKU__c;
            if(p.Name != null) name = p.Name;
            if(p.ccrz__E_PriceListItems__r.size()>0) price = p.ccrz__E_PriceListItems__r[0].ccrz__Price__c;
            quantity = 0;
        }

        public Integer compareTo(Object compareTo){
            ItemDetailWrapper compareToItem = (ItemDetailWrapper)compareTo;
            integer compareResult = quantity == compareToItem.quantity ? 0 :
                                    quantity > compareToItem.quantity ? -1 : 1;

            if(compareResult == 0){
               if(accessoryProduct == true){
                     if(productType == compareToItem.productType)
                        compareResult = name == compareToItem.name ? 0 :
                                        name < compareToItem.name ? -1 : 1;
                     else
                        compareResult = 1;
                }
                else
                    compareResult = name == compareToItem.name ? 0 :
                                    name < compareToItem.name ? -1 : 1;
            }

            return compareResult;
        }
    }
}