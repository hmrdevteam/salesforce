/**
* Apex Service Class for Knowledge
*
* @Date: 2017-05-18
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-31
* @JIRA:
*/
public with sharing class HMR_Knowledge_Service {
    private Map<String, Set<String>> DataCategoryToArticleMap {get; set;}
    public string publishStatus = 'Online';

    public ArticleRecord getByName(string articleName){
        ArticleRecord articleRecord;
        List<KnowledgeArticleVersion> articleVersionList = database.query('SELECT Id FROM KnowledgeArticleVersion WHERE UrlName =: articleName AND PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\'');

        if(articleVersionList.size() > 0){
            Id knowledgeArticleId = articleVersionList[0].Id;
            articleRecord = getById(knowledgeArticleId);
        }

        return articleRecord;
    }

    public ArticleRecord getById(string id){
        Map<String, Set<Id>> articleTypeToIdSetMap = new Map<String, Set<Id>>();
        Map<String, Set<String>> knowledgeDataCategoryMap = getKnowledgeDataCategoryMap();

        articleTypeToIdSetMap.put(((Id)Id).getSObjectType().getDescribe().getName(), new Set<Id>{id});

        List<ArticleRecord> articleRecordList = getKnowledgeArticleVersionDetails(articleTypeToIdSetMap, 0, 0, '', false);

        return (articleRecordList.size() > 0) ? articleRecordList[0] : null;
    }

    public List<ArticleRecord> getByArticleType(String articleType){
        return getByArticleType(articleType, 0, 0, '');
    }

    public List<ArticleRecord> getByArticleType(String articleType, Integer recordLimit, Integer offset, String additionalWhereClause){
        List<ArticleRecord> articleRecordList = new List<ArticleRecord>();
        List<SObject> recordList = new List<SObject>();

        //Query by article type
        String soql = getStandardSelectClause(articleType);

        soql += 'FROM ' + articleType + ' ';
        soql += 'WHERE PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ' + additionalWhereClause + ' ';

        soql += 'ORDER BY LastPublishedDate DESC ';

        if(recordLimit > 0)
            soql += 'LIMIT ' + recordLimit + ' ';

        if(offset > 0)
            soql += 'OFFSET ' + offset + ' ';

        recordList.addAll(Database.query(soql));

        //Convert to article records
        for(sObject record : recordList)
            articleRecordList.add(new ArticleRecord(record, this.DataCategoryToArticleMap));

        //Append article votes
        articleRecordList = getArticleVotes(articleType, articleRecordList);

        return articleRecordList;
    }

    public Integer getCountByArticleType(String articleType){
        return getCountByArticleType(articleType, '');
    }

    public Integer getCountByArticleType(String articleType, String additionalWhereClause){
        Integer recordCount = 0;

        String soql = 'SELECT COUNT(Id) totalCount FROM ' + articleType + ' WHERE PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ' + additionalWhereClause;
        List<AggregateResult> resultList = Database.query(soql);

        if(resultList.size() > 0)
            recordCount = Integer.valueOf(resultList[0].get('totalCount'));

        return recordCount;
    }

    public LayoutCount getLayoutCountByArticleType(String articleType){
        return getLayoutCountByArticleType(articleType, '');
    }

    public LayoutCount getLayoutCountByArticleType(String articleType, String additionalWhereClause){
        LayoutCount layoutCountResult;
        String groupByField = articleType == 'Recipe__kav' ? 'Template_Layout__c' : 'Card_Layout__c';
        String soql = 'SELECT ' + groupByField + ', COUNT(Id) totalCount FROM ' + articleType + ' WHERE PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ' + additionalWhereClause + ' GROUP BY ' + groupByField;
        List<AggregateResult> resultList = Database.query(soql);
        integer topBottomCount = 0;
        integer leftRightCount = 0;

        for(AggregateResult result : resultList){
            string layoutType = String.valueOf(result.get(groupByField));
            if(layoutType == 'TopBottom')
                topBottomCount = Integer.valueOf(result.get('totalCount'));
            else
                leftRightCount = Integer.valueOf(result.get('totalCount'));
        }

        layoutCountResult = new LayoutCount(topBottomCount, leftRightCount);

        return layoutCountResult;
    }

    public List<ArticleRecord> getByCategory(string category){
        List<KnowledgeArticleVersion> masterArticleList = getKnowledgeMasterArticleListByCategory(category);
        List<ArticleRecord> articleRecordList = getKnowledgeArticleVersionDetails(masterArticleList, false);

        articleRecordList = deDuplicateArticleRecordList(articleRecordList);

        return articleRecordList;
    }

    public List<ArticleRecord> getByCategory(string category, Integer recordLimit, Integer offset, String additionalWhereClause){
        List<KnowledgeArticleVersion> masterArticleList = getKnowledgeMasterArticleListByCategory(category);
        List<ArticleRecord> articleRecordList = getKnowledgeArticleVersionDetails(masterArticleList, 0, 0, additionalWhereClause, false);
        List<ArticleRecord> resultList = new List<ArticleRecord>();

        articleRecordList = deDuplicateArticleRecordList(articleRecordList);

        //Need to manually do our limit/offset
        if(offset > 0){
            for(integer index = offset; index < articleRecordList.size(); index++)
                resultList.add(articleRecordList[index - 1]);
        }
        else
            resultList.addAll(articleRecordList);

        if(recordLimit > 0 && resultList.size() > recordLimit){
            while(resultList.size() > recordLimit)
                resultList.remove(resultList.size() - 1);
        }

        return resultList;
    }

    public Integer getCountByCategory(string category){
        List<KnowledgeArticleVersion> masterArticleList = getKnowledgeMasterArticleListByCategory(category);
        List<ArticleRecord> articleRecordList = getKnowledgeArticleVersionDetails(masterArticleList, 0, 0, 'AND Card_Layout__c != null', false);

        articleRecordList = deDuplicateArticleRecordList(articleRecordList);

        return articleRecordList.size();
    }

    public LayoutCount getLayoutCountByCategory(String category){
        LayoutCount layoutCountResult;
        List<KnowledgeArticleVersion> masterArticleList = getKnowledgeMasterArticleListByCategory(category);
        List<ArticleRecord> articleRecordList = getKnowledgeArticleVersionDetails(masterArticleList, 0, 0, 'AND Card_Layout__c != null', false);
        integer topBottomCount = 0;
        integer leftRightCount = 0;

        articleRecordList = deDuplicateArticleRecordList(articleRecordList);

        for(ArticleRecord articleRecord : articleRecordList){
            if(articleRecord.TemplateLayout == 'TopBottom')
                topBottomCount++;
            else
                leftRightCount++;
        }

        layoutCountResult = new LayoutCount(topBottomCount, leftRightCount);

        return layoutCountResult;
    }


    //Articles can be flagged to multiple categories, de-duplicate here
    private List<ArticleRecord> deDuplicateArticleRecordList(List<ArticleRecord> articleRecordList){
        List<ArticleRecord> deDupeList = new List<ArticleRecord>();
        Set<Id> idSet = new Set<Id>();
        for(ArticleRecord articleRecord : articleRecordList){
            if(!idSet.contains(articleRecord.Id)){
                deDupeList.add(articleRecord);
                idSet.add(articleRecord.Id);
            }
        }

        return deDupeList;
    }

    public Map<String, Set<String>> getKnowledgeDataCategoryMap(){
        Map<String, Set<String>> dataCategoryMap = new Map<String, Set<String>>();

        for (Blog_Post__DataCategorySelection dc : [SELECT DataCategoryName, ParentId FROM Blog_Post__DataCategorySelection]){
            if (dataCategoryMap.containsKey(dc.dataCategoryName))
                dataCategoryMap.get(dc.dataCategoryName).add(dc.ParentId);
            else
                dataCategoryMap.put(dc.datacategoryName, new Set<String> {dc.ParentId});
        }

        for (FAQ__DataCategorySelection dc : [SELECT DataCategoryName, ParentId FROM FAQ__DataCategorySelection]){
            if (dataCategoryMap.containsKey(dc.dataCategoryName))
                dataCategoryMap.get(dc.dataCategoryName).add(dc.ParentId);
            else
                dataCategoryMap.put(dc.datacategoryName, new Set<String> {dc.ParentId});
        }

        for (Recipe__DataCategorySelection dc : [SELECT DataCategoryName, ParentId FROM Recipe__DataCategorySelection]){
            if (dataCategoryMap.containsKey(dc.dataCategoryName))
                dataCategoryMap.get(dc.dataCategoryName).add(dc.ParentId);
            else
                dataCategoryMap.put(dc.datacategoryName, new Set<String> {dc.ParentId});
        }

        for (Success_Story__DataCategorySelection dc : [SELECT DataCategoryName, ParentId FROM Success_Story__DataCategorySelection]){
            if (dataCategoryMap.containsKey(dc.dataCategoryName))
                dataCategoryMap.get(dc.dataCategoryName).add(dc.ParentId);
            else
                dataCategoryMap.put(dc.datacategoryName, new Set<String> {dc.ParentId});
        }

        this.DataCategoryToArticleMap = dataCategoryMap;

        return dataCategoryMap;
    }

    public List<KnowledgeArticleVersion> getKnowledgeMasterArticleListByCategory(string category){
        List<KnowledgeArticleVersion> masterArticleList = new List<KnowledgeArticleVersion>();
        Map<String, Set<String>> knowledgeDataCategoryMap = getKnowledgeDataCategoryMap();
        List<String> articleIdList = new List<Id>();

        if (category == 'All'){
            for (string catName : knowledgeDataCategoryMap.keySet())
                articleIdList.addAll(knowledgeDataCategoryMap.get(catName));
        }
        else if (knowledgeDataCategoryMap.containsKey(category))
           articleIdList.addAll(knowledgeDataCategoryMap.get(category));

        if (!articleIdList.isEmpty()){
            masterArticleList = database.query('SELECT Summary, Title, ArticleType FROM KnowledgeArticleVersion WHERE PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' AND Id IN: articleIdList ORDER BY CreatedDate DESC');
        }

        return masterArticleList;
    }

    private List<ArticleRecord> getKnowledgeArticleVersionDetails(Map<String, Set<Id>> articleTypeToIdSetMap, Integer recordLimit, Integer offset, String additionalWhereClause, boolean featuredOnly){
        List<ArticleRecord> articleRecordList = new List<ArticleRecord>();
        List<SObject> recordList = new List<SObject>();

        //If success story is included, remove it if layout is part of query
        if((articleTypeToIdSetMap.containsKey('Success_Story__kav') || articleTypeToIdSetMap.containsKey('Success_Story__ka')) &&
           ((additionalWhereClause.containsIgnoreCase('Template_Layout__c') || additionalWhereClause.containsIgnoreCase('Card_Layout__c'))))
        {
            articleTypeToIdSetMap.remove('Success_Story__kav');
            articleTypeToIdSetMap.remove('Success_Story__ka');
        }

        //Query by article type
        for(String articleType : articleTypeToIdSetMap.keySet()){
            String soql = '';
            Set<Id> articleIdSet = articleTypeToIdSetMap.get(articleType);
            String whereField = 'Id';

            if(articleType.endsWith('__ka')){
                whereField = 'KnowledgeArticleId';
                articleType = articleType.replace('__ka', '__kav');
            }

            soql = getStandardSelectClause(articleType);

            soql += 'FROM ' + articleType + ' ';
            soql += 'WHERE ' + whereField + ' IN: articleIdSet AND PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ';

            if(!String.isBlank(additionalWhereClause)){
                if(additionalWhereClause.contains('Card_Layout__c') && articleType.equals('Recipe__kav'))
                    soql += additionalWhereClause.replace('Card_Layout__c', 'Template_Layout__c') + ' ';
                else
                    soql += additionalWhereClause.replace('Template_Layout__c', 'Card_Layout__c') + ' ';
            }

            if(featuredOnly)
                soql += ' AND Is_Featured__c = true ';

            soql += 'ORDER BY LastPublishedDate DESC ';

            if(recordLimit > 0)
                soql += 'LIMIT ' + recordLimit + ' ';

            if(offset > 0)
                soql += 'OFFSET ' + offset + ' ';

            recordList.addAll(Database.query(soql));
        }

        //Convert to article records
        for(sObject record : recordList){
            articleRecordList.add(new ArticleRecord(record, this.DataCategoryToArticleMap));
        }

        //Append article votes, must be by type
        for(String articleType : articleTypeToIdSetMap.keySet()){
            articleRecordList = getArticleVotes(articleType, articleRecordList);
        }

        return articleRecordList;
    }

    private string getStandardSelectClause(string articleType){
         String soql = 'SELECT ArticleType, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.CommunityNickname, CreatedBy.SmallPhotoUrl, CreatedBy.FullPhotoUrl, CreatedBy.Title, KnowledgeArticleId, LastPublishedDate, LastModifiedDate, Summary, Title, Image_URI__c, UrlName  ';

         if(articleType.equals('Recipe__kav'))
                soql += ', Diet_Category__c, Ingredients__c, Instructions__c, Fruit_Servings__c, Calories__c, Fruit_Vegetable_Servings__c, Hero_Image_URI__c, Template_Layout__c, Recipe__r.Total_Hours__c, Recipe__r.Total_Minutes__c, Recipe__r.hmr_Recipe_Category__c, Submitted_By__r.FirstName, Submitted_By__r.LastName ';
            else
                soql += ', Read_Time__c ';

            if(articleType.equals('FAQ__kav'))
                soql += ', Answer__c, Card_Layout__c, File_Download_URL__c, Hero_Image_URI__c, Hero_Mobile_Image_URI__c ';

            if(articleType.equals('Blog_Post__kav'))
                soql += ', Card_Layout__c, Author__r.FirstName, Author__r.LastName, Author_Title__c, Caption_1__c, Caption_2__c, Caption_3__c, File_Download_URL__c, Hero_Image_URI__c, Hero_Mobile_Image_URI__c, Image_1__c, Image_2__c, Image_3__c, Post_Content__c, Post_Content_2__c, Post_Content_3__c, Post_Content_4__c, Takeaways__c, Teaser__c, Video_Code__c, Video_Image_URI__c, Video_Title__c ';

            if(articleType.equals('Success_Story__kav'))
                soql += ', Before_Photo__c, After_Photo__c, Client_First_Name__c, Hero_Image_URI__c, Hero_Mobile_Image_URI__c, HMR_Success_Story__r.Hometown__c, Pounds_Lost__c, Publish_to_Success_Story_Page__c, Short_Teaser__c, Long_Teaser__c, Story_Body__c, Submitted_By__r.FirstName, Submitted_By__r.LastName, Submitted_By__r.hmr_Program_Membership_Type__c, Submitted_By__r.Title ';
            else
                soql += ', Mobile_Image_URI__c ';

         return soql;
    }

    public List<ArticleRecord> getKnowledgeArticleVersionDetails(List<KnowledgeArticleVersion> masterArticleList, boolean featuredOnly){
        return getKnowledgeArticleVersionDetails(masterArticleList, 0, 0, '', featuredOnly);
    }

    public List<ArticleRecord> getKnowledgeArticleVersionDetails(List<KnowledgeArticleVersion> masterArticleList, Integer recordLimit, Integer offset, String additionalWhereClause, boolean featuredOnly){
        Map<String, Set<Id>> articleTypeToIdSetMap = new Map<String, Set<Id>>();

        if(this.DataCategoryToArticleMap == null)
            this.getKnowledgeDataCategoryMap();

        for(KnowledgeArticleVersion kav : masterArticleList){
            if(articleTypeToIdSetMap.containsKey(kav.ArticleType)){
                Set<Id> articleIdSet = articleTypeToIdSetMap.get(kav.ArticleType);
                articleIdSet.add(kav.Id);
            }
            else{
                articleTypeToIdSetMap.put(kav.ArticleType, new Set<Id>{kav.Id});
            }
        }

        return getKnowledgeArticleVersionDetails(articleTypeToIdSetMap, recordLimit, offset, additionalWhereClause, featuredOnly);
    }

    private List<ArticleRecord> getArticleVotes(String articleType, List<ArticleRecord> articleRecordList){
        Map<Id, ArticleRecord> articleIdToArticleRecordMap = new Map<Id, ArticleRecord>();

        articleType = articleType.replace('__kav', '__ka');

        for(ArticleRecord articleRecord : articleRecordList)
            articleIdToArticleRecordMap.put(articleRecord.KnowledgeArticleId, articleRecord);

        //There are restrictions on the way Votes can be queried so we must use Type here as ParentId IN: isn't an option
        if(articleIdToArticleRecordMap.size() > 0){
            for(AggregateResult voteRecord : [SELECT ParentId
                                                    ,Count(Id) totalVotes
                                              FROM Vote
                                              WHERE Parent.Type =: articleType
                                              GROUP BY ParentId]){
                ArticleRecord articleRecord = articleIdToArticleRecordMap.get(String.valueOf(voteRecord.get('ParentId')));
                if(articleRecord != null)
                    articleRecord.Rating = Integer.valueOf(voteRecord.get('totalVotes'));
            }
        }

        return articleIdToArticleRecordMap.values();
    }

    public Vote getVote(string articleName){
        List<Vote> voteList = new List<Vote>();

        if(!String.isBlank(articleName)){
            //Get the knowledge article
            List<KnowledgeArticleVersion> articleVersionList = database.query('SELECT KnowledgeArticleId FROM KnowledgeArticleVersion WHERE UrlName = :articleName AND PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ');

            if(articleVersionList.size() > 0){
                Id knowledgeArticleId = articleVersionList[0].KnowledgeArticleId;
                voteList = [SELECT Id FROM Vote WHERE ParentId =: knowledgeArticleId AND CreatedById =: UserInfo.getUserId()];
            }
        }

        return voteList.size() > 0 ? voteList[0] : new Vote();
    }

    public Integer getVoteCount(string articleName){
        List<AggregateResult> resultList = new List<AggregateResult>();
        Integer voteCount = 0;

        if(!String.isBlank(articleName)){
            //Get the knowledge article
            List<KnowledgeArticleVersion> articleVersionList = database.query('SELECT KnowledgeArticleId FROM KnowledgeArticleVersion WHERE UrlName =: articleName AND PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ');

            if(articleVersionList.size() > 0){
                Id knowledgeArticleId = articleVersionList[0].KnowledgeArticleId;
                resultList = [SELECT Count(Id) totalVotes FROM Vote WHERE ParentId =: knowledgeArticleId];

                if(resultList.size() > 0)
                    voteCount = Integer.valueOf(resultList[0].get('totalVotes'));
            }
        }

        return voteCount;
    }

    public Vote voteForKnowledgeArticle(string articleName){
        Vote voteRecord;

        if(!String.isBlank(articleName)){
            articleName = String.escapeSingleQuotes(articleName);
            //Get the knowledge article
            List<KnowledgeArticleVersion> articleVersionList = database.query('SELECT KnowledgeArticleId FROM KnowledgeArticleVersion WHERE UrlName =: articleName AND PublishStatus = \'' + this.publishStatus + '\' AND Language = \'en_US\' ');
            if(articleVersionList.size() > 0){
                Id knowledgeArticleId = articleVersionList[0].KnowledgeArticleId;
                //Query to be sure the user has not voted
                List<Vote> voteList = [SELECT Id FROM Vote WHERE ParentId =: knowledgeArticleId AND CreatedById =: UserInfo.getUserId()];

                if(voteList.size() <= 0){
                    voteRecord = new Vote(ParentId = knowledgeArticleId, Type = '5');
                    insert voteRecord;
                }
                else
                    voteRecord = voteList[0];
            }
        }

        return voteRecord;
    }

    public class LayoutCount{
        public integer TopBottomCount {get; set;}
        public integer LeftRightCount {get; set;}
        public integer TotalCount {get{return TopBottomCount + LeftRightCount;}}

        public LayoutCount(integer topBottomCount, integer leftRightCount){
            this.TopBottomCount = topBottomCount;
            this.LeftRightCount = leftRightCount;
        }
    }

    public class ArticleRecord{
        private final string DefaultAuthorName = 'HMR Team';
        private final string DefaultAuthorTitle = 'Member';
        public string Id {get; set;}
        public string KnowledgeArticleId {get; set;}
        public string ArticleType { get; set; }
        public string AuthorName {get; set;}
        public string AuthorTitle {get; set;}
        public string AuthorPhotoUrl {get; set;}
        public string AuthorSmallPhotoUrl {get; set;}
        public string Title {get; set;}
        public string Summary {get; set;}
        public integer Rating { get; set; }
        public string ImageUri {get; set;}
        public string UrlName {get; set;}
        public string LastPublishedDate {get; set;}
        public string LastModifiedDate {get; set;}
        public String ReadTime {get; set; }
        public integer TotalMinutes {get; set;}
        public String DataCategoryDisplay {get; set;}
        public String CategoryUrlName {get{
            return !String.isBlank(DataCategoryDisplay) ? DataCategoryDisplay.replace(' ','-').replace(';','').toLowerCase() : '';
        }}
        //Shared Additional Fields
        public String HeroImage {get; set;}
        public String HeroImageMobile {get; set;}
        public string MobileImageUri {get; set;}
        public String TemplateLayout {get; set;} // Card Layout for FAQ and Article
        //Blog Article Specific Fields
        public String Takeaways {get; set;}
        public String Teaser {get; set;}
        public String PostContent01 {get; set;}
        public String PostContent02 {get; set;}
        public String PostContent03 {get; set;}
        public String PostContent04 {get; set;}
        public String Image01 {get; set;}
        public String Image02 {get; set;}
        public String Image03 {get; set;}
        public String Caption01 {get; set;}
        public String Caption02 {get; set;}
        public String Caption03 {get; set;}
        public String VideoTitle {get; set;}
        public String VideoImage01 {get; set;}
        public String VideoCode {get; set;}
        //FAQ Specific Fields
        public String FileDownloadUrl {get; set;}
        //Recipe Specific Fields
        public Decimal Calories {get; set;}
        public Decimal FruitVegetableServings {get; set;}
        public Decimal FruitServings {get; set;}
        public String Ingredients {get; set;}
        public String Instructions {get; set;}
        public Integer TotalHours {get; set;}
        //Success Story Specific Fields
        public String ClientFirstName {get; set;}
        public String Hometown {get; set;}
        public Decimal PoundsLost {get; set;}
        public String BeforePhoto {get; set;}
        public String AfterPhoto {get; set;}
        public String ShortTeaser {get; set;}
        public String LongTeaser {get; set;}
        public String StoryBody {get; set;}

        public ArticleRecord(){}
        public ArticleRecord(sObject record, Map<String, Set<String>> dataCategoryToArticleMap){

            if(record instanceof FAQ__kav) {
                FAQ__kav recordKav = (FAQ__kav)record;

                this.ArticleType = recordKav.ArticleType;
                this.AuthorName = recordKav.CreatedBy != null ? recordKav.CreatedBy.FirstName + ' ' + recordKav.CreatedBy.LastName : '';
                this.AuthorTitle = recordKav.CreatedBy.Title;
                this.AuthorPhotoUrl = recordKav.CreatedBy.FullPhotoUrl;
                this.AuthorSmallPhotoUrl = recordKav.CreatedBy.SmallPhotoUrl;
                this.Id = recordKav.Id;
                this.KnowledgeArticleId = recordKav.KnowledgeArticleId;
                this.LastPublishedDate = recordKav.LastPublishedDate.format('MMMM d,  yyyy');
                this.Summary = recordKav.Summary;
                this.Title = recordKav.Title;
                this.ImageUri = recordKav.Image_URI__c;
                this.UrlName = recordKav.UrlName;
                this.ReadTime = recordKav.Read_Time__c;
                this.TemplateLayout = recordKav.Card_Layout__c;
                this.FileDownloadUrl = recordKav.File_Download_URL__c;
                //Article detail field mapping
                this.HeroImage = recordKav.Hero_Image_URI__c;
                this.HeroImageMobile = recordKav.Hero_Mobile_Image_URI__c;
                this.MobileImageUri = recordKav.Mobile_Image_URI__c;
                this.Teaser = recordKav.Title;
                this.PostContent01 = recordKav.Summary;
                this.PostContent02 = recordKav.Answer__c;
                this.Image01 = recordKav.Image_URI__c;
            }
            else if(record instanceof Blog_Post__kav) {
                Blog_Post__kav recordKav = (Blog_Post__kav)record;

                this.ArticleType = recordKav.ArticleType;
                this.AuthorName = recordKav.Author__r != null ? recordKav.Author__r.FirstName + ' ' + recordKav.Author__r.LastName : '';
                this.AuthorTitle = recordKav.Author_Title__c;
                this.AuthorPhotoUrl = recordKav.CreatedBy.FullPhotoUrl;
                this.AuthorSmallPhotoUrl = recordKav.CreatedBy.SmallPhotoUrl;
                this.Id =recordKav.Id;
                this.KnowledgeArticleId = recordKav.KnowledgeArticleId;
                if(recordKav.LastPublishedDate != null) {
                    this.LastPublishedDate = recordKav.LastPublishedDate.format('MMMM d,  yyyy');
                }
                this.Summary = recordKav.Summary;
                this.Title = recordKav.Title;
                this.ImageUri = recordKav.Image_URI__c;
                this.UrlName = recordKav.UrlName;
                this.ReadTime = recordKav.Read_Time__c;
                this.TemplateLayout = recordKav.Card_Layout__c;
                this.FileDownloadUrl = recordKav.File_Download_URL__c;
                //Blog specific fields
                this.HeroImage = recordKav.Hero_Image_URI__c;
                this.HeroImageMobile = recordKav.Hero_Mobile_Image_URI__c;
                this.MobileImageUri = recordKav.Mobile_Image_URI__c;
                this.Takeaways = recordKav.Takeaways__c;
                this.Teaser = recordKav.Teaser__c;
                this.Caption01 = recordKav.Caption_1__c;
                this.Caption02 = recordKav.Caption_2__c;
                this.Caption03 = recordKav.Caption_3__c;
                this.Image01 = recordKav.Image_1__c;
                this.Image02 = recordKav.Image_2__c;
                this.Image03 = recordKav.Image_3__c;
                this.PostContent01 = recordKav.Post_Content__c;
                this.PostContent02 = recordKav.Post_Content_2__c;
                this.PostContent03 = recordKav.Post_Content_3__c;
                this.PostContent04 = recordKav.Post_Content_4__c;
                this.VideoCode = recordKav.Video_Code__c;
                this.VideoImage01 = recordKav.Video_Image_URI__c;
                this.VideoTitle = recordKav.Video_Title__c;
            }
            else if(record instanceof Recipe__kav) {
                Recipe__kav recordKav = (Recipe__kav)record;

                this.ArticleType = recordKav.ArticleType;
                this.AuthorName = recordKav.Submitted_By__r.FirstName + ' ' + recordKav.Submitted_By__r.LastName;
                this.AuthorTitle = 'Member';
                this.AuthorPhotoUrl = recordKav.CreatedBy.FullPhotoUrl;
                this.AuthorSmallPhotoUrl = recordKav.CreatedBy.SmallPhotoUrl;
                this.Id = recordKav.Id;
                this.KnowledgeArticleId = recordKav.KnowledgeArticleId;
                this.LastPublishedDate = recordKav.LastPublishedDate.format('MMMM d,  yyyy');
                this.Summary = recordKav.Summary;
                this.TemplateLayout = recordKav.Template_Layout__c;
                this.DataCategoryDisplay = recordKav.Diet_Category__c;
                this.Title = recordKav.Title;
                this.HeroImage = recordKav.Hero_Image_URI__c;
                this.ImageUri = recordKav.Image_URI__c;
                this.MobileImageUri = recordKav.Mobile_Image_URI__c;
                this.UrlName = recordKav.UrlName;
                //Recipe Specific Fields
                this.Calories = recordKav.Calories__c;
                this.FruitVegetableServings = recordKav.Fruit_Vegetable_Servings__c;
                this.FruitServings = recordKav.Fruit_Servings__c;
                this.Ingredients = recordKav.Ingredients__c;
                this.Instructions = recordKav.Instructions__c;
                if(recordKav.Recipe__r.Total_Hours__c != null)
                    this.TotalHours = Integer.valueOf(recordKav.Recipe__r.Total_Hours__c);
                else
                    this.TotalHours = 0;
                if(recordKav.Recipe__r.Total_Minutes__c != null)
                    this.TotalMinutes = Integer.valueOf(recordKav.Recipe__r.Total_Minutes__c);
                else
                    this.TotalMinutes = 0;
            }
            else if(record instanceof Success_Story__kav) {
                Success_Story__kav recordKav = (Success_Story__kav)record;

                this.ArticleType = recordKav.ArticleType + (recordKav.Publish_to_Success_Story_Page__c ? ' - HMR Authored' : ' - User Submitted');
                this.AuthorName = recordKav.Submitted_By__r.FirstName + ' ' + recordKav.Submitted_By__r.LastName;
                this.AuthorTitle = recordKav.Submitted_By__r.Title;
                this.AuthorPhotoUrl = recordKav.CreatedBy.FullPhotoUrl;
                this.AuthorSmallPhotoUrl = recordKav.CreatedBy.SmallPhotoUrl;
                this.Id = recordKav.Id;
                this.KnowledgeArticleId = recordKav.KnowledgeArticleId;
                this.LastPublishedDate = recordKav.LastPublishedDate.format();
                this.Summary = recordKav.Summary;
                this.Title = recordKav.Title;
                this.ImageURI = recordKav.Image_URI__c;
                this.UrlName = recordKav.UrlName;
                this.ReadTime = recordKav.Read_Time__c;
                this.HeroImage = recordKav.Hero_Image_URI__c;
                this.HeroImageMobile = recordKav.Hero_Mobile_Image_URI__c;
                this.ClientFirstName = !String.isBlank(recordKav.Client_First_Name__c) ? recordKav.Client_First_Name__c : '';
                this.Hometown = !String.isBlank(recordKav.HMR_Success_Story__r.Hometown__c) ? recordKav.HMR_Success_Story__r.Hometown__c : '';
                this.PoundsLost = recordKav.Pounds_Lost__c;
                this.BeforePhoto = recordKav.Before_Photo__c;
                this.AfterPhoto = recordKav.After_Photo__c;
                this.ShortTeaser = !String.isBlank(recordKav.Short_Teaser__c) ? recordKav.Short_Teaser__c : '';
                this.LongTeaser = !String.isBlank(recordKav.Long_Teaser__c) ? recordKav.Long_Teaser__c : '';
                this.StoryBody = !String.isBlank(recordKav.Story_Body__c) ? recordKav.Story_Body__c : '';
            }

            if(record instanceof Recipe__kav){
                if(String.isBlank(this.DataCategoryDisplay))
                    this.DataCategoryDisplay = 'Recipe';
                else{
                    String[] dietCategories = this.DataCategoryDisplay.split(';');
                    this.DataCategoryDisplay = dietCategories[0];
                }
            }
            else
                this.DataCategoryDisplay = getDataCategoryDisplay(record.Id, dataCategoryToArticleMap);

            if(String.isBlank(this.AuthorName))
                this.AuthorName = DefaultAuthorName;
            if(String.isBlank(this.AuthorTitle))
                this.AuthorTitle = DefaultAuthorTitle;
            if(this.Rating == null)
                this.Rating = 0;
            if(String.isBlank(ReadTime))
                this.ReadTime = '0';
        }

        public String getDataCategoryDisplay(Id articleId, Map<String, Set<String>> dataCategoryToArticleMap){
            String dataCategoryDisplay = '';

            if(dataCategoryToArticleMap != null)
            {
                for(String dataCategory : dataCategoryToArticleMap.keySet()){
                    Set<String> articleSet = dataCategoryToArticleMap.get(dataCategory);
                    if(articleSet.contains(articleId))
                        dataCategoryDisplay += dataCategory + ', ';
                }

                if(!String.isBlank(dataCategoryDisplay)){
                    dataCategoryDisplay = dataCategoryDisplay.replaceAll('_',' ');
                    dataCategoryDisplay = dataCategoryDisplay.replaceAll(';','');
                    dataCategoryDisplay = dataCategoryDisplay.removeEnd(', ');
                }
            }

            //We want to only display category based on our prioritization of:
            //(Getting Started, Eating Well, Being More Active, Staying On Track)
            if(dataCategoryDisplay.containsIgnoreCase('Getting Started'))
                dataCategoryDisplay = 'Getting Started';
            else if(dataCategoryDisplay.containsIgnoreCase('Eating Well'))
                dataCategoryDisplay = 'Eating Well';
            else if(dataCategoryDisplay.containsIgnoreCase('Being More Active'))
                dataCategoryDisplay = 'Being More Active';
            else if(dataCategoryDisplay.containsIgnoreCase('Staying On Track'))
                dataCategoryDisplay = 'Staying On Track';

            return dataCategoryDisplay;
        }
    }
}