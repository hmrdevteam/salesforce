/**
* Apex Service Class for Data Category Access
*
* @Date: 2017-05-08
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-19;Ali Pierre 2017-07-24;
* @JIRA: 
*/
public with sharing class HMR_DataCategory_Service {
    public List<Category_Map__mdt> getDataCategoryList(){
        List<Category_Map__mdt> dataCategoryList = [SELECT MasterLabel
                                                          ,DeveloperName
                                                          ,Data_Category_Name__c
                                                          ,Topic_Name__c
                                                          ,Icon1__c
                                                          ,Icon2__c 
                                                          ,Url_Resource_Name__c
                                                          ,Topic_Description__c
                                                          ,Sort_Order__c
                                                     FROM Category_Map__mdt 
                                                     WHERE Data_Category_Name__c <> '' 
                                                     ORDER BY Sort_Order__c];
        return dataCategoryList;
    }

    public List<Category_Map__mdt> getTopicList(){
        List<Category_Map__mdt> topicList = [SELECT MasterLabel
                                                          ,DeveloperName
                                                          ,Data_Category_Name__c
                                                          ,Topic_Name__c
                                                          ,Icon1__c
                                                          ,Icon2__c 
                                                          ,Url_Resource_Name__c
                                                          ,Topic_Description__c
                                                          ,Sort_Order__c
                                                     FROM Category_Map__mdt 
                                                     WHERE Topic_Name__c  <> '' 
                                                     ORDER BY Sort_Order__c];
        return topicList;
    }

    public List<Category_Map__mdt> getRecipeDataCategoryList(){
        List<Category_Map__mdt> recipeCategoryList = [SELECT MasterLabel
                                                          ,DeveloperName
                                                          ,Data_Category_Name__c
                                                          ,Topic_Name__c
                                                          ,Icon1__c
                                                          ,Icon2__c 
                                                          ,Url_Resource_Name__c
                                                          ,Topic_Description__c
                                                          ,Sort_Order__c
                                                     FROM Category_Map__mdt 
                                                     WHERE Recipe_Category__c <> '' 
                                                     ORDER BY Sort_Order__c];
        return recipeCategoryList;
    }

    public Map<String, String> getTopicIconMap(){
        Map<String, String> iconMap = new Map<String, String>();
        for(Category_Map__mdt cm : getTopicList()){
            iconMap.put(cm.Topic_Name__c, cm.Icon1__c);
        }
        return iconMap;
    }

    public List<String> getPrimaryDataCategoryLabelList(){
        List<DataCategory> primaryDataCategoryList = getPrimaryDataCategoryList();
        List<String> primaryCategoryLabelList = new List<String>();
        
        for(DataCategory primaryDataCategory : primaryDataCategoryList)
            primaryCategoryLabelList.add(primaryDataCategory.getLabel());
        
        return primaryCategoryLabelList;
    }
    
    public List<DataCategory> getPrimaryDataCategoryList(){
        List<DataCategory> primaryDataCategoryList = new List<DataCategory>();
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        //Creating a list of pair objects to use as a parameter for the describe call
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>(); 
        //Making the call to the describeDataCategoryGroups toget the list of category groups associated
        List<String> objectTypeList = new List<String>{'KnowledgeArticleVersion'};
        describeCategoryResult = Schema.describeDataCategoryGroups(objectTypeList);

        //Looping throught the first describe result to create the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);            
         }         
         
         //describeDataCategoryGroupStructures()
         describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);

         //Getting data from the result
         for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){         
            //Get the top level categories
            DataCategory [] toplevelCategories = singleResult.getTopCategories();     
            for(DataCategory category : toplevelCategories) { 
                primaryDataCategoryList.addAll(category.getChildCategories());              
            }
        }
        
        return primaryDataCategoryList;
    }

    public List<DataCategoryHierarchyRecord> getDataCategoryGroupStructures(){
        Map<String, List<DataCategory>> dataCategoryHierarchyMap = new Map<String, List<DataCategory>>();
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        //Creating a list of pair objects to use as a parameter for the describe call
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
        //Making the call to the describeDataCategoryGroups to get the list of category groups associated
        List<String> objectTypeList = new List<String>{'KnowledgeArticleVersion'};
        List<DataCategoryHierarchyRecord> dataCategoryHierarchyList = new List<DataCategoryHierarchyRecord>();
         
        describeCategoryResult = Schema.describeDataCategoryGroups(objectTypeList);

        //Looping throught the first describe result to create the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);
        }
        
        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);
        
        //Getting data from the result
        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
            //Get the top level categories
            DataCategory [] toplevelCategories = singleResult.getTopCategories();

            for(DataCategory category : topLevelCategories){
                DataCategoryHierarchyRecord hierarchyRecord = new DataCategoryHierarchyRecord('',category.getLabel(),category.getName(),0);
                dataCategoryHierarchyList.add(hierarchyRecord);
                dataCategoryHierarchyList = appendChildCategories(category, dataCategoryHierarchyList, 0);  
            }
        }
        return dataCategoryHierarchyList;
    }
    
    private static List<DataCategoryHierarchyRecord> appendChildCategories(DataCategory parentCategory, List<DataCategoryHierarchyRecord> dataCategoryHierarchyList, integer hierarchyLevel){
        ++hierarchyLevel;
        for(DataCategory childCategory : parentCategory.getChildCategories()){
            DataCategoryHierarchyRecord hierarchyRecord = new DataCategoryHierarchyRecord(parentCategory.getName(),childCategory.getLabel(),childCategory.getName(),hierarchyLevel);
            dataCategoryHierarchyList.add(hierarchyRecord);
            //Recursively call to generate hierarchy
            dataCategoryHierarchyList = appendChildCategories(childCategory, dataCategoryHierarchyList, hierarchyLevel);
        }
        return dataCategoryHierarchyList;
    }
    
    public class DataCategoryHierarchyRecord{
        public string parentName {get; set;}
        public string label {get; set;}
        public string name {get; set;}
        public integer level {get; set;}

        public DataCategoryHierarchyRecord(string parentName, string label, string name, integer level){
            this.parentName = parentName;
            this.label = label;
            this.name = name;
            this.level = level;
        }
    }
}