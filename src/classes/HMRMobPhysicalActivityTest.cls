/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/15/2018
 * Created By: 
 * Last Modified Date: 01/15/2018
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobPhysicalActivity REST Service
 * ****************************************************/
@isTest
private class HMRMobPhysicalActivityTest {

    @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
      insert programRecord;

      Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                hmr_Active__c = true,
                                                                                hmr_Status__c = 'Active',
                                                                                hmr_Contact__c = contactRecord.Id,
                                                                                hmr_Diet_Start_Date__c = Date.today());
      insert programMembershipRecord;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170 
                                                           ,Entrees__c = 3
                                                           ,FruitsVegetables__c = 8
                                                           ,ShakesCereals__c = 4
                                                           ,Water__c = 8);
      insert activityLogRecord;
    }

    @isTest
    private static void testGetPhysicalActivityForNewRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobPhysicalActivity.doGet();

      Test.stopTest();

      //Verify the physical activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetPhysicalActivityForExistingRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      
      insert new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                               ,Program_Membership__c = programMembershipRecord.Id
                               ,DateDataEnteredFor__c = Date.today()
                               ,CurrentWeight__c = 170);
      
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobPhysicalActivity.doGet();

      Test.stopTest();

      //Verify the physical activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }
    
    @isTest
    private static void testGetPhysicalActivityWithInvalidData(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.params.put('contactId', '');
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobPhysicalActivity.doGet();

      Test.stopTest();

      //Verify invalid status code was returned
      System.assertEquals(404, response.statuscode);
    }

    @isTest
    private static void testPostPhysicalActivityLogWithExistingActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(Activity_Log__c = activityLogRecord.Id
                                                                             ,NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);

      HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        physicalActivityDTO = HMRMobPhysicalActivity.getPhysicalActivity(physicalActivityLogRecord);
        physicalActivityDTO.contactId = programMembershipRecord.hmr_Contact__c;
        physicalActivityDTO.programMembershipId = programMembershipRecord.Id;
        physicalActivityDTO.activityDate = Date.today();
        HMRMobPhysicalActivity.doPost(physicalActivityDTO);
      }

      Test.stopTest();

      //Verify the physical activity log record was created
      List<PhysicalActivity__c> physicalActivityLogResultList = [SELECT Id FROM PhysicalActivity__c];
      System.assert(physicalActivityLogResultList.size() == 1);
    }

    @isTest
    private static void testPostPhysicalActivityLogWithoutExistingActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);

      HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        physicalActivityDTO = HMRMobPhysicalActivity.getPhysicalActivity(physicalActivityLogRecord);
        physicalActivityDTO.contactId = programMembershipRecord.hmr_Contact__c;
        physicalActivityDTO.programMembershipId = programMembershipRecord.Id;
        physicalActivityDTO.activityDate = Date.today().addDays(-1);
        HMRMobPhysicalActivity.doPost(physicalActivityDTO);
      }

      Test.stopTest();

      //Verify the activity log record was created
      List<ActivityLog__c> activityLogResultList = [SELECT Id FROM ActivityLog__c];
      System.assertEquals(activityLogResultList.size(), 1);
    }

    @isTest
    private static void testPostPhysicalActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);

      HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        physicalActivityDTO = HMRMobPhysicalActivity.getPhysicalActivity(physicalActivityLogRecord);
        HMRMobPhysicalActivity.doPost(physicalActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(500, response.statusCode);
    }

    @isTest
    private static void testPatchPhysicalActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(Activity_Log__c = activityLogRecord.Id
                                                                             ,NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);

      insert physicalActivityLogRecord;

      HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        physicalActivityDTO = HMRMobPhysicalActivity.getPhysicalActivity(physicalActivityLogRecord);
        physicalActivityDTO.activity = 'Swimming';
        HMRMobPhysicalActivity.updatePhysicalActivity(physicalActivityDTO);
      }

      Test.stopTest();

      //Verify the physical activity log record was updated
      PhysicalActivity__c physicalActivityResultRecord = [SELECT NameofPhysicalActivity__c FROM PhysicalActivity__c LIMIT 1];
      System.assertEquals('Swimming', physicalActivityResultRecord.NameofPhysicalActivity__c);
    }

    @isTest
    private static void testPatchPhysicalActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(Activity_Log__c = activityLogRecord.Id
                                                                             ,NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);
      insert physicalActivityLogRecord;

      HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        physicalActivityDTO = HMRMobPhysicalActivity.getPhysicalActivity(physicalActivityLogRecord);
        physicalActivityDTO.recordId = null;
        HMRMobPhysicalActivity.updatePhysicalActivity(physicalActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(405, response.statusCode);
    }

    @isTest
    private static void testDeletePhysicalActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;
      PhysicalActivity__c physicalActivityLogRecord = new PhysicalActivity__c(Activity_Log__c = activityLogRecord.Id
                                                                             ,NameofPhysicalActivity__c = 'Tennis'
                                                                             ,TotalPhysicalActivityCalories__c = 700);
      insert physicalActivityLogRecord;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.params.put('id', physicalActivityLogRecord.Id);
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;


      Test.startTest();

      System.runAs(userRecord){
        HMRMobPhysicalActivity.deletePhysicalActivity();
      }

      Test.stopTest();

      //Verify the physical activity log record was deleted
      List<PhysicalActivity__c> physicalActivityResultList = [SELECT Id FROM PhysicalActivity__c];
      System.assertEquals(0, physicalActivityResultList.size());
    }

    @isTest
    private static void testDeletePhysicalActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/physicalData/';
      request.params.put('id', '00000000000000000000');
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobPhysicalActivity.deletePhysicalActivity();
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(404, response.statusCode);
    }
}