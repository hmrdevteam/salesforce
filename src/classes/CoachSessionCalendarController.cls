/**
* Controller for session canlendar visualforce page. This will display calendar as selected coach & Program
* Objects referenced --> Coaching_Session__c,Coach__c,Program__c, 
*
* @Date: 2016-12-08
* @Author Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-706
*/

public with sharing class CoachSessionCalendarController {
public string eventsJSON {get;set;}

    //The calendar plugin is expecting dates is a certain format. We can use this string to get it formated correctly
    static String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';   
    
    
    //public variables 
    public string CoachId{ get;set;}  
    public Boolean CurrentUserIsCoach{get;set;}
    public String CurrentUserId{get;set;}
    public Date StartDate{get; set;}
    public Date EndDate{get; set;}
    
  
    
    public void GetCurrentUserDetails(){
        if(Apexpages.currentpage().getparameters().get('minDate') != null && Apexpages.currentpage().getparameters().get('minDate') != '')
        	StartDate = Date.valueOf(Apexpages.currentpage().getparameters().get('minDate'));
        else 
        	StartDate = null;
        if(Apexpages.currentpage().getparameters().get('maxDate') != null && Apexpages.currentpage().getparameters().get('maxDate') != '')
        	EndDate = Date.valueOf(Apexpages.currentpage().getparameters().get('maxDate'));
        else 
        	EndDate = null;	  
        
        User CurrentUser=[Select Id , hmr_IsCoach__c from User where Id=:UserInfo.getUserId()];
        if(CurrentUser.hmr_IsCoach__c==true)
        {
            CurrentUserIsCoach=true;
            List<Coach__c> getCoachId=[Select Id from Coach__c where hmr_Coach__c=:CurrentUser.Id];
            for(Coach__c coach : getCoachId)
            {
              CurrentUserId=coach.id;
            
            }
        }  
    }
    
    @RemoteAction
    public static string eventdata(String NewCoachId){
    
		    //Create List of events
		    calEvent[] events = new calEvent[]{};
		    
		    //Cretae List of coaching sessions
		    Coaching_Session__c[] coachingsessionList= new Coaching_Session__c[]{};
		    
		     // Get Parameter & seperate them 
		    List<String> ParamList=NewCoachId.split('::');
		    		    
		    String CoachId =Id.valueOf(ParamList[0]);
		    String Program=ParamList[1];
		    Set<Id> ProgramId = new Set<Id>();
		   
		    List<Program__c> ProgramList=[Select Name, Id from Program__c];
		    If (Program == 'Phase1'){
		    			     
		      for(Program__c Prog :ProgramList){
                   if(Prog.Name=='P1 Healthy Solutions' || Prog.Name=='P1 Healthy Shakes'){
                   ProgramId.add(Prog.Id);
                   }
		      }
		    }
		    else if(Program == 'Phase2'){
		      for(Program__c Prog :ProgramList){
                   if(Prog.Name=='Phase 2'){
                   ProgramId.add(Prog.Id);
                   }
              } 	
		      
		    }
		    try{
		    //Get coaching sessions details based on coach id & program id
		    for(Coaching_Session__c session: [select Id, Name, hmr_Class_Date__c,hmr_Start_Time__c, hmr_End_Time__c,hmr_Coach_Name__c,hmr_Conference_call_number__c, hmr_Host_Code__c,hmr_Participant_code__c   from Coaching_Session__c where hmr_Coach__c=:CoachId And hmr_Class__r.Program__c IN:ProgramId]){
		 	     
		 	     Datetime startDT=DateTime.now();
		 	     Datetime endDT=DateTime.now();
		 	     
		 	     //Chekc if date & time are not null
		         if(session.hmr_Class_Date__c!=Null && session.hmr_Start_Time__c!=Null){
		 	     
			 	     //Covert date & time from string to datetime instance 
			 	     startDT = DateTime.parse( session.hmr_Class_Date__c.month()+'/'+session.hmr_Class_Date__c.day()+'/'+session.hmr_Class_Date__c.year()+' '+session.hmr_Start_Time__c);
			 	     endDT = DateTime.parse( session.hmr_Class_Date__c.month()+'/'+session.hmr_Class_Date__c.day()+'/'+session.hmr_Class_Date__c.year()+' '+session.hmr_End_Time__c);
			         
		         }
		        
		         //Prepare Event object to disply on calendar
		         calEvent myEvent = new calEvent();
		            myEvent.title = session.Name+'  '+ session.hmr_Conference_call_number__c;
		            myEvent.allDay = false;
		            myEvent.startString = startDT.format(dtFormat);
		            myEvent.endString = endDT.format(dtFormat);
		            myEvent.url = '/' + session.Id;
		            //myEvent.className = 'event-personal';
		            events.add(myEvent);
		     
		     }//end for
		 
            } catch(Exception ex) {
                System.debug(ex.getMessage());
            }     
		      //prepare the Json String to return the event data to display on Calendar
		    string jsonEvents = JSON.serialize(events);
		    jsonEvents = jsonEvents.replace('startString','start');
		    jsonEvents = jsonEvents.replace('endString','end');
		     
		    //retrun event data 
		    return jsonEvents;
  }

   // Class to hold calendar event data
   public class calEvent {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
   }
   

   
      //get the coach options for coach list 
   public List<SelectOption> getCoachList() {

	       	List<Coach__c>CoachobjectList=[Select Name, hmr_Coach_First_Name__c,hmr_Coach_Last_Name__c,Id from Coach__c where hmr_Active__c=true];
	       	List<SelectOption> options = new List<SelectOption>();
	        for(Coach__c coach :CoachobjectList){
	               options.add(new SelectOption(coach.Id,coach.hmr_Coach_First_Name__c+' '+coach.hmr_Coach_Last_Name__c));
	        }
            return options;
   }
        
       
       
}