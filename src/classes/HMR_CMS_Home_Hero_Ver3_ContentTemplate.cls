/**
* Apex Content Template Controller for Version 3 of Hero Section on Home Page
*
* @Date: 2018-03-12
* @Author Ali Pierre (HMR)
* @Modified: 
* @JIRA:
*/
global virtual class HMR_CMS_Home_Hero_Ver3_ContentTemplate extends cms.ContentTemplateController {

	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_Home_Hero_Ver3_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_Home_Hero_Ver3_ContentTemplate() {
        super();
    }

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String Version{
         get{
            return getPropertyWithDefault('Version', 'recognize');
        }
    }
    public String sectionGTMIDAttr{
         get{
            return getPropertyWithDefault('sectionGTMIDAttr', 'recognizeBanner');
        }
    }

	//Title Label Property
    public String TitleText{
        get{
            return getPropertyWithDefault('TitleText', 'LOSING WEIGHT IS TOUGH');
		}
    }

	//SubTitle Label Property
    public String SubTitleText{
        get{
            return getPropertyWithDefault('SubTitleText', 'Let’s not sugarcoat this,');
        }
    }

	//show US News Best Diets Fast Weight Loss Icon
	public Boolean showBestDietsFastWeightLoss {
        get {
            return getAttribute('showBestDietsFastWeightLoss') == 'true';
        }
    }

	//show US News Best Diets Weight Loss Icon
	public Boolean showBestDietsWeightLoss {
        get {
            return getAttribute('showBestDietsWeightLoss') == 'true';
        }
    }

	//global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup
		String html = '';
		String gtmTag = '';

		html += '<section class="hmr-hero-V3-section bg-typecover bg-homehero-' + Version + '">' +
                    '<div class="vert-parent">' + 
                        '<div class="vert-child">' +
        			        '<div class="container V3-container">' +
        			            '<div class="row">' +
        			                '<h4 class="hero-' + Version + '-subtitle home-hero-V3-subtitle">' + SubTitleText + '</h4>' +
        			            '</div>' +
        			            '<div class="row">' +
        			                '<h2 class="home-hero-V3-title hero-' + Version + '-title">' + TitleText + '</h2>' +
        			            '</div>' +
        			            '<div class="row no-padding">' +
                                '</div>' +
                            '</div>' +
			            '</div>';
		if(showBestDietsFastWeightLoss || showBestDietsWeightLoss) {
			html += '<div class="row best-diets-container-V3-parent">';
		}
		if(showBestDietsFastWeightLoss) {
			html += '<div class="best-diets-container-V3">' +
						'<a data-gtm-id="' + sectionGTMIDAttr + '" href="http://health.usnews.com/best-diet/hmr-diet" target="_blank">' +
							'<img src="https://www.myhmrprogram.com/ContentMedia/HomeDesktop/BestDietsFastWL2018.png" />' +
						'</a>' +
					'</div>';
		}
		if(showBestDietsWeightLoss) {
			html += '<div class="best-diets-container-V3">' +
						'<a data-gtm-id="' + sectionGTMIDAttr + '" href="http://health.usnews.com/best-diet/hmr-diet" target="_blank">' +
							'<img src="https://www.myhmrprogram.com/ContentMedia/HomeDesktop/BestDietsWL.png" />' +
						'</a>' +
					'</div>';
		}
		if(showBestDietsFastWeightLoss || showBestDietsWeightLoss) {
			html += '</div>';
		}
		html += '</div>' + '</section>';
        html += '<section class="hmr-page-section bg-typecover colHeaderSection hmr-2col-section-landing">' +
                    '<div class="container">' +
                        '<div class="row">' +
                            '<div class="col-sm-6 section-right">' +
                                '<div class="section-right-black">' +
                                    '<h4 class="hero-V3-title">' + SubTitleText + '</h4>' +
                                    '<div class="blurb text-center">' +
                                        '<p>' + TitleText + '</p>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</section>';
		return html;
	}
}