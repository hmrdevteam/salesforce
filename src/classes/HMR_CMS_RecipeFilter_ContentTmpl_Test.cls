/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 31st, 2017
Target Class: HMR_CMS_RecipeFilter_ContentTemplate
*/

@isTest
private class HMR_CMS_RecipeFilter_ContentTmpl_Test{
    @isTest 
    private static void testGetHtml() {
        HMR_CMS_RecipeFilter_ContentTemplate recipeFilter = new HMR_CMS_RecipeFilter_ContentTemplate();          
        String propertyValue = recipeFilter.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
        String dietCategory = recipeFilter.dietCategory;
        
        Map<String,String> testMap = new Map<String, String>();
       
        testMap.put('dietCategory', 'Decision Free');
        recipeFilter.testAttributes = testMap;        
        String respHTML = recipeFilter.getHTML(); 
         
        
        testMap.put('dietCategory', 'Healthy Solutions');
        recipeFilter.testAttributes = testMap;
        String respHTML1 = recipeFilter.getHTML();
        
        
        testMap.put('dietCategory', 'Healthy Shakes');
        recipeFilter.testAttributes = testMap;
        String respHTML2 = recipeFilter.getHTML();
        
        
        testMap.put('dietCategory', 'Phase 2');
        recipeFilter.testAttributes = testMap;
        String respHTML3 = recipeFilter.getHTML();           
        
        System.assert(!String.isBlank(respHTML));
    }       
           
}