/**
* Service class related to the weight table
* @Date: 2018-02-26
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-02-26
* @JIRA:
*/
public with sharing class HMR_WeightTable_Service {
	private decimal weight;

	public HMR_WeightTable_Service(decimal weight) {
		if(weight > 999.0){
			weight = 999;
		}
		else if (weight < 95){
			weight = 95;
		}

		this.weight = weight;
	}

	public Weight_Table_Setting__mdt getWeightSetting(){
		List<Weight_Table_Setting__mdt> weightSettingList = [SELECT Weight_Minimum__c,
																	Weight_Maximum__c,
																	Low_Intensity__c,
																	Medium_Intensity__c,
																	High_Intensity__c,
																	Very_High_Intensity__c
															  FROM Weight_Table_Setting__mdt
															  WHERE Weight_Minimum__c <=: this.weight 
															  	AND Weight_Maximum__c >=: this.weight];

		return weightSettingList.size() > 0 ? weightSettingList[0] : null;
	}

	public static List<Weight_Table_Setting__mdt> getAll(){
		List<Weight_Table_Setting__mdt> weightSettingList = [SELECT Weight_Minimum__c,
																	Weight_Maximum__c,
																	Low_Intensity__c,
																	Medium_Intensity__c,
																	High_Intensity__c,
																	Very_High_Intensity__c
															  FROM Weight_Table_Setting__mdt
															  ORDER BY Weight_Minimum__c];

		return weightSettingList;
	}
}