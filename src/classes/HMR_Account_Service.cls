/**
    * Apex Service Class for the Account
    *
    * @Date: 2017-11-15    
    * @Author: Utkarsh Goswami (Mindtree)
    * @Modified:
    * @JIRA: 
*/
public with sharing class HMR_Account_Service{

    public HMR_Account_Service(string userId){
        ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
        contextCCRZ.portalUserId = userId;    
        ccrz.cc_CallContext.initRemoteContext(contextCCRZ);
    }

    public List<HMR_CMS_AddressService.AddressRecord> getAddressRecords(String userId, String addressType){
       
        List<HMR_CMS_AddressService.AddressRecord> returnAddressList = new List<HMR_CMS_AddressService.AddressRecord>();
        //List<HMR_CMS_AddressService.AddressRecord> filteredAddList = new List<HMR_CMS_AddressService.AddressRecord>();

        Map<String, Object> addressMap = HMR_CMS_AddressService.getAddressBookByOwnerId(userId, null);
        
        if(addressMap.get('addressList') != null){
            List<Map<String, Object>> addressMapList = (List<Map<String, Object>>)addressMap.get('addressList');
            
            List<Map<String, Object>> addressBookMapList = (List<Map<String, Object>>)addressMap.get(ccrz.ccAPIAddressBook.ADDRESSBOOKS);
            
            for(Map<String, Object> addressRecordMap : addressMapList ){     
                returnAddressList.add(new HMR_CMS_AddressService.AddressRecord(addressRecordMap));   
            }
            
            for(HMR_CMS_AddressService.AddressRecord addressRecord : returnAddressList){
                for(Map<String, Object> addressBookRecordMap : addressBookMapList){
                    String addressId = (String)addressBookRecordMap.get('EContactAddress');
                  //  String addType = (String)addressBookRecordMap.get('addressType');
                    if(addressId == addressRecord.id){
                        addressRecord.AddressBookList.add(new HMR_CMS_AddressService.AddressBookRecord(addressBookRecordMap));
                      //  addressRecord.isDefault = (Boolean)addressBookRecordMap.get('default'); 
                      String theAddressType = (String)addressBookRecordMap.get('addressType');
                      Boolean isDefault = (Boolean)addressBookRecordMap.get('default');

                      if(String.isEmpty(theAddressType)) {
                        continue;
                      }

                      if(theAddressType == 'Shipping') {
                        addressRecord.isShippingAddress = true;
                        addressRecord.shippingAddressBookEntry = (String)addressBookRecordMap.get('sfid');
                        if(isDefault) {
                            addressRecord.isDefaultShippingAddress = true;
                        }

                      }

                      if(theAddressType == 'Billing') {
                        addressRecord.isBillingAddress = true;
                        addressRecord.billingAddressBookEntry = (String)addressBookRecordMap.get('sfid');
                        if(isDefault) {
                            addressRecord.isDefaultBillingAddress = true;
                        }
                      }
                        
                    }
                }
            }
        }
    
        return returnAddressList; 
    }

    
    public String changePassword(string oldPassword, string newPassword, string verifyNewPassword){
        pageReference pr;
        string reMessage = '';
        pr = Site.changePassword(newPassword, verifyNewPassword, oldPassword);
        if(pr != null){
            reMessage = 'Success';
        }else {
            for (ApexPages.Message m : ApexPages.getMessages()) {
                // Add messages to response string

                reMessage+=m.getDetail()+'';
            }
        }
        return reMessage;
    }

    public void addAddressToAccount(String userId, String addressJSON) {
        HMR_CMS_AddressService.AddressRecord addressRecord = (HMR_CMS_AddressService.AddressRecord)JSON.deserialize(addressJSON, HMR_CMS_AddressService.AddressRecord.class);
        HMR_CMS_AddressService.createAddressWithAdressBookEntries(userId, addressRecord);
    }

    public void updateAddress(String userId, String addressJSON) {
        HMR_CMS_AddressService.AddressRecord addressRecord = (HMR_CMS_AddressService.AddressRecord)JSON.deserialize(addressJSON, HMR_CMS_AddressService.AddressRecord.class);
        HMR_CMS_AddressService.updateAddressBook(userId, addressRecord);
    }

    public void deleteAddress(String addressJSON) {
        HMR_CMS_AddressService.AddressRecord addressRecord = (HMR_CMS_AddressService.AddressRecord)JSON.deserialize(addressJSON, HMR_CMS_AddressService.AddressRecord.class);
        HMR_CMS_AddressService.removeAddressBook(addressRecord);
    }

    public void updatePhoto(String userId, String imgData) {
        String communityId = null;
        imgData = imgData.remove('data:');
        Blob imgDataBlob = EncodingUtil.base64Decode(imgData);
        
        ConnectApi.Photo photo = ConnectApi.UserProfiles.setPhoto(communityId, userId, new ConnectApi.BinaryInput(imgDataBlob, 'image/jpg', 'userImage.jpg'));
    }

    public void removePhoto(String userId) {
        String communityId = null;
        ConnectApi.UserProfiles.deletePhoto(communityId, userId);

    }
}