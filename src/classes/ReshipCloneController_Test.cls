/*
 * ReshipCloneController_Test.cls
  * @Date: 2017-3-22
  * @Author Nathan Anderson (Magnet 360)
  * @JIRA:
 * Usage:
 *   Test class for ReShipCloneController.cls, CloneUtils.cls
 *   Uses cc_dataFActory for data creation
 */

@isTest
private class ReshipCloneController_Test {
	static testmethod void testMethod1() {
		//create test data
		cc_dataFActory.setupTestUser();
		cc_dataFActory.setupCatalog();
		cc_dataFActory.createOrders(1);

		ccrz__E_Order__c order = [SELECT Id FROM ccrz__E_Order__c LIMIT 1];


		ApexPages.currentPage().getParameters().put('oId', order.Id);
		ReShipCloneController controller = new ReShipCloneController();
		controller.cloneOrder();
	}
}