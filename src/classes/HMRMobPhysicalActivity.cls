/**
* Apex Rest Resource for Diet Activity
* @Date: 2018-01-11
* @Author: Zach Engman/Utkarsh Goswami (Magnet 360)
* @Modified: Zach Engman 2018-02-26 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/activity/physicalData/*')  
global with sharing class HMRMobPhysicalActivity {
    
    /**
     * insert the Physical Activity sObject
     * @param MobPhysicalActivityDTO
     * @see MobPhysicalActivityDTO
     * @return MobPhysicalActivityDTO - the DTO containing the physical activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 500 Internal Server Error
     */
    @HttpPost
    global static void doPost(MobPhysicalActivityDTO physicalActivityLog) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        try{
            //check if parent record exists, if not, create it
            if(!String.isBlank(physicalActivityLog.contactId) && physicalActivityLog.activityDate != null){
                HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
                PhysicalActivity__c physicalActivityRecord = physicalActivityLog.mapToPhysicalActivityLog();
                Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(physicalActivityLog.contactId, physicalActivityLog.activityDate);

                if(programMembershipRecord != null){
                    List<ActivityLog__c> activityLogList = [SELECT Id 
                                                            FROM ActivityLog__c 
                                                            WHERE Contact__c =: physicalActivityLog.contactId
                                                                AND Program_Membership__c =: programMembershipRecord.Id
                                                                AND DateDataEnteredFor__c =: physicalActivityLog.activityDate];
                    
                    
                    if(activityLogList.size() > 0){
                        physicalActivityRecord.Activity_Log__c = activityLogList[0].Id;
                    }
                    else{
                        ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = physicalActivityLog.contactId,
                                                                               Program_Membership__c = physicalActivityLog.programMembershipId,
                                                                               DateDataEnteredFor__c = physicalActivityLog.activityDate);
                        insert activityLogRecord;

                        physicalActivityRecord.Activity_Log__c = activityLogRecord.Id;
                    }

                    insert physicalActivityRecord;

                    response.statusCode = 201;

                    resultMap.put('success', true);
                    resultMap.put('id', physicalActivityRecord.Id);
                    resultMap.put('physicalActivityRecord', new MobPhysicalActivityDTO(physicalActivityRecord));
                }
                else{
                    RestContext.response.statuscode = 404;

                    resultMap.put('success', false);
                    resultMap.put('error', Label.Invalid_Activity_Date);
                }
            }
            else{
                response.statusCode = 500;
                resultMap.put('success', false);
                resultMap.put('error', 'Both contactId and activityDate are required');
            }
        }
        catch(Exception ex){
            response.statusCode = 500;
            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * update the Physical Activity object
     * @param MobPhysicalActivityDTO
     * @see MobPhysicalActivityDTO
     */
    @HttpPatch
    global static void updatePhysicalActivity(MobPhysicalActivityDTO physicalActivityLog) {
        Map<String, Object> resultMap = new Map<String, Object>();
        RestResponse response = RestContext.response;

        try{
            PhysicalActivity__c physicalActivityRecord = physicalActivityLog.mapToPhysicalActivityLog();
            //Get the latest version of the record, get all necessary fields to send back in case of mismatch
            List<PhysicalActivity__c> serverVersionList = [SELECT Activity_Log__r.Contact__c
                                                            ,Activity_Log__r.Program_Membership__c
                                                            ,Activity_Log__r.DateDataEnteredFor__c
                                                            ,Intensity_Level__c
                                                            ,Minutes__c
                                                            ,NameofPhysicalActivity__c
                                                            ,TotalPhysicalActivityCalories__c
                                                            ,Physical_Activity_Version__c
                                                      FROM PhysicalActivity__c
                                                      WHERE Id =: physicalActivityRecord.Id];

            if(serverVersionList.size() > 0){
                if(physicalActivityRecord.Physical_Activity_Version__c == null || serverVersionList[0].Physical_Activity_Version__c == null || serverVersionList[0].Physical_Activity_Version__c == physicalActivityRecord.Physical_Activity_Version__c){
                    
                    physicalActivityRecord.Physical_Activity_Version__c = physicalActivityRecord.Physical_Activity_Version__c == null ? 0 : physicalActivityRecord.Physical_Activity_Version__c + 1;

                    update physicalActivityRecord;

                    response.statuscode = 200;

                    resultMap.put('success', true);
                    resultMap.put('physicalActivityLog', new MobPhysicalActivityDTO(physicalActivityRecord));
                    
                }
                else{
                    response.statuscode = 405;

                    resultMap.put('success', false);
                    resultMap.put('error', 'record version mismatch');
                    resultMap.put('physicalActivityLog', new MobPhysicalActivityDTO(serverVersionList[0]));
                }
            }
            else{
                response.statuscode = 405;

                resultMap.put('success', false);
                resultMap.put('error', 'record has been deleted on the server');
            }
            
        }
        catch(Exception ex){
            response.statuscode = 404;

            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * method to get the physical activity record for the day
     * @param contactId - the related contact record id 
     * @param activityDate - the related date to log the activity for
     * @return MobPhysicalActivityDTO - the DTO containing the physical activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if error is encountered
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        Map<String, String> paramMap = RestContext.request.params;

        if(paramMap.get('contactId') != null && paramMap.get('activityDate') != null){
            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
            String contactId = paramMap.get('contactId');
            Date activityDate = Date.valueOf(paramMap.get('activityDate'));
            Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(contactId, activityDate);
            List<MobPhysicalActivityDTO> physicalActivityDTOList = new List<MobPhysicalActivityDTO>();
            //verify a program membership record existed for the date
            if(programMembershipRecord != null){   
                List<PhysicalActivity__c> physicalActivityLogList = [SELECT Activity_Log__r.Contact__c
                                                                           ,Activity_Log__r.Program_Membership__c
                                                                           ,Activity_Log__r.DateDataEnteredFor__c
                                                                           ,Intensity_Level__c
                                                                           ,Minutes__c
                                                                           ,NameofPhysicalActivity__c
                                                                           ,TotalPhysicalActivityCalories__c
                                                                           ,Physical_Activity_Version__c
                                                                    FROM PhysicalActivity__c
                                                                    WHERE Activity_Log__r.Contact__c =: contactId
                                                                      AND Activity_Log__r.DateDataEnteredFor__c =: activityDate];

                RestContext.response.statuscode = 200;
                                                         
                if(physicalActivityLogList.size() > 0){
                    for(PhysicalActivity__c physicalActivityRecord : physicalActivityLogList){
                        physicalActivityDTOList.add(getPhysicalActivity(physicalActivityRecord)); 
                    }
                }
                else{
                    physicalActivityDTOList.add(new MobPhysicalActivityDTO(contactId, programMembershipRecord.Id, activityDate));
                }

                returnMap.put('status', true);
                returnMap.put('physicalActivityList', physicalActivityDTOList);
            }
            else{
                RestContext.response.statuscode = 404;

                returnMap.put('success', false);
                returnMap.put('error', Label.Invalid_Activity_Date);
            }
        }
        else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'Parameters: contactId and activityDate are required');
        }
        
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  
    }

    /**
     * method to delete the physical activity record 
     * @param id - the record id 
     * @return HTTP/1.1 200 success
     * @return HTTP/1.1 404 not found if error is encountered
     */
    @HttpDelete
    global static void deletePhysicalActivity() {
        Map<String, Object> returnMap = new Map<String, Object>();
        RestRequest request = RestContext.request;
        String recordId = request.params.get('id');
        
        try{
            delete new PhysicalActivity__c(Id = recordId);

            RestContext.response.statusCode = 200;

            returnMap.put('success', true);
        }
        catch(Exception ex){
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
            returnMap.put('error', ex.getMessage());
        }

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnMap)); 
    }     

    global static MobPhysicalActivityDTO getPhysicalActivity(PhysicalActivity__c physicalActivityLog) {
         MobPhysicalActivityDTO mobileDTO = new MobPhysicalActivityDTO(physicalActivityLog);
         return mobileDTO; 
     }
    
    /**
     * global DTO class that represents a flat structure of the
     * Contact and Program Membership sObjects
     */
    global class MobPhysicalActivityDTO extends HMRActivityDTO{

        public String activity {get; set;}
        public String intensityLevel {get; set;}
        public Integer minutes {get; set;}
        public Integer totalPhysicalActivityCalories{get; set;}

        /**
         * constructors
         */
        public MobPhysicalActivityDTO(String contactId, String programMembershipId, Date activityDate){
            this.contactId = contactId;
            this.programMembershipId = programMembershipId;
            this.activityDate = activityDate;
        }
        public MobPhysicalActivityDTO(PhysicalActivity__c physicalActivityRecord) {
            this.recordId = physicalActivityRecord.Id;
            this.recordVersion = physicalActivityRecord.Physical_Activity_Version__c == null ? 0 : (Integer)physicalActivityRecord.Physical_Activity_Version__c;
            this.contactId = physicalActivityRecord.Activity_Log__r.Contact__c;
            this.programMembershipId = physicalActivityRecord.Activity_Log__r.Program_Membership__c;
            this.activityDate = physicalActivityRecord.Activity_Log__r.DateDataEnteredFor__c;
            this.activity = physicalActivityRecord.NameofPhysicalActivity__c;
            this.intensityLevel = physicalActivityRecord.Intensity_Level__c;
            this.totalPhysicalActivityCalories = (Integer)physicalActivityRecord.TotalPhysicalActivityCalories__c; 
            if(physicalActivityRecord.Minutes__c != null){
                this.minutes = Integer.valueOf(physicalActivityRecord.Minutes__c);
            }
        }  

        public PhysicalActivity__c mapToPhysicalActivityLog() {
            PhysicalActivity__c activityLogRecord = new PhysicalActivity__c(
                Id = this.recordId
               ,Physical_Activity_Version__c = this.recordVersion
               ,Intensity_Level__c = this.intensityLevel
               ,Minutes__c = this.minutes
               ,NameofPhysicalActivity__c = this.activity
               ,TotalPhysicalActivityCalories__c = this.totalPhysicalActivityCalories
            );
            
            return activityLogRecord;
        }   
    }   
}