global virtual with sharing class HMR_CMS_PLP_Hero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_PLP_Hero_ContentTemplate(){}
	global HMR_CMS_PLP_Hero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }

    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

	public Boolean is_eCommerce {
		get {
            return getAttribute('is_eCommerce') == 'true';
        }
	}

	public String PLPHeroTitleText{
         get{
            return getAttribute('PLPHeroTitleText');
        }
     }

     public String PLPHeroSubTitleText{
         get{
            return getAttribute('PLPHeroSubTitleText');
        }
     }

     public String PLPHeroDescriptionText{
         get{
            return getAttribute('PLPHeroDescriptionText');
        }
     }

     public String PLPHeroPromoText{
         get{
            return getAttribute('PLPHeroPromoText');
        }
     }

	global virtual override String getHTML() {
		String html = '';
		try {
			html +=	'<div class="bolded-header text-uppercase">' + PLPHeroTitleText + '</div>' +
				    '<h2 class="title">' + PLPHeroSubTitleText + '</h2>';
			if(is_eCommerce) {
				html += '<div class="bold-title">' + PLPHeroPromoText + '</div>';
		    }
		}
		catch(Exception ex) {
			html += 'Something went wrong ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
		}
		return html;
	}
}