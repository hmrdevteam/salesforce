/*****************************************************
 * Author: Utkarsh Goswami
 * Created Date: 02/19/2018
 * Created By:
 * Last Modified Date: 
 * Last Modified By:
 * Description: Test Coverage for the HMRMobDashboard REST Service
 * ****************************************************/
@isTest
private class HMRMobDashboardTest {
  @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = HMR_TestFactory.createProgram();
      insert programRecord;

      Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
      insert programMembershipRecord;

      //Create activity log records
      List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
      
      for(integer i = 0; i < 7; i++){
        ActivityLog__c activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);
        
        activityLogRecord.DateDataEnteredFor__c = Date.today().addDays(i * -1);

        activityLogList.add(activityLogRecord);
      }

      insert activityLogList;
    }
    
    @isTest
    private static void testGetMethod(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();
      
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
       
      request.requestURI = '/services/apexrest/mobile/user/dashboard/'; 
      request.params.put('communityUserId', userRecord.Id); 
      request.params.put('dashboardDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobDashboard.doGet();
      }

      Test.stopTest();

      //Verify result is returned
      System.assertEquals(200, response.statuscode);
    }
    
    
    @isTest
    private static void testGetMethodNullDate(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/dashboard/';
        request.params.put('communityUserId', '000000000000000000');
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobDashboard.doGet();

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
    }
    
    @isTest
    private static void testGetMethodNullCommunityDate(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/dashboard/';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobDashboard.doGet();

      Test.stopTest();

      //Verify null is returned
     // System.assert(profileResult == null);
    }
   
}