/**
* Trigger Handler for RefundObjTrigger.trigger on Refund__c (isAfter isUpdate)
* Updating order & Order item records based on refund amount paid to the client by HMR
* Order records are updated based on the Total Refund Amount field on Refund Object records
* Order Item records are updated based on the Price Paid by Client field on Refund Items Object records
* Objects referenced --> Refund__c, Refund_Item__c, ccrz__E_Order__c, ccrz__E_OrderItem__c
*
* @Created: 2017-01-23
* @Created by: Mustafa Ahmed (HMR)
* @Modified Date: 2017-01-23
* @Modified by: Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-1839
*/
public class RefundObjTriggerHandler {
    /**
        *  handleAfterUpdate
        *  Trigger Handler method for RefundObjTrigger.trigger
        *  only called for After Update trigger, after Refund__c object record is updated
        *  
        *  @param Trigger.new --> List of new or updated Refund__c records
        *  @param Trigger.OldMap --> List of old version of the Refund__c records
        *  @return void
    */
    public static void handleAfterUpdate(List<Refund__c> newUpdatedRefunds, Map<Id, Refund__c> oldUpdatedRefund) {
        //handler logic
        
        //set of Id's to hold the incoming refund record id's
        Set<Id> newUpdatedRefundIds = new Set<Id>();
        //loop thru Trigger.new and add incoming refund record id's to the set
        for(Refund__c newUpdatedRefund: newUpdatedRefunds) {
            newUpdatedRefundIds.add(newUpdatedRefund.Id);
        }
        //Map to hold only Approved Refund records and ignore everything else
        Map<Id, Refund__c> existingRefunds = new Map<Id, Refund__c>(
            [SELECT hmr_Return_Status__c, hmr_Refund_Amount_requested__c, hmr_Adjustments__c, hmr_Total_Refund_Amount__c FROM Refund__c 
                WHERE hmr_Return_Status__c = 'Approved' AND Id In :newUpdatedRefundIds]);
        //System.debug(existingRefunds);        
        
        //Set of id's to hold Approved Refund Record id's
        Set<Id> RefundIds = new Set<Id>(); 
        //loop thru the Approved refund records map and populate the set of id's
        for(Refund__c newUpdatedRefund: existingRefunds.values()) {
            Refund__c oldRefundRecord = oldUpdatedRefund.get(newUpdatedRefund.Id);
            if(oldRefundRecord.hmr_Return_Status__c != newUpdatedRefund.hmr_Return_Status__c) {
            	RefundIds.add(newUpdatedRefund.Id);    
            }            
        }
        
        //System.debug(RefundIds);
        
        //Map to hold the child Refund Item records for the Approved Refund record coming in
        Map<Id, Refund_Item__c> existingRefundItems;
        //System.debug(existingRefundItems);
        
        if(RefundIds != null && !RefundIds.isEmpty()) {
            existingRefundItems = new Map<Id, Refund_Item__c>(
            	[SELECT Id, Name, hmr_Refund__c, hmr_CC_Order__c, CC_Order_Item__c, hmr_Refund_Product_Quantity__c, Price_paid_by_client__c  FROM Refund_Item__c 
                	WHERE hmr_Refund__c In :RefundIds]);            
        }
        
        
        //set of id's to hold Order record lookup id's from the Refund items Map
        Set<Id> OrderIds = new Set<Id>(); 
        //Loop thru the Refund Items Map and add order lookup id's to the set
        
        if(existingRefundItems != null && !existingRefundItems.isEmpty()) {
            for(Refund_Item__c RefundOrder: existingRefundItems.values()) {
                OrderIds.add(RefundOrder.hmr_CC_Order__c);
            }        
        }
        
        //set of id's to hold Order Items record lookup id's from the Refund items Map
        Set<Id> OrderItemIds = new Set<Id>(); 
        
        if(existingRefundItems != null && !existingRefundItems.isEmpty()) {
           //Loop thru the Refund Items Map and add order item lookup id's to the set
            for(Refund_Item__c RefundItem: existingRefundItems.values()) {
                OrderItemIds.add(RefundItem.CC_Order_Item__c);
            } 
        }
        
        //List to hold the Order records for the Refund Items record 
        List<ccrz__E_Order__c> existingOrders = new List<ccrz__E_Order__c>();        
        if(OrderIds != null && !OrderIds.isEmpty()) {
            existingOrders = [SELECT Id, hmr_Total_Amount_Refunded__c FROM ccrz__E_Order__c 
                	               WHERE Id In :OrderIds];

            //Get the full list of related Refunds for these Orders
            List<Refund__c> relatedRefunds = [SELECT Id, Name, hmr_Order_Number__c,  hmr_Return_Status__c, hmr_Total_Refund_Amount__c
                                                FROM Refund__c
                                                WHERE hmr_Order_Number__c IN :orderIds AND hmr_Return_Status__c = 'Approved' ];
            //Loop through orders and calculate the total refund amount for each order
            //By then looping through related existing refunds and grabbing their totals 
            //To assign to hmr_Total_Amount_Refunded__c on Order object
            Decimal totalRefundAmount;
            for(ccrz__E_Order__c o : existingOrders) {
                totalRefundAmount = 0.00;
                for(Refund__c r : relatedRefunds){
                    if(r.hmr_Order_Number__c == o.Id){
                        totalRefundAmount += r.hmr_Total_Refund_Amount__c;
                    }
                }
                o.hmr_Total_Amount_Refunded__c = totalRefundAmount;
            }
        }

        
        
        
        //Map to hold the Order Item records for the Refund Items record 
        Map<Id, ccrz__E_OrderItem__c> existingOrderItems;               
        if(OrderItemIds != null && !OrderItemIds.isEmpty()) {
            existingOrderItems = new Map<Id, ccrz__E_OrderItem__c>(
            	[SELECT Id, hmr_Refunded_Quantity__c, hmr_Refunded_Amount__c  FROM ccrz__E_OrderItem__c 
                	WHERE Id In :OrderItemIds]);
        }

        
        //Initialize the list of Orders to Update
        List<ccrz__E_OrderItem__c> OrderItemsToUpdate = new List<ccrz__E_OrderItem__c>();
        
        if(existingRefundItems != null && !existingRefundItems.isEmpty()) {
            //Loop thru all the refund item records map
            for(Refund_Item__c refundedItem : existingRefundItems.values()){
                //Loop thru all the order item records map
                for(ccrz__E_OrderItem__c OrderItemsUpdate: existingOrderItems.values()) {
                    //check if the refund item is looking upto the appropriate order item record
                    if(refundedItem.CC_Order_Item__c == OrderItemsUpdate.id){                   
                        //Add Price_paid_by_client__c to the hmr_Refunded_Amount__c field
                        if(OrderItemsUpdate.hmr_Refunded_Amount__c > 0){
                            OrderItemsUpdate.hmr_Refunded_Amount__c = OrderItemsUpdate.hmr_Refunded_Amount__c +  refundedItem.Price_paid_by_client__c;
                        }
                        else{
                            OrderItemsUpdate.hmr_Refunded_Amount__c = refundedItem.Price_paid_by_client__c;                        
                        }                        
                        //Add Refund Quantity to the number of Order Items that have already been refunded
                        if(OrderItemsUpdate.hmr_Refunded_Quantity__c > 0){                            
                            OrderItemsUpdate.hmr_Refunded_Quantity__c = OrderItemsUpdate.hmr_Refunded_Amount__c +  refundedItem.hmr_Refund_Product_Quantity__c;                              
                        }
                        else{
                            OrderItemsUpdate.hmr_Refunded_Quantity__c = refundedItem.hmr_Refund_Product_Quantity__c;
                        }
                        
                        //add the udpated order items records to the list
                        OrderItemsToUpdate.add(OrderItemsUpdate);
                        System.debug('ItemsBeingUpdated' + OrderItemsToUpdate);                    
                    }                       
                }        
            }
        }                     
        try {
            //check for order items list & Existing Orders list to update for empty and then update
            if(!OrderItemsToUpdate.isEmpty()){
                update OrderItemsToUpdate;
            }
            
            if(!existingOrders.isEmpty()){
                update existingOrders;
            }                
        }
        catch (Exception ex) {
            System.debug('Exception in RefundObjTriggerHandler handleAfterUpdate ' + ex.getMessage());
        }       
    }
}