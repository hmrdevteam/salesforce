/**
* Apex Content Template Controller for the Legal Disclaimer Component
*
* @Date: 07/31/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 07/31/2017
* @JIRA: HPRP-4017
*/
global virtual class HMR_CMS_LegalDisclaimer_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_LegalDisclaimer_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_LegalDisclaimer_ContentTemplate() {}
	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String DisclaimerText{
         get{
             return getPropertyWithDefault('DisclaimerText', 'Average weight loss for the Healthy Solutions at Home® program with phone coaching is 23 lbs. at 12 weeks, and 28 lbs. at 26 weeks. Int J Obes 2007;31:1270-1276; Obes 2013;21:1951-1959');
         }
     }

     public String ContainerWrapperCSSClass{
        get{
             return getPropertyWithDefault('ContainerWrapperCSSClass', '');
         }
     }

    global virtual override String getHTML(){
    	String html =  '<div class="' + ContainerWrapperCSSClass + '">' + 
                          '<div class="container hmr">' +
    				        '<div class="row">' +
    				          '<div class="col-xs-12 average">' +
    				              DisclaimerText +
    				          '</div>' +
    				        '</div>' +
    				      '</div>' +
                        '</div>';
  		
    	return html;
    }
}