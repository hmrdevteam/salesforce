/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 21 June 2017
 * Created By: Nathan Anderson
 * Last Modified Date:
 * Last Modified By:
 * Description:
This logic is used by the HMR_OneTimeBillingChanges page, called from "Update Billing Information" button on Order page layout.
This allows Program Specialists to make updates to Pending orders, selecting a different Credit Card for this one time order.
 * ****************************************************/

public class HMR_OneTimeBillingChangesExt {

    //public variables
    public List<ccrz__E_StoredPayment__c> paymentList {get;set;}
    public ccrz__E_Order__c order {get;set;}
    public Id orderId {get;set;}
    public Id selectedPaymentId{get;set;}
    public ccrz__E_StoredPayment__c selectedPayment {get;set;}

    public HMR_OneTimeBillingChangesExt(ApexPages.StandardController stdController) {
        //get the order id from the URL
        orderId = apexpages.currentpage().getparameters().get('id');

        //get the full record
        order = [SELECT Id, Name, ccrz__Contact__c, ccrz__User__c, ccrz__BillTo__c, ccrz__ShipTo__c, ccrz__TotalAmount__c,
                        ccrz__Contact__r.FirstName, ccrz__Contact__r.LastName, ccrz__Contact__r.Client_Id__c, ccrz__Contact__r.AccountId
                    FROM ccrz__E_Order__c WHERE Id = :orderId];
        //Query for all Stored Payment records for this User, including the related address information for display
        paymentList = [SELECT Id, Name, ccrz__DisplayName__c, ccrz__AccountNumber__c, ccrz__ExpMonth__c, ccrz__ExpYear__c,
                                    HMR_Contact_Address__c, HMR_Contact_Address__r.ccrz__AddressFirstline__c, HMR_Contact_Address__r.ccrz__AddressSecondline__c,
                                    HMR_Contact_Address__r.ccrz__City__c, HMR_Contact_Address__r.ccrz__StateISOCode__c, HMR_Contact_Address__r.ccrz__PostalCode__c,
                                    HMR_Contact_Address__r.Id, ccrz__Token__c, ccrz__PaymentType__c, Card_Name__c, OwnerId
                        FROM ccrz__E_StoredPayment__c WHERE OwnerId = :order.ccrz__User__c];
    }


    public void selectPayment(){
        //get the id of the selected record from the page parameters
        selectedPaymentId = System.currentPagereference().getParameters().get('paymentId');
        //query for the full record from the id
        selectedPayment = [SELECT Id, Name, ccrz__DisplayName__c, ccrz__AccountNumber__c, ccrz__ExpMonth__c, ccrz__ExpYear__c,
                                    HMR_Contact_Address__c, HMR_Contact_Address__r.ccrz__AddressFirstline__c, HMR_Contact_Address__r.ccrz__AddressSecondline__c,
                                    HMR_Contact_Address__r.ccrz__City__c, HMR_Contact_Address__r.ccrz__StateISOCode__c, HMR_Contact_Address__r.ccrz__PostalCode__c,
                                    HMR_Contact_Address__r.Id, ccrz__Token__c, ccrz__PaymentType__c, Card_Name__c, OwnerId
                            FROM ccrz__E_StoredPayment__c WHERE Id = :selectedPaymentId];

    }

    public PageReference updateOrder(){
        //Initialize empty list of Transaction Payments, to use to update
        List<ccrz__E_TransactionPayment__c> transactionsToUpdate = new List<ccrz__E_TransactionPayment__c>();
        //Query for the Transaction Payment record that we will need to update.
        //Will likely be the only record, but we'll check that there is no verification code to eliminate any Declined or Error records
        List<ccrz__E_TransactionPayment__c> tp = [SELECT Id, ccrz__CCOrder__c, ccrz__AccountNumber__c, ccrz__BillTo__c, ccrz__ExpirationMonth__c, ccrz__ExpirationYear__c,
                                                    ccrz__PaymentType__c, ccrz__StoredPayment__c, ccrz__Token__c, ccrz__VerificationCode__c
                                            FROM ccrz__E_TransactionPayment__c WHERE ccrz__CCOrder__c = :order.Id AND ccrz__VerificationCode__c = null LIMIT 1];

        //Initialize empty list of Transaction Payments, to use to update
        List<ccrz__E_Order__c> ordersToUpdate = new List<ccrz__E_Order__c>();

        //check for null on transaction payment and selected payment record
        if(selectedPayment != null) {
            if(tp.size() > 0) {
                //Using the values from the selected Stored Payment, update the Transaction Payment record, including the Stored Payment and BillTo lookup fields
                tp[0].ccrz__StoredPayment__c = selectedPayment.Id;
                tp[0].ccrz__AccountNumber__c = selectedPayment.ccrz__AccountNumber__c;
                //tp.ccrz__BillTo__c = selectedPayment.HMR_Contact_Address__r.Id;
                tp[0].ccrz__ExpirationMonth__c = selectedPayment.ccrz__ExpMonth__c;
                tp[0].ccrz__ExpirationYear__c = selectedPayment.ccrz__ExpYear__c;
                tp[0].ccrz__PaymentType__c = selectedPayment.ccrz__PaymentType__c;
                tp[0].ccrz__Token__c = selectedPayment.ccrz__Token__c;
                //Add updated record to list for DML update
                transactionsToUpdate.add(tp[0]);


                //Set the BillTo field on the order to to the correct Contact Address record from the selected Stored Payment
                order.ccrz__BillTo__c = selectedPayment.HMR_Contact_Address__c;
                //Add updated record to list for DML update
                ordersToUpdate.add(order);
                //Run DML update
                try{
                    update transactionsToUpdate;
                    update ordersToUpdate;
                } catch(Exception e){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error updating this order.'));
                }

                //return to Order page after complete
                PageReference orderPage = new PageReference('/'+order.Id);
                return orderPage;

            } else {

                ccrz__E_TransactionPayment__c tpInsert = new ccrz__E_TransactionPayment__c();

                tpInsert.ccrz__BillTo__c = order.ccrz__Contact__r.AccountId;
                tpInsert.ccrz__CCOrder__c = order.Id;
                tpInsert.ccrz__Contact__c = order.ccrz__Contact__c;
                tpInsert.ccrz__User__c = order.ccrz__User__c;
                tpInsert.ccrz__Storefront__c = 'DefaultStore';

                tpInsert.ccrz__StoredPayment__c = selectedPayment.Id;
                tpInsert.ccrz__AccountNumber__c = selectedPayment.ccrz__AccountNumber__c;
                //tp.ccrz__BillTo__c = selectedPayment.HMR_Contact_Address__r.Id;
                tpInsert.ccrz__ExpirationMonth__c = selectedPayment.ccrz__ExpMonth__c;
                tpInsert.ccrz__ExpirationYear__c = selectedPayment.ccrz__ExpYear__c;
                tpInsert.ccrz__PaymentType__c = selectedPayment.ccrz__PaymentType__c;
                tpInsert.ccrz__Token__c = selectedPayment.ccrz__Token__c;
                //Add updated record to list for DML update
                transactionsToUpdate.add(tpInsert);

                //Set the BillTo field on the order to to the correct Contact Address record from the selected Stored Payment
                order.ccrz__BillTo__c = selectedPayment.HMR_Contact_Address__c;
                //Add updated record to list for DML update
                ordersToUpdate.add(order);

                //Run DML ops
                try{
                    insert transactionsToUpdate;
                    update ordersToUpdate;
                } catch(Exception e){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error updating this order.'));
                }

                //return to Order page after complete
                PageReference orderPage = new PageReference('/'+order.Id);
                return orderPage;
            }
        }
        else {
        //Throw an error if the update button is hit before a stored payment is selected
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a stored payment.'));

        return null;
        }
    }

    public PageReference cancel(){
        PageReference orderPage = new PageReference('/'+order.Id);
        return orderPage;
    }
}