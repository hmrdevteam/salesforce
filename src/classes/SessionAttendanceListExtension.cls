/*
 * Extension of EditableList for Logging Session Attendance
 * @Date: 2017-1-30
 * @Author Nathan Anderson (Magnet 360)
 * @JIRA:
 *
 * Usage:
 *   This is the implementation class where Object specific references
 *   need to be defined. Please see EditableList.cls for more information
 *   on what each method is responsible for.
 *
 *   Required methods:
 *     - SessionAttendanceListExtension(ApexPages.StandardController)
 *     - getChildren()
 *     - initChildRecord()
 *
 */
public class SessionAttendanceListExtension extends EditableList {

  public SessionAttendanceListExtension(ApexPages.StandardController stdController) {

    super(stdController);
    //Query for fields on the Child records for edit/view on page
    this.childList = [SELECT Id,
                          Name,
                          hmr_Client_Name__r.FirstName,
                                                    hmr_Client_Name__r.LastName,
                          hmr_Attendance__c,
                          hmr_Make_up_Client__c
                      FROM Session_Attendee__c
                      WHERE hmr_Coaching_Session__c =: mysObject.Id AND hmr_Attendance__c != 'Make-up Scheduled'];
  }

  /*
   * This method is necessary for reference on the Visualforce page,
   * in order to reference non-standard fields.
   */
  public List<Session_Attendee__c> getChildren() {
    return (List<Session_Attendee__c>)childList;
  }

  public override sObject initChildRecord() {
    Session_Attendee__c child = new Session_Attendee__c();
    // Set lookup on Session Attendee to the parent record
    child.hmr_Coaching_Session__c = mysObject.Id;

    return child;
  }
}