global virtual class HMR_CMS_P2_KitConfig_ContentTemplate extends cms.ContentTemplateController{

  public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
  }

  global HMR_CMS_P2_KitConfig_ContentTemplate(cms.createContentController cc) {
    super(cc);
  }

  global HMR_CMS_P2_KitConfig_ContentTemplate() {}

  /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global String returnNotNullAndNotEmptyString(String value) {
        if(value != null && value != '') {
            return value;
        }
        return '';
    }

    public String kitSKU{
         get{
            return getPropertyWithDefault('kitSKU', '[]');
        }
     }

     public Boolean isAdmin {
        get {
            return getAttribute('isAdmin') == 'true';
        }
    }

    global virtual override String getHTML() {
        String html = '';
        String shakesHtml = '';String entreesHtml = '';String barsHtml = '';
        String kitName = kitSKU;
        String baseUrl = IsProduction() ? '' : Site.getBaseUrl();
        try {
            //String addToCartFunction = isAdmin == true ? 'addCartClickAdmin' :'addCartClick';

            //Declare and initialize Set of string with selected Kit SKU to pass into API call
            Set<String> kitProduct_SKUList = new Set<String>{kitSKU};

            Map<String, Object> kitInputData = new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => 6,
                ccrz.ccApiProduct.PRODUCTSKULIST => kitProduct_SKUList,
                ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
                ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
                ccrz.ccAPIProduct.PARAM_BY_ASC => true,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                    }
                }
            };

            Map<String, Object> kitOutputProductData = ccrz.ccAPIProduct.fetch(kitInputData);
            List<Map<String, Object>> kitOutputProductList = (List<Map<String, Object>>) kitOutputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);

            Set<String> composite_sfidSet = new Set<String>(); //Set to store composite product Ids
            Set<String> composite_skuSet = new Set<String>(); //Set to store composite product SKUs

            Map<String,Integer> quantityMap = new Map<String, Integer>(); //Map of default product quantities within kit
            Map<String,String> productGroupMap = new Map<String, String>(); //Map of Product Groups/Categories

            if(kitOutputProductList[0].get('sfdcName') != null){
              kitName = (String) kitOutputProductList[0].get('sfdcName');
            }
            List<Map<String, Object>> compositeProductList = (List<Map<String, Object>>) kitOutputProductList[0].get('compositeProductsS');
            //Loop through kit product's composite products list and populate composite_sfidSet for 2nd Product API to retrieve product info
            for(Map<String, Object> compositeProductObj : compositeProductList){
                String product_sfid = '';
                String product_SKU = '';
                String product_Group = '';
                Integer quantity = 0;

                Map<String, Object> componentRelationshipMap = (Map<String, Object>)compositeProductObj.get('componentR');
                Map<String, Object> componentProductGroupMap = (Map<String, Object>)compositeProductObj.get('productGroupR');

                product_sfid = String.valueOf(componentRelationshipMap.get('sfid'));
                product_SKU = String.valueOf(componentRelationshipMap.get('SKU'));
                product_Group = String.valueOf(componentProductGroupMap.get('sfdcName'));

                quantityMap.put(product_SKU, Integer.valueOf(compositeProductObj.get('quantity')));
                productGroupMap.put(product_SKU, product_Group);
                composite_sfidSet.add(product_sfid);
                composite_skuSet.add(product_SKU);
            }

            Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => 6,
                ccrz.ccApiProduct.PRODUCTIDLIST => composite_sfidSet,
                ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
                ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
                ccrz.ccAPIProduct.PARAM_BY_ASC => true,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                    }
                }
            };

            Map<String, Object> outputProductData = ccrz.ccAPIProduct.fetch(inputData);
            if(outputProductData != null && outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
                List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);
                if(outputProductList.size() > 0) {

                    //Price lists Logic Map Input setup
                    Map<String, Object> inputPriceListsData = new Map<String, Object>{
                        ccrz.ccAPIPriceList.PRODUCTSKUS => composite_skuSet,
                        ccrz.ccAPI.API_VERSION => 6,
                        ccrz.ccAPI.SIZING => new Map<String, Object>{
                            ccrz.ccAPIPriceList.ENTITYNAME => new Map<String, Object>{
                                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                            }
                        }
                    };
                    Map<String, Object> outputPriceListsData = ccrz.ccAPIPriceList.fetch(inputPriceListsData);
                    Map<String, Map<String, String>> productPriceListsMap = HMR_CMS_CCRZ_Utility.getProductPriceListsMap(outputPriceListsData);
                    Map<String, String> employeesPriceList = productPriceListsMap.get('employeesPriceListMap');
                    Map<String, String> friendsandfamilyPriceList = productPriceListsMap.get('friendsandfamilyPriceListMap');
                    Map<String, String> standardPriceList = productPriceListsMap.get('standardPriceListMap');
                    //Price Lists Logic End
                    //Product HTML rendering Loop Start
                    Integer tabIndex = 0;
                    Set<String> grps = new Set<String>{'Shakes', 'Entrees', 'Bars'};
                    for(Map<String, Object> productObject: outputProductList) {
                        tabIndex++;
                        //Product status must be released and product category must be Shakes, Entrees or Bars
                        if(productObject.get('productStatus') == 'Released' && grps.contains(returnNotNullAndNotEmptyString(productGroupMap.get(String.valueOf(productObject.get('SKU')))))) {
                            String product_sfid = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfid')));
                            String productSKU = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('SKU')));
                            String productName = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfdcName')));
                            String productGroup = returnNotNullAndNotEmptyString(productGroupMap.get(String.valueOf(productObject.get('SKU'))));

                            //Append html block to appropriate html string variable
                            if(productGroup == 'Shakes'){
                                shakesHtml += HMR_CMS_CCRZ_Utility.phase2OrderItemHTML(product_sfid, productGroup, productName, productSKU, returnNotNullAndNotEmptyString(standardPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(employeesPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(friendsandfamilyPriceList.get(productSKU)), tabIndex);
                            }
                            else if(productGroup == 'Entrees'){
                                entreesHtml += HMR_CMS_CCRZ_Utility.phase2OrderItemHTML(product_sfid, productGroup, productName, productSKU, returnNotNullAndNotEmptyString(standardPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(employeesPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(friendsandfamilyPriceList.get(productSKU)), tabIndex);
                            }
                            else if(productGroup == 'Bars'){
                                barsHtml += HMR_CMS_CCRZ_Utility.phase2OrderItemHTML(product_sfid, productGroup, productName, productSKU, returnNotNullAndNotEmptyString(standardPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(employeesPriceList.get(productSKU)),
                                                                                                  returnNotNullAndNotEmptyString(friendsandfamilyPriceList.get(productSKU)), tabIndex);
                            }
                        }
                    }
                    //Product HTML rendering Loop End
                }
            }
            // html += '<div id="HMR_productDetails">' +
            //            '<div class="dynamicKitSection">' +
            //               '<!--**********************-->' +
            //               '<div class="order-review">' +
            //                  '<main class="main-container ">' +
            //                     '<div class="row">' +
            //                        '<div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-8 col-md-offset-0">' +
            //                           '<h1 class="col-xs-12 main-title">Welcome to Phase 2!</h1>' +
            //                           '<section class="order-detail col-xs-12">' +
            //                              '<div class="row">' +
            //                                 '<div class="col-xs-12 order-detail-item">' +
            //                                    '<p>' +
            //                                       'Please choose the first processing date for your Phase 2 monthly auto-delivery order.' +
            //                                    '</p>' +
            //                                    '<span class="date-label">' +
            //                                    'Processing Date: ' +
            //                                    '</span>' +
            //                                    '<div class="processing-date" style="display:inline-block;"></div>' +
            //                                    '<!--************************************************-->' +
            //                                    '<a class="calendar-display hmr-link" id="datepicker" readonly="readonly"> ' +
            //                                    'CHANGE' +
            //                                    '</a>' +
            //                                    '<div class="row">' +
            //                                       '<div class="col-xs-12 desc">' +
            //                                          '<p>' +
            //                                             'With your minimum order of $50, you can enjoy monthly auto-delivery and free shipping.  (Order $100 or more and get an extra 10% discount!)  Choose your foods below to create your standard monthly order.' +
            //                                          '</p>' +
            //                                       '</div>' +
            //                                    '</div>' +
            //                                 '</div>' +
            //                              '</div>' +
            //                           '</section>' +
            //                           '<section class="categories">' +
            //                              '<!--**********************-->' +
            //                              '<hr class="category-upper-bar">' +
            //                              '<div class="row">' +
            //                                 '<div class="col-xs-12">' +
            //                                    '<div class="col-xs-8 col-sm-6">' +
            //                                       '<img class="hidden-xs" src="/resource/1504909682000/HMRIcons/img/icon-shake.png">' +
            //                                       '<div class="cat-title">' +
            //                                          '<h3>Shakes</h3>' +
            //                                          '<a class="hidden-xs" onclick="p2Flow.viewItems(\'' + 'Shakes' + '\');">VIEW ITEMS</a>' +
            //                                       '</div>' +
            //                                    '</div>' +
            //                                    '<p class="col-xs-8 col-sm-6">' +
            //                                       'Have a shake or cereal instead of a higher calorie meal or snack and you could save hundreds of calories a day.' +
            //                                    '</p>' +
            //                                    '<a class="col-xs-8 visible-xs" onclick="p2Flow.viewItems(\'' + 'Shakes' + '\');">VIEW ITEMS</a>' +
            //                                 '</div>' +
            //                              '</div>' + shakesHtml +
            //                              '<!--**********************-->' +
            //                              '<hr class="category-upper-bar">' +
            //                              '<div class="row">' +
            //                                 '<div class="col-xs-12">' +
            //                                    '<div class="col-xs-8 col-sm-6">' +
            //                                       '<img class="hidden-xs" src="/resource/1504909682000/HMRIcons/img/icon-entree.png">' +
            //                                       '<div class="cat-title">' +
            //                                          '<h3>Entrees</h3>' +
            //                                          '<a class="hidden-xs" onclick="p2Flow.viewItems(\'' + 'Entrees' + '\');">VIEW ITEMS</a>' +
            //                                       '</div>' +
            //                                    '</div>' +
            //                                     '<p class="col-xs-8 col-sm-6">' +
            //                                        'Lunch or dinner can be ready in just a minute. Add any of your favorite vegetables for a meal that’s even more filling and nutritous.' +
            //                                     '</p>' +
            //                                     '<a class="col-xs-8 visible-xs" onclick="p2Flow.viewItems(\'' + 'Entrees' + '\');">VIEW ITEMS</a>' +
            //                                  '</div>' +
            //                               '</div>' + entreesHtml +
            //                               '<!--**********************-->' +
            //                               '<hr class="category-upper-bar">' +
            //                               '<div class="row">' +
            //                                  '<div class="col-xs-12">' +
            //                                     '<div class="col-xs-8 col-sm-6">' +
            //                                        '<img class="hidden-xs" src="/resource/1504909682000/HMRIcons/img/icon-bars.png">' +
            //                                        '<div class="cat-title">' +
            //                                           '<h3>Bars</h3>' +
            //                                           '<a class="hidden-xs" onclick="p2Flow.viewItems(\'' + 'Bars' + '\');">VIEW ITEMS</a>' +
            //                                        '</div>' +
            //                                     '</div>' +
            //                                     '<p class="col-xs-8 col-sm-6">' +
            //                                        'Satisfy your craving for something sweet, while staying on your diet plan. Great for snacking!' +
            //                                     '</p>' +
            //                                     '<a class="col-xs-8 visible-xs" onclick="p2Flow.viewItems(\'' + 'Bars' + '\');">VIEW ITEMS</a>' +
            //                                  '</div>' +
            //                               '</div>' + barsHtml +
            //                            '</section>' +
            //                         '</div>' +
            //                         '<!--***************-->' +
            //                         '<div class="hidden-xs hidden-sm col-md-4">' +
            //                            '<section class="order-summary hidden-xs hidden-sm col-md-4">' +
            //                               '<div class="menu">' +
            //                                  '<div class="title">' +
            //                                     'Order Summary' +
            //                                 '</div>' +
            //                                 '<!--*******-->' +
            //                                 '<div class="order-items-list">' +
            //                                 '</div>' +
            //                                 '<!--**********************-->' +
            //                                 '<div class="row subtotal">' +
            //                                    '<div class="col-xs-12">' +
            //                                       '<div class="col-xs-12 divider"></div>' +
            //                                    '</div>' +
            //                                    '<div class="col-xs-12 padding-top-20">' +
            //                                       '<div class="row">' +
            //                                          '<div class="col-xs-6 item">' +
            //                                             'Subtotal' +
            //                                          '</div>' +
            //                                          '<div class="col-xs-6 price subtotal-val">$0.00</div>' +
            //                                       '</div>' +
            //                                    '</div>' +
            //                                    '<!--**********************-->' +
            //                                 '</div>' +
            //                                 '<div class="row subtotal">' +
            //                                    '<div class="col-xs-12">' +
            //                                       '<div class="col-xs-12 divider"></div>' +
            //                                    '</div>' +
            //                                    '<div class="col-xs-6 item total">' +
            //                                       'Total' +
            //                                    '</div>' +
            //                                    '<div class="col-xs-6 price total total-val">$0.00</div>' +
            //                                    '<!--**********************-->' +
            //                                    '<div class="col-xs-12">' +
            //                                       '<button id="hmr-p2-proceed-cart" class="btn btn-green margin-top-20 addToCart" onclick="p2Flow.checkoutBtnClicked();">Proceed to Cart</button>' +
            //                                       '<div class="error-display"></div>'+
            //                                    '</div>' +
            //                                 '</div>' +
            //                              '</div>' +
            //                           '</section>' +
            //                        '</div>' +
            //                     '</div>' +
            //                     '<div class="package-icon visible-xs visible-sm">' +
            //                        '<img id="popout_btn" src="/resource/1504909682000/HMRIcons/img/icon-package.png">' +
            //                     '</div>' +
            //                  '</main>' +
            //                  '<div class="side-popout-menu visible-xs visible-sm">' +
            //                     '<div id="popout_right" class="menu">' +
            //                        '<div class="title">' +
            //                           'Order Summary' +
            //                        '</div>' +
            //                        '<!--**********************-->' +
            //                        '<div class="order-items-list">' +
            //                        '</div>' +
            //                        '<!--**********************-->' +
            //                        '<div class="row subtotal">' +
            //                           '<div class="col-xs-12">' +
            //                              '<div class="col-xs-12 divider"></div>' +
            //                           '</div>' +
            //                           '<div class="col-xs-12 padding-top-20">' +
            //                              '<div class="row">' +
            //                                 '<div class="col-xs-6 item">' +
            //                                    'Subtotal' +
            //                                 '</div>' +
            //                                 '<div class="col-xs-6 price subtotal-val">$0.00</div>' +
            //                              '</div>' +
            //                           '</div>' +
            //                           '<!--**********************-->' +
            //                        '</div>' +
            //                        '<div class="row subtotal">' +
            //                           '<div class="col-xs-12">' +
            //                              '<div class="col-xs-12 divider"></div>' +
            //                           '</div>' +
            //                           '<div class="col-xs-6 item total">' +
            //                              'Total' +
            //                           '</div>' +
            //                           '<div class="col-xs-6 price total total-val">0.00</div>' +
            //                           '<!--**********************-->' +
            //                           '<div class="col-xs-12">' +
            //                              '<button class="btn btn-green margin-top-20 checkoutBtn addToCart opacity-none" onclick="p2Flow.checkoutBtnClicked();">Add My Kit to Cart</button>' +
            //                              '<div class="error-display"></div>'+
            //                           '</div>' +
            //                        '</div>' +
            //                     '</div>' +
            //                  '</div>' +
            //               '</div>' +
            //            '</div>' +
            //         '</div>';
            html += '<main class="cart-wrapper phase-two-kit-config-wrapper">' +
                        '<div class="container">' +
                            '<div class="row">' +
                                '<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 cart-all-details">' +
                                    '<div class="row">' +
                                        '<h2>Welcome to Phase 2!</h2>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="hmr-desc-text text-left">' +
                                            'Please choose the first processing date for your Phase 2 monthly auto-delivery order.' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row date-wrapper">' +
                                        '<span class="bold-display-lg">Processing Date: </span>' +
                                        '<span class="processing-date hmr-desc-text text-left"></span>' +
                                        '<span class="data-selector-wrapper">' +
                                            '<a class="calendar-display hmr-allcaps" id="datepicker" readonly="readonly">Change</a>' +
                                        '</span>' +
                                    '</div>' +
                                    '<div class="row hmr-desc-text phase-two-info">' +
                                        'With your minimum order of $50, you can enjoy monthly auto-delivery and free shipping. (Order $100 or more and get an extra 10% discount!) Choose your foods below to create your standard monthly order.' +
                                    '</div>' +
                                    '<div class="row product-categories-wrapper">' +
                                        '<div class="col-lg-1 no-left-padding">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/Icons/Gray/ShakesP2.png" />' +
                                        '</div>' +
                                        '<div class="col-lg-3 no-right-padding">' +
                                            '<div class="sub-header">Shakes/Cereal</div>' +
                                            '<a class="hmr-allcaps view_kit_items" data-toggle="view_shakes_wrapper">View Items</a>' +
                                        '</div>' +
                                        '<div class="col-lg-1">' +'</div>' +
                                        '<div class="col-lg-7 hmr-desc-text">' +
                                            'Have a shake or cereal instead of a higher calorie meal or snack and you could save hundreds of calories a day.' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="view_shakes_wrapper hidden">' +
                                        shakesHtml +
                                    '</div>' +
                                    '<div class="row product-categories-wrapper">' +
                                        '<div class="col-lg-1 no-left-padding">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/Icons/Gray/EntreesP2.png" />' +
                                        '</div>' +
                                        '<div class="col-lg-3 no-right-padding">' +
                                            '<div class="sub-header">Entrees</div>' +
                                            '<a class="hmr-allcaps view_kit_items" data-toggle="view_entrees_wrapper">View Items</a>' +
                                        '</div>' +
                                        '<div class="col-lg-1">' +'</div>' +
                                        '<div class="col-lg-7 hmr-desc-text">' +
                                            'Lunch or dinner can be ready in just a minute. Add any of your favorite vegetables for a meal that’s even more filling and nutritous.' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="view_entrees_wrapper hidden">' +
                                        entreesHtml +
                                    '</div>' +
                                    '<div class="row product-categories-wrapper">' +
                                        '<div class="col-lg-1 no-left-padding">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/Icons/Gray/BarsP2.png" />' +
                                        '</div>' +
                                        '<div class="col-lg-3 no-right-padding">' +
                                            '<div class="sub-header">Bars/Flavorings</div>' +
                                            '<a class="hmr-allcaps view_kit_items" data-toggle="view_bars_wrapper">View Items</a>' +
                                        '</div>' +
                                        '<div class="col-lg-1">' +'</div>' +
                                        '<div class="col-lg-7 hmr-desc-text">' +
                                            'Satisfy your craving for something sweet, while staying on your diet plan. Great for snacking!' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="view_bars_wrapper hidden">' +
                                        barsHtml +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 order-summary-right-rail">' +
                                    '<div class="sub-header">' +
                                        'Order Summary' +
                                    '</div>' +
                                    '<div class="order-items-list">' +
                                    '</div>' +
                                    '<div class="row order-item order-total-row stick-left">' +
                                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 bold-display-lg no-left-padding">' +
                                            'Total' +
                                        '</div>' +
                                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bold-display-lg no-right-padding">' +
                                            '<div class="text-right total-val"></div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-right-padding">' +
                                            '<button id="checkOutBtn" data-loading-text="<i class=\'hmr-button-loading-inverse\'></i>" type="button" class="btn callout-btn green top" onclick="p2Flow.checkoutBtnClicked();">Proceed to Cart</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</main>';
            html+= '<input type="hidden" id="current_kitSKU" value="' + kitSKU + '"/>';
            html+= '<input type="hidden" id="current_kitName" value="' + kitName + '"/>';
        }
        catch(Exception ex) {
            html += 'Something went wrong in P2 Kit Config' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
        }
        return html;
    }
}