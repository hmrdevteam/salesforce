/**
    --- This class is not in user please goto 'HMR_CMS_ConsentOCMSServiceInterface' class ---
     
* This class is a controller consent agreement functionality 
* @Date:4/4/2017
* @Author: Aslesha(Mindtree)
* @Updated by : 
* @JIRA:
*/

global  class  HMR_CMS_ConsentServiceInterface
{
    /**
     * Remote action to get the information about consent for particular contact
     *
     * @param Id contactId : Id of the contact 
     *
     * @return Boolean : if there is any pending consent return true OR return false
     *
     * @Author: Aslesha (Mindtree)
     * @Date: 04/4/2017
     */
        
    @RemoteAction
    global static String GetConsent() {
    
        //User loggedInUser = [Select ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];   
        // aslesha 0054D000000IuI8
        // cathy 0054D000000Hzo7
        User loggedInUser = [Select ContactId FROM User WHERE Id = '0054D000000Hzo7' LIMIT 1];   
        
        List<Consent_Agreement__c> conAgreeList=[Select Id, Name,Version__c from Consent_Agreement__c  where Contact__c=:loggedInUser.ContactId and Status__c=:'Notified'];
        system.debug('query===>Select Id, Name from Consent_Agreement__c  where Contact__c=:'+loggedInUser.ContactId +'and Status__c=:\'Notified\'');
        system.debug('response=====>'+conAgreeList);
        
        Map<String, Id> versionIdMap = new Map<String, Id>();
        
        String versionIDs;
        
        for(Consent_Agreement__c con :conAgreeList )
        {
            versionIdMap.put('version_id',con.Version__c);
            versionIDs+=con.Version__c+'::';
        }
        
       // return versionIDs;

       return JSON.serialize(conAgreeList);
               
        /*if(conAgreeList.size()>0)
        {
              return true;
        }else
        {            
              return false; 
        }
          */
    }
    
     /**
     * Remote action to get the information about consent for particular contact
     *
     * @param Id contactId : Id of the contact 
     *
     * @return List<Consent_Agreement__c>: List of pending consents
     *
     * @Author: Aslesha (Mindtree)
     * @Date: 04/4/2017
     */
    /*
    @RemoteAction
    global static List<Consent_Agreement__c> GetConsentDetails(Id contactId) {
    
        List<Consent_Agreement__c> conAgreeList=[Select Id, Name from Consent_Agreement__c  where Contact__c=:contactId and Status__c=:'Notified'];
        
        return conAgreeList;
    }
    
     /**
     * Remote action to set the information about consent for particular contact
     *
     * @param Id contactId : Id of the contact 
     *
     * @return Boolean : true if consent records are created & false if not 
     *
     * @Author: Aslesha (Mindtree)
     * @Date: 04/4/2017
     */    
    
    
    @RemoteAction
    global static Boolean SetConsent() {
    
    
        User loggedInUser = [Select ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];   
            
        List<Consent_Agreement__c> conAgreeList=[Select Id, Name from Consent_Agreement__c  where Contact__c=:loggedInUser.ContactId and Status__c=:'Notified'];
        
        List<Consent_Agreement__c> updateConAgreeList=new  List<Consent_Agreement__c>();
        
        if(conAgreeList.size()>0)
        {
        
            for(Consent_Agreement__c agreement:conAgreeList){
                
                 agreement.Status__c='Consented';
                 updateConAgreeList.add(agreement);
            }
            
            update updateConAgreeList;   
            return true;         
                    
        }else
         return false; 
        
    }
    
}