/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 27th 2017
Target Class(ses): HMR_SiteUrlRewriter
*/

@isTest(SeeAllData=true)
private class HMR_SiteUrlRewriter_Test{
    @isTest
    static void testGenerateUrlForCCRZ() {
       HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
       List<PageReference> testPageReferences = new List<PageReference>();
       testPageReferences.add(new PageReference('/ccrz__Products'));
       System.assert(rewriter.generateUrlFor(testPageReferences) != null);
    }

    @isTest
    static void testGenerateUrlForNonCCRZ() {
       HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
       List<PageReference> testPageReferences = new List<PageReference>();
       testPageReferences.add(new PageReference('/home'));
       System.assert(rewriter.generateUrlFor(testPageReferences) != null);
    }

    @isTest
    static void testMapUrl() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/learn-more-zip-code?name=test')).getUrl();

        String expectedResult = '/c__HMR_ZipCodeFinder_Page';
        System.assert(actualResult == expectedResult);
    }

    @isTest
    static void testMapUrlNonCCRZ() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/my-account')).getUrl();

        String expectedResult = '/shop-hmr-foods';
        System.assert(actualResult != expectedResult);
    }

    @isTest
    static void testMapUrlCCRZ() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/ccrz__Products')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlVF() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/c__HMR_LoginPage')).getUrl();

        String expectedResult = '/c__HMR_LoginPage';
        System.assert(actualResult == expectedResult);
    }

    @isTest
    static void testMapUrlRecipes() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/recipes/decision-free-diet')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlRecipesOnly() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/recipes')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlRecipesOnlyAgain() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/recipes/')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlRecipeDetails() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/recipes/healthy-solutions-diet/pina-colada-smoothie')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlSuccessStories() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/community-success-stories/nickirae17')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlSuccessStory() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/community-success-stories/')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlForum() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/forum/getting-started/')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlForumNoSlash() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/forum/getting-started')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlForumOnly() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/forum/')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlResources() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/getting-started')).getUrl();
        System.assert(actualResult != null);
    }

    @isTest
    static void testMapUrlResourcesTest() {
        // Implement test code
        HMR_SiteUrlRewriter rewriter = new HMR_SiteUrlRewriter();
        String actualResult = rewriter.mapRequestUrl(new PageReference('/resources/getting-started/start-strong-first-day')).getUrl();
        System.assert(actualResult != null);
    }


}