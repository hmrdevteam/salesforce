/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/09/2018
 * Created By: Zach Engman
 * Last Modified Date: 04/02/2018
 * Last Modified By: Zach Engman
 * Description: Handler for the ActivityLog object triggers
 * ****************************************************/
public without sharing class HMR_ActivityLog_Handler {
	private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;
    public static Boolean isBeforeUpdateFlag = false;

	public HMR_ActivityLog_Handler(boolean executing, integer size) {
		this.isExecuting = executing;
        this.batchSize = size;
	}

	public void onBeforeInsert(List<ActivityLog__c> activityLogList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;
            
            //populate program membership and diet type fields
            populateProgramMembershipData(activityLogList);

            //check for linking to session attendee/group records
            populateSessionAttendeeData(activityLogList);
        }
    }

    public void onBeforeUpdate(List<ActivityLog__c> oldList, List<ActivityLog__c> newList, Map<Id, ActivityLog__c> oldMap, Map<Id, ActivityLog__c> newMap){
        if(!isBeforeUpdateFlag){
            isBeforeUpdateFlag = true;

            //populate program membership and diet type fields
            populateProgramMembershipData(newList);

            //check for linking to session attendee/group records
            populateSessionAttendeeData(newList);
        }
    }

    private void populateProgramMembershipData(List<ActivityLog__c> activityLogList){
        //create set of contact ids
        Set<Id> contactIdSet = new Set<Id>();

        for(ActivityLog__c activityLogRecord : activityLogList){
            contactIdSet.add(activityLogRecord.Contact__c);
        }

        //retrieve the program memberships by contact into a map
        Map<String, List<Program_Membership__c>> contactProgramMembershipMap = new Map<String, List<Program_Membership__c>>();
        for(Program_Membership__c programMembershipRecord : [SELECT DietType__c, hmr_Contact__c, hmr_Enrollment_Date__c 
                                                             FROM Program_Membership__c
                                                             WHERE hmr_Contact__c IN: contactIdSet
                                                             ORDER BY hmr_Contact__c, hmr_Enrollment_Date__c DESC]){
            if(contactProgramMembershipMap.containsKey(programMembershipRecord.hmr_Contact__c)){
                contactProgramMembershipMap.get(programMembershipRecord.hmr_Contact__c).add(programMembershipRecord);
            }
            else{
                contactProgramMembershipMap.put(programMembershipRecord.hmr_Contact__c, new List<Program_Membership__c>{programMembershipRecord});
            }
        }

        //associate the correct program membership and diet type to the activity log
        for(ActivityLog__c activityLogRecord : activityLogList){
            List<Program_Membership__c> programMembershipList = contactProgramMembershipMap.get(activityLogRecord.Contact__c);
            //these are sorted in descending by enrollment date so first one hit should be match
            if(programMembershipList != null){
                for(Program_Membership__c programMembershipRecord : programMembershipList){
                    if(programMembershipRecord.hmr_Enrollment_Date__c <= activityLogRecord.DateDataEnteredFor__c){
                        activityLogRecord.Program_Membership__c = programMembershipRecord.Id;
                        activityLogRecord.Diet_Type__c = programMembershipRecord.DietType__c;
                        break;
                    }
                }
            }
        }
    }

    /*********************************
	Relates the respective session attendee record with the activity log for that date	
    *********************************/
    private void populateSessionAttendeeData(List<ActivityLog__c> activityLogList){
    	//create map of contact to activity log list
    	Map<String, Map<Date, ActivityLog__c>> contactToActivityLogMap = new Map<String, Map<Date, ActivityLog__c>>();
    	Date maxActivityDate = activityLogList[0].DateDataEnteredFor__c.addDays(14);

    	for(ActivityLog__c activityLogRecord : activityLogList){
    		if(contactToActivityLogMap.containsKey(activityLogRecord.Contact__c)){
                contactToActivityLogMap.get(activityLogRecord.Contact__c).put(activityLogRecord.DateDataEnteredFor__c, activityLogRecord);
            }
            else{
                contactToActivityLogMap.put(activityLogRecord.Contact__c, new Map<Date, ActivityLog__c>{activityLogRecord.DateDataEnteredFor__c => activityLogRecord});
            }

            if(maxActivityDate < activityLogRecord.DateDataEnteredFor__c.addDays(14)){
            	maxActivityDate = activityLogRecord.DateDataEnteredFor__c.addDays(14);
            }
    	}

    	//match up the session attendee records to the activity log for that date
    	for(Session_Attendee__c attendeeRecord : [SELECT hmr_Class_Date__c, hmr_Client_Name__c, hmr_Class_Member__c 
    											  FROM Session_Attendee__c 
    											  WHERE hmr_Client_Name__c IN: contactToActivityLogMap.keySet()
                                                  AND hmr_Class_Date__c <=: maxActivityDate]){
    		if(contactToActivityLogMap.containsKey(attendeeRecord.hmr_Client_Name__c)){ //client name field is really the id
    			Map<Date, ActivityLog__c> activityLogMap = contactToActivityLogMap.get(attendeeRecord.hmr_Client_Name__c);
                Date classMinDate = attendeeRecord.hmr_Class_Date__c.addDays(-8);
                Date classMaxDate = attendeeRecord.hmr_Class_Date__c.addDays(-2);

                while(classMinDate <= classMaxDate){
                    if(activityLogMap.containsKey(classMinDate)){
                        ActivityLog__c activityLogRecord = activityLogMap.get(classMinDate);
                        activityLogRecord.Session_Attendee__c = attendeeRecord.Id;
                        activityLogRecord.Group_Member__c = attendeeRecord.hmr_Class_Member__c;
                        break;
                    }

                    classMinDate = classMinDate.addDays(1);
                }
    		}
    	}
    }
}