/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_Questions_List_ContentTemplate
*/

@isTest(SeeAllData=true)  //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_Questions_Home_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_Questions_Home_ContentTemplate questionHomeemplate = new HMR_CMS_Questions_Home_ContentTemplate();
    String propertyValue = questionHomeemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    questionHomeemplate.testAttributes = tab;

    string s2 = questionHomeemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = questionHomeemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');

 //   string topicName = questionHomeemplate.topicName;

	//ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
 //   List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
 //   testItemList.add(new ConnectApi.FeedItem());
 //   testItemList.add(new ConnectApi.FeedItem());
 //   testPage.elements = testItemList;

	//String communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;

 //   HMR_Topic_Service topicService = new HMR_Topic_Service();
 //   HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();
        
 //   //getFirstLevelCategoryLabelList
 //   //We have to do multiple calls as we just want our specific parent topics: can't use ConnectApi.Topics.getTopics(null).topics
 //   for(Category_Map__mdt cat : dataCategoryService.getTopicList()){
 //       //Our DataCategories Match our Topics
 //       ConnectApi.Topic topic = topicService.getTopicByName(cat.Topic_Name__c);
 //       if(topic != null){
 //           ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topic.Id, testPage);     
 //       }
 //   }  

    

    String renderHTML = questionHomeemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    try{
        HMR_CMS_Questions_Home_ContentTemplate qc = new HMR_CMS_Questions_Home_ContentTemplate(null);
    }catch(Exception e){}

  }
  
}