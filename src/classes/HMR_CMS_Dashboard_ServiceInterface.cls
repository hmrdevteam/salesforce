/**
* Service Interface Class to get the Dashboard Details
*
* @Date: 2018-02-12
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-03-12
* @JIRA: 
*/
global with sharing class HMR_CMS_Dashboard_ServiceInterface implements cms.ServiceInterface{
	/**
	  *
	  * @param params a map of parameters including at minimum a value for 'action'
	  * @return a JSON-serialized response string
	 */
    public String executeRequest(Map<String, String> params) {
    	Map<String, Object> returnMap = new Map<String, Object>{'success' => true};
        String action = params.get('action');
        String userId = params.get('userId');

        if(!String.isBlank(action) && !String.isBlank(userId)){
            if(action == 'getWeeklyDashboardData'){
                HMR_Dashboard_Service dashboardService = new HMR_Dashboard_Service(userId);
                List<Contact> contactList = [SELECT MobileLogin__c FROM Contact WHERE Community_User_ID__c =: userId];

                returnMap.put('weeklyDashboardData', dashboardService.getWeeklyDashboardView());
                returnMap.put('hasMobileApp', contactList.size() > 0 ? contactList[0].MobileLogin__c : false);
                returnMap.put('profile', dashboardService.profile);
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('error', 'action and userId required');
        }

        return JSON.serialize(returnMap);
    }

	public static Type getType() {
        return HMR_CMS_Dashboard_ServiceInterface.class;
    }
}