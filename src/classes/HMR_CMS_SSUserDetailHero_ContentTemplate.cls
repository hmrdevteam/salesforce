/**
* Apex Content Template Controller for the Success Stories User Authored Detail Hero Section Component
*
* @Date: 08/03/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 08/03/2017
* @JIRA: HPRP-4188
*/
global virtual class HMR_CMS_SSUserDetailHero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SSUserDetailHero_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SSUserDetailHero_ContentTemplate() {}
	
    global virtual override String getHTML(){
    	String html = '';
  		String articleName = ApexPages.currentPage().getParameters().get('name');
    	
    	if(!String.isBlank(articleName)){
    		HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    		HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);
    		if(articleRecord != null){
	    		html = 	'<div class="user-ss">' + 
	    					'<div class="ss-detail-hero-bg">' +
						        '<div class="container">' +
							          '<div class="row">' +
								            '<div class="col-xs-12 col-sm-6">' +
								              '<div class="min-header">' +
								                'SUCCESS STORIES' +
								              '</div>' +
								              '<div class="header">' +
								                articleRecord.ClientFirstName + ' lost ' + String.valueOf(articleRecord.PoundsLost).replace('.00','') + ' lbs' + 
								              '</div>' +
								            '</div>' +
								            '<div class="col-xs-12 col-sm-6 blocks text-center">' +
								              (!String.isBlank(articleRecord.BeforePhoto) && !String.isBlank(articleRecord.AfterPhoto) ?
									              '<div class="block">' +
									                  '<img class="img-responsive" src="' + articleRecord.BeforePhoto + '" />' +
									                '<div class="name">' +
									                  'BEFORE' +
									                '</div>' +
									              '</div>' +
									              '<div class="block">' +
									                  '<img class="img-responsive" src="' + articleRecord.AfterPhoto + '" />' +
									                '<div class="name">' +
									                  'AFTER' +
									                '</div>' +
									              '</div>' : '') + 
							            	'</div>' +
							          '</div>' +
						        '</div>' +
					      '</div>' +
					    '</div>';
			}
		}

      return html;
    }
}