/**
* Apex Content Template Controller for Registration Page registration section
*
* @Date: 2017-03-18
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual with sharing class HMR_CMS_RegistrationPage_ContentTemplate extends cms.ContentTemplateController {
    //need two constructors, 1 to initialize CreateContentController a
    global HMR_CMS_RegistrationPage_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_RegistrationPage_ContentTemplate() {
        super();
    }

    public String GeneralOrEmployeeRegistration {
        get {
            return getProperty('GeneralOrEmployeeRegistration');
        }
    }
    public String GeneralOrEmployeeRegistrationPicklistOptions {
        get {
            List <PicklistOption> options = new List <PicklistOption>();
            options.add( new PicklistOption('General', 'General'));
            options.add( new PicklistOption('Employee', 'Employee'));
            return (JSON.serialize(options));
        }
    }
    @testVisible
    private class PicklistOption {
        String Label;
        String Value;
        public PicklistOption (String label, String value) {
            this.Label = label;
            this.Value = value;
        }
    }
    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup

        String consTitleTerms;
        String dataIdTerms;
        String descriptionTerms;
        String consTitlePrivacy;
        String dataIdPrivacy;
        String descriptionPrivacy;
        List<ccrz__E_Term__c> consentDataOfSelectedConsent =
            [SELECT Id,ccrz__Description__c,ccrz__Title__c,Consent_Type__c  FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE ];

            if(consentDataOfSelectedConsent.size() > 0){
                for(ccrz__E_Term__c ccrzTerm : consentDataOfSelectedConsent){

                    if(ccrzTerm.Consent_Type__c == 'HMR Terms & Conditions' ){
                        consTitleTerms = ccrzTerm.ccrz__Title__c;
                     //   dataIdTerms = consTitleTerms.replaceAll('[-+.^:,&$\\//\\s]','').toLowerCase();
                        descriptionTerms = ccrzTerm.ccrz__Description__c;
                    }
                    if(ccrzTerm.Consent_Type__c == 'Privacy/Tracking' ){
                        consTitlePrivacy = ccrzTerm.ccrz__Title__c;
                     //   dataIdPrivacy = consTitlePrivacy.replaceAll('[-+.^:,&$\\//\\s]','').toLowerCase();
                        descriptionPrivacy = ccrzTerm.ccrz__Description__c;
                    }

                }
            }

        String html = '';
        html += '<div class="row form-registration">' +
                    '<form id="hmr_registration">'; 

        if(GeneralOrEmployeeRegistration == 'General') {
            html += '<h2>Create Account</h2>';
        }
        else {
            html += '<h2>Welcome!</h2>' +
                    '<div class="col-xs-12 form-tcprivacy">' +
                    '<div>' +                        
                        'Your employer or health care provider has referred you to HMR to support your weight-loss and healthy lifestyle goals. Please complete the information below so we can provide you with custom information regarding your options. By creating an account, you’ll get access to all HMR content and the HMR Community, even if you choose not to join at this time.' +
                        '<br/>'+  
                        '<br/>'+                                      
                    '</div>'; 
            }             

                html += '<div class="col-xs-12 form-error" id="regErrorDiv">'+
                            '<label id="hmr_reg_inputRegistration-error" class="error" style="display: none"></label>'+
                        '</div>' +
                        '<label for="hmr_reg_inputFirstName">First Name</label>' +
                        '<input type="text" id="hmr_reg_inputFirstName" name="r_fn" class="form-control">' +
                        '<label for="hmr_reg_inputLastName">Last Name</label>' +
                        '<input type="text" id="hmr_reg_inputLastName" name="r_ln" class="form-control">' +
                        '<label for="hmr_reg_inputEmail">Email address</label>' +
                        '<input type="email" id="hmr_reg_inputEmail" name="r_email" class="form-control">' +
                        '<label class="displayLabel hidden" for="hmr_reg_inputDisplayName">Display Name' +
                            // '<span class="dname"></span>' +
                        '</label>' +
                        '<input type="text" id="hmr_reg_inputDisplayName" name="r_dn" class="form-control hidden" value="test">' +
                        '<label class="displayLabel" for="hmr_reg_inputPassword">Password' +
                            // '<span></span>' +
                        '</label>' +
                        '<div class="input-group">' +
                            '<input type="password" id="hmr_reg_inputPassword" name="r_password" class="form-control cbox-shadow">' +
                            '<div class="input-group-addon">' +
                                '<div class="eyeclosed"></div>' +
                            '</div>' +
                        '</div>' +
                        '<label for="hmr_reg_inputConfirmPassword">Confirm Password</label>' +
                        '<div class="input-group">' +
                            '<input type="password" id="hmr_reg_inputConfirmPassword" name="r_cpassword" class="form-control cbox-shadow">' +
                            '<div class="input-group-addon">' +
                                '<div class="eyeclosed"></div>' +
                            '</div>' +
                        '</div>';
        if(GeneralOrEmployeeRegistration == 'General') {
            html += '<label for="hmr_reg_inputZipCode">Zip Code</label>' +
                    '<input type="text" id="hmr_reg_inputZipCode" name="r_zipcode" class="form-control">';
        }
        else {
            html += '<label for="hmr_reg_inputEmployerCode">Employer Code</label>' +
                    '<input type="text" id="hmr_reg_inputEmployerCode" name="r_ecode" class="form-control">';
        }
        html += '<div class="col-xs-12 form-mailinglist">' +
                    '<div class="form-checkboxdiv">' +
                        '<input type="checkbox" checked="checked" id="hmr_reg_emailpreference" name="r_emailpreference">' +
                    '</div>' +
                    '<div>' +
                        'Yes, I would like to receive updates, tips, and offers from HMR.' +
                    '</div>' +
                '</div>' +

                '<div class="submit-wrapper">' +
                    '<button class="btn btn-lg btn-primary btn-block" type="submit">Create Account</button>' +
                '</div>' +

                '<div class="col-xs-12 form-tcprivacy">' +
                    '<div>' +
                        'By creating an account I acknowledge that use of this website is subject to HMR&apos;s <a href="#" id="t" class="text-14b">' + consTitleTerms  + '</a> and <a href="#" id="p" class="text-14b">' + consTitlePrivacy + '</a>.' +
                        '<br/>'+
                        '<br/>';

        if(GeneralOrEmployeeRegistration == 'General') {
            html += 'If you need any customer support, please call us at 800-418-1367 Monday-Friday, 9am-6pm EST'+
                    '</div>';
        }
        else {
            html += 'If you need any customer support, please call us at 877-501-9257 Monday-Friday, 9am-6pm EST'+
                    '</div>';
        }     

            html +=  '<div id="hmr_termspopup" class="hmr-modal modal fade" role="dialog">' +
                        '<div class="modal-dialog">' +
                             '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                     '</button>' +
                                    '<h4 class="modal-title text-center">'+ consTitleTerms +'</h4>' + //'+ title +' +
                                '</div>' +
                                 '<div class="modal-body">' +
                                    '<p>' + descriptionTerms +
                                   '</p>' +
                               '</div>' +
                               '<div class="modal-footer">' +
                               '</div>' +
                           '</div>' +
                        '</div>' +
                    '</div>' +


                    '<div id="privacy_trackingpopup" class="hmr-modal modal fade" role="dialog">' +
                        '<div class="modal-dialog">' +
                             '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                     '</button>' +
                                    '<h4 class="modal-title text-center">'+ consTitlePrivacy +'</h4>' + //'+ title +' +
                                '</div>' +
                                 '<div class="modal-body">' +
                                    '<p>' + descriptionPrivacy +
                                   '</p>' +
                               '</div>' +
                               '<div class="modal-footer">' +
                               '</div>' +
                           '</div>' +
                        '</div>' +
                    '</div>' +

                '</div>' +

            '</form>' +
        '</div>';
        return html;
    }
}