/**
* Apex Recipe Home Service Interface for Recipe Home Page
*
* @Date: 2017-03-26
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-06-19
* @JIRA: 
*/
global with sharing class HMR_CMS_RecipeHomeServiceInterface implements cms.ServiceInterface{
/**
*
* @param params a map of parameters including at minimum a value for 'action'
* @return a JSON-serialized response string
*/
    public String executeRequest(Map<String, String> params) {
        Map<String, Object> returnMap = new Map<string, Object>();
        try {
            String action = params.get('action');
            if(action == 'getRecipeCards') {
                returnMap = getRecipeCards(params);
            }else{
                //Invalid Action
                returnMap.put('setdata','');
                returnMap.put('success',false);
                returnMap.put('message','Invalid Action');
            } 
        } catch(Exception e) {
            // Unexpected Error
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMap.put('setdata','');
            returnMap.put('success',false);
            returnMap.put('message',JSON.serialize(message));
        }
        
        // No actions matched and no error occurred
        return JSON.serialize(returnMap);
    }
    
    public Map<String, Object> getRecipeCards(Map<String, String> params){
        Map<String, Object> returnMap = new Map<string, Object>();
        String returnJSON;
        List<Recipe__kav> recipeList;
        String dietType = params.get('recipeType');
        try{
            if(String.isBlank(dietType)){
                recipeList = [SELECT Image_URI__c, 
                                     Template_Layout__c, 
                                     Title, 
                                     Recipe__r.Total_Hours__c,
                                     Recipe__r.Total_Minutes__c,
                                     Recipe__r.VF_Servings__c,
                                     Recipe__r.Contact__r.Name,
                                     Recipe__r.Contact__r.Title,
                                     Recipe__r.hmr_Recipe_Category__c,
                                     UrlName
                              FROM Recipe__kav 
                              WHERE PublishStatus = 'Online' AND Language = 'en_US'
                              ORDER BY Title];
           }
           else{
               recipeList = [SELECT Image_URI__c, 
                                     Template_Layout__c, 
                                     Title, 
                                     Recipe__r.Total_Hours__c,
                                     Recipe__r.Total_Minutes__c,
                                     Recipe__r.VF_Servings__c,
                                     Recipe__r.Contact__r.Name,
                                     Recipe__r.Contact__r.Title,
                                     Recipe__r.hmr_Recipe_Category__c,
                                     UrlName
                              FROM Recipe__kav 
                              WHERE PublishStatus = 'Online' AND Language = 'en_US' AND Recipe__r.Diet_Type__c INCLUDES (:dietType)
                              ORDER BY Title];
           }
            
            if(recipeList.isEmpty()){
                returnMap.put('setdata','');
                returnMap.put('success',true);
                returnMap.put('message','set is empty');
            }
            else{
                returnMap.put('setdata',recipeList);
                returnMap.put('sethtml', getCardListHTML(recipeList));
                returnMap.put('success',true);
                returnMap.put('message','set data');   
            }
        }
        catch(Exception e){
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMap.put('setdata','');
            returnMap.put('success',false);
            returnMap.put('message',JSON.serialize(message));
        }
        
        return returnMap;
    }
    
    public static Type getType() {
        return HMR_CMS_RecipeHomeServiceInterface.class;
    }
    @TestVisible
    private static String getCardListHTML(List<Recipe__kav> recipeList){
        String html = '';
        //Retrieve Necessary Static Resource URLS
        Map<String, String> resourceMap = GetResourceURLMap(new Set<String> {'ClockImage', 'ProfileImage', 'ServingImage'});
        //Divide the list into the respective Template Layout Types
        List<Recipe__kav> leftRightRecipeList = new List<Recipe__kav>();
        List<Recipe__kav> topBottomRecipeList = new List<Recipe__kav>();
        
        for(Recipe__kav recipe : recipeList){
            if(recipe.Template_Layout__c == 'LeftRight')
                leftRightRecipeList.add(recipe);
            else
                topBottomRecipeList.add(recipe);
        }
        
        //Generate the HTML - first where we have matching numbers of left right to top bottom
        integer matchingIndex = 0;
        while(matchingIndex < leftRightRecipeList.size() && matchingIndex < topBottomRecipeList.size()){
            if(Math.mod(matchingIndex, 2) == 0){
                html += getItemHTML('item double', leftRightRecipeList[matchingIndex], resourceMap);
                html += getItemHTML('item', topBottomRecipeList[matchingIndex], resourceMap);
            }
            else{
                html += getItemHTML('item', topBottomRecipeList[matchingIndex], resourceMap);
                html += getItemHTML('item double', leftRightRecipeList[matchingIndex], resourceMap);
            }
            matchingIndex ++;
        }
 
        //Render any unmatched records (non equal amount of left right and top bottom)
        for(integer i = matchingIndex; i < leftRightRecipeList.size(); i++)
            html += getItemHTML('item double', leftRightRecipeList[i], resourceMap);
        
        for(integer j = matchingIndex; j < topBottomRecipeList.size(); j++)
            html += getItemHTML('item', topBottomRecipeList[j], resourceMap);

        return html;
    }
    
    private static String getItemHTML(String itemClass, Recipe__kav recipe, Map<String, String> resourceMap){
        String html = '';        
        String sitePrefix = Site.getBaseUrl();
        html += '<div class="' + itemClass + '" onclick="recipeCards.navigateToRecipeDetails(\'' + recipe.UrlName + '\')" data-category="'+recipe.Recipe__r.hmr_Recipe_Category__c+'">';
            html += '<div class="image">';
                html += String.format('<img src="{0}"/>', new String[] {recipe.Image_URI__c});                
            html += '</div>';
            html += '<div class="content">';
                html += '<h5 class="recipe">Recipes</h5>';
                    html += String.format('<h5 class="recipe-name">{0}</h5>', new String[] {recipe.Title});
                html += '<div class="clearfix">';
                    html += '<div class="icon-with-text">';
                        html += String.format('<img src="{0}{1}"  class="detail-icon"/>', new String[] {sitePrefix, resourceMap.get('ClockImage')});
                        html += String.format('<span> < {0} MIN</span>', new String[] {String.valueOf(getTotalMinutes(recipe))});
                    html += '</div>';
                    /*html += '<div class="icon-with-text border-left">';
                        html += String.format('<img src="{0}{1}"  class="detail-icon"/>', new String[] {sitePrefix, resourceMap.get('ServingImage')});
                        html += String.format('<span>{0}</span>', new String[] {String.valueOf(recipe.Recipe__r.VF_Servings__c)});
                    html += '</div>';*/
                html += '</div>';
                html += '<div class="vcenter profile ">';  
                        html += String.format('<img src="{0}{1}"/>', new String[] {sitePrefix, resourceMap.get('ProfileImage')});
                        html += '<div class="info vcenter">';
                            html += '<div class="name vcenter">';
                                  html += 'HMR Team';
                            html += '</div>';
                            html += '<div class="title vcenter">';
                                  html+= 'Boston, MA';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
            html += '</div>';
        html += '</div>';
        return html;
    }

    private static integer getTotalMinutes(Recipe__kav recipe){
        return Integer.valueOf(recipe.Recipe__r.Total_Minutes__c);
    }
    
    
    private static Map<String, String> GetResourceURLMap(Set<String> resourceNameSet) {
        Map<String, String> resourceMap = new Map<String, String>();
        List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp 
                                             FROM StaticResource 
                                             WHERE Name IN:resourceNameSet];
        for(StaticResource resource : resourceList){
           String namespace = resource.NamespacePrefix;
           resourceMap.put(resource.Name, '/resource/' 
                                          + resource.SystemModStamp.getTime() + '/' 
                                          + (namespace != null && namespace != '' ? namespace + '__' : '') 
                                          + resource.Name); 
        }
        
        return resourceMap;
    }
}