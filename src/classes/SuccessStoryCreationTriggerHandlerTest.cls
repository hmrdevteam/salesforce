@isTest
private class SuccessStoryCreationTriggerHandlerTest{

    static testMethod void firstTestMethod(){
    
        Profile prof = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User']; 
        
        Account communityAc = new Account(name ='testComunity') ;
        insert communityAc; 
        
        Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
        insert con;  
        
        List<hmr_Success_Story__c> approvedStories = new List<hmr_Success_Story__c>();
        
        Map<Id, hmr_Success_Story__c> oldStoriesMap = new Map<Id, hmr_Success_Story__c>();
       
        hmr_Success_Story__c succesStryObjOld = new hmr_Success_Story__c(Submitted_By__c = con.id, Title__c = 'Test 2 Title',
                                                Long_Teaser__c = 'Long Test 2 Teaser',Story_Body__c = 'Test Story Body 2',
                                                Approval_Status__c = 'Submitted', Favorite_HMR_Products__c = 'Foods');
        insert succesStryObjOld;
       
        hmr_Success_Story__c succesStryObj = new hmr_Success_Story__c(Submitted_By__c = con.id, Title__c = 'Test Title',
                                                Long_Teaser__c = 'Long Test Teaser',Story_Body__c = 'Test Story Body',
                                                Approval_Status__c = 'Approved', Favorite_HMR_Products__c = 'Foods');
        insert succesStryObj;
        oldStoriesMap.put(succesStryObjOld.id, succesStryObjOld);
        approvedStories.add(succesStryObj);
        SuccessStoryCreationTriggerHandler.handleAfterUpdate(approvedStories,oldStoriesMap);
    }      
       
   static testMethod void secondTestMethod(){
       
        Profile prof = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User']; 
        
        Account communityAc = new Account(name ='testComunity') ;
        insert communityAc; 
        
        Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
        insert con;  
        
        List<hmr_Success_Story__c> approvedStories = new List<hmr_Success_Story__c>();
        
        Map<Id, hmr_Success_Story__c> oldStoriesMap = new Map<Id, hmr_Success_Story__c>();
       
        hmr_Success_Story__c succesStryObjOld = new hmr_Success_Story__c(Submitted_By__c = con.id, Title__c = 'Test 2 Title',
                                                Long_Teaser__c = 'Long Test 2 Teaser',Story_Body__c = 'Test Story Body 2',
                                                Approval_Status__c = 'Approved', Favorite_HMR_Products__c = 'Foods');
        insert succesStryObjOld;
       
       
        oldStoriesMap.put(succesStryObjOld.id, succesStryObjOld);
        
         Success_Story__kav stryKV = new Success_Story__kav();
         stryKV.hmr_Success_Story__c = succesStryObjOld.id;
         stryKV.Language = 'en_US';
         stryKV.Title = 'Test Title';
         stryKV.URLName = 'Success-story';
       // stryKV.KnowledgeArticleId = 'testID';
        insert stryKV;
      
        stryKV = [SELECT KnowledgeArticleId FROM Success_Story__kav WHERE Id = :stryKV.Id];    
        
         try {
            KbManagement.PublishingService.publishArticle(stryKV.KnowledgeArticleId, true);
         }
         catch(exception e) {
         
             System.debug('Exception ->  ' + e.getMessage());
            
        }
        
    //     System.debug('@@@@@@@@@@@@'+stryKV);
    
        hmr_Success_Story__c succesStryObj = [SELECT Id, Approval_Status__c FROM hmr_Success_Story__c LIMIT 1];
        
        succesStryObj.Approval_Status__c = 'Approved';
        succesStryObj.Story_Body__c = 'Updated Body';
        update succesStryObj;
        
        // try {
        
        //     KbManagement.PublishingService.editOnlineArticle(stryKV.KnowledgeArticleId, true);
            
        // } catch(exception e) {
                        
        // }
        approvedStories.add(succesStryObj);
        SuccessStoryCreationTriggerHandler.handleAfterUpdate(approvedStories,oldStoriesMap);
    }

}