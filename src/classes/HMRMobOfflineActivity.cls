/**
* Apex Rest Resource for Offline Activity Sync
* @Date: 2018-03-19
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-03-19 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/offline/activity/*')  
global with sharing class HMRMobOfflineActivity {

	/**
     * reconciles the offline data to be inserted/updated
     * @param List<MobOfflineActivityDTO>
     * @see MobOfflineActivityDTO
     * @return MobOfflineActivityDTO - the DTO containing the diet activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 500 Internal Server Error
     */
    @HttpPost
    global static void doPost(List<MobOfflineActivityDTO> offlineActivityList) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        List<HMRMobDietActivity.MobDietActivityDTO> mismatchedDietActivityList = new List<HMRMobDietActivity.MobDietActivityDTO>();
    	List<HMRMobWeightActivity.MobWeightActivityDTO> mismatchedWeightActivityList = new List<HMRMobWeightActivity.MobWeightActivityDTO>();
        List<HMRMobPhysicalActivity.MobPhysicalActivityDTO> mismatchedPhysicalActivityList = new List<HMRMobPhysicalActivity.MobPhysicalActivityDTO>();
        String communityUserId = RestContext.request.params.get('communityUserId');

        if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
        }

        //Get the date range of the activity being synched
        Date startDate = Date.today();
        Date endDate = Date.today();

        for(MobOfflineActivityDTO offlineActivity : offlineActivityList){
        	if(offlineActivity.activityDate != null){
        		if(offlineActivity.activityDate < startDate){
        			startDate = offlineActivity.activityDate;
        		}

        		if(offlineActivity.activityDate > endDate){
        			endDate = offlineActivity.activityDate;
        		}
        	}
        	else{
        		resultMap.put('success', false);
        		resultMap.put('error', 'Activity found without required activity date');
        	}
        }
        
        Contact contactRecord = [SELECT Id FROM Contact WHERE Community_User_ID__c =: communityUserId];
        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(contactRecord.Id, endDate);

        //verify a program membership record existed for the date
        if(programMembershipRecord != null){
	        //Get the user's current activity and physical activity for the range
	        Map<Date, ActivityLog__c> existingDateToActivityLogMap = new Map<Date, ActivityLog__c>();
	        Map<Id, PhysicalActivity__c> existingPhysicalActivityMap = new Map<Id, PhysicalActivity__c>();

	        for(ActivityLog__c existingRecord : [SELECT Contact__c
									               ,Program_Membership__c
									               ,Program_Membership__r.Program__r.Name
									               ,DateDataEnteredFor__c
									               ,CurrentWeight__c
									               ,Diet_Type__c
									               ,Entrees__c
									               ,ShakesCereals__c
									               ,InTheBox__c
									               ,FruitsVegetables__c
									               ,Water__c
									               ,BenefitBars__c 
									               ,Diet_Version__c
									               ,Weight_Version__c
									               ,TotalPhysicalActivityCaloriesRollUp__c
									               ,(SELECT Intensity_Level__c
									                       ,Minutes__c
									                       ,NameofPhysicalActivity__c
									                       ,TotalPhysicalActivityCalories__c
									                       ,Physical_Activity_Version__c
									                    FROM Physical_Activities__r)
									        	FROM ActivityLog__c
									        	WHERE Contact__r.Community_User_ID__c =: communityUserId
									        		AND DateDataEnteredFor__c >=: startDate 
									        		AND DateDataEnteredFor__c <=: endDate]){
	        	existingDateToActivityLogMap.put(existingRecord.DateDataEnteredFor__c, existingRecord);
	        	//add the physical activity records to our existing record map
	        	if(existingRecord.Physical_Activities__r != null && existingRecord.Physical_Activities__r.size() > 0){
	        		for(PhysicalActivity__c physicalActivityRecord : existingRecord.Physical_Activities__r){
	        			existingPhysicalActivityMap.put(physicalActivityRecord.Id, physicalActivityRecord);
	        		}
	        	}
	        }

	        try{
	        	//Start by synching the activity logs
	            List<ActivityLog__c> upsertActivityLogList = new List<ActivityLog__c>();

	            for(MobOfflineActivityDTO offlineActivity : offlineActivityList){
	            	//if record didn't exist previously, it is easy insert
	            	if(!existingDateToActivityLogMap.containsKey(offlineActivity.activityDate)){
	            		ActivityLog__c activityLogRecord = offlineActivity.mapToActivityLog();
	            		activityLogRecord.Program_Membership__c = programMembershipRecord.Id;

	            		upsertActivityLogList.add(activityLogRecord);
	            	}
	            	else{
	            		ActivityLog__c existingRecord = existingDateToActivityLogMap.get(offlineActivity.activityDate);
	            		Boolean shouldUpsert = false;
	            		//happy path of last sync wins
	            		if(existingRecord.Diet_Version__c == null || existingRecord.Diet_Version__c == 0 || existingRecord.Diet_Version__c == offlineActivity.dietRecordVersion){
	            			existingRecord = offlineActivity.overwriteExistingLog(existingRecord);
	            			shouldUpsert = true;
	            		}
	            		else{
	            			if(existingRecord.Entrees__c != null || existingRecord.ShakesCereals__c != null || existingRecord.BenefitBars__c != null || existingRecord.FruitsVegetables__c != null || existingRecord.Water__c != null || (existingRecord.InTheBox__c != null && existingRecord.InTheBox__c)){
	            				mismatchedDietActivityList.add(new HMRMobDietActivity.MobDietActivityDTO(existingRecord));
	            			}
	            		}

	            		if(existingRecord.Weight_Version__c == null || existingRecord.Weight_Version__c == 0 || existingRecord.Weight_Version__c == offlineActivity.weightRecordVersion){
	            			existingRecord = offlineActivity.overwriteExistingWeightLog(existingRecord);
	            			shouldUpsert = true;
	            		}
	            		else{
	            			if(existingRecord.CurrentWeight__c != null){
	            				mismatchedWeightActivityList.add(new HMRMobWeightActivity.MobWeightActivityDTO(existingRecord));
	            			}
	            		}

	            		if(shouldUpsert){
	            			upsertActivityLogList.add(existingRecord);
	            		}
	            		
	            	}
	            }

	            if(upsertActivityLogList.size() > 0){
	            	upsert upsertActivityLogList;
	            }

	            //add any newly upserted records to the map for linking to physical activity
	            for(ActivityLog__c activityLogRecord : upsertActivityLogList){
	            	if(!existingDateToActivityLogMap.containsKey(activityLogRecord.DateDataEnteredFor__c)){
	            		existingDateToActivityLogMap.put(activityLogRecord.DateDataEnteredFor__c, activityLogRecord);
	            	}
	            }

	            //Synch physical activity records
	            List<PhysicalActivity__c> upsertPhysicalActivityList = new List<PhysicalActivity__c>();

	            //Synch the Physical Activity (we need the ids from any new activityLog records)
	            for(MobOfflineActivityDTO offlineActivity : offlineActivityList){
	            	if(offlineActivity.physicalActivityList != null && offlineActivity.physicalActivityList.size() > 0){
	            		for(HMRMobPhysicalActivity.MobPhysicalActivityDTO offlinePhysicalActivity : offlineActivity.physicalActivityList){
	            			//if record didn't exist previously, it is easy insert
	            			if(String.isBlank(offlinePhysicalActivity.recordId) || !existingPhysicalActivityMap.containsKey(offlinePhysicalActivity.recordId)){
	            				if(existingDateToActivityLogMap.containsKey(offlinePhysicalActivity.activityDate)){
	            					PhysicalActivity__c physicalActivityRecord = offlinePhysicalActivity.mapToPhysicalActivityLog();
	            					physicalActivityRecord.Activity_Log__c = existingDateToActivityLogMap.get(offlinePhysicalActivity.activityDate).Id;

	            					upsertPhysicalActivityList.add(physicalActivityRecord);
	            				}
	            			}
	            			else{
	            				PhysicalActivity__c existingRecord = existingPhysicalActivityMap.get(offlinePhysicalActivity.recordId);
	            				//happy path of last sync wins
	            				if(existingRecord.Physical_Activity_Version__c == null || existingRecord.Physical_Activity_Version__c == 0 || existingRecord.Physical_Activity_Version__c == offlinePhysicalActivity.recordVersion){
	            					existingRecord = offlineActivity.overwriteExistingPhysicalActivity(offlinePhysicalActivity, existingRecord);
	            					upsertPhysicalActivityList.add(existingRecord);
	            				}
	            				else{
	            					mismatchedPhysicalActivityList.add(new HMRMobPhysicalActivity.MobPhysicalActivityDTO(existingRecord));
	            				}
	            			}
	            		}
	            	}
	            }

	            if(upsertPhysicalActivityList.size() > 0){
	            	upsert upsertPhysicalActivityList;
	            }

	            response.statusCode = 201;

	            resultMap.put('success', true);
	            resultMap.put('versionConflictDietList', mismatchedDietActivityList);
	            resultMap.put('versionConflictWeightList', mismatchedWeightActivityList);
	            resultMap.put('versionConflictPhysicalActivityList', mismatchedPhysicalActivityList);
	        }
	        catch(Exception ex){
	            response.statusCode = 500;

	            resultMap.put('success', false);
	            resultMap.put('error', ex.getMessage());
        	}
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }

	/**
     * global DTO class that represents a the structure of offline activity data to sync
     */
    global class MobOfflineActivityDTO extends HMRActivityDTO{
        public Integer entrees {get;set;}
        public Integer shakesCereals {get;set;}
        public Integer fruitsVegetable {get;set;}
        public Integer water {get;set;}
        public Integer benefitBars {get;set;}
        public Boolean stayedOnDiet {get;set;}
        public Decimal currentWeight {get;set;}
        public integer dietRecordVersion {get;set;}
        public integer weightRecordVersion {get;set;}

        List<HMRMobPhysicalActivity.MobPhysicalActivityDTO> physicalActivityList {get; set;}

        public ActivityLog__c mapToActivityLog() {
            if (this.stayedOnDiet == null) {this.stayedOnDiet=false;}

            ActivityLog__c activityLogRecord = new ActivityLog__c(
                Id = this.recordId
               ,Contact__c = this.contactId
               ,Program_Membership__c = this.programMembershipId
               ,DateDataEnteredFor__c = this.activityDate
               ,Entrees__c = this.entrees
               ,ShakesCereals__c = this.shakesCereals
               ,FruitsVegetables__c = this.fruitsVegetable 
               ,Water__c = this.water
               ,BenefitBars__c = this.benefitBars
               ,InTheBox__c = this.stayedOnDiet
               ,CurrentWeight__c = this.currentWeight
            );
            
            return activityLogRecord;
        }

        public ActivityLog__c overwriteExistingLog(ActivityLog__c activityLogRecord){
        	if (this.stayedOnDiet == null) {this.stayedOnDiet=false;}

        	activityLogRecord.Entrees__c = this.entrees;
        	activityLogRecord.ShakesCereals__c = this.shakesCereals;
        	activityLogRecord.FruitsVegetables__c = this.fruitsVegetable;
        	activityLogRecord.Water__c = this.water;
        	activityLogRecord.BenefitBars__c = this.benefitBars;
        	activityLogRecord.InTheBox__c = this.stayedOnDiet;
        	activityLogRecord.Diet_Version__c = activityLogRecord.Diet_Version__c == null ? 1 : activityLogRecord.Diet_Version__c + 1;

        	return activityLogRecord;
        }

        public ActivityLog__c overwriteExistingWeightLog(ActivityLog__c activityLogRecord){
        	activityLogRecord.CurrentWeight__c = this.currentWeight;
        	activityLogRecord.Weight_Version__c = activityLogRecord.Weight_Version__c == null ? 1 : activityLogRecord.Weight_Version__c + 1;

        	return activityLogRecord;
        }

        public PhysicalActivity__c overwriteExistingPhysicalActivity(HMRMobPhysicalActivity.MobPhysicalActivityDTO physicalActivityDTO, PhysicalActivity__c physicalActivityRecord){
        	physicalActivityRecord.Intensity_Level__c = physicalActivityDTO.intensityLevel;
            physicalActivityRecord.Minutes__c = physicalActivityDTO.minutes;
            physicalActivityRecord.NameofPhysicalActivity__c = physicalActivityDTO.activity;
            physicalActivityRecord.TotalPhysicalActivityCalories__c = physicalActivityDTO.totalPhysicalActivityCalories;

        	return physicalActivityRecord;
        }
    }
}