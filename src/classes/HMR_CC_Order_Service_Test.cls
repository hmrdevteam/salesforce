/**
* Test Class Coverage of the HMR_CC_Order_Service Class
*
* @Date: 04/25/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/
@isTest
private class HMR_CC_Order_Service_Test{
    @testSetup
    private static void setupOrderData(){
        List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
    }

    @isTest
    private static void testUpdateOrderRecordNoDiscount(){
        List<ccrz__E_Order__c> orderList = [SELECT Id FROM ccrz__E_Order__c];        

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //Verify no discount was applied
        orderList = [SELECT ccrz__TotalDiscount__c FROM ccrz__E_Order__c];
        System.assertEquals(10.00, orderList[0].ccrz__TotalDiscount__c);
    }

    @isTest
    private static void testUpdateOrderRecordWithDiscount(){
        List<ccrz__E_Order__c> orderList = [SELECT Id FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c FROM ccrz__E_OrderItem__c];

        //Update an order item above discount minimum to be sure we hit above it
        orderItemList[0].ccrz__Price__c = 300.00;
        orderItemList[0].ccrz__Quantity__c = 2;
        update orderItemList[0];

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //Verify the discount was applied
        orderList = [SELECT ccrz__TotalDiscount__c FROM ccrz__E_Order__c];
        System.assert(orderList[0].ccrz__TotalDiscount__c > 0);
    }

    @isTest
    private static void testConstructorInstantiation(){
        List<ccrz__E_Order__c> orderList = [SELECT Id FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c FROM ccrz__E_OrderItem__c];

        Test.startTest();

        HMR_CC_Order_Service constructorOrderId = new HMR_CC_Order_Service(orderList[0].Id);
        HMR_CC_Order_Service constructorOrderIdItemList = new HMR_CC_Order_Service(orderList[0].Id, orderItemList);
        HMR_CC_Order_Service constructorOrderItemList = new HMR_CC_Order_Service(orderList[0], orderItemList);

        Test.stopTest();

        //Verify property assignments made
        System.assert(constructorOrderId.orderRecord != null);
        System.assert(constructorOrderId.orderItemsList != null);
        System.assert(constructorOrderIdItemList.orderRecord != null);
        System.assert(constructorOrderIdItemList.orderItemsList != null);
        System.assert(constructorOrderItemList.orderRecord != null);
        System.assert(constructorOrderItemList.orderItemsList != null);
    }

    @isTest
    private static void testProcessBusinessRulesWithInvalidOrder(){
        List<ccrz__E_Order__c> orderList = [SELECT Id FROM ccrz__E_Order__c];
        delete orderList;

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        System.assert(orderService.orderRecord == null);
    }

    @isTest
    private static void testProcessBusinessRulesWithExistingDiscount(){
        List<ccrz__E_Order__c> orderList = [SELECT Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c FROM ccrz__E_OrderItem__c];
        orderList[0].Order_Value_Discount_Amount__c = 20.0;
        orderList[0].ccrz__TotalDiscount__c = 100.0;
        orderList[0].ccrz__ShipMethod__c = 'Expedited';
        update orderList[0];

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c > 0);
    }

    @isTest
    private static void testProcessBusinessRulesWithHMREmployeeAndInvalidOrderItemAbsoluteDiscount(){
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Account__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c, hmr_Order_Type_c__c FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c, ccrz__AbsoluteDiscount__c FROM ccrz__E_OrderItem__c];
        Account accountRecord = orderList[0].ccrz__Account__r;
        accountRecord.Name = 'HMR Employees';
        update accountRecord;

        orderList[0].hmr_Order_Type_c__c = 'P2 1st Order';
        orderList[0].Order_Value_Discount_Amount__c = null;
        update orderList;

        for(ccrz__E_OrderItem__c orderItem : orderItemList)
            orderItem.ccrz__AbsoluteDiscount__c = 1.00;

        update orderItemList;

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c == 0);
    }

    @isTest
    private static void testProcessBusinessRulesWithPercentageReorderCoupon(){
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Account__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c, hmr_Order_Type_c__c FROM ccrz__E_Order__c];
        ccrz__E_Coupon__c reorderCoupon =  new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'TestCoupon',
                                                                 ccrz__CouponName__c = 'Test Coupon',
                                                                 ccrz__TotalUsed__c = 0,
                                                                 ccrz__StartDate__c = Date.today(),
                                                                 ccrz__EndDate__c = Date.today(),
                                                                 ccrz__MaxUse__c = 1,
                                                                 ccrz__CouponType__c = 'Percentage',
                                                                 ccrz__DiscountType__c = 'Percentage',
                                                                 ccrz__DiscountAmount__c = 100.0,
                                                                 ccrz__CartTotalAmount__c = 1.00,
                                                                 ccrz__Enabled__c = true);




        insert reorderCoupon;

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id, reorderCoupon);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c > 0);
    }

    @isTest
    private static void testProcessBusinessRulesWithAbsoluteReorderCoupon(){
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Account__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c, hmr_Order_Type_c__c FROM ccrz__E_Order__c];
        ccrz__E_Coupon__c reorderCoupon =  new ccrz__E_Coupon__c(ccrz__CouponCode__c = 'TestCoupon',
                                                                 ccrz__CouponName__c = 'Test Coupon',
                                                                 ccrz__TotalUsed__c = 0,
                                                                 ccrz__StartDate__c = Date.today(),
                                                                 ccrz__EndDate__c = Date.today(),
                                                                 ccrz__MaxUse__c = 1,
                                                                 ccrz__CouponType__c = 'Absolute',
                                                                 ccrz__DiscountType__c = 'Absolute',
                                                                 ccrz__DiscountAmount__c = 100.0,
                                                                 ccrz__CartTotalAmount__c = 1.00,
                                                                 ccrz__Enabled__c = true);




        insert reorderCoupon;

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id, reorderCoupon);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c > 0);
    }
    @isTest
    private static void testProcessBusinessRulesWithExpeditedShipping(){
        List<ccrz__E_Order__c> orderList = [SELECT Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c FROM ccrz__E_OrderItem__c];

        //Update an order item above discount minimum to be sure we hit above it
        orderItemList[0].ccrz__Price__c = 300.00;
        orderItemList[0].ccrz__Quantity__c = 2;
        update orderItemList[0];
        
        orderList[0].ccrz__ShipMethod__c = 'Expedited';
        update orderList[0];

        Test.startTest();

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c > 0);
    }

    @isTest
    private static void testProcessBusinessRulesWithP2FirstOrder(){
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Account__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c, hmr_Order_Type_c__c FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c, ccrz__AbsoluteDiscount__c FROM ccrz__E_OrderItem__c];
        
        orderList[0].hmr_Order_Type_c__c = 'P2 1st Order';
        update orderList;

        orderItemList[0].ccrz__Price__c = 301.65;
        orderItemList[0].ccrz__Quantity__c = 1;
        orderItemList[0].ccrz__ProductType__c = 'Dynamic Kit';

        orderItemList.add(new ccrz__E_OrderItem__c(
                            ccrz__Order__c           = orderList[0].Id,
                            ccrz__Price__c           = 50,
                            ccrz__Quantity__c        = 2,
                            ccrz__SubAmount__c       = 100,
                            ccrz__ProductType__c = 'Product'
                    ));

        upsert orderItemList;

        Test.startTest();

        system.debug('@@ orderItemList ' + orderItemList);

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c == 0);
    }

    @isTest
    private static void testProcessBusinessRulesWithP2FirstOrderTwo(){
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Account__c, ccrz__Account__r.Name, Order_Value_Discount_Amount__c, ccrz__ShipMethod__c, ccrz__TotalDiscount__c, hmr_Order_Type_c__c FROM ccrz__E_Order__c];
        List<ccrz__E_OrderItem__c> orderItemList = [SELECT ccrz__Price__c, ccrz__Quantity__c, ccrz__AbsoluteDiscount__c FROM ccrz__E_OrderItem__c];
        
        orderList[0].hmr_Order_Type_c__c = 'P2 1st Order';
        update orderList;

        orderItemList[0].ccrz__Price__c = 301.65;
        orderItemList[0].ccrz__Quantity__c = 1;
        orderItemList[0].ccrz__ProductType__c = 'Dynamic Kit';

        orderItemList.add(new ccrz__E_OrderItem__c(
                            ccrz__Order__c           = orderList[0].Id,
                            ccrz__Price__c           = 50,
                            ccrz__Quantity__c        = 2,
                            ccrz__SubAmount__c       = 100,
                            ccrz__ProductType__c = 'Product'
                    ));

        upsert orderItemList;

        ccrz__E_Product__c p2CouponProduct = new ccrz__E_Product__c(
                                                Name = 'ProgramDiscount_P2', 
                                                ccrz__SKU__c = 'ProgramDiscount_P2',        
                                                ccrz__ProductType__c = 'Coupon'
                                                );

        insert p2CouponProduct;

        Test.startTest();

        system.debug('@@ orderItemList ' + orderItemList);

        HMR_CC_Order_Service orderService = new HMR_CC_Order_Service(orderList[0].Id);
        orderService.processBusinessRules();

        Test.stopTest();

        //System.assert(orderService.orderRecord.ccrz__TotalDiscount__c == 0);
    }    
}