/**
* Apex Controller calls OCMS Service Interface API to get content from header and Footer inside OCMS Master Page
*
* @Date: 2017-03-18
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
public with sharing class HMR_CMS_CC_BrandingController {
    public String headerRendering;
    public String footerRendering;
    public String htmlString;
    public String masterPage {get;set;}

    public HMR_CMS_CC_BrandingController() {
        masterPage = 'New HMR Master';

        try {
            Map<String,String> context_params = new Map<String,String>();
            context_params.put('page_mode', 'production');

            cms.ApexServiceInterface ora = (cms.ApexServiceInterface)cms.ServiceEndpoint.getService('OrchestraRenderingAPI',context_params);

            Map<String,Object> arguments = new Map<String,Object>();
            arguments.put('pageName', masterPage);

            cms.OrchestraPageResult result = (cms.OrchestraPageResult)ora.executeApex('getRenderedPage',arguments);

            for(cms.OrchestraRenderedPage renderedPage: result.getRenderedPages()){
                //System.debug(renderedPage);
                if(renderedPage.getName() == 'New HMR Master')
                   htmlString = renderedPage.getRendering();
            }
        }
        catch(Exception ex) {
            System.debug('HMR_CMS_CC_BrandingController Exception ' + ex.getMessage());
        }
    }

    public String getHeaderRendering() {
        return parseHeader(htmlString);
    }

    public String getFooterRendering() {
        return parseFooter(htmlString);
    }

    public String parseHeader(String input){
        Integer headerStart, headerEnd;
        headerStart = input.indexOf('<nav class="navbar navbar-default navbar-fixed-top hmr-header">');
        headerEnd = input.indexOf('</nav>') + 6;
        if (headerStart == -1 || headerEnd == -1){
            return '';
        }
        return input.substring(headerStart, headerEnd);
    }

    public String parseFooter(String input){
        Integer footerStart, footerEnd;
        footerStart = input.indexOf('<footer class="bg-typecover">');
        footerEnd = input.indexOf('</footer>',footerStart) + 9;
        if (footerStart == -1 || footerEnd == -1){
            return '';
        }
        return input.substring(footerStart, footerEnd);
    }
}