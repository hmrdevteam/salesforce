/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/08/2018
 * Created By: Zach Engman
 * Last Modified Date: 01/08/2018
 * Last Modified By:
 * Description: Test Coverage for the HMRMobInitialDataInfo REST Service
 * ****************************************************/
 @isTest
private class HMRMobInitialDataInfoTest {
    @testSetup
    private static void setupData(){
        User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;

        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;

        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;
    }

    @isTest
    private static void testGetInitialDataInfo(){
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        HMRMobInitialDataInfo.InitialDataInfoResponse initialDataInfoResult;

        request.requestURI = '/mobile/initialDataInfo/' + userRecord.Id;
        request.httpMethod = 'GET';

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        System.runAs(userRecord) {
            initialDataInfoResult = HMRMobInitialDataInfo.getInitialDataInfo();
        }

        Test.stopTest();

        //Verify the initial data info was successfully returned
        System.assertEquals(userRecord.ContactId, initialDataInfoResult.User.ContactId);
    }

    @isTest
    private static void testGetMobileResolvedImages(){
        string baseImage = 'Day1Hero.jpg';

        Test.startTest();
        //Android Related
        string hdpiResult = HMRMobInitialDataInfo.getResolvedImagePath('hdpi', baseImage);
        string ldpiResult = HMRMobInitialDataInfo.getResolvedImagePath('ldpi', baseImage);
        string mdpiResult = HMRMobInitialDataInfo.getResolvedImagePath('mdpi', baseImage);
        string xhdpiResult = HMRMobInitialDataInfo.getResolvedImagePath('xhdpi', baseImage);
        string xxhdpiResult = HMRMobInitialDataInfo.getResolvedImagePath('xxhdpi', baseImage);
        string xxxhdpiResult = HMRMobInitialDataInfo.getResolvedImagePath('xxxhdpi', baseImage);
        //IOS Related
        string xResult = HMRMobInitialDataInfo.getResolvedImagePath('x', baseImage);
        string x2xResult = HMRMobInitialDataInfo.getResolvedImagePath('x@2x', baseImage);
        string x3xResult = HMRMobInitialDataInfo.getResolvedImagePath('x@3x', baseImage);
        string result2x = HMRMobInitialDataInfo.getResolvedImagePath('@2x', baseImage);
        string result3x = HMRMobInitialDataInfo.getResolvedImagePath('@3x', baseImage);

        Test.stopTest();

        //Verify proper image result was returned
        //Android
        System.assert(hdpiResult.containsIgnoreCase('drawable-hdpi'));
        System.assert(ldpiResult.containsIgnoreCase('drawable-ldpi'));
        System.assert(mdpiresult.containsIgnoreCase('drawable-mdpi'));
        System.assert(xhdpiResult.containsIgnoreCase('drawable-xhdpi'));
        System.assert(xxhdpiResult.containsIgnoreCase('drawable-xxhdpi'));
        System.assert(xxxhdpiResult.containsIgnoreCase('drawable-xxxhdpi'));
        //IOS
        System.assert(xResult.containsIgnoreCase('x'));
        System.assert(x2xResult.containsIgnoreCase('@2x'));
        System.assert(x3xResult.containsIgnoreCase('@3x'));
        System.assert(result2x.containsIgnoreCase('@2x.'));
        System.assert(result3x.containsIgnoreCase('@3x.'));
    }
}