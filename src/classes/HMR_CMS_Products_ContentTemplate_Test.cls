/*
Developer: Ali Pierre (HMR)
Date: 10/18/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_Products_ContentTemplate
*/
@isTest(SeeAllData=true) 
private class HMR_CMS_Products_ContentTemplate_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_Products_ContentTemplate productsTemplate = new HMR_CMS_Products_ContentTemplate();
	    String propertyValue = productsTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String ShakesHeaderText = productsTemplate.ShakesHeaderText;
	    String ShakesDescriptionText = productsTemplate.ShakesDescriptionText;
	    String EntreesHeaderText = productsTemplate.EntreesHeaderText;
	    String EntreesDescriptionText = productsTemplate.EntreesDescriptionText;
	    String BarsHeaderText = productsTemplate.BarsHeaderText;
	    String BarsDescriptionText = productsTemplate.BarsDescriptionText;
	    Boolean is_eCommerce = productsTemplate.is_eCommerce;
	    Boolean is_eCommerceAdmin = productsTemplate.is_eCommerceAdmin;

	    productsTemplate.testAttributes = new Map<String, String>  {
            'ShakesHeaderText' => 'Test Shakes Header',
            'ShakesDescriptionText' => 'Test Shakes Description',
            'EntreesHeaderText' => 'Test Entree Header',
            'EntreesDescriptionText' => 'Test Entree Description',
            'BarsHeaderText' => 'Test Bar Header',
            'BarsDescriptionText' => 'Test Bar Description',
            'is_eCommerce' => 'False',
            'is_eCommerceAdmin' => 'False'
        };

	    string s1 = productsTemplate.getPropertyWithDefault('ShakesHeaderText','testValue1');
	    system.assertEquals(s1, 'Test Shakes Header');

	    String renderHTML = productsTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_Products_ContentTemplate kc = new HMR_CMS_Products_ContentTemplate(null);
	    }catch(Exception e){}
  	}

  	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_Products_ContentTemplate productsTemplate = new HMR_CMS_Products_ContentTemplate();
	    String propertyValue = productsTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String ShakesHeaderText = productsTemplate.ShakesHeaderText;
	    String ShakesDescriptionText = productsTemplate.ShakesDescriptionText;
	    String EntreesHeaderText = productsTemplate.EntreesHeaderText;
	    String EntreesDescriptionText = productsTemplate.EntreesDescriptionText;
	    String BarsHeaderText = productsTemplate.BarsHeaderText;
	    String BarsDescriptionText = productsTemplate.BarsDescriptionText;
	    Boolean is_eCommerce = productsTemplate.is_eCommerce;
	    Boolean is_eCommerceAdmin = productsTemplate.is_eCommerceAdmin;

	    productsTemplate.testAttributes = new Map<String, String>  {
            'ShakesHeaderText' => 'Test Shakes Header',
            'ShakesDescriptionText' => 'Test Shakes Description',
            'EntreesHeaderText' => 'Test Entree Header',
            'EntreesDescriptionText' => 'Test Entree Description',
            'BarsHeaderText' => 'Test Bar Header',
            'BarsDescriptionText' => 'Test Bar Description',
            'is_eCommerce' => 'True',
            'is_eCommerceAdmin' => 'True'
        };

	    string s1 = productsTemplate.getPropertyWithDefault('ShakesHeaderText','testValue1');
	    system.assertEquals(s1, 'Test Shakes Header');

	    String renderHTML = productsTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_Products_ContentTemplate kc = new HMR_CMS_Products_ContentTemplate(null);
	    }catch(Exception e){}
  	}
}