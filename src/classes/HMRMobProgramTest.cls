/**
* Apex Test Coverage of the HMRMobProgram Class
* @Date: 2018-01-16
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-01-16 
* @JIRA:
*/
@isTest
private class HMRMobProgramTest {
	  @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      List<Program__c> programList = new List<Program__c>{new Program__c(Name = 'A La Carte', Days_in_1st_Order_Cycle__c = 30),
  														  new Program__c(Name = 'In Person', Days_in_1st_Order_Cycle__c = 30)};
      insert programList;
    }

    @isTest
    private static void testGetProgramRecordsForMobile(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      List<HMRMobProgram.MobProgramDTO> programDTOList;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/program/';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	programDTOList = HMRMobProgram.doGet();
      }

      Test.stopTest();

      //Verify the program records were successfully returned
      System.assertEquals(200, response.statuscode);
    }
}