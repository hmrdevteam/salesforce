/**
* @Date: 5/25/2017
* @Author: Ali Pierre (HMR)
*/

@isTest(SeeAllData=False)

public class CC_CouponTriggerHandlerTest {

    static testMethod void testP1MERCK(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'MERCK', Standard_HSS_Kit_Discount__c = 100, Standard_HSAH_Kit_Discount__c = 50);
        insert testAccountObj;        
        
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3); 
        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();

        HMR_Coupon__c testHmrCouponObj1 = new HMR_Coupon__c(Name = 'MERCK', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj1);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

        ccrz__E_Coupon__c testCouponObj1 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_1' , ccrz__CouponCode__c = 'MERCK_P1_1', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P1 1st Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj1);        
        ccrz__E_Coupon__c testCouponObj2 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_Reorder' , ccrz__CouponCode__c = 'MERCK_P1_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P1 Re-Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 20.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj2);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = 50;
        }

        update ccCouponList;

        Test.stopTest();
    }

    static testMethod void testP1HSSBELLIN(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account',  Standard_HSS_Kit_Discount__c = 100, Standard_HSAH_Kit_Discount__c = 50);
        insert testAccountObj;
        
                
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3);

        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();
              
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'BELLIN', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

            
        ccrz__E_Coupon__c testCouponObj3 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_P1_HSS' , ccrz__CouponCode__c = 'BELLIN_P1_HSS', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'HSS 1st Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj3);
        ccrz__E_Coupon__c testCouponObj4 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_HSS_Reorder' , ccrz__CouponCode__c = 'BELLIN_HSS_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS Re-Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj4);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 25 : 100;
        }

        update ccCouponList;

        Test.stopTest();
    }
    
    static testMethod void testP2Misc(){

        Test.startTest();

        Date dt = Date.today();
        
        List<Account> accountList = new List<Account>();
        //create an Account       
        Account testAccountObj = new Account(Name = 'MERCK', Standard_HSS_Kit_Discount__c = 150, Standard_HSAH_Kit_Discount__c = 75);
        accountList.add(testAccountObj);

        Account testAccountObj2 = new Account(Name = 'BELLIN', Standard_HSS_Kit_Discount__c = 75, Standard_HSAH_Kit_Discount__c = 50);
        accountList.add(testAccountObj2);

        insert accountList;
        
        
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3); 
        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();

        HMR_Coupon__c testHmrCouponObj1 = new HMR_Coupon__c(Name = 'MERCK', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj1);        
        HMR_Coupon__c testHmrCouponObj2 = new HMR_Coupon__c(Name = 'TEST', Default_Consumer_Coupon__c = True,  hmr_Start_Date__c = dt, hmr_Active__c = true, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj2);        
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'BELLIN', Default_Consumer_Coupon__c = True, Account__c = testAccountObj2.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

        ccrz__E_Coupon__c testCouponObj5 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'TEST_P2_1' , ccrz__CouponCode__c = 'TEST_P2_1', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P2', HMR_Coupon__c = testHmrCouponObj2.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj3.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj5);
        ccrz__E_Coupon__c testCouponObj6 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'TEST_P2_Reorder' , ccrz__CouponCode__c = 'TEST_P2_Reorder', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'P2 Re-Order', HMR_Coupon__c = testHmrCouponObj2.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj3.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj6);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 5 : 10;
        }

        update ccCouponList;

        Test.stopTest();
    }

    static testMethod void testP1MERCK2(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'MERCK');
        insert testAccountObj;
        
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3); 
        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();

        HMR_Coupon__c testHmrCouponObj1 = new HMR_Coupon__c(Name = 'MERCK', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj1);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

        ccrz__E_Coupon__c testCouponObj1 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_1' , ccrz__CouponCode__c = 'MERCK_P1_1', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'P1 1st Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj1);        
        ccrz__E_Coupon__c testCouponObj2 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_Reorder' , ccrz__CouponCode__c = 'MERCK_P1_Reorder', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'P1 Re-Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 20.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj2);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 75 : 150;
        }

        update ccCouponList;

        Test.stopTest();
    }

    static testMethod void testP1HSSTest(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account', Standard_HSS_Kit_Discount__c = 100, Standard_HSAH_Kit_Discount__c = 50);
        insert testAccountObj;
                
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3);

        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();
              
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'TEST', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

            
        ccrz__E_Coupon__c testCouponObj3 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_P1_HSS' , ccrz__CouponCode__c = 'BELLIN_P1_HSS', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'HSS 1st Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj3);
        ccrz__E_Coupon__c testCouponObj4 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_HSS_Reorder' , ccrz__CouponCode__c = 'BELLIN_HSS_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS Re-Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj4);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 50 : 300;
        }

        update ccCouponList;

        Test.stopTest();
    }
    
    static testMethod void testP2Misc2(){

        Test.startTest();

        Date dt = Date.today();
        
        List<Account> accountList = new List<Account>();
        //create an Account       
        Account testAccountObj = new Account(Name = 'MERCK' , Standard_HSS_Kit_Discount__c = 100, Standard_HSAH_Kit_Discount__c = 50);
        accountList.add(testAccountObj);

        Account testAccountObj2 = new Account(Name = 'BELLIN');
        accountList.add(testAccountObj2);

        insert accountList;
        
                
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3); 
        insert prgList;

       

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();

        HMR_Coupon__c testHmrCouponObj1 = new HMR_Coupon__c(Name = 'MERCK', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj1);        
        HMR_Coupon__c testHmrCouponObj2 = new HMR_Coupon__c(Name = 'TEST', Default_Consumer_Coupon__c = True, hmr_Active__c = true,  hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj2);        
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'BELLIN', Default_Consumer_Coupon__c = True, Account__c = testAccountObj2.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

        ccrz__E_Coupon__c testCouponObj5 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'TEST_P2_1' , ccrz__CouponCode__c = 'TEST_P2_1', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P2', HMR_Coupon__c = testHmrCouponObj2.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj3.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj5);
        ccrz__E_Coupon__c testCouponObj6 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'TEST_P2_Reorder' , ccrz__CouponCode__c = 'TEST_P2_Reorder', ccrz__CouponType__c = 'Absolute', Coupon_Order_Type__c = 'P2 Re-Order', HMR_Coupon__c = testHmrCouponObj2.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Absolute', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj3.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj6);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 15 : 90;
        }

        update ccCouponList;

        Test.stopTest();
    }
    static testMethod void testP1HSSTest2(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;
                
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3);

        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();
              
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'TEST', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

            
        ccrz__E_Coupon__c testCouponObj3 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_P1_HSS' , ccrz__CouponCode__c = 'BELLIN_P1_HSS', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS 1st Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj3);
        ccrz__E_Coupon__c testCouponObj4 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_HSS_Reorder' , ccrz__CouponCode__c = 'BELLIN_HSS_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS Re-Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj4);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 50 : 300;
        }

        update ccCouponList;

        Test.stopTest();
    }

    static testMethod void testP1HSSTest3(){

        Test.startTest();

        Date dt = Date.today();
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account', Standard_HSS_Kit_Discount__c = 100, Standard_HSAH_Kit_Discount__c = 50);
        insert testAccountObj;
                
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Default_Promotional_Discount__c = 100, hmr_Standard_Kit_Price__c = 400);
        prgList.add(testProgramObj3);

        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();
              
        HMR_Coupon__c testHmrCouponObj3= new HMR_Coupon__c(Name = 'TEST', Default_Consumer_Coupon__c = True, Account__c = testAccountObj.Id, hmr_Active__c = true, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj3);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

            
        ccrz__E_Coupon__c testCouponObj3 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_P1_HSS' , ccrz__CouponCode__c = 'BELLIN_P1_HSS', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS 1st Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 50.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj3);
        ccrz__E_Coupon__c testCouponObj4 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'BELLIN_HSS_Reorder' , ccrz__CouponCode__c = 'BELLIN_HSS_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'HSS Re-Order', HMR_Coupon__c = testHmrCouponObj3.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj1.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj4);
        
        insert ccCouponList;
        
        for(ccrz__E_Coupon__c coup : ccCouponList){
        	coup.ccrz__DiscountAmount__c = coup.ccrz__DiscountType__c == 'Percentage' ? 50 : 300;
        }

        update ccCouponList;

        Test.stopTest();
    }
}