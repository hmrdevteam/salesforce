/**
 * convenience class to allow scheduling notifications using "Schedule Apex"
 */
global class HMRPushFirstDayOfDietSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        try {
            HMRPushNotificationJob bj = new HMRPushNotificationJob(HMRNotificationType.FIRST_DAY_OF_DIET_REMINDER);
            database.executebatch(bj, 200);
        }
        catch(Exception ex) {
            System.debug('The HMRPushFirstDayOfDietSchedule failed ' + ex.getMessage());
        }       
    }
}