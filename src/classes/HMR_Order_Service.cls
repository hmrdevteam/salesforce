/**
* Apex Service Class for the Order
*
* @Date: 2017-10-19
* @Author: Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA:
*/
public with sharing class HMR_Order_Service {


    public HMR_Order_Service(){
        this(UserInfo.getUserId());
    }

    public HMR_Order_Service(string userId){
        ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
        contextCCRZ.portalUserId = userId;
        ccrz.cc_CallContext.initRemoteContext(contextCCRZ);
    }


    /**
* Fetches order data from cloudcraze using the order encryption id.
* @param encryption id of the order
* @return the details of the order
*/
    public OrderRecord getOrderByEncryptedId(string encryptedOrderId){

        Map<String, Object> inputDataMap = new Map<String, Object>{
            ccrz.ccApi.API_Version => 6
                ,ccrz.ccApiOrder.PARAM_ORDER_ENCID_LIST => new Set<String>{encryptedOrderId}
            ,ccrz.ccApi.SIZING => new Map<String, Object>{
                ccrz.ccApiOrder.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL},
                    ccrz.ccApiTransactionPayment.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL}
            }
        };

            return getOrder(inputDataMap);
    }


    public Map<String, Object> updateOrderDate(string orderId, string orderDate, boolean isCSRFlow){
        Map<String, Object> returnMap = new Map<String, Object>();
        //Get the related order
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__OrderDate__c
                                            ,hmr_Order_Type_c__c
                                            ,PS_Reorder_Date_Change__c
                                            ,Reorder_Date_Change__c
                                            ,Original_Order_Date__c
                                            FROM ccrz__E_Order__c
                                            WHERE ccrz__EncryptedId__c =: orderId];
        ccrz__E_Order__c orderRecord;
        Date newOrderDate = Date.parse(orderDate);
        DateTime newOrderDateTime = DateTime.newInstance(newOrderDate.Year(), newOrderDate.month(), newOrderDate.day());

        if(orderList.size() > 0){
            orderRecord = orderList[0];
            //Confirm the date has been changed
            if(orderRecord.ccrz__OrderDate__c != newOrderDate){
                //If the date was already changed from both possible scenarios, it cannot be changed again
                //For P2 1st Orders, CSR/PS Can update unlimited amount
                if(!orderRecord.Reorder_Date_Change__c
                   || (!orderRecord.PS_Reorder_Date_Change__c && isCSRFlow)
                   || (orderRecord.hmr_Order_Type_c__c == 'P2 1st Order' && isCSRFlow)){

                       if(orderRecord.Original_Order_Date__c == null)
                           orderRecord.Original_Order_Date__c = orderRecord.ccrz__OrderDate__c;

                       orderRecord.ccrz__OrderDate__c = Date.parse(orderDate);

                       //The date can be changed by a PS User for a second update
                       if(orderRecord.Reorder_Date_Change__c)
                           orderRecord.PS_Reorder_Date_Change__c = true;
                       else
                           orderRecord.Reorder_Date_Change__c = true;

                       update orderRecord;

                       returnMap.put('success', true);
                       returnMap.put('newOrderDateTime', newOrderDateTime.format('MMMM dd, yyyy'));
                   }
                else{
                    returnMap.put('success', false);
                    returnMap.put('data', 'Order date has already been updated for this order and cannot be changed.');
                }
            }
            else{
                returnMap.put('success', true);
                returnMap.put('data', 'Order date has not been changed');
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('data', 'Order was not found.  It may have been deleted.');
        }

        return returnMap;
    }

    /**
* Fetches order data from cloudcraze using the order encryption id.
* @param inputDataMap the cloud craze formatted request parameters
* @return  the order record containing products and product specs
*/
    private OrderRecord getOrder(Map<String, Object> inputDataMap){
        OrderRecord orderRecord;

        try{
            Map<String, Object> outputDataMap = ccrz.ccAPIOrder.fetch(inputDataMap);
            if(outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST) != null){
                List<Map<String, Object>> outputOrderList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST);
                List<Map<String, Object>> outputAddressList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiAddress.ADDRESSLIST);
                List<Map<String, Object>> outputProductList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST);

                orderRecord = new OrderRecord(outputOrderList[0],outputAddressList,outputProductList);
            }

        }
        catch(Exception ex){
            System.Debug(ex);
            System.Debug('Stack Trace -> ' + ex.getStackTraceString());
        }

        return orderRecord;
    }



    /**
* Fetches order data from cloudcraze using the order owner id.
* @param owner id of the order
* @return the list details of the order
*/
    public List<OrderRecord> getOrderByOwner(string ownerId){

        Map<String, Object> inputDataMap = new Map<String, Object>{
            ccrz.ccApi.API_Version => 6
                ,ccrz.ccApiOrder.PARAM_OWNERID => ownerId
                ,ccrz.ccApi.SIZING => new Map<String, Object>{
                    ccrz.ccApiOrder.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL},
                        ccrz.ccApiTransactionPayment.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL}
                }
        };

            return getOrderList(inputDataMap);
    }

    /**
* Fetches order data from cloudcraze using the order owner id.
* @param inputDataMap the cloud craze formatted request parameters
* @return  List of order record containing products and product specs
*/
    private List<OrderRecord> getOrderList(Map<String, Object> inputDataMap){
        List<OrderRecord> orderRecordList = new List<OrderRecord>();

        try{
            Map<String, Object> outputDataMap = ccrz.ccAPIOrder.fetch(inputDataMap);
            if(outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST) != null){
                List<Map<String, Object>> outputOrderList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST);
                List<Map<String, Object>> outputAddressList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiAddress.ADDRESSLIST);
                List<Map<String, Object>> outputProductList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccApiProduct.PRODUCTLIST);

                for(Map<String, Object> mp : outputOrderList)
                    orderRecordList.add( new OrderRecord(mp,outputAddressList,outputProductList ));
            }

        }
        catch(Exception ex){
            System.Debug(ex);
            System.Debug('Stack Trace -> ' + ex.getStackTraceString());
        }

        orderRecordList.sort();

        return orderRecordList;
    }

    /**
    * Fetches order data from cloudcraze using the order owner id.
    * @param owner id of the order
    * @return the list details of the order
    * This is used for the My Orders view in My Account, strips out the extra order details.
    */
    public List<OrderRecord> getOrderByOwnerMA(string ownerId){

        Map<String, Object> inputDataMap = new Map<String, Object>{
            ccrz.ccApi.API_Version => 6
                ,ccrz.ccApiOrder.PARAM_OWNERID => ownerId
                ,ccrz.ccApi.SIZING => new Map<String, Object>{
                    ccrz.ccApiOrder.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L},
                        ccrz.ccApiTransactionPayment.ENTITYNAME => new Map<String, Object>{ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L}
                }
        };

            return getOrderListMA(inputDataMap);
    }

    /**
    * Fetches order data from cloudcraze using the order owner id.
    * @param inputDataMap the cloud craze formatted request parameters
    * @return  List of order record containing products and product specs
    * This is used for the My Orders view in My Account, strips out the extra order details.
    */
    private List<OrderRecord> getOrderListMA(Map<String, Object> inputDataMap){
        List<OrderRecord> orderRecordList = new List<OrderRecord>();
        Boolean isMyAccount = true;

        try{
            Map<String, Object> outputDataMap = ccrz.ccAPIOrder.fetch(inputDataMap);
            if(outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST) != null){
                List<Map<String, Object>> outputOrderList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccAPIOrder.ORDERLIST);

                for(Map<String, Object> mp : outputOrderList)
                    orderRecordList.add( new OrderRecord(mp, true));
            }

        }
        catch(Exception ex){
            System.Debug(ex);
            System.Debug('Stack Trace -> ' + ex.getStackTraceString());
        }

        orderRecordList.sort();

        return orderRecordList;
    }


    //Wrapper class to hold Order record and related object data
    public class OrderRecord implements Comparable{
        public string id {get; private set;}
        public decimal adjustmentAmount {get; private set;}
        public string orderName {get; private set;}
        public String billTo {get; private set;}
        public String buyerEmail {get; private set;}
        public String buyerPhone {get; private set;}
        public String hmrOrderType {get; private set;}
        public integer orderNumber {get; private set;}
        public String orderDate {get; private set;}
        public String orderStatus {get; private set;}
        public Date originalOrderDate {get; private set;}
        public string encryptedId {get; private set;}
        public string currencyISOCode {get; private set;}
        public decimal subtotalAmount {get; private set;}
        public decimal totalAmount {get; private set;}
        public integer totalQuantity {get; private set;}
        public decimal taxAmount{get; private set;}
        public decimal shipAmount{get; private set;}
        public decimal totalDiscount{get; private set;}
        public decimal totalSurcharge{get; private set;}
        public String shipMethod{get; private set;}
        public String shipNote{get; private set;}
        public String shipTo {get; private set;}
        public String shipTrackingNo{get; private set;}
        public AddressRecord shippingAddress{get; private set;}
        public AddressRecord billingAddress{get; private set;}
        public List<OrderItem> orderItemList {get; private set;}
        public List<Map<String, Object>> outputProductList{get; private set;}
        public Map<String, ccrz__E_ProductCategory__c> productCategoryMap {get; private set;}
        public HMR_TransactionPayment_Service.TransactionPaymentRecord paymentRecord{get; private set;}

        public OrderRecord(Map<String, Object> orderDataMap){
            this(orderDataMap, null, null);
        }

        public OrderRecord(Map<String, Object> orderDataMap, Boolean isMyAccount){
            this.orderName = (String)orderDataMap.get('sfdcName');
            this.totalAmount = (Decimal)orderDataMap.get('totalAmount');
            this.orderNumber = (Integer)orderDataMap.get('orderNumber');
            this.orderDate = (String)orderDataMap.get('orderDate');
            this.orderStatus = (String)orderDataMap.get('orderStatus');
            this.encryptedId = (String)orderDataMap.get('encryptedId');
        }

        public OrderRecord(Map<String, Object> orderDataMap,  List<Map<String, Object>> addressList, List<Map<String, Object>> productList){
            HMR_TransactionPayment_Service paymentService = new HMR_TransactionPayment_Service();

            //Default discount fields
            this.totalDiscount = 0.0;
            this.adjustmentAmount = 0.0;

            this.id = (String)orderDataMap.get('sfid');
            this.adjustmentAmount = (Decimal)orderDataMap.get('adjustmentAmount');
            this.orderName = (String)orderDataMap.get('sfdcName');
            this.billTo = (String)orderDataMap.get('billTo');
            this.buyerEmail = (String)orderDataMap.get('buyerEmail');
            this.buyerPhone = (String)orderDataMap.get('buyerPhone');
            this.currencyISOCode = (String)orderDataMap.get('currencyISOCode');
            this.encryptedId = (String)orderDataMap.get('encryptedId');
            this.subtotalAmount = (Decimal)orderDataMap.get('subtotalAmount');
            this.totalAmount = (Decimal)orderDataMap.get('totalAmount');
            this.totalQuantity = (Integer)orderDataMap.get('totalQuantity');
            this.orderNumber = (Integer)orderDataMap.get('orderNumber');
            this.orderDate = (String)orderDataMap.get('orderDate');
            this.orderStatus = (String)orderDataMap.get('orderStatus');
            this.taxAmount = (Decimal)orderDataMap.get('taxAmount');
            this.shipAmount = (Decimal)orderDataMap.get('shipAmount');
            this.shipMethod = (String)orderDataMap.get('shipMethod');
            this.shipNote = (String)orderDataMap.get('note');
            this.shipTrackingNo = (String)orderDataMap.get('trackingNo');
            this.shipTo = (String)orderDataMap.get('shipTo');
            this.totalSurcharge  = (Decimal)orderDataMap.get('totalSurcharge');
            //this.deliveryDate = (Date)orderDataMap.get('deliveryDate');
            this.totalDiscount = (Decimal)orderDataMap.get('totalDiscount');
            this.orderItemList = new List<OrderItem>();
            this.outputProductList = productList;
            List<Map<String, Object>> orderItemMapList = (List<Map<String, Object>>)orderDataMap.get('EOrderItemsS');

            //Get the product category map for summary groupings
            this.productCategoryMap = new Map<String, ccrz__E_ProductCategory__c>();
            for(ccrz__E_ProductCategory__c productCategory : [SELECT ccrz__Product__r.Id, ccrz__Product__r.ccrz__SKU__c, ccrz__Category__r.Name  FROM ccrz__E_ProductCategory__c WHERE ccrz__Category__r.Name != 'HMR Products'])
                productCategoryMap.put(String.valueOf(productCategory.ccrz__Product__r.Id), productCategory);

            for(Map<String, Object> orderItemDataMap : orderItemMapList){
                orderItemList.add(new OrderItem(orderItemDataMap, productList, productCategoryMap));
            }

            if(addressList != null){
                for(Map<String, Object> addressMap : addressList){
                    string addressId = (String)addressMap.get('sfid');
                    if(addressId == this.shipTo)
                        this.shippingAddress = new AddressRecord(addressMap);
                    if(addressId == this.billTo)
                        this.billingAddress = new AddressRecord(addressMap);
                }
            }

            this.paymentRecord = paymentService.getByOrderId(this.id);

            //Assign custom fields not available in the ccOrderApi
            if(!String.isBlank(this.encryptedId)){
                ccrz__E_Order__c orderRecord = HMR_CC_Order_Selector.getByEncryptedId(this.encryptedId);
                this.hmrOrderType = orderRecord.hmr_Order_Type_c__c;
                this.originalOrderDate = orderRecord.Original_Order_Date__c;
            }

        }

        public Integer compareTo(Object compareTo){
            OrderRecord compareToOrderRecord = (OrderRecord)compareTo;
            return orderNumber == compareToOrderRecord.orderNumber ? 0 :
            orderNumber < compareToOrderRecord.orderNumber ? 1 : -1;
        }
    }

    public class OrderItem{
        public String createdDate {get; private set;}
        public decimal absoluteDiscount {get; private set;}
        public decimal adjustmentAmount {get; private set;}
        public String productName {get; private set;}
        public Integer quantity {get; private set;}
        public Decimal itemTotal {get; private set;}
        public Decimal price {get; private set;}
        public String orderLineType {get; private set;}
        public String recId {get; private set;}
        public String productId {get; private set;}
        public String productType {get; private set;}
        public String imageUri {get; private set;}
        public String productCategory {get; private set;}
        public String productImage {get; private set;}
        public String productSearchImage {get; private set;}
        public String productSKU {get; private set;}
        public String productThumbnailImage {get; private set;}


        public OrderItem(Map<String, Object> orderItemDataMap, List<Map<String, Object>> productList, Map<String, ccrz__E_ProductCategory__c> productCategoryMap){

            this.createdDate = (String)orderItemDataMap.get('createdDate');
            this.absoluteDiscount = (Decimal)orderItemDataMap.get('absoluteDiscount');
            this.adjustmentAmount = (Decimal)orderItemDataMap.get('adjustmentAmount');
            this.productName = (String)orderItemDataMap.get('productName');
            this.quantity = (Integer)orderItemDataMap.get('quantity');
            this.itemTotal = (Decimal)orderItemDataMap.get('itemTotal');
            this.price = (Decimal)orderItemDataMap.get('price');
            this.recId = (String)orderItemDataMap.get('sfid');
            this.orderLineType = (String)orderItemDataMap.get('orderLineType');
            this.productId = (String)orderItemDataMap.get('product');
            this.productType = (String)orderItemDataMap.get('productType');

            if(productList != null && productList.size() > 0){
                for(Integer i=0; i < productList.size(); i++){
                    Map<String, Object> productMap = productList[i];
                    String productRecordId = (String)productMap.get('sfid');
                    if(this.productId == productRecordId){
                        this.productSKU = (String)productMap.get('SKU');
                    }
                    List<Map<String,Object>> productMediaList = (List<Map<String,Object>>)productList[i].get('EProductMediasS');
                    for(Map<String, Object> productMediaMap : productMediaList){
                        string mediaProductId = String.valueOf(productMediaMap.get('product'));
                        if(this.productId == mediaProductId && productMediaMap.get('mediaType') != null){
                            string mediaType = String.valueOf(productMediaMap.get('mediaType'));
                            if(mediaType == 'Product Image' && productMediaMap.get('URI') != null)
                                this.productImage = String.valueOf(productMediaMap.get('URI'));
                            else if(mediaType == 'Product Image Thumbnail' && productMediaMap.get('URI') != null)
                                this.productThumbnailImage = String.valueOf(productMediaMap.get('URI'));
                            else if(mediaType == 'Product Search Image' && productMediaMap.get('URI') != null)
                                this.productSearchImage = String.valueOf(productMediaMap.get('URI'));
                        }
                    }
                }
            }

            if(productCategoryMap != null && productCategoryMap.size() > 0 && productCategoryMap.containsKey(this.productId)){
                this.productCategory = productCategoryMap.get(this.productId).ccrz__Category__r.Name;
            }
        }
    }

    //TODO: Determine if we want to format/move this declaration to address service
    public class AddressRecord{
        public string firstName {get; private set;}
        public string lastName {get; private set;}
        public string addressFirstline {get; private set;}
        public string city {get; private set;}
        public string stateISOCode{get; private set;}
        public string postalCode{get; private set;}

        public AddressRecord(Map<String, Object> orderAddressDataMap){

            this.firstName = (String)orderAddressDataMap.get('firstName');
            this.lastName = (String)orderAddressDataMap.get('lastName');
            this.addressFirstline = (String)orderAddressDataMap.get('addressFirstline');
            this.city = (String)orderAddressDataMap.get('city');
            this.stateISOCode = (String)orderAddressDataMap.get('stateISOCode');
            this.postalCode = (String)orderAddressDataMap.get('postalCode');

        }
    }

}