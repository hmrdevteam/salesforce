/**
* Test Class Coverage of the HMR_CMS_ClinicLanding_ContentTemplate
*
* @Date: 03/25/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 03/25/2017
* @JIRA:
*/
@isTest
private class HMR_CMS_ClinicLanding_Test {
    @isTest
    private static void testGetHTMLWithSingleProvider(){
        HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();
        //TODO - Move Account Record Creation to a Test Factory
        List<Account> accountList = new List<Account>{new Account(Name = 'Single Account',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111')};

        controller.providerList = accountList;

        Test.startTest();

        String htmlResult = controller.getHTML();

        Test.stopTest();

        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }

    @isTest
    private static void testGetHTMLWithMultipleProviders(){
        HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();
        //TODO - Move Account Record Creation to a Test Factory
        List<Account> accountList = new List<Account>{new Account(Name = 'First Account',
                                                                  HMR_Account_Name_1_Optional__c = 'First Account Program',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111'),
            										new Account(Name = 'Second Account',
                                                                  BillingStreet = '222 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '222-222-2222')};

        controller.providerList = accountList;

        Test.startTest();

        String htmlResult = controller.getHTML();

        Test.stopTest();

        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }

    @isTest
    private static void testGetHTMLRemote(){
        //TODO - Move Account Record Creation to a Test Factory
        List<Account> accountList = new List<Account>{new Account(Name = 'Single Account',
                                                                  BillingStreet = '111 Main Street',
                                                                  BillingCity = 'Los Angeles',
                                                                  BillingState = 'CA',
                                                                  BillingPostalCode = '90067',
                                                                  Phone = '111-111-1111')};


        Test.startTest();

        List<String> htmlResultList = HMR_CMS_ClinicLanding_ContentTemplate.getHTMLRemote(JSON.serialize(accountList));

        Test.stopTest();

        //Verify the html was returned
        System.assert(htmlResultList.size() > 0);
    }

    private static void testGetHTMLRemoteWithInvalidJSON(){
        Test.startTest();

        List<String> htmlResultList = HMR_CMS_ClinicLanding_ContentTemplate.getHTMLRemote('JSON');

        Test.stopTest();

        //Verify the html was returned
        System.assert(htmlResultList.size() > 0);
    }

	@isTest
    private static void testLeadInsert(){
        //TODO - Move Lead Record Creation to a Test Factory
        List<Lead> leadList = new List<Lead>{new Lead(FirstName = 'First',
                                                      LastName = 'Last',
                                                      Company = 'Magnet360',
                                                      Email = 'testlead@magnet360.com',
                                                      Phone = '111-111-1111',
                                                      hmr_Lead_Type__c = 'Clinic')};
        Test.startTest();

        List<Lead> leadResultList = HMR_CMS_ClinicLanding_ContentTemplate.insertLeadRecords(leadList);

        Test.stopTest();

        //Verify the Lead Records were Inserted
        System.assert(!String.isBlank(leadResultList[0].Id));
    }

    @isTest
    private static void testFormatPhoneDisplay(){
        HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();
        string hyphenPhoneToFormat = '111-111-1111';
        string mixedPhoneToFormat = '(222) 222-2222';
        string invalidPhoneToFormat = '333 333 3333 33';

		Test.startTest();

        string hyphenPhoneResult = controller.formatPhoneDisplay(hyphenPhoneToFormat);
        string mixedPhoneResult = controller.formatPhoneDisplay(mixedPhoneToFormat);
        string invalidPhoneResult = controller.formatPhoneDisplay(invalidPhoneToFormat);

        Test.stopTest();

        //Verify the Results are as Expected
        System.assertEquals('111.111.1111', hyphenPhoneResult);
        System.assertEquals('222.222.2222', mixedPhoneResult);
        System.assertEquals(invalidPhoneToFormat, invalidPhoneResult);
    }

    @isTest
    private static void testRetrieveLeadSourcePicklistValues(){
        HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();

        Test.startTest();

        List<String> leadSourceList = controller.getLeadSourcePickListValues();

        Test.stopTest();

        //Verify some values were returned
        System.assert(leadSourceList.size() > 0);
    }

    @isTest
    private static void testContructors(){
        Test.startTest();

        HMR_CMS_ClinicLanding_ContentTemplate constructorEmpty = new HMR_CMS_ClinicLanding_ContentTemplate();
        HMR_CMS_ClinicLanding_ContentTemplate constructorFromContentController;
        HMR_CMS_ClinicLanding_ContentTemplate constructorFromContentCore;

        try{
        	constructorFromContentController = new HMR_CMS_ClinicLanding_ContentTemplate(new cms.CreateContentController());
        	constructorFromContentCore = new HMR_CMS_ClinicLanding_ContentTemplate(new cms.CoreController());
        }
        catch(Exception ex){}

        Test.stopTest();

        //Verify the classes were instantiated
        System.assert(constructorEmpty != null);
        System.assert(constructorFromContentController == null);
        System.assert(constructorFromContentCore == null);
    }

    @isTest
    private static void testGenericPropertyMethods(){
        HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();

        Test.startTest();

        String propertyDefaultValue = controller.getPropertyWithDefault('heroImage', 'image');
        String attributeDefaultValue = controller.getAttribute('heroImage');

        Test.stopTest();

        //Verify Values
        System.assert(!String.isBlank(propertyDefaultValue));
        System.assert(String.isBlank(attributeDefaultValue));
        System.assertEquals(propertyDefaultValue, 'image');
    }

    // @isTest
    // private static void testRetrievalOfProviders(){
    //     HMR_CMS_ClinicLanding_ContentTemplate controller = new HMR_CMS_ClinicLanding_ContentTemplate();
    //
    //     Test.startTest();
    //
    //     List<Account> providerList = controller.providerList;
    //
    //     Test.stopTest();
    //
    //     System.assert(providerList.size() == 0);
    // }
}