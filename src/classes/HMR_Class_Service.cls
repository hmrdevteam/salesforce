/**
* Service class to get coaching session records
*
* @Date: 2018-02-13
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: 2018-02-20 - Zach Engman
* @JIRA:
*/


public without sharing class HMR_Class_Service {
    public String contactId;
    private static final Map<String, Integer> getMilitaryHour = new Map<String, Integer>{
        '12 AM'=> 0,
        '1 AM'=> 1,
        '2 AM'=> 2,
        '3 AM'=> 3,
        '4 AM'=> 4,
        '5 AM'=> 5,
        '6 AM'=> 6,
        '7 AM'=> 7,
        '8 AM'=> 8,
        '9 AM'=> 9,
        '10 AM'=> 10,
        '11 AM'=> 11,
        '12 PM'=> 12,
        '1 PM'=> 13,
        '2 PM'=> 14,
        '3 PM'=> 15,
        '4 PM'=> 16,
        '5 PM'=> 17,
        '6 PM'=> 18,
        '7 PM'=> 19,
        '8 PM'=> 20,
        '9 PM'=> 21,
        '10 PM'=> 22,
        '11 PM'=> 23
    };
    private static final Map<Integer, String> getStandardHour = new Map<Integer, String>{
         0=> '12 AM',
         1=> '1 AM',
         2=> '2 AM',
         3=> '3 AM',
         4=> '4 AM',
         5=> '5 AM',
         6=> '6 AM',
         7=> '7 AM',
         8=> '8 AM',
         9=> '9 AM',
         10=> '10 AM',
         11=> '11 AM',
         12=> '12 PM',
         13=> '1 PM',
         14=> '2 PM',
         15=> '3 PM',
         16=> '4 PM',
         17=> '5 PM',
         18=> '6 PM',
         19=> '7 PM',
         20=> '8 PM',
         21=> '9 PM',
         22=> '10 PM',
         23=> '11 PM'
    };
    private ContactDetails contactDetails;
    private ProgramMembershipDetails programMembershipDetails;

    /* class constructor */
    public HMR_Class_Service(String contactId) {
        this.contactId = contactId;
        this.contactDetails = new ContactDetails(getContactDetails());
        this.programMembershipDetails = new ProgramMembershipDetails(getProgramMembershipRecordDetails());
    }

    private Contact getContactDetails(){
        Contact contactRecord;

        if(!String.isBlank(contactId)){
            List<Contact> conList = [SELECT FirstName, LastName, MobileLogin__c, Time_Zone__c, Time_Zone_Offset__c FROM Contact WHERE Id =: contactId];    
            if(conList.size() > 0){
                contactRecord = conList[0];
            }
        }

        return contactRecord;
    }
    
    public Program_Membership__c getProgramMembershipRecordDetails(){
        Program_Membership__c programMembershipRecord = new Program_Membership__c();

        if(!String.isBlank(contactId)){
            List<Program_Membership__c> membershipList = [SELECT Program__c, Program__r.Name FROM Program_Membership__c WHERE hmr_Status__c = 'Active' AND hmr_Contact__c = :contactId LIMIT 1];
            
            if(membershipList.size() > 0){           
                programMembershipRecord = membershipList[0];           
            }
        }

        return programMembershipRecord;
    }
    
    
    /**
     * method to get the class results by date for the week
     * @param classDate - week start date
     * @return classRecordList - class record list
    */
    public Map<String, Object> getSearchResult(Date classDate){
        Map<String, Object> resultMap = new Map<String, Object>();

        List<ClassRecord> classRecList = new List<ClassRecord>();

        if(classDate != null){
            List<Coaching_Session__c> coachingSessionList = getSelectedClassList(classDate);
            classRecList = getClassRecords(coachingSessionList);
        }

        classRecList = adjustCoachNameDisplay(classRecList);

        classRecList.sort();

        resultMap.put('contactDetails', this.contactDetails);
        resultMap.put('programMembershipDetails', this.programMembershipDetails);
        resultMap.put('classList', classRecList);

        //Check if user should see this page (meant to be a one-time scheduler for only Healthy Solutions and Phase 2 users)
        List<Class_Member__c> alreadyEnrolled = [SELECT Id FROM Class_Member__c where hmr_Contact_Name__c = :contactId AND hmr_Active__c = true];
        resultMap.put('autoRedirect', alreadyEnrolled.size() > 0 || (this.programMembershipDetails.programName != 'Healthy Solutions' && this.programMembershipDetails.programName != 'Phase 2'));

        return resultMap;
    }
    
    private List<ClassRecord> getClassRecords(List<Coaching_Session__c> coachingSessionList){
        List<ClassRecord> classRecList = new List<ClassRecord>();
        
        for(Coaching_Session__c cs : coachingSessionList){
            classRecList.add(new ClassRecord(cs, this.contactDetails, this.programMembershipDetails));    
        }       

        return classRecList;    
    }

    /**
     * method to adjust coach display name based on business rules (only show last initial if duplicate first name exists)
     * @param classRecordList - class record list to format
     * @return classRecordList - class record list with formatted coach names
    */
    private List<ClassRecord> adjustCoachNameDisplay(List<ClassRecord> classRecordList){
        Map<String, Integer> firstNameMap = new Map<String, Integer>();

        for(AggregateResult ar : [SELECT hmr_Coach__r.FirstName firstName, count(Id) total FROM Coach__c WHERE hmr_Active__c = true GROUP BY hmr_Coach__r.FirstName]){
            firstNameMap.put((String)ar.get('firstName'), Integer.valueOf(ar.get('total')));
        }

        for(ClassRecord classRecord : classRecordList){
            if(firstNameMap.containsKey(classRecord.coachFirstName)){
                classRecord.hasDuplicateFirstName = firstNameMap.get(classRecord.coachFirstName) > 1;
            }
        }

        return classRecordList;
    }

    /*  Method to get all the coaching session based on the input search
        values.
        Takes 'Date' as Parameter
    */
    public List<Coaching_Session__c> getSelectedClassList(Date firstClassDate){
        
         Set<Id> classWithMembershipSet = new Set<Id>();
         String programId =  this.programMembershipDetails.programId;
         
         if(!String.isBlank(programId )){
            List<Class__c> selectedClass = [SELECT hmr_First_Class_Date__c 
                                            FROM Class__c 
                                            WHERE hmr_Active__c = true AND Program__c =: programId];

            for(Class__c classObj : selectedClass){
                classWithMembershipSet.add(classObj.id);
            }
         } 
         
        Integer timeOffset;
         
        if(this.contactDetails.timeZone != null){
            timeOffset = Integer.valueOf(this.contactDetails.timeZone) - 5;
        }
        else{
            timeOffset = 0;
        }      
        
        String timeOfDay = '';
        Date endDate = firstClassDate.addDays(6);

        List<Coaching_Session__c>  classList = Database.Query('SELECT hmr_Class__c, hmr_Class__r.hmr_First_Class_Date__c, hmr_Class__r.hmr_Class_Name__c, hmr_Class__r.hmr_Coach__r.hmr_Coach_First_Name__c, hmr_Class__r.hmr_Coach__r.hmr_Coach_Last_Name__c, Do_Not_Offer_External__c, hmr_Class_Date__c,hmr_Start_Time__c,hmr_Class__r.hmr_Class_Day__c,' +
                                                                ' hmr_Class__r.hmr_Client_Enrolled__c, hmr_Class__r.hmr_Class_Strength__c, isEasternTime__c FROM Coaching_Session__c' +
                                                                ' WHERE Do_Not_Offer_External__c = false AND hmr_Number_of_Participants__c < 15 AND hmr_Class_Date__c >= :firstClassDate AND hmr_Class_Date__c < :endDate AND ' +
                                                                ' hmr_Class__c IN :classWithMembershipSet'+
                                                                ' ORDER BY hmr_Class_Date__c ASC');

        if(!classList.isEmpty() && timeOffset != null && timeOffset!= 0){
            for(Coaching_Session__c clsTime : classList){                
                try {
                    String hour = clsTime.hmr_Start_Time__c.substringBefore(':');
                    String min = clsTime.hmr_Start_Time__c.substringBetween(':', ' ');

                    if(clsTime.hmr_Start_Time__c.contains('AM')){
                        hour = hour + ' AM';
                    }
                    else{
                        hour = hour + ' PM';
                    }

                    Integer adjustHour = getMilitaryHour.get(hour);
                    //Loop counter
                    Integer i = 0;
                    
                    if(timeOffset > 0){
                        while (timeOffset > i) {
                            if(adjustHour == 0){
                                //if time is 0(Midnight), reset hour to 23(11 PM)
                                adjustHour = 23;
                            }
                            else{
                                adjustHour -= 1;
                            }
                            i++;
                        }
                    }
                    else if(timeOffset < 0){
                        timeOffset *= -1;
                        while (timeOffset > i) {
                            if(adjustHour == 23){
                                //if time is 23(11 PM), reset hour to 0(Midnight)
                                adjustHour = 0;
                            }
                            else{
                                adjustHour += 1;
                            }                            
                            i++;
                        }
                    }
                    

                    String newTime = getStandardHour.get(adjustHour);

                    clsTime.hmr_Start_Time__c = newTime.contains('AM') ? newTime.substringBefore(' ') + ':' + min + ' AM' : newTime.substringBefore(' ') + ':' + min + ' PM';
                }
                catch (exception e) {
                      clsTime.isEasternTime__c = true;
                }                
            }
        }        
        else if(!classList.isEmpty() && timeOffset == null){
            for(Coaching_Session__c clsTime : classList){
                clsTime.isEasternTime__c = true;
            }
        }

        return classList;
    }


    /* Method to assign the client to
    the selected class
    */
    public Map<String, Object> assignClass(String selectedClass, String selectedClassFirstClassDate){

        Map<String, Object> responseMap = new Map<String, Object>();

        try{
            // Here we are checking if the cliet has already registered for a class
            List<Class_Member__c> alreadyEnrolled = [SELECT Id FROM Class_Member__c where hmr_Contact_Name__c = :contactId AND hmr_Active__c = true];  
            
            if(alreadyEnrolled.size() > 0){
                responseMap.put('error', 'Client is already enrolled in the Phone Program');
                responseMap.put('success', true);

                return responseMap;
            }
    
            if(!String.isBlank(selectedClass)){
    
                Program_Membership__c membership = [SELECT Program__c, Program__r.Name FROM Program_Membership__c WHERE hmr_Status__c = 'Active' AND hmr_Contact__c = :contactId LIMIT 1];
                Class__c newClassObject = [SELECT hmr_Coach__c FROM Class__c WHERE Id = :selectedClass];
                String [] dateArray = selectedClassFirstClassDate.split('-');
                String year = dateArray[0];
                String month = dateArray[1];
                String day = dateArray[2];
                Date classStartDate = Date.newinstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
                
                Class_Member__c classMemberObj = new Class_Member__c();
                classMemberObj.hmr_Class__c = selectedClass;
                classMemberObj.hmr_Contact_Name__c = contactId;
                classMemberObj.hmr_Active__c = true;
                classMemberObj.hmr_Start_Date__c = classStartDate;
                classMemberObj.hmr_Program_Membership__c = membership.Id;
                classMemberObj.hmr_Coach__c = newClassObject.hmr_Coach__c;
                insert classMemberObj;
    
                membership.hmr_Membership_Type__c = 'Phone Program';
                membership.hmr_Current_Health_Coach__c = newClassObject.hmr_Coach__c;
                update membership;

                responseMap.put('success', true);
            }
            else{
                responseMap.put('error', 'Please select a class to register');
                responseMap.put('success', false);
            }
        }
        catch(Exception Ex){
            responseMap.put('success', false);
            responseMap.put('error', ex.getMessage() + ex.getStackTraceString());
        }

        return responseMap;
    }

    public Class ClassRecord implements Comparable{
               
        public String id {get;set;}
        public String classId {get; set;}
        public String firstClassDate {get;set;}
        public String className {get;set;}
        public string coachFirstName {get; set;}
        public string coachLastName {get; set;}
        public String coachName {get{
            return this.coachFirstName + (this.hasDuplicateFirstName ? ' ' + this.coachLastName.left(1) : '');
        } set;}
        public Boolean doNotOffer {get;set;}
        public String classDate {get;set;}
        public String startTime {get;set;}
        public String classDay {get;set;}
        public Decimal clientEnrolled {get;set;}
        public Decimal classStrength {get;set;}
        public Boolean isEasternTime {get;set;}  
        public Boolean isAM {get{
            return this.startTime.containsIgnoreCase('AM');
        }}
        public Boolean hasDuplicateFirstName {get; set;}
        public integer militaryHour {get{
            String hour = this.startTime.substringBefore(':');

            if(this.startTime.contains('AM')){
                hour = hour + ' AM';
            }
            else{
                hour = hour + ' PM';
            }

            return getMilitaryHour.get(hour);
        }}
        public integer sortOrder{get{
            integer hour = Integer.valueOf(startTime.remove('AM').remove('PM').remove(':').trim());
            return hour * (isAM ? -1 : 1);
        }}

        public ClassRecord(Coaching_Session__c coachingSession, ContactDetails con, ProgramMembershipDetails membership){
        
            this.id = coachingSession.Id;
            this.classId = coachingSession.hmr_Class__c;
            this.firstClassDate = String.valueOf(coachingSession.hmr_Class__r.hmr_First_Class_Date__c);
            this.className = coachingSession.hmr_Class__r.hmr_Class_Name__c;
            this.coachFirstName = coachingSession.hmr_Class__r.hmr_Coach__r.hmr_Coach_First_Name__c;
            this.coachLastName = coachingSession.hmr_Class__r.hmr_Coach__r.hmr_Coach_Last_Name__c;
            this.doNotOffer = coachingSession.Do_Not_Offer__c;
            this.classDate = String.valueOf(coachingSession.hmr_Class_Date__c);
            this.startTime = coachingSession.hmr_Start_Time__c;
            this.classDay = DateTime.newInstance(coachingSession.hmr_Class_Date__c, Time.newInstance(0,0,0,0)).format('EEEE');
            this.clientEnrolled = coachingSession.hmr_Class__r.hmr_Client_Enrolled__c;
            this.classStrength = coachingSession.hmr_Class__r.hmr_Class_Strength__c;
            this.isEasternTime = coachingSession.isEasternTime__c;      
        }

        public Integer compareTo(Object compareTo){
            ClassRecord compareToClassRecord = (ClassRecord)compareTo;
            return compareToClassRecord.sortOrder < 0 && sortOrder < 0 ? (compareToClassRecord.sortOrder <= sortOrder ? -1 : 1) : 
                   compareToClassRecord.sortOrder <= sortOrder ? 1 : -1;      
        }
    }
    
    public Class ContactDetails{
    
        public String id {get;set;}
        public String firstName {get;set;}
        public String lastName {get;set;}
        public Boolean isMobileAppUser {get; set;}
        public Double timeZone {get;set;}
        public String timeZoneDisplay {get;set;}
        public String timeZoneAbbreviation{get{
            return this.timeZoneDisplay == 'Pacific' ? 'PST' :
                   this.timeZoneDisplay == 'Mountain' ? 'MST' : 
                   this.timeZoneDisplay == 'Central' ? 'CST' :
                   this.timeZoneDisplay == 'Eastern' ? 'EST' :
                   this.timeZoneDisplay == 'Hawaii' ? 'HAST' :
                   this.timeZoneDisplay == 'Alaska' ? 'AKST' : '';
        }}
        
        public ContactDetails(Contact con){
            if(con != null){
                this.id = con.id;
                this.firstName = con.FirstName;
                this.lastName = con.LastName;
                this.isMobileAppUser = con.MobileLogin__c;
                this.timeZone = con.Time_Zone_Offset__c; 
                this.timeZoneDisplay = con.Time_Zone__c;
            }
        }
        
    }
    
    public Class ProgramMembershipDetails{
    
        public String id {get;set;}
        public String programId {get;set;}
        public String programName {get;set;}
        
        public ProgramMembershipDetails(Program_Membership__c membership){
            if(membership != null){
                this.id = membership.id;
                this.programId = membership.Program__c;
                this.programName = membership.Program__r.Name.remove('P1 ');
            }
        }
        
    }
}