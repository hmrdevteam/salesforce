@isTest
private class cc_dataFactory_Test {
	
	static testMethod void test_method_one() {
		// Calling all cc_DataFactory Methods to make sure it covers independently.
		
		cc_dataFActory.setupTestUser();
		cc_dataFActory.setupCatalog();
		cc_dataFActory.setupTestUser1();
		cc_dataFActory.setupTestGuest();
		cc_dataFActory.createCart();
		
		ccrz__E_Cart__c testCart = [SELECT Id FROM ccrz__E_Cart__c LIMIT 1];
		ccrz__E_Product__c prod = [SELECT Id FROM ccrz__E_Product__c LIMIT 1];
		
		cc_dataFActory.addCartItem(testCart, prod.Id, 1, 10);
		cc_dataFActory.getContactAddress();
		cc_dataFActory.generateRandomString(32);
		cc_dataFActory.createExternalOrders(2);
		//cc_dataFActory.createOrders(2);
		cc_dataFActory.createRootCategories();
		
		String profilename = 'HMR Customer Community User';
		String alias = 'abcd';
		
		cc_dataFActory.reusableUser(profilename, alias);
		
	}
	
	static testMethod void testCreateOrders() {
        cc_dataFActory.createOrders(2);
	}
}