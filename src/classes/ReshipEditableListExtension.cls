/*
 * Extension of EditableList for Setting up Reships
 * @Date: 2017-3-19
 * @Author Nathan Anderson (Magnet 360)
 * @JIRA:
 *
 * Usage:
 *   This is the implementation class where Object specific references
 *   need to be defined. Please see EditableList.cls for more information
 *   on what each method is responsible for.
 *
 *   Required methods:
 *     - ReshipEditableListExtension(ApexPages.StandardController)
 *     - getChildren()
 *     - initChildRecord()
 *   
 *    TODO Enhancement: Reviewed 06/07/2017 by Z. Engman 
 *      - Change to use inner wrapper class for items with guid identifier to avoid upsert and delete of upserted unassigned records
 */ 
public class ReshipEditableListExtension extends EditableList {

    public String shipping {get;set;}
   
    public list<ccrz__E_OrderItem__c>items{get;set;}
    public string itemId{get;set;}
    public integer removepos{get;set;}
    public List<ccrz__E_OrderItem__c> itemsToDelete;

    public ReshipEditableListExtension(ApexPages.StandardController stdController) {

        super(stdController);
        this.itemsToDelete = new List<ccrz__E_OrderItem__c>();
        //Query for fields on the Child records for edit/view on page
        items = [SELECT Id, Name, ccrz__Product__r.ccrz__SKU__c, ccrz__Quantity__c, ccrz__Product__c, ccrz__Product__r.Name, ccrz__ProductType__c
                          FROM ccrz__E_OrderItem__c
                            WHERE ccrz__Order__c =: mysObject.Id AND ccrz__ProductType__c <> 'Coupon'];


        ccrz__E_Order__c parent = [SELECT Id, ccrz__ShipMethod__c FROM ccrz__E_Order__c WHERE Id = :mysObject.Id];

        shipping = parent.ccrz__ShipMethod__c;
    }

    public void removecon()
    {
        if(items.size()>0)
        {
            for(Integer i=0;i<items.size();i++)
            {
                if(items[i].id==itemId)
                {
                    removepos=i;
                    if(items[removepos] != null){
                        itemsToDelete.add(items[removepos]);
                    }                         
                }
            }                  
            items.remove(removepos);                            
        }
    }

    /*
    * This method is necessary for reference on the Visualforce page,
    * in order to reference non-standard fields.
    */
    public List<ccrz__E_OrderItem__c> getChildren() {
        return (List<ccrz__E_OrderItem__c>)childList;
    }

    public override sObject initChildRecord() {
        ccrz__E_OrderItem__c child = new ccrz__E_OrderItem__c();
        // Set values on Order Item to defaults
        child.ccrz__Order__c = mysObject.Id;
        child.ccrz__Price__c = 0;
        child.ccrz__SubAmount__c = 0;
        child.ccrz__OrderLineType__c = 'Major';
        child.ccrz__ProductType__c = 'Product';
        child.ccrz__ItemStatus__c = 'Available';
        child.ccrz__UnitOfMeasure__c = 'Each';
        child.ccrz__StoreId__c = 'DefaultStore';
        child.ccrz__RequestDate__c = System.Today();
        child.ccrz__AbsoluteDiscount__c = 0;
        child.ccrz__Quantity__c = 0;

        return child;
    }

    
  /*
   * Used to add a new row to the table
   */
  public void addToNewList()
  {
    items.add((ccrz__E_OrderItem__c)initChildRecord());
    upsert items;
  }


    //construct list of Shipping Methods
    public List<SelectOption> getShipMethods() {

        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', '--Select Shipping Method--'));
        options.add(new SelectOption('Standard', 'Standard'));
        options.add(new SelectOption('Expedited', 'Expedited'));

        return options;
    }

    //This method ensures the shipping method gets updated on the parent order,
    //then calls the save method from the EditableList class and returns the PageReference
    public PageReference saveShipMethod() {

        ccrz__E_Order__c parentOrder = [SELECT Id, ccrz__ShipMethod__c FROM ccrz__E_Order__c WHERE Id =:mysObject.Id];

        parentOrder.ccrz__ShipMethod__c = shipping;

        update parentOrder;
        return save();
    }

    //This method does everything saveShipMethod does, but also sets the Order Status to Submitted
    //this ensure the NYDC job will pick this up for shipment
    public PageReference saveSubmit() {

        ccrz__E_Order__c parentOrder = [SELECT Id, ccrz__ShipMethod__c FROM ccrz__E_Order__c WHERE Id =:mysObject.Id];

        parentOrder.ccrz__ShipMethod__c = shipping;
        parentOrder.ccrz__OrderStatus__c = 'Order Submitted';

        update parentOrder;
        return save();
    }

    public PageReference saveNewSubmit() {
        string orderURL;

        ccrz__E_Order__c parentOrder = [SELECT Id, ccrz__ShipMethod__c FROM ccrz__E_Order__c WHERE Id =:mysObject.Id];

        parentOrder.ccrz__ShipMethod__c = shipping;
        parentOrder.ccrz__OrderStatus__c = 'Order Submitted';

        update parentOrder;
        upsert items;
        delete itemsToDelete;

        list<ccrz__E_OrderItem__c> nullItems = [SELECT Id, Name, ccrz__Order__c, ccrz__Quantity__c 
                        FROM ccrz__E_OrderItem__c
                            WHERE ccrz__Order__c =: mysObject.Id AND ccrz__Quantity__c = 0];

        delete nullItems;

        list<ccrz__E_OrderItem__c> couponItems = [SELECT Id, Name, ccrz__Order__c, ccrz__Quantity__c, ccrz__ProductType__c 
                        FROM ccrz__E_OrderItem__c
                            WHERE ccrz__Order__c =: mysObject.Id AND ccrz__ProductType__c = 'Coupon'];

        delete couponItems;

        orderURL = '/'+parentOrder.Id;
        PageReference rsPage = new PageReference(orderURL);
        return rsPage;
    }

    public PageReference cancelBtn() {
        string orderURL2;

        ccrz__E_Order__c parentOrder2 = [SELECT Id, ccrz__ShipMethod__c FROM ccrz__E_Order__c WHERE Id =:mysObject.Id];

        list<ccrz__E_OrderItem__c> nullItems2 = [SELECT Id, Name, ccrz__Order__c, ccrz__Quantity__c 
                        FROM ccrz__E_OrderItem__c
                            WHERE ccrz__Order__c =: mysObject.Id AND ccrz__Quantity__c = 0];

        delete nullItems2;

        list<ccrz__E_OrderItem__c> couponItems2 = [SELECT Id, Name, ccrz__Order__c, ccrz__Quantity__c, ccrz__ProductType__c 
                        FROM ccrz__E_OrderItem__c
                            WHERE ccrz__Order__c =: mysObject.Id AND ccrz__ProductType__c = 'Coupon'];

        delete couponItems2;

        orderURL2 = '/'+parentOrder2.Id;
        PageReference rsPage2 = new PageReference(orderURL2);
        return rsPage2;
    }
}