/*
Developer: Rafa Hernandez (Magnet360)
Date: March 21st 2017
Log: Missing 10%, pending to render variable result from the Controller.
Target Class(ses): HMR_CMS_CC_BrandingController
*/
@isTest
private class HMR_CMS_CC_BrandingController_Test {
    private static String htmlContent = '<header class="col-xs-12" id="header">Test Information</header><footer id="footer">Footter Example Test</footer>';
    
    private static testmethod void testMethodForHeaderRendering() {
        // Implement test code
        HMR_CMS_CC_BrandingController brandingController = new HMR_CMS_CC_BrandingController();
        String calltheFooter =  brandingController.parseFooter('this is a footer test');
        String calltheHeaderHTML =  brandingController.parseHeader('<nav class="navbar navbar-default navbar-fixed-top hmr-header">Test Info </nav>');
        String calltheHeader =  brandingController.parseHeader('this is a header test');
        brandingController.htmlString = htmlContent;
        String getHeaderString = brandingController.getHeaderRendering();
        System.assertEquals(calltheHeaderHTML, '<nav class="navbar navbar-default navbar-fixed-top hmr-header">Test Info </nav>');

    }
    
    private static testmethod void testMethodForFooterRendering() {
    
        HMR_CMS_CC_BrandingController brandingController = new HMR_CMS_CC_BrandingController();
        String calltheFooter =  brandingController.parseFooter('this is a footer test');
        String calltheFooterHTML =  brandingController.parseFooter('<footer class="bg-typecover">Footter Example Test</footer>');
        String calltheHeader =  brandingController.parseHeader('this is a header test');
        brandingController.htmlString = htmlContent;
        String getFooterString = brandingController.getFooterRendering();
        
        System.assert(!String.isBlank(calltheFooterHTML));
    
    } 
    
}