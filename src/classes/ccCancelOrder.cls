/**
* Global class triggered by javascript pop-up on CC Order Object (Cancel Order Button)
* Cancel Order (drop from AD if necessary)
*
* Objects referenced --> ccrz__E_Order__c, Program_Membership__c
*
* @Date: 2017-1-5
* @Author Ali Pierre (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-1139
*/
Global class ccCancelOrder {

    //Declare list of FeedItem to store chatter posts to contact object
    Public static List<FeedItem> contactFeedPosts = new List<FeedItem>();
    /**
    *  cancelOrder, this method will proccess the order cancellation request
    *
    *  @param orderId --> Id of order record to be cancelled
    *  @return void
    */
    webService static String cancelOrder(Id orderId){
        //Declare String to store popup message to be returned
        String Msg = '';
        //Retrieve CC Order record using orderId in SOQL query
        ccrz__E_Order__c Order = [SELECT Id, Name, ccrz__OrderStatus__c, hmr_Order_Type_c__c, hmr_Program_Membership__c, ccrz__Contact__c   FROM ccrz__E_Order__c WHERE Id = :orderId LIMIT 1];
        //process cancellation request
        if(Order != null){
            //If proccess cancellation request according to Order Status
            if(Order.ccrz__OrderStatus__c == 'Completed' || Order.ccrz__OrderStatus__c == 'Shipped' || Order.ccrz__OrderStatus__c == 'Template' || Order.ccrz__OrderStatus__c == 'On Hold'){
                //Set alert Message to inform user cancellation cannot be completed due to order status
                Msg = String.valueOf('Order may not be cancelled due to order status: ' + Order.ccrz__OrderStatus__c);
            }
            else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
                //Set alert Message to inform user cancellation cannot be completed because order has already been Cancelled
                Msg = String.valueOf('Order has already been cancelled');
            }
            else{
                    //Create Chatter Post for Contact feed
                    postToClientFeed(Order.ccrz__Contact__c, 'Client CC Order number: ' + Order.Name + ' has been cancelled.');
                //Update Order Status to cancelled
                 Order.ccrz__OrderStatus__c = 'Cancelled';
                Try{
                    Update Order;
                    //Set alert Message to inform user cancellation has been completed
                    Msg = String.valueOf('Order number ' + Order.Name + ' has successfully been cancelled');


                }
                catch(Exception ex) {
                    System.debug('Exception in Order Update ' + ex.getMessage());
                }
                system.debug('!!' + Order.hmr_Order_Type_c__c + 'PM -->' + Order.hmr_Program_Membership__c);
                //If Order was a 1st order with an associated Program_Membership__c record, change Program_Membership__c record to dropped
                if((Order.hmr_Order_Type_c__c != null && Order.hmr_Order_Type_c__c.contains('1st Order')) && Order.hmr_Program_Membership__c != null){
                    List<Program_Membership__c> programMemberships = new List<Program_Membership__c>();
                    Program_Membership__c ProgMembership;
                    
                    programMemberships = [SELECT Id, Name, hmr_Status__c,hmr_Status_Change_Reason__c, hmr_Contact__c, ProgramName__c FROM Program_Membership__c WHERE Id = :Order.hmr_Program_Membership__c AND hmr_Status__c = 'Active' LIMIT 1];
                    
                    if(programMemberships.size() > 0){
                        ProgMembership = programMemberships[0];
                    }

                    //List<Consent_Agreement__c> caList = [SELECT Id, Consent_Type__c, Status__c, Contact__c FROM Consent_Agreement__c WHERE Contact__c = :ProgMembership.hmr_Contact__c];
                    //List<Contact> contactList = [SELECT Id, hmr_Class_Notes__c, hmr_New_Class_Note__c FROM Contact WHERE Id = :ProgMembership.hmr_Contact__c];

                    if(ProgMembership != null){
                        //Updating Program_Membership__c status will start Program_Membership__c trigger logic to handle class drops
                        //and Contact record updates
                        ProgMembership.New_Phase__c = 'Drop';
                        ProgMEmbership.hmr_Status_Change_Reason__c = 'Cancelled 1st Order';
                        Try{
                           Update ProgMembership;
                            //Set alert Message to inform user cancellation has been completed and Program Membership has been dropped
                            Msg = String.valueOf('Order number ' + Order.Name + ' has successfully been cancelled and client has been dropped from AD');

                            //Create Chatter Post for Contact feed
                            postToClientFeed(ProgMembership.hmr_Contact__c, ProgMembership.ProgramName__c + ' Program Membership ' + ProgMembership.Name + ' has been dropped due to cancelled 1st order');

                            // //If consent list is not empty
                            // //set their consent status to 'Expired' in the Consent_Agreement__c object
                            // List<Consent_Agreement__c> caListtoUpdate = new List<Consent_Agreement__c>();
                            // if(!caList.isEmpty()){
                            //     for(Consent_Agreement__c consentRecord: caList){
                            //         if(consentRecord.Contact__c == ProgMembership.hmr_Contact__c){
                            //             consentRecord.Status__c = 'Expired';
                            //             caListtoUpdate.add(consentRecord);
                            //         }
                            //         update caListtoUpdate;
                            //     }
                            // }
                            //
                            // //If contact list is not empty
                            // //Clear Class Notes field and New Class Notes field
                            // List<Contact> contactListtoUpdate = new List<Contact>();
                            // if(!contactList.isEmpty()){
                            //     for(Contact contactRecord: contactList){
                            //         if(contactRecord.Id == ProgMembership.hmr_Contact__c){
                            //             contactRecord.hmr_Class_Notes__c = '';
                            //             contactRecord.hmr_New_Class_Note__c = false;
                            //             contactListtoUpdate.add(contactRecord);
                            //         }
                            //         update contactListtoUpdate;
                            //     }
                            // }

                        }
                        catch(Exception ex) {
                           System.debug('Exception in Program Membership Update ' + ex.getMessage());
                        }
                    }

                }
                //Post to Chatter
                if(!contactFeedPosts.isEmpty()){
                 Try{

                           insert contactFeedPosts;
                       }
                       catch(Exception ex) {
                          System.debug('Exception in Chatter Posts Update ' + ex.getMessage());
                       }
                }


            }
        }
        else{
            //Set alert Message to inform user an error has occured
             Msg = String.valueOf('An error has occured during the cancellation of this order. Please contact System Administrator');
        }
        System.debug(msg);
        //Return Alert Message
        return Msg;
    }


    /**
    *  stopShipment, this method will proccess the stop shipment request
    *
    *  @param orderId --> Id of order record to be cancelled
    *  @return void
    */

    webService static String stopShipment(Id orderId){
        //Declare String to store popup message to be returned
        String Msg = '';
        //Retrieve CC Order record using orderId in SOQL query
        ccrz__E_Order__c Order = [SELECT Id, Name, ccrz__OrderStatus__c, hmr_Order_Type_c__c, hmr_Program_Membership__c, ccrz__Contact__c   FROM ccrz__E_Order__c WHERE Id = :orderId LIMIT 1];
        //process cancellation request
        if(Order != null){
            //If proccess cancellation request according to Order Status
            if(Order.ccrz__OrderStatus__c == 'Completed' || Order.ccrz__OrderStatus__c == 'Pending' || Order.ccrz__OrderStatus__c == 'Shipped' || Order.ccrz__OrderStatus__c == 'Template' || Order.ccrz__OrderStatus__c == 'On Hold' || Order.ccrz__OrderStatus__c == 'Open' || Order.ccrz__OrderStatus__c == 'Order Submitted' || Order.ccrz__OrderStatus__c == 'Cancel Submitted' || Order.ccrz__OrderStatus__c == 'Return Submitted' || Order.ccrz__OrderStatus__c == 'Returned' || Order.ccrz__OrderStatus__c == 'Partial Shipped' || Order.ccrz__OrderStatus__c == 'Bill Of Material'){
                //Set alert Message to inform user cancellation cannot be completed due to order status
                Msg = String.valueOf('Shipment may not be stopped due to order status: ' + Order.ccrz__OrderStatus__c);
            }
            else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
                //Set alert Message to inform user cancellation cannot be completed because order has already been Cancelled
                Msg = String.valueOf('Order has already been cancelled');
            }
            else{
                    //Create Chatter Post for Contact feed
                    postToClientFeed(Order.ccrz__Contact__c, 'Client CC Order number: ' + Order.Name + ' has been stopped.');
                //Update Order Status to Stopped Shipment
                 Order.ccrz__OrderStatus__c = 'Stopped Shipment';
                Try{
                    Update Order;
                    //Set alert Message to inform user cancellation has been completed
                    Msg = String.valueOf('Order number ' + Order.Name + ' has successfully been stopped');


                }
                catch(Exception ex) {
                    System.debug('Exception in Order Update ' + ex.getMessage());
                }
                //If Order was a 1st order with an associated Program_Membership__c record, change Program_Membership__c record to dropped
                if((Order.hmr_Order_Type_c__c != null && Order.hmr_Order_Type_c__c.contains('1st Order')) && Order.hmr_Program_Membership__c != null){
                    List<Program_Membership__c> programMemberships = new List<Program_Membership__c>();
                    Program_Membership__c ProgMembership;

                    programMemberships = [SELECT Id, Name, hmr_Status__c,hmr_Status_Change_Reason__c, hmr_Contact__c, ProgramName__c FROM Program_Membership__c WHERE Id = :Order.hmr_Program_Membership__c AND hmr_Status__c = 'Active' LIMIT 1];

                    if(programMemberships.size() > 0){
                        ProgMembership = programMemberships[0];
                    }
                    
                    if(ProgMembership != null){
                        //Updating Program_Membership__c status will start Program_Membership__c trigger logic to handle class drops
                        //and Contact record updates
                        ProgMembership.hmr_Status__c = 'Dropped';
                        ProgMEmbership.hmr_Status_Change_Reason__c = 'Cancelled 1st Order';
                        Try{
                           Update ProgMembership;
                            //Set alert Message to inform user cancellation has been completed and Program Membership has been dropped
                            Msg = String.valueOf('Order number ' + Order.Name + ' has successfully been stopped and client has been dropped from AD');

                            //Create Chatter Post for Contact feed
                            postToClientFeed(ProgMembership.hmr_Contact__c, ProgMembership.ProgramName__c + ' Program Membership ' + ProgMembership.Name + ' has been dropped due to stopped 1st order');

                        }
                        catch(Exception ex) {
                           System.debug('Exception in Program Membership Update ' + ex.getMessage());
                        }
                    }

                }
                //Post to Chatter
                if(!contactFeedPosts.isEmpty()){
                   Try{

                           insert contactFeedPosts;
                       }
                       catch(Exception ex) {
                          System.debug('Exception in Chatter Posts Update ' + ex.getMessage());
                       }
                }


            }
        }
        else{
            //Set alert Message to inform user an error has occured
             Msg = String.valueOf('An error has occured during the cancellation of this order. Please contact System Administrator');
        }
        System.debug(msg);
        //Return Alert Message
        return Msg;
    }



 /**
    *  postToClientFeed, this method is used to build text chatter posts that will be posted to the contact object
    *
    *  @param contactId - record Id of contact related to CC Order
    *  @param ccOrder - record Id of CC Order record
    *  @param msg - Text portion of Chatter post detailing cancellation
    *  @return void
    */

    Public static void postToClientFeed(Id contactId, String msg){
        FeedItem post = new FeedItem();
        //Add Contact object as parent id to post to contact detail feed
        post.ParentId = contactId;
        post.Body = msg;
        //Add to list of contact posts
        contactFeedPosts.add(post);
    }

}