/**
* Apex Content Template Controller for Featured Articles Section
*
* @Date: 07/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified:
* @JIRA: HPRP-3977
*/
global virtual class HMR_CMS_CategoryTopicBar_ContentTemplate extends cms.ContentTemplateController{
    global HMR_CMS_CategoryTopicBar_ContentTemplate(cms.createContentController cc) {
        super(cc);
    }
    global HMR_CMS_CategoryTopicBar_ContentTemplate() {}

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String FilterCategory{
        get{
            return getPropertyWithDefault('FilterCategory', 'DataCategory');
        }
    }

    public String SelectedCategory{
        get{
            return getPropertyWithDefault('SelectedCategory', 'None');
        }
    }

    global virtual override String getHTML(){
        String sitePrefix = Site.getBaseUrl();
        //Map<String, String> resourceMap = HMR_StaticResource_Utility.GetResourceURLMap(new Set<String> {'Community_RectangleIcon'});
        List<Category_Map__mdt> dataCategoryList = this.getCategoryList();

        string html =   '<div class="global-filter">' +
                            '<div class="container">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<ol class="filter text-center">';
                                            for(integer index = 0; index < 4; index++)
                                            {
                                                Category_Map__mdt categoryRecord = dataCategoryList[index];
                                                string urlName = (FilterCategory == 'DataCategory') ? 'resources/' + categoryRecord.Url_Resource_Name__c.replace('Questions-','').toLowerCase() : categoryRecord.Url_Resource_Name__c;
                                                if(SelectedCategory != 'None') {
                                                    html += '<li class="col-xs-12 col-sm-3 global-filter-category' + (categoryRecord.DeveloperName == SelectedCategory ? ' active' : ' hidden-sm hidden-xs') + '">';
                                                }
                                                else {
                                                    html += '<li class="col-xs-12 col-sm-3 global-filter-category">';
                                                }
                                                html += '<a class="category-link" href="' + sitePrefix + '/' + urlName + '" >' +
                                                            '<span>' +
                                                                '<img class="icon" src="' + categoryRecord.Icon1__c +'" />' +
                                                                '<div class="section hmr-allcaps hmr-allcaps-blue ">' +
                                                                    categoryRecord.MasterLabel +
                                                                '</div>' +
                                                            '</span>' +
                                                            '<img class="indicator" src="https://www.myhmrprogram.com/ContentMedia/Resources/RectangleIcon.png" />' +
                                                        '</a>' +
                                                    '</li>';
                                            }
                html +=                 '</ol>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';

        return html;
    }

    private List<Category_Map__mdt> getCategoryList(){
        List<Category_Map__mdt> dataCategoryList;
        HMR_DataCategory_Service dataCategoryService = new HMR_DataCategory_Service();

        if(this.FilterCategory == 'RecipeCategory')
            dataCategoryList = dataCategoryService.getRecipeDataCategoryList();
        else
            dataCategoryList = dataCategoryService.getDataCategoryList();

        return dataCategoryList;
    }

}