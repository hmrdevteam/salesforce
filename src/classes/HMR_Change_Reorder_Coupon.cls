/**
* Apex Controller for the HMR_Change_Reorder_Coupon.page
* 	:	Give PS the ability to change coupon for reorders when clients join following the wrong flow
* @Date: 12/11/2017
* @Author Jay Zincone (Health Management Resources)
* @Modified: 
* @JIRA:
*/
public class HMR_Change_Reorder_Coupon {
	//public variables
	public String cId {get;set;}
	public ccrz__E_Order__c order {get;set;}
	public Id orderId {get;set;}

	public HMR_Change_Reorder_Coupon(ApexPages.StandardController stdController) {
		ccrz__E_Coupon__c coupon = new ccrz__E_Coupon__c();
		//get the order id from the URL
		orderId = apexpages.currentpage().getparameters().get('id');
		
		//get the full record
		order = [SELECT Id, Name, ccrz__Contact__c, ccrz__User__c, ccrz__BillTo__c, ccrz__ShipTo__c, ccrz__TotalAmount__c,
		 				ccrz__Contact__r.FirstName, ccrz__Contact__r.LastName, ccrz__Contact__r.Client_Id__c, hmr_Order_Type_c__c, ccrz__Contact__r.Account.Name,
		 				ccrz__Contact__r.Account.RecordType.Name
						FROM ccrz__E_Order__c WHERE Id = :orderId];
	}

	public List<SelectOption> getCoupons() {

		List<SelectOption> options = new List<SelectOption>();
		string ordType = '';
		string acctType = '';
		//Check Account Type
		if(order.ccrz__Contact__r.Account.RecordType.Name == 'HMR Customer Account'){
			//Get Coupons with no Account
			acctType = '';
		}else{
			//Get Coupons with specific Account
			acctType = order.ccrz__Contact__r.Account.Name;
		}
		//Check Order Type to determine which coupons to show
		if (order.hmr_Order_Type_c__c == 'P1 1st Order') ordType = 'P1 1st Order';
		else if (order.hmr_Order_Type_c__c == 'P1-HSS 1st Order') ordType = 'HSS 1st Order';
		else if (order.hmr_Order_Type_c__c == 'P1 Reorder') ordType = 'P1 Re-Order';
		else if (order.hmr_Order_Type_c__c == 'P1-HSS Reorder') ordType = 'HSS Re-Order';
		else if (order.hmr_Order_Type_c__c == 'P2 1st Order') ordType = 'P2';
		else if (order.hmr_Order_Type_c__c == 'P2 Reorder') ordType = 'P2 Re-order';
		else ordType = 'null';

		List<ccrz__E_Coupon__c> coups = [SELECT Id, ccrz__CouponName__c FROM ccrz__E_Coupon__c WHERE Coupon_Order_Type__c =:ordType AND HMR_Coupon__r.Account__r.Name =:acctType AND ccrz__Enabled__c = True];
		for(ccrz__E_Coupon__c c : coups) {
			options.add(new SelectOption(c.Id, c.ccrz__CouponName__c));
		}

		return options;
	}
	
	public PageReference updateOrder(){
		ccrz__E_Coupon__c selCoup = new ccrz__E_Coupon__c();
		decimal discountTotal = 0;
		if(cId != null){
			try {
				selCoup = [SELECT Id, Name, ccrz__CouponType__c, ccrz__DiscountAmount__c FROM ccrz__E_Coupon__c WHERE Id = :cId];
			}
			catch(Exception e) {

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a valid Coupon: ' + e.getMessage()));
			}
			}else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a valid Coupon'));	
				return null;
			}

		if(selCoup != null) {
			//Populate ccrz__Order__r.Name value in WHERE clause with Order Name
			List<ccrz__E_OrderItem__c> orderItems = new List<ccrz__E_OrderItem__c>([Select Id, Name, ccrz__Quantity__c, ccrz__SubAmount__c, ccrz__Order__c, 
                                            ccrz__AbsoluteDiscount__c,ccrz__PercentDiscount__c, ccrz__Coupon__c, ccrz__Price__c 
                                            FROM ccrz__E_OrderItem__c WHERE ccrz__Order__r.Id = :orderId]);
			System.debug(orderItems);
			System.debug(orderItems.size());
			System.debug(selCoup);
			for(ccrz__E_OrderItem__c ordItem : orderItems){
			    Decimal lineTotal = ordItem.ccrz__Price__c * ordItem.ccrz__Quantity__c;
			    system.debug(lineTotal);
			    Decimal lineDiscount = lineTotal * ((selCoup.ccrz__DiscountAmount__c / 100));
			    system.debug(lineDiscount);
			    lineDiscount = lineDiscount.setScale(2, RoundingMode.HALF_UP);
			    ordItem.ccrz__AbsoluteDiscount__c = lineDiscount;
			    discountTotal = discountTotal + lineDiscount;
			    ordItem.ccrz__SubAmount__c = lineTotal - lineDiscount;
			    ordItem.ccrz__PercentDiscount__c = selCoup.ccrz__DiscountAmount__c;
			    ordItem.ccrz__Coupon__c = selCoup.Id;
			}
			System.debug(orderItems);
			//Run DML updates on Items
			try{
				update orderItems;
			} catch(Exception e){
				ApexPages.addMessages(e);
	        }

	        //Update Order
	        List<ccrz__E_Order__c> orders = new List<ccrz__E_Order__c>([Select Id,  ccrz__TotalDiscount__c, Order_Value_Discount_Amount__c FROM ccrz__E_Order__c WHERE Id = :orderId]);
			for(ccrz__E_Order__c ord : orders){
			    ord.ccrz__TotalDiscount__c = discountTotal;
			    ord.Order_Value_Discount_Amount__c = discountTotal;
			}
			System.debug(orders);
			//Run DML updates on Items
			try{
				update orders;
			} catch(Exception e){
				ApexPages.addMessages(e);
	        }

			//return to Order page after complete
			PageReference orderPage = new PageReference('/'+order.Id);
			return orderPage;

		} else {
			//Throw an error if the update button is hit before an address is selected
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an coupon.'));
			return null;
		}
	}

	public PageReference cancel(){
        PageReference orderPage = new PageReference('/'+order.Id);
        return orderPage;
    }
}