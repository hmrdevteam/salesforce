/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_RegistrationServiceInterface
*/

@isTest(SeeAllData=true)
private class HMR_CMS_RegServiceInterface_Test{

  @isTest static void firstMethod() {
    // Implement test code
    HMR_CMS_RegistrationServiceInterface registrationController = new HMR_CMS_RegistrationServiceInterface();
    
    Map<String, String> params = new Map<String, String>();
    params.put('ec','123459');
    params.put('e','testuser@gma.com');
    params.put('p','Passtest!!22');
    params.put('cp','Passtest!!22');
    params.put('n','testU');
    params.put('f','testfirst');
    params.put('l','testUser');
    params.put('t','/redirectHome');
    params.put('tp','/redirectPlan');
    params.put('ep','test');
    params.put('z','123459');
   
    
    Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
    User u = new User(Alias = 'standt', Email='stan@torg.com', IsActive = TRUE,
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = profileId.id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='stanus@test.com');
                
    insert u;  

    
    cc_dataFActory.setupTestUser();
    User thisUser = [Select Id,contactId FROM User WHERE Id = :UserInfo.getUserId()];
    Id contactId = [ select Id from contact limit 1 ].id;  
    
    ccrz__E_AccountGroup__c accGrp = new ccrz__E_AccountGroup__c(Name = 'PortalAccount');
    insert accGrp;
    
    ccrz__E_AccountGroup__c accgrpp = [SELECT ID, Name FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount' LIMIT 1];
        
    Account testAcc = new Account(name = 'testAcc',hmr_Employer_Account_ID__c='123459');
    insert testAcc;
    
    
    Lead leadObj = new Lead(Status = 'New',Account__c = testAcc.id, firstName = 'Test', LastName = 'TestLead', hmr_Lead_Type__c = 'Clinic', Company = 'Test Company',Email = 'testuser@gma.com');
    insert leadObj;
    
    String resp = registrationController.executeRequest(params);
    System.assert(!String.isBlank(resp));

    //patch method for testing
    Lead leadObj1 = new Lead(Status = 'New',firstName = 'Test', LastName = 'TestLead', hmr_Lead_Type__c = 'Clinic', Company = 'Test Company',Email = 'testuser@gma.com');
    insert leadObj1;
    list<Lead> leadList = new list<Lead>();
    leadList.add(leadObj1);
    registrationController.ConvertExistingLead(leadList, testAcc.Id, new Map<String, Object>());

    User portalUser = [Select Id,contactId,UserName FROM User WHERE contactId != null limit 1];
    ccrz__E_Cart__c cartTemp = [Select Id from ccrz__E_Cart__c limit 1];
    registrationController.userId = portalUser.Id;
    registrationController.registerUser('String empCode', portalUser.UserName, 'Stringpassword', 'username@usn.com', 'Stringpassword', 'username@usn.com', 'firstname', 'lastname', '/Home', '', 'false', '60616', cartTemp.Id);
    registrationController.registerUser('String empCode', portalUser.UserName, 'Stringpassword', 'username@usn.com', 'Stringpassword', 'username@usn.com', 'firstname', 'lastname', '/Welcome', '', 'false', '60616', cartTemp.Id);
    registrationController.registerUser('String empCode', portalUser.UserName, 'Stringpassword', 'username@usn.com', 'Stringpassword', 'username@usn.com', 'firstname', 'lastname', 'checkout', '', 'false', '60616', cartTemp.Id);
    registrationController.registerUser('String empCode', portalUser.UserName, 'Stringpassword', 'username@usn.com', 'Stringpassword', 'username@usn.com', 'firstname', 'lastname', 'DefaultStore/', '', 'false', '60616', cartTemp.Id);

    PageReference p = Page.SiteRegister;
    Cookie cartId = new Cookie('currCartId','3312312312312',null,-1,false);
    list<Cookie> cList = new list<Cookie>();
    cList.add(cartId);
    p.setCookies(cList);
    Test.setCurrentPage(p );  
    HMR_CMS_RegistrationServiceInterface.transferGuestCart(portalUser.UserName);

    map<string, Object> pMap = new map<string, Object>();
    pMap.put('exceptionMsg','exceptionMsg');
    registrationController.trimResponseMessage(pMap);


  }

  @isTest static void secondMethod() { 
      
        HMR_CMS_RegistrationServiceInterface registrationController = new HMR_CMS_RegistrationServiceInterface();
    
        Map<String, String> params = new Map<String, String>();
        params.put('ec','');
        params.put('e','test112233@gmclkr.com');
        params.put('p','Passte!22');
        params.put('cp','Passte!22');
        params.put('n','trrrm');
        params.put('f','testsecend');
        params.put('l','testUser111');
        params.put('t','/Home');
        params.put('tp','/Plan');
        params.put('ep','test');
        params.put('z','');

       
     //   insert userTest;
     
        cc_dataFActory.setupTestUser();
        User thisUser = [Select Id,Email,contactId FROM User WHERE Id = :UserInfo.getUserId()];
   
       System.runAs(thisUser ) { 
       ccrz__E_AccountGroup__c accGrp = new ccrz__E_AccountGroup__c(Name = 'PortalAccount');
        insert accGrp; 
       
        Account testAcc = new Account(name = 'testAcc1234');
        insert testAcc;
        
        
        Zip_Code_Assignment__c zcAssing = new Zip_Code_Assignment__c(Name='11223',Account__c = testAcc.id );
        insert zcAssing;
        
            try{
                String resp = registrationController.executeRequest(params);
                params.put('z','11223');
                params.put('e','test1322278@gmclkr.com');
                params.put('p','Pasqst!22');
                params.put('cp','Pasqst!22');
                params.put('n','ttyyu');
                params.put('l','trtestseccond12322');
                String resp2 = registrationController.executeRequest(params);

                params.put('e',thisUser.Email);
                resp2 = registrationController.executeRequest(params);
                HMR_CMS_RegistrationServiceInterface.assingPermissionSet(thisUser.Id);
            }
            catch(Exception ex){
            
                System.debug(ex.getMessage());
                
            }
        }
     
  }

  @isTest static void thirdMethod() { 
      
        HMR_CMS_RegistrationServiceInterface registrationController = new HMR_CMS_RegistrationServiceInterface();
    
        Map<String, String> params = new Map<String, String>();
        params.put('ec','123459');
        params.put('e','test@gmcl.com');
        params.put('p','Passtest!!22');
        params.put('cp','PassTe!2');
        params.put('n','tetU');
        params.put('f','testthird');
        params.put('l','testUser');
        params.put('t','/redirectHome');
        params.put('tp','/redirectPlan');
        params.put('ep','test');
        params.put('z','');
        
       ccrz__E_AccountGroup__c accGrp = new ccrz__E_AccountGroup__c(Name = 'PortalAccount');
        insert accGrp;
        Account testAcc = new Account(name = 'testAcc',hmr_Employer_Account_ID__c='123459');
        insert testAcc;
               
        String resp = registrationController.executeRequest(params);
        
        System.assert(!String.isBlank(resp));
    
  }
 
   @isTest static void fourthMethod() { 
      
        HMR_CMS_RegistrationServiceInterface registrationController = new HMR_CMS_RegistrationServiceInterface();
    
        Map<String, String> params; 
        
        String resp = registrationController.executeRequest(params);  
       
        System.assert(!String.isBlank(resp));
      
  } 
  
  @isTest static void fifthMethod() { 
        
        Type resp = HMR_CMS_RegistrationServiceInterface.getType();  
       
        System.assertEquals(resp, HMR_CMS_RegistrationServiceInterface.class);
      
  } 
}