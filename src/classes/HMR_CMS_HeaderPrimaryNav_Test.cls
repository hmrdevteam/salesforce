/*
Developer: Mustafa Ahmed (HMR)
Date: March 22nd 2017
Target Class: HMR_CMS_HeaderPrimaryNav_ContentTemplate
*/

@isTest
public class HMR_CMS_HeaderPrimaryNav_Test {
	@isTest static void test_general_method() {
		// Implement test code
		HMR_CMS_HeaderPrimaryNav_ContentTemplate HeaderPrimaryNavTemplate = new HMR_CMS_HeaderPrimaryNav_ContentTemplate();			
                String propertyValue = HeaderPrimaryNavTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');		
                String PlansText = HeaderPrimaryNavTemplate.PlansText;                
                cms.Link PlansLinkObj = HeaderPrimaryNavTemplate.PlansLinkObj;                
                String RecipeText = HeaderPrimaryNavTemplate.RecipeText;                
                cms.Link RecipeLinkObj = HeaderPrimaryNavTemplate.RecipeLinkObj;                
                String ResourcesText = HeaderPrimaryNavTemplate.ResourcesText;                
                cms.Link ResourcesLinkObj = HeaderPrimaryNavTemplate.ResourcesLinkObj;
                String renderHMTL = HeaderPrimaryNavTemplate.getHTML(); 
        		System.Assert(renderHMTL != null); 
	}
}