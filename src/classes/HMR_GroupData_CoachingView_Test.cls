/*****************************************************
 * Author: Utkarsh Goswami    
 * Created Date: 03/21/2018
 * Created By: Utkarsh Goswami    
 * Last Modified Date: 
 * Last Modified By:
 * Description: Test Coverage for the HMR_GroupData_CoachingView_Controller Class
 * ****************************************************/
@isTest
private class HMR_GroupData_CoachingView_Test {
    
    public static String coachingSessId;
    
    @testSetup
    private static void setupTestData(){
        User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        
        Program__c programRecord = HMR_TestFactory.createProgram();
        insert programRecord;
        
        Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
        insert programMembershipRecord;
        
        //Create coach
        User coachUserRecord = HMR_TestFactory.createUser('HMR Standard User', true);
        insert coachUserRecord;
        
        Coach__c coachRecord = HMR_TestFactory.createCoach(coachUserRecord.Id);
        insert coachRecord;
        
        //Setup the group/class and sessions
        Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
        insert classRecord;
        
        Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
        insert classMemberRecord;
        
        Coaching_Session__c coachingSessionRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
        coachingSessionRecord.hmr_Class_Date__c = Date.today().addDays(-7);
        insert coachingSessionRecord;
        
        coachingSessId = coachingSessionRecord.Id;
        
        //Add future coaching session
        Coaching_Session__c coachingSessionFutureRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
        insert coachingSessionFutureRecord;
        
        Session_Attendee__c sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord.Id, classMemberRecord.Id, contactRecord.Id);
        sessionAttendeeRecord.hmr_Attendance__c = 'Attended';
        insert sessionAttendeeRecord;         
         
     //   Session_Attendee__c sessionAttendeeRecord2 = HMR_TestFactory.createSessionAttendee(coachingSessionRecord.Id, classMemberRecord.Id, contactRecord.Id);
      //  sessionAttendeeRecord2.hmr_Attendance__c = 'No Show';
      //  insert sessionAttendeeRecord2; 
        
        //Add future attendee record
        Session_Attendee__c sessionFutureAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionFutureRecord.Id, classMemberRecord.Id, contactRecord.Id);
        insert sessionFutureAttendeeRecord;
        
        //Create the handout records
        List<Handouts__c> handoutList = HMR_TestFactory.createHandoutList(programRecord.Id, 3);
        insert handoutList;
        
        List<Attachment> attachmentList = new List<Attachment>();
        for(Handouts__c handoutRecord : handoutList){
            Attachment attachmentRecord = new Attachment(Name='Unit Test Attachment',
                                                      body = Blob.valueOf('Unit Test Attachment Body'),
                                                      parentId = handoutRecord.Id);  
            
            attachmentList.add(attachmentRecord);
        }
        insert attachmentList;
        
        //Create activity log records
        List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
        
        for(integer i = 0; i < 7; i++){
            ActivityLog__c activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);
            
            activityLogRecord.DateDataEnteredFor__c = Date.today().addDays(i * -1);
            activityLogRecord.Group_Member__c = classMemberRecord.Id;
            activityLogRecord.Session_Attendee__c = sessionAttendeeRecord.Id;
            
            activityLogList.add(activityLogRecord);
        }
        
        insert activityLogList;
    }
    
    @isTest
    public static void sessionChangedTest(){
    
        System.debug('TEST CLASSS ->  '+ coachingSessId);
        Coaching_Session__c coachingSessionRecord = [SELECT Id FROM Coaching_Session__c ORDER BY hmr_Class_Date__c LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_GroupData_CoachView')); 
        System.currentPageReference().getParameters().put('id', coachingSessionRecord.Id);
        
        HMR_GroupData_CoachingView_Controller controller = new HMR_GroupData_CoachingView_Controller();
        PageReference resp = controller.sessionChanged();
    
    }
    
    
    @isTest
    public static void saveTest(){
    
        Coaching_Session__c coachingSessionRecord = [SELECT Id FROM Coaching_Session__c ORDER BY hmr_Class_Date__c LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_GroupData_CoachView')); 
        System.currentPageReference().getParameters().put('id', coachingSessionRecord.Id);
        
        HMR_GroupData_CoachingView_Controller controller = new HMR_GroupData_CoachingView_Controller();
        PageReference resp = controller.save();
    
    } /*
    @isTest
    public static void saveMakeupScheduledTest(){
    
        Coaching_Session__c coachingSessionRecord = [SELECT Id FROM Coaching_Session__c ORDER BY hmr_Class_Date__c LIMIT 1];
        
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        
        Program__c programRecord = HMR_TestFactory.createProgram();
        insert programRecord;
        
        Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
        insert programMembershipRecord;
        
        User coachUserRecord = HMR_TestFactory.createUser('HMR Standard User', true);
        insert coachUserRecord;
        
        Coach__c coachRecord = HMR_TestFactory.createCoach(coachUserRecord.Id);
        insert coachRecord;
        
         Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
        insert classRecord;
        
        Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
        insert classMemberRecord;

        
        Session_Attendee__c sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord.Id, classMemberRecord.Id, contactRecord.Id);
        sessionAttendeeRecord.hmr_Attendance__c = 'No Show';
        insert sessionAttendeeRecord;
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CMS_GroupData_CoachView')); 
        System.currentPageReference().getParameters().put('id', coachingSessionRecord.Id);
        
        HMR_GroupData_CoachingView_Controller controller = new HMR_GroupData_CoachingView_Controller();
        PageReference resp = controller.save();
    
    } */
}