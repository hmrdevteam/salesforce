/**
* CloudCraze Shipping and Handling API Extension
*
* @Date: 2017-03-12
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA:
*/
global with sharing class HMR_CC_ShippingAndHandling_Extn extends ccrz.cc_api_ShippingAndHandling{
    private static final string employeeRateType = 'Standard (EE)';
    private static final string standardRateType = 'Standard';
    private static final string expeditedRateType = 'Expedited';
    
    private static final Map<String, String> standardStatesList = new Map<String, String>{
        'AL'=> 'AL',
        'AZ'=> 'AZ',
        'AR'=> 'AR',
        'CA'=> 'CA',
        'CO'=> 'CO',
        'CT'=> 'CT',
        'DE'=> 'DE',
        'DC'=> 'DC',
        'FL'=> 'FL',
        'GA'=> 'GA',
        'ID'=> 'ID',
        'IL'=> 'IL',
        'IN'=> 'IN',
        'IA'=> 'IA',
        'KS'=> 'KS',
        'KY'=> 'KY',
        'LA'=> 'LA',
        'ME'=> 'ME',
        'MD'=> 'MD',
        'MA'=> 'MA',
        'MI'=> 'MI',
        'MN'=> 'MN',
        'MS'=> 'MS',
        'MO'=> 'MO',
        'MT'=> 'MT',
        'NE'=> 'NE',
        'NV'=> 'NV',
        'NH'=> 'NH',
        'NJ'=> 'NJ',
        'NM'=> 'NM',
        'NY'=> 'NY',
        'NC'=> 'NC',
        'ND'=> 'ND',
        'OH'=> 'OH',
        'OK'=> 'OK',
        'OR'=> 'OR',
        'PA'=> 'PA',
        'RI'=> 'RI',
        'SC'=> 'SC',
        'SD'=> 'SD',
        'TN'=> 'TN',
        'TX'=> 'TX',
        'UT'=> 'UT',
        'VT'=> 'VT',
        'VA'=> 'VA',
        'WA'=> 'WA',
        'WV'=> 'WV',
        'WI'=> 'WI',
        'WY'=> 'WY',
        null => null,
        '' => null,
        ' ' => null
    };
   
    public static void insertShippingOptions(List<Shipping_Rates__c> shippingRatesList, List<ccrz.cc_ctrl_hlpr_ShippingOption> shippingOptions, Boolean freeShipping) {
        if(shippingRatesList != null){
          for(Shipping_Rates__c shippingRates: shippingRatesList) {
                ccrz.cc_ctrl_hlpr_ShippingOption shippingOption = new ccrz.cc_ctrl_hlpr_ShippingOption();
                shippingOption.provider = 'HMR';
                shippingOption.serviceName = shippingRates.Shipping_Type__c;
                if(freeShipping){
                    shippingOption.price = 0.00;
                    shippingOption.discountedShipCost = 0.00;
                }
                else{
                    shippingOption.price = shippingRates.Shipping_Cost__c;
                    shippingOption.discountedShipCost = shippingRates.Shipping_Cost__c;
                }
                shippingOption.uniqueId = shippingRates.Shipping_Type__c;
                shippingOption.currencyCode = 'USD';
                shippingOption.discount = 0;
                shippingOptions.add(shippingOption);
            }
        }
    }

    global override List<ccrz.cc_ctrl_hlpr_ShippingOption> getShippingOptions(String zipCode, String stateCode, String countryCode, Id cartId, String storeName) {
        Date dt = Date.today();
        List<ccrz.cc_ctrl_hlpr_ShippingOption> shippingOptions = new List<ccrz.cc_ctrl_hlpr_ShippingOption>();

        ccrz__E_Cart__c cartDetails = [SELECT ccrz__Account__c, ccrz__Account__r.Name, ccrz__ActiveCart__c, ccrz__CartStatus__c, ccrz__CartType__c, ccrz__Contact__c, ccrz__Contact__r.hmr_Employee_Type__c, 
                                            ccrz__CurrencyISOCode__c, ccrz__ShipAmount__c, ccrz__SubtotalAmount__c, ccrz__TaxAmount__c, ccrz__TotalAmount__c, ccrz__TotalDiscount__c,
                                            ccrz__TotalQuantity__c, ccrz__TotalSurcharge__c, ccrz__User__c FROM ccrz__E_Cart__c WHERE Id = :cartId];


        Boolean freeShipping = false;
        Set<Id> kitIds = new Set<Id>();
        //Id used to store coupon apply to this cart if applicable
        Id ccCouponId = null;
        //Boolean used to indicate whether client is a program member or not
        Boolean programMember = false;

        Id contactId = null;
        //Get ContactId from cartDetails
        if(cartDetails.ccrz__Contact__c != null){
            contactId = cartDetails.ccrz__Contact__c;
        }

        //Declare a List to store Program Memberships
        List<Program_Membership__c> progMembership = new List<Program_Membership__c>();
        //If ContactId is filled, check if client has an active Program_Membership__c record
        if(contactId != null){
            progMembership = [SELECT Id, Name, Program__c FROM Program_Membership__c
                    WHERE  hmr_Status__c = 'Active' AND hmr_First_Order_Number__c <> null AND hmr_Contact__c = :contactId];
        }
        //If an active Program_Membership__c is found related to this client, set programMember = true
        if(!progMembership.isEmpty()){
            programMember = true;
        }

        //Retrieve items in cart
        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>(
                [SELECT Id, Name, ccrz__Product__c, ccrz__Cart__c, ccrz__Product__r.ccrz__ProductType__c, ccrz__Coupon__c, ccrz__Coupon__r.HMR_Coupon__c FROM ccrz__E_CartItem__c
                    WHERE ccrz__Cart__c = :cartId]);
        //If cart contains items, loop through items
        if(!cartItems.isEmpty()){
            for(ccrz__E_CartItem__c cItem : cartItems){
                //If kit is found, add Product Id to kitIds set
                if(cItem.ccrz__Product__r.ccrz__ProductType__c == 'Dynamic Kit'){
                    kitIds.add(cItem.ccrz__Product__c);
                }
                //If coupon is found, assign ccrz__Coupon__c value to ccCouponId
                if(cItem.ccrz__Coupon__c != null && ccCouponId == null){
                    ccCouponId = cItem.ccrz__Coupon__c;
                }
            }
            //Build a query using the kitId set to check if any kit within the cart is related to a program
            List<ccrz__E_Product__c> dynamicKitsforAD = new List<ccrz__E_Product__c>(
                [SELECT Id, Name, Program__c FROM ccrz__E_Product__c WHERE Id IN :kitIds AND Program__c <> null]);
            //If kit is related to a program and this client is not an existing program member, this order is eligible for free standard shipping
            if(!dynamicKitsforAD.isEmpty() && programMember == false){
                freeShipping = true;
            }
            //If cart does not contain kit or client is an exisiting program member
            //Set freeShipping = True if a CC Coupon or its parent coupon(HMR Coupon) has free shipping
            else{
                if(ccCouponId != null){
                    List<ccrz__E_Coupon__c> freeShipCCCoupons = new List<ccrz__E_Coupon__c>(
                    [SELECT Id, Name, ccrz__CartTotalAmount__c, HMR_Coupon__c, HMR_Coupon__r.hmr_Free_Shipping__c FROM ccrz__E_Coupon__c
                    WHERE Id = :ccCouponId AND (HMR_Coupon__r.hmr_Free_Shipping__c = true OR hmr_Free_Shipping__c = true)]);

                    if(!freeShipCCCoupons.isEmpty()){
                        freeShipping = true;
                    }
                }
            }
        }
        
        Map<String, List<Shipping_Rates__c>> shippingRateMap = new Map<String, List<Shipping_Rates__c>>();
        Set<String> rateTypeSet = new Set<String>{employeeRateType, standardRateType, expeditedRateType};
        //Determine the total amount to use in shipping calculation - it should not include the shipping rate
        decimal totalCartAmount = cartDetails.ccrz__TotalAmount__c;
        if(cartDetails.ccrz__ShipAmount__c != null && cartDetails.ccrz__ShipAmount__c > 0)
            totalCartAmount = totalCartAmount - cartDetails.ccrz__ShipAmount__c;
		
        
        for(Shipping_Rates__c shippingRateRecord :  [SELECT Active__c, 
                                                     		Cart_Total_High__c, 
                                                     		Cart_Total_Low__c, 
                                                     		Shipping_Cost__c, 
                                                     	    Shipping_Type__c 
                                                     FROM Shipping_Rates__c
                									 WHERE Cart_Total_Low__c <= :totalCartAmount AND
                   										   Cart_Total_High__c >= :totalCartAmount AND
                                                     	   Active__c = TRUE AND Shipping_Type__c IN: rateTypeSet])
        {
			if(!shippingRateMap.containsKey(shippingRateRecord.Shipping_Type__c))
                shippingRateMap.put(shippingRateRecord.Shipping_Type__c, new List<Shipping_Rates__c>{shippingRateRecord});
            else{
                List<Shipping_Rates__c> shippingRateList = shippingRateMap.get(shippingRateRecord.Shipping_Type__c);
                shippingRateList.add(shippingRateRecord);
                shippingRateMap.put(shippingRateRecord.Shipping_Type__c, shippingRateList);
            }
        }

        if(cartDetails.ccrz__Account__r.Name == 'HMR Employees' && cartDetails.ccrz__Contact__r.hmr_Employee_Type__c == 'Employee') {
            insertShippingOptions(shippingRateMap.get(employeeRateType), shippingOptions, freeShipping);
            insertShippingOptions(shippingRateMap.get(expeditedRateType), shippingOptions, false);
        }
        else {
            if(standardStatesList.get(stateCode) == null) {                
                insertShippingOptions(shippingRateMap.get(expeditedRateType), shippingOptions, false);
            }
            else {
                insertShippingOptions(shippingRateMap.get(standardRateType), shippingOptions, freeShipping);
                insertShippingOptions(shippingRateMap.get(expeditedRateType), shippingOptions, false);
            }
        }

        return shippingOptions;
    }
}