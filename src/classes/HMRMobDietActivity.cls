/**
* Apex Rest Resource for Diet Activity
* @Date: 2018-01-11
* @Author: Utkarsh Goswami (Magnet 360)
* @Modified: Zach Engman 2018-02-15 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/activity/dietData/*')  
global with sharing class HMRMobDietActivity  {
    
    /**
     * insert the Activity sObject
     * @param MobDietActivityDTO
     * @see MobDietActivityDTO
     * @return MobDietActivityDTO - the DTO containing the diet activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 500 Internal Server Error
     */
    @HttpPost
    global static void doPost(MobDietActivityDTO dietActivityLog) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();

        try{
            ActivityLog__c activityLog = dietActivityLog.mapToActivityLog();
            //Check for existing record
            List<ActivityLog__c> activityLogList = [SELECT Id 
                                                    FROM ActivityLog__c 
                                                    WHERE Program_Membership__c =: activityLog.Program_Membership__c
                                                    AND DateDataEnteredFor__c =: activityLog.DateDataEnteredFor__c];
            if(activityLogList.size() == 0){
                insert activityLog;

                response.statusCode = 201;

                resultMap.put('success', true);
                resultMap.put('id', activityLog.Id);
                resultMap.put('dietActivityLog', new MobDietActivityDTO(activityLog));
            }
            else{
                response.statusCode = 405;

                resultMap.put('success', false);
                resultMap.put('error', 'Activity record already created for date specified');
            }
        }
        catch(Exception ex){
            response.statusCode = 500;

            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * update the Activity sObject
     * @param MobDietActivityDTO
     * @see MobDietActivityDTO
     */
    @HttpPatch
    global static void updateDietActivity(MobDietActivityDTO dietActivityLog) { 
        Map<String, Object> resultMap = new Map<String, Object>();
        RestResponse response = RestContext.response;

        try{
            ActivityLog__c activityLogRecord = dietActivityLog.mapToActivityLog();
            
            //Get the latest version of the record, get all necessary fields to send back in case of mismatch
            List<ActivityLog__c> serverVersionList = [SELECT Contact__c
                                                          ,Program_Membership__c
                                                          ,Program_Membership__r.Program__r.Name
                                                          ,DateDataEnteredFor__c
                                                          ,Diet_Type__c
                                                          ,Diet_Version__c
                                                          ,Entrees__c
                                                          ,ShakesCereals__c
                                                          ,InTheBox__c
                                                          ,FruitsVegetables__c
                                                          ,TotalPhysicalActivityCaloriesRollUp__c
                                                          ,Water__c
                                                          ,BenefitBars__c 
                                                      FROM ActivityLog__c 
                                                      WHERE Id =: activityLogRecord.Id];

            if(serverVersionList.size() > 0){
                if(activityLogRecord.Diet_Version__c == null || serverVersionList[0].Diet_Version__c == null || serverVersionList[0].Diet_Version__c == activityLogRecord.Diet_Version__c){
                    response.statuscode = 200;

                    activityLogRecord.Diet_Version__c = activityLogRecord.Diet_Version__c == null ? 0 : activityLogRecord.Diet_Version__c + 1;
                    
                    update activityLogRecord;

                    resultMap.put('success', true);
                    resultMap.put('dietActivityLog', new MobDietActivityDTO(activityLogRecord));
                }
                else{
                    response.statuscode = 405;

                    resultMap.put('success', false);
                    resultMap.put('error', 'record version mismatch');
                    resultMap.put('dietActivityLog', new MobDietActivityDTO(serverVersionList[0]));
                } 
            }
            else{
                response.statuscode = 405;

                resultMap.put('success', false);
                resultMap.put('error', 'record has been deleted on the server');
            }  
        }
        catch(Exception ex){
            response.statuscode = 404;
            
            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * method to get the diet activity record for the day
     * @param contactId - the related contact record id 
     * @param activityDate - the related date to log the activity for
     * @return MobDietActivityDTO - the DTO containing the diet activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if error is encountered
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        Map<String, String> paramMap = RestContext.request.params;
        
        if(paramMap.get('contactId') != null && paramMap.get('activityDate') != null){
            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
            String contactId = paramMap.get('contactId');
            Date activityDate = Date.valueOf(paramMap.get('activityDate'));
            Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(contactId, activityDate);
            MobDietActivityDTO dietActivityDTO;
            //verify a program membership record existed for the date
            if(programMembershipRecord != null){
                List<ActivityLog__c> activityLogList = [SELECT Contact__c
                                                              ,Program_Membership__c
                                                              ,Program_Membership__r.Program__r.Name
                                                              ,DateDataEnteredFor__c
                                                              ,Diet_Type__c
                                                              ,Diet_Version__c
                                                              ,Entrees__c
                                                              ,ShakesCereals__c
                                                              ,InTheBox__c
                                                              ,FruitsVegetables__c
                                                              ,TotalPhysicalActivityCaloriesRollUp__c
                                                              ,Water__c
                                                              ,BenefitBars__c 
                                                         FROM ActivityLog__c 
                                                         WHERE Contact__c =: contactId
                                                          AND Program_Membership__c =: programMembershipRecord.Id
                                                          AND DateDataEnteredFor__c =: activityDate];

                RestContext.response.statuscode = 200;
                                                         
                if(activityLogList.size() > 0){
                    dietActivityDTO = getDietActivity(activityLogList[0]); 
                }
                else{
                    dietActivityDTO = new MobDietActivityDTO(contactId, programMembershipRecord.Id, activityDate);
                }

                returnMap.put('success', true);
                returnMap.put('dietActivityLog', dietActivityDTO);
                returnMap.put('dietType', programMembershipRecord.DietType__c);
                returnMap.put('programName', programMembershipRecord.Program__r.Name);
            }
            else{
                RestContext.response.statuscode = 404;

                returnMap.put('success', false);
                returnMap.put('error', Label.Invalid_Activity_Date);
            }
        }
        else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'Parameters: contactId and activityDate are required');
        }
        
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  

    }

    global static MobDietActivityDTO getDietActivity(ActivityLog__c activityLog) {
         MobDietActivityDTO mobileDTO = new MobDietActivityDTO(activityLog);

         return mobileDTO; 
     }

     /**
     * method to delete the activity record - TODO - what if weight is recorded
     * @param id - the record id 
     * @return HTTP/1.1 200 success
     * @return HTTP/1.1 404 not found if error is encountered
     */
    @HttpDelete
    global static void deleteActivity() {
        Map<String, Object> returnMap = new Map<String, Object>();
        RestRequest request = RestContext.request;
        String recordId = request.params.get('id');
        
        try{
            delete new ActivityLog__c(Id = recordId);

            RestContext.response.statusCode = 200;

            returnMap.put('success', true);
        }
        catch(Exception ex){
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
            returnMap.put('error', ex.getMessage());
        }

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnMap)); 
    }     
    
    /**
     * global DTO class that represents a flat structure of the 
     * diet activity with related key fields from ActivityLog__c
     */
    global class MobDietActivityDTO extends HMRActivityDTO{
        public Integer entrees {get;set;}
        public Integer shakesCereals {get;set;}
        public Integer fruitsVegetable {get;set;}
        public Integer totalPhysicalActivityCaloriesRollUp {get;set;}
        public Integer water {get;set;}
        public Integer benefitBars {get;set;}
        public Boolean stayedOnDiet {get;set;}

        //used for tracker configuration at point in time
        public string dietType {get;set;}
        public string programName {get;set;}

        /**
         * constructors
         */
        public MobDietActivityDTO(String contactId, String programMembershipId, Date activityDate){
            this.contactId = contactId;
            this.programMembershipId = programMembershipId;
            this.activityDate = activityDate;
        }
        public MobDietActivityDTO(ActivityLog__c ac){
            this.recordId = ac.Id;
            this.recordVersion = ac.Diet_Version__c == null ? 0 : (Integer)ac.Diet_Version__c;
            this.contactId = ac.Contact__c;
            this.programMembershipId = ac.Program_Membership__c;
            this.activityDate = ac.DateDataEnteredFor__c;
            this.entrees = (Integer)ac.Entrees__c;
            this.shakesCereals = (Integer)ac.ShakesCereals__c;
            this.fruitsVegetable = (Integer)ac.FruitsVegetables__c;
            this.totalPhysicalActivityCaloriesRollUp = (Integer)ac.TotalPhysicalActivityCaloriesRollUp__c;
            this.water = (Integer)ac.Water__c;
            this.benefitBars = (Integer)ac.BenefitBars__c;
            this.stayedOnDiet = ac.InTheBox__c;

            this.dietType = ac.Diet_Type__c;
            this.programName = ac.Program_Membership__r.Program__r.Name;
        }

        public ActivityLog__c mapToActivityLog() {
            if (this.stayedOnDiet == null) {this.stayedOnDiet=false;}
            
            ActivityLog__c activityLogRecord = new ActivityLog__c(
                Id = this.recordId
               ,Diet_Version__c = this.recordVersion
               ,Contact__c = this.contactId
               ,Program_Membership__c = this.programMembershipId
               ,DateDataEnteredFor__c = this.activityDate
               ,Entrees__c = this.entrees
               ,ShakesCereals__c = this.shakesCereals
               ,FruitsVegetables__c = this.fruitsVegetable 
               ,Water__c = this.water
               ,BenefitBars__c = this.benefitBars
               ,InTheBox__c = this.stayedOnDiet
            );
            
            return activityLogRecord;
        }
        
    }
        
}