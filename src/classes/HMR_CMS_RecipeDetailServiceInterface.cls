/**
* Apex Content Template Controller for Recipe Details
*
* @Date: 03/20/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Zach Engman 08/10/2017
* @JIRA:
*/
global with sharing class HMR_CMS_RecipeDetailServiceInterface implements cms.ServiceInterface {
    /**
     *
     * @param params a map of parameters including at minimum a value for 'action'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {
        map<String, Object> returnMe = new map<string, Object>();

        try {
            String action = params.get('action');
            if(action == 'getRecipeDetail') {
                returnMe = recipeDetail(params);
            }
            else{
                returnMe.put('success',false);
                returnMe.put('message','Invalid Action');
            }
        }
        catch(Exception e) {
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
        }

        // No actions matched and no error occurred
        return JSON.serialize(returnMe);
    }

    public map<String, Object> recipeDetail(Map<String, String> params){
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        String urlName = params.get('urlName');
        Map<String, Object> returnMe = new Map<string, Object>();
        String returnJSON;

        if(String.isBlank(urlName)){
            returnMe.put('success',false);
            returnMe.put('message','Please Enter urlName');
        }
        else
        {
            try{
                if(urlName != null){
                    HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(urlName);

                    if(articleRecord == null){
                        returnMe.put('success',false);
                        returnMe.put('message','set is empty' + urlName + ' ');
                    }
                    else
                    {
                        string totalTime = '';

                        if(articleRecord.TotalHours != null && articleRecord.TotalHours != 0)
                            totalTime += Integer.valueOf(articleRecord.TotalHours)+ 'HOUR ';

                        if(articleRecord.TotalMinutes != null )
                            totalTime += Integer.valueOf(articleRecord.TotalMinutes)+ 'MIN ';

                        returnMe.put('r_time', totalTime);

                        returnMe.put('r_rating', articleRecord.Rating);

                        if(articleRecord.FruitVegetableServings != null)
                            returnMe.put('r_vServe', articleRecord.FruitVegetableServings);

                        if(articleRecord.FruitServings != null)
                            returnMe.put('r_fServe', articleRecord.FruitServings);

                        if(articleRecord.Calories != null)
                            returnMe.put('r_calories', articleRecord.Calories);

                        if(!String.isBlank(articleRecord.Title))
                            returnMe.put('r_title', articleRecord.Title);

                        if(!String.isBlank(articleRecord.HeroImage))
                            returnMe.put('r_hero', articleRecord.HeroImage);

                        if(!String.isBlank(articleRecord.Summary))
                            returnMe.put('r_summary', articleRecord.Summary);

                        if(!String.isBlank(articleRecord.LastPublishedDate))
                            returnMe.put('r_date', articleRecord.LastPublishedDate);

                        if(!String.isBlank(articleRecord.Ingredients)){
                            string ingredientsList = '';
                            for(string s : articleRecord.Ingredients.split('<br>')){
                                if(s != '' && s != null){
                                    ingredientsList += '<li>'+s.replace('null','')+'</li>';
                                }
                            }

                            returnMe.put('r_ingredients', ingredientsList);
                        }

                        if(!String.isBlank(articleRecord.Instructions)){
                            string instructionList = '';
                            for(string s : articleRecord.Instructions.split('<br>')){
                                if(s != '' && s != null){
                                    instructionList += '<li>'+s+'</li>';
                                }
                            }

                            returnMe.put('r_instructions', instructionList);
                        }

                        returnMe.put('success',true);
                        returnMe.put('message','set data');
                    }
                }
            }
            catch(Exception e){
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMe.put('success',false);
                returnMe.put('message', JSON.serialize(message));
            }
        }

        return returnMe;
    }

    public static Type getType() {
        return HMR_CMS_RecipeDetailServiceInterface.class;
    }
}