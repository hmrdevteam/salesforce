/**
* Apex Controller to Grab Order Data
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2017-05-11
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-05-11
* @JIRA: 
*/
global with sharing class HMR_CC_OrderData_Controller {
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getExtendedOrderData(final ccrz.cc_RemoteActionContext ctx, string encryptedId){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult response = new ccrz.cc_RemoteActionResult();
        
        Map<String, ccrz__E_Order__c> dataMap = new Map<String, ccrz__E_Order__c>();
        ccrz__E_Order__c orderRecord = HMR_CC_Order_Selector.getByEncryptedId(encryptedId);
        
        if(orderRecord != null)
        	dataMap.put(orderRecord.Id, orderRecord);
        
        response.inputContext = ctx;
        response.success = orderRecord != null;
        response.data = orderRecord;
        
        return response;
    }
}