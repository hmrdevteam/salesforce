@isTest(SeeAllData=true)
private class HMR_QuestionAnswer_ServiceInterface_Test
{
	@isTest
	static void itShould()
	{
		HMR_ConnectApi_Service ser = new HMR_ConnectApi_Service();
		ser.communityId = null;
		ConnectApi.FeedElement fe = ser.postQuestionAndAnswer('string subject', 'string body');

		map<string,string> params = new map<string,string>();
		params.put('action','getAnswers');
		params.put('p_feedId',fe.Id);
		params.put('p_offset','0');
		params.put('p_limit','5');
		params.put('p_userId',UserInfo.getUserId());
		system.debug('params++++'+params);
		HMR_CMS_QuestionAnswer_ServiceInterface si= new HMR_CMS_QuestionAnswer_ServiceInterface();
		si.executeRequest(params);

		params.put('action','postComment');
		params.put('p_replyText','Awesome feed');
		si.executeRequest(params);
		ConnectApi.Comment comment= ConnectApi.ChatterFeeds.postCommentToFeedElement(null, fe.Id, 'Good Reply 2');

		params.put('action','likeFeed');
		params.put('p_likeItemId',fe.Id);
		si.executeRequest(params);

		params.put('action','unlikeFeed');
		params.put('p_likeItemId',fe.Id);
		si.executeRequest(params);

		params.put('action','likeComment');
		params.put('p_likeItemId',comment.Id);
		si.executeRequest(params);

		params.put('action','unlikeComment');
		params.put('p_likeItemId',comment.Id);
		si.executeRequest(params);

		params.put('action','setTheBestAnswer');
		params.put('p_bestAnswerId',comment.Id);
		si.executeRequest(params);

		params.put('action','updateComment');
		params.put('p_updatedTargetId',comment.Id);
		params.put('p_updatedText','abcdefg');
		si.executeRequest(params);

		params.put('action','updateFeedElement');
		params.put('p_updatedTargetId',fe.Id);
		params.put('p_updatedText','abcdefg');
		params.put('p_updatedSubject','abcdefg');
		si.executeRequest(params);

		params.put('action','updateFeedElement');
		params.put('p_updatedTargetId',fe.Id);
		params.put('p_updatedText','sadadasd');
		params.put('p_hmrSuggested_old','false');
		params.put('p_hmrSuggested_new','true');
		si.executeRequest(params);

		params.put('action','deleteComment');
		params.put('p_deleteTargetId',comment.Id);
		si.executeRequest(params);

		params.put('action','deleteElement');
		params.put('p_deleteTargetId',fe.Id);
		si.executeRequest(params);

		params.put('action','post');
		params.put('topicId',null);
		params.put('subject','subsubsub');
		params.put('p_topicValue','subsubsub');
		params.put('body','subsubsub');
		si.executeRequest(params);

		List<KnowledgeArticleVersion> articleVersionList = [SELECT Id,UrlName,KnowledgeArticleId FROM KnowledgeArticleVersion WHERE PublishStatus = 'Online' AND Language = 'en_US' limit 1];
		string aName = '';
		if(articleVersionList.size()>0){
			aName = articleVersionList.get(0).UrlName;
		}
       	system.debug('++++++++++0000+++'+aName);

		params.put('action','postToArticle');
		params.put('p_articleId',articleVersionList[0].KnowledgeArticleId);
		params.put('p_replyText','articleVersionList[0].KnowledgeArticleId');
		si.executeRequest(params);

		params.put('action','updateCommentForArticle');
		params.put('p_updatedTargetId',articleVersionList[0].KnowledgeArticleId);
		params.put('p_replyText','articleVersionList[0].KnowledgeArticleId');
		si.executeRequest(params);


		ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testItemList.add(new ConnectApi.FeedItem());
        testPage.elements = testItemList;
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Record, 'aName', testPage);


		params.put('action','getArticleComments');
		params.put('p_articleName','aName');
		si.executeRequest(params);
        //ConnectApi.FeedElementPage fpage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(null, ConnectApi.FeedType.Record, 'aName');
    	//List<ConnectApi.FeedElement> fl = new List<ConnectApi.FeedElement>();

		//fpage.elements;

		//KnowledgeArticleVersion kav = [SELECT Id,UrlName,KnowledgeArticleId FROM KnowledgeArticleVersion WHERE UrlName = 'aName' And PublishStatus = 'Online' AND Language = 'en_US' limit 1];
		//string aName = '';
		HMR_CMS_QuestionAnswer_ServiceInterface.FeedElementRecord acac = new HMR_CMS_QuestionAnswer_ServiceInterface.FeedElementRecord(fe);

	}
}