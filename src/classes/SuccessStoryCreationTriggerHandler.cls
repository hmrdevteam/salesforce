public class SuccessStoryCreationTriggerHandler {

    public static void handleAfterUpdate(List<hmr_Success_Story__c> approvedStories, Map<Id, hmr_Success_Story__c> oldStoriesMap) {

        //Approval_Status__c is Approved
        // handler logic
        Set<Id> storyIds = new Set<Id>();
        System.debug(approvedStories.size());
        System.debug(approvedStories);

        // getting and storing the record Id where approval status is Approved

        System.debug('Trigger Size  ---> ' + approvedStories.size());
        for(hmr_Success_Story__c approvedStry: approvedStories) {
            System.debug('Initial Status ---> ' + approvedStry.Approval_Status__c );
            System.debug('Initial Status test Body ---> ' + approvedStry.Story_Body__c );
            if(approvedStry.Approval_Status__c == 'Approved') {
                storyIds.add(approvedStry.Id);
            }
        }

       System.debug('Story Ids ---> ' + storyIds );

        // Map of Success story records where approval Status is approved
        Map<Id, hmr_Success_Story__c> approvedStoryMap = new Map<Id, hmr_Success_Story__c>(
            [SELECT Id, Display_Name__c, Title__c, Pounds_Lost__c, Short_Teaser__c, Long_Teaser__c, Story_Body__c, Favorite_HMR_Products__c, Submitted_By__c,
                    Before_Photo_URL__c, After_Photo_URL__c, Alternate_Display_Name__c
                FROM hmr_Success_Story__c WHERE Id In :storyIds]
          ); // Display_Name__c need to check this field - ut

        Set<Id> approvedStoryIds = new Set<Id>();

        for(hmr_Success_Story__c approvedStry: approvedStoryMap.values()) {
            System.debug(approvedStry.Display_Name__c);
            approvedStoryIds.add(approvedStry.Id);
        }

        System.debug('Approved Map  ->  ' + approvedStoryMap);

        //instantiate list of Articles to insert
        List<Success_Story__kav> storyArticlesToInsertUpdate = new List<Success_Story__kav>();
        List<Id> storyKAVIds = new List<Id>();
        List<String> storyKAVArticleNumbers = new List<String>();

        //Retrieve map of already Published Success Story Articles that match the HMR Success Story objects in the trigger
        Map<Id, Success_Story__kav> storyKAVMap = new Map<Id, Success_Story__kav>(
          [SELECT Id, hmr_Success_Story__c, ArticleNumber, KnowledgeArticleId, Language, PublishStatus, Before_Photo__c, After_Photo__c, Alternate_Display_Name__c  FROM Success_Story__kav
            WHERE hmr_Success_Story__c IN :storyIds AND PublishStatus = 'Online' AND Language = 'en_US']);

        System.debug('storyKAVMap Size');
        System.debug('Size ->   '  + storyKAVMap.values().size());

        //Check the map to see if the story in the loop has an existing published Article
        //if there are none in the list, insert fresh Knowledge Articles
        if(storyKAVMap.values().size() == 0) {
            //Loop through appoved Stories and create new Story Article Versions
            for(hmr_Success_Story__c approvedStry: approvedStoryMap.values()) {
                //new Article record
                Success_Story__kav storyArticle = new Success_Story__kav();

                //map values from Custom object to Knowledge Article
                storyArticle.hmr_Success_Story__c = approvedStry.Id;
                storyArticle.Title = approvedStry.Title__c;
                //storyArticle.Pounds_Lost__c = approvedStry.Pounds_Lost__c;
                storyArticle.Favorite_HMR_Products__c = approvedStry.Favorite_HMR_Products__c;
                storyArticle.Short_Teaser__c = approvedStry.Short_Teaser__c;
                storyArticle.Long_Teaser__c = approvedStry.Long_Teaser__c;
                storyArticle.Story_Body__c = approvedStry.Story_Body__c;
                storyArticle.URLName = approvedStry.Title__c.replace(' ', '-');
                storyArticle.Submitted_By__c = approvedStry.Submitted_By__c;
                storyArticle.Before_Photo__c = approvedStry.Before_Photo_URL__c;
                storyArticle.After_Photo__c = approvedStry.After_Photo_URL__c;
                storyArticle.Alternate_Display_Name__c= approvedStry.Alternate_Display_Name__c;


                //Add Articles to list to Insert
                storyArticlesToInsertUpdate.add(storyArticle);
            }
        }

        //If there are existing Articles that match the HMR Success Story, call the PublishingService class
        //to create a new version of a published Article
        //then map the updated values from the HMR Success Story object and prepare this version for updating
        else {
            System.debug('IN ELSE');
            String storyKAVId = null;
            if(storyKAVMap.values().size() > 0) {
                    System.debug('IN IF STORY KV MAP  -> ' + storyKAVMap.values().size());
                    for(Success_Story__kav storyKAV: storyKAVMap.values()) {

                        try {
                        storyKAVId = KbManagement.PublishingService.editOnlineArticle(storyKAV.KnowledgeArticleId, true);
                        } catch(exception e) {

                        }
                        if(storyKAVId == null) {
                            System.debug('Story not published yet!');
                        }
                        storyKAVArticleNumbers.add(storyKAV.ArticleNumber);
                    }
                }

                //Retrieve the newly create draft Article Version
                Map<Id, Success_Story__kav> draftStryKAVMap = new Map<Id, Success_Story__kav>(
                        [SELECT Id, hmr_Success_Story__c, KnowledgeArticleId, ArticleNumber, Language, PublishStatus, Before_Photo__c, After_Photo__c, Alternate_Display_Name__c  FROM Success_Story__kav
                          WHERE hmr_Success_Story__c IN :storyIds AND PublishStatus = 'Draft' AND Language = 'en_US']);

                //Loop through Stories and prepare data
                if(draftStryKAVMap.values().size() > 0) {
                    for(Success_Story__kav draftStoryKAV: draftStryKAVMap.values()) {
                        for(hmr_Success_Story__c approvedStry: approvedStoryMap.values()) {

                            if(approvedStry.Id == draftStoryKAV.hmr_Success_Story__c ) {
                                  draftStoryKAV.Title = approvedStry.Title__c;
                                  //recipeArticle.Pounds_Lost__c = approvedStry.Pounds_Lost__c;
                                  draftStoryKAV.Favorite_HMR_Products__c = approvedStry.Favorite_HMR_Products__c;
                                  draftStoryKAV.Short_Teaser__c = approvedStry.Short_Teaser__c;
                                  draftStoryKAV.Long_Teaser__c = approvedStry.Long_Teaser__c;
                                  draftStoryKAV.Story_Body__c = approvedStry.Story_Body__c;
                                  draftStoryKAV.Before_Photo__c = approvedStry.Before_Photo_URL__c;
                                  draftStoryKAV.After_Photo__c = approvedStry.After_Photo_URL__c;
                                  draftStoryKAV.Alternate_Display_Name__c= approvedStry.Alternate_Display_Name__c;
                                  //draftStoryKAV.URLName = approvedStry.Title__c.replace(' ', '-');

                                  //Add Articles to list to Update
                                  storyArticlesToInsertUpdate.add(draftStoryKAV);
                            }
                        }
                    }
                }
            }

        try {

            if(!storyArticlesToInsertUpdate.isEmpty()) {
            //DML operation to insert Knowledge articles
                upsert storyArticlesToInsertUpdate;
            }

            System.debug('Upserted ->  ' + storyArticlesToInsertUpdate);

        }

        catch(Exception ex) {
            System.debug('@@@@@@@@@@');
            System.debug(ex.getMessage());
            System.debug('@@@@@@@@@@');
        }
    }
}