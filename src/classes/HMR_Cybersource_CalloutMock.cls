/**
* Http Callout Mock for Testing of the Cybersource Utility
*
* @Date: 2017-06-20
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman (Magnet 360)
* @JIRA:
*/

@isTest
global class HMR_Cybersource_CalloutMock implements HttpCalloutMock {
    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeadersMap;

    public HMR_Cybersource_CalloutMock(Integer code, String status, String body, Map<String, String> responseHeadersMap) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeadersMap = responseHeadersMap;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse response = new HttpResponse();

        for (String key : this.responseHeadersMap.keySet()) 
            response.setHeader(key, this.responseHeadersMap.get(key));
        
        response.setBody(this.body);
        response.setStatusCode(this.code);
        response.setStatus(this.status);
        return response;
    }

}