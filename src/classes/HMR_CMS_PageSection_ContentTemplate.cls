/**
* Apex Content Template Controller for Page Sections on Home Page
*
* @Date: 03/17/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Joey Zhuang 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_PageSection_ContentTemplate extends cms.ContentTemplateController{
    
    global HMR_CMS_PageSection_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
    
    global HMR_CMS_PageSection_ContentTemplate(){
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }
   

    
    public String title{
    
        get{
            return getProperty('title');
        }
    
    }
    
    public String titleClass{
    
        get{
            return getProperty('titleClass');
        }
    
    }
    
    public String sectionId{
    
        get{
            return getProperty('sectionId');
        }
    
    }
    
    public String description{
    
        get{
            return getProperty('description');
        }
    
    }
    
    public String desktopImageUrl{
        get{
            return getProperty('desktopImageUrl');
        }
    }
    
    public String mobileImageUrl{
        get{
            return getProperty('mobileImageUrl');
        }
    }
    
    public String position{
        get{
            return getProperty('position');
        }
    }
    
    public String linkOrButtonText{
        get{
            return getProperty('linkOrButtonText');
        }
    }

    public String linkOrButtonClass{
        get{
            return getProperty('linkOrButtonClass');
        }
    }

     public String linkOrButton{
        get{
            return getProperty('linkOrButton');
        }
    }
    public cms.Link ButtonLinkObj{
        get{
            return this.getPropertyLink('ButtonLinkObj');
        }
    }    
    
    
    public String backgroundColor{
        get{
            return getProperty('backgroundColor');
        }
    }
    
    public String fontColor{
        get{
            return getProperty('fontColor');
        }
    }
    
    String sitePrefix = Site.getBaseUrl();
    


    //override tha method
    global virtual override String getHTML(){
            //system.Debug('mobileImageUrl = ' + mobileImageUrl);
        String html = '';
        //if defined special title class
        string tClass = (titleClass != null && titleClass != '' )?titleClass:'section-header';
        //if image position is left, then add col-md-push-6 class to text on right.
        string push6 = (position == 'Left')?' col-md-push-6':'';
        string pull6 = (position == 'Left')?' col-md-pull-6':'';
        //if action is button, give button class to link
        string buttonStyle = (linkOrButton == 'Button')?' btn btn-primary section-button':'';

        html += '<section id="'+sectionId+'" class="subsection"> ';
            html += '<div class="container">';
                html += '<div class="row">';
                    html += '<div class=" col-md-6'+push6+' center-content ">';
                        html += '<h3 class="'+tClass+'">' + title +'</h3>';
                            html += '<p class="section-text">' + description + '</p>';
                            
                            //if action is button, give a div wrap outside of the link 
                            if(linkOrButton == 'Button'){
                                html += '<div class="text-center">';
                            }

                            html += '<a id="'+ sectionId + '_btn" class="'+buttonStyle;
                            if(linkOrButtonClass!= null && linkOrButtonClass != ''){
                                html +=' '+linkOrButtonClass;
                            }
                            html += '" href="';

                            if(this.ButtonLinkObj == null){
                                html += '#"';
                            }  
                            else{                       
                                if(this.ButtonLinkObj.targetPage != null){
                                    html += ButtonLinkObj.targetPage;
                                    html += '"';
                                }
                                if(this.ButtonLinkObj.target != null && this.ButtonLinkObj.target != ''){
                                    html += ' target="'+this.ButtonLinkObj.target+'"';
                                }
                                if(this.ButtonLinkObj.javascript != null && this.ButtonLinkObj.javascript != ''){
                                    html += ' onclick=\''+this.ButtonLinkObj.javascript+'\'';
                                }  
                            }                          
                            html += '>';
                            html += linkOrButtonText+'</a>';  // this.ButtonLinkObj.getLinkName()

                            if(linkOrButton == 'Button'){
                                html += '</div>';
                            }      
                                
                        html += '</div>';
                    html += '<div class=" col-md-6'+pull6+' text-center ">';

                        //if only one image uploaded, do not apply dkt-img and mbl-img class
                        boolean onlyOneImage = (desktopImageUrl == '' || desktopImageUrl == null || mobileImageUrl == '' || mobileImageUrl == null );

                        //generetat the descktop image
                        if(desktopImageUrl != '' && desktopImageUrl != null ){
                            html += '<img src="'+ sitePrefix + desktopImageUrl +'" class="';
                            if(!onlyOneImage){
                                html += 'dkt-img ';
                            }
                            html += ' img-responsive"/>';
                        }
                        //generate the mobile image
                        if(mobileImageUrl != '' && mobileImageUrl != null ){
                            html += '<img src="'+ sitePrefix + mobileImageUrl +'" class="';
                            if(!onlyOneImage){
                                html += 'mbl-img ';
                            }
                            html += ' img-responsive"/>';
                        }            
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</section>';
        
        return html;
  
    }
    
}