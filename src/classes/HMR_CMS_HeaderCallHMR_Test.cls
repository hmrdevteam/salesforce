/*
Developer: Rafa Hernandez (Magnet360)
Date: March 21st 2017
Log: 
Target Class(ses): HMR_CMS_HeaderCallHMR_ContentTemplate
*/

@isTest
private class HMR_CMS_HeaderCallHMR_Test {
	 private static String title = 'my_article_title';
    private static String summary = 'my_article_summary';
	@isTest static void test_method_global() {
		// Implement test code
		HMR_CMS_HeaderCallHMR_ContentTemplate headerCall = new HMR_CMS_HeaderCallHMR_ContentTemplate();
		String propertyValue = headerCall.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
		headerCall.testAttributes = new Map<String, String> {
            'title' => title,
            'summary' => summary
        };
        String callLabel = headerCall.CallHMRLabel;
        String callNumber = headerCall.CallHMRNumber;

        String navSelect = headerCall.NavSelect;

        String renderHMTL = headerCall.getHTML();
        renderHMTL = headerCall.getHTML();
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}