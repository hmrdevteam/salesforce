/**
* Test Class Coverage of the HMR_CMS_CardList_ContentTemplate
*
* @Date: 07/31/2017
* @Author Utkarsh Goswami (Mindtree) / Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_CardList_ContentTemplate_Test{
    @testSetup
    private static void setTestData(){
        Recipe__kav leftRightRecipeArticle01 = new Recipe__kav(Title='Test LeftRight Recipe 01'
                                                              ,UrlName='test-leftright-recipe-01'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav leftRightRecipeArticle02 = new Recipe__kav(Title='Test LeftRight Recipe 02'
                                                              ,UrlName='test-leftright-recipe-02'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav leftRightRecipeArticle03 = new Recipe__kav(Title='Test LeftRight Recipe 03'
                                                              ,UrlName='test-leftright-recipe-03'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav topBottomRecipeArticle01 = new Recipe__kav(Title='Test TopBottom Recipe 01'
                                                              ,UrlName='test-topbottom-recipe-01'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle02 = new Recipe__kav(Title='Test TopBottom Recipe 02'
                                                              ,UrlName='test-topbottom-recipe-02'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle03 = new Recipe__kav(Title='Test TopBottom Recipe 03'
                                                              ,UrlName='test-topbottom-recipe-03'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle04 = new Recipe__kav(Title='Test TopBottom Recipe 04'
                                                              ,UrlName='test-topbottom-recipe-04'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle05 = new Recipe__kav(Title='Test TopBottom Recipe 05'
                                                              ,UrlName='test-topbottom-recipe-05'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Blog_Post__kav blogArticle01 = new Blog_Post__kav(Title='Test Blog 01'
                                                         ,UrlName='test-blog-01'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 1'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'LeftRight');
        Blog_Post__kav blogArticle02 = new Blog_Post__kav(Title='Test Blog 02'
                                                         ,UrlName='test-blog-02'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 2'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'TopBottom');
        Blog_Post__kav blogArticle03 = new Blog_Post__kav(Title='Test Blog 03'
                                                         ,UrlName='test-blog-03'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 3'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'TopBottom');


        List<Recipe__kav> recipeArticleList = new List<Recipe__kav>{leftRightRecipeArticle01
                                                                   ,leftRightRecipeArticle02
                                                                   ,leftRightRecipeArticle03
                                                                   ,topBottomRecipeArticle01
                                                                   ,topBottomRecipeArticle02
                                                                   ,topBottomRecipeArticle03
                                                                   ,topBottomRecipeArticle04
                                                                   ,topBottomRecipeArticle05};
        insert recipeArticleList;

        List<Recipe__kav> recipeArticleToPublishList = [SELECT KnowledgeArticleId FROM Recipe__kav WHERE Id IN: recipeArticleList];
        for(Recipe__kav recipeRecord : recipeArticleToPublishList)
            KbManagement.PublishingService.publishArticle(recipeRecord.KnowledgeArticleId, true);

        List<Blog_Post__kav> blogArticleList = new List<Blog_Post__kav>{blogArticle01
                                                                       ,blogArticle02
                                                                       ,blogArticle03};
        insert blogArticleList;
        
        List<Blog_Post__kav> blogArticleToPublishList = [SELECT KnowledgeArticleId FROM Blog_Post__kav WHERE Id IN: blogArticleList];
        for(Blog_Post__kav blogRecord : blogArticleToPublishList)
            KbManagement.PublishingService.publishArticle(blogRecord.KnowledgeArticleId, true);                                                               
    }

    @isTest 
    private static void testGetHtml(){ 
        HMR_CMS_CardList_ContentTemplate controller = new HMR_CMS_CardList_ContentTemplate();
        
        Test.startTest();

        String htmlResult = controller.getHTML();

        Test.stopTest();

        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }      

    @isTest
    private static void testConstructorInitializationWithPassedValuesForRecipes(){
        HMR_CMS_CardList_ContentTemplate controller;

        Test.startTest();
        //string listTitle, string objectType, string dataCategory, string recipeCategory, string subFilters, integer offset, integer recordCount, string recordsShown, string colorBlocksShown
        controller = new HMR_CMS_CardList_ContentTemplate('Recipes', 'Recipes', '', 'healthy-shakes', 'entrees', 1, 6, '', '');
        String htmlResult = controller.getHTML();

        Test.stopTest();

        System.assertEquals(controller.ListTitle, 'Recipes');
        System.assertEquals(controller.RecipeCategory, 'All');
    } 


    @isTest
    private static void testConstructorInitializationWithPassedValuesByDataCategory(){
        HMR_CMS_CardList_ContentTemplate controller;

        Test.startTest();
        //string listTitle, string objectType, string dataCategory, string recipeCategory, string subFilters, integer offset, integer recordCount, string recordsShown, string colorBlocksShown
        controller = new HMR_CMS_CardList_ContentTemplate('Eating Well', 'DataCategory', 'Eating_Well', '', '', 1, 6, '', '');
        String htmlResult = controller.getHTML();

        Test.stopTest();

        System.assertEquals(controller.ListTitle, 'Recipes');
        System.assertEquals(controller.RecipeCategory, 'All');
    }

    @isTest
    private static void testPropertyWithDefault(){
        HMR_CMS_CardList_ContentTemplate controller = new HMR_CMS_CardList_ContentTemplate();

        Test.startTest();
        
        //Invalid Property
        String result = controller.getPropertyWithDefault('RecipeCategoryDisplay', 'EatingWell');

        Test.stopTest();

        System.assertEquals('EatingWell', result);
    }

    @isTest
    private static void testContentControllerConstructor(){
        HMR_CMS_CardList_ContentTemplate controller;
        
        Test.startTest();
        //This always fails as no way to get context but here for coverage
        try{
            controller = new HMR_CMS_CardList_ContentTemplate(null);
        }
        catch(Exception ex){}
        
        Test.stopTest();
        
        System.assert(controller == null);
    }
           
}