public with sharing class HMR_CC_CartItem_Trigger_Handler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;
    
    public HMR_CC_CartItem_Trigger_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }

     public void onBeforeInsert(List<ccrz__E_CartItem__c> cartItemList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;

            for(ccrz__E_CartItem__c cartItem : cartItemList){
                if(String.isBlank(cartItem.ccrz__UnitOfMeasure__c))
                    cartItem.ccrz__UnitOfMeasure__c = 'Each';
            }
        }
    }

    public static void deleteCoupon(List<ccrz__E_CartItem__c> cartItems){
    	//DK cart item list will need to affect the coupon	
    	set<Id> dkCouponList = new set<Id>();
    	set<Id> dkList = new set<Id>();

    	//loop the incoming records
    	for(ccrz__E_CartItem__c c: cartItems){
    		if(c.ccrz__ProductType__c == 'Dynamic Kit'){
    			dkCouponList.add(c.ccrz__Cart__c);
    			dkList.add(c.Id);
    		}
    	}

    	//hcekc if there are any coupon for this cart
    	if(dkList.size()>0){
    		list<ccrz__E_CartCoupon__c> deleteMe = new list<ccrz__E_CartCoupon__c>();

    		list<ccrz__E_CartItem__c> cartItemList = [Select ccrz__Product__r.Program__c, ccrz__Product__c, ccrz__Cart__c From ccrz__E_CartItem__c where Id in: dkList];

    		list<ccrz__E_CartCoupon__c> cartCouponList = [Select ccrz__Coupon__r.hmr_Program__c, ccrz__Coupon__c, ccrz__Cart__c From ccrz__E_CartCoupon__c where ccrz__Cart__c in: dkCouponList];

    		for(ccrz__E_CartItem__c cil: cartItemList){
    			for(ccrz__E_CartCoupon__c coup: cartCouponList){
    				//if same cart
    				if(cil.ccrz__Cart__c == coup.ccrz__Cart__c){
    					if(cil.ccrz__Product__c != null && coup.ccrz__Coupon__c!=null && cil.ccrz__Product__r.Program__c!=null && coup.ccrz__Coupon__r.hmr_Program__c!=null){
    						if(cil.ccrz__Product__r.Program__c == coup.ccrz__Coupon__r.hmr_Program__c){
    							deleteMe.add(coup);
    						}
    					}
    				}
    			}
    		}

    		//delete the coupon
    		if(deleteMe.size()>0){
    			delete deleteMe;
    		}
    	}
    }
}