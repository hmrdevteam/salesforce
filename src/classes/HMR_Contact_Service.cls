/**
    * Apex Service Class for the Contact
    *
    * @Date: 2017-11-27   
    * @Author: Utkarsh Goswami (Mindtree)
    * @Modified: 2018-03-14 Zach Engman
    * @JIRA: 
*/
public with sharing class HMR_Contact_Service {


    public HMR_Contact_Service (){
        this(UserInfo.getUserId());
    }

    public HMR_Contact_Service(string userId){
        ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
        contextCCRZ.portalUserId = userId;    
        ccrz.cc_CallContext.initRemoteContext(contextCCRZ);
    }


    public List<ContactRecord> getContactRecordList(String portalUserId){
        List<ContactRecord> contactRecord = new List<ContactRecord>();
        User userDetail;
        List<Contact> contactInfo;
        List<Program_Membership__c> progMemb;
        
        try{
        
             if (!String.isBlank(portalUserId)) {
                userDetail = [SELECT ContactId,CommunityNickname,SmallPhotoUrl FROM User WHERE ID = :portalUserId];
             }
             if (!String.isBlank(userDetail.contactId)) {
             
                 contactInfo = [SELECT FirstName, LastName, Email, hmr_Date_Of_Birth__c, hmr_Gender__c, hmr_Height_Feet__c, hmr_Height_Inches__c, Phone FROM Contact WHERE Id = :userDetail.contactId ];
                 progMemb = [SELECT hmr_Membership_Type__c, hmr_Status__c, InitialWeight__c, Motivation__c, Program__r.hmr_Program_Display_Name__c, SupportNeeded__c, WeightLossGoal__c FROM Program_Membership__c WHERE hmr_Contact__c = :userDetail.contactId AND hmr_Status__c = 'Active'];
             
             }
             
             if(contactInfo.size() > 0 && progMemb.size()> 0){
                 contactRecord.add(new ContactRecord(contactInfo[0], userDetail, progMemb[0]));
             }
             else if(contactInfo.size() > 0 && progMemb.size()<= 0){
                 contactRecord.add(new ContactRecord(contactInfo[0], userDetail, new Program_Membership__c()));
             }

        }
        catch(Exception ex){
            System.Debug(ex);
            System.Debug('Stack Trace -> ' + ex.getStackTraceString());
        }

        return contactRecord;
    }


    public void updateContactInfo(Id portalUserId, Map<String, Object> contactInfoMap) {
        User userDetail = [SELECT ContactId FROM User WHERE ID = :portalUserId LIMIT 1];

        update new Contact(
            Id = userDetail.Contactid,
            firstName = (String)contactInfoMap.get('firstName'),
            lastName = (String)contactInfoMap.get('lastName'),
            phone = (String)contactInfoMap.get('phone'),
            email = (String)contactInfoMap.get('email')
        );
    }

    public Map<String, Object> updateProfileInfo(Id portalUserId, Map<String, Object> contactInfoMap) {
        Map<String, Object> returnObject = new Map<String, Object>();
        User userDetail = [SELECT ContactId FROM User WHERE ID = :portalUserId LIMIT 1];
        List<Program_Membership__c> programMembershipList = [SELECT InitialWeight__c, Motivation__c, Program__r.hmr_Program_Display_Name__c, SupportNeeded__c, WeightLossGoal__c FROM Program_Membership__c WHERE hmr_Contact__c = :userDetail.ContactId AND hmr_Status__c = 'Active'];

        Contact contactRecord = new Contact(
            Id = userDetail.Contactid,
            firstName = (String)contactInfoMap.get('firstName'),
            lastName = (String)contactInfoMap.get('lastName'),
            phone = (String)contactInfoMap.get('phone'),
            email = (String)contactInfoMap.get('email'),
            hmr_Gender__c = (String)contactInfoMap.get('gender')
        );

        if(contactInfoMap.get('heightFeet') != '' && contactInfoMap.get('heightFeet') != null){
            contactRecord.hmr_Height_Feet__c = Integer.valueOf(contactInfoMap.get('heightFeet'));
        }

        if(contactInfoMap.get('heightInches') != '' && contactInfoMap.get('heightInches') != null){
            contactRecord.hmr_Height_Inches__c = Integer.valueOf(contactInfoMap.get('heightInches'));
        }

        if(contactInfoMap.get('birthDate') != '' && contactInfoMap.get('birthDate') != null){
            contactRecord.hmr_Date_Of_Birth__c = Date.valueOf((String)contactInfoMap.get('birthDate'));
        }

        if(programMembershipList.size() > 0){
            programMembershipList[0].Motivation__c = (String)contactInfoMap.get('motivation');
            programMembershipList[0].SupportNeeded__c = (String)contactInfoMap.get('supportNeeded');

            if(contactInfoMap.get('initialWeight') != '' && contactInfoMap.get('initialWeight') != null){
                programMembershipList[0].InitialWeight__c = Double.valueOf(contactInfoMap.get('initialWeight'));
            }
            
            if(contactInfoMap.get('weightLossGoal') != '' && contactInfoMap.get('weightLossGoal') != null){
                programMembershipList[0].WeightLossGoal__c = Double.valueOf(contactInfoMap.get('weightLossGoal'));
            }
        }

        try{
            update contactRecord;

            if(programMembershipList.size() > 0)
                update programMembershipList[0];

            returnObject.put('success', true);
        }
        catch(Exception ex){
            returnObject.put('success', false);
            returnObject.put('error', ex.getMessage());
        }

        return returnObject;
    }
   

    public class ContactRecord{
        public string firstName {get; private set;}
        public string lastName {get; private set;}
        public string email {get; private set;}
        public string phone {get; private set;}
        public Date birthDate {get; private set;}
        public Integer birthDate_Year {get; private set;}
        public Integer birthDate_Month {get; private set;}
        public Integer birthDate_Day {get; private set;}
        public string gender {get; private set;}
        public string heightDisplay {get{
            return heightFeet != null && heightFeet > 0 ? heightFeet + '\' ' + (heightInches != null ? heightInches + '"' : '') : '';
        }}
        public Integer heightFeet {get; private set;}
        public Integer heightInches {get; private set;}
        public UserRecord userRec {get; private set;}
        public ProgramMembershipRecord programMemRec {get; private set;}
        
        public ContactRecord(Contact contactRec, User userRecord, Program_Membership__c programMemRec){
        
            this.firstName = contactRec.firstName;
            this.lastName = contactRec.lastName;
            this.email = contactRec.Email;
            this.phone = contactRec.Phone;
            this.birthDate = contactRec.hmr_Date_Of_Birth__c;
            this.gender = contactRec.hmr_Gender__c;

            if(contactRec.hmr_Height_Feet__c != null && contactRec.hmr_Height_Feet__c > 0){
                this.heightFeet = Integer.valueOf(contactRec.hmr_Height_Feet__c);
            }
            if(contactRec.hmr_Height_Inches__c != null && contactRec.hmr_Height_Feet__c > 0){
                this.heightInches = Integer.valueOf(contactRec.hmr_Height_Inches__c);
            }

            this.userRec = new  UserRecord(userRecord);
            this.programMemRec = new ProgramMembershipRecord(programMemRec);

            if(birthDate != null){
                this.birthDate_Year = birthDate.Year();
                this.birthDate_Month = birthDate.Month();
                this.birthDate_Day = birthDate.Day();
            }
        
        }
    }
    
    public class UserRecord{
    
        String nickName {get; private set;}
        String smallPhotoUrl {get; private set;}
        
        public UserRecord(User userRecord){
        
            this.nickName = userRecord.CommunityNickname;
            this.smallPhotoUrl = userRecord.SmallPhotoUrl;
        
        }
    
    }
    
    public class ProgramMembershipRecord{
        
        Decimal initialWeight {get; private set;} 
        String membershipType {get; private set;} 
        String motivation {get; private set;}
        String programDisplayName {get; private set;}
        String status {get; private set;}
        String supportNeeded {get; private set;}
        Integer weightLossGoal {get; private set;}

        public ProgramMembershipRecord(Program_Membership__c programMemRec){
        
            this.programDisplayName = programMemRec.Program__r.hmr_Program_Display_Name__c;
            this.membershipType = programMemRec.hmr_Membership_Type__c;
            this.initialWeight = programMemRec.InitialWeight__c;
            this.motivation = programMemRec.Motivation__c;
            this.status = programMemRec.hmr_Status__c;
            this.supportNeeded = programMemRec.SupportNeeded__c;
            this.weightLossGoal = Integer.valueOf(programMemRec.WeightLossGoal__c);
        }
    
    }

}