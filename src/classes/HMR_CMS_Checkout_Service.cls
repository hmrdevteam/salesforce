public with sharing class HMR_CMS_Checkout_Service {
    private string encryptedCartId;
	private string userId;
	private boolean isGuest = false;
	private HMR_Cart_Service cartService;

	private HMR_CMS_Checkout_Service() {}

	public HMR_CMS_Checkout_Service(string userId, boolean isGuest){
		this(userId, isGuest, null);
	}

    public HMR_CMS_Checkout_Service(string userId, boolean isGuest, string encryptedCartId){
        ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
        contextCCRZ.portalUserId = userId;    
        ccrz.cc_CallContext.initRemoteContext(contextCCRZ);

        this.userId = userId;
        this.isGuest = isGuest;
        this.encryptedCartId = encryptedCartId;
        this.cartService = new HMR_Cart_Service(userId, isGuest);
    }

	public Map<String, Object> authorizePayment(Map<String, String> userAddressMap, Map<String, String> unsignedFieldMap){
		Map<String, Object> responseMap = new Map<String, Object>();

		try {
            Map<String, Object> cartDataMap = this.getCartByEncryptedId(this.encryptedCartId).cartDataMap;
            HMR_Cybersource_Utility.authorizePayment(this.userId, userAddressMap, unsignedFieldMap, cartDataMap);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();

            for(String postStringItem: postStringArray) 
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);

            Http http = new Http();
            HTTPResponse response = http.send(req);

            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', this.userId);
            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

		return responseMap;
	}

	public Map<String, Object> authorizePaymentAndCreatePaymentToken(Map<String, String> userAddressMap, Map<String, String> unsignedFieldMap){
		Map<String, Object> responseMap = new Map<String, Object>();

		try {
            HMR_Cybersource_Utility.authorizePaymentAndCreatePaymentToken(this.userId, userAddressMap, unsignedFieldMap);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();

            for(String postStringItem: postStringArray) 
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('=').replace('\\',''));
            
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);

            Http http = new Http();
            HTTPResponse response = http.send(req);

            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', this.userId);

            Map<String, Object> billingAddressMap = this.addBillingAddress(this.userId, userAddressMap);
            if(billingAddressMap.size() > 0 && billingAddressMap.containsKey('contactAddress'))
                responseMap.put('billingAddress', billingAddressMap.get('contactAddress'));

            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
	}

	public Map<String, Object> addBillingAddress(String userId, Map<String, String> billingAddressMap) {
		Map<String, Object> outputData = new Map<String, Object>();
		User userRecord = new HMR_User_Selector().getById(userId);
        
        if(!String.isBlank(userRecord.ContactId) && userRecord.Contact != null && !String.isBlank(userRecord.Contact.AccountId)){
        	String accountId = userRecord.Contact.AccountId;
            String contactId = userRecord.ContactId;

            if(!billingAddressMap.isEmpty()) {
                ccrz__E_ContactAddr__c contactAddressToInsert = new ccrz__E_ContactAddr__c();
                contactAddressToInsert.ccrz__FirstName__c = billingAddressMap.get('bill_to_forename').unescapeUnicode();
                contactAddressToInsert.ccrz__LastName__c = billingAddressMap.get('bill_to_surname').unescapeUnicode();
                contactAddressToInsert.ccrz__AddressFirstline__c = billingAddressMap.get('bill_to_address_line1');
                contactAddressToInsert.ccrz__AddressSecondline__c = billingAddressMap.get('bill_to_address_line2');
                contactAddressToInsert.ccrz__City__c = billingAddressMap.get('bill_to_address_city');
                contactAddressToInsert.ccrz__State__c = billingAddressMap.get('bill_to_address_state');
                contactAddressToInsert.ccrz__StateISOCode__c = billingAddressMap.get('bill_to_address_state_code');
                contactAddressToInsert.ccrz__PostalCode__c = billingAddressMap.get('bill_to_address_postal_code');
                contactAddressToInsert.ccrz__Country__c = 'United States';
                contactAddressToInsert.ccrz__CountryISOCode__c = 'US';
                contactAddressToInsert.OwnerId = userId;
                //Check for Duplicate
                List<ccrz__E_ContactAddr__c> existingAddressList = [SELECT ccrz__AddressFirstline__c,
                                                                           ccrz__AddressSecondline__c,
                                                                           ccrz__City__c,
                                                                           ccrz__State__c,
                                                                           ccrz__StateISOCode__c,
                                                                           ccrz__PostalCode__c,
                                                                           ccrz__Country__c,
                                                                           ccrz__CountryISOCode__c
                                                                    FROM ccrz__E_ContactAddr__c
                                                                    WHERE OwnerId =: userId
                                                                      AND ccrz__FirstName__c =: contactAddressToInsert.ccrz__FirstName__c
                                                                      AND ccrz__LastName__c =: contactAddressToInsert.ccrz__LastName__c
                                                                      AND ccrz__AddressFirstline__c =: contactAddressToInsert.ccrz__AddressFirstline__c
                                                                      AND ccrz__City__c =: contactAddressToInsert.ccrz__City__c
                                                                      AND ccrz__PostalCode__c =: contactAddressToInsert.ccrz__PostalCode__c];
                if(existingAddressList.size() > 0)
                  contactAddressToInsert.Id = existingAddressList[0].Id;
                else{
                  insert contactAddressToInsert;
                  ccrz__E_AccountAddressBook__c contactAccountAddressBookToInsert = new ccrz__E_AccountAddressBook__c();
                  contactAccountAddressBookToInsert.ccrz__E_ContactAddress__c = contactAddressToInsert.Id;
                  contactAccountAddressBookToInsert.ccrz__AddressType__c = 'Billing';
                  contactAccountAddressBookToInsert.ccrz__Default__c = true;
                  contactAccountAddressBookToInsert.ccrz__AccountId__c = accountId;
                  contactAccountAddressBookToInsert.ccrz__Account__c = accountId;
                  contactAccountAddressBookToInsert.OwnerId = userId;
                  insert contactAccountAddressBookToInsert;
                }

                outputData.put('contactAddress', contactAddressToInsert);
            }
        }

        return outputData;
    }

	public Map<String, Object> authorizeUsingPaymentToken(Map<String, String> userAddressMap, string paymentToken) {
		Map<String, Object> responseMap = new Map<String, Object>();

        try {
            HMR_Cybersource_Utility.authorizeUsingPaymentToken(this.userId, userAddressMap, paymentToken);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();

            for(String postStringItem: postStringArray) 
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);

            Http http = new Http();
            HTTPResponse response = http.send(req);

            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', this.userId);
            responseMap.put('success', true); 
        }
        catch(Exception e) {
        	responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

    public Map<String, Object> createPaymentToken(Map<String, String> userAddressMap, Map<String, String> unsignedFieldMap){
        Map<String, Object> responseMap = new Map<String, Object>();

        try {
            HMR_Cybersource_Utility.createPaymentToken(this.userId, userAddressMap, unsignedFieldMap);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();

            for(String postStringItem: postStringArray) 
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);

            Http http = new Http();
            HTTPResponse response = http.send(req);

            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', this.userId);
            responseMap.put('userAddressMap', userAddressMap);
            
            Map<String, Object> billingAddressMap = this.addBillingAddress(this.userId, userAddressMap);
            if(billingAddressMap.size() > 0 && billingAddressMap.containsKey('contactAddress'))
                responseMap.put('billingAddress', billingAddressMap.get('contactAddress'));

            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
            responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

    public Map<String, Object> createPaymentTokenForP2(Map<String, String> userAddressMap, Map<String, String> unsignedFieldMap){
    	Map<String, Object> responseMap = new Map<String, Object>();

    	try {
            HMR_Cybersource_Utility.createPaymentToken(this.userId, userAddressMap, unsignedFieldMap);
            String postString = HMR_Cybersource_Utility.generatePOSTString();
            String endPoint = System.Label.HMR_Cybersource_SilentOrderPayEndPoint;
            List<String> postStringArray = postString.split('&');
            Map<String, String> postStringMap = new Map<String, String>();

            for(String postStringItem: postStringArray) 
                postStringMap.put(postStringItem.subStringBefore('='), postStringItem.subStringAfter('='));
            
            responseMap.put('postStringMap', postStringMap);
            responseMap.put('endPoint', endPoint);

            HttpRequest req = new HttpRequest();
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setBody(postString);

            Http http = new Http();
            HTTPResponse response = http.send(req);

            responseMap.put('response', HMR_Cybersource_Utility.parseHTMLResponse(response.getBody()));
            responseMap.put('responseStatusCode', response.getStatusCode());
            responseMap.put('currUserId', this.userId);
            responseMap.put('userAddressMap', userAddressMap);
            
            Map<String, Object> billingAddressMap = this.addBillingAddress(this.userId, userAddressMap);
            if(billingAddressMap.size() > 0 && billingAddressMap.containsKey('contactAddress'))
                responseMap.put('billingAddress', billingAddressMap.get('contactAddress'));

            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

    public Map<String, Object> createStoredPayment(Map<String, String> paymentInformationMap){
    	Map<String, Object> responseMap = new Map<String, Object>();

    	try {  
            if(!paymentInformationMap.isEmpty()) {
                if((paymentInformationMap.get('contactAddressId') != null || paymentInformationMap.get('contactAddressId') != '') &&
                   (paymentInformationMap.get('contactFirstName') != null || paymentInformationMap.get('contactFirstName') != '' ||
                    paymentInformationMap.get('contactLastName') != null || paymentInformationMap.get('contactLastName') != '')) {
                        ccrz__E_ContactAddr__c contactAddressToUpdate =
                            [SELECT Id, ccrz__FirstName__c, ccrz__LastName__c FROM ccrz__E_ContactAddr__c
                             WHERE Id = :paymentInformationMap.get('contactAddressId')];
                        if(paymentInformationMap.get('contactFirstName') != null && paymentInformationMap.get('contactFirstName') != '') {
                            contactAddressToUpdate.ccrz__FirstName__c = paymentInformationMap.get('contactFirstName');
                        }
                        if(paymentInformationMap.get('contactLastName') != null && paymentInformationMap.get('contactLastName') != '') {
                            contactAddressToUpdate.ccrz__LastName__c = paymentInformationMap.get('contactLastName');
                        }
                        update contactAddressToUpdate;
                        responseMap.put('contactAddressToUpdate', contactAddressToUpdate);
                    }

                ccrz__E_StoredPayment__c storedPaymentToInsert = new ccrz__E_StoredPayment__c(
                    ccrz__Account__c = ccrz.cc_CallContext.currAccountId,
                    ccrz__User__c = ccrz.cc_CallContext.currUserId,
                    ccrz__Storefront__c = ccrz.cc_CallContext.storefront,
                    ccrz__DisplayName__c = paymentInformationMap.get('displayName'),
                    ccrz__AccountNumber__c = paymentInformationMap.get('accountNumber'),
                    ccrz__AccountType__c = 'cc',
                    HMR_Contact_Address__c = paymentInformationMap.get('contactAddressId'),
                    ccrz__Token__c = paymentInformationMap.get('paymentToken'),
                    ccrz__ExpMonth__c = Decimal.valueOf(paymentInformationMap.get('expirationMonth')),
                    ccrz__ExpYear__c = Decimal.valueOf(paymentInformationMap.get('expirationYear')),
                    ccrz__PaymentType__c = paymentInformationMap.get('paymentType'),
                    ccrz__Enabled__c = true,
                    OwnerId = ccrz.cc_CallContext.currUserId
                );

                insert storedPaymentToInsert;

                responseMap.put('storedPayment', storedPaymentToInsert);
                responseMap.put('success', true);
            }
            
            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

	public Map<String, Object> fetchTerms() {
		Map<String, Object> inputData = new Map<String, Object> {
		 ccrz.ccApi.API_VERSION => 4
		};

		Map<String, Object> outputData = ccrz.ccAPITerms.fetch(inputData);
		System.debug('*** termsOutput: ' + JSON.serializePretty(outputData));

		return outputData;
	}

	public HMR_Cart_Service.CartRecord getCartByEncryptedId(String encryptedCartId) {
		HMR_Cart_Service.CartRecord cartRecord = cartService.getCartByEncryptedId(encryptedCartId);
        cartRecord.cartDataMap.put('cartCount', 1);
        cartRecord.cartDataMap.put('cartTotalAmount', cartRecord.totalAmount);
        return cartRecord;
	}

	public HMR_Cart_Service.CartRecord getCartByUserId(String userId) {
		return cartService.getCartByUserId(userId);
	}

	public CartAddresses getCartAddresses(String encryptedCartId) {
		return new CartAddresses(getCartByEncryptedId(encryptedCartId));
	}

	public Map<String, Object> getCartShippingAddress(String encryptedCartId) {
		return getCartAddresses(encryptedCartId).shippingAddress;
	}

	public Map<String, Object> getCartBillingAddress(String encryptedCartId) {
		return getCartAddresses(encryptedCartId).billingAddress;
	}

    public Map<String, Object> getCCTerms(string contactId, List<String> productIdList){
        Map<String, Object> responseMap = new Map<String, Object>();

        if(!String.isBlank(contactId)) {
            Contact c = [SELECT AccountId FROM Contact WHERE Id = :contactId];

            Boolean contactIsHealthConsented = false;
            Boolean contactIsADConsented = false;

            List<Consent_Agreement__c> contactCAs = new List<Consent_Agreement__c>(
                [SELECT Id, Version__c, Status__c, Consent_Type__c, Contact__c FROM Consent_Agreement__c
                 WHERE Contact__c = :c.Id AND Status__c = 'Consented']);

            for(Consent_Agreement__c contactCA: contactCAs) {
                if(contactCA.Consent_Type__c == 'Health') {
                    contactIsHealthConsented = true;
                }
                if(contactCA.Consent_Type__c == 'Auto-Delivery') {
                    contactIsADConsented = true;
                }
            }

            //Map of Ids and Products from the argument passed into the method
            Map<Id, ccrz__E_Product__c> cc_CartProducts = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, Program__c FROM ccrz__E_Product__c WHERE Id IN :productIdList] );

            //empty set to hold Program Ids of all Kits in the cart.
            //This will be used in more depth in R2 to split out MDC orders that should only see AD and NOT Health terms
            Set<Id> kitIds = new Set<Id>();
            //variable that we'll set if there is a valid kit in the cart
            Boolean kitOrder = false;

            //Loop through the Products in the cart to find Kits related to a program, and add them to the set
            for(ccrz__E_Product__c cartProd: cc_CartProducts.values()) {
                if(cartProd.Program__c != null) {
                    kitIds.add(cartProd.Program__c);
                }
            }

            //if the set is not empty, set flag to true
            if(kitIds.size() > 0) {
                kitOrder = true;
            }

            //empty list of ccTerms to fill in according to conditions below
            List<ccrz__E_Term__c> ccTermsList = new List<ccrz__E_Term__c>();

            try {
                //Get the full list of terms to parse through individually
                List<ccrz__E_Term__c> fullTermsList = new List<ccrz__E_Term__c>(
                    [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                     FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE]);

                //Get standard Health Acknowledgement record for CC Term
                ccrz__E_Term__c standardHealthTerm =
                    [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                     FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE AND Consent_Type__c = 'Health' AND Employer_Account__c = null];

                //if cart contains a Kit Order, add Health and AD terms to the list
                if(kitOrder) {
                    //If the Contact is associated to an account that has a specific Health Term, add it to the list
                    for(ccrz__E_Term__c term : fullTermsList) {
                        if(term.Consent_Type__c == 'Health' && term.Employer_Account__c == c.AccountId && !contactIsHealthConsented) {
                            ccTermsList.add(term);
                        }
                    }
                    //if no Employer specific Health Term was added to the list, add the standard Health Term
                    if(ccTermsList.isEmpty() && !contactIsHealthConsented) {
                        ccTermsList.add(standardHealthTerm);
                    }
                    //add AD terms to the list
                    for(ccrz__E_Term__c term : fullTermsList) {
                        if(term.Consent_Type__c == 'Auto-Delivery' && !contactIsADConsented) {
                            ccTermsList.add(term);
                        }
                    }
                }
                //Add Money Back Guarantee to List for all orders
                for(ccrz__E_Term__c term : fullTermsList) {
                    if(term.Consent_Type__c == 'Shipping') {
                        ccTermsList.add(term);
                    }
                }
                responseMap.put('success', true);
                responseMap.put('data', ccTermsList);  
            }
            catch (Exception e){
                responseMap.put('success', false); 
                responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
            }
        }
        else {
            try {
                //Get the full list of terms to parse through individually
                List<ccrz__E_Term__c> ccTermsListGuest = new List<ccrz__E_Term__c>(
                    [SELECT Id, ccrz__Enabled__c, ccrz__Title__c, Consent_Type__c, Employer_Account__c, ccrz__Description__c
                     FROM ccrz__E_Term__c WHERE ccrz__Enabled__c = TRUE AND Consent_Type__c = 'Shipping']);

                responseMap.put('success', true);
                responseMap.put('data', ccTermsListGuest);
            }
            catch (Exception e){
                responseMap.put('success', false); 
                responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
            } 
        }

        return responseMap;
    }

    public Map<String, Object> getExtendedDataForProducts(List<String> productIdList){
        Map<String, Object> responseMap = new Map<String, Object>();

        try{
            Map<String, String> hmrADLegalCopies = new Map<String, String>{
                'P1 Healthy Solutions' => String.valueOf(System.Label.HMR_AD_LegalMandatory_HSAH),
                'P1 Healthy Shakes' => String.valueOf(System.Label.HMR_AD_LegalMandatory_HS),
                'Phase 2' => String.valueOf(System.Label.HMR_AD_LegalMandatory_P2)
            };
            Map<String, String> hmrADLegalCheckboxCopies = new Map<String, String>{
                'P1 Healthy Solutions' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_HSAH),
                'P1 Healthy Shakes' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_HS),
                'Phase 2' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_P2)
            };

            Map<String, Object > dataMap = new Map<String, Object>();

            Map<Id, ccrz__E_Product__c> cc_ProductsMap = new Map<Id, ccrz__E_Product__c>(
                [SELECT Name, hmr_Ingredients__c, ccrz__ShortDesc__c, ccrz__SKU__c, Program__r.Name,
                    (SELECT ccrz__Spec__r.Name, ccrz__Spec__r.ccrz__SpecGroup__c, ccrz__SpecValue__c
                        FROM ccrz__Product_Specs__r Order by ccrz__Spec__r.ccrz__sequence__c ASC),
                    (SELECT ccrz__MediaType__c, ccrz__URI__c
                        FROM ccrz__E_ProductMedias__r WHERE ccrz__MediaType__c = 'Product Image Thumbnail')
                  FROM ccrz__E_Product__c WHERE Id IN :productIdList] );

            for(ccrz__E_Product__c productItem: cc_ProductsMap.values()){
                dataMap.put(String.valueOf(productItem.Id), (ccrz__E_Product__c)productItem);
                if(productItem.Program__r.Name != null && productItem.Program__r.Name != '')  {
                    String legalLang = String.valueOf(productItem.Id) + '_legalLang';
                    String legalCheckboxLang = String.valueOf(productItem.Id) + '_legalCheckboxLang';
                    dataMap.put(legalLang, hmrADLegalCopies.get(productItem.Program__r.Name));
                    dataMap.put(legalCheckboxLang, hmrADLegalCheckboxCopies.get(productItem.Program__r.Name));
                }
            }

            responseMap.put('success', true);
            responseMap.put('data', dataMap);
        }
        catch (Exception e){
            responseMap.put('success', false); 
            responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

	public Map<String, Object> updateAddress(Map<String, Object> addressDataMap) {
		return HMR_CMS_AddressService.reviseAddress(addressDataMap);
	}

	public Map<String, Object> getStoredPaymentDetails(string storedPaymentId){
        Map<String, Object> responseMap = new Map<String, Object>();

        try {
            ccrz__E_StoredPayment__c storedPayment =
                [SELECT ccrz__AccountNumber__c, ccrz__AccountType__c, ccrz__ExpMonth__c, ccrz__ExpYear__c, ccrz__PaymentType__c,
                            ccrz__Token__c 
                 FROM ccrz__E_StoredPayment__c 
                 WHERE Id = :storedPaymentId];

            responseMap.put('storedPayment', storedPayment);       
            responseMap.put('success', true);
        }
        catch(Exception e) {
            responseMap.put('success', false); 
        	responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }
       
        return responseMap;
	}

	public Map<String, Object> createCartAddress(Map<String, Object> addressDataMap, String encryptedCartId, String addressType) {
		Map<String,Object> createAddressResponse = HMR_CMS_AddressService.createAddress(addressDataMap);

		Id addressId;
		if(createAddressResponse.containsKey(ccrz.ccApiAddress.ADDRESSIDLIST)) {
			List<Id> addressIdList = (List<Id>) createAddressResponse.get(ccrz.ccApiAddress.ADDRESSIDLIST);
			if(addressIdList.size() == 1) {
				addressId = addressIdList[0];
			}
		}
		setCartAddress(getCartByEncryptedId(encryptedCartId).id, addressType, addressId);
		return createAddressResponse;
	}

	// This method will be moved or removed
	// addressType can be either 'billTo' or 'shipTo'
	public static Map<String, Object> setCartAddress(Id cartId, String addressType, Id addressId) {
		Map<String, Object> inputData = new Map<String, Object>{
	 		ccrz.ccAPI.API_VERSION => 4,
	 		ccrz.ccAPI.SIZING => new Map<String, Object> {
		        ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
		            ccrz.ccAPI.SZ_REFETCH => true
		        }
		    },
	 		ccrz.ccApiCart.CART_OBJLIST => new List<Map<String, Object>> {
	 			new Map<String, Object> {
	 				'sfid' => cartId,
	 				//ccrz.ccApiCart.CART_ENCID => encryptedCartId,
	 				addressType => addressId
	 			}
	 		}
		};

		Map<String, Object> outputData = ccrz.ccAPICart.revise(inputData);

		return outputData;
	}

	public Map<String, Object> getUserAddresses(Id userId, String addressType) {
		return HMR_CMS_AddressService.getAddressBookByOwnerId(userId, addressType);
	}

    public Map<String, Object> updateCartForP2(string encryptedCartId, Date p2OrderDate){
        Map<String, Object> responseMap = new Map<String, Object>();

        try {
            if(!String.isBlank(encryptedCartId)) {
                ccrz__E_Cart__c cartToUpdate = [SELECT ccrz__EncryptedId__c, P2_1st_Order_Date__c, P2_1st_Order_Delay__c
                                                FROM ccrz__E_Cart__c 
                                                WHERE ccrz__EncryptedId__c = :encryptedCartId];

                if(cartToUpdate != null && p2OrderDate != null) {
                    cartToUpdate.P2_1st_Order_Date__c = p2OrderDate;
                    cartToUpdate.P2_1st_Order_Delay__c = true;
                    update cartToUpdate;

                    responseMap.put('success', true);
                    responseMap.put('data', cartToUpdate);
                }
                else
                    responseMap.put('success', false);
            }
            else
                responseMap.put('success', false);
        }
        catch(Exception e) {
            responseMap.put('success', false);
            responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

    public Map<String, Object> saveCCTerms(String contactId, List<String> ccTermIdList, boolean isCSRFlow){
        Map<String, Object> responseMap = new Map<String, Object>();

        try {
            List<Consent_Agreement__c> clientCAList = new List<Consent_Agreement__c>();

            //if cc term ids are passed in from checkout process, loop through ids and set up consent agreement records
            if(!ccTermIdList.isEmpty() && !String.isBlank(contactId)) {
                for(string ccTermId : ccTermIdList) {
                    Consent_Agreement__c clientCA = new Consent_Agreement__c(
                        Contact__c = contactId,
                        Version__c = ccTermId
                    );

                    if(isCSRFlow)  //if in csr context, set the ca status to notified
                        clientCA.Status__c = 'Notified'; 
                    else { //if not CSR, set status to consented
                        clientCA.Agreement_Timestamp__c = DateTime.now();
                        clientCA.Status__c = 'Consented';
                    }

                    clientCAList.add(clientCA);
                }
            }

            //insert new consent agreement records
            if(!clientCAList.isEmpty())
                insert clientCAList;

            responseMap.put('success', true);
            responseMap.put('clientCAList', clientCAList);
        }
        catch (Exception e){
            responseMap.put('success', false);
            responseMap.put('error', e.getMessage()+'line: '+e.getLineNumber()+ ''+e.getStackTraceString());
        }

        return responseMap;
    }

	public class CartAddresses {
		public Map<String, Object> shippingAddress {get; private set;}
		public Map<String, Object> billingAddress {get; private set;}

		public CartAddresses(HMR_Cart_Service.CartRecord cartRecord) {
			if(cartRecord.shipTo != null) {
				this.shippingAddress = findAddressByIdInAddressList(cartRecord.shipTo, cartRecord.addressList);
			}
			if(cartRecord.billTo != null) {
				this.billingAddress = findAddressByIdInAddressList(cartRecord.billTo, cartRecord.addressList);
			}
		}

		// Move to utility?
		private Map<String, Object> findAddressByIdInAddressList(Id addressId, List<Map<String, Object>> addressList) {
			for(Map<String, Object> address : addressList) {
				if(Id.valueOf((String) address.get('sfid')) == addressId) {
					return address;
				}
			}

			return null;
		}
	}
}