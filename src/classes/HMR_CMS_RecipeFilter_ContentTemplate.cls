/**
*
* @Date: 2017-07-20
* @Author Utkarsh Goswami (Mindtree)
* @Modified: Zach Engman - 2017-08-03
* @JIRA: HPRP-4116
*/
global virtual with sharing class HMR_CMS_RecipeFilter_ContentTemplate extends cms.ContentTemplateController{

   global HMR_CMS_RecipeFilter_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    global HMR_CMS_RecipeFilter_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }


    public String dietCategory{
        get{
            return getAttribute('dietCategory');
        }
    }


    global virtual override String getHTML() {
        String html = '';
       // String category = ApexPages.currentPage().getParameters().get('category');
        List<String> filterOptions = getFilterListOptions();
        String categoryDesc = getCategoryDescription();


        html += '<div class="sub-filter-wrapper"><div class="container">';
            html += '<div class="col-xs-12 sub-filter-header">';
                html += '<div class="col-xs-12 col-sm-8 hmr-info">';
                    html += categoryDesc;
                html += '</div>';
                html += '<div class="col-xs-12 col-sm-4 hmr-link show-filter hmr-allcaps hmr-allcaps-blue">';
                    html += 'SHOW FILTER +';
                html += '</div>';
            html += '</div>';
            html += '<div class="col-xs-12 sub-filter">';
                html += '<div class="col-xs-12 flex-container">';

                        for(String option : filterOptions){

                           html += '<div class="hmr-form-control">';
                                html += '<input id="'+ option +'" name="category" type="checkbox" class="section-checkbox blue" />';
                                html += '<label class="form-control__label hmr-link" for="'+ option +'">'+ option +'</label>';
                           html += '</div>';
                        }

                html += '</div>';
                html += '<div class="col-xs-12 filter-btns text-right">';
                        html += '<div class="hmr-allcaps-container">';
                            html += '<a id="clearSubFilters" data-gtm-id="homeBlock3" href="#" class="hmr-allcaps hmr-allcaps-blue">CLEAR FILTERS</a>';
                        html += '</div>';
                        html += '<button id="applySubFilters" class="btn hmr-btn-blue hmr-btn-xsmall">Apply</button>';
                html += '</div>';
            html += '</div>';

        html += '</div></div>';



        return html;
    }

    public List<String> getFilterListOptions(){
        //Map<String,List<String>> dependentPicklistMap = HMR_Picklist_Utility.GetDependentOptions('hmr_Recipe__c', 'Diet_Category__c', 'hmr_Recipe_Category__c');
      List<String> filterOptions;


      if(dietCategory == 'Decision Free'){
          filterOptions = new List<String>{
              'Shakes','Entrees','Puddings','Ice Cream','Hot Drinks','Multigrain Cereal','Soups'
          };
      }

      if(dietCategory == 'Healthy Solutions'){
          filterOptions = new List<String>{
              'Fruit and Vegetables','Shakes','Entrees','Puddings','Ice Cream','Hot Drinks','Multigrain Cereal','Soups'
          };
      }

      if(dietCategory == 'Healthy Shakes'){
          filterOptions = new List<String>{
              'Lean proteins','Grains','Fruit and Vegetables','Shakes','Entrees','Puddings','Ice Cream','Hot Drinks','Multigrain Cereal','Soups'

          };
      }

      if(dietCategory == 'Phase 2'){

          filterOptions = new List<String>{
               'Phase 2 recipes','Lean proteins','Grains','Fruit and Vegetables','Shakes','Entrees','Puddings','Ice Cream','Hot Drinks','Multigrain Cereal','Soups'

          };

      }


      //if(filterOptions == null || filterOptions.size() <= 0)
            //filterOptions = new List<String>();

      return filterOptions;
    }

    public String getCategoryDescription(){

      String categoryDesc;


      if(dietCategory == 'Decision Free'){
          categoryDesc = 'Quick, easy and creative low-calorie recipes for the Decision-Free® diet plan, using HMR foods with no- and low-calorie condiments and flavorings.';
      }

      if(dietCategory == 'Healthy Solutions'){
          categoryDesc = 'Add variety and volume to your Healthy Solutions® diet plan with Phase 1 recipes using HMR foods, fruits, and vegetables.';
      }

      if(dietCategory == 'Healthy Shakes'){
          categoryDesc = 'Delicious Phase 1 recipes for HMR’s Healthy Shakes® plan, including ideas for creating your “healthy meal” with portion-controlled lean proteins and grains.';
      }

      if(dietCategory == 'Phase 2'){
          categoryDesc = 'A comprehensive collection of recipes to support long-term weight management, including Phase 2 favorites from the HMR Community.';

      }

      return categoryDesc;
    }
}