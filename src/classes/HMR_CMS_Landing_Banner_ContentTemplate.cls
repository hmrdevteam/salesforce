/**
* 
*
* @Date: 03/14/2018
* @Author Ali Pierre (HMR)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_Landing_Banner_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_Landing_Banner_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_Landing_Banner_ContentTemplate() {}

	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
	 */
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
	 */
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	global virtual override String getHTML(){
		String html = '';
		try {
			html += '<div class="hmr-modal modal fade" id="details_exclusions">' +
			        '<div class="hmr-clinic-modal-dialog modal-dialog" role="document">' +
			            '<div class="modal-content hmr-modal-content">' +
			                '<div class="modal-header hmr-modal-header">' +
			                    '<h5 class="modal-title">Money Back Guarantee</h5>' +
			                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
			                        '<span aria-hidden="true">&times;</span>' +
			                    '</button>' +
			                '</div>' +
			                '<div class="modal-body hmr-modal-body">' +
			                    '<p>We want you to be happy. If you\'re not completely satisfied with anything you purchased from this site, we will gladly offer you a full refund. No questions asked! Simply send back what you don\'t like within 60 days. (Shipping costs are nonrefundable.)</p>' +
			                '</div>' +
			            '</div>' +
		        	'</div>' +
				'</div>'+
				'<section class="hmr-page-section-landing-banner">' +
			        '<div class="container ">' +
			            '<div class="row">' +
			                '<p>TRY HMR RISK FREE When you’re ready to get serious about weight loss, you can join HMR risk-free with our <a data-gtm-id="considerBlock3" href="#" data-toggle="modal" data-target="#details_exclusions">money-back guarantee</a>.</p>' +
			            '</div>' +			            
			        '</div>' +
			    '</section>';
		return html;
		}
		catch(Exception ex) {
			System.debug('Exception in Plans Details Section Content Template');
			System.debug(ex);
			html += ex.getMessage();
		}
		return html;
	}
}