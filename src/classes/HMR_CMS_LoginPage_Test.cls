/*
Developer: Mustafa Ahmed (HMR)
Date: March 22nd 2017
Target Class: HMR_CMS_LoginPage_ContentTemplate
*/

@isTest
public class HMR_CMS_LoginPage_Test {
    	@isTest static void test_general_method() {
		// Implement test code
		HMR_CMS_LoginPage_ContentTemplate LoginPageTemplate = new HMR_CMS_LoginPage_ContentTemplate();			
        
            
        ccrz__E_AccountGroup__c accountGroup1 = new ccrz__E_AccountGroup__c(
            Name = 'TestAccountGroup1',
            ccrz__PriceListSelectionMethod__c = 'Best Price1'
            );
            insert accountGroup1;
        
                
        Account accountTest1 = new Account(
            name = 'Test Account1',
            ccrz__E_AccountGroup__c = accountGroup1.Id,
            Standard_HSAH_Kit_Discount__c = 2.22,
            hmr_HSAH_Standard_Kit_Price__c = 222.22,
            Standard_HSS_Kit_Discount__c = 1.11,
            hmr_HSS_Standard_Kit_Price__c = 111.11
        );
        insert accountTest1;

        
        Contact testContactObj1 = new Contact(
            FirstName='test Name1',
            LastName = 'test contact1', 
            Email = 'test1@testing.com', 
            hmr_Contact_Original_Source__c = 'Referral',
            hmr_Contact_Original_Source_Detail__c = 'Friend',
            AccountId = accountTest1.Id
        );
        insert testContactObj1;
 

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile' LIMIT 1];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
        User userWithRole = new User(
                                       alias = 'hasrole', 
                                       email='userwithrole@roletest1.com', 
                                       //userroleid = rportalRole.id, 
                                       emailencodingkey='UTF-8', 
                                       lastname='Testing', 
                                       languagelocalekey='en_US',
                                       localesidkey='en_US', 
                                       profileid = p.id,
                                       timezonesidkey='America/Los_Angeles', 
                                       //ContactId = testContactObj1.Id,
                                       username='userwithrole@testorg.com');  
        Database.insert(userWithRole);
        
              
        System.runAs (userWithRole){            
            	String renderHMTL = LoginPageTemplate.getHTML(); 
        		System.Assert(renderHMTL != null);            
        }               	        
	} 
}