/**
* @Date: 2017-01-19
* @Author Mustafa Ahmed (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-1839
*/

@isTest(SeeAllData=False)

public class RefundObjTriggerTest {
static testMethod void CreateRecords(){
        
        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;
        
        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();        
       
        Contact testContactObj1 = new Contact(FirstName='test',LastName = 'Ahmed', Account = testAccountObj);
        conList.add(testContactObj1);            
        Contact testContactObj2 = new Contact(FirstName='test',LastName = 'Mustafa', Account = testAccountObj);
        conList.add(testContactObj2);  
        insert conList;      
                
        //create different kinds of Products and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();
        
        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj1); 
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);         
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'Chocolate Shake', ccrz__SKU__c = '333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj3);
        insert prdList;
        
        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();
        
        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj1);
        ccrz__E_Order__c testOrderObj2 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj2.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj2);        
        insert orderList;
        
        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();
        
        ccrz__E_OrderItem__c testOrderItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 3.50, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 3.50, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testOrderItemObj1);        
        ccrz__E_OrderItem__c testOrderItemObj2 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 3.50, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 2, ccrz__SubAmount__c= 7.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testOrderItemObj2);
        ccrz__E_OrderItem__c testOrderItemObj3 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj2.Id, ccrz__Price__c = 3.50, ccrz__Product__c = testProductObj3.Id, ccrz__Quantity__c = 3, ccrz__SubAmount__c= 10.50, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testOrderItemObj3);        
        insert orderItemList; 
    
      	//insert the Refunds with status as Approved
        List<Refund__c> refundList = new List<Refund__c>();
        
        Refund__c testRefundObj1 = new Refund__c(hmr_Contact__c = testContactObj1.Id, hmr_Return_Status__c = 'Requested', hmr_Reason_Code__c = 'Client Error', hmr_Refund_Type__c = 'Return');
        refundList.add(testRefundObj1);
        Refund__c testRefundObj2 = new Refund__c(hmr_Contact__c = testContactObj2.Id, hmr_Return_Status__c = 'Requested', hmr_Reason_Code__c = 'Client Error', hmr_Refund_Type__c = 'Return');
        refundList.add(testRefundObj2);        
        insert refundList;   
    
        //insert the Refund Items to the Refunds
        List<Refund_Item__c> refundItemList = new List<Refund_Item__c>();
        
        Refund_Item__c testRefundItemObj1 = new Refund_Item__c(hmr_Refund__c = testRefundObj1.Id, hmr_CC_Order__c = testOrderObj1.Id, CC_Order_Item__c = testOrderItemObj1.Id, hmr_Refund_Product_SKU__c = '111', Price_paid_by_client__c = 3.50, hmr_Refund_Product_Quanity__c = 1, hmr_Refund_Product_Quantity__c = 1);
        refundItemList.add(testRefundItemObj1);
    
        Refund_Item__c testRefundItemObj2 = new Refund_Item__c(hmr_Refund__c = testRefundObj1.Id, hmr_CC_Order__c = testOrderObj1.Id, CC_Order_Item__c = testOrderItemObj2.Id, hmr_Refund_Product_SKU__c = '222', Price_paid_by_client__c = 3.50, hmr_Refund_Product_Quanity__c = 1, hmr_Refund_Product_Quantity__c = 1);
        refundItemList.add(testRefundItemObj2);
    
        Refund_Item__c testRefundItemObj3 = new Refund_Item__c(hmr_Refund__c = testRefundObj2.Id, hmr_CC_Order__c = testOrderObj2.Id, CC_Order_Item__c = testOrderItemObj3.Id, hmr_Refund_Product_SKU__c = '333', Price_paid_by_client__c = 3.50, hmr_Refund_Product_Quanity__c = 1, hmr_Refund_Product_Quantity__c = 1);
        refundItemList.add(testRefundItemObj3);
                
        insert refundItemList;
    
    	for(Refund__c Refund : refundList){
            Refund.hmr_Return_Status__c = 'Approved';
        }
        update refundList;
    
     	if(!refundItemList.isEmpty()){
            System.assertEquals(refundItemList.size(), 3);
        }
    }  
}