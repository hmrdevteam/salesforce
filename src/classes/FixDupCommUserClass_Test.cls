/**
* @Date: 2017-07-20
* @Author Mustafa Ahmed (HMR)
*/

@isTest(SeeAllData=False)

public class FixDupCommUserClass_Test {

	@isTest static void testFixDupCommUserClass() {		
    
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con1 = new Contact(FirstName='test1',LastName ='testComCon1',AccountId = communityAc.Id);
			insert con1;

			Contact con2 = new Contact(FirstName='test2',LastName ='testComCon2',AccountId = communityAc.Id);
			insert con2;

			Contact con3 = new Contact(FirstName='test3',LastName ='testComCon3',AccountId = communityAc.Id);
			insert con3;

			Contact con4 = new Contact(FirstName='test4',LastName ='testComCon4',AccountId = communityAc.Id);
			insert con4;			

			User client1 = new User(Alias = 'client1', Email='client1@testorg.com',
															EmailEncodingKey='UTF-8', LastName='client1LN', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id, ContactId = con1.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='client1@testorg.com');

			User client2 = new User(Alias = 'client2', Email='client1@testorg.com',
															EmailEncodingKey='UTF-8', LastName='client2LN', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id, ContactId = con2.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='client2@testorg.com');

			User client3 = new User(Alias = 'client3', Email='client3@testorg.com',
															EmailEncodingKey='UTF-8', LastName='client3LN', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id, ContactId = con3.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='client3@testorg.com');

			User client4 = new User(Alias = 'client4', Email='client3@testorg.com',
															EmailEncodingKey='UTF-8', LastName='client4LN', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id, ContactId = con4.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='client4@testorg.com');

			insert new List<User>{client1, client2, client3, client4};
			
		Datetime datetimeofToday = Datetime.now();
		Datetime datetimeofYesterday = Datetime.now().addDays(-3);

		Test.setCreatedDate(client1.Id, datetimeofToday);
		Test.setCreatedDate(client2.Id, datetimeofYesterday);
		Test.setCreatedDate(client3.Id, datetimeofToday);
		Test.setCreatedDate(client4.Id, datetimeofYesterday);		  

		Test.startTest();

		Test.setCurrentPageReference(new PageReference('FixDupCommUser'));
		FixDupCommUserClass controllerObj = new FixDupCommUserClass();  
        controllerObj.getxtodaysUsers();

        System.assert(controllerObj.todaysUsers.size() >= 2);
		//System.assert(controllerObj.xtodaysUsers.size() >= 2);
		//System.assert(controllerObj.txtodaysUsers.size() >= 2);

        //System.assertEquals(controllerObj.todaysUsers.size(), 2);
        //System.assertEquals(controllerObj.xtodaysUsers.size(), 2);
        //System.assertEquals(controllerObj.txtodaysUsers.size(), 2);
        
        System.debug('test class todaysUsers ' + controllerObj.todaysUsers.size());
        System.debug('test class xtodaysUsers ' + controllerObj.xtodaysUsers.size());
        System.debug('test class txtodaysUser ' + controllerObj.txtodaysUsers.size());        

        Test.stopTest();
		}
}