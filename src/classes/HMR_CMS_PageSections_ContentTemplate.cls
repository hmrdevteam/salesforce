/**
* Apex Content Template Controller for Page Sections on Home Page
*
* @Date: 05/19/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_PageSections_ContentTemplate extends cms.ContentTemplateController{

	global HMR_CMS_PageSections_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_PageSections_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	public String SectionTitle{
        get{
            return getPropertyWithDefault('SectionTitle', 'Section Title');
        }
    }

	public String SectionDescription{
        get{
            return getPropertyWithDefault('SectionDescription', 'This is the Section Description');
        }
    }

	public String PickLinkOrButton {
        get{
            return getPropertyWithDefault('PickLinkOrButton', '[]');
        }
    }

	public String LinkOrButtonURLText{
        get{
            return getPropertyWithDefault('LinkOrButtonURLText', '');
        }
    }

	public cms.Link LinkOrButtonURLLinkObj{
        get{
            return getPropertyLink('LinkOrButtonURLLinkObj');
        }
    }

	public String LinkColorBlueOrWhite{
        get{
            return getPropertyWithDefault('LinkColorBlueOrWhite', '[]');
        }
    }

	public String FontColorBlackOrWhite{
        get{
            return getPropertyWithDefault('FontColorBlackOrWhite', '[]');
        }
    }

	public String CSSClassSelector{
        get{
            return getPropertyWithDefault('CSSClassSelector', '[]');
        }
    }

	public Boolean isLinkOrButtonForFoodPage {
		get {
            return getAttribute('isLinkOrButtonForFoodPage') == 'true';
        }
	}

	public String SectionGTMID{
        get{
            return getPropertyWithDefault('SectionGTMID', '');
        }
    }

	global virtual override String getHTML(){
		String html = '';
		String imageAlignment = CSSClassSelector.substringAfter('_');
		String emptyColumnEightLeft = '<div class="col-sm-8 section-right-first visible-lg visible-md"></div>';
		String emptyColumnEightRight = '<div class="col-sm-8 section-right-first visible-lg visible-md"></div>';
		if(imageAlignment == 'Left') {
			emptyColumnEightRight = '';
		}
		if(imageAlignment == 'Right') {
			emptyColumnEightLeft = '';
		}
		String linkOrButtonURL = (this.LinkOrButtonURLLinkObj != null) ? this.LinkOrButtonURLLinkObj.targetPage : '#';
		String linkOrButtonHTML = '';String sectionGTMIDAttr = ' ';
		if(SectionGTMID != null && SectionGTMID != '') {
			sectionGTMIDAttr = ' data-gtm-id="' + SectionGTMID + '" ';
		}
		if(isLinkOrButtonForFoodPage) {
			Id hmrProductsCategoryId = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Name = 'HMR Products'].Id;
			linkOrButtonURL = 'about-hmr-foods-nutrition-ingredients';
		}
		if(PickLinkOrButton == 'Link') {
			linkOrButtonHTML += '<div class="link-container">' +
									'<div class="hmr-allcaps-container">' +
										'<a' + sectionGTMIDAttr + 'href="' + linkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-' + LinkColorBlueOrWhite + '">' + LinkOrButtonURLText + '</a>' +
										'<a' + sectionGTMIDAttr + 'href="' + linkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-' + LinkColorBlueOrWhite + '">></a>' +
									'</div>' +
								'</div>';
		}
		if(PickLinkOrButton == 'Button'){
			linkOrButtonHTML += '<div class="btn-container">' +
		                            '<a' + sectionGTMIDAttr + 'href="' + linkOrButtonURL + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">' + LinkOrButtonURLText + '</a>' +
		                        '</div>';
		}
		if(PickLinkOrButton == 'None') {
			linkOrButtonHTML = '';
		}
		html += '<section data-c="' + CSSClassSelector + '" class="hmr-page-section bg-typecover ' + CSSClassSelector.substringBefore('_') + '">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                emptyColumnEightLeft +
			                '<div class="col-sm-4 section-right">' +
			                    '<div class="section-right-' + FontColorBlackOrWhite + '">' +
			                        '<h2 class="title">' + SectionTitle + '</h2>' +
			                        '<div class="blurb text-left">' +
			                            '<p>' + SectionDescription + '</p>' +
			                        '</div>' +
			                        linkOrButtonHTML +
			                    '</div>' +
			                '</div>' +
							emptyColumnEightRight +
			            '</div>' +
			        '</div>' +
			    '</section>';
		return html;
	}
}