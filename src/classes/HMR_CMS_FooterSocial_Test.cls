/*
Developer: Rafa Hernandez (Magnet360)
Date: March 21st 2017
Log: Missing Meaningful Asserts (if needed) and test for parameter method.
Target Class(ses): HMR_CMS_FooterSocial_ContentTemplate
*/

@isTest
private class HMR_CMS_FooterSocial_Test {
    
    @isTest static void test_general_method() {
        // Implement test code
        HMR_CMS_FooterSocial_ContentTemplate footerSocialTemplate = new HMR_CMS_FooterSocial_ContentTemplate();
        String propertyValue = footerSocialTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
        String connectTitleText = footerSocialTemplate.ConnectTitleText;
        String facebookText = footerSocialTemplate.FacebookText;
        cms.Link facebookObject = footerSocialTemplate.FacebookLinkObj;
        String twitterText = footerSocialTemplate.TwitterText;
        cms.Link twitterObject = footerSocialTemplate.TwitterLinkObj;
        String youtubeText = footerSocialTemplate.YouTubeText;
        cms.Link youtubeObject = footerSocialTemplate.YouTubeLinkObj;
        String renderHTML = footerSocialTemplate.getHTML();
        
        System.assert(!String.isBlank(renderHTML));
        
    }
    
    @isTest static void test_general_method_parameters() { //TODO 
        //cms.CreateContentController cc = new cms.CreateContentController();

        ////init controller with parameter
        //HMR_CMS_FooterSocial_ContentTemplate footerSocialTemplatewithParam = new HMR_CMS_FooterSocial_ContentTemplate(cc);
    }
}