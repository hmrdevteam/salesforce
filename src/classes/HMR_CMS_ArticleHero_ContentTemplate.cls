/**
* Apex Content Template for the Article Detail Hero Section
*
* @Date: 2017-07-21
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-21
* @JIRA: HPRP-4083
*/
global virtual class HMR_CMS_ArticleHero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_ArticleHero_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_ArticleHero_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	string articleName = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('name'));
    	string html = '';
    	
    	if(!String.isBlank(articleName)){
	    	HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);

	    	if(articleRecord != null){
		    	html = ' <div class="slim-hero bg-article-home-hero" style="background-image:url(' + articleRecord.HeroImage + ');">' +
	                      	'<div class="container">' +
					                '<div class="col-xs-12">' +      
			                            '<h6 class="white">' + articleRecord.DataCategoryDisplay + '</h6>' +
			                            '<h2 class="no-top-padding min-52 white">' + articleRecord.Title + '</h2>' +
					                '</div>' +
					                '<div class="col-xs-12 white hmr-allcaps">' +
					                   '<div class="hero-icons">' +
					                       '<span class="nowrap">' +
                                                '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockWhite.svg"> <div class="icon-text">' + articleRecord.ReadTime + ' MIN</div> ' +
                                            '</span>' +
                                            '<div class="verticle-divider"></div>' +
                                            '<span class="nowrap">' +
                                                '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartWhite.svg"> <div class="icon-text">' + articleRecord.Rating + '</div> ' +
                                            '</span>' +
					                    '</div>' +
					               '</div>' +
					        '</div>' +
					    '</div> ';
			}
		}

    	return html;
    }
}