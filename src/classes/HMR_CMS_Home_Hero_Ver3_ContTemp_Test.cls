/*
Developer: Ali Pierre (HMR)
Date: 2018-03-12
Modified: 
Modified Date : 
Target Class: HMR_CMS_Home_Hero_Ver3_ContentTemplate
*/

@isTest
public class HMR_CMS_Home_Hero_Ver3_ContTemp_Test{  

        
    @isTest static void test_general_method() { 

                
                HMR_CMS_Home_Hero_Ver3_ContentTemplate homePageHero = new HMR_CMS_Home_Hero_Ver3_ContentTemplate();
                String propertyValue = homePageHero.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                String TitleText = homePageHero.TitleText;                
                String SubTitleText = homePageHero.SubTitleText;
                Boolean showBestDietsFastWeightLoss = homePageHero.showBestDietsFastWeightLoss;                
                Boolean showBestDietsWeightLoss = homePageHero.showBestDietsWeightLoss;
                String Version = homePageHero.Version;                
                String sectionGTMIDAttr = homePageHero.sectionGTMIDAttr;
                
                homePageHero.testAttributes = new Map<String, String>  {
                    'PlansDetailsTitle' => 'title',
                    'SubTitleText' => 'desc',
                    'showBestDietsFastWeightLoss' => 'TRUE',
                    'showBestDietsWeightLoss' => 'TRUE',
                    'Version' => 'Recognize',
                    'sectionGTMIDAttr' => 'testGTM'
                };
                
                String renderHTML = homePageHero.getHTML();
                
                System.assert(!String.isBlank(renderHTML));                
    }
    
}