/**
* @Date: 5/25/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
 Class : HMR_CMS_RecipeDetailServiceInterface
*/


@isTest
private class HMR_CMS_RecipeDetailServInterface_Test{

  @isTest static void test_method_one() {
   
       Map<String, String> requestMap = new Map<String, String>();
       requestMap.put('action','getRecipeDetail');
       requestMap.put('urlName','testrecipe');
       
       hmr_Recipe__c recipeObj = new hmr_Recipe__c(VF_Servings__c = 2,hmr_Recipe_Category__c = 'Soups', Date_Published__c = Date.today());
       insert recipeObj;
      
      try{    
       Recipe__kav recipeTest = new Recipe__kav(Title = 'testrecipe', UrlName = 'testrecipe', Recipe__c = recipeObj.id, Language='en_US');
       insert recipeTest;

       Id articleId = [Select KnowledgeArticleId FROM Recipe__Kav where id = :recipeTest.id ].KnowledgeArticleId;
       
       
       KbManagement.PublishingService.publishArticle(articleId, true);    
       
       HMR_CMS_RecipeDetailServiceInterface recipeServ = new HMR_CMS_RecipeDetailServiceInterface();
       recipeServ.executeRequest(requestMap);   
       }
       catch(Exception ex){
           System.debug('ERRRRRORRR -> ' + ex.getMessage());
           System.debug('ERRRRRORRR STTTACCCCKKK -> ' + ex.getStackTraceString());
       }

    }  

}