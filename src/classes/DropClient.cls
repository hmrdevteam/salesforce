/**
* Apex Class for droping a client
* Managing (Search, Delete) Class Member and Session Attendee records
*
* Objects referenced --> Class_Member__c,Session_Attendee__c
*
* @Date: 2016-12-06
* @Author Utkarsh Goswami (Mindtree)
* @Modified: Ali Pierre (HMR) 7/21/2017
* @JIRA: Issue https://reside.jira.com/browse/HPRP-183, Task https://reside.jira.com/browse/HPRP-758
*/

global without sharing class DropClient {

   String classMemberId;
   public Class_Member__c classMemberDetail {get;set;}
   public String dropReason{get;set;}
   public Boolean closeWindow {get;set;}

   public DropClient(){
       classMemberId = ApexPages.currentpage().getparameters().get('Id');

       if(!String.isBlank(classMemberId)){
         List<Class_Member__c> classMemberDetailList = [SELECT hmr_Contact_Name__r.Name,
                                                               hmr_Class__r.Name,
                                                               hmr_Client_Drop_Reason__c
                                                        FROM Class_Member__c
                                                        WHERE Id = :classMemberId.trim()];

         if(classMemberDetailList.size() > 0)
            classMemberDetail = classMemberDetailList[0];
         else
            closeWindow = true;
       }
       else
         closeWindow = true;
   }

    public List<SelectOption> getReasons()
        {
          List<SelectOption> options = new List<SelectOption>();

           options.add(new SelectOption('---Select Reason---', '---Select Reason---'));

           Schema.DescribeFieldResult fieldResult = Class_Member__c.hmr_Client_Drop_Reason__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

           for( Schema.PicklistEntry f : ple)
              options.add(new SelectOption(f.getLabel(), f.getValue()));

           return options;
        }

   /**
    *   method to remove OR drop a client
    *  from a Class Member
    *
    *  @param String
    *  @return PageReference
    */
   public PageReference removeClient() {

      if(String.isBlank(dropReason)){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Provide Reason To Drop This Client'));
          closeWindow = false;
      }
      else{

          //getting the list of future session attendee records
          List<Session_Attendee__c> attendeeList = [SELECT Id
                                                    FROM Session_Attendee__c
                                                    WHERE hmr_Class_Member__r.hmr_Active__c = TRUE
                                                      AND hmr_Class_Member__c = :classMemberId.trim()
                                                      AND hmr_Coaching_Session__r.hmr_Class_Date__c >= today
                                                      AND hmr_Attendance__c != 'Attended'];

          Class_Member__c classMemberDetail = [SELECT hmr_Active__c,
                                                      hmr_Client_Drop_Reason__c,
                                                      hmr_Contact_Name__c,
                                                      hmr_Consented__c
                                               FROM Class_Member__c
                                               WHERE Id = :classMemberId.trim()];

          List<Consent_Agreement__c> consentAgreementList = [SELECT Contact__c,
                                                                Status__c,
                                                                Consent_Type__c,
                                                                Version__c
                                                         FROM Consent_Agreement__c
                                                         WHERE Contact__c = :classMemberDetail.hmr_Contact_Name__c
                                                              AND (Status__c = 'Consented' OR Status__c = 'Notified')
                                                              AND Consent_Type__c = 'Health'];

          Contact contactDetail = [SELECT hmr_Class_Notes__c,
                                          hmr_New_Class_Note__c
                                   FROM Contact
                                   WHERE Id = :classMemberDetail.hmr_Contact_Name__c];
          
          Program_Membership__c pmDetail;
          try{
                pmDetail = [SELECT hmr_Membership_Type__c
                              FROM Program_Membership__c
                              WHERE hmr_Contact__c = :contactDetail.Id
                              AND hmr_Status__c = 'Active'
                              LIMIT 1];
            }
            catch (exception e) {
                //Client does not have active program membership so do nothing
            }          

          //Clear Class Notes if Client is Dropped from Class
          contactDetail.hmr_Class_Notes__c = '';
          contactDetail.hmr_New_Class_Note__c = false;

          //Set clients consent agreement status to expired
          for(Consent_Agreement__c consentRecord : consentAgreementList)
              consentRecord.Status__c = 'Expired';

          //Set PM record Membership Type to Self-Directed
          if(pmDetail != null){
            pmDetail.hmr_Membership_Type__c = 'Self-Directed';
          }         

          //setting class member record to inactive
          classMemberDetail.hmr_Active__c = false;
          classMemberDetail.hmr_Client_Drop_Reason__c = dropReason;
          classMemberDetail.hmr_Consented__c = false;

           delete attendeeList;
           if(pmDetail != null){
               update pmDetail;
           }          
           update classMemberDetail;
           //update consentAgreementList;
           update contactDetail;
           closeWindow = true;
    }
      return null;
   }
}