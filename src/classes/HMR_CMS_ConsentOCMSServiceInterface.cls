/**
* This class is a controller consent agreement functionality
* @Date:4/4/2017
* @Author: Aslesha(Mindtree)
* @Updated by : Pranay Mistry (4/6/17)
* @JIRA:
*/

global class HMR_CMS_ConsentOCMSServiceInterface implements cms.ServiceInterface {

    public HMR_CMS_ConsentOCMSServiceInterface() {}

    public String executeRequest(Map<String, String> params) {
        Map<String, Object> responseString = new Map<String, Object>();
        try {
            String action = params.get('action');
            if(action == 'getConsent') {
                responseString.put(action, getConsent());
            }
            if(action == 'setConsent') {
                responseString.put(action, setConsent());
            }
            if(action != 'getConsent'  &&  action != 'setConsent'){
                responseString.put('success', false);
                responseString.put('message', 'Invalid Action');
                responseString.put('respdata', '');
            }
            responseString.put('success',true);
        }
        catch(Exception e) {
            responseString.put('success', false);
            responseString.put('exceptionMsg', e.getMessage());
            responseString.put('exceptiongetTypeName', e.getTypeName());
            responseString.put('exceptiongetLineNumber', e.getLineNumber());
            return JSON.serialize(responseString);
        }
        return JSON.serialize(responseString);
    }

    /**
     * Remote action to get the information about consent for particular contact
     * @param Id contactId : Id of the contact
     * @return Boolean : if there is any pending consent return true OR return false
     *
     * @Author: Aslesha (Mindtree)
     * @Date: 04/4/2017
    */
    global static Map<String, Object> getConsent() {
        Map<String, Object> returnMap = new Map<String, Object>();
        User loggedInUser = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        List<Consent_Agreement__c> conAgreeList =
            [SELECT Id, Name,Version__c FROM Consent_Agreement__c WHERE Contact__c =: loggedInUser.ContactId and Status__c = 'Notified'];
        returnMap.put('getConsentData', conAgreeList);
        return returnMap;
    }


    /**
     * Remote action to set the information about consent for particular contact
     * @param Id contactId : Id of the contact
     * @return Boolean : true if consent records are created &amp; false if not
     * @Author: Aslesha (Mindtree)
     * @Date: 04/4/2017
    */
    global static Map<String, Object> setConsent() {
        Map<String, Object> returnMap = new Map<String, Object>();
        try{
            User loggedInUser = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
            List<Consent_Agreement__c> conAgreeList =
                [SELECT Id, Name FROM Consent_Agreement__c WHERE Contact__c =: loggedInUser.ContactId and Status__c = 'Notified'];
            List<Consent_Agreement__c> updateConAgreeList = new List<Consent_Agreement__c>();

            if(conAgreeList.size() > 0) {
                for(Consent_Agreement__c agreement: conAgreeList){
                    agreement.Status__c = 'Consented';
                    agreement.Agreement_Timestamp__c = System.now();
                    updateConAgreeList.add(agreement);
                }
                update updateConAgreeList;
                returnMap.put('consentAgreementUpdated', true);
                returnMap.put('setConsentData', conAgreeList);
            }
        }
        catch(Exception e){
            returnMap.put('success', false);
            returnMap.put('exceptionMsg', e.getMessage());
            returnMap.put('exceptiongetTypeName', e.getTypeName());
            returnMap.put('exceptiongetLineNumber', e.getLineNumber());
            return returnMap;
        }
        return returnMap;
    }

    public static Type getType() {
        return HMR_CMS_ConsentOCMSServiceInterface.class;
    }
}