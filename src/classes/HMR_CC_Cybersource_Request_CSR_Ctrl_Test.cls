/**
* @Date: 4/7/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
* This test class covers HMR_CC_Cybersource_Request_CSR_Ctrl  Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CC_Cybersource_Request_CSR_Ctrl_Test{

  @isTest static void test_method_one() {
        
        PageReference pg = Page.HMR_CC_Cybersource_RequestForm;
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CC_Cybersource_Response')); 
        System.currentPageReference().getParameters().put('id', 'testvar');
        
        System.currentPageReference().getParameters().put('joinedSignedFields','testsigned');
        System.currentPageReference().getParameters().put('joinedUnsignedFields', 'testunsigned');
        
        List<String> signStr = new List<String>();
        List<String> unSignStr = new List<String>(); 
        
        HMR_CC_Cybersource_Request_CSR_Ctrl cybrsorceCSRReq = new HMR_CC_Cybersource_Request_CSR_Ctrl();
        
        cybrsorceCSRReq.signedFields = signStr;
        cybrsorceCSRReq.unsignedFields = unSignStr;
        cybrsorceCSRReq.signed = 'sign';
        cybrsorceCSRReq.unsigned = 'unsign';
        cybrsorceCSRReq.formAction = 'frmaction';
        cybrsorceCSRReq.selector = 'selector';
       
    }  

}