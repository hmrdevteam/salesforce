/**
* Test Class Coverage of the HMR_Contact_Handler Class
*
* @Date: 06/02/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/20/2017
* @JIRA:
*/
@isTest
private class HMR_CC_CheckOut_Extension_Test {
	@testSetup
    private static void setupOrderData(){
        cc_dataFactory.setupTestUser();
        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.getContactAddress();
        insert contactAddressRecord;
        cc_dataFactory.setupCatalog();
        cc_dataFactory.createCart();

        List<ccrz__E_Term__c> termList = new List<ccrz__E_Term__c>{new ccrz__E_Term__c(ccrz__Description__c = 'Consent Agreement',
                                                                                       ccrz__Enabled__c = true,
                                                                                       ccrz__Title__c = 'Consent',
                                                                                       ccrz__Type__c = 'Digital',
                                                                                       Consent_Type__c = 'Auto-Delivery',
                                                                                       Version_Number__c = 1),
            													   new ccrz__E_Term__c(ccrz__Description__c = 'Consent Agreement',
                                                                                       ccrz__Enabled__c = true,
                                                                                       ccrz__Title__c = 'Consent',
                                                                                       ccrz__Type__c = 'Digital',
                                                                                       Consent_Type__c = 'Health',
                                                                                       Version_Number__c = 1)};
        insert termList;

        Program__c programRecord = new Program__c(hmr_Phase_Type__c = 'P1',
                                                  hmr_Program_Type__c = 'Healthy Solutions',
                                                  hmr_Program_Display_Name__c = 'P1 Healthy Solutions',
                                                  Days_in_1st_Order_Cycle__c = 30,
                                                  Default_Promotional_Discount__c = 0.0,
                                                  hmr_Standard_Kit_Price__c = 301.65);
        insert programRecord;
    }

	@isTest
    private static void testConstructorInitialization() {
		Test.startTest();

        HMR_CC_CheckOut_Extension checkoutController = new HMR_CC_CheckOut_Extension();

        Test.stopTest();

        //Verify the current address string was initialized
        System.assert(!String.isBlank(checkoutController.curAddrString));
	}

    @isTest
    private static void testConstructorInitializationFromPage(){
        List<User> userList = [SELECT Id FROM User WHERE Alias = 'cctest'];

        Test.setCurrentPageReference(Page.HMR_CC_CheckOut_PageIncl);
        System.currentPageReference().getParameters().put('auth_code', '001');
        System.currentPageReference().getParameters().put('decision', 'DENIED');
        System.currentPageReference().getParameters().put('portalUser', userList[0].Id);

        Test.startTest();

        HMR_CC_CheckOut_Extension checkoutController = new HMR_CC_CheckOut_Extension();

        Test.stopTest();

        //Verify the current address string was initialized
        System.assert(!String.isBlank(checkoutController.curAddrString));
    }

    @isTest
    private static void testUpdateCartForP2(){
        List<ccrz__E_Cart__c> cartList = [SELECT ccrz__User__c,
                                          		 ccrz__EncryptedId__c
                                          FROM ccrz__E_Cart__c];
        Date processDate = Date.today();
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.updateCartForP2(ctx, cartList[0].ccrz__EncryptedId__c, processDate);

        Test.stopTest();

        //Verify the result was a success and cart was updated
        ccrz__E_Cart__c cartResultRecord = [SELECT P2_1st_Order_Date__c,
                                           		   P2_1st_Order_Delay__c
                                            FROM ccrz__E_Cart__c WHERE Id =: cartList[0].Id];
        System.assert(result.success);
        System.assertEquals(processDate, cartResultRecord.P2_1st_Order_Date__c);
        System.assert(cartResultRecord.P2_1st_Order_Delay__c);
    }

    @isTest
    private static void testCheckoutAsClient(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz__E_Term__c termRecord = [SELECT Id FROM ccrz__E_Term__c LIMIT 1];

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.checkOut(ctx, new List<String>{termRecord.Id}, contactRecord.Id, false);

        Test.stopTest();

        //Verify the consent records were created as consented
        List<Consent_Agreement__c> consentAgreementList = [SELECT Status__c FROM Consent_Agreement__c];
        for(Consent_Agreement__c consentRecord : consentAgreementList)
            System.assert(consentRecord.Status__c == 'Consented');
    }

    @isTest
    private static void testCheckoutAsCustomerServiceRepresentative(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz__E_Term__c termRecord = [SELECT Id FROM ccrz__E_Term__c LIMIT 1];

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.checkOut(ctx, new List<String>{termRecord.Id}, contactRecord.Id, true);

        Test.stopTest();

        //Verify the consent records were created as notified
        List<Consent_Agreement__c> consentAgreementList = [SELECT Status__c FROM Consent_Agreement__c];
        for(Consent_Agreement__c consentRecord : consentAgreementList)
            System.assert(consentRecord.Status__c == 'Notified');
    }

	@isTest
    private static void testGetCCTermsWhenNoneExist(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.getCCTerms(ctx, contactRecord.Id, new List<String>());

        Test.stopTest();

        System.assert(result != null);
    }

    @isTest
    private static void testGetCCTermsWhenConsentedRecordsExist(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        List<ccrz__E_Product__c> productList = [SELECT Program__c FROM ccrz__E_Product__c];
        List<Program__c> programList = [SELECT Id FROM Program__c];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        List<Consent_Agreement__c> consentAgreementList = new List<Consent_Agreement__c>();
        List<String> cartProductIdList = new List<String>();

        //Assign a Program to the Products
        for(ccrz__E_Product__c productRecord : productList){
            productRecord.Program__c = programList[0].Id;
            cartProductIdList.add(productRecord.Id);
        }

        update productList;

        for(ccrz__E_Term__c consentTermRecord : [SELECT Id FROM ccrz__E_Term__c]){
            consentAgreementList.add(new Consent_Agreement__c(Contact__c = contactRecord.Id,
                                                              Version__c = consentTermRecord.Id,
                                                              Status__c = 'Consented',
                                                              Agreement_Timestamp__c = DateTime.now()));
        }

        insert consentAgreementList;

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.getCCTerms(ctx, contactRecord.Id, cartProductIdList);

        Test.stopTest();

        System.assert(result != null);
    }

    @isTest
    private static void testGetCCTermsWithNullContactId(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.getCCTerms(ctx, null, new List<String>());

        Test.stopTest();

        System.assert(result != null);
    }

    @isTest
    private static void testAuthorizePaymentSuccess(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.setMock(HttpCalloutMock.class, new HMR_Cybersource_CalloutMock(200, 'Success', 'Success', new Map<String, String>()));

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizePayment(ctx, new Map<String, String>(), new Map<String, String>());

        Test.stopTest();

        //Verify authorization was successful
        System.assert(result.success);
    }

    @isTest
    private static void testAuthorizePaymentUnsuccessful(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizePayment(ctx, new Map<String, String>(), new Map<String, String>());

        Test.stopTest();

        //Verify authorization was unsuccessful
        System.assert(!result.success);
    }

    @isTest
    private static void testAuthorizePaymentAndCreatePaymentTokenWithHttpUnsuccess(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.setMock(HttpCalloutMock.class, new HMR_Cybersource_CalloutMock(500, 'Denied', 'Denied', new Map<String, String>()));

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizePaymentAndCreatePaymentToken(ctx, new Map<String, String>(), new Map<String, String>());

        Test.stopTest();

        System.assert(!result.success);
    }

    @isTest
    private static void testAuthorizePaymentAndCreatePaymentTokenUnsuccessful(){
        Contact contactRecord = [SELECT Id FROM Contact LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizePaymentAndCreatePaymentToken(ctx, new Map<String, String>(), new Map<String, String>());

        Test.stopTest();

        System.assert(!result.success);
    }

    @isTest
    private static void testAddBillingAddress(){
        Account accountRecord = [SELECT Id FROM Account WHERE Name =: cc_DataFactory.ACCOUNT_NAME LIMIT 1];
        User userRecord = [SELECT Id FROM User WHERE Alias = 'cctest' LIMIT 1];
        Map<String, String> billingAddressMap = new Map<String, String>{'bill_to_forename' => 'Bill',
            															'bill_to_surname' => 'DeGrasi',
            															'bill_to_address_line1' => '111 Main Street',
            															'bill_to_address_city' => 'Tucson',
            															'bill_to_address_state' => 'Arizona',
            															'bill_to_address_state_code' => 'AZ',
            															'bill_to_address_postal_code' => '85741'};

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        Map<String, Object> resultMap = HMR_CC_CheckOut_Extension.addBillingAddress(userRecord.Id, accountRecord.Id, billingAddressMap);

        Test.stopTest();

        System.assert(resultMap.size() > 0);
    }

    @isTest
    private static void testAuthorizeUsingPaymentTokenSuccess(){
        Account accountRecord = [SELECT Id FROM Account LIMIT 1];
        User userRecord = [SELECT Id FROM User LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.setMock(HttpCalloutMock.class, new HMR_Cybersource_CalloutMock(200, 'Success', 'Success', new Map<String, String>()));

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizeUsingPaymentToken(ctx, new Map<String, String>(), '');

        Test.stopTest();

        System.assert(result.success);
    }

    @isTest
    private static void testAuthorizeUsingPaymentTokenUnsuccessful(){
        Account accountRecord = [SELECT Id FROM Account LIMIT 1];
        User userRecord = [SELECT Id FROM User LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizeUsingPaymentToken(ctx, new Map<String, String>(), '');

        Test.stopTest();

        System.assert(!result.success);
    }

    @isTest
    private static void testAuthorizeUsingBillingAddressAndPaymentTokenSuccess(){
        Account accountRecord = [SELECT Id FROM Account LIMIT 1];
        User userRecord = [SELECT Id FROM User LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.setMock(HttpCalloutMock.class, new HMR_Cybersource_CalloutMock(200, 'Success', 'Success', new Map<String, String>()));

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizeUsingBillingAddressAndPaymentToken(ctx, new Map<String, String>(), '');

        Test.stopTest();

        System.assert(result.success);
    }

    @isTest
    private static void testAuthorizeUsingBillingAddressAndPaymentTokenUnsuccessful(){
        Account accountRecord = [SELECT Id FROM Account LIMIT 1];
        User userRecord = [SELECT Id FROM User LIMIT 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.authorizeUsingBillingAddressAndPaymentToken(ctx, new Map<String, String>(), '');

        Test.stopTest();

        System.assert(!result.success);
    }

    @isTest
    private static void testCreateStoredPaymentUnsuccessful(){
        System.runAs(cc_dataFactory.testUser){
            Contact contactRecord = cc_dataFactory.testUser.Contact;
            ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.getContactAddress();
            insert contactAddressRecord;

            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

            Map<String, String> paymentInformationMap = new Map<String, String>{'contactAddressId'=>contactAddressRecord.Id,
                																'contactFirstName'=>contactRecord.FirstName,
                																'contactLastName'=>contactRecord.LastName,
                                                                                'expirationMonth'=>'01',
                                                                                'expirationYear'=>String.valueOf((Date.today().year() + 5))};

                Test.startTest();

                ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.createStoredPayment(ctx, paymentInformationMap);

                Test.stopTest();

                //System.assert(!result.success);
        }
    }

    @isTest
    private static void testGetStoredPaymentBillingAddressRecordByTokenUnsuccessful(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.getStoredPaymentBillingAddressRecordByToken(ctx, '');

        Test.stopTest();

        System.assert(!result.success);
    }

    @isTest
    private static void testGetStoredPaymentRecords(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

        Test.startTest();

        ccrz.cc_RemoteActionResult result = HMR_CC_CheckOut_Extension.getStoredPaymentRecords(ctx);

        Test.stopTest();

        System.assert(result.success);
    }
}