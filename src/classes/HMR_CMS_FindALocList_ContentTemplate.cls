/**
*
* @Date: 2017-07-14
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 2017-07-17 Zach Engman (Mindtree)
* @JIRA: HPRP-4039
*/
global virtual with sharing class HMR_CMS_FindALocList_ContentTemplate extends cms.ContentTemplateController{
   global HMR_CMS_FindALocList_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    global HMR_CMS_FindALocList_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    global virtual override String getHTML() {
        String html = '';
        Map<String, List<Account>> stateMap = getStateMap();
        Set<String> stateSet = stateMap.keySet();
        
        List<String> stateList = new List<String>();
        stateList.addAll(stateSet);
        stateList.sort();

        html += '<div class="find-a-location tail">' +
                    '<div class="container">';

        for(String state : stateList){
                html += '<div class="row">' +
                            '<div class="location-container">' +
                                '<div class="col-xs-12 location-row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="trigger js-trigger">'+ state + '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 location-items js-location-items">' +
                                    '<div class="col-xs-12">' +
                                        generateSectionContent(stateMap.get(state)) +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
        }

        html +=     '</div>' +
                '</div>';

        return html;
    }

    public String generateSectionContent(List<Account> accountList){
        String html = '';

        for(Account accountRecord : accountList){
            String mainAccountName = String.isBlank(accountRecord.hmr_Account_Name_1_Optional__c) ? accountRecord.Name : accountRecord.hmr_Account_Name_1_Optional__c;
            String[] addressLines = accountRecord.BillingStreet.split('\n');
            String addressLine2 = '';

            //Try to parse/separate out the second address line if one exists
            if(addressLines.size() == 1)
                addressLines = accountRecord.BillingStreet.split(',');

            if(addressLines.size() > 1){
                for(integer index = 1; index < addressLines.size(); index++)
                    addressLine2 += addressLines[index] + ' ';

                addressLine2 = addressLine2.trim();
            }

            html += '<div class="col-sm-4 location">' +
                        '<h3>'+ mainAccountName + '</h3>';

                if(!mainAccountName.equals(accountRecord.Name))
                    html += '<p>' + accountRecord.Name + '</p>';

                html += '<p>'+ addressLines[0] + '</p>' +
                        '<p>' + addressLine2 + '</p>' +
                        '<p>' + accountRecord.BillingCity + ', '+ accountRecord.BillingState + ' ' + accountRecord.BillingPostalCode +'</p>' +
                        '<p>' + HMR_Format_Utility.phone(accountRecord.Phone,'.') + '</p>' +
                    '</div>';
        }

        return html;
    }

    private Map<String, List<Account>> getStateMap(){
        Map<String, List<Account>> stateMap = new Map<String, List<Account>>();
        for(Account accountRecord : [SELECT   Name,
                                              Full_Billing_State__c,
                                              BillingStreet,
                                              BillingCity,
                                              BillingState,
                                              BillingPostalCode,
                                              BillingCountry,
                                              BillingLatitude,
                                              BillingLongitude,
                                              Phone,
                                              hmr_Account_Name_1_Optional__c
                                      FROM Account
                                      WHERE RecordType.DeveloperName = 'Professional_Account'
                                            AND hmr_Referral__c != 'DNR' 
                                            AND (hmr_Location_Type__c = 'Product & Services(P&S)'
                                            OR hmr_Location_Type__c = 'HMR Training Center')
                                      ORDER BY BillingCity]){
                                      
            
            if(!String.isBlank(accountRecord.Full_Billing_State__c)){
                if(!stateMap.containsKey(accountRecord.Full_Billing_State__c)){
                    stateMap.put(accountRecord.Full_Billing_State__c, new List<Account>{accountRecord});
                }
                else{
                    List<Account> accountList = stateMap.get(accountRecord.Full_Billing_State__c);
                    accountList.add(accountRecord);
                    stateMap.put(accountRecord.Full_Billing_State__c, accountList);
                }
            }
        }

        return stateMap;
    }
}