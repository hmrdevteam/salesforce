/**
* Unit Test for HMR_ChatterCommentEmailUtility
*
* Objects referenced --> User, Account, Contact, Recipe__kav, Blog_Post__kav, Success_Story__kav, FAQ__kav, FeedItem, FeedComment
*
* @Date: 1/31/2016
* @Author Nathan Anderson (M360)
* @Modified: 8/11/2016
* @JIRA: Issue https://reside.jira.com/browse/1461
*/

@isTest(SeeAllData=true)
private class ArticleCommentAlerts_Test {
    @isTest static void testRecipeComments() {
    // Testing Data creation
			Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
			insert con;

			User client = new User(Alias = 'client', Email='commuser321_SCTest@testorg.com',
															EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='commuser@org.com',
                                                            ContactId=con.Id);

            insert client;

			// User moderator = new User(Alias = 'cmod', Email='moduser321_SCTest@testorg.com',
			// 													EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',
			// 													LocaleSidKey='en_US', ProfileId = prof.Id,
			// 													TimeZoneSidKey='America/Los_Angeles', UserName='moduser@org.com');
            //
            // insert moderator;
            //
			// PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Community_Moderator' LIMIT 1];
            //
			// PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=moderator.Id, PermissionSetId=ps.Id);
            //
            // //insert psa;

			Recipe__kav recipeArticle = new Recipe__kav(Language = 'en_US', UrlName='test12345', Title='Test12345', Submitted_By__c=con.id);

			insert recipeArticle;

			recipeArticle = [SELECT KnowledgeArticleId FROM Recipe__kav WHERE Id = :recipeArticle.Id];
			KbManagement.PublishingService.publishArticle(recipeArticle.KnowledgeArticleId, true);

			FeedItem recipeComment = new FeedItem(CreatedById = client.Id,
                                                    Body = 'Test Comment',
													ParentId = recipeArticle.KnowledgeArticleId,
													Type = 'TextPost',
													Visibility = 'AllUsers');

        // Testing
        Test.startTest();

            insert recipeComment;

        Test.stopTest();

    }

    @isTest static void testblogComments() {
    // Testing Data creation
			Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
			insert con;

			User client = new User(Alias = 'client', Email='commuser321_SCTest@testorg.com',
															EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='commuser@org.com',
                                                            ContactId=con.Id);

            insert client;


			Blog_Post__kav blogArticle = new Blog_Post__kav(Language = 'en_US', UrlName='test23456', Title='Test23456');

			insert blogArticle;

            Blog_Post__DataCategorySelection blogDC = new Blog_Post__DataCategorySelection(DataCategoryGroupName='HMR_Knowledge', DataCategoryName='Eating_Well', ParentId=blogArticle.Id);

            insert blogDC;

			blogArticle = [SELECT KnowledgeArticleId FROM Blog_Post__kav WHERE Id = :blogArticle.Id LIMIT 1];
			KbManagement.PublishingService.publishArticle(blogArticle.KnowledgeArticleId, true);


			FeedItem blogComment = new FeedItem(CreatedById = client.Id,
													Body = 'Test Comment',
													ParentId = blogArticle.KnowledgeArticleId,
													Type = 'TextPost',
													Visibility = 'AllUsers');



        // Testing
        Test.startTest();

			insert blogComment;

        Test.stopTest();

    }

    @isTest static void testFAQComments() {
    // Testing Data creation
			Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
			insert con;

			User client = new User(Alias = 'client', Email='commuser321_SCTest@testorg.com',
															EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='commuser@org.com',
                                                            ContactId=con.Id);

            insert client;


            FAQ__kav faqArticle = new FAQ__kav(Language = 'en_US', UrlName='test34567', Title='Test34567');

			insert faqArticle;

            FAQ__DataCategorySelection faqDC = new FAQ__DataCategorySelection(DataCategoryGroupName='HMR_Knowledge', DataCategoryName='Eating_Well', ParentId=faqArticle.Id);

            insert faqDC;

			faqArticle = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :faqArticle.Id LIMIT 1];
			KbManagement.PublishingService.publishArticle(faqArticle.KnowledgeArticleId, true);


            FeedItem faqComment = new FeedItem(CreatedById = client.Id,
                                                    Body = 'Test Comment',
													ParentId = faqArticle.KnowledgeArticleId,
													Type = 'TextPost',
													Visibility = 'AllUsers');


        // Testing
        Test.startTest();

            insert faqComment;

        Test.stopTest();

    }

    @isTest static void testSSComments() {
    // Testing Data creation
			Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
			insert con;

			User client = new User(Alias = 'client', Email='commuser321_SCTest@testorg.com',
															EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='commuser@org.com',
                                                            ContactId=con.Id);

            insert client;

            Success_Story__kav ssArticle = new Success_Story__kav(Language = 'en_US', UrlName='test45678', Title='Test45678');

			insert ssArticle;

			ssArticle = [SELECT KnowledgeArticleId FROM Success_Story__kav WHERE Id = :ssArticle.Id LIMIT 1];
			KbManagement.PublishingService.publishArticle(ssArticle.KnowledgeArticleId, true);


			FeedItem ssComment = new FeedItem(CreatedById = client.Id,
													Body = 'Test Comment',
													ParentId = ssArticle.KnowledgeArticleId,
													Type = 'TextPost',
													Visibility = 'AllUsers');


        // Testing
        Test.startTest();

            insert ssComment;

        Test.stopTest();

    }

    @isTest static void testQuestions() {
    // Testing Data creation
			Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];
			Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

			Account communityAc = new Account(name ='testComunity') ;
			insert communityAc;

			Contact con = new Contact(FirstName='test',LastName ='testComCon',AccountId = communityAc.Id);
			insert con;

			User client = new User(Alias = 'client', Email='commuser321_SCTest@testorg.com',
															EmailEncodingKey='UTF-8', LastName='Testing321SCTest', LanguageLocaleKey='en_US',
															LocaleSidKey='en_US', ProfileId = commProf.Id,
															TimeZoneSidKey='America/Los_Angeles', UserName='commuser@org.com',
                                                            ContactId=con.Id);

            insert client;



            FeedItem question = new FeedItem(CreatedById = client.Id,
                                                    Title = 'Test Question',
                                                    Body = 'Test Comment',
                                                    ParentId = communityAc.Id,
                                                    Type = 'QuestionPost',
                                                    Visibility = 'AllUsers');

        // Testing
        Test.startTest();

            insert question;


            FeedComment reply = new FeedComment(CreatedById = client.Id,
                                                    CommentBody = 'Test',
                                                    CommentType = 'TextComment',
                                                    FeedItemId = question.Id);

            insert reply;

        Test.stopTest();


    }
}