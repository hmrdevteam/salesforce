/**
 * test class for HMRMobDeviceToken
 * @author Javier Arroyo
 * @see HMRMobDeviceToken
 */
@isTest
public class HMRMobDeviceTokenTest {
    
    @isTest
    public static void testNewToken() {
        User userRecord = cc_dataFactory.testUser;
        Contact contact = cc_dataFactory.testUser.Contact;        
        System.debug('===> contact id ' + contact.Id );
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.httpMethod = 'POST';
        req.requestURI = 'services/apexrest/mobile/user/token';
        req.params.put('contactId',  contact.Id);
        //req.requestBody = Blob.valueOf(JSON.serialize(HMRDeviceTokenDTO.getTestToken()));
        
        RestContext.request = req;
        RestContext.response = res;
        
        System.runAs(userRecord) {
            Test.startTest();
            HMRMobDeviceToken.doPost(HMRDeviceTokenDTO.getTestTokenIos().get(0));
            Integer sResp = res.statusCode;
            //System.debug('===> status code ' + sResp );
            System.assertEquals(201, sResp);
            
            HMRMobDeviceToken.doPost(HMRDeviceTokenDTO.getTestTokenAndroid().get(0));
            sResp = res.statusCode;
            System.assertEquals(201, sResp);
        
            Test.stopTest();
        }
    }
}