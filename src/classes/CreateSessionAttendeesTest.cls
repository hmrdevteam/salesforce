/**
* Unit Test for Trigger Handler for CreateSessionAttendees.trigger
*
* Objects referenced --> Class_Member__c,Class__c, Coaching_Session__c, Contact, Session_Attendee__c
*
* @Date: 2016-12-06
* @Author Aslesha Kokate (Mindtree)
* @Modified: Pranay Mistry on 2016-12-07
* @JIRA: Issue https://reside.jira.com/browse/HPRP-189, Task https://reside.jira.com/browse/HPRP-677
*/
@isTest (seeAllData=false)
public class CreateSessionAttendeesTest {
	//start of Pranay's changes
	static testMethod void TestClassMemberIsActive() {
		//Need to create Users
		//Need to create Coaches
		//Need to create Programs
		//Need to Create Accounts
		//Need to create Contacts
		//Need to create classes
		//Need to create Class Member records with Active field true
		//Verify the session Future Attendee records are created

		Test.startTest();

		//Declare variable to store today's date
        Date dt = Date.today();

		//Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Declare a new list to store Users
        List<User> userList = new List<User>();
        //Create a Coach Users
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        userList.add(userObj);
        User userObj1 = new User(FirstName = 'Test', LastName='Coach1', Email='Test1@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1231@testing.com', Alias = 'Test1');
        userList.add(userObj1);
        User userObj2 = new User(FirstName = 'Test', LastName='Coach2', Email='Test2@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1232@testing.com', Alias = 'Test2');
        userList.add(userObj2);
        //Insert User List
        insert userList;

        //Declare a new list to store Coaches
        List<Coach__c> coachList = new List<Coach__c>();
        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        coachList.add(coachObj);
        Coach__c coachObj1 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test1@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj1);
        Coach__c coachObj2 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test2@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj2);
        //Insert Coach
        insert coachList;

        //Declare a new list to store Programs
        List<Program__c> progList = new List<Program__c>();
        //Create new Programs and add them to conList to insert into the database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', hmr_Program_Display_Name__c = 'TestProg1HSS',
        									hmr_Program_Description__c = 'P1 Healthy Shakes Description', hmr_Program_Type__c = 'Healthy Shakes',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        progList.add(programObj1);
        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', hmr_Program_Display_Name__c = 'TestProg1HSAH',
        									hmr_Program_Description__c = 'Test Prog 1 HSS Description', hmr_Program_Type__c = 'Healthy Solutions',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj2);
        Program__c programObj3 = new Program__c(Name = 'Phase 2', hmr_Program_Display_Name__c = 'TestProg2',
        									hmr_Program_Description__c = 'Test Prog Phase 2 Description', hmr_Program_Type__c = 'P2',
        									hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj3);
        //insert List of Programs
        insert progList;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');
        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();
        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj1);
        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj2);
        Contact testContatObj3 = new Contact(FirstName = 'P2', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj3);
        Contact testContatObj4 = new Contact(FirstName = 'P2R', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj4);
        Contact testContatObj5 = new Contact(FirstName = 'P1HSSR', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj5);
        Contact testContatObj6 = new Contact(FirstName = 'P1HSSRcls', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj6);
        //List of contacts
        insert conList;

        //Declare a new List of Class object
        List<Class__c> clsList = new List<Class__c>();
        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Shakes Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj1);
        Class__c classObj = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Solutios Class', hmr_Coach__c = coachObj1.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Evening',hmr_Start_Time__c = '5:00 PM', hmr_End_Time__c = '6:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj);
        Class__c classObj2 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P2 Class', hmr_Coach__c = coachObj2.id, Program__c = programObj3.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P2 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Afternoon',hmr_Start_Time__c = '3:00 PM', hmr_End_Time__c = '4:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj2);
        //Insert Classes
        insert clsList;

        //Declare a new List of Class Member object
        List<Class_Member__c> clsMemberList = new List<Class_Member__c>();
		//Create Classe members to be added to clsMemberList and inserted into the Database
        Class_Member__c clsMemberObj1 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj1.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj1.Id, hmr_Start_Date__c = dt.addDays(-2));
        clsMemberList.add(clsMemberObj1);
        Class_Member__c clsMemberObj2 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj1.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj2.Id, hmr_Start_Date__c = dt.addDays(2));
        clsMemberList.add(clsMemberObj2);
        Class_Member__c clsMemberObj3 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj3.Id, hmr_Start_Date__c = dt.addDays(3));
        clsMemberList.add(clsMemberObj3);
        Class_Member__c clsMemberObj4 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj4.Id, hmr_Start_Date__c = dt.addDays(-3));
        clsMemberList.add(clsMemberObj4);
        Class_Member__c clsMemberObj5 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj2.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj5.Id, hmr_Start_Date__c = dt.addDays(3));
        clsMemberList.add(clsMemberObj5);
        Class_Member__c clsMemberObj6 = new Class_Member__c(hmr_Active__c = true, hmr_Class__c = classObj2.Id, hmr_Consented__c = true,
        													hmr_Contact_Name__c = testContatObj6.Id, hmr_Start_Date__c = dt.addDays(-3));
        clsMemberList.add(clsMemberObj6);
        insert clsMemberList;

        List<Session_Attendee__c> sessionAttendeeList1 =
        								[Select Id, Name, hmr_Class_Member__c, hmr_Class_Date__c FROM Session_Attendee__c
        									WHERE hmr_Class_Member__c = :clsMemberObj1.Id AND hmr_Class_Date__c >= TODAY];
      	//System.assert(sessionAttendeeList1.size() > 0,'Session Attendees are created');


      	clsMemberObj1.hmr_Active__c = false;
      	update clsMemberObj1;

      	Test.stopTest();
	}
	static testMethod void TestClassMemberIsNotActive() {
		//Need to create Users
		//Need to create Coaches
		//Need to create Programs
		//Need to Create Accounts
		//Need to create Contacts
		//Need to create classes
		//Need to create Class Member records with Active field false
		//Verify the session Future Attendee records gets deleted

		Test.startTest();

		//Declare variable to store today's date
        Date dt = Date.today();

		//Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //Declare a new list to store Users
        List<User> userList = new List<User>();
        //Create a Coach Users
        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        userList.add(userObj);
        User userObj1 = new User(FirstName = 'Test', LastName='Coach1', Email='Test1@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1231@testing.com', Alias = 'Test1');
        userList.add(userObj1);
        User userObj2 = new User(FirstName = 'Test', LastName='Coach2', Email='Test2@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test1232@testing.com', Alias = 'Test2');
        userList.add(userObj2);
        //Insert User List
        insert userList;

        //Declare a new list to store Coaches
        List<Coach__c> coachList = new List<Coach__c>();
        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        coachList.add(coachObj);
        Coach__c coachObj1 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test1@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj1);
        Coach__c coachObj2 = new Coach__c(hmr_Coach__c = userObj1.id, hmr_Email__c = 'test2@testing.com', hmr_Class_Strength__c = 30);
        coachList.add(coachObj2);
        //Insert Coach
        insert coachList;

        //Declare a new list to store Programs
        List<Program__c> progList = new List<Program__c>();
        //Create new Programs and add them to conList to insert into the database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', hmr_Program_Display_Name__c = 'TestProg1HSS',
        									hmr_Program_Description__c = 'P1 Healthy Shakes Description', hmr_Program_Type__c = 'Healthy Shakes',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        progList.add(programObj1);
        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', hmr_Program_Display_Name__c = 'TestProg1HSAH',
        									hmr_Program_Description__c = 'Test Prog 1 HSS Description', hmr_Program_Type__c = 'Healthy Solutions',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj2);
        Program__c programObj3 = new Program__c(Name = 'Phase 2', hmr_Program_Display_Name__c = 'TestProg2',
        									hmr_Program_Description__c = 'Test Prog Phase 2 Description', hmr_Program_Type__c = 'P2',
        									hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj3);
        //insert List of Programs
        insert progList;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');
        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();
        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj1);
        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj2);
        Contact testContatObj3 = new Contact(FirstName = 'P2', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj3);
        Contact testContatObj4 = new Contact(FirstName = 'P2R', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj4);
        Contact testContatObj5 = new Contact(FirstName = 'P1HSSR', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj5);
        Contact testContatObj6 = new Contact(FirstName = 'P1HSSRcls', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj6);
        //List of contacts
        insert conList;

        //Declare a new List of Class object
        List<Class__c> clsList = new List<Class__c>();
        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Shakes Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj1);
        Class__c classObj = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Solutios Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Evening',hmr_Start_Time__c = '5:00 PM', hmr_End_Time__c = '6:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj);
        Class__c classObj2 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P2 Class', hmr_Coach__c = coachObj1.id, Program__c = programObj3.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P2 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Afternoon',hmr_Start_Time__c = '3:00 PM', hmr_End_Time__c = '4:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        clsList.add(classObj2);
        //Insert Classes
        insert clsList;

        //Declare a new List of Class Member object
        List<Class_Member__c> clsMemberList = new List<Class_Member__c>();
		//Create Classe members to be added to clsMemberList and inserted into the Database
        Class_Member__c clsMemberObj1 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj1.Id,
        													hmr_Contact_Name__c = testContatObj1.Id, hmr_Start_Date__c = dt.addDays(-2));
        clsMemberList.add(clsMemberObj1);
        Class_Member__c clsMemberObj2 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj1.Id,
        													hmr_Contact_Name__c = testContatObj2.Id, hmr_Start_Date__c = dt.addDays(2));
        clsMemberList.add(clsMemberObj2);
        Class_Member__c clsMemberObj3 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj.Id,
        													hmr_Contact_Name__c = testContatObj3.Id, hmr_Start_Date__c = dt.addDays(3));
        clsMemberList.add(clsMemberObj3);
        Class_Member__c clsMemberObj4 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj.Id,
        													hmr_Contact_Name__c = testContatObj4.Id, hmr_Start_Date__c = dt.addDays(-3));
        clsMemberList.add(clsMemberObj4);
        Class_Member__c clsMemberObj5 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj2.Id,
        													hmr_Contact_Name__c = testContatObj5.Id, hmr_Start_Date__c = dt.addDays(3));
        clsMemberList.add(clsMemberObj5);
        Class_Member__c clsMemberObj6 = new Class_Member__c(hmr_Active__c = false, hmr_Class__c = classObj2.Id,
        													hmr_Contact_Name__c = testContatObj6.Id, hmr_Start_Date__c = dt.addDays(-3));
        clsMemberList.add(clsMemberObj6);
        insert clsMemberList;

        List<Session_Attendee__c> sessionAttendeeList1 =
        								[Select Id, Name, hmr_Class_Member__c, hmr_Class_Date__c FROM Session_Attendee__c
        									WHERE hmr_Class_Member__c = :clsMemberObj1.Id AND hmr_Class_Date__c >= TODAY];
      	System.assert(sessionAttendeeList1.size() == 0,'Session Attendees are not created');

      	Test.stopTest();
	}
	//end of Pranay's Changes
}