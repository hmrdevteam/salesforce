/**
* Apex Content Template Controller for Page Sections on Home Page
*
* @Date: 01/18/2018
* @Author Jay Zincone (HMR)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_Marketing_2Col_ContentTemplate extends cms.ContentTemplateController{

	global HMR_CMS_Marketing_2Col_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_Marketing_2Col_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    /** Left Section */
    public String LeftImageURL{
        get{return getPropertyWithDefault('LeftImageURL', 'URL of left image');}
    }

	public String LeftTitle{
        get{return getPropertyWithDefault('LeftTitle', 'Left Section Title');}
    }

	public String LeftDescription{
        get{return getPropertyWithDefault('LeftDescription', 'This is the Left Section Description');}
    }

	public String LeftPickLinkOrButton {
        get{return getPropertyWithDefault('LeftPickLinkOrButton', '[]');}
    }

	public String LeftLinkOrButtonURLText{
        get{return getPropertyWithDefault('LeftLinkOrButtonURLText', '');}
    }

	public cms.Link LeftLinkOrButtonURLLinkObj{
        get{return getPropertyLink('LeftLinkOrButtonURLLinkObj');}
    }

	public String LeftLinkColorBlueOrWhite{
        get{return getPropertyWithDefault('LeftLinkColorBlueOrWhite', '[]');}
    }

	public String LeftFontColorBlackOrWhite{
        get{return getPropertyWithDefault('LeftFontColorBlackOrWhite', '[]');}
    }

	public String LeftCSSClassSelector{
        get{return getPropertyWithDefault('LeftCSSClassSelector', '[]');}
    }

	public Boolean LeftisLinkOrButtonForFoodPage {
		get {return getAttribute('LeftisLinkOrButtonForFoodPage') == 'true';}
	}

	public String LeftSectionGTMID{
        get{return getPropertyWithDefault('LeftSectionGTMID', '');}
    }

	/** Right Section */
	public String RightImageURL{
        get{return getPropertyWithDefault('RightImageURL', 'URL of right image');}
    }

	public String RightTitle{
        get{return getPropertyWithDefault('RightTitle', 'Right Section Title');}
    }

	public String RightDescription{
        get{return getPropertyWithDefault('RightDescription', 'This is the Right Section Description');}
    }

	public String RightPickLinkOrButton {
        get{return getPropertyWithDefault('RightPickLinkOrButton', '[]');}
    }

	public String RightLinkOrButtonURLText{
        get{return getPropertyWithDefault('RightLinkOrButtonURLText', '');}
    }

	public cms.Link RightLinkOrButtonURLLinkObj{
        get{return getPropertyLink('RightLinkOrButtonURLLinkObj');}
    }

	public String RightLinkColorBlueOrWhite{
        get{return getPropertyWithDefault('RightLinkColorBlueOrWhite', '[]');}
    }

	public String RightFontColorBlackOrWhite{
        get{return getPropertyWithDefault('RightFontColorBlackOrWhite', '[]');}
    }

	public String RightCSSClassSelector{
        get{return getPropertyWithDefault('RightCSSClassSelector', '[]');}
    }

	public Boolean RightisLinkOrButtonForFoodPage {
		get {return getAttribute('RightisLinkOrButtonForFoodPage') == 'true';}
	}

	public String RightSectionGTMID{
        get{return getPropertyWithDefault('RightSectionGTMID', '');}
    }
	global virtual override String getHTML(){
		String html = '';
		String LeftlinkOrButtonURL = (this.LeftLinkOrButtonURLLinkObj != null) ? this.LeftLinkOrButtonURLLinkObj.targetPage : '#';
		String RightlinkOrButtonURL = (this.RightLinkOrButtonURLLinkObj != null) ? this.RightLinkOrButtonURLLinkObj.targetPage : '#';
		String LeftlinkOrButtonHTML = '';String LeftsectionGTMIDAttr = ' ';
		String RightlinkOrButtonHTML = '';String RightsectionGTMIDAttr = ' ';

		if(LeftSectionGTMID != null && LeftSectionGTMID != '') {
			LeftsectionGTMIDAttr = ' data-gtm-id="' + LeftSectionGTMID + '" ';
		}
		if(LeftisLinkOrButtonForFoodPage) {
			Id hmrProductsCategoryId = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Name = 'HMR Products'].Id;
			LeftlinkOrButtonURL = 'about-hmr-foods-nutrition-ingredients?categoryId=' + String.valueOf(hmrProductsCategoryId);
		}
		if(LeftPickLinkOrButton == 'Link') {
			LeftlinkOrButtonHTML += '<div class="link-container">' +
									'<div class="hmr-allcaps-container">' +
										'<a' + LeftsectionGTMIDAttr + 'href="' + LeftlinkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-' + LeftLinkColorBlueOrWhite + '">' + LeftLinkOrButtonURLText + '</a>' +
										'<a' + LeftsectionGTMIDAttr + 'href="' + LeftlinkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-' + LeftLinkColorBlueOrWhite + '">></a>' +
									'</div>' +
								'</div>';
		}
		if(LeftPickLinkOrButton == 'Button'){
			LeftlinkOrButtonHTML += '<div class="btn-container">' +
		                            '<a' + LeftsectionGTMIDAttr + 'href="' + LeftlinkOrButtonURL + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">' + LeftLinkOrButtonURLText + '</a>' +
		                        '</div>';
		}
		if(LeftPickLinkOrButton == 'None') {
			LeftlinkOrButtonHTML = '';
		}
		if(RightSectionGTMID != null && RightSectionGTMID != '') {
			RightsectionGTMIDAttr = ' data-gtm-id="' + RightSectionGTMID + '" ';
		}
		if(RightisLinkOrButtonForFoodPage) {
			Id hmrProductsCategoryId = [SELECT Id, Name FROM ccrz__E_Category__c WHERE Name = 'HMR Products'].Id;
			RightlinkOrButtonURL = 'about-hmr-foods-nutrition-ingredients?categoryId=' + String.valueOf(hmrProductsCategoryId);
			// new link to HMR Foods page /about-hmr-foods-nutrition-ingredients
		}
		if(RightPickLinkOrButton == 'Link') {
			RightlinkOrButtonHTML += '<div class="link-container">' +
									'<div class="hmr-allcaps-container">' +
										'<a' + RightsectionGTMIDAttr + 'href="' + RightlinkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-' + RightLinkColorBlueOrWhite + '">' + RightLinkOrButtonURLText + '</a>' +
										'<a' + RightsectionGTMIDAttr + 'href="' + RightlinkOrButtonURL + '" class="hmr-allcaps hmr-allcaps-carat hmr-allcaps-' + RightLinkColorBlueOrWhite + '">></a>' +
									'</div>' +
								'</div>';
		}
		if(RightPickLinkOrButton == 'Button'){
			RightlinkOrButtonHTML += '<div class="btn-container">' +
		                            '<a' + RightsectionGTMIDAttr + 'href="' + RightlinkOrButtonURL + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">' + RightLinkOrButtonURLText + '</a>' +
		                        '</div>';
		}
		if(RightPickLinkOrButton == 'None') {
			RightlinkOrButtonHTML = '';
		}
		html += '<section data-c="hmr-2col-section" class="hmr-page-section bg-typecover hmr-2col-section">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<div class="col-sm-6 section-right">' +
			                    '<div class="section-right-' + LeftFontColorBlackOrWhite + '">' +
			                    	'<img class="icon" src="' + LeftImageURL +'" />' +
			                        '<h2 class="title">' + LeftTitle + '</h2>' +
			                        '<div class="blurb text-center">' +
			                            '<p>' + LeftDescription + '</p>' +
			                        '</div>' +
			                        LeftlinkOrButtonHTML +
			                    '</div>' +
			                '</div>' +
							'<div class="col-sm-6 section-right">' +
			                    '<div class="section-right-' + RightFontColorBlackOrWhite + '">' +
			                    	'<img class="icon" src="' + RightImageURL +'" />' +
			                        '<h2 class="title">' + RightTitle + '</h2>' +
			                        '<div class="blurb text-center">' +
			                            '<p>' + RightDescription + '</p>' +
			                        '</div>' +
			                        RightlinkOrButtonHTML +
			                    '</div>' +
			                '</div>' +
			            '</div>' +
			        '</div>' +
			    '</section>';
		return html;
	}
}