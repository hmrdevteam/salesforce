/**
* Apex Content Template Controller for Subscribe form
*
* @Date: 2017-03-30
* @Author Utkarsh Goswami (Mindtree)
* @Modified: Mustafa Ahmed (HMR)
* @Date: 2017-04-12
* @JIRA: 
*/
global virtual class HMR_CMS_Subscribe_ContentTemplate extends cms.ContentTemplateController{
  //need two constructors, 1 to initialize CreateContentController a
  global HMR_CMS_Subscribe_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_Subscribe_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String backgroundImage{
        get{
            return getProperty('backgroundImage');
        }
    }

    

    // This method generates the markup for the shopping cart in the header
    global virtual override String getHTML() {    
        
        String sitePrefix = Site.getBaseUrl();
        String cPageUrl =  Apexpages.currentPage().getUrl();
        String html = '';
        
        html += '<div id="SubscriptionModal" class="hmr-modal modal fade" role="dialog" data-url="' + sitePrefix + '">';
            html += '<div class="modal-dialog">';
                html += '<div class="modal-content">';
                    html += '<div class="row">';
                        html += '<div class="hidden-xs col-sm-6">';
                             html += '<div class="sub-bg" style="background-image : url(\'' + sitePrefix + backgroundImage + '\');"></div>';                           
                        html += '</div>';
                        html += '<div class="col-xs-12 col-sm-6">';
                            html += '<div class="modal-header">';
                                html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                    html += '<span aria-hidden="true">&times;</span>';
                                html += '</button>';
                                html += '<h4 class="modal-title text-center">Subscribe</h4>';
                            html += '</div>';
                            html += '<form class="subscribeform">';
                                html += '<div class="modal-body">';
                                    html += '<div class="row">';
                                        html += '<div class="col-xs-12">';                                            
                                            html += '<div class="form-group">';
                                                html += '<label for="firstName">First Name</label>';
                                                html += '<input type="text" class="form-control" id="firstName" name="firstName" aria-describedby="First Name" placeholder="Enter First Name"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="lastName">Last Name</label>';
                                                html += '<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="email">Email address</label>';
                                                html += '<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="telInput">Zipcode</label>';
                                                html += '<input class="form-control" type="text" id="telInput" name="telInput" placeholder="Zipcode"/>';
                                            html += '</div>';                                            
                                        html += '</div>';
                                    html += '</div>';
                                    html += '<div id="subscribeFormError" class="hidden"><p style="color: red; overflow: auto;"></p></div>';
                                html += '</div>';                           
                                html += '<div class="modal-footer">';
                                    html += '<button type="submit" class="btn btn-primary submit-btn">Subscribe</button>';
                                html += '</div>';                                
                                html += '<div>';
                                html += '<input data-id="ca:input" type="hidden" name="ca" value="7e444613-7d32-470c-be42-03c92c963375">';
                                if(cPageUrl.contains('Knowledge')){
                                    html += '<input data-id="list:input" type="hidden" name="list" value="1128393743">';                                    
                                }
                                else if(cPageUrl.contains('Recipe')){
                                    html += '<input data-id="list:input" type="hidden" name="list" value="1903706083">';
                                }
                                else if(cPageUrl.contains('Home')){
                                    html += '<input data-id="list:input" type="hidden" name="list" value="1986788089">';
                                }
                                else{
                                    html += '<input data-id="list:input" type="hidden" name="list" value="1986788089">';
                                }
                                html += '</div>';
                            html += '</form>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';  
                     
        return html;            
    } 
}