/**
* Test Class Coverage of the HMR_CC_Order_Handler Class
*
* @Date: 06/19/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/

@isTest
private class HMR_CC_Order_Handler_Test{
  
  @isTest
  private static void testFirstOrderInsertWithAddressRecords(){
        Test.startTest();
      	
      	List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
      	
      	Test.stopTest();
      
      	//Verify the address book records were created
		List<ccrz__E_AccountAddressBook__c>  addressBookList = [SELECT Id FROM ccrz__E_AccountAddressBook__c];
      	System.assert(addressBookList.size() == 2);    	
  }
    
  @isTest
  private static void testAnotherOrderInsertWithSameAddressRecords(){
        List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
      
        Test.startTest();
      	
      	//List<ccrz__E_Order__c> newOrderList = cc_dataFactory.createOrders(1);
      	
      	Test.stopTest();
      
      	//Verify no additional address book records were created, there will be the one extra from the data factory
		List<ccrz__E_AccountAddressBook__c>  addressBookList = [SELECT Id FROM ccrz__E_AccountAddressBook__c];
      	System.assertEquals(2, addressBookList.size());    	
  }
    
  @isTest
  private static void testFirstOrderInsertWithAddressRecordsNoSecondAddressLine(){
      	ccrz__E_ContactAddr__c shipToRecord = cc_DataFactory.shipToAddress;
        ccrz__E_ContactAddr__c billToRecord = cc_DataFactory.billToAddress;
      	shipToRecord.ccrz__AddressSecondline__c = null;
      	billToRecord.ccrz__AddressSecondline__c = null;
      	cc_dataFactory.shipToAddress = shipToRecord;
      	cc_dataFactory.billToAddress = billToRecord;
      	update new List<ccrz__E_ContactAddr__c>{shipToRecord, billToRecord};
      
        Test.startTest();
     
      	List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
      	
      	Test.stopTest();
      
      	//Verify the address book records were created
		List<ccrz__E_AccountAddressBook__c>  addressBookList = [SELECT Id FROM ccrz__E_AccountAddressBook__c];
      	System.assert(addressBookList.size() == 2);    	
  }
    
  @isTest
  private static void testOrderInsertWithExistingAddressRecordOfOtherType(){
      	ccrz__E_ContactAddr__c shipToRecord = cc_DataFactory.shipToAddress;
        ccrz__E_ContactAddr__c billToRecord = cc_DataFactory.billToAddress;
      	shipToRecord.ccrz__AddressFirstline__c = '1111 Main Street';
      	shipToRecord.ccrz__AddressSecondline__c = null;
        billToRecord.ccrz__AddressFirstline__c = '2222 Main Street';
      	billToRecord.ccrz__AddressSecondline__c = null;
      	cc_dataFactory.shipToAddress = shipToRecord;
      	cc_dataFactory.billToAddress = billToRecord;
      	update new List<ccrz__E_ContactAddr__c>{shipToRecord, billToRecord};
            
      	List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
      	
        //Reset the ship to address so that it gets set in the data factory as the original address (matching the original bill to)
      	cc_dataFactory.shipToAddress = null;
        //List<ccrz__E_Order__c> secondOrderList = cc_dataFactory.createOrders(1);
      	
      	
        Test.startTest();

      	//List<ccrz__E_Order__c> finalOrderList = cc_dataFactory.createOrders(1);
      	
      	Test.stopTest();
      
      	//Verify the address book records were created
		List<ccrz__E_AccountAddressBook__c>  addressBookList = [SELECT Id FROM ccrz__E_AccountAddressBook__c];
      	System.assertEquals(2, addressBookList.size()); 
  }
}