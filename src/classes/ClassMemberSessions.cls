/**
* Controller for Scheduling 1 Time Make Up Classes
* Objects referenced --> Class__c, Session_Attendee__c, Class_Member__c, Coaching_Session__c
*
* @Date: 2016-12-21
* @Author Mustafa Ahmed (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-750
*/

Public class ClassMemberSessions {
    Public Id parentClassId;

    Public Id sessionAttId;
    Public Session_Attendee__c sessionAtt {get; set;}
    Public Class_Member__c parentClassMembership {get; set;}
    Public Class__c parentClass {get; set;}
    Public Coaching_Session__c coachingsessionList {get; set;}
    Public Date sessionDate;
    List<Coaching_Session__c> SessionList;
    Public String selectedSession{get;set;}
    Public Session_Attendee__c MakeUp;
    Public Boolean success {get;set;}
    Public Boolean error {get;set;}
    Public Boolean moreRecordLink {get;set;}
    Public Boolean noRecords{get;set;}
    Public String noRecordMessage{get;set;}
    Public Integer recordsToShow = 12;
    Public Integer numberOfRecords = recordsToShow ;
    Public List<Coaching_Session__c> searchedSessionList{get;set;}
    Public String attendanceStatus = 'Make-up Scheduled';
    Public Session_Attendee__c[] makeUpSession = new List<Session_Attendee__c>();
    List<Session_Attendee__c> newSessionAttendeeList;
    Public Set<Id> existingCoachingSessionSet;
    public Contact contactRecord {get;set;}
    public Integer timeOffset {get;set;}
    private static final Map<String, Integer> getMilitaryHour = new Map<String, Integer>{
        '12 AM'=> 0,
        '1 AM'=> 1,
        '2 AM'=> 2,
        '3 AM'=> 3,
        '4 AM'=> 4,
        '5 AM'=> 5,
        '6 AM'=> 6,
        '7 AM'=> 7,
        '8 AM'=> 8,
        '9 AM'=> 9,
        '10 AM'=> 10,
        '11 AM'=> 11,
        '12 PM'=> 12,
        '1 PM'=> 13,
        '2 PM'=> 14,
        '3 PM'=> 15,
        '4 PM'=> 16,
        '5 PM'=> 17,
        '6 PM'=> 18,
        '7 PM'=> 19,
        '8 PM'=> 20,
        '9 PM'=> 21,
        '10 PM'=> 22,
        '11 PM'=> 23
    };
    private static final Map<Integer, String> getStandardHour = new Map<Integer, String>{
         0=> '12 AM',
         1=> '1 AM',
         2=> '2 AM',
         3=> '3 AM',
         4=> '4 AM',
         5=> '5 AM',
         6=> '6 AM',
         7=> '7 AM',
         8=> '8 AM',
         9=> '9 AM',
         10=> '10 AM',
         11=> '11 AM',
         12=> '12 PM',
         13=> '1 PM',
         14=> '2 PM',
         15=> '3 PM',
         16=> '4 PM',
         17=> '5 PM',
         18=> '6 PM',
         19=> '7 PM',
         20=> '8 PM',
         21=> '9 PM',
         22=> '10 PM',
         23=> '11 PM'
    };



    //Class constructor
    Public ClassMemberSessions(ApexPages.StandardController ctrl) {
        try{
            //Grabs the id from the page query
            sessionAttId = apexpages.currentpage().getparameters().get('id');
            if(sessionAttId != null){
                //The SA record based on the id from the query
                sessionAtt = [SELECT Id, Name, hmr_Class_Member__c, hmr_Class_Date__c, hmr_Coaching_Session__c, hmr_Class_Member__r.hmr_Class__c
                                FROM Session_Attendee__c WHERE Id = :sessionAttId LIMIT 1];
                                System.debug('Session Att -> ' + sessionAtt);
                //The class date of the SA record
                sessionDate = sessionAtt.hmr_Class_Date__c;
                //The Class Member record based on the SA record
                parentClassMembership = [SELECT Id, Name, hmr_Class__c, hmr_Contact_Name__c, hmr_Contact_Name__r.Time_Zone_Offset__c, hmr_Class_Type__c, hmr_Class__r.Name, hmr_Class__r.hmr_Class_Name__c
                                            FROM Class_Member__c WHERE Id = :sessionAtt.hmr_Class_Member__c LIMIT 1];
                //The Class record based on the Class Member record
                parentClass = [SELECT Id, Name, hmr_Class_Type__c
                                    FROM Class__c WHERE Id = :parentClassMembership.hmr_Class__c LIMIT 1];
                //Current Coaching Session
                coachingsessionList = [SELECT Id, Name, hmr_Class__c, isEasternTime__c
                                    FROM Coaching_Session__c WHERE Id = :sessionAtt.hmr_Coaching_Session__c  LIMIT 1];
                //shows the initial Available Coaching Sessions
                    searchedSessionList = getSelectedSessionList(sessionDate);
                    System.Debug('@@@@@@@@@@@@@@@ searchedSessionList' + searchedSessionList);

            }
            else {
                //Error message if there is no id being passed
                success = false;
                error = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Session Attendee Not Found'));
            }

        }

        catch(Exception ex){
            //Error message for invalid id
            success = false;
            error = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Session Attendee Id'));
        }

    }

    //Method to get all the coaching session based on the SessionDate Parameter
    public List<Coaching_Session__c> getSelectedSessionList(Date sessionDateSent){

        Date sessionDate = sessionDateSent;
        //SessionList = Database.Query('Select Id, Name, hmr_Class__c, hmr_Coach__c, hmr_Coach_Name__c, hmr_Class_Date__c, hmr_Start_Time__c from Coaching_Session__c' +
        //                                                        ' where hmr_Class__c <> :parentClass.Id AND hmr_Class__r.hmr_Active__c = TRUE ' +
        //                                                        ' AND (hmr_Class_Date__c >= :Date.today() AND hmr_Class_Date__c <= :sessionDate.addDays(6) AND hmr_Class_Date__c >= :sessionDate.addDays(-6))' +
        //                                                        ' ORDER BY hmr_Class_Date__c Limit :recordsToShow');
        //System.debug(SessionList.size());

        //Grabs all the available Coaching Sessions 6 days prior and 6 days after from the passed session date

        newSessionAttendeeList = [SELECT Id, Name, hmr_Make_up_Client__c, hmr_Coaching_Session__c FROM Session_Attendee__c
                                    WHERE hmr_Class_Member__c = :sessionAtt.hmr_Class_Member__c AND hmr_Make_up_Client__c =TRUE];

            existingCoachingSessionSet = new Set<Id>();
               for(Session_Attendee__c SessAtt : newSessionAttendeeList){
                        existingCoachingSessionSet.add(SessAtt.hmr_Coaching_Session__c);
               }

        SessionList = ([SELECT Id, Name, isEasternTime__c, hmr_Class__c, hmr_Coach__c, hmr_Coach_Name__c, hmr_Class_Date__c, hmr_Start_Time__c, hmr_Class__r.hmr_Class_Strength__c, hmr_Class__r.hmr_Client_Enrolled__c
                        FROM Coaching_Session__c WHERE Do_Not_Offer__c = false AND hmr_Class__c <> :parentClass.Id AND hmr_Class__c <> :coachingsessionList.hmr_Class__c
                        AND hmr_Class__r.hmr_Class_Type__c = :parentClass.hmr_Class_Type__c AND hmr_Class__r.hmr_Active__c = TRUE
                        AND (hmr_Class_Date__c >= :Date.today() AND hmr_Class_Date__c <= :sessionDate.addDays(6) AND hmr_Class_Date__c >= :sessionDate.addDays(-6))
                        AND Id NOT IN :existingCoachingSessionSet
                        ORDER BY hmr_Class_Date__c Limit :recordsToShow]);

        if(parentClassMembership.hmr_Contact_Name__c != null){
                    contactRecord = [SELECT Id, FirstName, LastName, Time_Zone_Offset__c FROM Contact WHERE Id = :parentClassMembership.hmr_Contact_Name__c];

                    if(contactRecord.Time_Zone_Offset__c != null){
                        timeOffset = Integer.valueOf(contactRecord.Time_Zone_Offset__c) - 5;
                    }
                }

                if(!SessionList.isEmpty() && timeOffset != null && timeOffset != 0){
                    for(Coaching_Session__c clsTime : SessionList){
                        try {
                            String hour = clsTime.hmr_Start_Time__c.substringBefore(':');
                            String min = clsTime.hmr_Start_Time__c.substringBetween(':', ' ');

                            if(clsTime.hmr_Start_Time__c.contains('AM')){
                                hour = hour + ' AM';
                            }
                            else{
                                hour = hour + ' PM';
                            }

                            Integer adjustHour = getMilitaryHour.get(hour);
                            //Loop counter
                            Integer i = 0;

                            if(timeOffset > 0){
                                while (timeOffset > i) {
                                    if(adjustHour == 0){
                                        //if time is 0(Midnight), reset hour to 23(11 PM)
                                        adjustHour = 23;
                                    }
                                    else{
                                        adjustHour -= 1;
                                    }
                                    i++;
                                }
                            }
                            else if(timeOffset < 0){
                                timeOffset *= -1;
                                while (timeOffset > i) {
                                    if(adjustHour == 23){
                                        //if time is 23(11 PM), reset hour to 0(Midnight)
                                        adjustHour = 0;
                                    }
                                    else{
                                        adjustHour += 1;
                                    }                            
                                    i++;
                                }
                            }

                            String newTime = getStandardHour.get(adjustHour);

                            clsTime.hmr_Start_Time__c = newTime.contains('AM') ? newTime.substringBefore(' ') + ':' + min + ' AM' : newTime.substringBefore(' ') + ':' + min + ' PM';
                        }
                        Catch (exception e) {
                              clsTime.isEasternTime__c = true;
                        }
                    }
                }
                else if(!SessionList.isEmpty() && timeOffset == null){
                    for(Coaching_Session__c clsTime : SessionList){
                        clsTime.isEasternTime__c = true;
                    }
                }


        //Counts the number of available Coaching Sessions, for displaying no record found message and the "More" link
            Integer totalRecords = [Select count() FROM Coaching_Session__c WHERE hmr_Class__c <> :parentClass.Id AND hmr_Class__r.hmr_Active__c = TRUE
                             AND (hmr_Class_Date__c >= :Date.today() AND hmr_Class_Date__c <= :sessionDate.addDays(6) AND hmr_Class_Date__c >= :sessionDate.addDays(-6))];

                             if(totalRecords == 0){
                                         noRecords = TRUE;
                                   noRecordMessage = 'No Sessions To Display';
                                                  }
                           else{
                                 noRecords = FALSE;
                               }

                     if(totalRecords <= recordsToShow){
                            moreRecordLink = TRUE;
                                                      }
                   else{
                            moreRecordLink = FALSE;
                       }
        return SessionList;
    }

   // Method to go to the previous page (pagination)
   public void previous(){
        recordsToShow  = numberOfRecords;
        sessionDate = sessionDate.addDays(-13);
        searchedSessionList = getSelectedSessionList(sessionDate);
    }

    // Method to go to the next page (pagination)
    public void next(){
        recordsToShow  = numberOfRecords;
        sessionDate = sessionDate.addDays(13);
        searchedSessionList = getSelectedSessionList(sessionDate);
    }

    //Method to enable or disable previous week button.
    public boolean getprev(){
        if(sessionDate == Date.today() || ((searchedSessionList == null || searchedSessionList.size() == 0) && sessionDate < Date.today()))
        return true;
        else
        return false;
    }

    //Method to enable or disable next week button
    public boolean getnxt(){
          if((searchedSessionList == null || searchedSessionList.size() == 0) && sessionDate > Date.today())
            return true;
        else
            return false;
    }

    //Method assign selected Session Id
    public void selectSessionId(){
        selectedSession = System.currentPagereference().getParameters().get('coachSessionID');
        System.debug('Session Id -> ' + selectedSession);
    }

    //Method to assign the client to the selected Session
    public PageReference assignSession(){
        //Mark the current SA record as having scheduled a makeup
        MakeUp = [SELECT Id, hmr_Coaching_Session__c, hmr_Attendance__c, hmr_Class_Member__c, hmr_Email__c, hmr_Client_Name__c FROM Session_Attendee__c WHERE id = :sessionAttId LIMIT 1];
        MakeUp.hmr_Attendance__c = attendanceStatus;
        //Create a new SA record that is marked as a make up
        Session_Attendee__c newSARecord = new Session_Attendee__c(hmr_Client_Name__c = MakeUp.hmr_Client_Name__c, hmr_Class_Member__c = MakeUp.hmr_Class_Member__c,
                                                                  hmr_Coaching_Session__c = selectedSession, hmr_Make_up_Client__c = true);
        makeUpSession.add(newSARecord);

        try{
            if(!String.isBlank(selectedSession)){
            Update MakeUp;
            Insert makeUpSession;

            //Redirects the page to the new SA record that was created
            PageReference cmPage = new PageReference('/'+newSARecord.Id);
            return cmPage;
           }else{
            success = false;
            error = true;
            //If the user tries to assign a session without selecting it, display error message
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Session to assign'));
            return null;
           }

        }catch(Exception e){
                return null;
            }
    }

    //Method to load more results when "More" link is clicked
    public void loadMoreResults(){
        recordsToShow +=12;
        searchedSessionList = getSelectedSessionList(sessionDate);
    }
}