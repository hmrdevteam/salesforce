/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/15/2018
 * Created By: 
 * Last Modified Date: 01/15/2018
 * Last Modified By: Zach Engman
 * Description: Test Coverage for the HMRMobWeightActivity REST Service
 * ****************************************************/
@isTest
private class HMRMobWeightActivityTest {

    @testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
      insert programRecord;

      Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                hmr_Active__c = true,
                                                                                hmr_Status__c = 'Active',
                                                                                hmr_Contact__c = contactRecord.Id,
                                                                                hmr_Diet_Start_Date__c = Date.today());
      insert programMembershipRecord;
    }

    @isTest
    private static void testGetWeightActivityForNewRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobWeightActivity.doGet();

      Test.stopTest();

      //Verify the weight activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }

    @isTest
    private static void testGetWeightActivityForExistingRecord(){
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      
      insert new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                               ,Program_Membership__c = programMembershipRecord.Id
                               ,DateDataEnteredFor__c = Date.today()
                               ,CurrentWeight__c = 170);
      
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.params.put('contactId', programMembershipRecord.hmr_Contact__c);
      request.params.put('programMembershipId', programMembershipRecord.Id);
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobWeightActivity.doGet();

      Test.stopTest();

      //Verify the weight activity was successfully returned
      System.assertEquals(200, response.statuscode);
    }
    
    @isTest
    private static void testGetWeightActivityWithInvalidData(){
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.params.put('contactId', '');
      request.params.put('activityDate', String.valueOf(Date.today()));
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      HMRMobWeightActivity.doGet();

      Test.stopTest();

      //Verify invalid status code was returned
      System.assertEquals(404, response.statuscode);
    }

    @isTest
    private static void testPostWeightActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);

      HMRMobWeightActivity.MobWeightActivityDTO weightActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        weightActivityDTO = HMRMobWeightActivity.getWeightActivity(activityLogRecord);
        HMRMobWeightActivity.doPost(weightActivityDTO);
      }

      Test.stopTest();

      //Verify the activity log record was created
      ActivityLog__c activityLogResultRecord = [SELECT Contact__c FROM ActivityLog__c];
      System.assertEquals(userRecord.ContactId, activityLogResultRecord.Contact__c);
    }

    @isTest
    private static void testPostWeightActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.httpMethod = 'POST';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);

      HMRMobWeightActivity.MobWeightActivityDTO weightActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        weightActivityDTO = HMRMobWeightActivity.getWeightActivity(activityLogRecord);
        HMRMobWeightActivity.doPost(weightActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(500, response.statusCode);
    }

    @isTest
    private static void testPatchWeightActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      HMRMobWeightActivity.MobWeightActivityDTO weightActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        weightActivityDTO = HMRMobWeightActivity.getWeightActivity(activityLogRecord);
        weightActivityDTO.currentWeight = 160;
        HMRMobWeightActivity.updateWeightActivity(weightActivityDTO);
      }

      Test.stopTest();

      //Verify the activity log record was updated
      ActivityLog__c activityLogResultRecord = [SELECT CurrentWeight__c FROM ActivityLog__c];
      System.assertEquals(160, activityLogResultRecord.CurrentWeight__c);
    }

    @isTest
    private static void testPatchWeightActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.httpMethod = 'PATCH';

      RestContext.request = request;
      RestContext.response = response;

      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      HMRMobWeightActivity.MobWeightActivityDTO weightActivityDTO;

      Test.startTest();

      System.runAs(userRecord){
        weightActivityDTO = HMRMobWeightActivity.getWeightActivity(activityLogRecord);
        weightActivityDTO.recordId = null;
        HMRMobWeightActivity.updateWeightActivity(weightActivityDTO);
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assertEquals(405, response.statusCode);
    }

    @isTest
    private static void testDeleteWeightActivityLog(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
      Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c FROM Program_Membership__c LIMIT 1];
      ActivityLog__c activityLogRecord = new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c
                                                           ,Program_Membership__c = programMembershipRecord.Id
                                                           ,DateDataEnteredFor__c = Date.today()
                                                           ,CurrentWeight__c = 170);
      insert activityLogRecord;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.params.put('id', activityLogRecord.Id);
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;


      Test.startTest();

      System.runAs(userRecord){
        HMRMobWeightActivity.deleteActivity();
      }

      Test.stopTest();

      //Verify the activity log record was deleted
      List<ActivityLog__c> activityLogResultList = [SELECT Id FROM ActivityLog__c];
      System.assertEquals(1, activityLogResultList.size());
    }

    @isTest
    private static void testDeleteWeightActivityLogWithInvalidData(){
      User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/activity/weightData/';
      request.params.put('id', '00000000000000000000');
      request.httpMethod = 'DELETE';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
        HMRMobWeightActivity.deleteActivity();
      }

      Test.stopTest();

      //Verify the error return status was set
      System.assert(!String.isBlank(String.valueOf(response.responseBody)));
    }
}