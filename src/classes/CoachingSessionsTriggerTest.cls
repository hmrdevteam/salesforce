/**
* Unit test class for CoachingSessionTrigger.trigger
* Flagging Coaching Session records for holiday based on class date
*
* @Date: 2016-12-08
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-695
*/
@isTest
private class CoachingSessionsTriggerTest {
	
	@isTest static void test_method_one() {
		// Implement test code
		Test.startTest();
	    //Create test user 
		User sampleUser = new User();
		sampleUser.Username = 'testuser1@gmail.com.hmrtest';
		sampleUser.FirstName='FirstName';
		sampleUser.LastName = 'LastTestName';
		sampleUser.Email = 'testuser1@gmail.com';
		sampleUser.alias = 'testAl';
		sampleUser.TimeZoneSidKey = 'America/New_York';
		sampleUser.LocaleSidKey = 'en_us';
		sampleUser.EmailEncodingKey = 'ISO-8859-1';
		sampleUser.ProfileId = [select id from Profile where Name='System Administrator'].Id;
		sampleUser.LanguageLocaleKey = 'en_us';
		sampleUser.IsActive = true;
		sampleUser.hmr_IsCoach__c = true;
		insert sampleUser;

		//Create Test Data for 'Coach__C' object 
		Coach__c coachRecord1 = new Coach__c (
		                                hmr_Active__c=True,
		                                hmr_Coach__c= sampleUser.Id,
		                                hmr_Email__c='sampleemail1@gmail.com',
		                                hmr_Class_Strength__c=20
		);
		insert coachRecord1;    

		//Create Test Data for 'Class' object
		Class__c classRecord1 = New Class__c (
		                              hmr_Active__c=true,
		                              hmr_Class_Name__c='Session on eCommerce',
		                              hmr_Coach__c=coachRecord1.id,
		                              hmr_Start_Time__c = '11:00 AM',
		                              hmr_End_Time__c ='12:00 PM',
		                              hmr_First_Class_Date__c=date.newInstance(2016, 11, 11),    	                                  
		                              hmr_Active_Date__c=date.newInstance(2016, 10, 11),
		                              hmr_Class_Type__c='P1 PP',
		                              hmr_Coference_Call_Number__c='1234',
		                              hmr_Time_of_Day__c='Noon',
		                              hmr_Participant_Code__c='Placeholder',
		                              hmr_Host_Code__c='Placeholder'
		                              
		);
		insert classRecord1; 

		//insert coaching session record
		Coaching_Session__c coachingSession = new Coaching_Session__c(
                                hmr_Class__c = classRecord1.Id,
                                hmr_Class_Date__c = Date.newInstance(2017, 7, 4),
                                hmr_Coach__c = coachRecord1.Id,
                                hmr_Start_Time__c = classRecord1.hmr_Start_Time__c,
                                hmr_End_Time__c =  classRecord1.hmr_End_Time__c       
                            );		
		try {
			insert coachingSession;
			//get coaching session record
			Coaching_Session__c cInsertTest = [SELECT Id, hmr_Class_Date__c, hmr_Holiday_Name__c, hmr_Holiday_Conflict__c FROM Coaching_Session__c WHERE Id = :coachingSession.Id];
			//check if holiday conflict is set to true
			System.assertEquals(cInsertTest.hmr_Holiday_Conflict__c, true,'Coaching Session Flagged as Holiday');		
			//update the coaching session record
			coachingSession.hmr_Class_Date__c = Date.newInstance(2017, 7, 15);
			update coachingSession;
			Coaching_Session__c cUpdateTest = [SELECT Id, hmr_Class_Date__c, hmr_Holiday_Name__c, hmr_Holiday_Conflict__c FROM Coaching_Session__c WHERE Id = :coachingSession.Id];
			//check if holiday conflict is set to false
			System.assertEquals(cUpdateTest.hmr_Holiday_Conflict__c, false,'Coaching Session is not Flagged as Holiday');			
		}
		catch(Exception ex){
			System.debug(ex.getMessage());
		}
		Test.stopTest();
	}
}