/**
* Apex Content Template Controller for the first Sections immdiately after Hero Section on Plans Page
*
* @Date: 05/24/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_PlansDetails_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_PlansDetails_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_PlansDetails_ContentTemplate() {}

	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
	 */
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
	 */
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	public String PlansDetailsTitle{
        get{
            return getPropertyWithDefault('PlansDetailsTitle', 'Plans Details Title');
        }
    }

	public String PlansDetailsDescription{
        get{
            return getPropertyWithDefault('PlansDetailsDescription', 'Plans Details Description');
        }
    }

	public String ProgramType{
        get{
            return getPropertyWithDefault('ProgramType', '[]');
        }
    }

	public String returnPlansDetailsHTML(Program__c program, String backgroundCSSClass) {
		String html = '';
		String targetProductDetailsPageUrl = '';
		if(program.Name.contains('Solutions')) {
			targetProductDetailsPageUrl = 'healthy-solutions-at-home-diet-plan-build-your-kit';
		}
		if(program.Name.contains('Shakes')) {
			targetProductDetailsPageUrl = 'healthy-shakes-diet-plan-build-your-kit';
		}
		html += '<section class="hmr-page-section-plans">' +
			        '<div class="container hmr-plans-container">' +
			            '<div class="row">' +
			                '<h2>' + PlansDetailsTitle + '</h2>' +
			            '</div>' +
			            '<div class="row">' +
			                '<p>' + PlansDetailsDescription + '</p>' +
			            '</div>' +
			            '<div class="row">' +
			                '<div class="btn-container plans">' +
			                    '<a href="' + targetProductDetailsPageUrl + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">Join Today</a>' +
			                '</div>' +
			            '</div>' +
			        '</div>' +
			        '<div class="bg-plans ' + backgroundCSSClass + ' bg-typecover"></div>' +
			    '</section>';
		return html;
	}


	global virtual override String getHTML(){
		String html = '';
		try {
			List<Program__c> programList = [SELECT Id, Name, Standard_Kit__r.ccrz__SKU__c FROM Program__c];
			Program__c healthySolutionsProgram;
			Program__c healthyShakesProgram;
			for(Program__c program: programList) {
				if(program.Name.contains('Solutions')) {
					healthySolutionsProgram = program;
				}
				if(program.Name.contains('Shakes')) {
					healthyShakesProgram = program;
				}
			}
			if(ProgramType == 'HSAH') {
				html = returnPlansDetailsHTML(healthySolutionsProgram, 'bg-healthysolutions');
			}
			if(ProgramType == 'HSS') {
				html = returnPlansDetailsHTML(healthyShakesProgram, 'bg-healthyshakes');
			}
		}
		catch(Exception ex) {
			System.debug('Exception in Plans Details Section Content Template');
			System.debug(ex);
			html += ex.getMessage();
		}
		return html;
	}
}