/**
* Apex Content Template Controller for daily tip
* @Date: 2018-01-10
* @Author Utkarsh Goswami (Mindtree)
* @Modified:
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_DailyTip_ContentTemplate extends cms.ContentTemplateController{  
  //Need two contructors: 1 to initialize CreateContentController
  global HMR_CMS_DailyTip_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    
    global HMR_CMS_DailyTip_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Title Property
    public String title{
        get{
            return getProperty('title');
        }
    }
    
    //Summary Property
    public String summary{
        get{
            return getProperty('summary');
        }
    }
    
     public String hasButton {
        get{
            return getProperty('hasButton');
        }
    }
    
    //Background Image Property
    public String backgroundImage{
        get{
            return getProperty('backgroundImage');
        }
    }
    
    //Button Text Property
    public String buttonCopy{
        get{
            return getProperty('buttonCopy');
        }
    }
    
    //Button Type Property
    public String destinationType{
        get{
            return getProperty('destinationType');
        }
    }

    //Destination Type Options
    public String destinationTypeOptions {
       get {
          List <HMR_CMS_PicklistOption> options = new List <HMR_CMS_PicklistOption>();
          options.add(new HMR_CMS_PicklistOption('Internal', 'Internal'));
          options.add(new HMR_CMS_PicklistOption('Mobile', 'Mobile'));
          options.add(new HMR_CMS_PicklistOption('Web', 'Web'));
          return (JSON.serialize(options));
       }
    }
    
    //Button Or Link Property
    public String googleAnalyticsTag{
        get{
            return getProperty('googleAnalyticsTag');
        }
    }
    
    //CMS Link Property for the Button Target URL
    public cms.Link linkDestination{
        get{
            return getPropertyLink('linkDestination');
        }
    }

    //global override getHTML - renders hero section image
    global virtual override String getHTML() { 
        String html = '';     
        
        return html;
    }
}