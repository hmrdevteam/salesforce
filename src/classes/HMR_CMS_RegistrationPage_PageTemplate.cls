/**
* Apex Content Template Controller for Registration Page registration section
*
* @Date: 2017-03-18
* @Author Pranay Mistry (Magnet 360)
* @Modified: 
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_RegistrationPage_PageTemplate extends cms.ContentTemplateController { 
    //need two constructors, 1 to initialize CreateContentController a
    global HMR_CMS_RegistrationPage_PageTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_RegistrationPage_PageTemplate() {
        super();
    }

    public String GeneralOrEmployeeRegistration {
        get {
            return getProperty('GeneralOrEmployeeRegistration');
        }
    }
    public String GeneralOrEmployeeRegistrationPicklistOptions {
        get {
            List <PicklistOption> options = new List <PicklistOption>();
            options.add( new PicklistOption('General', 'General'));
            options.add( new PicklistOption('Employee', 'Employee'));           
            return (JSON.serialize(options));
        }
    }
    @testVisible
    private class PicklistOption {
        String Label;
        String Value;
        public PicklistOption (String label, String value) {
            this.Label = label;
            this.Value = value;
        }
    }
    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        html += '<div class="row form-registration">' +            
                    '<form>' +
                        '<h2>Create Account</h2>' +
                        '<div class="col-xs-12 form-error">*Sorry, your email is not valid</div>' +
                        '<label for="inputEmail">Email address</label>' +
                        '<input type="email" id="inputEmail" class="form-control" required="" autofocus="">' +
                        '<label class="displayLabel" for="inputDisplayName">Display Name' + 
                            '<span class="dname"></span>' +
                        '</label>' +
                        '<input type="email" id="inputDisplayName" class="form-control" required="" autofocus="">' +
                        '<label class="displayLabel" for="inputPassword">Password' + 
                            '<span></span>' +
                        '</label>' +
                        '<div class="input-group">' +
                            '<input type="password" id="inputPassword" class="form-control cbox-shadow" required="">' +
                            '<div class="input-group-addon">' +
                                '<div class="eyeclosed"></div>' +
                            '</div>' +
                        '</div>' +
                        '<label for="inputConfirmPassword">Confirm Password</label>' +
                        '<div class="input-group">' +
                            '<input type="password" id="inputConfirmPassword" class="form-control" required="" autofocus="">' +
                            '<div class="input-group-addon">' +
                                '<div class="eyeclosed"></div>' +
                            '</div>' +
                        '</div>';
        if(GeneralOrEmployeeRegistration == 'General') {
            html += '<label for="inputZipCode">Zip Code</label>' +
                    '<input type="email" id="inputZipCode" class="form-control" required="" autofocus="">';
        }
        else {
            html += '<label for="inputEmployerCode">Employer Code</label>' +
                    '<input type="email" id="inputEmployerCode" class="form-control" required="" autofocus="">';
        }
        html += '<div class="col-xs-12 form-mailinglist">' +
                    '<div class="form-checkboxdiv">' +                   
                        '<input type="checkbox" checked="checked">' +
                    '</div>' +
                    '<div>' +
                        'Yes, I would like to join your mailing list and receive…' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-12 form-tcprivacy">' +
                    '<div>' +
                        'By creating an account I agree to HMR’s Terms and Conditions and Privacy Policy.' +
                    '</div>' +
                '</div>' +         
                '<div class="submit-wrapper">' +
                    '<button class="btn btn-lg btn-primary btn-block" type="submit">Create Account</button>' +
                '</div>' +
            '</form>' +                             
        '</div>';
        return html;
    }
}