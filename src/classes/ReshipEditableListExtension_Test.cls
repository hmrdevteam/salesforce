/*
Developer: Utkarsh Goswami (Mindtree)
Date: 5/24/2017
Target Class: ReshipEditableListExtension
*/

@isTest
public class ReshipEditableListExtension_Test{  

        
    @isTest static void test_general_method1() { 

        ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com');
        insert orderObj;
        
        ccrz__E_OrderItem__c orderItem = new ccrz__E_OrderItem__c(ccrz__Order__c = orderObj.id,ccrz__Comments__c = 'test',ccrz__Quantity__c = 10,ccrz__Price__c= 12, ccrz__SubAmount__c =123);
        insert orderItem;
        
        ApexPages.StandardController std = new  ApexPages.StandardController(orderObj);
        
        ReshipEditableListExtension reObj = new ReshipEditableListExtension(std);
        
       List<ccrz__E_OrderItem__c > orderItemResp =  reObj.getChildren();
        reObj.initChildRecord();
        reObj.getShipMethods();
        reObj.saveShipMethod();
        reObj.saveSubmit();
                
        System.assert(orderItemResp.size()==0);
    }


    @isTest static void test_general_method2() { 

        ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com');
        insert orderObj;
        
        ccrz__E_OrderItem__c orderItem = new ccrz__E_OrderItem__c(ccrz__Order__c = orderObj.id,ccrz__Comments__c = 'test',ccrz__Quantity__c = 10,ccrz__Price__c= 12, ccrz__SubAmount__c =123);
        insert orderItem;
        
        ApexPages.StandardController std = new  ApexPages.StandardController(orderObj);
        
        ReshipEditableListExtension reObj = new ReshipEditableListExtension(std);
        
       List<ccrz__E_OrderItem__c > orderItemResp =  reObj.getChildren(); 
        reObj.itemId = orderItem.id; 
        reObj.removecon();
        reObj.addToNewList();
        reObj.saveNewSubmit();
        reObj.cancelBtn();
        
        System.assert(orderItemResp.size()==0);
    }
    
}