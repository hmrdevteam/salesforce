/**
* @Date: 1/5/2017
* @Author: Ali Pierre (HMR)
* @JIRA: https://reside.jira.com/browse/HPRP-
*/
@isTest
private class ccCancelOrderTest {

    static testMethod void CreateRecords1(){

        //create an Account
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();

        Contact testContactObj1 = new Contact(FirstName='test',LastName = 'TestHSS', Account = testAccountObj);
        conList.add(testContactObj1);
        Contact testContactObj2 = new Contact(FirstName='test',LastName = 'TestHS', Account = testAccountObj);
        conList.add(testContactObj2);
        Contact testContactObj3 = new Contact(FirstName='test',LastName = 'Test2', Account = testAccountObj);
        conList.add(testContactObj3);
        Contact testContactObj4 = new Contact(FirstName='test',LastName = 'Misc', Account = testAccountObj);
        conList.add(testContactObj4);
        insert conList;

        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();

        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj1);
        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21);
        prgList.add(testProgramObj2);
        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30);
        prgList.add(testProgramObj3);
        insert prgList;

        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();

        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj1);
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'P1 HSS Kit', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
        prdList.add(testProductObj3);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj2.Id);
        prdList.add(testProductObj4);
        ccrz__E_Product__c testProductObj5 = new ccrz__E_Product__c(Name= 'P2 Kit', ccrz__SKU__c = '555555', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj3.Id);
        prdList.add(testProductObj5);
        ccrz__E_Product__c testProductObj6 = new ccrz__E_Product__c(Name= 'Chocolate Shake', ccrz__SKU__c = '777777', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj6);
        insert prdList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();

        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Completed', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj1);
        ccrz__E_Order__c testOrderObj2 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Cancelled', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj2);
        ccrz__E_Order__c testOrderObj3 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1-HSS 1st Order');
        orderList.add(testOrderObj3);
        ccrz__E_Order__c testOrderObj4 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj2.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1 1st Order');
        orderList.add(testOrderObj4);
        ccrz__E_Order__c testOrderObj5 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj3.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P2 1st Order');
        orderList.add(testOrderObj5);
        ccrz__E_Order__c testOrderObj6 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Pending', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj6);
        insert orderList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();

        ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 10.00);
        orderItemList.add(testItemObj1);
        ccrz__E_OrderItem__c testItemObj2 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj2.Id, ccrz__Price__c = 20.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 20.00);
        orderItemList.add(testItemObj2);
        ccrz__E_OrderItem__c testItemObj3 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj3.Id, ccrz__Price__c = 100.00, ccrz__Product__c = testProductObj3.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 100.00);
        orderItemList.add(testItemObj3);
        ccrz__E_OrderItem__c testItemObj4 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 150.00);
        orderItemList.add(testItemObj4);
        ccrz__E_OrderItem__c testItemObj5 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 200.00, ccrz__Product__c = testProductObj5.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 200.00);
        orderItemList.add(testItemObj5);
        ccrz__E_OrderItem__c testItemObj6 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj6);
        ccrz__E_OrderItem__c testItemObj7 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj7);
        ccrz__E_OrderItem__c testItemObj8 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj8);
        ccrz__E_OrderItem__c testItemObj9 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj9);
         ccrz__E_OrderItem__c testItemObj10 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj6.Id, ccrz__Quantity__c = 3, ccrz__SubAmount__c= 6.00);
        orderItemList.add(testItemObj10);
        ccrz__E_OrderItem__c testItemObj11 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj6.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 5, ccrz__SubAmount__c= 10.00);
        orderItemList.add(testItemObj11);
        insert orderItemList;

        //checks to see the Program Membership was created for the 3 orders
        List<Program_Membership__c> MembersList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active']);

        if(!MembersList.isEmpty()){
            System.assertEquals(MembersList.size(), 3);
        }

        System.debug('@@@@@@@@@@@@@@@@@@@@@@'+MembersList.size());

        //checks to see the Program Membership was created for the 3 orders
        List<ccrz__E_Order__c> Orders = new List<ccrz__E_Order__c>([SELECT Id, Name, hmr_Program_Membership__c, hmr_Program_Membership_Type__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c]);
        System.debug('@@@@@@@@@@@@@@@@@@@@@@'+Orders);

        //Loop through orders and confirm callouts to the ccCancel Order class return the appropriate string values
        //according to Order Status
        for(ccrz__E_Order__c Order : Orders){
        	If(Order.ccrz__OrderStatus__c == 'Completed'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order may not be cancelled due to order status: ' + Order.ccrz__OrderStatus__c);
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order has already been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Order Submitted' && Order.hmr_Program_Membership_Type__c	 != null){
        		 System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled and client has been dropped from AD');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Order Submitted'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Pending'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled');
        	}

        }

         //checks to see that 3 Program Memberships were dropped
        List<Program_Membership__c> DropList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Dropped']);

        if(!DropList.isEmpty()){
            System.assertEquals(DropList.size(), 3);
        }
    }

	static testMethod void CreateRecords2(){

        //create an Account
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();
        Contact testContactObj1 = new Contact(FirstName='test', LastName = 'TestHSS', Account = testAccountObj);
        conList.add(testContactObj1);
        insert conList;

        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj1);
        insert prgList;

		//create product categories and insert them
        List<ccrz__E_Category__c> CatList = new List<ccrz__E_Category__c>();
        ccrz__E_Category__c testCategoryObj1= new ccrz__E_Category__c(Name = 'Kits', ccrz__CategoryID__c = '600');
        CatList.add(testCategoryObj1);
        insert CatList;

        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Materials', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
        prdList.add(testProductObj1);
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'FreeEntree1', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj3);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'FreeEntree2', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj4);
        insert prdList;

        testProgramObj1.Standard_Kit_Materials__c = testProductObj2.Id;
        testProgramObj1.HSS_Free_Entree_1__c = testProductObj3.Id;
        testProgramObj1.HSS_Free_Entree_2__c = testProductObj4.Id;
        update testProgramObj1;

        //create Product Category records
        List<ccrz__E_ProductCategory__c> prdCatList = new List<ccrz__E_ProductCategory__c>();
        ccrz__E_ProductCategory__c testProductCatObj1 = new ccrz__E_ProductCategory__c( ccrz__Product__c = testProductObj1.Id, ccrz__Category__c = testCategoryObj1.Id,  ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj1);
        insert prdCatList;

        //insert CC cart records for Program orders
        List<ccrz__E_Cart__c> cartList = new List<ccrz__E_Cart__c>();
        ccrz__E_Cart__c testCartObj1 = new ccrz__E_Cart__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c = testAccountObj.Id);
        cartList.add(testCartObj1);
        insert cartList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        //P1 Kit
        ccrz__E_CartItem__c testItemcObj1 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__Category__c = testCategoryObj1.Id);
        cartItemList.add(testItemcObj1);
        insert cartItemList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();
        //P1 Kit
        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__OriginatedCart__c = testCartObj1.Id, ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1-HSS 1st Order');
        orderList.add(testOrderObj1);
        insert orderList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();
        //P1 Kit
        ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 0.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c = 0.00, ccrz__Category__c = testCategoryObj1.Id);
        orderItemList.add(testItemObj1);
        insert orderItemList;

        //checks to see the Program Membership was created for the 3 orders
        List<Program_Membership__c> MembersList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active']);

        if(!MembersList.isEmpty()){
            //System.assertEquals(MembersList.size(), 3);
        }

        System.debug('@@@@@@@@@@@@@@@@@@@@@@'+MembersList.size());

        //checks to see the Program Membership was created for the 3 orders
        List<ccrz__E_Order__c> Orders = new List<ccrz__E_Order__c>([SELECT Id, Name, hmr_Program_Membership__c, hmr_Program_Membership_Type__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c]);
        System.debug('@@@@@@@@@@@@@@@@@@@@@@'+Orders);

        //Loop through orders and confirm callouts to the ccCancel Order class return the appropriate string values
        //according to Order Status
        for(ccrz__E_Order__c Order : Orders){
        	If(Order.ccrz__OrderStatus__c == 'Completed'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order may not be cancelled due to order status: ' + Order.ccrz__OrderStatus__c);
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order has already been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Order Submitted' && Order.hmr_Program_Membership_Type__c	 != null){
        		 System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled and client has been dropped from AD');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Order Submitted'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Pending'){
        		System.assertEquals(ccCancelOrder.cancelOrder(Order.Id), 'Order number ' + Order.Name + ' has successfully been cancelled');
        	}

        }

         //checks to see that 3 Program Memberships were dropped
        List<Program_Membership__c> DropList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Dropped']);

        if(!DropList.isEmpty()){
            System.assertEquals(DropList.size(), 1);
        }

    }

    static testMethod void testStopShipment1(){

        //create an Account
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();

        Contact testContactObj1 = new Contact(FirstName='test',LastName = 'TestHSS', Account = testAccountObj);
        conList.add(testContactObj1);
        Contact testContactObj2 = new Contact(FirstName='test',LastName = 'TestHS', Account = testAccountObj);
        conList.add(testContactObj2);
        Contact testContactObj3 = new Contact(FirstName='test',LastName = 'Test2', Account = testAccountObj);
        conList.add(testContactObj3);
        Contact testContactObj4 = new Contact(FirstName='test',LastName = 'Misc', Account = testAccountObj);
        conList.add(testContactObj4);
        insert conList;

        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();

        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj1);
        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21);
        prgList.add(testProgramObj2);
        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30);
        prgList.add(testProgramObj3);
        insert prgList;

        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();

        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj1);
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'P1 HSS Kit', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
        prdList.add(testProductObj3);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj2.Id);
        prdList.add(testProductObj4);
        ccrz__E_Product__c testProductObj5 = new ccrz__E_Product__c(Name= 'P2 Kit', ccrz__SKU__c = '555555', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj3.Id);
        prdList.add(testProductObj5);
        ccrz__E_Product__c testProductObj6 = new ccrz__E_Product__c(Name= 'Chocolate Shake', ccrz__SKU__c = '777777', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj6);
        insert prdList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();

        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Completed', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj1);
        ccrz__E_Order__c testOrderObj2 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Cancelled', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj2);
        ccrz__E_Order__c testOrderObj3 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1-HSS 1st Order');
        orderList.add(testOrderObj3);
        ccrz__E_Order__c testOrderObj4 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj2.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1 1st Order');
        orderList.add(testOrderObj4);
        ccrz__E_Order__c testOrderObj5 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj3.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P2 1st Order');
        orderList.add(testOrderObj5);
        ccrz__E_Order__c testOrderObj6 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Pending', hmr_Order_Type_c__c = 'A La Carte');
        orderList.add(testOrderObj6);
        insert orderList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();

        ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 10.00);
        orderItemList.add(testItemObj1);
        ccrz__E_OrderItem__c testItemObj2 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj2.Id, ccrz__Price__c = 20.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 20.00);
        orderItemList.add(testItemObj2);
        ccrz__E_OrderItem__c testItemObj3 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj3.Id, ccrz__Price__c = 100.00, ccrz__Product__c = testProductObj3.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 100.00);
        orderItemList.add(testItemObj3);
        ccrz__E_OrderItem__c testItemObj4 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 150.00);
        orderItemList.add(testItemObj4);
        ccrz__E_OrderItem__c testItemObj5 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 200.00, ccrz__Product__c = testProductObj5.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 200.00);
        orderItemList.add(testItemObj5);
        ccrz__E_OrderItem__c testItemObj6 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj6);
        ccrz__E_OrderItem__c testItemObj7 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj7);
        ccrz__E_OrderItem__c testItemObj8 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj8);
        ccrz__E_OrderItem__c testItemObj9 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00);
        orderItemList.add(testItemObj9);
         ccrz__E_OrderItem__c testItemObj10 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj6.Id, ccrz__Quantity__c = 3, ccrz__SubAmount__c= 6.00);
        orderItemList.add(testItemObj10);
        ccrz__E_OrderItem__c testItemObj11 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj6.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 5, ccrz__SubAmount__c= 10.00);
        orderItemList.add(testItemObj11);
        insert orderItemList;

        //checks to see the Program Membership was created for the 3 orders
        List<Program_Membership__c> MembersList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active']);

        if(!MembersList.isEmpty()){
            System.assertEquals(MembersList.size(), 3);
        }

        //checks to see the Program Membership was created for the 3 orders
        List<ccrz__E_Order__c> Orders = new List<ccrz__E_Order__c>([SELECT Id, Name, hmr_Program_Membership_Type__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c]);

        //Loop through orders and confirm callouts to the ccCancel Order class return the appropriate string values
        //according to Order Status
        for(ccrz__E_Order__c Order : Orders){
        	If(Order.ccrz__OrderStatus__c == 'Completed'){
        		System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Shipment may not be stopped due to order status: ' + Order.ccrz__OrderStatus__c);
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
        		System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order has already been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Order Submitted' && Order.hmr_Program_Membership_Type__c	 != null){
        		 System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order number ' + Order.Name + ' has successfully been stopped and client has been dropped from AD');
        	}
        	// else if(Order.ccrz__OrderStatus__c == 'Order Submitted'){
        	// 	System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order number ' + Order.Name + ' has successfully been stopped');
        	// }

        }

         //checks to see that 3 Program Memberships were dropped
        List<Program_Membership__c> DropList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Dropped']);

        if(!DropList.isEmpty()){
            System.assertEquals(DropList.size(), 3);
        }
    }

    static testMethod void testStopShipment2(){

        //create an Account
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();
        Contact testContactObj1 = new Contact(FirstName='test', LastName = 'TestHSS', Account = testAccountObj);
        conList.add(testContactObj1);
        insert conList;

        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj1);
        insert prgList;

		//create product categories and insert them
        List<ccrz__E_Category__c> CatList = new List<ccrz__E_Category__c>();
        ccrz__E_Category__c testCategoryObj1= new ccrz__E_Category__c(Name = 'Kits', ccrz__CategoryID__c = '600');
        CatList.add(testCategoryObj1);
        insert CatList;

        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Materials', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
        prdList.add(testProductObj1);
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'FreeEntree1', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj3);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'FreeEntree2', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj4);
        insert prdList;

        testProgramObj1.Standard_Kit_Materials__c = testProductObj2.Id;
        testProgramObj1.HSS_Free_Entree_1__c = testProductObj3.Id;
        testProgramObj1.HSS_Free_Entree_2__c = testProductObj4.Id;
        update testProgramObj1;

        //create Product Category records
        List<ccrz__E_ProductCategory__c> prdCatList = new List<ccrz__E_ProductCategory__c>();
        ccrz__E_ProductCategory__c testProductCatObj1 = new ccrz__E_ProductCategory__c( ccrz__Product__c = testProductObj1.Id, ccrz__Category__c = testCategoryObj1.Id,  ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj1);
        insert prdCatList;

        //insert CC cart records for Program orders
        List<ccrz__E_Cart__c> cartList = new List<ccrz__E_Cart__c>();
        ccrz__E_Cart__c testCartObj1 = new ccrz__E_Cart__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c = testAccountObj.Id);
        cartList.add(testCartObj1);
        insert cartList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        //P1 Kit
        ccrz__E_CartItem__c testItemcObj1 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__Category__c = testCategoryObj1.Id);
        cartItemList.add(testItemcObj1);
        insert cartItemList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();
        //P1 Kit
        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__OriginatedCart__c = testCartObj1.Id, ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted', hmr_Order_Type_c__c = 'P1-HSS 1st Order');
        orderList.add(testOrderObj1);
        insert orderList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();
        //P1 Kit
        ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 0.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c = 0.00, ccrz__Category__c = testCategoryObj1.Id);
        orderItemList.add(testItemObj1);
        insert orderItemList;

        for(ccrz__E_Order__c complete : orderList){
            complete.ccrz__OrderStatus__c = 'Stopped Shipment';
        }
        update orderList;

        //checks to see the Program Membership was created for the 3 orders
        List<Program_Membership__c> MembersList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active']);

        if(!MembersList.isEmpty()){
            System.assertEquals(MembersList.size(), 1);
        }

        System.debug('@@@@@@@@@ZZZZZZZZ'+MembersList.size());

        //checks to see the Program Membership was created for the 3 orders
        List<ccrz__E_Order__c> Orders = new List<ccrz__E_Order__c>([SELECT Id, Name, hmr_Program_Membership_Type__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c]);

        //Loop through orders and confirm callouts to the ccCancel Order class return the appropriate string values
        //according to Order Status
        for(ccrz__E_Order__c Order : Orders){
        	If(Order.ccrz__OrderStatus__c == 'Completed'){
        		System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Shipment may not be stopped due to order status: ' + Order.ccrz__OrderStatus__c);
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Cancelled'){
        		System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order has already been cancelled');
        	}
        	else if(Order.ccrz__OrderStatus__c == 'Stopped Shipment' && Order.hmr_Program_Membership_Type__c != null){
        		 System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order number ' + Order.Name + ' has successfully been stopped and client has been dropped from AD');
        	}
        	// else if(Order.ccrz__OrderStatus__c == 'Order Submitted'){
        	// 	System.assertEquals(ccCancelOrder.stopShipment(Order.Id), 'Order number ' + Order.Name + ' has successfully been stopped');
        	// }

        }

         //checks to see that 3 Program Memberships were dropped
        List<Program_Membership__c> DropList = new List<Program_Membership__c>([SELECT Id, hmr_Status__c FROM Program_Membership__c WHERE hmr_Status__c = 'Dropped']);

        if(!DropList.isEmpty()){
            System.assertEquals(DropList.size(), 1);
        }
    }
}