/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 31st, 2017
Target Class: HMR_CMS_CommunityHero_ContentTemplate
*/

@isTest
public class HMR_CMS_CommunityHero_ContentTmpl_Test {
    @isTest static void test_general_method1() {
        // Implement test code
        
        Map<String, String> testVal = new Map<String, String>{
        
            'DisplaySearch' => 'FALSE',
            'TitleDisplayType' => 'Standard',
            'Subtitle' => 'test'
        
        };
        
        
        HMR_CMS_CommunityHero_ContentTemplate communityHero = new HMR_CMS_CommunityHero_ContentTemplate();          
        String propertyValue = communityHero.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');  
        String HeroSupplementCSSClass = communityHero.HeroSupplementCSSClass;  
        String HeroImage = communityHero.HeroImage;
        String Title = communityHero.Title;
        String TitleAlignmentCSSClass = communityHero.TitleAlignmentCSSClass;  
        String TitleDisplayType = communityHero.TitleDisplayType;
        String TitleIcon = communityHero.TitleIcon;
        String DisplaySearch = communityHero.DisplaySearch;
        String SubtitleCSSClass = communityHero.SubtitleCSSClass;
        String SearchPlaceholder = communityHero.SearchPlaceholder;
        String SearchType = communityHero.SearchType;
        String FilterCategory = communityHero.FilterCategory;
        String TitleCSSClass = communityHero.TitleCSSClass;
        String MinimumSearchLength = communityHero.MinimumSearchLength;
        
        communityHero.testAttributes = testVal;
        
        String respHTML = communityHero.getHTML();              
        
        System.assert(!String.isBlank(respHTML));
    }       
           
}