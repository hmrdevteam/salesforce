public class HMRMobFirePushNotificationCtlr {

  public String contactId {get; set;}

  public PageReference fireDayBeforeDiet() {
      HMRMobFirePushNotificationManually.fire('DAY_BEFORE_START_DATE_REMIDER');
      return null;
  }

  public PageReference fireTest() {
      HMRMobFirePushNotificationManually.fire('TEST_REMINDER');
      return null;
  }

  public PageReference fireWeighin() {
      HMRMobFirePushNotificationManually.fire('WEIGHIN_REMINDER');
      return null;
  }

  public PageReference fireFirstDayOfDiet() {
      HMRMobFirePushNotificationManually.fire('FIRST_DAY_OF_DIET_REMINDER');
      return null;
  }

  public PageReference fireInactiveReminder() {
      HMRMobFirePushNotificationManually.fire('INACTIVE_REMINDER');
      return null;
  }

  public PageReference deleteTokenForContact() {
    try {
      Contact c = [select hmr_Diet_Tracker_Token__c, id from contact where id =: contactId];
      c.hmr_Diet_Tracker_Token__c = null;
      update c;
      ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Tokens for the contact:' + contactId + ' have been removed'));
      contactId = null;
    }
    catch(Exception e) {
      ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage() ) );
      System.debug(e.getMessage());
    }
    return null;
  }

}