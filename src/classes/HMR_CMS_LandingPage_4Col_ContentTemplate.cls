/**
* Apex Content Template Controller for 4 column section on new landing pages
*
* @Date: 03/13/2018
* @Author Ali Pierre (HMR)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_LandingPage_4Col_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_LandingPage_4Col_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_LandingPage_4Col_ContentTemplate() {}

	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
	 */
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
	 */
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	public String numberOfColumns{
         get{
            return getPropertyWithDefault('numberOfColumns', '4');
        }
     }

    public String sectionGTMIDAttr{
         get{
            return getPropertyWithDefault('sectionGTMIDAttr', 'recognizeBlock2');
        }
    }

	public String Title{
        get{
            return getPropertyWithDefault('Title', '');
        }
    }

	public String Description{
        get{
            return getPropertyWithDefault('Description', '');
        }
    }

    public String name1{
        get{
            return getPropertyWithDefault('name1', '');
        }
    }
    public String name2{
        get{
            return getPropertyWithDefault('name2', '');
        }
    }
    public String name3{
        get{
            return getPropertyWithDefault('name3', '');
        }
    }

	public String image1Url{
        get{
            return getPropertyWithDefault('image1Url', '');
        }
    }
    public String image1Caption{
        get{
            return getPropertyWithDefault('image1Caption', '');
        }
    }
    public String image2Url{
        get{
            return getPropertyWithDefault('image2Url', '');
        }
    }
    public String image2Caption{
        get{
            return getPropertyWithDefault('image2Caption', '');
        }
    }
    public String image3Url{
        get{
            return getPropertyWithDefault('image3Url', '');
        }
    }
    public String image3Caption{
        get{
            return getPropertyWithDefault('image3Caption', '');
        }
    }
    public String image4Url{
        get{
            return getPropertyWithDefault('image4Url', '');
        }
    }
    public String image4Caption{
        get{
            return getPropertyWithDefault('image4Caption', '');
        }
    }    
	public Boolean showButton {
        get {
            return getAttribute('showButton') == 'true';
        }
    }
    public String buttonText{
        get{
            return getPropertyWithDefault('buttonText', '');
        }
    } 
    public String buttonTargetUrl{
        get{
            return getPropertyWithDefault('buttonTargetUrl', '');
        }
    }
    public String buttonMobileTargetUrl{
        get{
            return getPropertyWithDefault('buttonMobileTargetUrl', '');
        }
    }

	public String return3ColumnHTML() {
		String html = '';
		html += '<section class="hmr-page-section-google-reviews">' +
			        '<div class="container hmr-plans-container-centered-google-reviews">' +
			            '<div class="row">' +
			                '<h2 class="google-review-title">' + Title + '</h2>' +
			            '</div>' +
			            '<div class="container-fluid text-center">'+
						  '<div class="row">'+
						    '<div class="col-sm-4"> '+
						    	'<div class="google-review-container">' +
						    		'<p class="google-review-name">' + name1 + '</p>'+
						      		'<img src="'+ image1Url +'" class="google-review-stars">'+
						      		'<p class="google-review">' + image1Caption + '</p>'+
						    	'</div>'+						      
						    '</div>'+
						    '<div class="col-sm-4"> '+
						    	'<div class="google-review-container">' +
						    		'<p class="google-review-name">' + name2 + '</p>'+
						      		'<img src="'+ image2Url +'" class="google-review-stars">'+
						      		'<p class="google-review">' + image2Caption + '</p>'+
						    	'</div>'+						      
						    '</div>'+
						    '<div class="col-sm-4"> '+
						    	'<div class="google-review-container">' +
						    		'<p class="google-review-name">' + name3 + '</p>'+
						      		'<img src="'+ image3Url +'" class="google-review-stars">'+
						      		'<p class="google-review">' + image3Caption + '</p>'+
						    	'</div>'+						      
						    '</div>'+
						  '</div>'+			            
			        '</div>' +
			    '</section>';
		return html;
	}

	public String return4ColumnHTML() {
		String html = '';
		html += '<section class="hmr-page-section-landing">' +
			        '<div class="container hmr-plans-container-centered-landing-4">' +
			            '<div class="row">' +
			                '<h2 class="col-4-title">' + Title + '</h2>' +
			            '</div>' +
			            '<div class="row">' +
			                '<p class="landing-4-desc">' + Description + '</p>' +
			            '</div>' +
			            '<div class="container-fluid text-center">'+
						  '<div class="row">'+
						    '<div class="col-sm-3">'+
						      '<img src="https://www.myhmrprogram.com/ContentMedia/LandingPages/'+ image1Url +'.jpg" class="col-4-img">'+
						      '<p class="img-caption-4-col">' + image1Caption + '</p>'+
						    '</div>'+
						    '<div class="col-sm-3">'+
						      '<img src="https://www.myhmrprogram.com/ContentMedia/LandingPages/'+ image2Url +'.jpg" class="col-4-img">'+
						      '<p class="img-caption-4-col">' + image2Caption + '</p>'+
						    '</div>'+
						    '<div class="col-sm-3"> '+
						      '<img src="https://www.myhmrprogram.com/ContentMedia/LandingPages/'+ image3Url +'.jpg" class="col-4-img">'+
						      '<p class="img-caption-4-col">' + image3Caption + '</p>'+
						    '</div>'+
						    '<div class="col-sm-3">'+
						      '<img src="https://www.myhmrprogram.com/ContentMedia/LandingPages/'+ image4Url +'.jpg" class="col-4-img">'+
						      '<p class="img-caption-4-col">' + image4Caption + '</p>'+
						    '</div>'+
						  '</div>'+
						'</div>';
						if(showButton){
							html += '<div class="row">' +
			                '<div class="btn-container">' +
			                    '<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + buttonTargetUrl + '" class="hidden-sm hidden-xs btn btn-primary hmr-btn-blue hmr-btn-small">' + buttonText + '</a>' +
			                    '<a data-gtm-id="' + sectionGTMIDAttr + '" href="' + buttonMobileTargetUrl + '" class="hidden-lg hidden-md btn btn-primary hmr-btn-blue hmr-btn-small">' + buttonText + '</a>' +
			                '</div>';
						}			            
			            html += '</div>' +
			        '</div>' +
			    '</section>';
		return html;
	}


	global virtual override String getHTML(){
		String html = '';
		try {
			if(numberOfColumns == '3'){
				html = return3ColumnHTML();
			}
			else if(numberOfColumns == '4'){
				html = return4ColumnHTML();
			}
		}
		catch(Exception ex) {
			System.debug('Exception in Landing Page Section Content Template');
			System.debug(ex);
			html += ex.getMessage();
		}
		return html;
	}
}