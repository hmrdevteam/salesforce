/**
* Apex Content Template Controller for Zip Code Finder Page
*
* @Date: 2017-03-206
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_ZipCodeFinder_ContentTemplate extends cms.ContentTemplateController{
	
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_ZipCodeFinder_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_ZipCodeFinder_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Title Text Property
    public String TitleText{    
        get{
            return getPropertyWithDefault('TitleText', 'Let\'s Get Started');
        }    
    }

    //Description Text Property
    public String DescriptionText{    
        get{
            return getPropertyWithDefault('DescriptionText', '');
        }    
    }

    //Hero Background Image Property
    public String BackgroundImage{
        get{
            return getProperty('BackgroundImage');
        }
    }

    //Other Link (No Thanks) Label Property
    public String NoThanksLinkLabelText{
    	get{
            return getPropertyWithDefault('NoThanksLinkLabelText', 'NO THANKS >');
        }
    }

    //Other Link (No Thanks) Page Link Property
    public cms.Link NoThanksLinkLinkObj{
        get{
            return this.getPropertyLink('NoThanksLinkLinkObj');
        }
    }

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        String sitePrefix = Site.getBaseUrl();
        String noThanksLinkUrl = '';
        noThanksLinkUrl = (this.NoThanksLinkLinkObj != null) ? this.NoThanksLinkLinkObj.targetPage : '#';        
        html += '<div id="zipImage" class="right-photo col-xs-12 col-sm-6 pull-right landing-block-md" style= "background-image : url(\'' + sitePrefix + BackgroundImage + '\');background-repeat:no-repeat;background-size:cover;"></div>' +
	            '<div class="col-xs-12 col-sm-6 pull-left landing-block-md started">' +
	                '<h3 class="started-text-header">' +
	                	TitleText +
	                '</h3>' +
	                '<p class="started-text-body">' +
	                	DescriptionText +
	                '</p>' +
	                '<div class="margin-top-35">' +
	                    '<div class="input-group">' +
	                        '<input type="text" class="zip-input form-control zip-input" id="zipcodefinderInput" placeholder="Enter your zip code" />' +
	                        '<span class="input-group-addon white-background" id="zipcodebutton">' +
	                            '<a href="#">' +
	                                '<i class="hmr-icon-search search-margin"></i>' +
	                            '</a>' +
	                        '</span>' +
	                    '</div>' +
	                    '<a href="' + noThanksLinkUrl + '" class="no-thanks no-thanks-link">' + NoThanksLinkLabelText + '</a>' +
	                '</div>' +
	            '</div>';
        return html;
    }
}