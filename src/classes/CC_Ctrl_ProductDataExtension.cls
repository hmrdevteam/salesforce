/**
*This is controller for Product List Page fetching product data and add to cart
* @Date: 2017-1-18
* @Author Pranay Mistry (Magnet 360)
* @Updated by : Aslesha(Mindtree)for add to cart
* @Updated by : Pranay Mistry to modify query to include Allergen data https://reside.jira.com/browse/HPRP-1796
* @JIRA: https://reside.jira.com/browse/HPRP-80,https://reside.jira.com/browse/HPRP-1812
*/
global with sharing class CC_Ctrl_ProductDataExtension {

    global Boolean isCSRFlow { get; set; }
    global Integer entreeLimit { get; set; }
    global Integer entreeProgress { get; set; }

    global CC_Ctrl_ProductDataExtension(HMR_CC_CheckOut_Extension c) {}

    global CC_Ctrl_ProductDataExtension() {
        Map<String, String> pageParams = ApexPages.currentPage().getParameters();
        if(pageParams.get('portalUser') != null && pageParams.get('portalUser') != '') {
            isCSRFlow = true;
            entreeLimit = 7;
            entreeProgress = 0;
        }
        else {
            isCSRFlow = false;
            entreeLimit = 42;
            entreeProgress = 42;
        }
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getExtendedDataForProducts(final ccrz.cc_RemoteActionContext ctx, List<string> productIds){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            Map<String, String> hmrADLegalCopies = new Map<String, String>{
                'P1 Healthy Solutions' => String.valueOf(System.Label.HMR_AD_LegalMandatory_HSAH),
                'P1 Healthy Shakes' => String.valueOf(System.Label.HMR_AD_LegalMandatory_HS),
                'Phase 2' => String.valueOf(System.Label.HMR_AD_LegalMandatory_P2)
            };
            Map<String, String> hmrADLegalCheckboxCopies = new Map<String, String>{
                'P1 Healthy Solutions' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_HSAH),
                'P1 Healthy Shakes' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_HS),
                'Phase 2' => String.valueOf(System.Label.HMR_AD_LegalMandatory_Checkbox_P2)
            };
            //Map<String,ccrz__E_Product__c > dataMap = new Map<String, ccrz__E_Product__c>();
            Map<String, Object > dataMap = new Map<String, Object>();

            Map<Id, ccrz__E_Product__c> cc_ProductsMap = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, hmr_Ingredients__c, ccrz__ShortDesc__c, ccrz__SKU__c, Program__r.Name,
                    (SELECT ccrz__Spec__r.Name, ccrz__Spec__r.ccrz__SpecGroup__c, ccrz__SpecValue__c
                        FROM ccrz__Product_Specs__r Order by ccrz__Spec__r.ccrz__sequence__c ASC),
                    (SELECT ccrz__MediaType__c, ccrz__URI__c
                        FROM ccrz__E_ProductMedias__r WHERE ccrz__MediaType__c = 'Product Image Thumbnail')
                  FROM ccrz__E_Product__c WHERE Id IN :productIds] );
            //SELECT c.Id, c.Name, c.hmr_Ingredients__c, c.ccrz__ShortDesc__c,c.ccrz__SKU__c,
            // (SELECT ccrz__Spec__r.Name, ccrz__Spec__r.ccrz__SpecGroup__c, ccrz__SpecValue__c FROM ccrz__Product_Specs__r)
            //FROM ccrz__E_Product__c WHERE c.Id IN :productIds
            /*
            old query
            SELECT c.Id, c.Name, c.hmr_Ingredients__c, c.ccrz__ShortDesc__c,c.ccrz__SKU__c,
                    (SELECT ccrz__SpecValue__c FROM ccrz__Product_Spec_Indicies__r WHERE CCRZ__Spec__r.Name = 'Allergens')
                        FROM ccrz__E_Product__c c WHERE c.Id IN :productIds
            */
            for(ccrz__E_Product__c productItem: cc_ProductsMap.values()){
                dataMap.put(String.valueOf(productItem.Id), (ccrz__E_Product__c)productItem);
                if(productItem.Program__r.Name != null && productItem.Program__r.Name != '')  {
                    String legalLang = String.valueOf(productItem.Id) + '_legalLang';
                    String legalCheckboxLang = String.valueOf(productItem.Id) + '_legalCheckboxLang';
                    dataMap.put(legalLang, hmrADLegalCopies.get(productItem.Program__r.Name));
                    dataMap.put(legalCheckboxLang, hmrADLegalCheckboxCopies.get(productItem.Program__r.Name));
                }
            }

            res.success = true;
            res.data = dataMap;
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getExtendedDataForProducts ' + res + ' ' + err.getMessage());
        }
        res.success = true;
        return null;
    }

    //Remote method will take product Id from page, finds its program and create new Program Membership Record
    // @RemoteAction
    // global static ccrz.cc_RemoteActionResult createProgramMembership(final ccrz.cc_RemoteActionContext ctx, final String productId){
    //     ccrz.cc_CallContext.initRemoteContext(ctx);
    //     ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
    //     res.inputContext = ctx;
    //     res.success = false;
    //     try {
    //         LIST<ccrz__E_Product__c> productInfo = [SELECT Id, Program__c FROM ccrz__E_Product__c WHERE Id = :productId];
    //         if(productInfo != null && productInfo.size() > 0){
    //             LIST<User> currentContact = [SELECT Id, ContactId FROM User WHERE id = :UserInfo.getUserId()];
    //             Id hmrContactId = null;
    //             if(currentContact != null && currentContact.size() > 0){
    //                 hmrContactId = currentContact[0].ContactId;
    //             }
    //             Program_Membership__c member = new Program_Membership__c();
    //             member.Program__c = productInfo[0].Program__c;
    //             member.hmr_Contact__c = hmrContactId;
    //             insert member;
    //             res.success = true;
    //             res.data = member;
    //         }
    //     }
    //     catch (Exception err){
    //         res.success = false;
    //         res.data = new Map<String, Object>{
    //                   'Exception' => err.getMessage(),
    //                   'Line' => err.getLineNumber()
    //                 };
    //     }
    //     return res;
    // }
}