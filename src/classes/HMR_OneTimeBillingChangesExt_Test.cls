/*****************************************************
 * Author: Utkarsh Goswami
 * Created Date: 06/23/2017
 * Created By: Utkarsh Goswami
 * Last Modified Date:
 * Last Modified By:
 * Description: Test Coverage for HMR_OneTimeBillingChangesExt class
 * ****************************************************/
@isTest
private class HMR_OneTimeBillingChangesExt_Test{


  @isTest
  private static void testMethodForselectPayment(){


        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id, MailingState = 'CA');
        insert con;

        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

       insert userTest;

      System.runAs(userTest){

          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card');
          insert orderObj;

          ccrz__E_StoredPayment__c storedPaymentObj = new ccrz__E_StoredPayment__c(Name = 'test', ccrz__Account__c = testAcc.id,ccrz__Enabled__c = TRUE, ccrz__User__c = userTest.id );

          insert storedPaymentObj;

          ccrz__E_TransactionPayment__c transactionPayment = new ccrz__E_TransactionPayment__c(ccrz__Account__c = testAcc.id, ccrz__AccountType__c = 'test',
                                                               ccrz__CCOrder__c = orderObj.id);
          insert transactionPayment;

          List<ccrz__E_StoredPayment__c> spList = new List<ccrz__E_StoredPayment__c>();
          spList.add(storedPaymentObj);

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeBillingChangesExt billingExtController = new HMR_OneTimeBillingChangesExt(sc);


          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('paymentId', storedPaymentObj.id);

          billingExtController.order = orderObj;
          billingExtController.paymentList = spList;
          billingExtController.orderId = orderObj.id;
          billingExtController.selectedPaymentId = storedPaymentObj.id;
          billingExtController.selectedPayment = storedPaymentObj;

          billingExtController.selectPayment();

          PageReference cancel = billingExtController.cancel();

          List<ccrz__E_StoredPayment__c> sp = [Select id FROM ccrz__E_StoredPayment__c where Name = 'test'];

          System.assertEquals(sp.size(), 1);

          System.assert(!String.isBlank(String.valueOf(cancel)));

      }

  }

  @isTest
  private static void testMethodForUpdateOrder(){

        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;


          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card');
          insert orderObj;

          ccrz__E_StoredPayment__c storedPaymentObj = new ccrz__E_StoredPayment__c(Name = 'test', ccrz__Account__c = testAcc.id,ccrz__Enabled__c = TRUE );

          insert storedPaymentObj;

          ccrz__E_TransactionPayment__c transactionPayment = new ccrz__E_TransactionPayment__c(ccrz__Account__c = testAcc.id, ccrz__AccountType__c = 'test',
                                                               ccrz__CCOrder__c = orderObj.id);
          insert transactionPayment;

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeBillingChangesExt billingExtController = new HMR_OneTimeBillingChangesExt(sc);


          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('paymentId', storedPaymentObj.id);

          billingExtController.selectedPayment = storedPaymentObj;

          PageReference updateOder = billingExtController.updateOrder();

          System.assert(!String.isBlank(String.valueOf(updateOder)));

  }

  @isTest
  private static void testMethodForUpdateOrderNoTP(){

        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;


          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card');
          insert orderObj;

          ccrz__E_StoredPayment__c storedPaymentObj = new ccrz__E_StoredPayment__c(Name = 'test', ccrz__Account__c = testAcc.id,ccrz__Enabled__c = TRUE );

          insert storedPaymentObj;

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeBillingChangesExt billingExtController = new HMR_OneTimeBillingChangesExt(sc);


          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('paymentId', storedPaymentObj.id);

          billingExtController.selectedPayment = storedPaymentObj;

          PageReference updateOder = billingExtController.updateOrder();

          System.assert(!String.isBlank(String.valueOf(updateOder)));

  }


   @isTest
  private static void testMethodForUpdateOrderNegative(){

          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card');
          insert orderObj;


          ccrz__E_TransactionPayment__c transactionPayment = new ccrz__E_TransactionPayment__c(ccrz__AccountType__c = 'test', ccrz__CCOrder__c = orderObj.id);
          insert transactionPayment;

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeBillingChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeBillingChangesExt billingExtController = new HMR_OneTimeBillingChangesExt(sc);


          PageReference updateOder = billingExtController.updateOrder();

          System.assert(String.isBlank(String.valueOf(updateOder)));

  }

}