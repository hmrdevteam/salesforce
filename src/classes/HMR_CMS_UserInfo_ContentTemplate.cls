global virtual with sharing class HMR_CMS_UserInfo_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_UserInfo_ContentTemplate(){}
	global HMR_CMS_UserInfo_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global String returnNotNullAndNotEmptyString(String value) {
		if(value != null && value != '') {
			return value;
		}
		return '';
	}
	global virtual override String getHTML() {
		String html = '';
		try {
			String currentUserId;
			String portalUserId;

			if(ApexPages.currentPage().getParameters().get('portalUser') != null)
				portalUserId = ApexPages.currentPage().getParameters().get('portalUser');

			if(!String.isBlank(portalUserId)){				
			 	currentUserId = portalUserId;
			}
			else{
				currentUserId = UserInfo.getUserId();
			}
			//current user profile Id
			String currentUserProfileId = UserInfo.getProfileId();
			String currentContactId = '';String currentAccountId = '';
			String currentAccountGroupId = '';String currentAccountGroupName = '';
			String currentContactFirstName = '';
			String currentContactEmail = '';
			String cartsfdcId = '';
			String cartOwnerID = '';String cartEncryptedId = '';
			Integer cartItemCount = 0;
			Decimal cartTotalAmount = 0.0;
			//check if current user is the site guest user
			User currentUser = [SELECT ContactId, Profile.Name FROM User WHERE Id =: currentUserId];
			Boolean isCurrentUserGuest = (currentUser.Profile.Name == 'HMR Program Community Profile');
			Contact currentContact;
			Account currentAccount;

			ccrz__E_AccountGroup__c currentAccountGroup;

			if(currentUser.ContactId != null){
				currentContact = [SELECT Name, FirstName, LastName, AccountId, Account.ccrz__E_AccountGroup__c, Account.ccrz__E_AccountGroup__r.Name, Email  FROM Contact WHERE Id = :currentUser.ContactId];
				//currentAccount = [SELECT Id, Name, ccrz__E_AccountGroup__c FROM Account WHERE Id = :currentContact.AccountId];
				//currentAccountGroup = [SELECT Id, Name FROM ccrz__E_AccountGroup__c WHERE Id = :currentAccount.ccrz__E_AccountGroup__c];
				currentContactId = currentContact.Id;
				currentAccountId = currentContact.AccountId;
				currentContactFirstName = currentContact.FirstName;
				currentContactEmail = currentContact.Email;
				if(currentContact.Account != null){
					currentAccountGroupId = currentContact.Account.ccrz__E_AccountGroup__c;
					if(currentContact.Account.ccrz__E_AccountGroup__r != null)
						currentAccountGroupName = currentContact.Account.ccrz__E_AccountGroup__r.Name;	
				}			
			}

			//retrieve cart data - too process heavy here, retrieve via separate thread JS call
			/*
			//instantiate cart service
			HMR_Cart_Service cartService = new HMR_Cart_Service(currentUserId, isCurrentUserGuest);
			//reduce sizing call for needed data
			cartService.ccrzApiSizing = new Map<String, Object>{ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
		            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M,
		            ccrz.ccAPI.SZ_REL => new List<String>{'E_CartItems__r'},
		            ccrz.ccAPI.SZ_ASSC => false
		    }};

			//get active cart
			//HMR_Cart_Service.CartRecord cartRecord = cartService.getCartByUserId(currentUserId);

	        if(cartRecord != null){
	        	cartTotalAmount = cartRecord.totalAmount;
				cartsfdcId = cartRecord.id;
				cartEncryptedId = cartRecord.encryptedId;
				cartOwnerId = cartRecord.ownerId;
				cartItemCount = cartRecord.cartItemList.size();
	        }*/

			String currentPageHost = ApexPages.currentPage().getHeaders().get('Host');
			html += '<input type="hidden" id="current_user_id" value="' + currentUserId + '"/>' +
			 		'<input type="hidden" id="current_user_email" value="' + returnNotNullAndNotEmptyString(currentContactEmail) + '"/>' +
					'<input type="hidden" id="current_user_profile_id" value="' + currentUserProfileId + '"/>' +
					'<input type="hidden" id="current_user_first_name" value="' + returnNotNullAndNotEmptyString(currentContactFirstName) + '"/>' +
					'<input type="hidden" id="current_user_contact_id" value="' + returnNotNullAndNotEmptyString(currentContactId) + '"/>' +
					'<input type="hidden" id="current_user_isguest" value="' + isCurrentUserGuest + '"/>' +
					'<input type="hidden" id="current_user_account_id" value="' + returnNotNullAndNotEmptyString(currentAccountId) + '"/>' +
					'<input type="hidden" id="current_user_account_group_id" value="' + returnNotNullAndNotEmptyString(currentAccountGroupId) + '"/>' +
					'<input type="hidden" id="current_user_account_group_name" value="' + returnNotNullAndNotEmptyString(currentAccountGroupName) + '"/>' +
					'<input type="hidden" id="current_page_host" value="' + returnNotNullAndNotEmptyString(currentPageHost) + '"/>' +
					'<input type="hidden" id="current_user_cart_sfdc_id" value="' + returnNotNullAndNotEmptyString(cartsfdcId) + '"/>' +
					'<input type="hidden" id="current_user_cart_owner_id" value="' + returnNotNullAndNotEmptyString(cartOwnerID) + '"/>' +
					'<input type="hidden" id="current_user_cart_encrypted_id" value="' + returnNotNullAndNotEmptyString(cartEncryptedId) + '"/>' + 
					'<input type="hidden" id="current_user_cart_total_amount" value="' + String.valueOf(cartTotalAmount) + '"/>' + 
					'<input type="hidden" id="current_user_cart_item_count" value="' + String.valueOf(cartItemCount) + '"/>';			
		}
		catch(Exception ex) {
			html += 'Something went wrong ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
		}
		return html;
	}
}