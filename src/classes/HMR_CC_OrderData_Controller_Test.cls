/**
* @Date: 5/23/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
* Class : HMR_CC_OrderData_Controller
*/


@isTest
private class HMR_CC_OrderData_Controller_Test{

  @isTest static void test_method_one() {
   
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com');
        insert orderObj;
        
        ccrz.cc_RemoteActionResult orderResp = HMR_CC_OrderData_Controller.getExtendedOrderData(ctx, orderObj.id);  
        
         HMR_CC_OrderData_Controller.getExtendedOrderData(ctx, '');    
        
        System.assert(!String.isBlank(String.valueOf(orderResp)));

    }  

}