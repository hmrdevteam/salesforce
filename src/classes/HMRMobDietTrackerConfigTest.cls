/**
 * this test verifies that the diet tracker configuration data is return by the rest service
 * the configuration data is used only by the mobile app - there is no logic to test 
 */
@isTest(seeAllData=true)
public class HMRMobDietTrackerConfigTest {

    @isTest
    public static void test1() {
	  User userRecord = cc_dataFactory.testUser;
        
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/dietTracker/config?allRecords=false&program=Phase%202&dietType=Phase%202';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	 HMRMobDietTrackerConfig.doGet();
      }
      Test.stopTest();
      System.assertEquals(200, response.statuscode);
    }
    
    @isTest
    public static void test2() {
	  User userRecord = cc_dataFactory.testUser;
        
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/dietTracker/config?allRecords=true';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	 HMRMobDietTrackerConfig.doGet();
      }
      Test.stopTest();
      System.assertEquals(200, response.statuscode);
    }    
}