/*
Developer: Ali Pierre (HMR)
Date: 11/30/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_P2_KitConfig_ContentTemplate
*/
@isTest(SeeAllData=true) 
private class HMR_CMS_P2_KitConfig_Content_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_P2_KitConfig_ContentTemplate p2kitConfigTemplate = new HMR_CMS_P2_KitConfig_ContentTemplate();
	    String propertyValue = p2kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = p2kitConfigTemplate.kitSKU;                
        Boolean isAdmin = p2kitConfigTemplate.isAdmin;

	    p2kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'P2ADMIN',
            'isAdmin' => 'TRUE'
        };

	    string s1 = p2kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'P2ADMIN');

	    String renderHTML = p2kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_P2_KitConfig_ContentTemplate kc = new HMR_CMS_P2_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_P2_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}

	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_P2_KitConfig_ContentTemplate p2kitConfigTemplate = new HMR_CMS_P2_KitConfig_ContentTemplate();
	    String propertyValue = p2kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    String kitSKU = p2kitConfigTemplate.kitSKU;                
        Boolean isAdmin = p2kitConfigTemplate.isAdmin;

	    p2kitConfigTemplate.testAttributes = new Map<String, String>  {
            'kitSKU' => 'P2KIT',
            'isAdmin' => 'FALSE'
        };

	    string s1 = p2kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    system.assertEquals(s1, 'P2KIT');

	    String renderHTML = p2kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_P2_KitConfig_ContentTemplate kc = new HMR_CMS_P2_KitConfig_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_P2_KitConfig_ContentTemplate(null) ' + e);
	    }
  	}
}