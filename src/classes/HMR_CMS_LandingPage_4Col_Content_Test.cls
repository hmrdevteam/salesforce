/*
Developer: Mustafa Ahmed (HMR)
Date: 03/16/2018
Modified: 
Modified Date : 
Target Class: HMR_CMS_LandingPage_4Col_ContentTemplate
*/
@isTest(SeeAllData=false) 

public class HMR_CMS_LandingPage_4Col_Content_Test {
	@isTest static void test_method_one() {
	    // Implement test code
	    HMR_CMS_LandingPage_4Col_ContentTemplate kitConfigTemplate = new HMR_CMS_LandingPage_4Col_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    //String kitSKU = kitConfigTemplate.kitSKU;                
        //Boolean isAdmin = kitConfigTemplate.isAdmin;
        
        String name1 = kitConfigTemplate.name1;
        String name2 = kitConfigTemplate.name2;
        String name3 = kitConfigTemplate.name3;
        String buttonText = kitConfigTemplate.buttonText;
        String buttonTargetUrl = kitConfigTemplate.buttonTargetUrl;
        Boolean showButton = kitConfigTemplate.showButton;
        String numberOfColumns = kitConfigTemplate.numberOfColumns;
        String sectionGTMIDAttr = kitConfigTemplate.sectionGTMIDAttr;
        String buttonMobileTargetUrl = kitConfigTemplate.buttonMobileTargetUrl;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            //'kitSKU' => 'SHAKEM',
            'name1' => 'testName1',
            'name2' => 'testName2',
            'name3' => 'testName3',
            'buttonText' => 'testName4',
            'buttonTargetUrl' => 'testName5',
            'showButton' => 'TRUE',
            'numberOfColumns' => '3',
            'sectionGTMIDAttr' => 'sectionGTMIDAttr',
            'buttonMobileTargetUrl' => 'testName6'
        };

	    string s1 = kitConfigTemplate.getPropertyWithDefault('name1','testName1');
	    system.assertEquals(s1, 'testName1');

	    string s2 = kitConfigTemplate.getPropertyWithDefault('name2','testName2');
	    system.assertEquals(s2, 'testName2');

	    string s3 = kitConfigTemplate.getPropertyWithDefault('name3','testName3');
	    system.assertEquals(s3, 'testName3');

	    string s4 = kitConfigTemplate.getPropertyWithDefault('buttonText','testName4');
	    system.assertEquals(s4, 'testName4');

	    string s5 = kitConfigTemplate.getPropertyWithDefault('buttonTargetUrl','testName5');
	    system.assertEquals(s5, 'testName5');

	    string s6 = kitConfigTemplate.getPropertyWithDefault('buttonMobileTargetUrl','testName6');
	    system.assertEquals(s6, 'testName6');

	    String renderHTML = kitConfigTemplate.return3ColumnHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_LandingPage_4Col_ContentTemplate kc = new HMR_CMS_LandingPage_4Col_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_LandingPage_4Col_ContentTemplate(null) ' + e);
	    }
  	}

	@isTest static void test_method_two() {
	    // Implement test code
	    HMR_CMS_LandingPage_4Col_ContentTemplate kitConfigTemplate = new HMR_CMS_LandingPage_4Col_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    //String kitSKU = kitConfigTemplate.kitSKU;                
        //Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            //'kitSKU' => 'HSQSKRVC',
            //'isAdmin' => 'FALSE'
        };

	    //string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    //system.assertEquals(s1, 'HSQSKRVC');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_LandingPage_4Col_ContentTemplate kc = new HMR_CMS_LandingPage_4Col_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_LandingPage_4Col_ContentTemplate(null) ' + e);
	    }
  	}

  	@isTest static void test_method_three() {
	    // Implement test code
	    HMR_CMS_LandingPage_4Col_ContentTemplate kitConfigTemplate = new HMR_CMS_LandingPage_4Col_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    //String kitSKU = kitConfigTemplate.kitSKU;                
        //Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            //'kitSKU' => 'HSQSKRVCADMIN',
            //'isAdmin' => 'TRUE'
        };

	    //string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    //system.assertEquals(s1, 'HSQSKRVCADMIN');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_LandingPage_4Col_ContentTemplate kc = new HMR_CMS_LandingPage_4Col_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_LandingPage_4Col_ContentTemplate(null) ' + e);
	    }
  	}

  	@isTest static void test_method_four() {
	    // Implement test code
	    HMR_CMS_LandingPage_4Col_ContentTemplate kitConfigTemplate = new HMR_CMS_LandingPage_4Col_ContentTemplate();
	    String propertyValue = kitConfigTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

	    //String kitSKU = kitConfigTemplate.kitSKU;                
        //Boolean isAdmin = kitConfigTemplate.isAdmin;

	    kitConfigTemplate.testAttributes = new Map<String, String>  {
            //'kitSKU' => 'SHAKEMADMIN',
            //'isAdmin' => 'TRUE'
        };

	    //string s1 = kitConfigTemplate.getPropertyWithDefault('kitSKU','testValue2');
	    //system.assertEquals(s1, 'SHAKEMADMIN');

	    String renderHTML = kitConfigTemplate.getHTML();
	    
	    System.assert(!String.isBlank(renderHTML));
	    try{
	        HMR_CMS_LandingPage_4Col_ContentTemplate kc = new HMR_CMS_LandingPage_4Col_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_LandingPage_4Col_ContentTemplate(null) ' + e);
	    }
  	}

}