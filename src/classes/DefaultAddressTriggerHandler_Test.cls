/**
* 
*
* @Date: 05/25/2017
* @Author Utkarsh Goswami (Mindtree)
* 
* @Class : DefaultAddressTriggerHandler
*/

@isTest
private class DefaultAddressTriggerHandler_Test {

    @isTest
    private static void test_method_one(){
    
    
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert userTest;
        
        System.runAs(userTest){

            ccrz__E_ContactAddr__c contactAdrObj = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'test first', ccrz__LastName__c = 'test last', ccrz__AddressFirstline__c = 'test addr',
                                                    ccrz__City__c = 'test city');
            insert contactAdrObj;
            
            ccrz__E_AccountAddressBook__c accountAddrObj = new ccrz__E_AccountAddressBook__c(Name = 'test', ccrz__E_ContactAddress__c = contactAdrObj.id,ccrz__Default__c = TRUE,ccrz__AddressType__c = 'Shipping');
            
            insert accountAddrObj;
            
            ccrz__E_AccountAddressBook__c accountAddrObj2 = new ccrz__E_AccountAddressBook__c(Name = 'test2', ccrz__E_ContactAddress__c = contactAdrObj.id,ccrz__Default__c = TRUE,ccrz__AddressType__c = 'Billing');
            
            insert accountAddrObj2;
           
            accountAddrObj.ccrz__AddressType__c = 'Shipping';
            accountAddrObj.ccrz__Default__c = FALSE;
            update accountAddrObj;
            
            accountAddrObj.ccrz__AddressType__c = 'Shipping';
            accountAddrObj.ccrz__Default__c = TRUE;
            update accountAddrObj;
            
            accountAddrObj.ccrz__AddressType__c = 'Shipping';
            accountAddrObj.ccrz__Default__c = FALSE;
            update accountAddrObj;
            
            accountAddrObj.ccrz__AddressType__c = 'Billing';
            update accountAddrObj;
        }
 
    }
    
    @isTest
    private static void test_method_second(){
    
         Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert userTest;

        ccrz__E_ContactAddr__c contactAdrObj = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'test first', ccrz__LastName__c = 'test last', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test city');
        insert contactAdrObj;
        
        ccrz__E_AccountAddressBook__c accountAddrObj = new ccrz__E_AccountAddressBook__c(Name = 'test', ccrz__E_ContactAddress__c = contactAdrObj.id,ccrz__Default__c = FALSE,
                                                            ownerId = userTest.id,ccrz__AddressType__c = 'Shipping');
        
        insert accountAddrObj;
        
         ccrz__E_AccountAddressBook__c accountAddrObj2 = new ccrz__E_AccountAddressBook__c(Name = 'test2', ccrz__E_ContactAddress__c = contactAdrObj.id,ccrz__Default__c = FALSE,
                                                            ownerId = userTest.id,ccrz__AddressType__c = 'Shipping');
        
        insert accountAddrObj2;

        
        accountAddrObj.ccrz__AddressType__c = 'Shipping';
        accountAddrObj.ccrz__Default__c = TRUE;
        update accountAddrObj;
        
        accountAddrObj.ccrz__AddressType__c = 'Shipping';
        accountAddrObj.ccrz__Default__c = FALSE;
        update accountAddrObj;
        
        accountAddrObj.ccrz__AddressType__c = 'Billing';
        update accountAddrObj;
 
    }
}