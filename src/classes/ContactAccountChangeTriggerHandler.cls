/**
* Called from SetContactTimeZone.trigger
* Updating Account field on open carts and Active orders when Contact Accounthas been updated
*
* Objects referenced --> Contact, ccrz__E_Cart__c, ccrz__E_Order__c
*
* @Date: 2017-8-17
* @Author Ali Pierre (HMR)
*/
public class ContactAccountChangeTriggerHandler {
	/**
    *  handleContactAfterUpdate, first method to be called from the Trigger (After update only)
    *                     this method will proccess updated contact records and check to see if Account fields have been updated.
    *
    *  @param Trigger.new --> List of new or updated Contact records
    *  @param Trigger.OldMap --> List of old version of the Contact records
    *  @return void
    */
    public static void handleContactAfterUpdate(List<Contact> newUpdatedContacts, Map<Id, Contact> oldContactVersionsMap){
        
        //declare Map to store Contacts and new Accounts for contact records that have updated Account Fields
        Map<Id, Id> updatedAcctContacts = new Map<Id, Id>();
        //Declare set of Ids to store updated contacts with new Accounts
        Set<Id> contactIds = new Set<Id>();
        
        //Loop through contacts 
        for(Contact updatedContact : newUpdatedContacts) {
        	//If Account field has been updated to a new account, store Contact Id and AccountId in updatedAcctContacts Map
            if(oldContactVersionsMap.containsKey(updatedContact.Id) && updatedContact.AccountId != oldContactVersionsMap.get(updatedContact.Id).AccountId){
                updatedAcctContacts.put(updatedContact.Id, updatedContact.AccountId);
                //Add Contact Id to contactIds set
                contactIds.add(updatedContact.Id);
            }
        }

        if(!updatedAcctContacts.isEmpty()){
        	updateCartsAndOrders(updatedAcctContacts, contactIds);
        }
    }

    /**
    *  updateCartsAndOrders, this method will update the Account field on Open Carts and Active orders when Contact's Account has been changed.
    *
    *  @param Trigger.new --> List of new or updated Contact records
    *  @param Trigger.OldMap --> List of old version of the Contact records
    *  @return void
    */

    public static void updateCartsAndOrders(Map<Id, Id> updatedAcctContacts, Set<Id> contactIds){
    	//Declare Lists to store active orders and open carts related to updated Clients
        List<ccrz__E_Order__c> activeOrders = new List<ccrz__E_Order__c>();
        List<ccrz__E_Cart__c> openCarts = new List<ccrz__E_Cart__c>();
        //Declare Lists to store active orders and open carts to be updated
        List<ccrz__E_Order__c> ordersToUpdate = new List<ccrz__E_Order__c>();
        List<ccrz__E_Cart__c> cartsToUpdate = new List<ccrz__E_Cart__c>();

        //Retrieve Pending and Template orders with active program memberships
        activeOrders = [SELECT Id, ccrz__Contact__c, ccrz__Account__c, ccrz__OrderStatus__c
        					FROM ccrz__E_Order__c WHERE ccrz__Contact__c IN :contactIds AND ccrz__OrderStatus__c IN('Template','Pending') AND Program_Membership_Status__c = 'Active'];
        //Retrieve open carts
        openCarts = [SELECT Id, ccrz__Contact__c, ccrz__Account__c, ccrz__CartStatus__c, ccrz__ShipMethod__c, ccrz__ShipAmount__c
        					FROM ccrz__E_Cart__c WHERE ccrz__CartStatus__c = 'Open' AND ccrz__Contact__c IN :contactIds];

        if(!activeOrders.isEmpty() || !openCarts.isEmpty()){
        	//Loop through active orders list and update Contact's active orders
        	for(ccrz__E_Order__c ord : activeOrders){
        		if(updatedAcctContacts.containsKey(ord.ccrz__Contact__c) && ord.ccrz__Account__c != updatedAcctContacts.get(ord.ccrz__Contact__c)){
        			ord.ccrz__Account__c = updatedAcctContacts.get(ord.ccrz__Contact__c);
        			ordersToUpdate.add(ord);
        		}
        	}
        	//Loop through active orders list and update Contact's open carts
        	for(ccrz__E_Cart__c cart : openCarts){
        		if(updatedAcctContacts.containsKey(cart.ccrz__Contact__c) && cart.ccrz__Account__c != updatedAcctContacts.get(cart.ccrz__Contact__c)){
        			cart.ccrz__Account__c = updatedAcctContacts.get(cart.ccrz__Contact__c);
                    cart.ccrz__ShipMethod__c = null;
                    cart.ccrz__ShipAmount__c = null;
        			cartsToUpdate.add(cart);
        		}
        	}
        }

        try {
            //DML update on the Updated Orders List
            if(!ordersToUpdate.isEmpty()) {
                update ordersToUpdate;
            }

            //DML Update on the Update Carts List
            if(!cartsToUpdate.isEmpty()) {
                update cartsToUpdate;
            }
        }
        catch(Exception ex) {
            System.debug('Error in updateCartsAndOrders update ' +ex.getMessage());
        }
    }
}