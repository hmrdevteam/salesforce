global with sharing class HMR_CMS_CSSClassMtd_ServiceInterface implements cms.ServiceInterface {
	public HMR_CMS_CSSClassMtd_ServiceInterface() {}
	/**
	 * executeRequest --> method to execute request
	 * @param params a map of parameters including
	 * @return a JSON-serialized response string
	*/
	global static String executeRequest(Map<String, String> params) {
		Map<String, Object> responseString = new Map<String, Object>();
		try {
			List<hmr_Content_Image_CSS_Class__mdt> cssClassMetadataTypes = new List<hmr_Content_Image_CSS_Class__mdt>(
				[SELECT MasterLabel, Desktop_Image_URL__c, Mobile_Image_URL__c, Image_Alignment__c FROM hmr_Content_Image_CSS_Class__mdt]);
			if(cssClassMetadataTypes != null && !cssClassMetadataTypes.isEmpty()) {
				responseString.put('success', true);
				responseString.put('cssClasses', cssClassMetadataTypes);
			}
			else {
				responseString.put('success', false);
				responseString.put('cssClasses', null);
			}
		}
		catch(Exception e) {
			responseString.put('success', false);
			responseString.put('cssClasses', null);
			responseString.put('exceptionMsg', e.getMessage());
			responseString.put('exceptiongetTypeName', e.getTypeName());
			responseString.put('exceptiongetLineNumber', e.getLineNumber());
		}
		return JSON.serialize(responseString);
	}

	/**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
    */
    public static Type getType() {
        return HMR_CMS_CSSClassMtd_ServiceInterface.class;
    }
}