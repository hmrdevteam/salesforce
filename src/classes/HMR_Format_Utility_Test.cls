/**
* Test Class Coverage of the HMR_Format_Utility Class
*
* @Date: 06/19/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/

@isTest
private class HMR_Format_Utility_Test{  
	
    @isTest
    private static void testCapitalize(){
        string testString = 'hello';
        
        Test.startTest();
        
        testString = HMR_Format_Utility.capitalize(testString);
        
        Test.stopTest();
        
        System.assertEquals('Hello', testString);
    }
    
    @isTest
    private static void testCapitalizeAll(){
        string testString = 'hello world';
        
        Test.startTest();
        
        testString = HMR_Format_Utility.capitalizeAll(testString);
        
        Test.stopTest();
        
        System.assertEquals('Hello World', testString);
    }
    
    @isTest
    private static void testUppercase(){
        string testString = 'hello world';
        
        Test.startTest();
        
        testString = HMR_Format_Utility.uppercase(testString);
        
        Test.stopTest();
        
        System.assertEquals('HELLO WORLD', testString);
    }
    
}