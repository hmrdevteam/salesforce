/**
* CMS Registration Service Interface that creates all the necessary records for a user in the community
*
* @Date: 2017-03-20
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

global class HMR_CMS_RegistrationServiceInterface implements cms.ServiceInterface{
    public HMR_CMS_RegistrationServiceInterface() {}

    /**
     * executeRequest --> method to execute request
     * @param params a map of parameters including
     * @return a JSON-serialized response string
     */
     public Account a = new Account();
     public String executeRequest(Map<String, String> params) {
        Map<String, Object> responseString = new Map<String, Object>();

        try {
            if(!params.isEmpty()) {
                String empCode = params.get('ec');
                String username = params.get('e');
                String password = params.get('p');
                String email = username;
                String confirmPassword = params.get('cp');
                String firstname = params.get('f');
                String lastname = params.get('l');
                String alias = firstname + lastname;
                String communityNickname = alias.left(8);
                String redirURL = params.get('t');
                String redirPlanURL = params.get('tp');
                String preference = params.get('ep');
                String zipcode = params.get('z');
                String cartId = params.get('ac');
                responseString = registerUser(empCode, username, password, email, confirmPassword, communityNickname, firstname, lastname, redirURL, redirPlanURL, preference, zipcode, cartId);
            }
        }
        catch(Exception e) {
            // Unexpected error
            responseString.put('success', false);
            responseString.put('exceptionMsg', e.getMessage());
            responseString.put('exceptiongetTypeName', e.getTypeName());
            responseString.put('exceptiongetLineNumber', e.getLineNumber());
            return JSON.serialize(responseString);
        }

        // No actions matched and no error occurred
        return JSON.serialize(responseString);
    }

    public Map<String, Object> ConvertExistingLead(List<Lead> existingLeads, Id PORTAL_ACCOUNT_ID, Map<String, Object> returnMap) {
        try {
            Lead existingLead = existingLeads[0];
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(existingLead.id);
            if(existingLead.Account__c != null){
                lc.setAccountId(existingLead.Account__c);
            } else {
                //get the PortalAccount Account Group Id
                Id agId = [SELECT Id FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount'].Id;
                //get the Customer Account Record Type
                Id rtype = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;

                Lead l = [Select FirstName, LastName FROM Lead WHERE Id = :existingLead.Id];

                //Create the Account for the Client, assign the PortalAccount Group
                User uOwner = [SELECT Id, Name, IsActive FROM USER Where Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
                a.OwnerId = uOwner.Id;
                a.Name = l.firstname + ' ' + l.lastname;
                a.ccrz__E_AccountGroup__c = agId;
                a.RecordTypeId = rtype;

                insert a;

                lc.setAccountId(a.Id);
            }

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(TRUE);

            Database.LeadConvertResult lcr = Database.convertLead(lc);

            if(lcr.isSuccess()){
                returnMap.put('lead_success', true);
                if(existingLead.Account__c != null){
                    PORTAL_ACCOUNT_ID = existingLead.Account__c;
                    returnMap.put('PORTAL_ACCOUNT_ID', PORTAL_ACCOUNT_ID);
                } else {
                    PORTAL_ACCOUNT_ID = a.Id;
                    returnMap.put('PORTAL_ACCOUNT_ID', PORTAL_ACCOUNT_ID);
                    returnMap.put('inside consumer account', 'inside');
                }
            }
            else {
                returnMap.put('lead_success', false);
            }
        }
        catch(Exception e) {
            returnMap.put('success', false);
            returnMap.put('excpetionIn', 'lead_conversion');
            returnMap.put('exceptionMsg', e.getMessage());
            returnMap.put('exceptiongetTypeName', e.getTypeName());
            returnMap.put('exceptiongetLineNumber', e.getLineNumber());
        }
        return returnMap;
    }

    public Map<String, Object> EmployeeRegistration(List<Account> ecomAccountList, Id PORTAL_ACCOUNT_ID, Map<String, Object> returnMap, String redirPlanURL, String patientType, String empCode) {
        //query for Account field hmr_Employer_Account_ID__c and match it with employer code
        //get the account record
        //assign the Portal Id var to Account id
        if(ecomAccountList.isEmpty()){
            returnMap.put('success', false);
            returnMap.put('InvalidEmpCode', empCode);
        }
        else{
            PORTAL_ACCOUNT_ID = ecomAccountList[0].id;
            returnMap.put('EmployerAssociation', empCode);
            patientType = 'Employer Group Member';
            returnMap.put('redirUrl', redirPlanURL);
            returnMap.put('PORTAL_ACCOUNT_ID', PORTAL_ACCOUNT_ID);
            returnMap.put('PatientType', patientType);
        }
        return returnMap;
    }

    public Map<String, Object> ZipCodeAssociation(List<Zip_Code_Assignment__c> zipCodeAssigment, Id PORTAL_ACCOUNT_ID, Map<String, Object> returnMap, String redirURL, String patientType, String zipcode) {

        if(!zipCodeAssigment.isEmpty()){
            PORTAL_ACCOUNT_ID = zipCodeAssigment[0].Account__c;
            returnMap.put('ClinicAssociation', zipCodeAssigment[0].Name);
            patientType = 'Clinic Patient';
            returnMap.put('redirUrl', redirURL);
            returnMap.put('PORTAL_ACCOUNT_ID', PORTAL_ACCOUNT_ID);
            returnMap.put('PatientType', patientType);
        }
        return returnMap;
    }

    public String userId {get;set;}
    public Map<String, Object> registerUser(String empCode, String username, String password, String email, String confirmPassword, String communityNickname, String firstname, String lastname, String redirURL, String redirPlanURL, String preference, String zipcode, String cartId) {
        Id PORTAL_ACCOUNT_ID;
        Map<String, Object> returnMap = new Map<String, Object>();
        String patientType = '';String redirectUrl = '';
        Integer lastNameSubLength =  lastname.length();
        boolean redirectToCheckout = false;
        if(lastNameSubLength >= 5){
            lastNameSubLength = 4;
        }
        Savepoint sp = Database.setSavepoint();

        //If an exisiting Lead has a matching email address, convert this lead into a contact
        List<Lead> existingLeads = new List<Lead>([SELECT Id, Account__c FROM Lead WHERE Email = :email LIMIT 1]);
        if(!existingLeads.isEmpty()){
            returnMap.putAll(ConvertExistingLead(existingLeads, PORTAL_ACCOUNT_ID, returnMap));
        }
        else {
            if(!String.isBlank(empCode)) {
                List<Account> ecomAccountList = [Select Id, hmr_Employer_Account_ID__c, name FROM Account WHERE hmr_Employer_Account_ID__c = :empCode LIMIT 1];
                returnMap.putAll(EmployeeRegistration(ecomAccountList, PORTAL_ACCOUNT_ID, returnMap, redirPlanURL, patientType, empCode));
            }
            else {
                if(!String.isBlank(zipcode)){
                    List<Zip_Code_Assignment__c> zipCodeAssigment = [SELECT ID, Name, Account__c FROM Zip_Code_Assignment__c WHERE Name = :zipcode AND Account__c <> null AND Account__r.hmr_Active__c = TRUE AND Account__r.hmr_Referral__c != 'DNR' LIMIT 1];
                    returnMap.putAll(ZipCodeAssociation(zipCodeAssigment, PORTAL_ACCOUNT_ID, returnMap, redirURL, patientType, zipcode));
                }

                System.debug('Return Map  ->  ' + returnMap);

                if(returnMap.get('PatientType') == null) {

                    ccrz__E_AccountGroup__c accountGroup = [SELECT ID, Name FROM ccrz__E_AccountGroup__c WHERE Name = 'PortalAccount' LIMIT 1];
                    System.debug('Acc group  ->  ' + accountGroup );
                    returnMap.put('AccountGroup', accountGroup.Name);
                    User uOwner = [SELECT Id, Name, IsActive FROM USER Where Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
                    returnMap.put('AccountOwner', uOwner.Name);
                    Account a = new Account();
                    a.Name = firstname + ' ' + lastname;
                    a.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'HMR_Customer_Account'].Id;
                    a.ccrz__E_AccountGroup__c = accountGroup.id;
                    a.OwnerId = uOwner.Id;
                    insert a;
                    returnMap.put('AccoutName', a.Name);
                    PORTAL_ACCOUNT_ID = a.Id;
                }
            }
        }
        System.debug('Portal Id   -> ' + returnMap.get('PORTAL_ACCOUNT_ID'));

        if(returnMap.get('PORTAL_ACCOUNT_ID') != null) {
            PORTAL_ACCOUNT_ID = (Id)returnMap.get('PORTAL_ACCOUNT_ID');
        }
        User u = new User();
        u.FirstName = firstname;
        u.LastName = lastname;
        u.Username = username;
        u.Email = email;
        u.TimeZoneSidKey = 'America/New_York';
        string nickName = email.split('@').get(0);
        if(nickName.length()>34){
            nickName = nickName.substring(0,34);
        }
        nickName = nickName+'_'+String.valueOf(Math.random()).substring(2,7);
        u.CommunityNickname = nickName;
        // u.Alias = communityNickname.substring(0, communityNickname.length());
        u.Alias = (firstname.substring(0, 1) + lastname.substring(0, lastNameSubLength)).toLowerCase();



        try {
            returnMap.put('beforeUserCreation', userId);
            returnMap.put('UserInfo', u);
            if(userId == null)
                userId = Site.createExternalUser(u, PORTAL_ACCOUNT_ID, password);
            returnMap.put('afterUserCreation', userId);
        }
        catch(Exception ex) {
            returnMap.put('success', false);
            returnMap.put('excpetionIn', 'ExceptionCreateUser');
            if(returnMap.get('InvalidEmpCode') == empCode){
                returnMap.put('exceptionMsg', 'The Employer Code you have entered is invalid');
            }
            else{
                returnMap.put('exceptionMsg', ex.getMessage());
            }
            returnMap.put('exceptiongetTypeName', ex.getTypeName());
            returnMap.put('exceptiongetLineNumber', ex.getLineNumber());
            Database.rollback(sp);
        }
        returnMap.put('outSideUserIdLogic', userId);

        if (userId != null) {
            returnMap.put('insideUserIdLogic', userId);
            // transfer guest user cart to register user
            string activeCartId = '';
            boolean transferedCart = false;


            if(!String.isBlank(cartId))
                activeCartId = cartId;
            else if(ApexPages.currentPage().getParameters().get('cartID') != null)
                activeCartId = ApexPages.currentPage().getParameters().get('cartID');
            else if(ApexPages.currentPage().getCookies().get('currCartId') != null)
                activeCartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();

            if(!String.isBlank(activeCartId)) {
                if(!String.isBlank(activeCartId)){
                    List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: username];
                    ccrz.cc_RemoteActionResult cartTransferResult = HMR_CC_CartTransfer_Extension.transferCartToUser(activeCartId, userRecordList[0].Id);
                    //If Cart was Transfered/Take to Cart, must pass in redirect here to login, not directly to response string or it won't login
                    if(cartTransferResult.success){
                        transferedCart = true;
                        redirectToCheckout = redirURL == 'checkout';
                        redirUrl = Site.getBaseUrl() + String.format('/ccrz__CheckoutNew?cartID={0}&isCSRFlow=true&portalUser=&store=&cclcl=en_US', new String[]{String.valueOf(cartTransferResult.data)});
                    }
                }
            }
            else if(redirUrl == 'checkout')
                redirUrl = redirPlanURL;

            DateTime dt = DateTime.now();

            //update Contact record with entered email preference and zip code
            Id contactId = [SELECT Id, ContactId FROM User WHERE Id = :userId].ContactId;
            Contact c = [SELECT Id, HMR_Email_Preference__c, MailingPostalCode, Patient_Type__c, HasOptedOutOfEmail FROM Contact WHERE Id =:contactId];
            Boolean preference_b = Boolean.valueOf(preference);
            c.HMR_Email_Preference__c = preference_b;
            c.HasOptedOutOfEmail = !preference_b;
            c.hmr_Contact_Original_Source__c = 'Website';
            c.hmr_Contact_Original_Source_Detail__c = 'Self-Registration';
            if(!String.isBlank(zipcode)){
                c.MailingPostalCode = zipcode;
            }
            if(returnMap.get('PatientType') != null){
                c.Patient_Type__c = String.valueOf(returnMap.get('PatientType'));
            }

            //TODO: Consent Agreement Stuffs
            returnMap.put('userId', userId);
            try {
                update c;
            }
            catch(Exception ex) {
                returnMap.put('success', false);
                returnMap.put('exceptionIn', 'UpdateContact');
                returnMap.put('exceptionMsg', ex.getMessage());
                returnMap.put('exceptiongetTypeName', ex.getTypeName());
                returnMap.put('exceptiongetLineNumber', ex.getLineNumber());
                Database.rollback(sp);
            }

            //    //Create Terms & Conditions + Privacy policy consent agreements
            //Id ccTerm = [Select Id, Name, ccrz__Title__c, ccrz__Description__c FROM ccrz__E_Term__c WHERE Consent_Type__c = 'HMR Terms & Conditions' AND ccrz__Enabled__c = True LIMIT 1].Id;
            ////Privacy Policy
            //Id ccPolicy = [Select Id, Name, ccrz__Title__c, ccrz__Description__c FROM ccrz__E_Term__c WHERE Consent_Type__c = 'Privacy/Tracking' AND ccrz__Enabled__c = True LIMIT 1].Id;

            //List<Consent_Agreement__c> consents = new List<Consent_Agreement__c>();

            //Consent_Agreement__c terms = new Consent_Agreement__c(
            //    Version__c = ccTerm,
            //    Agreement_Timestamp__c = dt,
            //    Contact__c = contactId,
            //    Status__c = 'Consented'
            //);
            //consents.add(terms);

            //Consent_Agreement__c policy = new Consent_Agreement__c(
            //    Version__c = ccPolicy,
            //    Agreement_Timestamp__c = dt,
            //    Contact__c = contactId,
            //    Status__c = 'Consented'
            //);
            //consents.add(policy);

            //try {
            //    insert consents;
            //}
            //catch(Exception ex) {
            //    returnMap.put('excpetionIn', 'InsertConsent');
            //    returnMap.put('exceptionMsg', ex.getMessage());
            //    returnMap.put('exceptiongetTypeName', ex.getTypeName());
            //    returnMap.put('exceptiongetLineNumber', ex.getLineNumber());
            //}
            if(!redirectToCheckout && returnMap.get('redirUrl') != null && !String.isBlank((String)returnMap.get('redirUrl'))) {
                redirUrl = (String)returnMap.get('redirUrl');
            }

            //HPRP-3789: When creating a new account, we want to bring them to the MyAccount page if they would have been brought back home
            if(!String.isBlank(redirUrl)){
                if(redirUrl.containsIgnoreCase('/Home')){
                    //can't convert to lower case incase case-sensitive id is used to replace both variations
                    redirUrl = redirUrl.replace('/home', '/ccrz__MyAccount');
                    redirUrl = redirUrl.replace('/Home', '/ccrz__MyAccount');
                }
                else if(redirUrl.containsIgnoreCase('/Welcome')){
                    redirUrl = redirUrl.toLowerCase().replace('/welcome', '/ccrz__MyAccount');
                    redirUrl = redirUrl.toLowerCase().replace('/Welcome', '/ccrz__MyAccount');
                    //we are seeing strange behavior differences between environments, in QA2 an extra defaultstore is added
                    redirUrl = redirUrl.replace('/defaultstore','');
                }
                else if(redirUrl.endsWith('DefaultStore/'))
                    redirUrl = redirUrl + 'ccrz__MyAccount';
            }

            assingPermissionSet(userId);
            try{
                PageReference loginPageRedirect = Site.login(username, password, redirUrl);
                if(loginPageRedirect != null) {
                    returnMap.put('success', true);
                    returnMap.put('redirect', redirUrl);
                    returnMap.put('redirectUrl', loginPageRedirect.getUrl());
                }
                else {
                    returnMap.put('success', false);
                    returnMap.put('failure', 'login failed');
                }
            }
            catch(Exception ex){
                returnMap.put('success', false);
                returnMap.put('excpetionIn', 'SiteLogin');
                returnMap.put('exceptionMsg', ex.getMessage());
                returnMap.put('exceptiongetTypeName', ex.getTypeName());
                returnMap.put('exceptiongetLineNumber', ex.getLineNumber());
            }
        }
        if(returnMap.get('exceptionMsg') != null && returnMap.get('exceptionMsg') != '[User already exists.]'){
            trimResponseMessage(returnMap);
        }
        return returnMap;
    }

    public void trimResponseMessage(Map<String, Object> rMap){
        string ss = string.valueOf(rMap.get('exceptionMsg'));
        ss = ss.replace('[','');
        ss = ss.replace(']','');
        rMap.put('exceptionMsg', ss);
    }


     // method to transfer guest user cart to register or login user
     //Checks if there is a cart within the cookies and if so transfers it to the user upon login
    public static ccrz.cc_RemoteActionResult transferGuestCart(String username){
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.success = false;

        if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
             String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
             List<User> userRecordList = [SELECT Id FROM User WHERE UserName =: username];

             if(!String.isBlank(cartId) && userRecordList.size() > 0)
                result = HMR_CC_CartTransfer_Extension.transferCartToUser(cartId, userRecordList[0].Id);
        }

        return result;
    }


    @future
    public static void assingPermissionSet(Id userIdp) {
        PermissionSetAssignment psa = new PermissionSetAssignment();
        Id permSet = [SELECT Id FROM PermissionSet WHERE Name = 'ocms_autogen_SiteViewer'].Id;
        psa.PermissionSetId = permSet;
        psa.AssigneeId = userIdp;
        try {
            insert psa;
        }
        catch(Exception ex) {
            System.debug('excpetionIn AssignPermissionSet');
            System.debug('exceptionMsg' + ex.getMessage());
            System.debug('exceptiongetTypeName' + ex.getTypeName());
            System.debug('exceptiongetLineNumber' + ex.getLineNumber());
        }
    }

    /**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_RegistrationServiceInterface.class;
    }
}