@isTest
private class ReturnAuthorizationPdfControllerTest {

	@isTest static void test_method_one() {

		//create an Account
        Account testAccountObj = new Account(Name = 'Test Account');
        insert testAccountObj;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();

        Contact testContactObj1 = new Contact(FirstName='Test', LastName = 'TestHSS', Account = testAccountObj, Phone='312-123-4567');
        conList.add(testContactObj1);
        Contact testContactObj2 = new Contact(FirstName='Test', LastName = 'TestHS', Account = testAccountObj);
        conList.add(testContactObj2);
        Contact testContactObj3 = new Contact(FirstName='Test', LastName = 'Test2', Account = testAccountObj);
        conList.add(testContactObj3);
        Contact testContactObj4 = new Contact(FirstName='Test', LastName = 'Misc', Account = testAccountObj);
        conList.add(testContactObj4);
        insert conList;

        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();

        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj1);
        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj2);
        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 14);
        prgList.add(testProgramObj3);
        insert prgList;

        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();

        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj1);
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'P1 HSS Kit', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
        prdList.add(testProductObj3);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj2.Id);
        prdList.add(testProductObj4);
        ccrz__E_Product__c testProductObj5 = new ccrz__E_Product__c(Name= 'P2 Kit', ccrz__SKU__c = '555555', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj3.Id);
        prdList.add(testProductObj5);
        ccrz__E_Product__c testProductObj6 = new ccrz__E_Product__c(Name= 'Chocolate Shake', ccrz__SKU__c = '777777', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj6);
        insert prdList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();

        ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj1);
        ccrz__E_Order__c testOrderObj2 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj2);
        ccrz__E_Order__c testOrderObj3 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj3);
        ccrz__E_Order__c testOrderObj4 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj2.Id, ccrz__BuyerPhone__c='312-123-4567', ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj4);
        ccrz__E_Order__c testOrderObj5 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj3.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
        orderList.add(testOrderObj5);
        insert orderList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();

        ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 10.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj1);
        ccrz__E_OrderItem__c testItemObj2 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj2.Id, ccrz__Price__c = 20.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 20.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj2);
        ccrz__E_OrderItem__c testItemObj3 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj3.Id, ccrz__Price__c = 100.00, ccrz__Product__c = testProductObj3.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 100.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj3);
        ccrz__E_OrderItem__c testItemObj4 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 150.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj4);
        ccrz__E_OrderItem__c testItemObj5 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 200.00, ccrz__Product__c = testProductObj5.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 200.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj5);
        ccrz__E_OrderItem__c testItemObj6 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj6);
        ccrz__E_OrderItem__c testItemObj7 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj7);
        ccrz__E_OrderItem__c testItemObj8 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj8);
        ccrz__E_OrderItem__c testItemObj9 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj9);
         ccrz__E_OrderItem__c testItemObj10 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj6.Id, ccrz__Quantity__c = 3, ccrz__SubAmount__c= 6.00, ccrz__OrderItemStatus__c = 'Order Submitted');
        orderItemList.add(testItemObj10);
        insert orderItemList;

		Refund__c refund = new Refund__c();
		refund.hmr_Contact__c = testContactObj1.Id;
		refund.hmr_Order_Number__c = testOrderObj3.Id;
		refund.hmr_Return_Status__c = 'Approved';
		refund.hmr_Reason_Code__c = 'Other';
		refund.hmr_Refund_Type__c = 'Return';
		insert refund;

		Refund_Item__c rItem = new Refund_Item__c();
		rItem.hmr_Refund__c = refund.Id;
		rItem.hmr_CC_Order__c = testOrderObj3.Id;
		rItem.CC_Order_Item__c = testItemObj3.Id;
		rItem.Price_paid_by_Client__c = 100.00;
		rItem.hmr_Refund_Product_Quantity__c = 1;
		rItem.hmr_Refund_Product_SKU__c = testProductObj3.ccrz__SKU__c;
		insert rItem;

		test.StartTest();

			ApexPages.StandardController sc = new ApexPages.StandardController(refund);

			ReturnAuthorizationPdfController controller = new ReturnAuthorizationPdfController(sc);

			PageReference tpageRef = Page.ReturnAuthorizationPdf;
			Test.setCurrentPage(tpageRef);

			ApexPages.currentPage().getParameters().put('Id', refund.Id);

			System.assertEquals( refund.Id,ApexPages.currentPage().getParameters().get('Id'));

		test.StopTest();


	}
	@isTest static void test_method_two() {

		//create an Account
		Account testAccountObj = new Account(Name = 'Test Account');
		insert testAccountObj;

		//create Contacts associated to the Account created and insert them
		List<Contact> conList = new List<Contact>();

		Contact testContactObj1 = new Contact(FirstName='Test', LastName = 'TestHSS', Account = testAccountObj, Phone='312-123-4567');
		conList.add(testContactObj1);
		Contact testContactObj2 = new Contact(FirstName='Test', LastName = 'TestHS', Account = testAccountObj);
		conList.add(testContactObj2);
		Contact testContactObj3 = new Contact(FirstName='Test', LastName = 'Test2', Account = testAccountObj);
		conList.add(testContactObj3);
		Contact testContactObj4 = new Contact(FirstName='Test', LastName = 'Misc', Account = testAccountObj);
		conList.add(testContactObj4);
		insert conList;

		//create the different kinds of available Programs and insert them
		List<Program__c> prgList = new List<Program__c>();

		Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14);
		prgList.add(testProgramObj1);
		Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 14);
		prgList.add(testProgramObj2);
		Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 14);
		prgList.add(testProgramObj3);
		insert prgList;

		//create different kinds of Products, with & without kits, and insert them
		List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();

		ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
		prdList.add(testProductObj1);
		ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
		prdList.add(testProductObj2);
		ccrz__E_Product__c testProductObj3 = new ccrz__E_Product__c(Name= 'P1 HSS Kit', ccrz__SKU__c = '333333', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj1.Id);
		prdList.add(testProductObj3);
		ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj2.Id);
		prdList.add(testProductObj4);
		ccrz__E_Product__c testProductObj5 = new ccrz__E_Product__c(Name= 'P2 Kit', ccrz__SKU__c = '555555', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj3.Id);
		prdList.add(testProductObj5);
		ccrz__E_Product__c testProductObj6 = new ccrz__E_Product__c(Name= 'Chocolate Shake', ccrz__SKU__c = '777777', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
		prdList.add(testProductObj6);
		insert prdList;

		//insert the Orders with status as Submitted
		List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();

		ccrz__E_Order__c testOrderObj1 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
		orderList.add(testOrderObj1);
		ccrz__E_Order__c testOrderObj2 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj4.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
		orderList.add(testOrderObj2);
		ccrz__E_Order__c testOrderObj3 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj1.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
		orderList.add(testOrderObj3);
		ccrz__E_Order__c testOrderObj4 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj2.Id, ccrz__BuyerPhone__c='312-123-4567', ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
		orderList.add(testOrderObj4);
		ccrz__E_Order__c testOrderObj5 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj3.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderStatus__c = 'Order Submitted');
		orderList.add(testOrderObj5);
		insert orderList;

		//insert the corresponding Order items for the inserted Orders
		List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();

		ccrz__E_OrderItem__c testItemObj1 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj1.Id, ccrz__Price__c = 10.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 10.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj1);
		ccrz__E_OrderItem__c testItemObj2 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj2.Id, ccrz__Price__c = 20.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 20.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj2);
		ccrz__E_OrderItem__c testItemObj3 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj3.Id, ccrz__Price__c = 100.00, ccrz__Product__c = testProductObj3.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 100.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj3);
		ccrz__E_OrderItem__c testItemObj4 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 150.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj4);
		ccrz__E_OrderItem__c testItemObj5 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 200.00, ccrz__Product__c = testProductObj5.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c= 200.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj5);
		ccrz__E_OrderItem__c testItemObj6 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj6);
		ccrz__E_OrderItem__c testItemObj7 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj7);
		ccrz__E_OrderItem__c testItemObj8 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj8);
		ccrz__E_OrderItem__c testItemObj9 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__SubAmount__c= 42.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj9);
		 ccrz__E_OrderItem__c testItemObj10 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj5.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj6.Id, ccrz__Quantity__c = 3, ccrz__SubAmount__c= 6.00, ccrz__OrderItemStatus__c = 'Order Submitted');
		orderItemList.add(testItemObj10);
		insert orderItemList;

		Refund__c refund2 = new Refund__c();
		refund2.hmr_Contact__c = testContactObj2.Id;
		refund2.hmr_Order_Number__c = testOrderObj3.Id;
		refund2.hmr_Return_Status__c = 'Approved';
		refund2.hmr_Reason_Code__c = 'Other';
		refund2.hmr_Refund_Type__c = 'Return';
		insert refund2;

		Refund_Item__c rItem2 = new Refund_Item__c();
		rItem2.hmr_Refund__c = refund2.Id;
		rItem2.hmr_CC_Order__c = testOrderObj4.Id;
		rItem2.CC_Order_Item__c = testItemObj4.Id;
		rItem2.Price_paid_by_Client__c = 100.00;
		rItem2.hmr_Refund_Product_Quantity__c = 1;
		rItem2.hmr_Refund_Product_SKU__c = testProductObj4.ccrz__SKU__c;
		insert rItem2;

		test.StartTest();

			ApexPages.StandardController sc = new ApexPages.StandardController(refund2);

			ReturnAuthorizationPdfController controller = new ReturnAuthorizationPdfController(sc);

			PageReference tpageRef = Page.ReturnAuthorizationPdf;
			Test.setCurrentPage(tpageRef);

			ApexPages.currentPage().getParameters().put('Id', refund2.Id);

			System.assertEquals( refund2.Id,ApexPages.currentPage().getParameters().get('Id'));

		test.StopTest();

	}
}