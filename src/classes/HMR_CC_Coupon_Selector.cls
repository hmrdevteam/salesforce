/**
* Apex Selector Class for CC_Order
* Sharing Should be Determined by Calling Class, Purposely Excluded Here
* @Date: 2017-05-25
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-05-25
* @JIRA:
*/
public class HMR_CC_Coupon_Selector {
	public static List<ccrz__E_Coupon__c> getCouponListByHMRCouponIdAndProgramIdAndOrderType(string hmrCouponId, string programId, string orderType){
		//The picklist values are not in-sync between the objects so adjust here to reconcile
		if(orderType == 'P1 Reorder')
			orderType = 'P1 Re-Order';
		if(orderType == 'HSS Reorder' || orderType == 'P1-HSS Reorder')
			orderType = 'HSS Re-Order';
		if(orderType == 'P2 1st Order')
			orderType = 'P2';
		if(orderType == 'P2 Reorder')
			orderType = 'P2 Re-Order';

		List<ccrz__E_Coupon__c> couponList = [SELECT ccrz__CouponType__c,
													 ccrz__DiscountAmount__c,
													 ccrz__DiscountType__c,
													 ccrz__CartTotalAmount__c
									          FROM ccrz__E_Coupon__c
									          WHERE HMR_Coupon__c =: hmrCouponId 
									          		AND hmr_Program__c =: programId
									        	  	AND Coupon_Order_Type__c =: orderType
									        	  	AND ccrz__Enabled__c = true];

	   return couponList;
	}

	public static List<ccrz__E_Coupon__c> getByHMRCouponIdAndOrderType(string hmrCouponId, string orderType){
		List<ccrz__E_Coupon__c> couponList = [SELECT ccrz__CouponName__c, 
                            						 ccrz__CouponCode__c, 
                            						 HMR_Coupon__c 
                             				  FROM ccrz__E_Coupon__c
                             				  WHERE HMR_Coupon__c =: hmrCouponId 
                             					AND Coupon_Order_Type__c =: orderType
                             					AND ccrz__Enabled__c = true];
		return couponList;
	}
}