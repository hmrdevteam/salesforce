/**
* CMS CloudCraze Context Service Interface that creates all the necessary context when the user logs in
*
* @Date: 2017-04-05
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

global class HMR_CMS_CCContextServiceInterface implements cms.ServiceInterface{

    global static Boolean isGuestUser { get; set; }

    public HMR_CMS_CCContextServiceInterface() {
        isGuest();
    }

    public HMR_CMS_CCContextServiceInterface(HMR_CC_Reorder_Controller controller) {
        isGuest();
    }

    public HMR_CMS_CCContextServiceInterface(HMR_ZipCodeFinder_Page_Controller controller) {
        isGuest();
    }

    global static void isGuest() {
        isGuestUser = true;
        String userId = UserInfo.getUserId();
        String userProfileId = UserInfo.getProfileId();
        Id guestProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile'].Id;


        if(userProfileId != guestProfileId) {
            isGuestUser = false;
        }
    }

    global static Integer getCartItemCount(Map<String, Object> outputCartData) {
        Integer cartCount = 0;
        Map<String, Object> outputData = new Map<String, Object>();
        Set<String> cartKeys = new Set<String>();
        String exCartString = '';
        String encryptedCartId = '';
        //Map<String, Object> outputCartData  = ccrz.ccAPICart.fetch(inputData);
        if (outputCartData.get(ccrz.ccAPICart.CART_OBJLIST) != null) {
            // The cast to List<Map<String, Object>> is necessary...
            List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputCartData.get(ccrz.ccAPICart.CART_OBJLIST);
            if(outputCartList.size() > 0) {
                outputData = outputCartList[0];
                cartKeys = outputCartList[0].keySet();
                encryptedCartId =  (String)outputData.get('encryptedId');
                List<Map<String, Object>> cartItems = (List<Map<String, Object>>)outputData.get('ECartItemsS');
                if(cartItems != null) {
                    for(Integer i = 0; i < cartItems.size(); i++) {
                        if(cartItems[i].get('cartItemType') == 'Major') {
                            cartCount++;
                            if(cartItems[i].get('productType') == 'Product') {
                                cartCount--;
                                cartCount += (Integer)cartItems[i].get('quantity');
                            }
                        }
                    }
                }
            }
        }
        return cartCount;
    }

    global static Double getCartTotalAmount(Map<String, Object> outputCartData) {
        Double cartTotalAmount = 0;
        if (outputCartData.get(ccrz.ccAPICart.CART_OBJLIST) != null) {
            // The cast to List<Map<String, Object>> is necessary...
            List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputCartData.get(ccrz.ccAPICart.CART_OBJLIST);
            if(outputCartList.size() > 0) {
                if(outputCartList[0].get('totalAmount') != null) {
                    cartTotalAmount = (Double)outputCartList[0].get('totalAmount');
                }
            }
        }
        return cartTotalAmount;
    }

    global static Map<String, Object> getCartServiceInterfaceCall(Map<String, Object> inputData) {
        String userId = UserInfo.getUserId();
        return getCartServiceInterfaceCallByUserId(inputData, userId);
    }

    global static Map<String, Object> getCartServiceInterfaceCallByUserId(Map<String, Object> inputData, String userId) {

        // String sitePrefix = Site.getBaseUrl();
        //String userId = UserInfo.getUserId();
        // String userProfileId = UserInfo.getProfileId();
        // Id guestProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile'].Id;

        //check if there is parameters for Cart Id, if there is, directly use this CartID
        system.debug('0*&^^^^^^'+inputData.get(ccrz.ccApiCart.CART_ENCID) );
        if(inputData.get(ccrz.ccApiCart.CART_ENCID) == null){
            if(isGuestUser) {
                if(ApexPages.currentPage().getCookies().get('currCartId') != null) {
                    String cartId = ApexPages.currentPage().getCookies().get('currCartId').getValue();
                    if(cartId != null) {
                        inputData.put(ccrz.ccApiCart.CART_ENCID, cartId);
                    }
                    else {
                        inputData.put(ccrz.ccApiCart.BYOWNER, userId);//'0055C000000NCnc',
                    }
                }
            }
            else {
                if(inputData.get(ccrz.ccApiCart.BYOWNER) == null) {
                    inputData.put(ccrz.ccApiCart.BYOWNER, userId);//'0055C000000NCnc',
                }
            }
        }
        system.debug('1*&^^^^^^'+inputData);
        return ccrz.ccAPICart.fetch(inputData);
    }


    /**
     * executeRequest --> method to execute request
     * @param params a map of parameters including
     * @return a JSON-serialized response string
     */
    global static String executeRequest(Map<String, String> params) {
        for(string s: params.keySet()){
            system.debug('2*&^^^^^^'+s+'----'+params.get(s));
        }
        Map<String, Object> responseString = new Map<String, Object>();
        Integer cartCount = 0;
        Double cartTotalAmount = 0;
        Map<String, Object> outputCartData = new Map<String, Object>();
        String userId;
        if(isGuestUser == null)
            isGuest();
        if(params.get('activeCartId') != null && params.get('activeCartId') != '') {
            responseString.put('cartIDURLParam', params.get('activeCartId'));
        }
        if(isGuestUser && (ApexPages.currentPage() != null && ApexPages.currentPage().getCookies().get('currCartId') == null)) {
            cartCount = 0;
            cartTotalAmount = 0;
            responseString.put('outputData', outputCartData);
        }
        else {
            Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccApiCart.CARTSTATUS => 'Open',
                ccrz.ccApiCart.ACTIVECART => true,
                ccrz.ccAPI.API_VERSION => 3,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                    }
                }
            };

            if(params.get('activeCartId') != null && !String.isBlank((String)params.get('activeCartId'))) {
                inputData.put(ccrz.ccApiCart.CART_ENCID, params.get('activeCartId'));
            }

            if(params.get('userId') != null && !String.isBlank((String)params.get('userId'))) {
                inputData.put(ccrz.ccApiCart.BYOWNER, params.get('userId'));
            }
            else if(params.get('portalUser') != null && !String.isBlank((String)params.get('portalUser'))){
                userId = params.get('portalUser');
                inputData.put(ccrz.ccApiCart.BYOWNER, params.get('portalUser'));
            }
            responseString.put('userId', params.get('userId'));
            responseString.put('portalUser', params.get('portalUser'));
            try {

                outputCartData = String.isBlank(userId) ? getCartServiceInterfaceCall(inputData) : getCartServiceInterfaceCallByUserId(inputData, userId);

                cartCount = getCartItemCount(outputCartData);
                cartTotalAmount = getCartTotalAmount(outputCartData);
                responseString.put('isGuest', isGuestUser);
                responseString.put('outputData', outputCartData);
                //responseString.put('inputData', inputData);
                responseString.put('success', true);
            }
            catch(Exception e) {
                cartCount = -1;
                cartTotalAmount = -1;
                responseString.put('success', false);
                responseString.put('exceptionMsg', e.getMessage());
                responseString.put('exceptiongetTypeName', e.getTypeName());
                responseString.put('exceptiongetLineNumber', e.getLineNumber());
                return JSON.serialize(responseString);
            }
        }
        responseString.put('cartCount', cartCount);
        responseString.put('cartTotalAmount', cartTotalAmount);
        // No actions matched and no error occurred
        return JSON.serialize(responseString);
    }

    @RemoteAction
    global static String getCartCountRemoteActionCall(Map<String, String> params) {
        return executeRequest(params);
    }


    /**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_CCContextServiceInterface.class;
    }
}