/**
* Apex Rest Resource for Menu (Hamburger)
* @Date: 2018-01-17
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-03-28 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/menu/*')
global with sharing class HMRMobMenu {
    final static Map<String, String> consentTypeMap = new Map<String, String>{'Privacy/Tracking' => 'privacy', 'HMR Terms & Conditions' => 'termsOfUse'};
    /**
     * method to get the menu selections for the mobile application
     * @return List<MobMenuDTO> - the list of DTO containing the menu selections (flat structure) (HTTP 200 OK)
     * @return HTTP/1.1 404 not found if the menu items are unavailable
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        List<MobMenuDTO> menuSettingDTOList = new List<MobMenuDTO>(); 
        RestRequest request = RestContext.request;
        String communityUserId = RestContext.request.params.get('communityUserId');

        if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
        }

        //check if user is enrolled in class
        List<Class_Member__c> classMemberList = [SELECT Id FROM Class_Member__c WHERE hmr_Contact_Name__r.Community_User_ID__c =: communityUserId AND hmr_Active__c = true];
        boolean isEnrolled = classMemberList.size() > 0;
        
        for(Mobile_Menu_Setting__mdt menuSetting : [SELECT MasterLabel, Icon_Url__c, Url_Name__c, Requires_Authentication__c, Sequence__c FROM Mobile_Menu_Setting__mdt ORDER BY Sequence__c]){
            menuSettingDTOList.add(new MobMenuDTO(menuSetting, isEnrolled));
        }

        for(ccrz__E_Term__c termRecord : [SELECT Consent_Type__c, ccrz__Description__c, ccrz__Title__c FROM ccrz__E_Term__c WHERE Consent_Type__c IN: consentTypeMap.keySet()]){
            returnMap.put(consentTypeMap.get(termRecord.Consent_Type__c), new MobConsentDTO(termRecord));
        }

        if(menuSettingDTOList.size() > 0){
            RestContext.response.statuscode = 200;

            returnMap.put('success', true);
            returnMap.put('menuItems', menuSettingDTOList);
        }
        else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
        }

        response.responseBody = Blob.valueOf(JSON.serialize(returnMap)); 
    }

    /**
     * global DTO class that represents a flat structure of the menu records
     */
    global class MobMenuDTO {
        public String label {get; set;}
        public String iconUrl {get; set;}
        public String pageUrl {get; set;}
        public Boolean requiresAuthentication {get; set;}
        
        public MobMenuDTO(Mobile_Menu_Setting__mdt menuSettingRecord, boolean hasCoaching){
            this.label = menuSettingRecord.MasterLabel;
            this.iconUrl = menuSettingRecord.Icon_Url__c;
            this.pageUrl = menuSettingRecord.Url_Name__c;
            this.requiresAuthentication = menuSettingRecord.Requires_Authentication__c;

            if(hasCoaching && menuSettingRecord.MasterLabel == 'Coaching'){
                this.pageUrl = 'group-data';
                this.requiresAuthentication = true;
            }
        }
    }

    /**
     * global DTO class that represents a flat structure of the consent records
     */
    global class MobConsentDTO {
        public String title {get; private set;}
        public String html {get; private set;}

        public MobConsentDTO(ccrz__E_Term__c termRecord){
            this.title = termRecord.ccrz__Title__c;
            this.html = termRecord.ccrz__Description__c;
        }
    }
}