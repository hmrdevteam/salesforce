/**
* Test Class Coverage of the HMR_Knowledge_Service
*
* @Date: 08/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA:
*/
@isTest
private class HMR_Knowledge_Service_Test {
	  @testSetup
    private static void setTestData(){
        Recipe__kav leftRightRecipeArticle01 = new Recipe__kav(Title='Test LeftRight Recipe 01'
                                                              ,UrlName='test-leftright-recipe-01'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav leftRightRecipeArticle02 = new Recipe__kav(Title='Test LeftRight Recipe 02'
                                                              ,UrlName='test-leftright-recipe-02'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav leftRightRecipeArticle03 = new Recipe__kav(Title='Test LeftRight Recipe 03'
                                                              ,UrlName='test-leftright-recipe-03'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'LeftRight');
        Recipe__kav topBottomRecipeArticle01 = new Recipe__kav(Title='Test TopBottom Recipe 01'
                                                              ,UrlName='test-topbottom-recipe-01'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle02 = new Recipe__kav(Title='Test TopBottom Recipe 02'
                                                              ,UrlName='test-topbottom-recipe-02'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle03 = new Recipe__kav(Title='Test TopBottom Recipe 03'
                                                              ,UrlName='test-topbottom-recipe-03'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle04 = new Recipe__kav(Title='Test TopBottom Recipe 04'
                                                              ,UrlName='test-topbottom-recipe-04'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Recipe__kav topBottomRecipeArticle05 = new Recipe__kav(Title='Test TopBottom Recipe 05'
                                                              ,UrlName='test-topbottom-recipe-05'
                                                              ,Language='en_US'
                                                              ,Template_Layout__c = 'TopBottom');
        Blog_Post__kav blogArticle01 = new Blog_Post__kav(Title='Test Blog 01'
                                                         ,UrlName='test-blog-01'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 1'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'LeftRight');
        Blog_Post__kav blogArticle02 = new Blog_Post__kav(Title='Test Blog 02'
                                                         ,UrlName='test-blog-02'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 2'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'TopBottom');
        Blog_Post__kav blogArticle03 = new Blog_Post__kav(Title='Test Blog 03'
                                                         ,UrlName='test-blog-03'
                                                         ,Language='en_US'
                                                         ,Caption_1__c = 'Test Caption 3'
                                                         ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
                                                         ,Image_1__c = 'http://www.hmrprogram.com'
                                                         ,Card_Layout__c = 'TopBottom');
        FAQ__kav faqArticle01 = new FAQ__kav(Title='Test FAQ 01'
                                            ,UrlName='test-faq-01'
                                            ,Language='en_US'
                                            ,File_Download_URL__c = 'http://www.hmrprogram.com'
                                            ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
                                            ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com');
        Success_Story__kav successStoryArticle01 = new Success_Story__kav(Title='Test Success Story 01'
							                                             ,UrlName='test-success-story-01'
							                                             ,Language='en_US'
							                                             ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
							                                             ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com');

        List<Recipe__kav> recipeArticleList = new List<Recipe__kav>{leftRightRecipeArticle01
                                                                   ,leftRightRecipeArticle02
                                                                   ,leftRightRecipeArticle03
                                                                   ,topBottomRecipeArticle01
                                                                   ,topBottomRecipeArticle02
                                                                   ,topBottomRecipeArticle03
                                                                   ,topBottomRecipeArticle04
                                                                   ,topBottomRecipeArticle05};
        insert recipeArticleList;

        List<Recipe__kav> recipeArticleToPublishList = [SELECT KnowledgeArticleId FROM Recipe__kav WHERE Id IN: recipeArticleList];
        for(Recipe__kav recipeRecord : recipeArticleToPublishList)
            KbManagement.PublishingService.publishArticle(recipeRecord.KnowledgeArticleId, true);

        List<Blog_Post__kav> blogArticleList = new List<Blog_Post__kav>{blogArticle01
                                                                       ,blogArticle02
                                                                       ,blogArticle03};
        insert blogArticleList;

        List<Blog_Post__kav> blogArticleToPublishList = [SELECT KnowledgeArticleId FROM Blog_Post__kav WHERE Id IN: blogArticleList];
        for(Blog_Post__kav blogRecord : blogArticleToPublishList)
            KbManagement.PublishingService.publishArticle(blogRecord.KnowledgeArticleId, true);

        insert faqArticle01;

        List<FAQ__kav> faqArticleToPublishList = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id =: faqArticle01.Id];
        for(FAQ__kav faqRecord : faqArticleToPublishList)
            KbManagement.PublishingService.publishArticle(faqRecord.KnowledgeArticleId, true);

        insert successStoryArticle01;

        List<Success_Story__kav> successStoryArticleToPublishList = [SELECT KnowledgeArticleId FROM Success_Story__kav WHERE Id =: successStoryArticle01.Id];

        for(Success_Story__kav successStoryRecord : successStoryArticleToPublishList)
            KbManagement.PublishingService.publishArticle(successStoryRecord.KnowledgeArticleId, true);

        //Assign Data Categories
        DescribeDataCategoryGroupResult[] dataCategoryGroupList = Schema.describeDataCategoryGroups(new String[] { 'KnowledgeArticleVersion'});

        Recipe__DataCategorySelection recipeDataCategorySelection = new Recipe__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	         ,DataCategoryName = 'Getting_Started'
        																			     	         ,ParentId = leftRightRecipeArticle01.Id);


        Blog_Post__DataCategorySelection blogDataCategorySelection = new Blog_Post__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	   			 ,DataCategoryName = 'Getting_Started'
        																			     	   			 ,ParentId = blogArticle01.Id);

        FAQ__DataCategorySelection faqDataCategorySelection = new FAQ__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     	,DataCategoryName = 'Getting_Started'
        																			     	,ParentId = faqArticle01.Id);

        Success_Story__DataCategorySelection successDataCategorySelection = new Success_Story__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupList[0].getName()
        																			     				     		,DataCategoryName = 'Getting_Started'
        																			     					 	    ,ParentId = successStoryArticle01.Id);

        insert recipeDataCategorySelection;
        insert blogDataCategorySelection;
        insert faqDataCategorySelection;
        insert successDataCategorySelection;
    }

    @isTest
    private static void testGetByNameForBlogArticle(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-blog-01');

    	Test.stopTest();

    	System.assert(articleResult != null);
    	System.assert(!String.isBlank(articleResult.Id));
    }

    @isTest
    private static void testGetByNameForFAQArticle(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-faq-01');
    	String categoryUrlName = articleResult.CategoryUrlName;

    	Test.stopTest();

    	System.assert(articleResult != null);
    	System.assert(!String.isBlank(articleResult.Id));
    }

    @isTest
    private static void testGetByNameForSuccessStoryArticle(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-success-story-01');

    	Test.stopTest();

    	System.assert(articleResult != null);
    	System.assert(!String.isBlank(articleResult.Id));
    }

    @isTest
    private static void testGetByIdForBlogArticle(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName('test-blog-01');

    	Test.startTest();

    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getById(articleRecord.Id);

    	Test.stopTest();

    	System.assert(articleResult != null);
    	System.assert(!String.isBlank(articleResult.Id));
    }

    @isTest
    private static void testGetByArticleType(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByArticleType('Recipe__kav');

    	Test.stopTest();

    	System.assertEquals(8, articleResultList.size());
    }

    @isTest
    private static void testGetCountByArticleType(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	Integer articleCount = knowledgeService.getCountByArticleType('Recipe__kav');

    	Test.stopTest();

    	System.assertEquals(8, articleCount);
    }

    @isTest
    private static void testGetLayoutCountByArticleType(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	HMR_Knowledge_Service.LayoutCount articleLayoutCount = knowledgeService.getLayoutCountByArticleType('Recipe__kav');

    	Test.stopTest();

    	System.assertEquals(8, articleLayoutCount.TotalCount);
    }

    @isTest
    private static void testGetByCategory(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByCategory('Getting_Started');

    	Test.stopTest();

    	System.assertEquals(4, articleResultList.size());
    }

     @isTest
    private static void testGetByCategoryWithRecordLimitAndOffset(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByCategory('Getting_Started', 1, 1, '');

    	Test.stopTest();

    	System.assertEquals(1, articleResultList.size());
    }


    @isTest
    private static void testGetByCategoryNoResults(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByCategory('Eating_Well');

    	Test.stopTest();

    	System.assertEquals(0, articleResultList.size());
    }

    @isTest
    private static void testGetByCategoryWithoutClauseNoResults(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByCategory('Eating_Well', 10, 0, '');

    	Test.stopTest();

    	System.assertEquals(0, articleResultList.size());
    }

    @isTest
    private static void testGetByCategoryWithCardLayoutClauseNoResults(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	List<HMR_Knowledge_Service.ArticleRecord> articleResultList = knowledgeService.getByCategory('Eating_Well', 10, 0, 'Card_Layout__c = \'TopBottom\'');

    	Test.stopTest();

    	System.assertEquals(0, articleResultList.size());
    }

    @isTest
    private static void testGetCountByCategoryNoResults(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	Integer articleCount = knowledgeService.getCountByCategory('Eating_Well');

    	Test.stopTest();

    	System.assertEquals(0, articleCount);
    }

    @isTest
    private static void testGetLayoutCountByCategoryNoResults(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

    	Test.startTest();

    	HMR_Knowledge_Service.LayoutCount articleLayoutCount = knowledgeService.getLayoutCountByCategory('Eating_Well');

    	Test.stopTest();

    	System.assertEquals(0, articleLayoutCount.TotalCount);
    }

    @isTest
    private static void testGetVoteWithoutRecord(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-blog-01');

    	Test.startTest();

    	Vote voteRecord = knowledgeService.getVote(articleResult.UrlName);

    	Test.stopTest();

    	System.assert(voteRecord != null);
    }

    @isTest
    private static void testGetVoteCountWithoutRecord(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-blog-01');

    	Test.startTest();

    	Integer voteCount = knowledgeService.getVoteCount(articleResult.UrlName);

    	Test.stopTest();

    	System.assert(voteCount == 0);
    }

    @isTest
    private static void testVoteForKnowledgeArticle(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	HMR_Knowledge_Service.ArticleRecord articleResult = knowledgeService.getByName('test-blog-01');

    	Test.startTest();

    	Vote voteRecord = knowledgeService.voteForKnowledgeArticle(articleResult.UrlName);

    	Test.stopTest();

    	System.assert(!String.isBlank(voteRecord.Id));
    }

	@isTest
    private static void testArticlePreview(){

		Test.setCurrentPageReference(new PageReference('HMR_ArticlePreview'));
		System.currentPageReference().getParameters().put('name', 'test-blog-01');

		HMR_ArticlePreviewController controller = new HMR_ArticlePreviewController();
    	controller.articleName = 'test-blog-01';

    	Test.startTest();

    		controller.getArticleRecord();

    	Test.stopTest();

    }

	@isTest
    private static void testArticlePreviewNoURL(){

		Test.setCurrentPageReference(new PageReference('HMR_ArticlePreview'));
		//System.currentPageReference().getParameters().put('name', 'test-blog-01');

		HMR_ArticlePreviewController controller = new HMR_ArticlePreviewController();
    	//controller.articleName = 'test-blog-01';

    	Test.startTest();

    		controller.getArticleRecord();

    	Test.stopTest();

    }
}