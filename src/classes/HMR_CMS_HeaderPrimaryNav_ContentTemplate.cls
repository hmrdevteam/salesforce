/**
* Apex Content Template Controller for Primary Nav inside header on OCMS Master
*
* @Date: 2017-03-206
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/

global virtual with sharing class HMR_CMS_HeaderPrimaryNav_ContentTemplate extends cms.ContentTemplateController{
	
    //need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_HeaderPrimaryNav_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_HeaderPrimaryNav_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Plans Label Property
    public String PlansText{    
        get{
            return getPropertyWithDefault('PlansText', 'Plans');
        }    
    }
    //Plans Page Property
    public cms.Link PlansLinkObj{
        get{
            return this.getPropertyLink('PlansLinkObj');
        }
    }
    //Recipes Label Property
    public String RecipeText{    
        get{
            return getPropertyWithDefault('RecipeText', 'Recipes');
        }    
    }
    //Recipes Page Property
    public cms.Link RecipeLinkObj{
        get{
            return this.getPropertyLink('RecipeLinkObj');
        }
    }
    //Resources Label Property
    public String ResourcesText{    
        get{
            return getPropertyWithDefault('ResourcesText', 'Resources');
        }    
    }
    //Resources Page Property
    public cms.Link ResourcesLinkObj{
        get{
            return this.getPropertyLink('ResourcesLinkObj');
        }
    }
    

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        //init links URL var to hold Url's of the target page
        String plansLinkUrl = '', recipeLinkUrl = '', resourcesLinkUrl = '';
        //check cms.Link properties for null and assign values to above var's
        plansLinkUrl = (this.PlansLinkObj != null) ? this.PlansLinkObj.targetPage : '#';
        recipeLinkUrl = (this.RecipeLinkObj != null) ? this.RecipeLinkObj.targetPage : '#';
        resourcesLinkUrl = (this.ResourcesLinkObj != null) ? this.ResourcesLinkObj.targetPage : '#';
        html += '<li>' +
                    '<a href="' + plansLinkUrl + '">' + 
                        PlansText.escapeHtml4() + 
                    '</a>' +
                '</li>' +
                '<li>' +
                    '<a href="' + recipeLinkUrl + '">' + 
                        RecipeText.escapeHtml4() + 
                    '</a>' +
                '</li>' +
                '<li>' +
                    '<a href="' + resourcesLinkUrl + '">' + 
                        ResourcesText.escapeHtml4() + 
                    '</a>' +
                '</li>';
        return html;
    }
}