/**
* Apex Content Template Controller for Featured Articles Section
*
* @Date: 07/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified:
* @JIRA: HPRP-4046
*/
global virtual class HMR_CMS_FeaturedArticles_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_FeaturedArticles_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_FeaturedArticles_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String SectionCSSClass {
        get{
            return getPropertyWithDefault('BackgroundCSSClass', 'bg-grey');
        }
    }

    global virtual override String getHTML(){
    	String html = '<section class="' + SectionCSSClass + ' resources-featured-articles">' +
                            '<div class="container">' +
                                '<div class="col-xs-12" style="padding-top: 40px; padding-bottom: 40px;">' +
                                    '<p class="sub-header featured-header">Featured Articles</p>' +
                                '</div>' +
                                '<div class="row">' +
                                    getFeaturedArticleHtml() +
                                '</div>' +
                            '</div>' +
                        '</section>';

    	return html;
   	}

    private string getFeaturedArticleHtml(){
        integer maxFeaturedArticles = 3;  //Possibly move to edit variable in future
        String sitePrefix = Site.getBaseUrl();
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        List<KnowledgeArticleVersion> masterArticleList =  knowledgeService.getKnowledgeMasterArticleListByCategory('All');
        List<HMR_Knowledge_Service.ArticleRecord> featuredArticleList = knowledgeService.getKnowledgeArticleVersionDetails(masterArticleList, true);
        string html = '';

        for(integer index = 0; index < maxFeaturedArticles && index < featuredArticleList.size(); index++){
            HMR_Knowledge_Service.ArticleRecord article = featuredArticleList[index];

            html += '<div class="ka-card tb-card col-lg-4 col-md-6 col-sm-6 col-xs-12" onclick="' + 'cardList.navigateToRecord(\'' + article.ArticleType + '\', \'' + article.DataCategoryDisplay + '\', \'' + article.UrlName + '\')">' +
                        '<div class="product-card">' +
                            '<div class="product-img">' +
                                '<img src="' + (String.isBlank(article.MobileImageUri) ? article.ImageUri : article.MobileImageUri) + '" />' +
                            '</div>' +
                            '<div class="card-block">' +
                                '<div class="card-row">' +
                                    '<div class="hmr-info">' + article.DataCategoryDisplay + '</div>' +
                                '</div>' +
                                '<div class="card-row ka-title-row">' +
                                    '<h4 class="big-label">'+ article.Title + '</h4>' +
                                '</div>' +
                                '<div class="card-row ka-card-info">' +
                                    '<div class="ka-time-img">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockIcon.svg" />' +
                                    '</div>' +
                                    '<div class="ka-info hmr-allcaps">' +
                                            (article.TotalMinutes == null ? article.ReadTime : String.valueOf(article.TotalMinutes)) + ' MIN' +
                                    '</div>' +
                                    '<div class="ka-heart-img">' +
                                            '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartIcon.svg" />' +
                                    '</div>' +
                                    '<div class="ka-heart-count hmr-allcaps">' +
                                            article.Rating +
                                    '</div>' +
                                '</div>' +
                                '<div class="card-row ka-details col-xs-12">' +
                                    '<div class="ka-author-img">' +
                                        // '<img src="' + article.AuthorSmallPhotoUrl + '" />' +
										'<img src="https://www.myhmrprogram.com/ContentMedia/RecipeHome/DefaultProfilePicture.png" />' +
                                    '</div>' +
                                    '<div class="ka-author">' +
                                        // '<h3>' + article.AuthorName + '</h3>' +
									    '<h3>HMR Team</h3>' +
                                    '</div>' +
                                    '<div class="ka-separator">' +
                                        '<h3 class="book">|</h3>' +
                                    '</div>' +
                                    '<div class="ka-location">' +
                                        '<h3 class="book">' + article.AuthorTitle + '</h3>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
        }

        return html;
    }
}