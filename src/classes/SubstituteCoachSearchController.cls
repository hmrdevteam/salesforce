/**
* Apex Class for searching all the coaches available for substitution
* and assinging substitute coach
* Managing (Search, update) Class Member and Session Attendee records
*
* Objects referenced --> Class__c,Coaching_Session__c,Coach__c
*
* @Date: 2016-12-08
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA: Issue https://reside.jira.com/browse/HPRP-631, Task https://reside.jira.com/browse/HPRP-682
*/


public Class SubstituteCoachSearchController{

    public String selectedCoachId {get;set;}
    public List<Coach__c> substituteCoachList{get;set;}
    public Coaching_Session__c coachingSessDetail {get;set;}
    String csId;
    
    public SubstituteCoachSearchController(ApexPages.StandardController controller) {    
         

       // getting the coaching session id from url
        csId = apexpages.currentpage().getparameters().get('Id');
        try{
               // getting the original coaching session details.
               // fetching coaching session records for the id which we get from url.
                coachingSessDetail = [Select hmr_Class_Date__c, hmr_Start_Time__c, hmr_End_Time__c, hmr_Substitute_Coach__c, hmr_Class__r.hmr_Coach__r.hmr_Coach__r.name, hmr_Coach__c, hmr_Coach_Name__c,
                                       hmr_Class_Name__c,hmr_Class_Type__c from Coaching_Session__c where id = :csId];
    
        
               // converting original start date into String.
               Date originaldate = coachingSessDetail.hmr_Class_Date__c;
               String originalStartDate = DateTime.newInstance(originaldate.year(),originaldate.month(),originaldate.day()).format('MM/d/YYYY');
           
           
          
               // List of all the available coaches with following condions:-
               //     Having an active coach
               //     class date may or may not b same as original class date
               //     if class date is same then class start time should b different
               //     if class start time is same date should be different
               //     having same Class Type
               //     not having same coach id (We dont want to show current coach in substitute coach list)                                        
                List<Coaching_Session__c> availableClassList = [Select hmr_Coach__c,hmr_Start_Time__c,hmr_End_Time__c, hmr_Class__c,hmr_Class_Date__c from Coaching_Session__c where
                                                                hmr_Coach__r.hmr_Active__c = TRUE AND
                                                            ( ( hmr_Class_Date__c != :coachingSessDetail.hmr_Class_Date__c ) OR
                                                                ( hmr_Class_Date__c = :coachingSessDetail.hmr_Class_Date__c ) OR
                                                                (hmr_Class_Date__c = :coachingSessDetail.hmr_Class_Date__c AND
                                                                 hmr_Start_Time__c != :coachingSessDetail.hmr_Start_Time__c) OR
                                                              (hmr_Class_Date__c != :coachingSessDetail.hmr_Class_Date__c AND
                                                                 hmr_Start_Time__c = :coachingSessDetail.hmr_Start_Time__c)
                                                             ) AND
                                                            hmr_Class_Type__c = :coachingSessDetail.hmr_Class_Type__c
                                                             AND
                                                            hmr_Coach__c != :coachingSessDetail.hmr_Coach__c];
             
              // checking if the list is empty, that is coaches are not available
               if(availableClassList.size() == 0){
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Coaches are not available'));
               }
               else{
                    System.debug('List Of Available Class -> ' + availableClassList);
                
                 //  converting original Start and End Time from String to DateTime
                   DateTime originalStartDateTime = DateTime.parse(originalStartDate + ' ' + coachingSessDetail.hmr_Start_Time__c);
                   DateTime originalEndDateTime = DateTime.parse(originalStartDate + ' ' + coachingSessDetail.hmr_End_Time__c);
               
                   Date sesDate;
                   String sessionDate;
                   DateTime sessionStartDateTime;  
                   DateTime sessionEndDateTime;  
                   Set<ID> coachIdSet = new Set<ID>();
                   Set<Id> coachToRemove = new set<Id>();
                   
                   
                   for(Coaching_Session__c classObj : availableClassList){
                        
                        // checking if both the sessions are on same date
                         if(classObj.hmr_Class_Date__c == coachingSessDetail.hmr_Class_Date__c){
                            
                            // converting start time of session from string to DateTime
                            sesDate = classObj.hmr_Class_Date__c;
                            sessionDate = DateTime.newInstance(sesDate.year(),sesDate.month(),sesDate.day()).format('MM/d/YYYY');
                            sessionStartDateTime = DateTime.parse(sessionDate + ' ' + classObj.hmr_Start_Time__c);
                            sessionEndDateTime = DateTime.parse(sessionDate + ' ' + classObj.hmr_End_Time__c);
                            
                            // checking if the start time of the session falls between the original start and end time of session
                            // note :- since both the session are on same date then the same coach cannot take session if
                            // his/her class falls between the original start and end time of session.
                            if((sessionStartDateTime >= originalStartDateTime && sessionStartDateTime < originalEndDateTime) ||
                                (sessionEndDateTime > originalStartDateTime &&  sessionEndDateTime  <= originalEndDateTime)){                        
                                
                                //set of all the coachs who cannot take the class   
                                coachToRemove.add(classObj.hmr_Coach__c);
                            
                            }
                            else{
                                // set of available coachs
                                coachIdSet.add(classObj.hmr_Coach__c);    
                            }
                         }
                         else{
                             // set of available coachs
                            coachIdSet.add(classObj.hmr_Coach__c);
                        }
                    }
                
                System.debug('Substitute Coach SET  -> ' + coachIdSet);       
                
                // getting the list of all the coaches with following conditions.
                // must b active
                // must present in list of available coachs
                // must not present in list of unavailable coaches.
                substituteCoachList = [Select id, name, hmr_Active__c,hmr_Coach__r.name from Coach__c where hmr_Active__c = TRUE AND Id IN :coachIdSet 
                                       AND Id NOT IN :coachToRemove ];      
                System.debug('Substitute Coach LIst -> ' + substituteCoachList);
            }
        }
                
        catch(Exception ex){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Record'));
              System.debug('Error -> ' + ex.getMessage());
        }

    }
    
     /**
    *  method to get the Id of the selected coach 
    *  for substitution
    *
    *  @param 
    *  @return void
    */  
   
   
    public void selectedCoach(){
    
        // assinging selected coach Id to the variable
        selectedCoachId = System.currentPagereference().getParameters().get('subsCoachId');
        System.debug('Selected Coach -> ' + selectedCoachId );
    }
    
    /**
    *  method to assign the new coach as a 'substitute coach'
    *  to the coaching sessions
    *
    *  @param 
    *  @return PageReference
    */ 
   
   
    public PageReference assignSubstitute(){
    
        // checking if the selected coach is not the original coach
    /*
        if(selectedCoachId != coachingSessDetail.hmr_Coach__c){
            coachingSessDetail.hmr_Substitute_Coach__c = TRUE;
        }
    */
        coachingSessDetail.hmr_Coach__c = selectedCoachId;
        
        try{
            update coachingSessDetail;
        }
        catch(DMLException ex){
            System.debug(ex.getMessage());
        } 
        
        PageReference retPage = new PageReference('/'+ csId); 
        return retPage;
    
    }
    
    
    /**
    *  method to return back to the previous page 
    *  when cancel button is clicked
    *
    *  @param 
    *  @return PageReference
    */ 
    
    
    public PageReference returnToPreviousPage(){
    
        PageReference retPage = new PageReference('/'+ csId); 
        return retPage;
    
    }

}