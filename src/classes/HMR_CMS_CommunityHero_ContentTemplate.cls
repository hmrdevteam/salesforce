/**
* Apex Content Template Controller for Main Community Hero
*
* @Date: 07/13/2017
* @Author Zach Engman (Magnet 360)
* @Modified:
* @JIRA: HPRP-4051
*/
global virtual class HMR_CMS_CommunityHero_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_CommunityHero_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_CommunityHero_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public String HeroSupplementCSSClass{
        get{
            return getPropertyWithDefault('HeroSupplementCSSClass', '');
        }
    }

    public String HeroImage{
    	get{
    		return getPropertyWithDefault('HeroImage', 'bg-resource-hero');
    	}
    }

    public String Title {
        get{
            return getPropertyWithDefault('Title', '');
        }
    }

    public String TitleCSSClass {
        get{
            return getPropertyWithDefault('TitleCSSClass', '');
        }
    }

    public String TitleAlignmentCSSClass {
        get{
            return getPropertyWithDefault('TitleAlignmentCSSClass', 'text-center');
        }
    }

    public String Subtitle{
        get{
            return getPropertyWithDefault('Subtitle', '');
        }
    }

    public String SubtitleCSSClass {
        get{
            return getPropertyWithDefault('SubtitleCSSClass', 'no-top-padding min-52');
        }
    }

    public String TitleDisplayType{
        get{
           return getPropertyWithDefault('TitleDisplayType', 'Standard');     
        }
    } 


    public String TitleIcon{
        get{
            return getPropertyWithDefault('TitleIcon', '');
        }
    }

    public String DisplaySearch{
        get{
            return getPropertyWithDefault('DisplaySearch', 'true');
        }
    }

    public String SearchPlaceholder{
        get{
            return getPropertyWithDefault('SearchPlaceholder', 'Search all Content...');
        }
    }

    public String SearchType{
        get{
            return getPropertyWithDefault('SearchType', 'both');
        }
    }

    public String FilterCategory{
        get{
            return getPropertyWithDefault('FilterCategory', '');
        }
    }

    public String MinimumSearchLength{
        get{
            return getPropertyWithDefault('MinimumSearchLength', '3');
        }
    }

    global virtual override String getHTML(){

		String html = 	'<div class="slim-hero ' + HeroImage + ' ' + HeroSupplementCSSClass + '">' +
					        '<div class="container ' + (!Boolean.valueOf(DisplaySearch) ? 'hidden-xs' : '') + '">' +
					            '<div class="row">' +
					                '<div class="col-xs-12">' +
                                        (TitleDisplayType == 'Standard' ? 
										'<h2 class="' + TitleAlignmentCSSClass + '">' + (!String.isBlank(TitleIcon) ? '<img src="' + TitleIcon + '" class="img-responsive hero-title-icon" />' : '') +  Title + '</h2>' +
										(!String.isBlank(Subtitle) ? '<div class="desc hmr-info">' + Subtitle + '</div>' : '') : 
                                        '<div class="col-sm-8 col-sm-offset-2">' + 
                                            '<h6 class="' + TitleCSSClass + '">' + Title + '</h6>' + 
                                            '<h2 class="' + SubtitleCSSClass + '">' + Subtitle + '</h2>' +
                                        '</div>') + 
                                        (Boolean.valueOf(DisplaySearch) ? 
                                        '<div class="col-sm-8 col-sm-offset-2">' +
                                            '<div class="search-list">' +
                                                '<div class="input-group searchBar col-xs-12">' +
        				                            '<select id="globalSearch" type="text" class="form-control input-group-addon global-search-dropdown" data-placeholder="' + SearchPlaceholder + '">' +       
        				                            '</select>' +
        				                            '<span class="input-group-addon search-icon">' +
        				                                '<img src="https://www.myhmrprogram.com/ContentMedia/Resources/MagnifyGlass.svg" />' +
        				                            '</span>' +
                                                '</div>' + 
    				                        '</div>' + 
                                        '</div>' : '') +
				                    '</div>' +
			                    '</div>' + 
			                '</div>' + 
		                '</div>';

        //Mobile Hero for non-search
        if(!Boolean.valueOf(DisplaySearch)){
            html +=     '<div class="container">' +
                            '<div class="col-xs-12 visible-xs-block">' +
                                '<div class="hero-content-mobile">' +
                                    '<h2 class="hero-title">' + Title + '</h2>' +
                                    (!String.isBlank(Subtitle) ? '<div class="desc hmr-info">' + Subtitle + '</div>' : '') +
                                '</div>' +
                            '</div>' +
                        '</div>';
        }
        
        //Add in properties used by type ahead search
        html += '<input type="hidden" id="globalSearch_SearchType" value="' + SearchType + '"/>' +
                '<input type="hidden" id="globalSearch_FilterCategory" value="' + FilterCategory + '"/>' + 
                '<input type="hidden" id="globalSearch_MinimumSearchLength" value="' + MinimumSearchLength + '"/>' + 
                '<input type="hidden" id="globalSearch_Placeholder" value="' + SearchPlaceholder + '"/>';
        return html;
	}
}