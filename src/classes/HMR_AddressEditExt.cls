/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 14 August 2017
 * Created By: Nathan Anderson
 * Last Modified Date:
 * Last Modified By:
 * Description:
This logic is used by the HMR_AddressEdit page and HMR_AddressEditSelector, called from "Update Addresses" button on Contact page layout.
This allows Program Specialists to make updates to Addresses in Salesforce without going to Customers Profile.
This is a workaround, meant to help with accounts that have many addresses
 * ****************************************************/

public with sharing class HMR_AddressEditExt {
	//public variables
	public List<ccrz__E_AccountAddressBook__c> addressList {get;set;}
	public Contact client {get;set;}
	public Id contactId {get;set;}
	public Id selectedAddressId {get;set;}
	public ccrz__E_AccountAddressBook__c selectedAddressBook {get; set;}
	public ccrz__E_ContactAddr__c selectedContactAddress {get; set;}

	public HMR_AddressEditExt(ApexPages.StandardController stdController) {
		//get the contact id from the URL
		contactId = apexpages.currentpage().getparameters().get('id');
		//get the Contact record
		client = [SELECT Id, Name, FirstName, LastName, Client_ID__c, Community_User_Id__c FROM Contact WHERE Id = :contactId LIMIT 1];


		//Query for all Contact Address & AddressBook records for this User
		addressList = [SELECT Id, Name, ccrz__E_ContactAddress__c, ccrz__Default__c, ccrz__AddressType__c, ccrz__E_ContactAddress__r.Name,
							ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c, ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
							ccrz__E_ContactAddress__r.ccrz__City__c, ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
							ccrz__E_ContactAddress__r.ccrz__PostalCode__c, ccrz__E_ContactAddress__r.ccrz__FirstName__c,
							ccrz__E_ContactAddress__r.ccrz__LastName__c, OwnerId
						FROM ccrz__E_AccountAddressBook__c WHERE OwnerId = :client.Community_User_Id__c];

	}

	public void selectAddress(){
		//get the id of the selected record from the page parameters
        selectedAddressId = System.currentPagereference().getParameters().get('addressId');

    }

	public PageReference goToEditPage() {

		//Called from "Select Address" button on HMR_AddressEditSelector Page, sends use to Edit page
		if(selectedAddressId != null) {

			PageReference page = new PageReference('/apex/HMR_AddressEditPage?id='+client.Id+'&addressId='+selectedAddressId);
			return page;

		} else {
			//Throw an error if the update button is hit before an address is selected
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an address to update.'));

			return null;
		}
	}

	//This method is called on page load of HMR_AddressEditPage, sets up the Address Book and Contact Address records
	public void loadAddressBooks() {

		//get the address id sent from the selectAddress method (on click of radio button)
		selectedAddressId = apexpages.currentpage().getparameters().get('addressId');

		//get the parent Address Book record
		selectedAddressBook = [SELECT Id, Name, ccrz__E_ContactAddress__c, ccrz__Default__c, ccrz__AddressType__c, OwnerId
								FROM ccrz__E_AccountAddressBook__c WHERE Id = :selectedAddressId];

		//get the contact address record selected
		selectedContactAddress = [SELECT Id, Name, ccrz__AddressFirstline__c, ccrz__AddressSecondline__c, ccrz__City__c, ccrz__StateISOCode__c,
									ccrz__PostalCode__c, ccrz__FirstName__c, ccrz__LastName__c, OwnerId FROM ccrz__E_ContactAddr__c
									WHERE Id = :selectedAddressBook.ccrz__E_ContactAddress__c];

	}


	public PageReference updateAddress(){

		//if selection is not null, get updates from input fields and run updates
		if(selectedAddressBook != null) {

			//Run DML updates
			try{
				update selectedAddressBook;
				update selectedContactAddress;
			} catch(Exception e){
				ApexPages.addMessages(e);
	        }

			//return to Order page after complete
			PageReference page = new PageReference('/'+client.Id);
			return page;

		} else {
			//Throw an error if the update button is hit before an address is selected
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error Updating this address'));

			return null;
		}

	}

	public PageReference cancel(){

		PageReference page = new PageReference('/'+client.Id);
		return page;

    }
}