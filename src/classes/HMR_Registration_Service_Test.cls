/*
Developer: Ali Pierre (HMR)
Date: 1/12/2018
Modified: 
Modified Date : 
Target Class: HMR_Registration_Service
 */
@IsTest public with sharing class HMR_Registration_Service_Test {
    @IsTest(SeeAllData=true) static void testRegistration() {
        //Initialize map to hold return data and set up call to HMR Registration Service
        Map<String, Object> returnMap = new Map<String, Object>();

        HMR_Registration_Service regService = new HMR_Registration_Service();

        String firstname = 'TestFirstname';
        String lastname = 'TestUser';
        String email = 'test112233@gmclkr.com';
        String password = 'TestPAssword!!!44';
        String zipcode = '02645';
        Boolean preference = true;
        String regType = 'General';

        //Create Contact and associate test HMR Customer Community User to Contact 
        Account testAcc = new Account(name = 'testAcc');
        insert testAcc;
        
        Contact newContact = new Contact(FirstName = 'TestFirstname', LastName = 'TestUser', AccountId = testAcc.Id, Email = 'testuser@gma.com');
        insert newContact;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];
        User u = new User(Alias = 'rtest', Email='testuser@gma.com', IsActive = TRUE,
                    EmailEncodingKey='UTF-8', LastName='RunningTest', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = profileId.id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='testuser@gma.com', ContactId = newContact.Id);
                    
        insert u;

        returnMap = regService.processRegistration(firstname, lastname, email, password, zipcode, preference, regType); 
        System.assertEquals((Boolean)returnMap.get('success'), true);
        System.assertEquals((String)returnMap.get('userId'), u.Id);
    }

    @IsTest(SeeAllData=true) static void testRegistration_two() {

        Map<String, Object> returnMap = new Map<String, Object>();

        HMR_Registration_Service regService = new HMR_Registration_Service();

        String firstname = 'TestFirstname';
        String lastname = 'TestLastname';
        String email = 'test112233@gmclkr.com';
        String password = 'TestingPassword!23';
        String zipcode = '02110';
        Boolean preference = true;
        String regType = 'General';


        Lead newLead = new Lead(Email = 'test112233@gmclkr.com', FirstName = 'Testing', LastName = 'Test', Status = 'New', hmr_Lead_Type__c = 'Consumer', Company = 'Test Company');
        insert newLead;

        returnMap = regService.processRegistration(firstname, lastname, email, password, zipcode, preference, regType);
        System.assertEquals((Boolean)returnMap.get('success'), false); 
        System.assertEquals((String)returnMap.get('exceptionIn'), 'GeneralSiteRegister');
    }

    @IsTest(SeeAllData=true) static void testRegistration_three() {

        Map<String, Object> returnMap = new Map<String, Object>();

        HMR_Registration_Service regService = new HMR_Registration_Service();

        String firstname = 'Test';
        String lastname = 'User3';
        String email = 'test112233@gmclkr.com';
        String password = 'TestingPassword!!2';
        String empcode = 'AURORA';
        Boolean preference = false;
        String regType = 'Employer';

        //Create Contact and associate test HMR Customer Community User to Contact 
        Account testAcc = new Account(name = 'testAcc');
        insert testAcc;
        
        Contact newContact = new Contact(FirstName = 'TestFirstname', LastName = 'TestUser', AccountId = testAcc.Id, Email = 'testuser@gma.com');
        insert newContact;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];
        User u = new User(Alias = 'rtest', Email='testuser@gma.com', IsActive = TRUE,
                    EmailEncodingKey='UTF-8', LastName='RunningTest', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = profileId.id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='testuser@gma.com', ContactId = newContact.Id);
                    
        insert u;

        returnMap = regService.processRegistration(firstname, lastname, email, password, empcode, preference, regType); 
        System.assertEquals((Boolean)returnMap.get('success'), true);
        System.assertEquals((String)returnMap.get('userId'), u.Id);
    }

    @IsTest(SeeAllData=true) static void testRegistration_four() {

        Map<String, Object> returnMap = new Map<String, Object>();

        HMR_Registration_Service regService = new HMR_Registration_Service();

        String firstname = 'Test';
        String lastname = 'User3';
        String email = 'test112233@gmclkr.com';
        String password = 'TestingPassword!!2';
        String empcode = 'WRONGCODE';
        Boolean preference = false;
        String regType = 'Employer';


        returnMap = regService.processRegistration(firstname, lastname, email, password, empcode, preference, regType);
        System.assertEquals((Boolean)returnMap.get('success'), false); 
        System.assertEquals((String)returnMap.get('errorMsg'), 'Please enter a valid Employer Code.'); 

    }

    @IsTest(SeeAllData=true) static void testRegistration_five() {

        Map<String, Object> returnMap = new Map<String, Object>();

        HMR_Registration_Service regService = new HMR_Registration_Service();

        String firstname = 'TestFirstname';
        String lastname = 'TestUser';
        String email = 'test112233@gmclkr.com';
        String password = 'TestPAssword!!!44';
        String zipcode = '02645';
        Boolean preference = true;
        String regType = 'General';   

        returnMap = regService.processRegistration(firstname, lastname, email, password, zipcode, preference, regType); 
        System.assertEquals((Boolean)returnMap.get('success'), false); 
        System.assertEquals((String)returnMap.get('exceptionIn'), 'GeneralSiteRegister');
    }

}