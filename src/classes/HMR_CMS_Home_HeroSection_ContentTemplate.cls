/**
* Apex Content Template Controller for Hero Section on Home Page
*
* @Date: 2017-03-206
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_Home_HeroSection_ContentTemplate extends cms.ContentTemplateController{	
	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_Home_HeroSection_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_Home_HeroSection_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    //Title Label Property
    public String TitleText{    
        get{
            return getPropertyWithDefault('TitleText', 'Willpower is Not Enough!');
        }    
    }

    //SubTitle Label Property
    public String SubTitleText{    
        get{
            return getPropertyWithDefault('SubTitleText', 'LOSING WEIGHT');
        }    
    }

    //Hero Background Image Property
    public String BackgroundImage{
        get{
            return getProperty('BackgroundImage');
        }
    }

    //US News Icon Image Property
    public String USNewsIconImage_First{
        get{
            return getProperty('USNewsIconImage_First');
        }
    }

    //US News Icon Image Property
    public String USNewsIconImage_Second{
        get{
            return getProperty('USNewsIconImage_Second');
        }
    }

    //Learn More Button Label Property
    public String ButtonLabelText{
    	get{
            return getPropertyWithDefault('ButtonLabelText', 'Learn More');
        }
    }

    //Learn More Button Page Link Property
    public cms.Link ButtonLinkObj{
        get{
            return this.getPropertyLink('ButtonLinkObj');
        }
    }

    //Other Link (Already an HMR Member) Label Property
    public String OtherLinkLabelText{
    	get{
            return getPropertyWithDefault('OtherLinkLabelText', 'Already an HMR Member >');
        }
    }

    //Other Link (Already an HMR Member) Page Link Property
    public cms.Link OtherLinkLinkObj{
        get{
            return this.getPropertyLink('OtherLinkLinkObj');
        }
    }

    //global override getHTML - renders the nav list items for primary nav header
    global virtual override String getHTML() { // For generated markup 
        String html = '';
        String sitePrefix = Site.getBaseUrl();
        //init links URL var to hold Url's of the target page
        String buttonLinkUrl = '', otherLinkLinkUrl = '';
        //check cms.Link properties for null and assign values to above var's
        buttonLinkUrl = (this.ButtonLinkObj != null) ? this.ButtonLinkObj.targetPage : '#';
        otherLinkLinkUrl = (this.OtherLinkLinkObj != null) ? this.OtherLinkLinkObj.targetPage : '#';        
        html += '<section id="donut" class="mobile-padding" style= "background-image : url(\'' + sitePrefix + BackgroundImage + '\')" aria-hidden="true">' +
	                '<div class="container">' +
	                    '<div class="row">' +
	                        '<div class="col-md-10 col-md-offset-1 col-xs-offset-1">' +
                                '<h3 class="main-section-LW">' + SubTitleText + '</h3>' +
                                '<h3 class="main-section-header">' + TitleText + '</h3>' +
	                        '</div>' +
	                    '</div>' +
	                    '<div class="row">' +
	                        '<div class="col-md-9 col-md-offset-1 text-center">' +
	                            '<button class="btn btn-primary section-button home-hero-button" onClick="javascript:window.location.href=\'' + buttonLinkUrl + '\'" data-url="' + buttonLinkUrl + '">' + 
	                            	ButtonLabelText + 
	                            '</button>' +	                           
	                        '</div>' +	                        
	                        '<div class="col-md-9 col-md-offset-1 text-center spacing-top-lg">' +
	                            '<a class="main-section-link" href="' + otherLinkLinkUrl + '">' +
	                            	OtherLinkLabelText + 
	                            '</a>' +
	                        '</div>' +
	                    '</div>' +
	                    '<div class="row">' +
	                        '<div class="col-sm-5 col-xs-12 best-diet">' +
	                            '<img src="' + sitePrefix + USNewsIconImage_First + '">' + 
	                            '<img src="' + sitePrefix + USNewsIconImage_Second + '">' +
	                        '</div>' +
	                    '</div>' +
	                '</div>' +
		        '</section>';
        return html;
    }
}