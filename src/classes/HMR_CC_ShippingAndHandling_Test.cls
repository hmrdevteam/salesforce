/**
* @Date: 3/27/2017
* @Author: Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-
*
* This test class covers HMR_CC_ShippingAndHandling_Extn Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CC_ShippingAndHandling_Test {

  @isTest static void test_method_one() {
      
    cc_dataFActory.setupTestUser();
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
    System.runAs ( thisUser ) {
    
    List<Shipping_Rates__c> shippingRatesList=new List<Shipping_Rates__c>();
    List<ccrz.cc_ctrl_hlpr_ShippingOption> shippingOptions=new List<ccrz.cc_ctrl_hlpr_ShippingOption>();
    Boolean freeShipping=true;
    
    Shipping_Rates__c shRate1=new Shipping_Rates__c(
                                    Shipping_Type__c='Standard',
                                    Shipping_Cost__c=11.95,
                                    Cart_Total_Low__c=0.00,
                                    Cart_Total_High__c=224.99,
                                    Active__c=true
                                   );
    insert shRate1;                              
   
    Shipping_Rates__c shRate2=new Shipping_Rates__c(
                                    Shipping_Type__c='Expedited',
                                    Shipping_Cost__c=35.00,
                                    Cart_Total_Low__c=0.00,
                                    Cart_Total_High__c=150.00,
                                    Active__c=true
                                   );  
                    
     insert shRate2;                             
     ccrz.cc_ctrl_hlpr_ShippingOption shippingOption = new ccrz.cc_ctrl_hlpr_ShippingOption();
            shippingOption.provider = 'HMR';
            shippingOption.serviceName = 'HMR EE';
            shippingOption.price = 50.00;
            shippingOption.discountedShipCost = 10.00;
            shippingOption.uniqueId = 'HMR' + 'HMR EE';
            shippingOption.currencyCode = 'USD';
            shippingOption.discount = 0; 
                                                                      
    
    shippingOptions.add(shippingOption );
    shippingRatesList.add(shRate1);
    shippingRatesList.add(shRate2);    
    
    //test if shipping is free
    HMR_CC_ShippingAndHandling_Extn.insertShippingOptions(shippingRatesList,shippingOptions,freeShipping);  
    
    //test if shipping is not free
    freeShipping=false;
    HMR_CC_ShippingAndHandling_Extn.insertShippingOptions(shippingRatesList,shippingOptions,freeShipping);  
    
    }
  }
  
    @isTest static void test_method_two() {
      
        cc_dataFActory.setupTestUser();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
                
        System.runAs ( thisUser ) {
         
        HMR_CC_ShippingAndHandling_Extn objClass=new HMR_CC_ShippingAndHandling_Extn();
        
        cc_dataFActory.setupCatalog();
       
        ccrz__E_Product__c objProduct= [Select Id from ccrz__E_Product__c where ccrz__SKU__c ='test001' limit 1];
       
        ccrz__E_Cart__c objCart=cc_dataFActory.createCart();
        
        cc_dataFActory.addCartItem(objCart,objProduct.Id,3,150);
        
        String zipCode='06066';
        String stateCode='IL';
        String countryCode='USA';
        String storeName='DefaultStore';
         
        List<ccrz.cc_ctrl_hlpr_ShippingOption> lstShippingOptions =objClass.getShippingOptions(zipCode, stateCode,countryCode, objCart.Id, storeName);
       
        //System.Assert(lstShippingOptions.size() > 0);
        }
    
    }
}