/**
* Apex Content Template for the Article Detail Section
*
* @Date: 2017-07-21
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-21
* @JIRA: HPRP-1435
*/
global virtual class HMR_CMS_ArticleDetail_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_ArticleDetail_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_ArticleDetail_ContentTemplate() {}

    global virtual override String getHTML(){
        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        string articleName = ApexPages.currentPage().getParameters().get('name');
        string html = '';

        //Try to get from url if empty
        if(String.isBlank(articleName)){
            List<String> urlPartList = ApexPages.currentPage().getURL().split('/');
            if(urlPartList.size() > 0)
                articleName = urlPartList[urlPartList.size() - 1].split('\\?')[0];
        }

        articleName = String.escapeSingleQuotes(articleName);

        if(!String.isBlank(articleName)){
            HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(articleName);
			System.debug('@@@@@@@@@@@ articleRecord: ' + articleRecord);

            if(articleRecord != null){
            	html = '<div class="article-page">' +
                            '<div class="container">' +
                                '<div class="col-xs-12">' +
                                    '<div class="avatar-info">' +
										'<img class="img-circle" src="https://www.myhmrprogram.com/ContentMedia/RecipeHome/DefaultProfilePicture.png" />' +
										'<div class="name">HMR Team</div>' +
                                        '<div class="location">' + articleRecord.LastPublishedDate + '</div>' +
                                    '</div>' +
                                '</div>' +
                                (!String.isBlank(articleRecord.Teaser) ?
                                '<div class="col-xs-12 sub-header">' +
                                    articleRecord.Teaser +
                                '</div>' : '') +
                            '</div>' +
                            (!String.isBlank(articleRecord.Takeaways) ?
                            '<div class="container takeaways-bg">' +
                                '<div class="col-xs-12">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="col-xs-12">' +
                                           articleRecord.Takeaways +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' : '') +
                            '<div class="container pt-20">' +
                                (!String.isBlank(articleRecord.PostContent01) ?
                                    '<div class="col-xs-12 caps-letter">' +
                                        '<p class="cap">' +
                                            articleRecord.PostContent01 +
                                        '</p>' +
                                    '</div>' : '') +
                                (!String.isBlank(articleRecord.PostContent02) ?
                                    '<div class="col-xs-12">' +
                                        '<p>' +
                                            articleRecord.PostContent02 +
                                        '</p>' +
                                    '</div>' : '') +
                                (!String.isBlank(articleRecord.Image01) && String.isBlank(articleRecord.Image02) ?
                                '<div class="text-center">' +
                                    '<img src="' + articleRecord.Image01 + '" class="col-xs-12 article-img" />' +
                                '</div>' +
                                (!String.isBlank(articleRecord.Caption01) ?
                                '<div class="col-xs-12">' +
                                    '<p>' +
                                         articleRecord.Caption01 +
                                    '</p>' +
                                '</div>' : '') : '') +
                                (!String.isBlank(articleRecord.Image01) && !String.isBlank(articleRecord.Image02) && String.isBlank(articleRecord.Image03) ?
                                '<div class="col-xs-12 col-sm-6">' +
                                    '<div class="text-center">' +
                                        '<img src="' + articleRecord.Image01 + '" class="article-img" />' +
                                            (!String.isBlank(articleRecord.Caption01) ?
                                            '<div class="col-xs-12">' +
                                                '<p>' +
                                                   articleRecord.Caption01 +
                                                '</p>' +
                                            '</div>' : '') +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-sm-6">' +
                                    '<div class="text-center">' +
                                        '<img src="' + articleRecord.Image02 + '" class="article-img" />' +
                                            (!String.isBlank(articleRecord.Caption02) ?
                                            '<div class="col-xs-12">' +
                                                '<p>' +
                                                    articleRecord.Caption02 +
                                                '</p>' +
                                            '</div>' : '') +
                                    '</div>' +
                                '</div>' : '') +
                                (!String.isBlank(articleRecord.Image01) && !String.isBlank(articleRecord.Image02) && !String.isBlank(articleRecord.Image03) ?
                                '<div class="col-xs-12 col-sm-4">' +
                                    '<div class="text-center">' +
                                        '<img src="' + articleRecord.Image01 + '" class="article-img" />' +
                                            (!String.isBlank(articleRecord.Caption01) ?
                                            '<div class="col-xs-12">' +
                                                '<p>' +
                                                    articleRecord.Caption01 +
                                                '</p>' +
                                            '</div>' : '') +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-sm-4">' +
                                    '<div class="text-center">' +
                                        '<img src="' + articleRecord.Image02 + '" class="article-img" />' +
                                            (!String.isBlank(articleRecord.Caption02) ?
                                            '<div class="col-xs-12">' +
                                                '<p>' +
                                                    articleRecord.Caption02 +
                                                '</p>' +
                                            '</div>' : '') +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-sm-4"> ' +
                                    '<div class="text-center">' +
                                        '<img src="' + articleRecord.Image03 + '" class="article-img" /> ' +
                                            (!String.isBlank(articleRecord.Caption03) ?
                                            '<div class="col-xs-12">' +
                                                '<p>' +
                                                    articleRecord.Caption03 +
                                                '</p>' +
                                            '</div>' : '') +
                                    '</div>' +
                                '</div>' : '') +
                                (!String.isBlank(articleRecord.PostContent03) ?
                                    '<div class="col-xs-12">' +
                                        '<p>' +
                                            articleRecord.PostContent03 +
                                        '</p>' +
                                    '</div>' : '');

                html +=     '</div>'; //container pt-20

                if(!String.isBlank(articleRecord.VideoCode) && !String.isBlank(articleRecord.VideoImage01)){
                    //Video Player Section
                    html += '<section class="hmr-page-section bg-typecover video-bg" style="background-image: url(' + articleRecord.VideoImage01 + ')" >' +
                                '<div class="container">' +
                                    '<div class="media">' +
                                          '<h2 class="col-xs-12 col-sm-6 col-sm-push-2">' + articleRecord.VideoTitle + '</h2>' +
                                          '<a class="col-xs-12 col-sm-2 col-sm-pull-6 text-center" href="#articleVideo" data-toggle="modal">' +
                                            '<img class="play-square-icon" src="https://www.myhmrprogram.com/ContentMedia/CoachValueProp/PlaySquare.png" />' +
                                          '</a>' +
                                    '</div>' +
                                '</div>' +
                            '</section>';
                }

                html += (!String.isBlank(articleRecord.PostContent04) ?
                                '<div class="container pt-20">' +
                                    '<div class="col-xs-12">' +
                                        '<p>' +
                                            articleRecord.PostContent04 +
                                        '</p>' +
                                    '</div>' +
                                '</div>' : '');

                html += (!String.isBlank(articleRecord.FileDownloadUrl) ?
                            '<div class="row text-center">' +
                                '<div class="col-xs-12">' +
                                    '<a href="' + articleRecord.FileDownloadUrl + '" target="_blank" class="btn hmr-btn-blue hmr-btn-small white" id="startTopicBtn">Download</a>' +
                                '</div>' +
                            '</div>' : '');

                html += '</div>'; //article-page

                if(!String.isBlank(articleRecord.VideoCode) && !String.isBlank(articleRecord.VideoImage01)){
                    //Video Modal Popup
                    html += '<div id="articleVideo" class="modal fade video">' +
                                '<div class="modal-dialog">' +
                                        '<div>' +
                                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
                                        '</div>' +
                                        '<div>' +
                                            '<div class="fluidMedia">' +
                                                '<iframe data-modal="articleVideo" class="videoModalFrame" src="https://www.youtube.com/embed/' + articleRecord.VideoCode + '" frameborder="0" allowfullscreen></iframe>' +
                                            '</div>' +
                                        '</div>' +
                                '</div>' +
                            '</div>';
                }
            }
        }
    	return html;
   	}
}