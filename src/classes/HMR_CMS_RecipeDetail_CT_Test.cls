/*
Developer: Utkarsh Goswami (Mindtree)
Date: April 7th, 2017
Log: 
Target Class(ses): HMR_CMS_RecipeDetail_ContentTemplate
*/

@isTest
private class HMR_CMS_RecipeDetail_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_RecipeDetail_ContentTemplate recipeDetailTemplate = new HMR_CMS_RecipeDetail_ContentTemplate();
    String propertyValue = recipeDetailTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
    //String planImage1 = recipeDetailTemplate.planImage1;
    //String planImage2 = recipeDetailTemplate.planImage2;
    //String planImage3 = recipeDetailTemplate.planImage3;
    //String planImage4 = recipeDetailTemplate.planImage4;
    //String planImage5 = recipeDetailTemplate.planImage5;
    //recipeDetailTemplate.debugString = 'debugstring';

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    recipeDetailTemplate.testAttributes = tab;

    string s2 = recipeDetailTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = recipeDetailTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');

    String renderHTML = recipeDetailTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));
    Recipe__kav pk = recipeDetailTemplate.recipe;
    try{
        HMR_CMS_RecipeDetail_ContentTemplate qc = new HMR_CMS_RecipeDetail_ContentTemplate(null);
    }catch(Exception e){}
  }
  
}