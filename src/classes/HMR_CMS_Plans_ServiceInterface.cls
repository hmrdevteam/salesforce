/**
* Orchestra Service Interface class to determine if the logged in user is allowed to see
* the Healthy Shakes tab on Plans Page
*
* @Date: 01/19/2018
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global class HMR_CMS_Plans_ServiceInterface implements cms.ServiceInterface {
	global HMR_CMS_Plans_ServiceInterface() {}

	global HMR_CMS_Plans_ServiceInterface(cms.CoreController controller){}

	/**
     *
     * @param params a map of parameters including at minimum a value for 'action'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {
		Map<String, Object> returnMap = new Map<String, Object>();
		String userId = params.get('userId');
		User curUser = [Select Id, ContactId from User where Id =: userId];
		returnMap.put('curUser', curUser);
		try {
			if(curUser.ContactId != null){
				Id cId = curUser.ContactId;
				Contact con = [SELECT Account.RecordType.DeveloperName,
									  Account.hmr_Is_Healthy_Shakes_an_Option__c,
									  Account.Standard_HSAH_Kit_Discount__c,
									  Account.Standard_HSS_Kit_Discount__c,
									  Account.hmr_HSAH_Standard_Kit_Price__c,
									  Account.hmr_HSS_Standard_Kit_Price__c FROM Contact WHERE Id = :cId];//Corporate_Account
				returnMap.put('currentContact', con);
				//if cooperate account then use the value of hmr_Is_Healthy_Shakes_an_Option__c
				if(con.Account.RecordType.DeveloperName == 'Corporate_Account'){
					returnMap.put('isShakeOption', con.Account.hmr_Is_Healthy_Shakes_an_Option__c);
				}
				else {
					returnMap.put('isShakeOption', true);
				}
				returnMap.put('success', true);
			}
		}
		catch(Exception e) {
			// Unexpected Error
			String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
			returnMap.put('success', false);
			returnMap.put('message', JSON.serialize(message));
			returnMap.put('stack', e.getStackTraceString());
		}
		return JSON.serialize(returnMap);
	}

	public static Type getType() {
        return HMR_CMS_Plans_ServiceInterface.class;
    }
}