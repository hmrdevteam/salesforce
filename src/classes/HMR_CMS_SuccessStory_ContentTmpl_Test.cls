/**
* Test Class Coverage of the HMR_CMS_SuccessStory_ContentTemplate
*
* @Date: 08/17/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_SuccessStory_ContentTmpl_Test{     

    @isTest
    private static void testGetHtml(){
    	HMR_CMS_SuccessStory_ContentTemplate controller = new HMR_CMS_SuccessStory_ContentTemplate();

    	Test.startTest();
		
    	String htmlResult = controller.getHTML();

    	Test.stopTest();

    	System.assert(!String.isBlank(htmlResult));
    }
    
    @isTest
    private static void testContentControllerConstructor(){
        HMR_CMS_SuccessStory_ContentTemplate controller;
        
        Test.startTest();
        //This always fails as no way to get context but here for coverage
        try{
            controller = new HMR_CMS_SuccessStory_ContentTemplate(null);
        }
        catch(Exception ex){}
        
        Test.stopTest();
        
        System.assert(controller == null);
    }
           
}