public class TransactionPaymentTriggerHandler {
    
    public static void handlePaymentInsert(List<ccrz__E_TransactionPayment__c> newOrderPayments){
        // start of handler logic
            Set<Id> orderIds = new Set<Id>();   
            List<ccrz__E_TransactionPayment__c> paymentsToInsert = new List<ccrz__E_TransactionPayment__c>();           
        
           System.debug('This is First Trans  ---->  ' + newOrderPayments);
       
        for(ccrz__E_TransactionPayment__c paymentId : newOrderPayments){
            orderIds.add(paymentId.ccrz__CCOrder__c);
        }

        List<ccrz__E_Order__c> templateOrders = new List<ccrz__E_Order__c>(
            [SELECT Id, Name, ccrz__Order__c FROM ccrz__E_Order__c 
             WHERE ccrz__Order__c IN :orderIds AND ccrz__OrderStatus__c = 'Template' AND hmr_Program_Membership__r.hmr_Status__c = 'Active']);
        
        for(ccrz__E_Order__c tempOrd : templateOrders){
            for(ccrz__E_TransactionPayment__c ordPayment : newOrderPayments){
                if(tempOrd.ccrz__Order__c == ordPayment.ccrz__CCOrder__c){
                    paymentsToInsert.add(createPaymentDetails(tempOrd.Id, tempOrd.ccrz__Order__c, ordPayment));
                    break;
                }
            }
        }
        
        
       System.debug('###  handlePaymentInsert ' + paymentsToInsert.size());
        
        try {
            //DML insert on the Payments list
            if(!paymentsToInsert.isEmpty())
                insert paymentsToInsert;           
        }
        catch (Exception ex) {
            System.debug('handlePaymentInsert paymentsToInsert Exception ' + ex.getMessage());
        }
                      
    } 
    
       /**
    *  createPaymentDetails, method to create ccrz__E_TransactionPayment__c records to associate to appropriate template orders
    *  
    *  @param TemplateOrderID --> Id of the template order for which order items needs to be created
    *  @param TemplateOrderID --> Id of Parent order
    *  @paranm Map of ccrz__E_TransactionPayment__c --> Payment detail records associated with parent orders
    */
    public static ccrz__E_TransactionPayment__c createPaymentDetails(Id TemplateOrderID, Id parentOrderId, ccrz__E_TransactionPayment__c paymentDetail){
        //create a Payment Detail object and add relevant details 
                    ccrz__E_TransactionPayment__c newPaymentDetail = new ccrz__E_TransactionPayment__c(
                        ccrz__AccountNumber__c = paymentDetail.ccrz__AccountNumber__c, 
                        ccrz__AccountType__c = paymentDetail.ccrz__AccountType__c, 
                        ccrz__BillTo__c = paymentDetail.ccrz__BillTo__c, 
                        ccrz__CCOrder__c = TemplateOrderID, 
                        ccrz__CCSubscription__c = paymentDetail.ccrz__CCSubscription__c, 
                        ccrz__Contact__c = paymentDetail.ccrz__Contact__c, 
                        ccrz__CurrencyISOCode__c = paymentDetail.ccrz__CurrencyISOCode__c, 
                        ccrz__ExpirationMonth__c = paymentDetail.ccrz__ExpirationMonth__c, 
                        ccrz__ExpirationYear__c = paymentDetail.ccrz__ExpirationYear__c, 
                        ccrz__InvoiceAppliedFor__c = paymentDetail.ccrz__InvoiceAppliedFor__c, 
                        ccrz__InvoiceAppliedTo__c = paymentDetail.ccrz__InvoiceAppliedTo__c, 
                        ccrz__ParentTransactionPayment__c = paymentDetail.Id, 
                        ccrz__PaymentType__c = paymentDetail.ccrz__PaymentType__c, 
                        ccrz__RequestAmount__c = paymentDetail.ccrz__RequestAmount__c, 
                        ccrz__SoldTo__c = paymentDetail.ccrz__SoldTo__c, 
                        ccrz__SourceTransactionPayment__c = paymentDetail.ccrz__SourceTransactionPayment__c, 
                        ccrz__StoredPayment__c = paymentDetail.ccrz__StoredPayment__c, 
                        ccrz__Storefront__c = paymentDetail.ccrz__Storefront__c, 
                        ccrz__SubAccountNumber__c = paymentDetail.ccrz__SubAccountNumber__c,
                        ccrz__Token__c = paymentDetail.ccrz__Token__c, 
                        ccrz__User__c = paymentDetail.ccrz__User__c
                        //ccrz__VerificationCode__c = paymentDetail.ccrz__VerificationCode__c
                    );
                return  newPaymentDetail;       
    }
}