/**
* Trigger Handler for CoachingSessionRecords.trigger
* Managing (Creating, Deleting) Coaching Session records based on class records information,
* additional comments on the develop branch
* Objects referenced --> Class__c, Coaching_Session__c
*
* @Date: 2016-12-08
* @Author Pranay Mistry (Magnet 360)
* @JIRA: https://reside.jira.com/browse/HPRP-692
*/

public class CoachingSessionsTriggerHandler {
	//initialize the HolidayUtility Object
	//public static HolidayUtility checkHoliday = new HolidayUtility();
		
	/**
    *  flagCoachingSession
    *  static function to flag coaching session records with a holiday conflict or not
    *  only called when there is a change in the coaching session class date and when
    *  a new coaching session record is created or an existing one is modified. called from handler method
    *  
    *  @param Coaching Session record --> the newly created or updated coaching session record with change in Class Date
    *  @param Integer flag --> 0 indicates insert, 1 indicates update
    *  @return void
    */	
	public static void flagCoachingSession(Coaching_Session__c coachingSession, Integer flag) {
		//initialize the holiday Name String
		String holidayNameStr = HolidayUtility.isHoliday(coachingSession.hmr_Class_Date__c);		
		try {
			//check for empty holiday name string
			if(holidayNameStr != ''){
				//set the Holiday Conflict checkbox field to true
                coachingSession.hmr_Holiday_Conflict__c = true;
                //populate holiday name string in Holiday name field
                coachingSession.hmr_Holiday_Name__c = holidayNameStr;
        	}
        	else {
    			//if the trigger is an update trigger and 
    			//holiday name string in empty
    			if(flag == 1) {
    				//reset the holiday conflict checkbox and empty the Holiday name field
    				coachingSession.hmr_Holiday_Conflict__c = false;
            		coachingSession.hmr_Holiday_Name__c = '';	
    			}        				        		
        	}
		}
		catch(Exception ex) {
			System.debug(ex.getMessage());
		}    		
	}

	/**
    *  handleBeforeInsert
    *  static function --> trigger handler for insert trigger
    *  
    *  @param List of new Coaching Session records 
    *  @param Integer flag --> 0 indicates insert, 1 indicates update
    *  @return void
    */	
	public static void handleBeforeInsert(List<Coaching_Session__c> newCoachingSessions, Integer flag) {
		//loop through the new coaching session records 
		for(Coaching_Session__c coachingSession: newCoachingSessions) {
			//flag the new coaching sessions for holiday
			flagCoachingSession(coachingSession, flag);
		}
	}

	/**
    *  handleBeforeUpdate
    *  static function --> trigger handler for update trigger
    *  
    *  @param List of updated Coaching Session records 
    *  @param Map of old version of updated Coaching Session Records
    *  @return void
    */	
	public static void handleBeforeUpdate(List<Coaching_Session__c> newCoachingSessions, Map<Id, Coaching_Session__c> oldCoachingSessionVersionsMap, Integer flag) {
		//Set of Coaching Session IDs with updated dates or start times
		Set<Id> changedIds = new Set<Id>();
		//Set of Coach Ids with updated dates or start times
		Set<Id> coaches = new Set<Id>();
		//Set of Class Dates for classes were the Date or Start time have been updated
		Set<Date> changedDates = new Set<Date>();

		for(Coaching_Session__c coachingSession: newCoachingSessions) {			
	        Coaching_Session__c getOldVersionOfCoachingSession = oldCoachingSessionVersionsMap.get(coachingSession.Id);
	        //verify if there is a change in the Class date field or Start Time field for the coaching session record 
	        if((getOldVersionOfCoachingSession.hmr_Class_Date__c != coachingSession.hmr_Class_Date__c || (getOldVersionOfCoachingSession.hmr_Start_Time__c != coachingSession.hmr_Start_Time__c)) && coachingSession.hmr_Coach__c != null && coachingSession.hmr_Class_Date__c != null) {
	            //Add Coaching Session Id, Coach Id, Class Date to appropriate sets
	            changedIds.add(coachingSession.Id);
	            coaches.add(coachingSession.hmr_Coach__c);
	            changedDates.add(coachingSession.hmr_Class_Date__c);                    
	        }            
		}
		//Retrieve list of exisitng coaching sessions with the following conditions
		//Coaching session is not one that is currently being updated
		//Class Date matches date in changedDates set
		//Coach is in the set of Coaches with updated classes
		List<Coaching_Session__c> existingSessions = [SELECT Id, Name, hmr_Class_Date__c, hmr_Coach__c, hmr_Start_Time__c 
														FROM Coaching_Session__c WHERE Id NOT IN :changedIds 
                											AND hmr_Class_Date__c IN :changedDates AND hmr_Coach__c IN :coaches];

		

		//loop through the updated Coaching session records
		for(Coaching_Session__c coachingSession: newCoachingSessions) {			
	        Coaching_Session__c getOldVersionOfCoachingSession = oldCoachingSessionVersionsMap.get(coachingSession.Id);
	        //verify if there is a change in the Class date field or Start Time field for the coaching session record 
	        if(getOldVersionOfCoachingSession.hmr_Class_Date__c != coachingSession.hmr_Class_Date__c || getOldVersionOfCoachingSession.hmr_Start_Time__c != coachingSession.hmr_Start_Time__c) {
	            //loop through existing sessions list
	            for(Coaching_Session__c session : existingSessions){
	            	//if a coaching session with the same coach is on the same date at a time that conflicts with the record being update, add an error and stop the process
	        		if(coachingSession.id != session.Id && coachingSession.hmr_Class_Date__c == session.hmr_Class_Date__c && coachingSession.hmr_Coach__c == session.hmr_Coach__c && TimeConversionUtility.isTimeConflict(coachingSession.hmr_Start_Time__c, session.hmr_Start_Time__c) == True){
	        			coachingSession.addError(coachingSession.hmr_Coach_Name__c + ' has a time conflict with a class on ' + session.hmr_Class_Date__c.format() + ' at ' +  session.hmr_Start_Time__c);
	        		}
				}
				if(getOldVersionOfCoachingSession.hmr_Class_Date__c != coachingSession.hmr_Class_Date__c){
					//if there is a change in the Class date for the coaching session record
					//then flag them for holiday
					flagCoachingSession(coachingSession, flag);	  					
				}				                  
	        }           
		}
	}

	
	/**
    *  handleAfterEvents
    *  static function --> trigger handler for after insert and update trigger
    *  on the Coaching Sesssion Object to create Session Attendee Records
    *  
    *  @param List of updated Coaching Session records 
    *  @return void
    */	
	public static void handleAfterEvents(List<Coaching_Session__c> newUpdatedCSs) {
		//Delcare the set of Parent Class Ids & Coaching Session Ids
		Set<Id> csClassIds = new Set<Id>();
		Set<Id> csIds = new Set<Id>();

		//loop through all the Coaching Session Records in Trigger.New
		for(Coaching_Session__c newUpdatedCS: newUpdatedCSs) {
			//Only process session attendee records for future Coaching Session Records
			if(newUpdatedCS.hmr_Class_Date__c >= Date.today()) {
				//add the Coaching Session Id to the Coaching Session Id Set
				//these set of id's will be used to filter the Session Attendee Records in the Map below
				csIds.add(newUpdatedCS.Id);
				//only add the Parent Class Id once in the set
				//if there are 5 Coaching Sessions created for the same Parent Class
				//no need to add the Parent Class Id in the set for all 5
				if(!csClassIds.contains(newUpdatedCS.hmr_Class__c)) 
					csClassIds.add(newUpdatedCS.hmr_Class__c);	
			}			
		}

		//get the 'Active' Class Membership records for all the parent class 
		Map<Id, Class_Member__c> classMemberships = new Map<Id, Class_Member__c>(
			[SELECT Id, Name, hmr_Class__c, hmr_Active__c, hmr_Contact_Name__c FROM Class_Member__c 
				WHERE hmr_Class__c In :csClassIds AND hmr_Active__c = TRUE]);

		
        //Set of contact Id's and Session Attendee Ids, used to get class member contacts map
        Set<Id> classMemberContactIds = new Set<Id>();
		Set<Id> clMemForSessAttIds = new Set<Id>();

		//loop through all the Class Membership records list and add the Contact Ids to the Contact Set
		//Session Attendee Ids to the Session Attendee Set
		for(Class_Member__c clMembship: classMemberships.values()) {
			classMemberContactIds.add(clMembship.hmr_Contact_Name__c);                                        
			clMemForSessAttIds.add(clMembship.Id);
		}

		//Map for Class Member Contact records, to verify if the client has signed the consent
        Map<Id, Contact> classMemberContacts = new Map<Id, Contact>(
            [SELECT Id, Name, hmr_Consent_Form__c FROM Contact WHERE Id In :classMemberContactIds 
                AND hmr_Consent_Form__c = TRUE]); 

        //Map for existing Session Attendees records related to class member records
        //This will be used to verify if we need to create Session Attendee records
		Map<Id, Session_Attendee__c> sessionAttendees = new Map<Id, Session_Attendee__c>(
			[SELECT Id, Name, hmr_Class_Member__c, hmr_Coaching_Session__c FROM Session_Attendee__c 
				WHERE hmr_Class_Member__c In :clMemForSessAttIds AND hmr_Coaching_Session__c In :csIds]);

		System.debug('Size of Session Attendees Map is ' + sessionAttendees.values().size());

		//initialize the session attendee list to insert
		List<Session_Attendee__c> sessionAttendeesToInsert = new List<Session_Attendee__c>();

		//loop through all the Coaching Session records
		for(Coaching_Session__c newUpdatedCS: newUpdatedCSs) {
			//loop through all the Class Membership records 
			for(Class_Member__c clMembship: classMemberships.values()) {
				//if Coaching Session and Class Membership belong to the same class
				if(newUpdatedCS.hmr_Class__c == clMembship.hmr_Class__c) {
					//if there are no session attendee records 
					if(sessionAttendees.values().size() == 0) {						
						//get the client contact record to check for the Consent Form Signed 
                        //the contact record is feteched from the Contact Map, and from class member record
                        Contact classMemberContact = classMemberContacts.get(clMembship.hmr_Contact_Name__c);
                        //check if contact is not null, this will again validate if the consent form was signed by the client
                        //only then the contact cannot be null
                        if(classMemberContact != null) {
                            //add session attendee sObject to Session Attendees List to Insert
                            Session_Attendee__c sessionAttendeeToInsert = new Session_Attendee__c(                                
                                hmr_Coaching_Session__c = newUpdatedCS.Id,
                                hmr_Class_Member__c = clMembship.Id,
                                hmr_Client_Name__c = classMemberContact.Id
                            );
                            //add session attendee sObject to Session Attendees List to Insert
                            sessionAttendeesToInsert.add(sessionAttendeeToInsert);                                                       
                        }						
					}					
				}
			}
		}

		try {      
			//DML insert on the Session Attendees List
			if(!sessionAttendeesToInsert.isEmpty())    
                insert sessionAttendeesToInsert;        	
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }	
	}
}