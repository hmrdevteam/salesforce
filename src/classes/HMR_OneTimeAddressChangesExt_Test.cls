/*****************************************************
 * Author: Utkarsh Goswami
 * Created Date: 06/23/2017
 * Created By: Utkarsh Goswami
 * Last Modified Date:
 * Last Modified By:
 * Description: Test Coverage for HMR_OneTimeAddressChangesExt class
 * ****************************************************/
@isTest
private class HMR_OneTimeAddressChangesExt_Test{


  @isTest
  private static void testMethodForSelectAddress(){


        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id, MailingState = 'CA');
        insert con;

        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

       insert userTest;

      System.runAs(userTest){

          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card',ccrz__User__c = userTest.id);
          insert orderObj;

          ccrz__E_AccountAddressBook__c accountAddressBook = new ccrz__E_AccountAddressBook__c(Name = 'test', ccrz__AddressType__c = 'Shipping');
          insert accountAddressBook;

          ccrz__E_ContactAddr__c contactAdr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'firstName', ccrz__LastName__c = 'lastName', ccrz__AddressFirstline__c = 'test', ccrz__City__c = 'test');
          insert contactAdr;

          List<ccrz__E_AccountAddressBook__c> addressList = new List<ccrz__E_AccountAddressBook__c>();
          addressList.add(accountAddressBook);

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeAddressChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeAddressChangesExt addressExtController = new HMR_OneTimeAddressChangesExt(sc);

          addressExtController.order = orderObj;
          addressExtController.addressList = addressList;
          addressExtController.orderId = orderObj.id;
          addressExtController.selectedAddress = contactAdr.id;

          addressExtController.selectAddress();

          PageReference cancel = addressExtController.cancel();

          System.assert(!String.isBlank(String.valueOf(cancel)));

      }

  }

  @isTest
  private static void testMethodForUpdateOrder(){

       Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id, MailingState = 'CA');
        insert con;

        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

       insert userTest;

      System.runAs(userTest){

          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card',ccrz__User__c = userTest.id);
          insert orderObj;

          ccrz__E_AccountAddressBook__c accountAddressBook = new ccrz__E_AccountAddressBook__c(Name = 'test', ccrz__AddressType__c = 'Shipping');
          insert accountAddressBook;

          ccrz__E_ContactAddr__c contactAdr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'firstName', ccrz__LastName__c = 'lastName', ccrz__AddressFirstline__c = 'test', ccrz__City__c = 'test');
          insert contactAdr;

          List<ccrz__E_AccountAddressBook__c> addressList = new List<ccrz__E_AccountAddressBook__c>();
          addressList.add(accountAddressBook);

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeAddressChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeAddressChangesExt addressExtController = new HMR_OneTimeAddressChangesExt(sc);


          Test.setCurrentPageReference(new PageReference('HMR_OneTimeAddressChanges'));
          System.currentPageReference().getParameters().put('addressId', contactAdr.id);

          addressExtController.order = orderObj;
          addressExtController.addressList = addressList;
          addressExtController.orderId = orderObj.id;
          addressExtController.selectedAddress = contactAdr.id;

          PageReference updateOrder = addressExtController.updateOrder();

          System.assert(!String.isBlank(String.valueOf(updateOrder)));

      }

  }


   @isTest
  private static void testMethodForUpdateOrderNegative(){

          ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card');
          insert orderObj;

          Test.setCurrentPageReference(new PageReference('HMR_OneTimeAddressChanges'));
          System.currentPageReference().getParameters().put('id', orderObj.id);

          ccrz__E_Order__c order = new ccrz__E_Order__c();

          ApexPages.StandardController sc = new ApexPages.StandardController(order);

          HMR_OneTimeAddressChangesExt addressExtController = new HMR_OneTimeAddressChangesExt(sc);


          PageReference updateOder = addressExtController.updateOrder();

          System.assert(String.isBlank(String.valueOf(updateOder)));

  }

  @isTest
  private static void testMethodForUpdateAddress(){


        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc;

        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id, MailingState = 'CA');
        insert con;

        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

       insert userTest;

      System.runAs(userTest){

        //   ccrz__E_Order__c orderObj = new ccrz__E_Order__c(Confirmation_Emailed__c = TRUE,ccrz__PaymentMethod__c='card',ccrz__User__c = userTest.id);
        //   insert orderObj;

          ccrz__E_ContactAddr__c contactAdr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'firstName', ccrz__LastName__c = 'lastName', ccrz__AddressFirstline__c = 'test', ccrz__City__c = 'test');
          insert contactAdr;

          ccrz__E_AccountAddressBook__c accountAddressBook = new ccrz__E_AccountAddressBook__c(Name = 'test', ccrz__AddressType__c = 'Shipping', ccrz__E_ContactAddress__c = contactAdr.Id);
          insert accountAddressBook;

          List<ccrz__E_AccountAddressBook__c> addressList = new List<ccrz__E_AccountAddressBook__c>();
          addressList.add(accountAddressBook);

          Test.setCurrentPageReference(new PageReference('HMR_AddressEditSelector'));
          System.currentPageReference().getParameters().put('id', con.id);
          System.currentPageReference().getParameters().put('addressId', accountAddressBook.Id);

          Contact cont = new Contact();

          ApexPages.StandardController sc = new ApexPages.StandardController(cont);

          HMR_AddressEditExt controller = new HMR_AddressEditExt(sc);

          controller.contactId = con.Id;
          controller.selectAddress();
          controller.goToEditPage();

          Test.setCurrentPageReference(new PageReference('HMR_AddressEditPage'));
          System.currentPageReference().getParameters().put('id', con.id);
          System.currentPageReference().getParameters().put('addressId', accountAddressBook.Id);

          controller.selectedAddressId = accountAddressBook.Id;
          controller.loadAddressBooks();
          controller.selectedAddressBook = accountAddressBook;
          accountAddressBook.ccrz__Default__c = true;
          controller.selectedContactAddress = contactAdr;
          contactAdr.ccrz__AddressFirstline__c = 'updated';
          controller.updateAddress();


          PageReference cancel = controller.cancel();

          System.assert(!String.isBlank(String.valueOf(cancel)));

      }

  }

}