/**
* @Date: 5/23/2017
* @Author: Utkarsh Goswami(Mindtree)
* @JIRA: 
* @Modified: 
* 
* Class HMR_CC_StoredPayment_Controller
*/


@isTest
private with sharing class HMR_CC_StoredPayment_Controller_Test{
/******************************************************************************
 * Test the constructor for the CartDetail page override
 */
    static testMethod void constructor_Test(){
        
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'tet123', email='test123@noemlail.com',
                emailencodingkey='UTF-8', lastname='Testiing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemlail.com');
       
        insert userTest;
        
       
        Contact testcontact = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id,Community_User_ID__c = userTest.id);
        insert testcontact;  
        
        System.debug('contact ->  ' + testcontact);
        
        ccrz__E_ContactAddr__c testContctAddr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testafirst', ccrz__LastName__c = 'testslast', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
        insert testContctAddr;
        
        
        ccrz__E_AccountAddressBook__c testAccAddress = new ccrz__E_AccountAddressBook__c(Name = 'test',ccrz__AccountId__c = testAcc.id, OwnerId = testcontact.Community_User_ID__c,
                                                        ccrz__AddressType__c = 'Billing' );
                                                        
        insert testAccAddress;
        
              
          System.runAs(userTest){
        
            Map<String, Object> addressMap =  HMR_CC_StoredPayment_Controller.getAddressBookDetails(testcontact);  
            
            System.assert(addressMap.size() > 0);
        
        }
    }
    
    static testMethod void testMethodFetchAddressBook(){
    
    
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'tet123', email='test123@noemlail.com',
                emailencodingkey='UTF-8', lastname='Testiing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemlail.com');
       
        insert userTest;
        
       
        Contact testcontact = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id,Community_User_ID__c = userTest.id);
        insert testcontact;  
        
        System.debug('contact ->  ' + testcontact);
        
        ccrz__E_ContactAddr__c testContctAddr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testafirst', ccrz__LastName__c = 'testslast', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
        insert testContctAddr;
        
        
        ccrz__E_AccountAddressBook__c testAccAddress = new ccrz__E_AccountAddressBook__c(Name = 'test',ccrz__AccountId__c = testAcc.id, OwnerId = testcontact.Community_User_ID__c,
                                                        ccrz__AddressType__c = 'Billing' );
                                                        
        insert testAccAddress;
        
              
        System.runAs(userTest){
        
            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
            
            Map<String,String> userAddress = new Map<String, String>();
            userAddress.put('bill_to_forename', 'first');  
            userAddress.put('bill_to_surname', 'testCon');
            userAddress.put('bill_to_email', 'test@test.com');
            
             Map<String,String> billingAddr = new Map<String, String>();
            billingAddr.put('bill_to_address_line1', 'addr 1');  
            billingAddr.put('bill_to_address_line2', 'addr 2');
            billingAddr.put('bill_to_address_city', 'test city');
            billingAddr.put('bill_to_address_state', 'test state');  
            billingAddr.put('bill_to_address_postal_code', '1122334');
            billingAddr.put('bill_to_address_state_code', '12314');
            
            Map<String,String> unSigned = new Map<String, String>();
            unSigned.put('test', 'test');
            
          //  Test.setMock(HttpCalloutMock.class, new StoredPaymentMock());
          //  ccrz.cc_RemoteActionResult ctxRest = HMR_CC_StoredPayment_Controller.CreatePaymentToken(ctx,userAddress,unSigned);
    
          ccrz.cc_RemoteActionResult addrBook = HMR_CC_StoredPayment_Controller.fetchAddressBook(ctx, con.Id );
          
             System.assert(!String.isBlank(String.valueOf(addrBook)));
          
          }
      }
      
      static testMethod void testMethodFetchContactAddressForStoredPayment(){
    
    
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'tet123', email='test123@noemlail.com',
                emailencodingkey='UTF-8', lastname='Testiing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemlail.com');
       
        insert userTest;
        
       
        Contact testcontact = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id,Community_User_ID__c = userTest.id);
        insert testcontact;  
        
        System.debug('contact ->  ' + testcontact);
        
        ccrz__E_ContactAddr__c testContctAddr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testafirst', ccrz__LastName__c = 'testslast', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
        insert testContctAddr;
        
        
        ccrz__E_AccountAddressBook__c testAccAddress = new ccrz__E_AccountAddressBook__c(Name = 'test',ccrz__AccountId__c = testAcc.id, OwnerId = testcontact.Community_User_ID__c,
                                                        ccrz__AddressType__c = 'Billing' );
                                                        
        insert testAccAddress;
        
              
        System.runAs(userTest){
              
              ccrz__E_ContactAddr__c testContctAddr2 = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testfirst', ccrz__LastName__c = 'testlast', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
              insert testContctAddr2;
              
              ccrz__E_StoredPayment__c storePayment = new ccrz__E_StoredPayment__c(Name = 'test', HMR_Contact_Address__c =  testContctAddr2.id, ccrz__AccountNumber__c = '123516613311141414',
                                                          ccrz__ExpMonth__c = 6, ccrz__PaymentType__c = 'Card');
              insert storePayment;
              
             ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();  
              
              ccrz.cc_RemoteActionResult storePaymentResp = HMR_CC_StoredPayment_Controller.fetchContactAddressForStoredPayment(ctx, storePayment.Id);
              
              System.assert(!String.isBlank(String.valueOf(storePaymentResp)));
              
           }
       }  
       
       static testMethod void testMethodcreateStoredPayment(){
    
        
            Id p = [select id from profile where name='HMR Customer Community User'].id;
            Account testAcc = new Account(Name = 'testAcc');
            insert testAcc; 
           
            Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
            insert con;  
                      
            User userTest = new User(alias = 'tet123', email='test123@noemlail.com',
                    emailencodingkey='UTF-8', lastname='Testiing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemlail.com');
           
            insert userTest;
            
           
            Contact testcontact = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id,Community_User_ID__c = userTest.id);
            insert testcontact;  
            
            System.debug('contact ->  ' + testcontact);
            
            ccrz__E_ContactAddr__c testContctAddr = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testafirst', ccrz__LastName__c = 'testslast', ccrz__AddressFirstline__c = 'test addr',
                                                    ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
            insert testContctAddr;
            
            
            ccrz__E_AccountAddressBook__c testAccAddress = new ccrz__E_AccountAddressBook__c(Name = 'test',ccrz__AccountId__c = testAcc.id, OwnerId = testcontact.Community_User_ID__c,
                                                            ccrz__AddressType__c = 'Billing' );
                                                            
            insert testAccAddress;
            
            Map<String,String> billingAddr = new Map<String, String>();
            billingAddr.put('bill_to_address_line1', 'addr 1');  
            billingAddr.put('bill_to_address_line2', 'addr 2');
            billingAddr.put('bill_to_address_city', 'test city');
            billingAddr.put('bill_to_address_state', 'test state');  
            billingAddr.put('bill_to_address_postal_code', '1122334');
            billingAddr.put('bill_to_address_state_code', '12314');
            
            ccrz__E_ContactAddr__c testContctAddr2 = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'testfirst', ccrz__LastName__c = 'testlast', ccrz__AddressFirstline__c = 'test addr',
                                                ccrz__City__c = 'test',ccrz__Country__c = 'test country', ccrz__PostalCode__c = '112244', ccrz__CountryISOCode__c = '11122');
              insert testContctAddr2;
            
            ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
                  
            System.runAs(userTest){
                  
                  ccrz.cc_RemoteActionResult billingAdd = HMR_CC_StoredPayment_Controller.addBillingAddress(ctx,'test','test last',con.Id,true,billingAddr);
                  
                  
                   Map<String,String> paymentInfo = new Map<String, String>();
                    paymentInfo.put('contactAddressId', testContctAddr2.id);  
                    paymentInfo.put('contactFirstName', 'test contact');
                    paymentInfo.put('contactLastName', 'test last');
                    paymentInfo.put('contactAddressId', testContctAddr2.id);  
                    paymentInfo.put('displayName', 'test display');
                    paymentInfo.put('accountNumber', '123516613311141414');
                    paymentInfo.put('token', 'test');  
                    paymentInfo.put('expirationMonth', '6');
                    paymentInfo.put('expirationYear', '1992');
                    paymentInfo.put('paymentType', 'card');
    
                    ccrz.cc_RemoteActionResult storedPayment = HMR_CC_StoredPayment_Controller.createStoredPayment(ctx, paymentInfo);
                    
                    System.assert(!String.isBlank(String.valueOf(storedPayment)));
              
              }
    
        }
}