/**
* Apex Service Class for the Program Membership
*
* @Date: 2017-05-04
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-02-12
* @JIRA: 
*/
public with sharing class HMR_CC_ProgramMembership_Service {
    public HMR_CC_ProgramMembership_Service(){}
    
    /**
     * method to retrieve indication of whether the diet start date can be updated for the respective contact
     * @param contactId - the related contact record id 
     * @param programMembershipId - the related program membership id
     * @return boolean - true if the diet start date can be updated else false
    */
    public boolean canUpdateDietStartDate(string contactId, string programMembershipId){
        boolean canUpdateDietStartDate = true;
        integer pendingCount = 0;
        integer completedReorderCount = 0;

        //Only allow updates to diet start when there is 1 pending order and no completed orders with order type containing 'Reorder'
        List<ccrz__E_Order__c> orderList = HMR_CC_Order_Selector.getPendingAndCompletedByContactIdAndProgramMembershipId(contactId, programMembershipId);
        for(ccrz__E_Order__c order : orderList){
            if(order.ccrz__OrderStatus__c == 'Pending')
                pendingCount++;
            else if(order.hmr_Order_Type_c__c.containsIgnoreCase('reorder'))
                completedReorderCount++;
        }

        return pendingCount == 1 && completedReorderCount == 0;
    }

    /**
     * method to retrieve the minimum date that activity can be logged for the client (which is their first enrollment date)
     * @param contactId - the related contact record id 
     * @return date - the minimum date activity can be logged for, or today as default if no historical program memberships exist
    */
    public Date getMinDietDate(string contactId){
        Date minimumDate = Date.today();
        List<AggregateResult> aggregateResultList = [SELECT MIN(hmr_Enrollment_Date__c) minimumDate FROM Program_Membership__c WHERE hmr_Contact__c =: contactId];

        if(aggregateResultList.size() > 0 && aggregateResultList[0].get('minimumDate') != null){
            minimumDate = (Date)aggregateResultList[0].get('minimumDate');
        }

        return minimumDate;    
    }
    
    /**
     * method to retrieve the current/active diet start date for the respective client
     * @param contactId - the related contact record id 
     * @return date - the diet start date for the contact or null if not found
    */
    public Date getDietStartDate(string contactId){
        Date dietStartDate = null;
        List<Program_Membership__c> programMembershipList = [SELECT hmr_Diet_Start_Date__c  
                                                             FROM Program_Membership__c      
                                                             WHERE hmr_Status__c = 'Active' AND hmr_Contact__c = :contactId];

        if(programMembershipList.size() > 0){
            if(programMembershipList[0].hmr_Diet_Start_Date__c != null)
                dietStartDate = programMembershipList[0].hmr_Diet_Start_Date__c;
            else
            {
                //Determine from pending order
                ccrz__E_Order__c orderPendingRecord = HMR_CC_Order_Selector.getPendingByContactId(contactId);

                if(orderPendingRecord != null)
                    dietStartDate  = orderPendingRecord.ccrz__OrderDate__c;
                else{
                    //Update the Template Record
                    ccrz__E_Order__c orderTemplateRecord = HMR_CC_Order_Selector.getTemplateByContactIdAndProgramMembershipId(contactId, programMembershipList[0].Id);
                    if(orderTemplateRecord != null)
                        dietStartDate  = orderTemplateRecord.ccrz__OrderDate__c;  
                }
            }
        }

        return dietStartDate;
    }
    
    /**
     * method to update the diet start date for the respective client
     * @param contactId - the related contact record id 
     * @param programMembershipId - the related program membership id
     * @param dietStartDate - the new diet start date
     * @return null
    */
    public void updateDietStartDate(string contactId, string programMembershipId, date dietStartDate){
        //Update the Diet Start Date on the Program Membership Record
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Id = programMembershipId, hmr_Diet_Start_Date__c = dietStartDate, hmr_Diet_Start_Date_Changed__c = true);
        update programMembershipRecord;
        
        //Determine if there is a pending order to update, otherwise update the template record
        ccrz__E_Order__c orderPendingRecord = HMR_CC_Order_Selector.getPendingByContactId(contactId);
        if(orderPendingRecord != null){
            orderPendingRecord.ccrz__OrderDate__c = dietStartDate;
            update orderPendingRecord;
        }
        else{
            //Update the Template Record
            ccrz__E_Order__c orderTemplateRecord = HMR_CC_Order_Selector.getTemplateByContactIdAndProgramMembershipId(contactId, programMembershipId);
            orderTemplateRecord.ccrz__OrderDate__c = dietStartDate;
            update orderTemplateRecord;
        }
    }

    /**
     * method to get the program membership record for the date specified
     * @param contactId - the related contact record id 
     * @param membershipDate - the related date 
     * @return Program_Membership__c - the program membership record at that time or null if none existed
    */
    public Program_Membership__c getProgramMembership(string contactId, date membershipDate){
        List<Program_Membership__c> programMembershipList = [SELECT DietType__c,
                                                                    hmr_Day_of_Week__c,
                                                                    hmr_Diet_Start_Date__c,
                                                                    hmr_Enrollment_Date__c,
                                                                    Program__r.Name
                                                             FROM Program_Membership__c
                                                             WHERE hmr_Contact__c =: contactId
                                                             AND hmr_Enrollment_Date__c <=: membershipDate
                                                             ORDER BY hmr_Enrollment_Date__c DESC,
                                                                      CreatedDate DESC,
                                                                      hmr_Status__c DESC
                                                             ];

        return programMembershipList.size() > 0 ? programMembershipList[0] : null;
    }
}