/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_Questions_List_ContentTemplate
*/

@isTest //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_ProfessionalHero_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_ProfessionalHero_ContentTemplate pHeroTemplate = new HMR_CMS_ProfessionalHero_ContentTemplate();
    String propertyValue = pHeroTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    pHeroTemplate.testAttributes = tab;

    string s2 = pHeroTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = pHeroTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');

    List<String> ls = pHeroTemplate.getLeadHMRSettingPickListValues();

	string s4 = pHeroTemplate.returnProfessionalsLeadCreationModal();

	string s3 = pHeroTemplate.returnStatePickListOptions();

    String renderHTML = pHeroTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    map<string, string> param = new map<string, string>();
    param.put('action','insertProfessionalsPageFormLead');
    param.put('FirstName','professionalsFN');
    param.put('LastName','professionalsLN');
    param.put('Title','professionalsJT');
    param.put('Email','professionalsEmail');
    param.put('Company','professionalsCompany');
    param.put('Phone','professionalsPhone');
    param.put('Stree','professionalsAddress1');
    param.put('City','professionalsCity');
    param.put('State','professionalsState');
    param.put('PostalCode','professionalsZipCode');
    param.put('hmr_Setting__c','professionalsHMRSetting');

    HMR_CMS_ProfessionalHero_ContentTemplate.executeRequest(param);


    try{

    	cms.CoreController ccc= new cms.CoreController();
        HMR_CMS_ProfessionalHero_ContentTemplate qc = new HMR_CMS_ProfessionalHero_ContentTemplate(ccc);
    }catch(Exception e){}

    //try{
    //    HMR_CMS_ProfessionalHero_ContentTemplate qc2= new HMR_CMS_ProfessionalHero_ContentTemplate(new cms.createContentController());
    //}catch(Exception e){}


    try{
        HMR_CMS_ProfessionalHero_ContentTemplate.getType();
    }catch(Exception e){}

  }
  
}