/**
* Apex Content Template Controller for Primary Nav inside header on OCMS Master
*
* @Date: 2017-03-12
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_HeaderSignIn_ContentTemplate extends cms.ContentTemplateController{

	//need two constructors, 1 to initialize CreateContentController a
	global HMR_CMS_HeaderSignIn_ContentTemplate(cms.CreateContentController cc) {
        super(cc);
    }
    //2 no ARG constructor
    global HMR_CMS_HeaderSignIn_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }   
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String DisplayLabel{    
        get{
            return getPropertyWithDefault('DisplayLabel', 'Sign In');
        }    
    }

    public String DisplayIconImage{
        get{
            return getProperty('DisplayIconImage');
        }
    }

    public String exString { get; set; } 

    // This method generates the markup for the shopping cart in the header
    global virtual override String getHTML() {    
        String html = '';
        String sitePrefix = Site.getBaseUrl();
        String userId = UserInfo.getUserId();
        String userProfileId = UserInfo.getProfileId();
        Id guestProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile'].Id;
        //String displayString = '';
        String anchorString = '<a id="hmrSignIn" href="';
        String iconString = '<span class="hmr-icon-user" style= "background-image : url(\'' + sitePrefix + DisplayIconImage + '\')" aria-hidden="true"></span><span id="hdr_user_fname" class="hidden-xxs hidden-sm">';
        String endString = '</span></a>';
        String targetUrl = '';
        try {
            if(userProfileId != guestProfileId) {    
                targetUrl = 'ccrz__MyAccount';      
                //displayString = UserInfo.getFirstName();
            }
            else {
                targetUrl = 'login';      
                //displayString = DisplayLabel.escapeHtml4();
            }                        
        } 
        catch (Exception e) {
            // Error handling...
            exString = e.getMessage() + '<br/>' + e.getTypeName() + '<br/>' + e.getStackTraceString() + '<br/>' + e.getLineNumber();
            System.debug(e.getMessage());
        }        
        //html += '<li>' + anchorString + targetUrl + '">' + iconString + displayString + endString + '</li>';
        html += '<li>' + anchorString + targetUrl + '">' + iconString + endString + '</li>';
        return html;            
    }

}