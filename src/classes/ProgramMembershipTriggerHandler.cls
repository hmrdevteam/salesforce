/**
 * Trigger Handler class for ProgramMembership trigger
 * @Date: 11/28/2016
 * @Author: Ali Pierre (HMR)
 * @Modified: 03/09/2018 - Z. Engman - added after insert/update to avoid conflict with before logic and write/update activity log 
 *                                     for initial weight
 * @JIRA: https://reside.jira.com/browse/HPRP-577
*/

public class ProgramMembershipTriggerHandler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isAfterInsertFlag = false;
    public static Boolean isAfterUpdateFlag = false;

//Declare a list of program memberships to populate and insert
    public static List<Program_Membership__c> newPrgmMembershipToInsert = new List<Program_Membership__c>();
    //Declare a Map of Ids and Consent_Agreement__c within this trigger
    public static Map<Id, Consent_Agreement__c> consentMap = new Map<Id, Consent_Agreement__c>();
    //Declare a Map of Ids and Contact within this trigger
    public static Map<Id, Contact> contactMap = new Map<Id, Contact>();
    //Declare a Map of Ids and Class Member records belonging to contacts within this trigger
    public static Map<Id, Class_Member__c> classMembershipMap = new Map<Id, Class_Member__c>();
    //Declare a Map of Ids and Session Attendee records belonging to contacts within this trigger
    public static Map<Id, Session_Attendee__c> sessionAttendeesMap = new Map<Id, Session_Attendee__c>();
    //Declare a Map of String, and Ids of Programs within HMR
    public static Map<String, Id> prgNamesMap = new Map<String, Id>();
    //Declare a map of Id, User to store User Ids and Names of coaches for chatter posts
    public static Map<Id, User> CoachesMap = new Map<Id, User>();
    //Declare a map of Id, CollaborationGroup to store  Ids and Names of chatter groups for chatter posts
    public static Map<Id, CollaborationGroup> GroupMap = new Map<Id, CollaborationGroup>();
    //Declare a map of Id, Program Membership to store Digital PM records that are transitioning to a remote program via client checkout flow
    //These records will be updated but flagged to not result in creation of new PM record as that will have been created via checkout
    public static Map<Id, Program_Membership__c> digitalMembershipMap = new Map<Id, Program_Membership__c>();
    //Declare a list of ConnectApi.FeedItemInput to store chatter posts
    //public static List<ConnectApi.FeedItemInput> posts = new List <ConnectApi.FeedItemInput> ();
    //Declare list of FeedItem to store chatter posts to contact object
    //public static List<FeedItem> contactFeedPosts = new List<FeedItem>();

    public ProgramMembershipTriggerHandler(boolean executing, integer size) {
        this.isExecuting = executing;
        this.batchSize = size;
    }

    //Before insert check past membership records to determine whether a client is a 'Restart' or 'New' Client Type
    public static void OnBeforeInsert(Program_Membership__c[] prgMembers){
        //Declare a new set of Ids
        Set<Id> contactIds = new Set<Id>();
        Set<Id> programMembershipIds = new Set<Id>();

        //Loop through the trigger collection and add the contacts to contactIds and set each record's Client Type to 'New'
        for(Program_Membership__c prg : prgMembers) {
            contactIds.add(prg.hmr_Contact__c);
            prg.hmr_Client_Type__c = 'New';
        }

        //Query past membership records belonging to contacts in contactIds set
        List<Program_Membership__c> prgmMembershipList = new List<Program_Membership__c>(
            [SELECT Id, hmr_Contact__c, Program__c, ProgramName__c, Motivation__c, SupportNeeded__c, hmr_Status__c, New_Phase__c, hmr_Status_Change_Reason__c, ProfileBuildProcessComplete__c
             FROM Program_Membership__c WHERE hmr_Contact__c In :contactIds ORDER BY CreatedDate DESC]);
        //Loop through the records within the trigger collection
        for(Program_Membership__c curMem : prgMembers) {
            //Loop through the list of past memberships
            for(Program_Membership__c pastMem : prgmMembershipList){
                if(curMem.hmr_Contact__c == pastMem.hmr_Contact__c){
                    //pass through the Motivation and Support Needs fields
                    if(String.isBlank(curMem.Motivation__c)){
                        curMem.Motivation__c  = pastMem.Motivation__c;
                    }
                    if(String.isBlank(curMem.SupportNeeded__c)){
                        curMem.SupportNeeded__c  = pastMem.SupportNeeded__c;
                    }
                    //if the most recent active program is Digital, handle updating the PM and flagging to avoid duplication later
                    if(pastMem.hmr_Status__c == 'Active' && pastMem.ProgramName__c == 'Digital' && String.isBlank(pastMem.New_Phase__c)){
                        pastMem.New_Phase__c = curMem.ProgramName__c;
                        pastMem.hmr_Status_Change_Reason__c = 'Joined ' + curMem.ProgramName__c;
                        if(curMem.ProgramName__c == 'Phase 2') {
                            curMem.ProfileBuildProcessComplete__c = pastMem.ProfileBuildProcessComplete__c;
                        }
                        digitalMembershipMap.put(pastMem.Id, pastMem);
                    }
                    //If a contact has previously been in the pahse they are moving into, set their Client Type as 'Restart'
                    if(curMem.Program__c == pastMem.Program__c){
                        curMem.hmr_Client_Type__c = 'Restart';
                        //If client transitions to/from the same phase, carry over Profile Build Complete flag for Mobile App
                        // if(pastMem.hmr_Status__c == 'Active'){
                        //     curMem.ProfileBuildProcessComplete__c = pastMem.ProfileBuildProcessComplete__c;
                        // }
                    }
                }

            }
        }
        if(digitalMembershipMap.size() > 0){
            updatePastDigitals(digitalMembershipMap);
        }

    }

    public static void updatePastDigitals(Map<Id, Program_Membership__c> digitalPMs) {
        if(!digitalPMs.values().isEmpty()) {
            update digitalPMs.values();
        }
    }

    public static void handleTransitionMembers(List<Program_Membership__c> TransitionMembers,  Set<Id> programIds, Set<Id> contactIds, Set<Id> dropContactIds){
        //Fill CoachesMap with coaches from the user table
        for(User coach : [SELECT Id, Name FROM User WHERE hmr_IsCoach__c = True]){
            CoachesMap.put(Coach.id, coach);
        }
        //Fill GroupMap with Enrollment group details
        for(CollaborationGroup en : [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Enrollment']){
            GroupMap.put(en.id, en);
        }
        //Populate consentMap with clients who are dropping from AD
        for(Consent_Agreement__c consentAgreements : [SELECT Id, Contact__c, Status__c, Consent_Type__c, Version__c  FROM Consent_Agreement__c WHERE Contact__c In :dropContactIds AND (Status__c = 'Consented' OR Status__c = 'Notified')]){
            consentMap.put(consentAgreements.id, consentAgreements);
        }

        //Populate contactMap
        for(Contact c : [SELECT Id, Name, hmr_Class_Notes__c, hmr_New_Class_Note__c FROM Contact WHERE Id In :contactIds]) {
            contactMap.put(c.Id, c);
        }
        //Populate classMembershipMap with client's that are dropping or transitioning to P1-HSS
        for(Class_Member__c clsMember : [SELECT Id, Name, Coach_User_Id__c, hmr_Contact_Name__c, hmr_Active__c, hmr_Class_Type__c, hmr_Client_Drop_Reason__c
             FROM Class_Member__c WHERE hmr_Contact_Name__c In :contactIds]){
            classMembershipMap.put(clsMember.Id, clsMember);
        }
        //Populate sessionAttendeesMap with client's that are making a status change
        for(Session_Attendee__c session : [SELECT Id, hmr_Client_Name__c, hmr_Class_Date__c, hmr_Class_Name__c, hmr_Class_Member__r.hmr_Class_Type__c  FROM Session_Attendee__c
             WHERE hmr_Client_Name__c In :contactIds AND hmr_Class_Date__c >= :Date.today() AND hmr_Attendance__c = null]){
            System.debug('QDate!! ' + session.hmr_Class_Date__c);
            sessionAttendeesMap.put(session.Id, session);
        }
        //Populate program list
        for (Program__c p : [select Id, Name from Program__c]) {
                    prgNamesMap.put(p.Name, p.Id);
        }
        //Declare a list of CC Orders to be cancelled
        //List<ccrz__E_Order__c> ccOrdersToUpdate = new List<ccrz__E_Order__c>([SELECT Id, ccrz__OrderStatus__c, ccrz__OrderNumber__c   FROM ccrz__E_Order__c WHERE hmr_Program_Membership__c In :programIds AND  ccrz__OrderDate__c >= :Date.today() AND ccrz__OrderStatus__c NOT IN ('Completed','Shipped','Cancelled')]);

        List<ccrz__E_Order__c> ccOrdersToUpdate = new List<ccrz__E_Order__c>([SELECT Id, ccrz__OrderStatus__c, ccrz__OrderNumber__c   FROM ccrz__E_Order__c WHERE hmr_Program_Membership__c IN :programIds AND ccrz__OrderStatus__c = 'Pending']);

        //Update Order Status of orders to 'Cancelled'
        for (ccrz__E_Order__c Ord : ccOrdersToUpdate) {
                    Ord.ccrz__OrderStatus__c = 'Cancelled';
        }
        System.debug('@@@@ in loop'  + TransitionMembers.size());
        //Loop through the program memberships and make the appropriate changes based on the outlined conditions
        for(Program_Membership__c prg : TransitionMembers) {
            System.debug('@@@@ in loop');
            //Call handleTransition method which will return client's new status after proccessing change
            if(prg.ProgramName__c == 'P1 Healthy Solutions'){
                prg.hmr_Status__c = handleTransition(Prg, 'P1 Healthy Solutions', prg.New_Phase__c);
            }
            else if(prg.ProgramName__c == 'P1 Healthy Shakes'){
                prg.hmr_Status__c = handleTransition(Prg, 'P1 Healthy Shakes', prg.New_Phase__c);
            }
            else if(prg.ProgramName__c == 'Phase 2'){
                prg.hmr_Status__c = handleTransition(Prg, 'Phase 2', prg.New_Phase__c);
            }
            else if(prg.ProgramName__c == 'Digital'){
                prg.hmr_Status__c = handleTransition(Prg, 'Digital', prg.New_Phase__c);
            }
        }
        try{
            System.debug('Updating Session Attendees' + ' ' + sessionAttendeesMap.values());
            System.debug('Updating classMembershipMap' + ' ' + classMembershipMap.values());
            System.debug('Updating consentMap' + ' ' + consentMap.values());
            System.debug('Updating contactMap' + ' ' + contactMap.values());
            System.debug('Updating ccOrdersToUpdate' + ' ' + ccOrdersToUpdate.size());
            System.debug('Updating newPrgmMembershipToInsert' + ' ' + newPrgmMembershipToInsert.size());

            if(!sessionAttendeesMap.values().isEmpty())
                delete sessionAttendeesMap.values();

            if(!classMembershipMap.values().isEmpty())
                update classMembershipMap.values();

            if(!consentMap.values().isEmpty())
                update consentMap.values();

            if(!contactMap.values().isEmpty())
                update contactMap.values();

            if(!ccOrdersToUpdate.isEmpty())
                update ccOrdersToUpdate;

            if(!newPrgmMembershipToInsert.isEmpty())
                insert newPrgmMembershipToInsert;

            /*if(!contactFeedPosts.isEmpty())
                insert contactFeedPosts;*/
        }
        catch(DMLException e){
            System.debug('The following exception has occurred during DML statements: ' + e.getMessage());
        }
        //Post stored Chatter posts
        /*for(ConnectApi.FeedItemInput chat : posts){
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), chat);
        }*/
    }
    public static void OnBeforeUpdate(Program_Membership__c[] prgMembers, Map<Id, Program_Membership__c> cOS){
        //Declare a set of Ids to store Program membership record Ids
        Set<Id> programIds = new Set<Id>();
        //Declare a set of Ids to store Contact record Ids
        Set<Id> contactIds = new Set<Id>();
        //Declare a set of Ids to store Contact record Ids of clients that will
        //be dropping dropping from AD - will need to be dropped from PP and have consent dropped
        Set<Id> dropContactIds = new Set<Id>();
        //Declare list of program members transitioning
        List<Program_Membership__c> TransitionMembers = new List<Program_Membership__c>();
        //Loop through trigger collection to populate Id sets
        for(Program_Membership__c prg : prgMembers) {
             //If the client's New Phase field has been changed and their record is active add to Id lists
            if(prg.New_Phase__c != cOS.get(prg.Id).New_Phase__c && prg.hmr_Status__c == 'Active'){
                //Set Status Changed date to Today
                prg.hmr_Status_Changed_Date__c = Date.today();
                //Add records to Sets/Lists
                programIds.add(prg.Id);
                contactIds.add(prg.hmr_Contact__c);
                TransitionMembers.add(prg);
                if(prg.New_Phase__c == 'Drop'){
                    dropContactIds.add(prg.hmr_Contact__c);
                }
            }
        }

        if(!TransitionMembers.isEmpty()){
            handleTransitionMembers(TransitionMembers, programIds, contactIds, dropContactIds);
        }
    }

    public static String handleTransition(Program_Membership__c Prg, String oldPhase, String newPhase){
        //Declare a boolean varible that indicates whether a client's consent will be dropped and set it to true
        //before going through transition process
        DateTime dt = DateTime.now();
        System.debug(prg.New_Phase__c);
        //Drop Process
        if(prg.New_Phase__c == 'Drop'){
            //Set Status to Dropped
            prg.hmr_Status__c = 'Dropped';

            //Loop through the class memberships and find Active P1 PP Memberships belonging to this contact
            //If found, set the membership to inactive and dropConsent = True
            //If client does not have active P1 PP membership, set dropConsent = false and  remove their class membership record from the set
            if(!classMembershipMap.values().isEmpty()){
                for(Class_Member__c classMembership: classMembershipMap.values()){
                    if(classMembership.hmr_Contact_Name__c == prg.hmr_Contact__c){
                        classMembership.hmr_Active__c = false;
                        classMembership.hmr_Client_Drop_Reason__c = 'Dropped from AD';
                        classMembershipMap.put(classMembership.Id, classMembership);
                        //Create Chatter post to notify coach and Enrollment Group
                        //posts.add(ChatterUtil.ChatterToCoachesAndGroups('Client has been dropped from AD and removed from the Phone Program. '+ dt.format(), classMembership.Coach_User_Id__c, 'Enrollment', classMembership.hmr_Contact_Name__c, CoachesMap, GroupMap));
                    }
                }
            }
            //If consentMap is not empty and dropConsent for this client is = true,
            //set their consent status to 'Expired' in the Consent_Agreement__c object
            if(!consentMap.values().isEmpty()){
                for(Consent_Agreement__c consentRecord: consentMap.values()){
                    if(consentRecord.Contact__c == prg.hmr_Contact__c){
                        consentRecord.Status__c = 'Expired';
                        consentMap.put(consentRecord.Id, consentRecord);
                    }
                }
            }

            //If contactMap is not empty and dropConsent for this client is = true,
            //Clear Class Notes field and New Class Notes field
            if(!contactMap.values().isEmpty()){
                for(Contact contactRecord: contactMap.values()){
                    if(contactRecord.Id == prg.hmr_Contact__c){
                        contactRecord.hmr_Class_Notes__c = '';
                        contactRecord.hmr_New_Class_Note__c = false;
                        contactMap.put(contactRecord.Id, contactRecord);
                    }
                }
            }
            //Create Chatter post to notify coach and Enrollment Group
            //contactFeedPosts.add(ChatterUtil.postToClientFeed(prg.hmr_Contact__c, 'Client has been dropped from ' + oldPhase + ' due to the following reason: ' + Prg.hmr_Status_Change_Reason__c  + '.' + dt.format()));

            //Create new "Digital" Program Membership record if Contact has logged into mobile app
            //Added diet type field so new Digital PM record carries over Diet type
            if(prg.MobileLogin__c){
                Program_Membership__c newMembership = new Program_Membership__c(hmr_Contact__c = prg.hmr_Contact__c, Program__c = prgNamesMap.get('Digital'),
                                                                hmr_Status__c = 'Active', DietType__c = prg.DietType__c, hmr_Enrollment_Date__c = Date.today());
                newPrgmMembershipToInsert.add(NewMembership);
                //Create Chatter post for contact object
                //contactFeedPosts.add(ChatterUtil.postToClientFeed(prg.hmr_Contact__c, 'Client has transitioned from ' + oldPhase + ' to ' + newPhase + ' due to the following reason: ' + Prg.hmr_Status_Change_Reason__c  + '.' + dt.format()));
            }

        }
        //Tranasition to P1 HSAH
        else if(prg.New_Phase__c == 'P1 Healthy Solutions'){
            //Set Status to Transitioned
            prg.hmr_Status__c = 'Transitioned';
            //Loop through the Session Attendees Map and find records belonging to this contact. set hmr_Allow_Transition__c = true
            if(!classMembershipMap.values().isEmpty()){
                for(Class_Member__c classMembership: classMembershipMap.values()){
                    if(classMembership.hmr_Contact_Name__c == prg.hmr_Contact__c){
                        classMembership.hmr_Allow_Transition__c = true;
                        classMembershipMap.put(classMembership.Id, classMembership);
                        //Create Chatter post to notify coach and Enrollment Group
                        //posts.add(ChatterUtil.ChatterToCoachesAndGroups('Client has transitioned from ' + oldPhase + ' to ' + newPhase +' and has been removed the Phone Program. '+ dt.format(), classMembership.Coach_User_Id__c, 'Enrollment', classMembership.hmr_Contact_Name__c, CoachesMap, GroupMap));
                    }
                }
            }
            //Make sure the updated PM is not in the digitalMembershipMap before creating new PM record to avoid duplicates
            if(!digitalMembershipMap.containsKey(prg.Id)) {
                //Create new Program Membership record
                if(prgNamesMap.containsKey(newPhase)){
                    Program_Membership__c newMembership = new Program_Membership__c(hmr_Contact__c = prg.hmr_Contact__c, Program__c = prgNamesMap.get(newPhase),
                                                                    hmr_Status__c = 'Active', hmr_Enrollment_Date__c = Date.today());
                    newPrgmMembershipToInsert.add(NewMembership);
                    //Create Chatter post for contact object
                    //contactFeedPosts.add(ChatterUtil.postToClientFeed(prg.hmr_Contact__c, 'Client has transitioned from ' + oldPhase + ' to ' + newPhase + ' due to the following reason: ' + Prg.hmr_Status_Change_Reason__c  + '.' + dt.format()));
                }
            }
        }
            //Transition to P2 process
        else if(prg.New_Phase__c == 'Phase 2'){
            //Set Status to Transitioned
            prg.hmr_Status__c = 'Transitioned';
            //Loop through the Session Attendees Map and find records belonging to this contact. set hmr_Allow_Transition__c = true
            if(!classMembershipMap.values().isEmpty()){
                for(Class_Member__c classMembership: classMembershipMap.values()){
                    if(classMembership.hmr_Contact_Name__c == prg.hmr_Contact__c){
                        classMembership.hmr_Allow_Transition__c = true;
                        classMembershipMap.put(classMembership.Id, classMembership);
                        //Create Chatter post to notify coach and Enrollment Group
                        //posts.add(ChatterUtil.ChatterToCoachesAndGroups('Client has transitioned from ' + oldPhase + ' to ' + newPhase +' and has been removed the Phone Program. '+ dt.format(), classMembership.Coach_User_Id__c, 'Enrollment', classMembership.hmr_Contact_Name__c, CoachesMap, GroupMap));
                    }
                }
            }
            //Make sure the updated PM is not in the digitalMembershipMap before creating new PM record to avoid duplicates
            if(!digitalMembershipMap.containsKey(prg.Id)) {
            //Create new Program Membership record
                if(prgNamesMap.containsKey(newPhase)){
                    Program_Membership__c newMembership = new Program_Membership__c(hmr_Contact__c = prg.hmr_Contact__c, Program__c = prgNamesMap.get(newPhase),
                                                                    hmr_Status__c = 'Active', hmr_Enrollment_Date__c = Date.today());
                    newPrgmMembershipToInsert.add(NewMembership);
                    //Create Chatter post for contact object
                    //contactFeedPosts.add(ChatterUtil.postToClientFeed(prg.hmr_Contact__c, 'Client has transitioned from ' + oldPhase + ' to ' + newPhase + ' due to the following reason: ' + Prg.hmr_Status_Change_Reason__c  + '.' + dt.format()));
                }
            }
        }
            //Transition to P1 HSS process
        else if(prg.New_Phase__c == 'P1 Healthy Shakes'){
            prg.hmr_Status__c = 'Transitioned';
            //Loop through the Session Attendees Map and find records belonging to this contact. Place in map to be deleted
            if(!classMembershipMap.values().isEmpty()){
                for(Class_Member__c classMembership: classMembershipMap.values()){
                    if(classMembership.hmr_Contact_Name__c == prg.hmr_Contact__c){
                        classMembership.hmr_Active__c = false;
                        classMembership.hmr_Client_Drop_Reason__c = 'Transitioned to P1-HSS';
                        classMembershipMap.put(classMembership.Id, classMembership);
                        //Create Chatter post to notify coach and Enrollment Group
                        //posts.add(ChatterUtil.ChatterToCoachesAndGroups('Client has transitioned from ' + oldPhase + ' to ' + newPhase +' and has been removed the Phone Program. '+ dt.format(), classMembership.Coach_User_Id__c, 'Enrollment', classMembership.hmr_Contact_Name__c, CoachesMap, GroupMap));
                    }
                }
            }
            //If contactMap is not empty and dropConsent for this client is = true,
            //Clear Class Notes field and New Class Notes field
            if(!contactMap.values().isEmpty()){
                for(Contact contactRecord: contactMap.values()){
                    if(contactRecord.Id == prg.hmr_Contact__c){
                        contactRecord.hmr_Class_Notes__c = '';
                        contactRecord.hmr_New_Class_Note__c = false;
                        contactMap.put(contactRecord.Id, contactRecord);
                    }
                }
            }
            //Make sure the updated PM is not in the digitalMembershipMap before creating new PM record to avoid duplicates
            if(!digitalMembershipMap.containsKey(prg.Id)) {
                //Create New P1 HSS Program Membership for Client and add to newPrgmMembershipToInsert list to be inserted into the database
                if(prgNamesMap.containsKey('P1 Healthy Shakes')){
                    Program_Membership__c newMembership = new Program_Membership__c(hmr_Contact__c = prg.hmr_Contact__c, Program__c = prgNamesMap.get('P1 Healthy Shakes'),
                                                                    hmr_Status__c = 'Active', hmr_Enrollment_Date__c = Date.today());
                    newPrgmMembershipToInsert.add(NewMembership);
                    //Create Chatter post for contact object
                    //contactFeedPosts.add(ChatterUtil.postToClientFeed(prg.hmr_Contact__c, 'Client has transitioned from ' + oldPhase + ' to ' + newPhase + ' due to the following reason: ' + Prg.hmr_Status_Change_Reason__c  + '. ' + dt.format()));
                }
            }
        }
        //Return Client's new status
        return prg.hmr_Status__c;
    }

    public void onAfterInsert(List<Program_Membership__c> programMembershipList){
        if(!isAfterInsertFlag){
            isAfterInsertFlag = true;

            this.transitionActivityLog(programMembershipList);

            List<Program_Membership__c> initialWeightRecordList = this.getInitialWeightRecords(programMembershipList);

            if(initialWeightRecordList.size() > 0){
                this.reconcileActivityLog(initialWeightRecordList);
            }
        }
    }

    public void onAfterUpdate(List<Program_Membership__c> oldList, List<Program_Membership__c> newList, Map<Id, Program_Membership__c> oldMap, Map<Id, Program_Membership__c> newMap){
        if(!isAfterUpdateFlag){
            isAfterUpdateFlag = true;
            
            List<Program_Membership__c> initialWeightRecordList = this.getInitialWeightRecords(newList);

            if(initialWeightRecordList.size() > 0){
                this.reconcileActivityLog(initialWeightRecordList);
            }
        }
    }

    private List<Program_Membership__c> getInitialWeightRecords(List<Program_Membership__c> programMembershipList){
        //Get records having initial weight
        List<Program_Membership__c> initialWeightRecordList = new List<Program_Membership__c>();

        for(Program_Membership__c programMembershipRecord : programMembershipList){
            if(programMembershipRecord.InitialWeight__c != null && programMembershipRecord.InitialWeight__c > 0 && programMembershipRecord.hmr_Status__c == 'Active'){
                initialWeightRecordList.add(programMembershipRecord);
            }
        }

        return initialWeightRecordList;
    }

    /************************************************************
    Check for activity log records to move when new program membership is transitioned/created
      on date activity log already was tied to old program membership
    ************************************************************/
    public void transitionActivityLog(List<Program_Membership__c> programMembershipList){
        Set<Id> contactIdSet = new Set<Id>();
        List<ActivityLog__c> activityLogUpdateList = new List<ActivityLog__c>();
        //create map of activity log records
        Map<String, Program_Membership__c> contactToProgramMembershipMap = new Map<String, Program_Membership__c>();

        for(Program_Membership__c programMembershipRecord : programMembershipList){
            if(programMembershipRecord.hmr_Status__c == 'Active'){
                contactToProgramMembershipMap.put(programMembershipRecord.hmr_Contact__c, programMembershipRecord);
            }
        }

        for(ActivityLog__c activityLogRecord : [SELECT Contact__c, DateDataEnteredFor__c, Program_Membership__c
                                                FROM ActivityLog__c
                                                WHERE Contact__c IN: contactToProgramMembershipMap.keySet()
                                                 AND DateDataEnteredFor__c >= TODAY]){
            
            Program_Membership__c programMembershipRecord = contactToProgramMembershipMap.get(activityLogRecord.Contact__c);

            if(programMembershipRecord != null && activityLogRecord.Program_Membership__c != programMembershipRecord.Id){
                activityLogRecord.Program_Membership__c = programMembershipRecord.Id;
                activityLogUpdateList.add(activityLogRecord);
            }
        }

        if(activityLogUpdateList.size() > 0){
            update activityLogUpdateList;
        }
    }

    /************************************************************
    Create/update activity log for initial weight
    ************************************************************/
    public void reconcileActivityLog(List<Program_Membership__c> programMembershipList){
        List<ActivityLog__c> insertList = new List<ActivityLog__c>();
        List<ActivityLog__c> updateList = new List<ActivityLog__c>();

        //create map of activity log records
        Map<String, Map<Date, ActivityLog__c>> contactToActivityLogMap = new Map<String, Map<Date, ActivityLog__c>>();

        for(ActivityLog__c activityLogRecord : [SELECT Contact__c, DateDataEnteredFor__c, CurrentWeight__c
                                                FROM ActivityLog__c
                                                WHERE Program_Membership__c IN: programMembershipList
                                                 AND Program_Membership__r.hmr_Status__c = 'Active']){
            if(contactToActivityLogMap.containsKey(activityLogRecord.Contact__c)){
                contactToActivityLogMap.get(activityLogRecord.Contact__c).put(activityLogRecord.DateDataEnteredFor__c, activityLogRecord);
            }
            else{
                contactToActivityLogMap.put(activityLogRecord.Contact__c, new Map<Date, ActivityLog__c>{activityLogRecord.DateDataEnteredFor__c => activityLogRecord});
            }
        }

        //iterate over the program membership list to determine the activity log records to insert/update
        for(Program_Membership__c programMembershipRecord : programMembershipList){
            if(contactToActivityLogMap.containsKey(programMembershipRecord.hmr_Contact__c)){
                Map<Date, ActivityLog__c> activityLogMap = contactToActivityLogMap.get(programMembershipRecord.hmr_Contact__c);

                if(activityLogMap.containsKey(programMembershipRecord.hmr_Enrollment_Date__c)){
                    ActivityLog__c existingActivityLog = activityLogMap.get(programMembershipRecord.hmr_Enrollment_Date__c);

                    //check if weight is different
                    if(existingActivityLog.CurrentWeight__c != programMembershipRecord.InitialWeight__c){
                        existingActivityLog.CurrentWeight__c = programMembershipRecord.InitialWeight__c;
                        updateList.add(existingActivityLog);
                    }
                }
                else{
                    insertList.add(new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c,
                                                      CurrentWeight__c = programMembershipRecord.InitialWeight__c,
                                                      DateDataEnteredFor__c = programMembershipRecord.hmr_Enrollment_Date__c == null ? Date.today() : programMembershipRecord.hmr_Enrollment_Date__c, 
                                                      Program_Membership__c = programMembershipRecord.Id));
                }
            }
            else{
                insertList.add(new ActivityLog__c(Contact__c = programMembershipRecord.hmr_Contact__c,
                                                  CurrentWeight__c = programMembershipRecord.InitialWeight__c,
                                                  DateDataEnteredFor__c = programMembershipRecord.hmr_Enrollment_Date__c == null ? Date.today() : programMembershipRecord.hmr_Enrollment_Date__c,
                                                  Program_Membership__c = programMembershipRecord.Id));
            }
        }

        if(insertList.size() > 0){
            insert insertList;
        }

        if(updateList.size() > 0){
            update updateList;
        }
    }
}