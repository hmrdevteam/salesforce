/**
* @Date: 5/23/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
 Class : HMR_CC_OrderView_Controller
*/


@isTest
private class HMR_CC_OrderView_Controller_Test{

  @isTest static void updateOrderDateTest() {
   
        ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = true, ccrz__EncryptedId__c = 'testorderid' );
        insert orderObj;
        
        ccrz.cc_RemoteActionResult updateOrderDate = HMR_CC_OrderView_Controller.updateOrderDate('testorderid', '5/15/2017', false);
        
        ccrz.cc_RemoteActionResult updateOrderDateBlank = HMR_CC_OrderView_Controller.updateOrderDate('testorderid23', '5/15/2017', false);
        
        ccrz__E_Order__c orderObj2 = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = false, ccrz__EncryptedId__c = 'testorderid2' );
        insert orderObj2;
        
        ccrz.cc_RemoteActionResult updateOrderDate2 = HMR_CC_OrderView_Controller.updateOrderDate('testorderid2', '5/15/2017', false);
        
        System.assert(!String.isBlank(String.valueOf(updateOrderDate2)));
        
    }
    
     @isTest static void updateOrderTypeTest() {
     
         ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = true, ccrz__EncryptedId__c = 'testorderid' );
        insert orderObj;
        
        ccrz.cc_RemoteActionResult updateOrderDate = HMR_CC_OrderView_Controller.updateOrderDate('testorderid', '5/15/2017', false);
        
        ccrz.cc_RemoteActionResult updateOrderDateBlank = HMR_CC_OrderView_Controller.updateOrderDate('testorderid23', '5/15/2017', false);
        
        ccrz__E_Order__c orderObj2 = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = false, ccrz__EncryptedId__c = 'testorderid2' );
        insert orderObj2;
         
         ccrz.cc_RemoteActionResult orderType =  HMR_CC_OrderView_Controller.getOrderType('testorderid');
         
         System.assert(!String.isBlank(String.valueOf(orderType)));
     
     }
     
     @isTest static void updateOrderDateWrongOrderIdTest() {
     
     
     ccrz__E_Order__c orderObj = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = true, ccrz__EncryptedId__c = 'testorderid' );
        insert orderObj;
        
        ccrz.cc_RemoteActionResult updateOrderDate = HMR_CC_OrderView_Controller.updateOrderDate('testorderid', '5/15/2017', false);
        
        ccrz.cc_RemoteActionResult updateOrderDateBlank = HMR_CC_OrderView_Controller.updateOrderDate('testorderid23', '5/15/2017', false);
        
        ccrz__E_Order__c orderObj2 = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@text.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = false, ccrz__EncryptedId__c = 'testorderid2' );
        insert orderObj2;
        
        ccrz.cc_RemoteActionResult orderType2 =  HMR_CC_OrderView_Controller.getOrderType('testorderidtest');
        
        System.assert(!orderType2.success);
        
    }
    
    @isTest static void updateDietStartDateTest() {
        
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        Account testAcc = new Account(Name = 'testAcc');
        insert testAcc; 
       
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
                  
        User userTest = new User(alias = 'tet123', email='test123@noemlail.com',
                emailencodingkey='UTF-8', lastname='Testiing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemlail.com');
       
        insert userTest;
        
        Program__c program = new Program__c(Name = 'test', Days_in_1st_Order_Cycle__c = 20);
        insert program;
        
        Program_Membership__c programMembership = new Program_Membership__c(Program__c = program.id, hmr_Active__c = true);
        insert programMembership;
        
        ccrz__E_Order__c orderObj3 = new ccrz__E_Order__c(ccrz__BuyerEmail__c = 'test@texttt.com', ccrz__OrderDate__c = Date.today(), 
                                        PS_Reorder_Date_Change__c = true, Reorder_Date_Change__c = false, ccrz__EncryptedId__c = 'testorderid3',
                                        ccrz__Contact__c = con.Id, hmr_Program_Membership__c = programMembership.id, ccrz__OrderStatus__c = 'Template');
        insert orderObj3;
        
        ccrz.cc_RemoteActionResult updateResp =  HMR_CC_OrderView_Controller.updateDietStartDate(con.Id, programMembership.id,'5/15/2017');
        
        System.assert(updateResp.success);
                  

    }  

}