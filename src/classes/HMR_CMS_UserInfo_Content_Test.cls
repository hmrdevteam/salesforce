/*
Developer: Ali Pierre (HMR)
Date: 12/12/2017
Modified: 
Modified Date : 
Target Class: HMR_CMS_UserInfo_ContentTemplate
*/
@isTest(SeeAllData=false) 
private class HMR_CMS_UserInfo_Content_Test {	

	@isTest static void test_method_one() {
		//Implement test code
		HMR_CMS_UserInfo_ContentTemplate userInfoTemplate = new HMR_CMS_UserInfo_ContentTemplate();
		
		//Create Test Account Group
		ccrz__E_AccountGroup__c testAccountGroup = new ccrz__E_AccountGroup__c( Name = 'Test Account Group' );
        insert testAccountGroup;

        //Create Test Account 
		Account testAccount = new Account(
                        Name                    = 'Test Account',
                        ccrz__E_AccountGroup__c = testAccountGroup.Id,
                        OwnerId                 = UserInfo.getUserId(),
                        Type                    = 'Customer',
                        BillingStreet           = '45 Test Street',
                        BillingCity             = 'Test City'
                );
		insert testAccount;

		String emailAddress = String.valueOf(System.now().getTime() + '@cc-test.mail');
		//Create Contact
        Contact contactCC = new Contact(
                FirstName = 'CloudCraze',
                LastName  = 'TestUser',
                AccountId = testAccount.Id,
                Email = emailAddress,
                MailingStreet = 'abc'
        );
        insert contactCC;

        //Retrieve Community Profile to assign to test user
        Profile profile = [SELECT Id FROM Profile WHERE Name = :'HMR Customer Community User' LIMIT 1];

        //Retrieve current user's TimeZoneSidKey to assign to test user
        User testOwner = [SELECT TimeZoneSidKey FROM User WHERE Id = :UserInfo.getUserId()];

     	//Create test User
        User testUser = new User(
                Alias                    = 'cctest',
                Email                    = emailAddress,
                EmailEncodingKey         = 'UTF-8',
                LastName                 = 'TestUser',
                LanguageLocaleKey        = 'en_US',
                LocaleSidKey             = 'en_US',
                ProfileId                = profile.Id,
                TimeZoneSidKey           = testOwner.TimeZoneSidKey,
                Username                 = emailAddress,
                isActive                 = true,
                ContactId                = contactCC.Id,
                Contact                  = contactCC,
                ccrz__CC_CurrencyCode__c = 'USD'
        );
        insert testUser;
        update testUser.Contact;    	

    	System.debug('Test User ' + testUser);
            
        System.runAs ( testUser ) {
        	String renderHTML = userInfoTemplate.getHTML();
	    
	    	System.assert(!String.isBlank(renderHTML));
        }

        try{
	        HMR_CMS_UserInfo_ContentTemplate userInfo = new HMR_CMS_UserInfo_ContentTemplate(null);
	    }catch(Exception e){
	    	System.debug('HMR_CMS_UserInfo_ContentTemplate(null) ' + e);
	    }		
	}
}