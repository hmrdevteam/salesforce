/**
* Apex Rest Service Class for Goal Operations
* @Date: 2018-02-13
* @Author: Magnet 360 - Zach Engman
* @Modified: 2018-02-13
* @JIRA:
*/
@RestResource(urlMapping='/mobile/user/goal/*')
global with sharing class HMRMobGoal {

    /**
     * method to retrieve the goal information
     * @param goalDate - the date to retreive the goal data for
     * @param communityUserId (optional) - will use the current/calling user id if not passed 
     * @return returnMap success - true or false
     *                   goal - dto object representing the goal record or null if it doesn't exist
     *                   error - error message
    */
    @HttpGet
    global static void doGet(){
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        Map<String, String> paramMap = RestContext.request.params;
        String communityUserId = paramMap.get('communityUserId');

        if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
        }

        if(paramMap.get('goalDate') != null){
            RestContext.response.statusCode = 200;

            Date goalDate = Date.valueOf(paramMap.get('goalDate'));
            //get the contact record
            HMR_Contact_Selector contactSelector = new HMR_Contact_Selector();
            Contact contactRecord = contactSelector.getProfileByCommunityUserId(communityUserId);

            returnMap.put('success', true);
            returnMap.put('goal', getGoal(contactRecord, goalDate));

            //get the goal stats for all incase goal is changed so the data is readily available for mobile
            if(contactRecord.Program_Memberships__r.size() > 0){
                HMR_Goal_Service goalService = new HMR_Goal_Service(contactRecord.Id);
                Program_Membership__c programMembershipRecord = contactRecord.Program_Memberships__r[0];
                Map<String, HMR_Goal_Service.GoalStatistics> goalStatisticsMap = goalService.getAllGoalStatistics(programMembershipRecord.Id);   
                returnMap.put('entreeStatistics', goalStatisticsMap.get('Entrees__c'));
                returnMap.put('fruitAndVegetableStatistics', goalStatisticsMap.get('FruitsVegetables__c'));
                returnMap.put('hmrFoodStatistics', goalStatisticsMap.get('TotalMealReplacements__c'));
                returnMap.put('physicalActivityStatistics', goalStatisticsMap.get('TotalPhysicalActivityCaloriesRollUp__c'));
                returnMap.put('shakeStatistics', goalStatisticsMap.get('ShakesCereals__c'));
                returnMap.put('goalSettings', HMR_Goal_Service.getGoalSetting(programMembershipRecord.Program__r.Name, programMembershipRecord.DietType__c));
            }
        }
        else{
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'Goal date parameter required');
        }

        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));
    }

    /**
     * method to insert the goal information
     * @param goalDTO - the goal dto object with the required fields 
     * @return success - true/false indication of success (201 for true, 409 for false because of duplicate, 400 for false because of missing fields)
     * @return goalRecord - newly created record (or existing record in case of duplicate)
     * @return error - error description
    */
    @HttpPost
    global static void doPost(MobGoalDTO goal){
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        Goal__c goalRecord = goal.mapToGoal();

        //verify required fields
        if(!String.isBlank(goalRecord.Contact__c) && !String.isBlank(goalRecord.FocusArea__c) && goalRecord.GoalAmount__c != null && goalRecord.WeekStartDate__c != null){
            //verify a goal does not already exist for the week
            List<Goal__c> goalList = [SELECT Contact__c,
                                             FocusArea__c,
                                             GoalAmount__c,
                                             WeekStartDate__c
                                      FROM Goal__c 
                                      WHERE Contact__c =: goalRecord.Contact__c
                                            AND WeekStartDate__c =: goalRecord.WeekStartDate__c];
            if(goalList.size() == 0){
                insert goalRecord;

                response.statusCode = 201;

                resultMap.put('success', true);
                resultMap.put('goal', new MobGoalDTO(goalRecord));
            }
            else{
                response.statusCode = 409;

                resultMap.put('success', false);
                resultMap.put('goal', new MobGoalDTO(goalList[0]));
                resultMap.put('error', 'Goal already saved for the week');
            }
        }
        else{
            response.statusCode = 400;

            resultMap.put('success', false);
            resultMap.put('error', 'All goal fields are required');
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }

    global static MobGoalDTO getGoal(string communityUserId, Date goalDate){
        HMR_Contact_Selector contactSelector = new HMR_Contact_Selector();
        Contact contactRecord = contactSelector.getProfileByCommunityUserId(communityUserId);

        return getGoal(contactRecord, goalDate);
    }

    global static MobGoalDTO getGoal(Contact contactRecord, Date goalDate){
        HMR_Goal_Service goalService = new HMR_Goal_Service(contactRecord.Id);
        Goal__c goalRecord = goalService.getGoalForDate(goalDate);
        MobGoalDTO goalResult;

        if(goalRecord != null){
            goalResult = new MobGoalDTO(goalRecord);
        }

        return goalResult;
    }

    /**
     * global DTO class that represents the goal object and any additional details needed
    */
    global class MobGoalDTO {
        public String contactId {get; set;}
        public String focusArea {get; set;}
        public Integer goalAmount {get; set;}
        public Date weekStartDate {get; set;}

        public MobGoalDTO(Goal__c goalRecord){
            this.contactId = goalRecord.Contact__c;
            this.focusArea = goalRecord.FocusArea__c;
            this.weekStartDate = goalRecord.WeekStartDate__c;

            if(goalRecord.GoalAmount__c != null){
                this.goalAmount = Integer.valueOf(goalRecord.GoalAmount__c);
            }
        }

        public Goal__c mapToGoal() {
            Goal__c goalRecord = new Goal__c(
                Contact__c = this.contactId,
                FocusArea__c = this.focusArea,
                GoalAmount__c = this.goalAmount,
                WeekStartDate__c = this.weekStartDate
            );
            
            return goalRecord;
        }
    }
}