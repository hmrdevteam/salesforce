/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_CallOutSection_ContentTemplate
*/

@isTest //See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_CallOutSection_CT_Test{
  
  @isTest static void test_general_method() {
    // Implement test code
    HMR_CMS_CallOutSection_ContentTemplate calloutTemplate = new HMR_CMS_CallOutSection_ContentTemplate();
    String propertyValue = calloutTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');

    map<String, String> tab = new Map<String, String>();
    tab.put('testKey1','testValue1');
    tab.put('topicName','Eating Well');
    calloutTemplate.testAttributes = tab;

    string s2 = calloutTemplate.getPropertyWithDefault('testKey2','testValue2');
    string s1 = calloutTemplate.getPropertyWithDefault('testKey1','testValue2');
    system.assertEquals(s1, 'testValue1');

    string s3 = calloutTemplate.CSSClassForBackgroundColor;

    string s4 = calloutTemplate.CallOutDisplayMessage;

    string s5 = calloutTemplate.FontColorBlackOrWhite;

    cms.Link s6 = calloutTemplate.CallOutButtonURLLinkObj;

    string s7 = calloutTemplate.CallOutButtonURLLabelText;


    String renderHTML = calloutTemplate.getHTML();
    
    System.assert(!String.isBlank(renderHTML));

    try{
        HMR_CMS_CallOutSection_ContentTemplate qc = new HMR_CMS_CallOutSection_ContentTemplate(null);
    }catch(Exception e){}
  }
  
}