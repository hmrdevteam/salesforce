/**
* Apex Content Template Controller for the Success Stories List (Authored) Component
*
* @Date: 08/03/2017
* @Author Zach Engman (Magnet 360)
* @Modified: 08/03/2017
* @JIRA: HPRP-4014
*/
global virtual  class HMR_CMS_SSList_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_SSList_ContentTemplate(cms.createContentController cc) {
		super(cc);
	}
	global HMR_CMS_SSList_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String Title{    
        get{
            return getPropertyWithDefault('Title', 'Over 30 Years Of Success');
        }    
    }

    public String Subtitle{    
        get{
            return getPropertyWithDefault('Subtitle', 'Here are just a few stories...');
        }    
    }

    global virtual override String getHTML(){
    	String html = '<div class="success-stories">' +
					        '<div class="container">' +
					            '<div class="row">' +
					                '<div class="col-xs-12">' +
					                  '<div class="col-xs-12">' +
					                      '<div class="min-header">' +
					                          Title +
					                      '</div>' +
					                      '<div class="header">' +
					                          Subtitle +
					                      '</div>' +
					                  '</div>' +
					                '</div>' +
					            '</div>' +
		            			getSuccessStoriesHtml() +
		            		'</div>' +
		        		'</div>';

    	return html;
    }

    private string getSuccessStoriesHtml(){
    	HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
    	List<HMR_Knowledge_Service.ArticleRecord> articleRecordList = knowledgeService.getByArticleType('Success_Story__kav', 0, 0, 'AND Publish_to_Success_Story_Page__c = true');
    	String html = '';

    	for(integer index = 0; index < articleRecordList.size(); index++){
    		if(Math.mod(index, 3) == 0)
    			html += '<div class="row">';
    		
    		html += '<div class="col-xs-12 col-sm-4 text-center pointer" onclick="cardList.navigateToRecord(\'Success\', \' \', \'' + articleRecordList[index].UrlName + '\')">' +
	                    '<div class="block">' +
	                        '<img class="img-responsive" src="' + articleRecordList[index].ImageUri + '" />' +
	                        '<div class="name">' +
	                            articleRecordList[index].ClientFirstName + ' - ' + String.valueOf(articleRecordList[index].PoundsLost).replace('.00','') + ' LBS' +
	                        '</div>' +
	                    '</div>' +
		            '</div>';
		    if(Math.mod(index, 3) == 2)
		    	html += '</div>';
    	}

    	if(articleRecordList.size() > 0 && Math.mod(articleRecordList.size(), 3) != 2)
    		html += '</div>';

    	return html;
    }
}