/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/23/2017
 * Created By: Zach Engman
 * Last Modified Date:06/19/2017
 * Last Modified By:
 * Description: Handler for the ccrz__E_ContactAddr__c object trigger
 * ****************************************************/
public with sharing class HMR_CC_ContactAddress_Handler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;
    public static Boolean isBeforeUpdateFlag = false;
    
    public HMR_CC_ContactAddress_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }
    
    public void onBeforeInsert(List<ccrz__E_ContactAddr__c> contactAddressList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;
            //Standardize/Format the Field Data
            contactAddressList = standardizeFieldFormat(contactAddressList);
        }
    }

    public void onBeforeUpdate(List<ccrz__E_ContactAddr__c> oldContactAddressList, List<ccrz__E_ContactAddr__c> newContactAddressList, Map<Id, ccrz__E_ContactAddr__c> oldContactAddressMap, Map<Id, ccrz__E_ContactAddr__c> newContactAddressMap){
        if(!isBeforeUpdateFlag){
            isBeforeUpdateFlag = true;
            //Standardize/Format the Field Data
            newContactAddressList = standardizeFieldFormat(newContactAddressList);
        }
    }

    //Data Hygiene: Capitalize/Format fields appropriately
    private List<ccrz__E_ContactAddr__c> standardizeFieldFormat(List<ccrz__E_ContactAddr__c> contactAddressList){
        for(ccrz__E_ContactAddr__c contactAddressRecord : contactAddressList){
            contactAddressRecord.ccrz__FirstName__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__FirstName__c);
            contactAddressRecord.ccrz__LastName__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__LastName__c);
            contactAddressRecord.ccrz__MiddleName__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__MiddleName__c);
            contactAddressRecord.ccrz__AddressFirstline__c = HMR_Format_Utility.capitalizeAll(contactAddressRecord.ccrz__AddressFirstline__c);
            contactAddressRecord.ccrz__AddressSecondline__c = HMR_Format_Utility.capitalizeAll(contactAddressRecord.ccrz__AddressSecondline__c);
            contactAddressRecord.ccrz__City__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__City__c);
            contactAddressRecord.ccrz__State__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__State__c);
            contactAddressRecord.ccrz__StateISOCode__c = HMR_Format_Utility.uppercase(contactAddressRecord.ccrz__StateISOCode__c);
            contactAddressRecord.ccrz__Country__c = HMR_Format_Utility.capitalize(contactAddressRecord.ccrz__Country__c);
            contactAddressRecord.ccrz__CountryISOCode__c = HMR_Format_Utility.uppercase(contactAddressRecord.ccrz__CountryISOCode__c);
        }

        return contactAddressList;
    }
}