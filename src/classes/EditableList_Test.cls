/*
 * EditableList_Test.cls
  * @Date: 2017-1-30
  * @Author Nathan Anderson (Magnet 360)
  * @JIRA:
 * Usage:
 *   Test class for EditableList.cls
 *
 */
@isTest
private class EditableList_Test
{
  /*
   * Mock implementation of abstract class used for testing
   */
	private class MockEditableList extends EditableList
  {
    public MockEditableList(ApexPages.StandardController stdController)
    {
      super(stdController);
    }
  }

  private static Coaching_Session__c session;
  private static Session_Attendee__c myAttendee1;
  private static Session_Attendee__c myAttendee2;

  private static void setupData()
  {
    //Declare variable to store today's date
        Date dt = Date.today();

		//Retrieve Id for Standard User profile
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(FirstName = 'Test', LastName='Coach', Email='Test@testing.com',
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='123Test123@testing.com', Alias = 'Test');
        //Insert User
        insert userObj;

        //Create Coach from the user that has been created
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);

        //Insert Coach
        insert coachObj;

        //Declare a new list to store Programs
        List<Program__c> progList = new List<Program__c>();
        //Create new Programs and add them to conList to insert into the database
        Program__c programObj1 = new Program__c(Name = 'P1 Healthy Shakes', hmr_Program_Display_Name__c = 'TestProg1HSS',
        									hmr_Program_Description__c = 'P1 Healthy Shakes Description', hmr_Program_Type__c = 'Healthy Shakes',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 14);
        progList.add(programObj1);
        Program__c programObj2 = new Program__c(Name = 'P1 Healthy Solutions', hmr_Program_Display_Name__c = 'TestProg1HSAH',
        									hmr_Program_Description__c = 'Test Prog 1 HSS Description', hmr_Program_Type__c = 'Healthy Solutions',
        									hmr_Phase_Type__c = 'P1', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj2);
        Program__c programObj3 = new Program__c(Name = 'Phase 2', hmr_Program_Display_Name__c = 'TestProg2',
        									hmr_Program_Description__c = 'Test Prog Phase 2 Description', hmr_Program_Type__c = 'P2',
        									hmr_Phase_Type__c = 'P2', Days_in_1st_Order_Cycle__c = 30, hmr_Has_Phone_Coaching__c = true);
        progList.add(programObj3);
        //insert List of Programs
        insert progList;

        //Create Test Account
        Account testAccountObj = new Account(name = 'Class Account');
        //Insert Account
        insert testAccountObj;

        //Declare a new list to store contacts
        List<Contact> conList = new List<Contact>();
        //Create new Contacts and add them to conList to insert into the database
        Contact testContatObj1 = new Contact(FirstName = 'P1', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj1);
        Contact testContatObj2 = new Contact(FirstName = 'P1HSS', LastName = 'contact', Account = testAccountObj, hmr_Consent_Form__c = true);
        conList.add(testContatObj2);

        //List of contacts
        insert conList;

        //Create Classes to be added to clsList and inserted into the Database
        Class__c classObj1 = new Class__c(hmr_Active__c = true, hmr_Class_Name__c = 'testing P1 Healthy Shakes Class', hmr_Coach__c = coachObj.id, Program__c = programObj2.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = dt.addDays(1),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');

        //Insert Classes
        insert classObj1;

        //Declare a new List of Class Member object
        List<Class_Member__c> clsMemberList = new List<Class_Member__c>();
		//Create Classe members to be added to clsMemberList and inserted into the Database
        Class_Member__c clsMemberObj1 = new Class_Member__c(hmr_Active__c = true, hmr_Consented__c = true, hmr_Class__c = classObj1.Id,
        													hmr_Contact_Name__c = testContatObj1.Id, hmr_Start_Date__c = dt.addDays(-2));
        clsMemberList.add(clsMemberObj1);
        Class_Member__c clsMemberObj2 = new Class_Member__c(hmr_Active__c = true, hmr_Consented__c = true, hmr_Class__c = classObj1.Id,
        													hmr_Contact_Name__c = testContatObj2.Id, hmr_Start_Date__c = dt.addDays(2));
        clsMemberList.add(clsMemberObj2);

        insert clsMemberList;
		
      	//insert new Coaching_Session__c(hmr_Class__c = classObj1.Id);
        session = new Coaching_Session__c(hmr_Class__c = classObj1.Id); //[SELECT Id from Coaching_Session__c WHERE hmr_Class__c =: classObj1.Id LIMIT 1];
        //myAttendee1 = [SELECT Id from Session_Attendee__c WHERE hmr_Class_Member__c =: clsMemberObj1.Id LIMIT 1];
        //myAttendee2 = [SELECT Id from Session_Attendee__c WHERE hmr_Class_Member__c =: clsMemberObj2.Id LIMIT 1];
  }

	@isTest static void test_EditableList()
  {
		setupData();

    Test.startTest();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    Test.stopTest();

    System.assertEquals(0, mock.ZERO);
    System.assertEquals(0, mock.childList.size());
    System.assertEquals(0, mock.removeChildList.size());
	}

  @isTest static void test_getHasChildren()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    Test.startTest();

    System.assertEquals(false, mock.getHasChildren());

    mock.childList.add(new Contact());

    System.assertEquals(true, mock.getHasChildren());

    Test.stopTest();
  }

  @isTest static void test_initChildRecord()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    Test.startTest();

    System.assert(mock.initChildRecord() instanceof Contact);

    Test.stopTest();
  }

  @isTest static void test_addToList()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    System.assertEquals(0, mock.childList.size());

    Test.startTest();

    mock.addToList();

    Test.stopTest();

    System.assertEquals(1, mock.childList.size());
  }

  @isTest static void test_removeFromList()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    mock.childList.add(myAttendee1);
    mock.childList.add(myAttendee2);

    Test.startTest();

    mock.removeIndex = '1';
    mock.removeFromList();

    Test.stopTest();

    System.assertEquals(1, mock.childList.size());
    //System.assertEquals(myAttendee2.Id, mock.childList[0].Id);
    //System.assertEquals(1, mock.removeChildList.size());
    //System.assertEquals(myAttendee1.Id, mock.removeChildList[0].Id);
  }

  @isTest static void test_getSuccessURL_param()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    String retURL = '/test';
    PageReference pr = new PageReference('/');
    pr.getParameters().put('retURL', retURL);

    Test.setCurrentPageReference(pr);
    Test.startTest();

    System.assertEquals(retURL, mock.getSuccessURL().getUrl());

    Test.stopTest();
  }

  @isTest static void test_getSuccessURL_noParam()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);

    PageReference pr = new PageReference('/');

    Test.setCurrentPageReference(pr);
    Test.startTest();

    System.assertEquals(std.view().getUrl(), mock.getSuccessURL().getUrl());

    Test.stopTest();
  }

  // @isTest static void test_save_failure()
  // {
  //   setupData();
  //
  //   ApexPages.StandardController std = new ApexPages.StandardController(session);
  //   MockEditableList mock = new MockEditableList(std);
  //
  //   mock.childList.add(myAttendee1);
  //   mock.removeChildList.add(myAttendee2);
  //
  //   // Purposely leave out required LastName
  //   Session_Attendee__c newAttendee = new Contact();
  //   newContact.AccountId = acct.Id;
  //
  //   mock.childList.add(newContact);
  //
  //   Test.startTest();
  //
  //   System.assertEquals(null, mock.save());
  //
  //   Test.stopTest();
  //
  //   System.assertEquals(1, ApexPages.getMessages().size());
  //   System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
  //
  // }

  @isTest static void test_save_success()
  {
    setupData();

    ApexPages.StandardController std = new ApexPages.StandardController(session);
    MockEditableList mock = new MockEditableList(std);
    SessionAttendanceListExtension ext = new SessionAttendanceListExtension(std);

    mock.childList.add(myAttendee1);
    mock.removeChildList.add(myAttendee2);

    Test.startTest();

    ext.initChildRecord();
    ext.getChildren();
    //myAttendee1.hmr_Attendance__c = 'Attended';
    //System.assertEquals(mock.getSuccessURL().getUrl(), mock.save().getUrl());

    Test.stopTest();

  }

  @isTest static void test_save_success2() {

      cc_dataFActory.setupTestUser();
      cc_dataFActory.setupCatalog();
      cc_dataFActory.createOrders(1);

      ccrz__E_Order__c order = [SELECT Id FROM ccrz__E_Order__c LIMIT 1];
      ccrz__E_OrderItem__c item = [SELECT Id FROM ccrz__E_OrderItem__c LIMIT 1];

      ApexPages.StandardController std = new ApexPages.StandardController(order);
      MockEditableList mock = new MockEditableList(std);
      ReshipEditableListExtension ext = new ReshipEditableListExtension(std);

      mock.childList.add(item);
      mock.removeChildList.add(item);

      Test.startTest();

      ext.initChildRecord();
      ext.getChildren();
      ext.getShipMethods();
      ext.shipping = 'Standard';
      ext.saveShipMethod();
      ext.saveSubmit();
      //myAttendee1.hmr_Attendance__c = 'Attended';
      System.assertEquals(mock.getSuccessURL().getUrl(), mock.save().getUrl());

      Test.stopTest();

  }


}