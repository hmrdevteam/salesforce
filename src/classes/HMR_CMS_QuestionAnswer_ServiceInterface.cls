global with sharing class HMR_CMS_QuestionAnswer_ServiceInterface implements cms.ServiceInterface {

    global HMR_CMS_QuestionAnswer_ServiceInterface(){}

    global HMR_CMS_QuestionAnswer_ServiceInterface(cms.CoreController controller){}
    /**
     *
     * @param params a map of parameters including:
     * 'action' ==> 'getAnswers'
     * 'p_feedId' ==> 'XXXXXXXXXXXX'
     * 'p_likeItemId' ==> 'XXXXXXXXXXXX'
     * 'p_bestAnswerId' ==> 'XXXXXXXXXXXX'
     * 'p_limit' ==> '10'
     * 'p_offset' ==> '0'
     * 'p_userId' ==> '0'
     * 'p_replyText' ==> 'this is good post, very good'
     * 'p_updatedTargetId' ==> 'XXXXXXXXXXXX' // this could be either comment or feedElement
     * 'p_updatedSubject' ==> 'this is good post, very good'
     * 'p_updatedText' ==> 'this is good post, very good'
     * 'p_deleteTargetId' ===> 'xxxxxxxxxx'
     * 'p_hmrSuggested_old' ===> 'true'
     * 'p_hmrSuggested_new' ===> 'true'
     * 'p_articleName' ===> 'Name-name-nameee'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {
        
        //String action = params.get('action');
        map<String, Object> returnMe = new map<string, Object>();
        try {
            String action = params.get('action');
            if(action == 'getAnswers') {
                returnMe = getAnswers(params);
            }else if(action == 'likeFeed') {
                returnMe = toggleFeedElementLike(params, true);
            }else if(action == 'unlikeFeed') {
                returnMe = toggleFeedElementLike(params, false);
            }else if(action == 'likeComment') {
                returnMe = toggleCommentLike(params, true);
            }else if(action == 'unlikeComment') {
                returnMe = toggleCommentLike(params, false);
            }else if(action == 'postComment') {
                returnMe = postAnswerToFeedElement(params);
            }else if(action == 'setTheBestAnswer') {
                returnMe = setTheBestAnswer(params);
            }else if(action == 'updateComment') {
                returnMe = updateComment(params);
            }else if(action == 'updateFeedElement') {
                returnMe = updateFeedElement(params);
            }else if(action == 'deleteComment') {
                returnMe = deleteComment(params);
            }else if(action == 'deleteElement') {
                returnMe = deleteElement(params);
            }else if(action == 'post') {
                returnMe = postQuestion(params);
            }else if(action == 'postToArticle') {
                returnMe = postFeedElement(params);
            }else if(action == 'getArticleComments') {
                returnMe = getArticleComments(params);
            }else if(action == 'updateCommentForArticle') {
                returnMe = updateCommentForArticle(params);
            }else{
                //invalid action
                returnMe.put('success',false);
                returnMe.put('message','Invalid Action+++'+(action == 'getAnswers'));
            }
        } catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
            //return '{"setdata":"","success":false,"message":' + JSON.serialize(message) + '}';
        }

        // No actions matched and no error occurred
        return JSON.serialize(returnMe);
        
        return '';
    }

    private static map<String, Object> getArticleComments(Map<String, String> params){
        map<String, Object> returnMe = new map<string, Object>();
        String p_articleName = params.get('p_articleName');   

        Integer p_offset = 0; //default value
        if(!String.isBlank(params.get('p_offset'))){
            p_offset= Integer.valueOf(params.get('p_offset'));
        }
        Integer p_limit =10; //default value
        if(!String.isBlank(params.get('p_limit'))){
            p_limit= Integer.valueOf(params.get('p_limit'));
        }

        HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();
        
        HMR_Knowledge_Service.ArticleRecord articleRecord = knowledgeService.getByName(p_articleName);
        
        
        ArticleCommentsRecord articleComments;
        
        try{
            //ConnectApi.FeedElement fe = ConnectApi.ChatterFeeds.getFeedElement(null, p_feedId);
            //system.debug('fe+++'+fe);
            articleComments = new ArticleCommentsRecord(articleRecord.Id);
            //system.debug('fe+++'+detailRecord);
            articleComments.getOffsetList(p_offset, p_limit);


            returnMe.put('success',true);
            returnMe.put('feedElement',articleComments);

            //User curUser = [select SmallPhotoUrl FROM User WHERE User.ID = :params.get('p_userId') LIMIT 1];
            //returnMe.put('currentUser', curUser);

        }catch(Exception e){
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
        }


        return returnMe;

    }



    private static map<String, Object> getAnswers(Map<String, String> params){
        map<String, Object> returnMe = new map<string, Object>();
        String p_feedId = params.get('p_feedId');   

        Integer p_offset = 0; //default value
        if(!String.isBlank(params.get('p_offset'))){
            p_offset= Integer.valueOf(params.get('p_offset'));
        }
        Integer p_limit =10; //default value
        if(!String.isBlank(params.get('p_limit'))){
            p_limit= Integer.valueOf(params.get('p_limit'));
        }

        QuestionDetailRecord detailRecord;
        
        try{
            //ConnectApi.FeedElement fe = ConnectApi.ChatterFeeds.getFeedElement(null, p_feedId);
            //system.debug('fe+++'+fe);
            detailRecord = new QuestionDetailRecord(p_feedId);
            //system.debug('fe+++'+detailRecord);
            detailRecord.getOffsetList(p_offset, p_limit);


            returnMe.put('success',true);
            returnMe.put('feedElement',detailRecord);

            User curUser = [select SmallPhotoUrl FROM User WHERE User.ID = :params.get('p_userId') LIMIT 1];
            returnMe.put('currentUser', curUser);

        }catch(Exception e){
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
        }


        return returnMe;
    }



    public map<String, Object> toggleFeedElementLike(Map<String, String> params, boolean isLiked){
        //map<String, Object> returnMe = new map<string, Object>();
        HMR_CMS_QuestionAnswer_ServiceInterface.likeOrNotFeed(Network.getNetworkId(), params.get('p_likeItemId'), isLiked);
        return refreshData(params);
    }

    public map<String, Object> toggleCommentLike(Map<String, String> params, boolean isLiked){
        HMR_CMS_QuestionAnswer_ServiceInterface.likeOrNotComment(Network.getNetworkId(), params.get('p_likeItemId'), isLiked);
        return refreshData(params);
    }


    public map<String, Object> postAnswerToFeedElement(Map<String, String> params){
        string feedElementAnswerText = params.get('p_replyText').trim();
        
        if(!String.isBlank(feedElementAnswerText)){
            ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), params.get('p_feedId'), feedElementAnswerText);
        }
        return refreshData(params);
    }

    public map<String, Object> setTheBestAnswer(Map<String, String> params){
        string bestAnswerId = params.get('p_bestAnswerId');
        if(bestAnswerId == '') bestAnswerId = null;
        ConnectApi.QuestionAndAnswersCapabilityInput qaInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        qaInput.bestAnswerId = bestAnswerId;
        system.debug('Anydatatype_msg++++++======>>>>'+bestAnswerId+params.get('p_feedId'));
        ConnectApi.QuestionAndAnswersCapability qa = ConnectApi.QuestionAndAnswers.updateQuestionAndAnswers(Network.getNetworkId(), params.get('p_feedId'), qaInput);
        return refreshData(params);
    }

    public map<String, Object> updateComment(Map<String, String> params){
        string targetId = params.get('p_updatedTargetId');
        ConnectApi.FeedEntityIsEditable isEditable = ConnectApi.ChatterFeeds.isCommentEditableByMe(Network.getNetworkId(), targetId);

        if (isEditable.isEditableByMe == true){
            ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            textSegmentInput.text = params.get('p_updatedText');
            messageBodyInput.messageSegments.add(textSegmentInput);

            commentInput.body = messageBodyInput;

            ConnectApi.Comment editedComment = ConnectApi.ChatterFeeds.updateComment(Network.getNetworkId(), targetId, commentInput);
        }
        return refreshData(params);
    }

    public map<String, Object> updateFeedElement(Map<String, String> params){
        string targetId = params.get('p_updatedTargetId');
        ConnectApi.FeedEntityIsEditable isEditable = ConnectApi.ChatterFeeds.isFeedElementEditableByMe(Network.getNetworkId(), targetId);

        if (isEditable.isEditableByMe == true){

            //start checking if hmr suggested need to be updated
            string hmrSuggestedOld = params.get('p_hmrSuggested_old');
            string hmrSuggestedNew = params.get('p_hmrSuggested_new');
                //system.debug('hmrSuggestedOld+++'+hmrSuggestedNew);
                //system.debug('hmrSuggestedOld+++'+(hmrSuggestedNew == 'false'));
            if(hmrSuggestedNew != hmrSuggestedOld){
                //HMR suggested TOPIC id
                Id topicId ;
                for(Topic tpc: [select Id, Name, NetworkId from Topic where Name = 'HMR Suggested' and NetworkId =: Network.getNetworkId()]){
                    topicId = tpc.Id;
                }
                if(hmrSuggestedNew == 'true'){
                    TopicAssignment ta = new TopicAssignment(EntityId = targetId, TopicId = topicId);
                    insert ta;
                }else if(hmrSuggestedNew == 'false'){//if unchecked the HMR suggested box
                    list<TopicAssignment> ts = [select EntityId, EntityType, Id, NetworkId, TopicId from TopicAssignment where TopicId = : topicId and EntityId =: targetId limit 1];
                    if(ts.size()>0){
                        delete ts;
                    }
                }
            }


            string bodyText = params.get('p_updatedText');
            string titleText = params.get('p_updatedSubject');
            HMR_ConnectApi_Service ser = new HMR_ConnectApi_Service();
            //system.debug('===========___'+targetId+titleText+bodyText);
            //ser.communityId = null;
            ConnectApi.FeedElement feee = ser.updateQuestionAndAnswer(targetId, null, titleText, bodyText);
            //for(ConnectApi.MessageSegment m: feee.body.messageSegments){
            //    system.debug('sysysysysysysy====='+m.type);
            //}
        }
        return refreshData(params);
    }

    public map<String, Object> updateCommentForArticle(Map<String, String> params){
        string targetId = params.get('p_updatedTargetId');
        ConnectApi.FeedEntityIsEditable isEditable = ConnectApi.ChatterFeeds.isFeedElementEditableByMe(Network.getNetworkId(), targetId);

        if (isEditable.isEditableByMe == true){

            string bodyText = params.get('p_updatedText');
            

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            textSegmentInput.text = bodyText;
            messageBodyInput.messageSegments.add(textSegmentInput);
            feedItemInput.body = messageBodyInput;
            ConnectApi.FeedElement editedFeedElement = ConnectApi.ChatterFeeds.updateFeedElement(Network.getNetworkId(), targetId, feedItemInput);


        }
        return refreshData(params);
    }

    public map<String, Object> deleteElement(Map<String, String> params){
            ConnectApi.ChatterFeeds.deleteFeedElement(Network.getNetworkId(), params.get('p_deleteTargetId'));
        return refreshData(params);
    }

    public map<String, Object> deleteComment(Map<String, String> params){
            ConnectApi.ChatterFeeds.deleteComment(Network.getNetworkId(), params.get('p_deleteTargetId'));
        return refreshData(params);
    }

    public map<String, Object> postQuestion(Map<String, String> params){
        String topicValue = params.get('p_topicValue');
        if(topicValue != null){
            topicValue = topicValue.replace('Questions-', '');
            topicValue = topicValue.replace('-', ' ');
        }
        HMR_Topic_Service topicService = new HMR_Topic_Service();
        string topicId = topicService.getTopicId(topicValue);//topicValue //'HMR Foods'
        String subject = params.get('p_updatedSubject');
        String body = params.get('p_updatedText');
        
        HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
        ConnectApi.FeedElement fe = connectApiService.postQuestionAndAnswer(topicId, subject, body);
        map<String, Object> returnMe = new map<String, Object>();

        returnMe.put('feedElement',fe);
        returnMe.put('success',true);

        return returnMe;
    }

    public map<String, Object> postFeedElement(Map<String, String> params){
        string articleId = params.get('p_articleId');
        //String subject = params.get('p_updatedSubject');
        String body = params.get('p_replyText');
             
        ConnectApi.FeedElement fe = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), articleId, ConnectApi.FeedElementType.FeedItem, body);
        return refreshData(params);
    }

    global static void likeOrNotFeed(string communityId, string fId, boolean islike){
        connectApi.ChatterFeeds.updateLikeForFeedElement(communityId, fId, isLike);
    }

    global static void likeOrNotComment(string communityId, string cId, boolean islike){
        connectApi.ChatterFeeds.updateLikeForComment(communityId, cId, isLike);
    }




    //data refresher dispatcher
    public map<String, Object> refreshData(Map<String, String> params){
        if(params.get('p_articleName')!=null){
            //means request comes from Article comments page
            return getArticleComments(params);
        }
        return getAnswers(params);
    }


    /**************************************************/
    /*************For Question Detail Page*************/
    /**************************************************/

    //wrapper class for FE
    public class QuestionDetailRecord{
        public ConnectApi.FeedElement feedElement{get; set;}//
        public ConnectApi.UserSummary userSummary {get; set;} //
        public ConnectApi.Comment bestAnswer {get; set;} 
        public string bestAnswerText {get; set;} 
        public boolean hmrSuggested {get; set;} 
        public string questionCategory {get; set;} 
        public string questionText {get; set;} //
        public string bestAnswerDate {get; set;} 
        public string creatDate {get; set;} 
        public List<QuestionDetailCommentRecord> commentRecordList {get; set;}
        public List<QuestionDetailCommentRecord> offsetList {get; set;}
        public Integer pageTotal {get; set;}
        public Integer commentsTotal {get; set;}
        
        //lighter version wrapper, for comments on Article pages use
        public QuestionDetailRecord(ConnectApi.FeedElement fe){
            this.feedElement =fe;
            this.questionText = '';
            if(feedElement.body.text != null){
                questionText = feedElement.body.text.unescapeHtml4();
            }
            this.creatDate = feedElement.createdDate.format('MMMM dd, yyyy hh:mm a', UserInfo.getTimeZone().toString());
            //this.userSummary = ((ConnectApi.UserSummary)feedElement.parent);
        }
        public QuestionDetailRecord(string p_feedId){
            //feed item
            this.feedElement = ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), p_feedId);
            //for(ConnectApi.MessageSegment m: feedElement.body.messageSegments){
            //    system.debug('1sysysysysysysy====='+m.type);
            //}
            this.questionText = '';
            if(feedElement.body.text != null){
                questionText = feedElement.body.text.unescapeHtml4();
            }
            //user summrary
            this.userSummary = ((ConnectApi.UserSummary)feedElement.parent);
            this.hmrSuggested = false;

            HMR_Topic_Service topicService = new HMR_Topic_Service();
            map<string, Category_Map__mdt>  topicMap = topicService.getTopicMap();
            for(ConnectApi.Topic t: this.feedElement.capabilities.topics.items){
                if(t.name == 'HMR Suggested'){
                    this.hmrSuggested = true;
                }else{
                    if(topicMap.get(t.name) != null){
                        this.questionCategory = 'Questions-'+(t.name.replace(' ','-'));
                    }
                }
            }
            system.debug('++___)))'+UserInfo.getTimeZone().toString());
            if(feedElement.capabilities.questionAndAnswers.bestAnswer != null){
                bestAnswer = feedElement.capabilities.questionAndAnswers.bestAnswer;
                bestAnswerText = bestAnswer.body.text.unescapeHtml4();
                bestAnswerDate = bestAnswer.createdDate.format('MMMM dd, yyyy hh:mm a', UserInfo.getTimeZone().toString());
            }
            //comments page number
            this.pageTotal = feedElement.capabilities.comments.page.total;

            this.creatDate = feedElement.createdDate.format('MMMM dd, yyyy hh:mm a', UserInfo.getTimeZone().toString());
            //list of comments
            list<ConnectApi.Comment> comList =ConnectApi.ChatterFeeds.getCommentsForFeedElement(Network.getNetworkId(), p_feedId).items;
            this.commentsTotal = comList.size();
            this.commentRecordList = new List<QuestionDetailCommentRecord>();
            for(Integer i = commentsTotal-1; i >=0; i--){
                //ConnectApi.Comment commentRecord : comList){
                commentRecordList.add(new QuestionDetailCommentRecord(comList[i]));
            }
        }

        public void getOffsetList(Integer o, Integer l){
            this.offsetList = new List<QuestionDetailCommentRecord>();
            system.debug('offsetList++++'+(o+l));
            system.debug('offsetList++++'+commentsTotal);
            Integer endPoint = (o+l)>commentsTotal? commentsTotal : o+l;
            for(Integer i =o; i<endPoint; i++){
                //if(feedElement.capabilities.comments.page.items.get(i)!= null){
                    offsetList.add(commentRecordList[i]);
                //}
            }
            this.commentsTotal = commentRecordList.size();
 
        }
    }
    
    public class QuestionDetailCommentRecord{
        public ConnectApi.Comment commentRecord {get; set;}
        public string answerText {get; set;}
        public string creatDate {get; set;} 
        public QuestionDetailCommentRecord(ConnectApi.Comment commentRecord){
            this.commentRecord = commentRecord;
            answerText = '';
            if(commentRecord.body.text != null){
                answerText = commentRecord.body.text.unescapeHtml4();
            }
            this.creatDate = commentRecord.createdDate.format('MMMM dd, yyyy hh:mm a', UserInfo.getTimeZone().toString());

        }
    }
    /**************************************************/
    /*************For Question Detail Page*************/
    /**************************************************/







    /**************************************************/
    /*************For Article Comment Page*************/
    /**************************************************/
    //wrapper class for article comments
    public class ArticleCommentsRecord{
        public Integer commentsTotal {get; set;}
        //public ConnectApi.UserSummary userSummary {get; set;} 
        public list<FeedElementRecord> allComments{get; set;}
        public list<FeedElementRecord> offsetList {get; set;}
        public string articleId {get; set;}
        public ArticleCommentsRecord (string p_articleId){
            if(!String.isBlank(p_articleId)){
                articleId = p_articleId;
                allComments = new list<FeedElementRecord>();
                list<ConnectApi.FeedElement> feedElements = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record,p_articleId).elements;
                
                for(ConnectApi.FeedElement f: feedElements){
                    if(!String.isblank(f.body.text)){
                        allComments.add(new FeedElementRecord(f));
                    }
                }
                commentsTotal = allComments.size();

            }
        }

        public void getOffsetList(Integer o, Integer l){
            this.offsetList = new List<FeedElementRecord>();
            Integer endPoint = (o+l)>commentsTotal? commentsTotal : o+l;
            for(Integer i =o; i<endPoint; i++){
                //if(feedElement.capabilities.comments.page.items.get(i)!= null){
                    offsetList.add(allComments[i]);
                //}
            }
            this.commentsTotal = allComments.size();
        }

    }    

    public class FeedElementRecord{
        public ConnectApi.FeedElement feedElement{get; set;}//
        public string questionText {get; set;} //
        public string creatDate {get; set;} 
        //lighter version wrapper, for comments on Article pages use
        public FeedElementRecord(ConnectApi.FeedElement fe){
            this.feedElement =fe;
            this.questionText = '';
            if(feedElement.body.text != null){
                questionText = feedElement.body.text.unescapeHtml4();
            }
            this.creatDate = feedElement.createdDate.format('MMMM dd, yyyy hh:mm a', UserInfo.getTimeZone().toString());
        }
    }
    /**************************************************/
    /*************For Article Comment Page*************/
    /**************************************************/


    public static Type getType() {
        return HMR_CMS_QuestionAnswer_ServiceInterface.class;
    }


}