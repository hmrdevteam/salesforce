/**
* Test Class Coverage of the HMR_CC_Order_Selector Class
*
* @Date: 04/25/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA: 
*/

@isTest
private class HMR_CC_Order_Selector_Test {
	@testSetup
    private static void setupOrderData(){
        List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
    }
    
    @isTest
    private static void testGetById(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT Id FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        ccrz__E_Order__c result = HMR_CC_Order_Selector.getById(orderList[0].Id);
        
        Test.stopTest();

        //Verify the correct record was returned
        System.assertEquals(orderList[0].Id, result.Id);   
    }
    
    @isTest
    private static void testGetByEncryptedId(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        ccrz__E_Order__c result = HMR_CC_Order_Selector.getByEncryptedId(orderList[0].ccrz__EncryptedId__c);
        
        Test.stopTest();
        
        //Verify the correct record was returned
        System.assertEquals(orderList[0].Id, result.Id);   
    }
    
    @isTest
    private static void testGetPendingByContactId(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Contact__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        ccrz__E_Order__c result = HMR_CC_Order_Selector.getPendingByContactId(orderList[0].ccrz__Contact__c);
        
        Test.stopTest();
        
        //Verify if the original record was pending that it was returned else that it wasn't
        if(orderList[0].ccrz__OrderStatus__c == 'Pending')
        	System.assertEquals(orderList[0].Id, result.Id); 
        else
            System.assert(result == null);
    }
    
    @isTest
    private static void testGetByProgramMembershipId(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT hmr_Program_Membership__c FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        List<ccrz__E_Order__c> orderResultList = HMR_CC_Order_Selector.getByProgramMembershipId(orderList[0].hmr_Program_Membership__c);
        
        Test.stopTest();
        
        //Verify the result was returned
        System.assertEquals(orderList[0].Id, orderResultList[0].Id); 
    }
    
    @isTest
    private static void testGetPendingAndCompletedByContactIdAndProgramMembershipId(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Contact__c, hmr_Program_Membership__c FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        List<ccrz__E_Order__c> orderResultList = HMR_CC_Order_Selector.getPendingAndCompletedByContactIdAndProgramMembershipId(orderList[0].ccrz__Contact__c, 
                                                                                                                               orderList[0].hmr_Program_Membership__c);
        
        Test.stopTest();
        
        //Verify if the original record was assigned a program membership that it was returned else that it wasn't
        if(!String.isBlank(orderList[0].ccrz__Contact__c) && !String.isBlank(orderList[0].hmr_Program_Membership__c))
        	System.assertEquals(orderList[0].hmr_Program_Membership__c, orderResultList[0].hmr_Program_Membership__c); 
        else
            System.assert(orderResultList.size() == 0);
    }
    
    @isTest
    private static void testGetTemplateByContactIdAndProgramMembershipId(){
        //Existing Order Record
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__Contact__c, hmr_Program_Membership__c, ccrz__OrderStatus__c FROM ccrz__E_Order__c];
        
        Test.startTest();
        
        ccrz__E_Order__c result = HMR_CC_Order_Selector.getTemplateByContactIdAndProgramMembershipId(orderList[0].ccrz__Contact__c, 
                                                                                                                    orderList[0].hmr_Program_Membership__c);
        
        Test.stopTest();
        
        //Verify if the original record was assigned a program membership and was a template order that it was returned else that it wasn't
        if(orderList[0].ccrz__OrderStatus__c == 'Template' && !String.isBlank(orderList[0].ccrz__Contact__c) && !String.isBlank(orderList[0].hmr_Program_Membership__c))
        	System.assertEquals(orderList[0].hmr_Program_Membership__c, result.hmr_Program_Membership__c); 
        else
            System.assert(result ==  null);
    }
}