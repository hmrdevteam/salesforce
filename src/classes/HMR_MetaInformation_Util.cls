/**
* Apex Content Template Controller for Hero Section on Clinic Landing Page
*
* @Date: 06/16/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: 07/25/2017 Nathan Anderson (Magnet 360) - Added support for querying for metadata details from the Knowledge Article records directly
* @JIRA:
*/
global virtual class HMR_MetaInformation_Util extends cms.ContentTemplateController{

	//take this out of here and put it in a new class constructor
	global String pageTitleProp { get; set; }
	global String pageDescriptionProp { get; set; }
	global String pageKeywordsProp { get; set; }
	global list<HMR_MetaInformation_Class> metaList { get; set; }

	global HMR_MetaInformation_Util(cms.createContentController cc){
        super(cc);
    }
	global HMR_MetaInformation_Util(cms.CoreController controller){
        super();
		//take this out of here and put it in a new class constructor
		String pageURL = ApexPages.currentPage().getUrl();

		//System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
		//System.debug(ApexPages.currentPage().getRedirect());

		//pageTitleProp='';
		//pageDescriptionProp='';
		//pageKeywordsProp='';

		metaList = new list<HMR_MetaInformation_Class>();
		HMR_MetaInformation_Class metaData;
		metadata = new HMR_MetaInformation_Class('healthy-solutions-at-home-diet-plan-build-your-kit','Build Your Healthy Solutions Quick Start Kit - HMR','Cutstomize your Healthy Solutions Quick Start Kit. Choose your shakes and entrees, free coaching options, to build an HMR kit thats right for you.','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('checkout','Checkout - HMR','Complete your HMR order by providing payment and shipping information.','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('about-hmr-foods-nutrition-ingredients','','','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('healthy-shakes-diet-plan-build-your-kit','Build Your Healthy Shakes Kit - HMR','Our weight loss kits are completely customizable! Build your own, and choose your shakes, entrees, coaching options, and more!','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('/shopping-cart','Shopping Cart - HMR','View your HMR Shopping Cart. Update your order, add a discount code or coupon.','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('my-account-order-view','Re-Order Management - HMR','Update your HMR account re-order frequency settings.','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('shop-hmr-foods','Shop HMR Foods - HMR','Shop any of our HMR foods including Shakes, Entrees, Cereal, Soup, and BeneFit Bars.','');
		metaList.add(metaData);

		metadata = new HMR_MetaInformation_Class('/recipes','Weight-Loss Recipes - HMR','Each one of our HMR foods can be enhanced by adding fruits, vegetables, and even other HMR foods! Check out our favorite HMR Recipes.','');
		metaList.add(metaData);

		//Recipes
		List<Recipe__kav> recipes = [SELECT Meta_Title__c, Meta_Description__c, Keywords__c, UrlName FROM Recipe__kav WHERE PublishStatus = 'Online' AND Language='en_US'];

		for(Recipe__kav r : recipes) {
			metadata = new HMR_MetaInformation_Class(r.UrlName.toLowerCase(), r.Meta_Title__c, r.Meta_Description__c, r.Keywords__c);
			metaList.add(metadata);
		}

		//Success Stories
		List<Success_Story__kav> stories = [SELECT Meta_Title__c, Meta_Description__c, Keywords__c, UrlName FROM Success_Story__kav WHERE PublishStatus = 'Online' AND Language='en_US'];

		for(Success_Story__kav s : stories) {
			metadata = new HMR_MetaInformation_Class(s.UrlName.toLowerCase(), s.Meta_Title__c, s.Meta_Description__c, s.Keywords__c);
			metaList.add(metadata);
		}

		//Articles
		List<Blog_Post__kav> articles = [SELECT Meta_Title__c, Meta_Description__c, Keywords__c, UrlName FROM Blog_Post__kav WHERE PublishStatus = 'Online' AND Language='en_US'];

		for(Blog_Post__kav a : articles) {
			metadata = new HMR_MetaInformation_Class(a.UrlName.toLowerCase(), a.Meta_Title__c, a.Meta_Description__c, a.Keywords__c);
			metaList.add(metadata);
		}

		//FAQs
		List<FAQ__kav> faqs = [SELECT Meta_Title__c, Meta_Description__c, Keywords__c, UrlName FROM FAQ__kav WHERE PublishStatus = 'Online' AND Language='en_US'];

		for(FAQ__kav f : faqs) {
			metadata = new HMR_MetaInformation_Class(f.UrlName.toLowerCase(), f.Meta_Title__c, f.Meta_Description__c, f.Keywords__c);
			metaList.add(metadata);
		}

		//for(HMR_MetaInformation_Class m: metaList){
		//	system.debug(pageURL+'!@@@@@@@@@@@'+m.url+pageURL.contains(m.url));
		//	if(pageURL.contains(m.url)) {
		//		pageTitleProp =m.title;
		//		pageDescriptionProp=m.description;
		//		pageKeywordsProp=m.keywords;
		//	}
		//}
    }
	global HMR_MetaInformation_Util() {}

	global class HMR_MetaInformation_Class{
		global String url { get; set; }
		global String title { get; set; }
		global String description { get; set; }
		global String keywords { get; set; }

		global HMR_MetaInformation_Class(){

		}
		global HMR_MetaInformation_Class(string u, string t, string d, string k){
			url=u;
			title = t;
			description = d;
			keywords = k;
		}
	}

	//
	global virtual override String getHTML(){
		return '';
	}
}