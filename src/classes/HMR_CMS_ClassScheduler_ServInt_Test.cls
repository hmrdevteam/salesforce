/*
Developer: Utkarsh Goswami (Mindtree)
Date: 19th Feb, 2018
Target Class(ses): HMR_CMS_ClassScheduler_ServiceInterface
*/

@isTest
private class HMR_CMS_ClassScheduler_ServInt_Test{
  

  @isTest static void testMethodForOnLoadRecords() {
  
       Account testAcc = new Account(Name = 'test');
        
        Contact testContact = new Contact( lastName = 'testcon',
                                            firstName = 'test',
                                            AccountId = testAcc.Id,
                                            Email = 'test@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000000'
                                    );
        insert testContact;
  
    // Implement test code
    HMR_CMS_ClassScheduler_ServiceInterface schedulerController = new HMR_CMS_ClassScheduler_ServiceInterface();
     
    
    Map<String, String> params = new Map<String, String>();
    params.put('contactId', testContact.Id);
    params.put('action', 'getDataOnLoad');
    
    String resp = schedulerController.executeRequest(params);
    //Map<String, Object> respMap = (Map<String, Object>)JSON.deserialize(resp, Map<String, Object>.class);
    
   // System.assert(Boolean.valueOf(respMap.get('success')));
   System.debug('resposne getDataOnLoad ->  ' + resp);
    
  }
  

  @isTest static void testMethodForSearchResultRecords() {
  
       Account testAcc = new Account(Name = 'test');
        
        Contact testContact = new Contact( lastName = 'testcon',
                                            firstName = 'test',
                                            AccountId = testAcc.Id,
                                            Email = 'test@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000000'
                                    );
        insert testContact;
  
    // Implement test code
    HMR_CMS_ClassScheduler_ServiceInterface schedulerController = new HMR_CMS_ClassScheduler_ServiceInterface();
     
    
    Map<String, String> params = new Map<String, String>();
    params.put('contactId', testContact.Id);
    params.put('action', 'getSearchResult');
    params.put('searchDate', String.valueOf(Date.today()));
    
    String resp = schedulerController.executeRequest(params);
   // System.assert(!String.isBlank(resp));
   
   System.debug('resposne getSearchResult ->  ' + resp);
    
  }
    
    @isTest static void testMethodForInvalidAction() { 
        
        Account testAcc = new Account(Name = 'test');
        
        Contact testContact = new Contact( lastName = 'testcon',
                                            firstName = 'test',
                                            AccountId = testAcc.Id,
                                            Email = 'test@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000000'
                                    );
        insert testContact;
  
    // Implement test code
        HMR_CMS_ClassScheduler_ServiceInterface schedulerController = new HMR_CMS_ClassScheduler_ServiceInterface();
         
        
        Map<String, String> params = new Map<String, String>();
        params.put('contactId', testContact.Id);
        params.put('action', 'invalid');
        
        String resp = schedulerController.executeRequest(params);
        
        System.debug('resposne getSearchResult ->  ' + resp);
    
  }
  
   @isTest static void testMethodForBlankContact() { 
        
        Account testAcc = new Account(Name = 'test');
        
        Contact testContact = new Contact( lastName = 'testcon',
                                            firstName = 'test',
                                            AccountId = testAcc.Id,
                                            Email = 'test@test.com',
                                            hmr_Contact_Original_Source__c = 'Social',
                                            hmr_Contact_Original_Source_Detail__c = 'Twitter',
                                            Community_User_ID__c = '000000000000000000'
                                    );
        insert testContact;
  
    // Implement test code
        HMR_CMS_ClassScheduler_ServiceInterface schedulerController = new HMR_CMS_ClassScheduler_ServiceInterface();
         
        
        Map<String, String> params = new Map<String, String>();
        params.put('action', 'invalid');
        
        String resp = schedulerController.executeRequest(params);
        
        System.debug('resposne getSearchResult ->  ' + resp);
    
  }
  
  
  @isTest static void testMethodForAssignClass() { 
        
         Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stan321', Email='standuser321@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName='test',LastName = 'test contact', Account = testAccountObj);
        insert testContatObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;


        Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;


        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;
  
    // Implement test code
        HMR_CMS_ClassScheduler_ServiceInterface schedulerController = new HMR_CMS_ClassScheduler_ServiceInterface();
         
        
        Map<String, String> params = new Map<String, String>();
        params.put('contactId', testContatObj.Id);
        params.put('action', 'assignClass');
        params.put('selectedClassId', classObj.Id);
        params.put('selectedClassDate', String.valueOf(Date.today()));
        
        String resp = schedulerController.executeRequest(params);
        
        System.debug('resposne getSearchResult ->  ' + resp);
    
  }
  
  @isTest static void testMethodForGetType() { 
        
      Type classtype = HMR_CMS_ClassScheduler_ServiceInterface.getType();  
      System.assertEquals(HMR_CMS_ClassScheduler_ServiceInterface.class, classType);    
    
  }
  
}