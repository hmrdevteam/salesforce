/**
* @Date: 4/7/2017
* @Author: Utkarsh Goswami (Mindtree)
* @JIRA: 
*
* This test class covers HMR_CC_Pmt_CreditCard_Pay_Ctrl  Class
*/


@isTest
private class HMR_CC_Pmt_CreditCard_Pay_Ctrl_Test{

    @isTest static void test_method_one() {
   
        
        Map<String, String> respCC = new Map<String, String>();
        respCC.put('Test','Test');
        
         Set<String> respKeys = new  Set<String>();
         respKeys.add('test');
        
        Map<String, Object> sigFld = new Map<String, Object>();
        sigFld.put('Test', 'Test SingFields');
        
        Map<String, Object> unsidFds = new Map<String, Object>();
        unsidFds.put('test', 'Un sing');
        
        List<String> signFldNamesLst = new List<String>();
        signFldNamesLst.add('Test Names');
        
         List<String> signUnFldNamesLst = new List<String>();
         signUnFldNamesLst.add('Test Names');
         
         Map<String, String> cuurUrlMap= new Map<String, String>();
        cuurUrlMap.put('test', 'Un sing');
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.setCurrentPageReference(new PageReference('Page.HMR_CC_Pmt_CreditCard_Pay')); 
        System.currentPageReference().getParameters().put('currentPageURL', 'visual.force.com');
        
        HMR_CC_Pmt_CreditCard_Pay_Ctrl  ccPay = new HMR_CC_Pmt_CreditCard_Pay_Ctrl();
        
        ccPay.blank = 'blank';
        ccPay.formAction = 'formaction';
        ccPay.signedDtString = 'Signed fa string';
        ccPay.iframe = 'test frame';
        ccPay.iframeParameters = 'iframe param';
        ccPay.joinedSignedFields = 'join fields test';
        ccPay.joinedUnsignedFields = 'test unsinged fields';
        ccPay.finalDataForSignatureCC = 'final data';
        ccPay.currentPageURL = 'test.cm';
        ccPay.portalUser = 'portal ds';
        ccPay.isCustomerLoggedIn = TRUE;
        ccPay.isCybersourceReponse = TRUE;
        ccPay.responseCC = respCC;
        ccPay.responseKeys = respKeys;
        ccPay.signedFields = sigFld;
        ccPay.unsignedFields = unsidFds;
        ccPay.signedFieldNames  = signFldNamesLst;
        ccPay.unsignedFieldNames = signUnFldNamesLst;
        ccPay.currentPageURLPageRefParams = cuurUrlMap;
        ccPay.getReferenceTime();
        HMR_CC_Pmt_CreditCard_Pay_Ctrl.uuid();
        HMR_CC_Pmt_CreditCard_Pay_Ctrl.signData('Test token');
        HMR_CC_Pmt_CreditCard_Pay_Ctrl.signQAData('Test token');
        ccPay.getUTCDateTime();
        HMR_CC_Pmt_CreditCard_Pay_Ctrl.getSignature(ctx,'data for sig',TRUE);

    }  

}