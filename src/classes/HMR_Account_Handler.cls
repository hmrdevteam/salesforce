/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/24/2017
 * Created By: Zach Engman
 * Last Modified Date: 06/19/2017
 * Last Modified By: Zach Engman
 * Description: Handler for the Account object triggers
 * ****************************************************/
public with sharing class HMR_Account_Handler 
{
     private boolean isExecuting = false;
     private integer batchSize = 0;
     public static Boolean isBeforeInsertFlag = false;
     public static Boolean isBeforeUpdateFlag = false;
    
    public HMR_Account_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }

    public void onBeforeInsert(List<Account> accountList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;
            //Standardize/Format the Field Data
            accountList = standardizeFieldFormat(accountList);
        }
    }

    public void onBeforeUpdate(List<Account> oldAccountList, List<Account> newAccountList, Map<Id, Account> oldAccountMap, Map<Id, Account> newAccountMap){
        if(!isBeforeUpdateFlag)
            isBeforeUpdateFlag = true;
        
        //Standardize/Format the Field Data
        newAccountList = standardizeFieldFormat(newAccountList);
    }

    //Data Hygiene: Format the created account name for HMR Customer Account records types to be "Capital Case"
    private List<Account> standardizeFieldFormat(List<Account> accountList){
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HMR Customer Account').getRecordTypeId();

        for(Account accountRecord : accountList){
            if(!String.isBlank(accountRecord.RecordTypeId) && accountRecord.RecordTypeId == customerRecordTypeId)
                accountRecord.Name = HMR_Format_Utility.capitalizeAll(accountRecord.Name);
        }

        return accountList;
    }
    
}