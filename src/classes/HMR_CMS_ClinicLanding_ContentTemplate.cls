/**
* Apex Content Template Controller for Clinic Landing Page
*
* @Date: 03/23/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 03/24/2017
* @JIRA:
*/

global virtual with sharing class HMR_CMS_ClinicLanding_ContentTemplate extends cms.ContentTemplateController{
    //Constructors Required (With and Without Argument)
    global HMR_CMS_ClinicLanding_ContentTemplate(cms.createContentController cc){
        super(cc);
    }

    global HMR_CMS_ClinicLanding_ContentTemplate(){
        super();
    }

    global HMR_CMS_ClinicLanding_ContentTemplate(cms.CoreController controller){
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    @TestVisible
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    //Hero Image Property
    public String heroImage{
        get{
            return getProperty('heroImage');
        }
    }

    //Max Miles Radius to Search
    //TODO: Find how to do Integer Property
    public String maxSearchMileRadius{
        get{
            return getProperty('maxSearchMileRadius');
        }
    }

    //Number of Providers to Show
    //TODO: Find how to do Integer Property
    public String maxProvidersDisplayed{
        get{
            return getProperty('maxProvidersDisplayed');
        }
    }

    //Nearby Provider (Account) List
    public List<Account> providerList {
        get{
            if(providerList == null)
                providerList = new List<Account>();
            return providerList;
        }
        set;
    }

    @RemoteAction
    global static List<String> getHTMLRemote(string providerListJSON){
        List<Account> providerList;
        HMR_CMS_ClinicLanding_ContentTemplate clinicLandingCT = new HMR_CMS_ClinicLanding_ContentTemplate();

        if(!String.isBlank(providerListJSON)){
            try{
                providerList = (List<Account>)JSON.deserialize(providerListJSON, List<Account>.class);
                clinicLandingCT.providerList = providerList;
            }
            catch(Exception ex){}//TODO - Right now will default to SOQL Query
        }
        
        String imgUri = '';
        
         for(Account prov : providerList){
             imgUri = prov.hmr_Location_Image_URI__c;
         }

        return new List<String> {clinicLandingCT.getProviderListingHTML(clinicLandingCT.providerList),
                                 clinicLandingCT.getProviderModalHTML(clinicLandingCT.providerList),
                                 imgUri};
    }

    @RemoteAction
    global static List<Lead> insertLeadRecords(List<Lead> leadList){
        insert leadList;
        return leadList;
    }

    global virtual override String getHTML() {
        String sitePrefix = Site.getBaseUrl();
        String html = '';
        html += '<div class="heros" id="heroimg" style="background-image: url(\'' + sitePrefix + heroImage + '\');">';
            html += String.format('<img class="top-img" src="{0}{1}" width="100%" />', new String[]{sitePrefix, heroImage});
            html += '<div class="content" id="providerListContent">';
                html += getProviderListingHTML(providerList);
            html += '</div>';
        html += '</div>';
        html += '<div class="clearfix"></div>';
        html += '<div class="program">';
            html += '<div class="content" id="providerListProgramContent">';
                html += getProviderListingHTML(providerList);
            html += '</div>';
        html += '</div>';
        html += '<div class="clearfix"></div>';
        //Modal Popup Section
        html += '<div id="providerModalSection" class="providerModalSection">';
        html += getProviderModalHTML(providerList);
        html += '</div>';

        return html;
    }
    private String getProviderListingHTML(List<Account> providerList){
        String html = '';
        integer providerCount = 0;
        if(providerList.size() > 0){
            html += '<h1 class="title">Great News!</h1>';
            html += '<div class="subtitle">There\'s an HMR Program near you.</div>';
            for(Account provider : providerList){
                providerCount++;
                html += '<div class="address">';
                html += String.format('<span class="bold">{0}</span><br />', new String[] {!String.isBlank(provider.HMR_Account_Name_1_Optional__c) ? provider.HMR_Account_Name_1_Optional__c : provider.Name});
                if(!String.isBlank(provider.HMR_Account_Name_1_Optional__c))
                    html += String.format('{0}<br />', new String[] {provider.Name});
                html += String.format('{0}<br />', new String[] {provider.BillingStreet});
                html += String.format('{0}, {1} {2}<br />', new String[] {provider.BillingCity, provider.BillingState, provider.BillingPostalCode});
                html += String.format('{0}<br />', new String[] { formatPhoneDisplay(provider.Phone) });
                //Distance Field may not Exist
                try{
                    html += String.format('<span class="bold">{0} miles</span><br />', new String[] { String.valueOf(Decimal.valueOf(String.valueOf(provider.get('Distance'))).setScale(1))});
                }
                catch(Exception ex){
                    html += '<span class="bold">&nbsp;</span><br />';
                }
                html += String.format('<span class="request-info-btn" data-toggle="modal" data-target="#{0}ProviderModal" data-id="', new String[] { providerList.size() > 1 ? 'Two' : 'One' });
                html += String.format('{0}', new String[] {provider.Name});
                html += '"onclick="defaultCheck(\'' + provider.Name + '\')">REQUEST INFORMATION</span>';
                html += '</div>';
            }
        }
        else{
            html += '<h3 class="title">There are no providers within your area</h3>';
        }
        return html;
    }
    private String getProviderSelectionHTML(List<Account> providerList){
        String html = '';
        integer providerCount = 0;
        if(providerList.size() > 0){
            for(Account provider : providerList){
                providerCount++;
                html += '<div class="address">';
                    html += '<div class="checkbox checkbox-primary">';
                            html += '<label>';
                            html += String.format('<input type="checkbox" name="primary" data-id="{0}" data-name="{1}" onclick="unCheck(this)"/>', new String[] {provider.Id, provider.Name});
                            html += '</label>';
                    html += '</div>';
                    html += '<div class="info">';
                            html += String.format('<span class="bold">{0}</span><br />', new String[] {provider.Name});
                            html += String.format('{0}<br />', new String[] {provider.BillingStreet});
                            html += String.format('{0}, {1} {2}<br />', new String[] {provider.BillingCity, provider.BillingState, provider.BillingPostalCode});
                            html += String.format('{0}<br />', new String[] { formatPhoneDisplay(provider.Phone) });
                            //Distance Field may not Exist
                            try{
                                html += String.format('<span class="bold miles">{0} miles</span><br />', new String[] { String.valueOf(Decimal.valueOf(String.valueOf(provider.get('Distance'))).setScale(1))});
                            }
                            catch(Exception ex){
                                html += '<span class="bold">&nbsp;</span><br />';
                            }
                    html += '</div>';
                 html += '</div>';

                 if(providerCount < providerList.size())
                    html += '<div class="clearfix"></div>';
            }
        }
        return html;
    }
    private String getProviderModalHTML(List<Account> providerList){
        String html = '';
        List<String> leadSourceOptions = getLeadSourcePickListValues();

        if(providerList.size() == 1){
            html += String.format('<div id="OneProviderModal" class="hmr-modal modal fade in" role="dialog" data-id="{0}" data-name="{1}">', new String[] {providerList[0].Id, providerList[0].Name});
                html += '<div class="modal-dialog">';
                        html += '<div class="modal-content">';
                            html += '<div class="modal-header">';
                                html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                    html += '<span aria-hidden="true">&times;</span>';
                                html += '</button>';
                                html += '<h4 class="modal-title">Get Started</h4>';
                            html += '</div>';
                            html += '<form class= "providerForm">';
                                html += '<div class="modal-body">';
                                    html += '<p class="subtitle">Complete the following form to receive more information about the HMR Program near you.</p>';
                                        html += '<div class="row">';
                                            html += '<div class="col-xs-12">';
                                                html += '<div class="form-group">';
                                                    html += '<label for="firstName">First Name</label>';
                                                    html += '<input type="text" class="form-control" id="firstName" aria-describedby="First Name" placeholder="Enter First Name"/>';
                                                html += '</div>';
                                                html += '<div class="form-group">';
                                                    html += '<label for="lastName">Last Name</label>';
                                                    html += '<input type="text" class="form-control" id="lastName" placeholder="Last Name"/>';
                                                html += '</div>';
                                                html += '<div class="form-group">';
                                                    html += '<label for="email">Email address</label>';
                                                    html += '<input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email"/>';
                                                html += '</div>';
                                                html += '<div class="form-group">';
                                                    html += '<label for="telInput">Telephone</label>';
                                                    html += '<input class="form-control" type="text" placeholder="1-(555)-555-5555" id="telInput"/>';
                                                html += '</div>';
                                                html += '<div class="form-group">';
                                                    html += '<label for="select1">How did you hear about us?</label>';
                                                    html += '<select class="form-control" id="select1">';
                                                       for(String leadSource : leadSourceOptions)
                                                            html += String.format('<option>{0}</option>', new String[] {leadSource});
                                                    html += '</select>';
                                                html += '</div>';
                                            html += '</div>';
                                        html += '</div>';
                                html += '</div>';
                                html += '<div class="modal-footer">';
                                    html += '<button type="button" class="btn btn-primary submit-btn" onclick="submitLeads();">Submit</button>';
                                html += '</div>';
                            html += '</form>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            }
            else{
                html += '<div id="TwoProviderModal" class="hmr-modal modal fade in" role="dialog">';
                    html += '<div class="modal-dialog">';
                        html += '<div class="modal-content">';
                            html += '<div class="modal-header">';
                                html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                    html += '<span aria-hidden="true">&times;</span>';
                                html += '</button>';
                                html += '<h4 class="modal-title">Get Started</h4>';
                            html += '</div>';
                            html += '<div class="modal-body">';
                                    html += '<p class="subtitle">Choose a location and complete the form to receive more information about the HMR Program near you.</p>';
                                html += '<form class= "providerForm">';
                                    html += '<div class="row">';
                                        html += '<div class="col-xs-12 col-sm-6">';
                                            html += getProviderSelectionHTML(providerList);
                                        html += '</div>';
                                        html += '<div class="col-xs-12 col-sm-6">';
                                            html += '<div class="form-group">';
                                                html += '<label for="firstName">First Name</label>';
                                                html += '<input type="text" class="form-control" id="firstName" aria-describedby="First Name" placeholder="Enter First Name"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="lastName">Last Name</label>';
                                                html += '<input type="text" class="form-control" id="lastName" placeholder="Last Name"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="email1">Email address</label>';
                                                html += '<input type="email" class="form-control" id="email1" aria-describedby="emailHelp" placeholder="Enter email"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="telInput">Telephone</label>';
                                                html += '<input class="form-control" type="text" placeholder="1-(555)-555-5555" id="telInput"/>';
                                            html += '</div>';
                                            html += '<div class="form-group">';
                                                html += '<label for="select1">How did you hear about us?</label>';
                                                html += '<select class="form-control" id="select1">';
                                                    for(String leadSource : leadSourceOptions)
                                                        html += String.format('<option>{0}</option>', new String[] {leadSource});
                                                html += '</select>';
                                            html += '</div>';
                                        html += '</div>';
                                    html += '</div>';
                                html += '</form>';
                            html += '</div>';
                            html += '<div class="modal-footer">';
                                html += '<button type="button" class="btn btn-primary submit-btn" onclick="submitLeads();">Submit</button>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            }
        return html;
    }
    /*private List<Account> getNearbyProviders(){
        integer searchMileRadius = maxSearchMileRadius != null && maxSearchMileRadius.isNumeric() ? Integer.valueOf(maxSearchMileRadius) : 50;
        integer resultLimit = maxProvidersDisplayed != null && maxProvidersDisplayed.isNumeric() ? Integer.valueOf(maxProvidersDisplayed) : 2;
        User currentUser = [SELECT Contact.MailingLatitude, Contact.MailingLongitude FROM User WHERE Id =: UserInfo.getUserId()];
        Decimal latitude = currentUser.Contact.MailingLatitude;
        Decimal longitude = currentUser.Contact.MailingLongitude;

        if(latitude == null || longitude == null){
            //TODO: for testing - default to this
            latitude = 44.556633;
            longitude = -90.606732;
        }

        //Return providers by distance
        List<Account> providerList = [SELECT Name,
                                             HMR_Account_Name_1_Optional__c,
                                             BillingStreet,
                                             BillingCity,
                                             BillingState,
                                             BillingPostalCode,
                                             Phone,
                                             DISTANCE(BillingAddress, GEOLOCATION( :latitude, :longitude ),'mi') Distance
                                      FROM Account
                                      WHERE DISTANCE(BillingAddress, GEOLOCATION( :latitude, :longitude ),'mi') < :searchMileRadius
                                      ORDER BY DISTANCE(BillingAddress, GEOLOCATION( :latitude, :longitude ),'mi')
                                      LIMIT :resultLimit];
        return providerList;
    }*/
    @TestVisible
    private String formatPhoneDisplay(string phone){
        String formattedPhone = !String.isBlank(phone) ? phone.replaceAll('[^0-9]','') : '';
        //If the length is 10, it is a number we can format nicely
        return formattedPhone.length() == 10 ? String.format('{0}.{1}.{2}', new String[] {formattedPhone.left(3), formattedPhone.mid(3,3), formattedPhone.right(4)}) :
                                               phone;
    }
    public List<String> getLeadSourcePickListValues(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Lead.How_did_you_hear_about_us__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        // pickListValuesList.add('Google or other search');
        // pickListValuesList.add('Friend or Family Referral');
        // pickListValuesList.add('Physician Referral');
        // pickListValuesList.add('I am a former HMR client');
        // pickListValuesList.add('In the News');
        // pickListValuesList.add('Online Ads');
        // pickListValuesList.add('Social media');
        // pickListValuesList.add('Other');

        return pickListValuesList;
    }
}