/**
* Test Class Coverage of the HMR_CC_Coupon_Selector Class
*
* @Date: 06/06/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Mustafa 23/01/2018
* @JIRA: 
*/

@isTest
private class HMR_CC_Coupon_Selector_Test {
	@testSetup
    private static void setupData(){
        insert new HMR_Coupon__c();
        insert new Program__c(Days_in_1st_Order_Cycle__c = 30);
    }
    
    @isTest
    private static void testGetCouponListByHMRCouponIdAndProgramIdAndOrderType(){
        List<HMR_Coupon__c> couponList = [SELECT Id FROM HMR_Coupon__c];
        List<Program__c> programList = [SELECT Id FROM Program__c];
        
        Test.startTest();

        List<ccrz__E_Coupon__c> resultList_P1 = HMR_CC_Coupon_Selector.getCouponListByHMRCouponIdAndProgramIdAndOrderType(couponList[0].Id, programList[0].Id, 'P1 Reorder');
        List<ccrz__E_Coupon__c> resultList_HSS = HMR_CC_Coupon_Selector.getCouponListByHMRCouponIdAndProgramIdAndOrderType(couponList[0].Id, programList[0].Id, 'HSS Reorder');
        List<ccrz__E_Coupon__c> resultList_P2 = HMR_CC_Coupon_Selector.getCouponListByHMRCouponIdAndProgramIdAndOrderType(couponList[0].Id, programList[0].Id, 'P2 Reorder');
		List<ccrz__E_Coupon__c> resultList_P21st = HMR_CC_Coupon_Selector.getCouponListByHMRCouponIdAndProgramIdAndOrderType(couponList[0].Id, programList[0].Id, 'P2 1st Order');
        List<ccrz__E_Coupon__c> resultList_P1HSS = HMR_CC_Coupon_Selector.getByHMRCouponIdAndOrderType(couponList[0].Id, 'P1-HSS Reorder');
        
        Test.stopTest();
        
        //Verify no data was returned
        System.assert(resultList_P1.size() == 0); 
        System.assert(resultList_HSS.size() == 0);
        System.assert(resultList_P2.size() == 0);  
        System.assert(resultList_P21st.size() == 0); 
        System.assert(resultList_P1HSS.size() == 0); 
    }
}