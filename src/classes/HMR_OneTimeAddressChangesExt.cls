/*****************************************************
 * Author: Nathan Anderson
 * Created Date: 21 June 2017
 * Created By: Nathan Anderson
 * Last Modified Date:
 * Last Modified By:
 * Description:
This logic is used by the HMR_OneTimeAddressChanges page, called from "Update Shipping Information" button on Order page layout.
This allows Program Specialists to make updates to Pending orders, selecting a different Shipping Address for this one time order.
 * ****************************************************/

public class HMR_OneTimeAddressChangesExt {

	//public variables
	public List<ccrz__E_AccountAddressBook__c> addressList {get;set;}
	public ccrz__E_Order__c order {get;set;}
	public Id orderId {get;set;}
	public Id selectedAddress{get;set;}

	public HMR_OneTimeAddressChangesExt(ApexPages.StandardController stdController) {
		//get the order id from the URL
		orderId = apexpages.currentpage().getparameters().get('id');
		//get the full record
		order = [SELECT Id, Name, ccrz__Contact__c, ccrz__User__c, ccrz__BillTo__c, ccrz__ShipTo__c, ccrz__TotalAmount__c,
		 				ccrz__Contact__r.FirstName, ccrz__Contact__r.LastName, ccrz__Contact__r.Client_Id__c
					FROM ccrz__E_Order__c WHERE Id = :orderId];
		//Query for all Contact Address records for this User
		addressList = [SELECT Id, Name, ccrz__E_ContactAddress__c, ccrz__Default__c, ccrz__AddressType__c, ccrz__E_ContactAddress__r.Name,
							ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c, ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
							ccrz__E_ContactAddress__r.ccrz__City__c, ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
							ccrz__E_ContactAddress__r.ccrz__PostalCode__c, ccrz__E_ContactAddress__r.ccrz__FirstName__c,
							ccrz__E_ContactAddress__r.ccrz__LastName__c, OwnerId
						FROM ccrz__E_AccountAddressBook__c WHERE OwnerId = :order.ccrz__User__c AND ccrz__AddressType__c = 'Shipping'];

	}

	public void selectAddress(){
		//get the id of the selected record from the page parameters
        selectedAddress = System.currentPagereference().getParameters().get('addressId');

    }

	public PageReference updateOrder(){
		//Initialize empty list of orders, to use to update
		List<ccrz__E_Order__c> ordersToUpdate = new List<ccrz__E_Order__c>();

		if(selectedAddress != null) {
			//Set the ShipTo field on the order to to the selected Contact Address record
			order.ccrz__ShipTo__c = selectedAddress;
			//Add updated record to list for DML update
			ordersToUpdate.add(order);

			//Run DML updates
			try{
				update ordersToUpdate;
			} catch(Exception e){
				ApexPages.addMessages(e);
	        }

			//return to Order page after complete
			PageReference orderPage = new PageReference('/'+order.Id);
			return orderPage;

		} else {
			//Throw an error if the update button is hit before an address is selected
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an address.'));

			return null;
		}

	}

	public PageReference cancel(){

        PageReference orderPage = new PageReference('/'+order.Id);

        return orderPage;

    }

}