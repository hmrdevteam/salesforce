/**
* Apex Controller for the HMR_CC_OrderView_PageIncl
*
* @Date: 2017-04-10
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-04-10
* @JIRA: 
*/
global with sharing class HMR_CC_OrderView_Controller {
    global HMR_CC_OrderView_Controller() {}
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateOrderDate(string orderId, string orderDate, boolean isCSRFlow){
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        //Get the related order
        List<ccrz__E_Order__c> orderList = [SELECT ccrz__OrderDate__c
                                                  ,hmr_Order_Type_c__c
                                                  ,PS_Reorder_Date_Change__c 
                                                  ,Reorder_Date_Change__c
                                                  ,Original_Order_Date__c 
                                            FROM ccrz__E_Order__c 
                                            WHERE ccrz__EncryptedId__c =: orderId];
        ccrz__E_Order__c orderRecord;
        Date newOrderDate = Date.parse(orderDate);
        DateTime newOrderDateTime = DateTime.newInstance(newOrderDate.Year(), newOrderDate.month(), newOrderDate.day());
        
        if(orderList.size() > 0){
            orderRecord = orderList[0];
            //Confirm the date has been changed
            if(orderRecord.ccrz__OrderDate__c != newOrderDate){
                //If the date was already changed from both possible scenarios, it cannot be changed again
                //For P2 1st Orders, CSR/PS Can update unlimited amount
                if(!orderRecord.Reorder_Date_Change__c 
                    || (!orderRecord.PS_Reorder_Date_Change__c && isCSRFlow) 
                    || (orderRecord.hmr_Order_Type_c__c == 'P2 1st Order' && isCSRFlow)){
                    
                    if(orderRecord.Original_Order_Date__c == null)
                        orderRecord.Original_Order_Date__c = orderRecord.ccrz__OrderDate__c;

                    orderRecord.ccrz__OrderDate__c = Date.parse(orderDate);
                    
                    //The date can be changed by a PS User for a second update
                    if(orderRecord.Reorder_Date_Change__c)
                        orderRecord.PS_Reorder_Date_Change__c = true;
                    else
                        orderRecord.Reorder_Date_Change__c = true;  
                    
                    update orderRecord;
                    result.success = true;
                    result.data = newOrderDateTime.format('MMMM dd, yyyy');
                }
                else{
                    result.success = false;
                    result.data = 'Order date has already been updated for this order and cannot be changed.';
                }
            }
            else{
                result.success = false;
                result.data = 'Order date has not been changed';
            }
        }
        else{
            result.success = false;
            result.data = 'Order was not found.  It may have been deleted.';
        }
        
        return result;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateDietStartDate(string contactId, string programMembershipId, string dietStartDate){
       ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
       Date newDietStartDate = Date.parse(dietStartDate);
       DateTime newDietStartDateTime = DateTime.newInstance(newDietStartDate.Year(), newDietStartDate.month(), newDietStartDate.day());
        
       HMR_CC_ProgramMembership_Service membershipService = new HMR_CC_ProgramMembership_Service();
       membershipService.updateDietStartDate(contactId, programMembershipId, newDietStartDate);
        
       result.success = true;
       result.data = newDietStartDateTime.format('MM/dd/yyyy');
        
       return result;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getOrderType(string orderId){
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        //Get the related order
        List<ccrz__E_Order__c> orderList = [SELECT hmr_Order_Type_c__c 
                                            FROM ccrz__E_Order__c 
                                            WHERE ccrz__EncryptedId__c =: orderId];
        ccrz__E_Order__c orderRecord;
        
        if(orderList.size() > 0){
            orderRecord = orderList[0];
            result.success = true;  
            if(!String.isBlank(orderRecord.hmr_Order_Type_c__c))
                result.data = orderRecord.hmr_Order_Type_c__c;
            else
                result.data = 'N/A';
        }
        else{
            result.success = false;
            result.data = 'Order was not found.  It may have been deleted.';
        }
        
        return result;
    }
    
}