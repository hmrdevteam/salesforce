/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/08/2018
 * Created By: Zach Engman
 * Last Modified Date: 04/04/2018
 * Last Modified By:
 * Description: REST service for tracker note operations
 * ****************************************************/
 @RestResource(urlMapping='/mobile/user/trackerNote/*')
global with sharing class HMRMobTrackerNote {
    @HttpPost
    global static void doPost(MobTrackerNoteDTO trackerNote) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        
        try {       
            Tracker_Note__c noteRecord = trackerNote.mapToTrackerNote();

            insert noteRecord;

            response.statusCode = 201;
            resultMap.put('trackerNote', new MobTrackerNoteDTO(noteRecord));
            resultMap.put('success', true);
            
        } catch (Exception e) {
            response.statusCode = 500;
            resultMap.put('success', false);
            resultMap.put('error', e.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * update the the tracker note record
     * @param MobTrackerNoteDTO
     */
    @HttpPatch
    global static String doPatch(MobTrackerNoteDTO trackerNote) {
        try {
            Tracker_Note__c noteRecord = trackerNote.mapToTrackerNote();

            update noteRecord;

            RestContext.response.statuscode = 204;

            return 'no content';
        }
        catch(Exception e) {
            RestContext.response.statuscode = 404;
            return e.getMessage();
        }       
    }
    
    /**
     * method to get the tracker note data
     * @param communityUserId - the userId of the respective user
     * @param offset - offset value of records to skip
     * @return List<MobTrackerNoteDTO> - the DTO list containing tracker notes ordered by desc (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if the contact is not found
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        RestRequest request = RestContext.request;
        Map<String, Object> resultMap = new Map<String, Object>();
        List<MobTrackerNoteDTO> trackerNoteList;
        Integer totalNoteCount;

        String communityUserId = RestContext.request.params.get('communityUserId');
        String offset = RestContext.request.params.get('offset');
        String todayOnly = RestContext.request.params.get('todayOnly');

        if(String.isBlank(communityUserId)){
            communityUserId = UserInfo.getUserId();
        }

        if(String.isBlank(offset) || !offset.isNumeric()){
            offset = '0';
        }

        if(todayOnly != null && todayOnly == 'true'){
            trackerNoteList = getTrackerNotesForToday(communityUserId, Integer.valueOf(offset));
            totalNoteCount = Integer.valueOf([SELECT Count(Id) recordCount FROM Tracker_Note__c WHERE Contact__r.Community_User_ID__c =: communityUserId AND Date_Time__c = TODAY][0].get('recordCount'));
        }
        else{
            trackerNoteList = getTrackerNotes(communityUserId, Integer.valueOf(offset));
            totalNoteCount = Integer.valueOf([SELECT Count(Id) recordCount FROM Tracker_Note__c WHERE Contact__r.Community_User_ID__c =: communityUserId][0].get('recordCount'));
        }

        RestContext.response.statusCode = 200;
         
        resultMap.put('success', true);
        resultMap.put('trackerNoteList', trackerNoteList);
        resultMap.put('totalNoteCount', totalNoteCount);
  
        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
  
    /**
     * this method gets the note information for the specified community user and offset
     * and can be re-used to aggregate data in other services
     */
    global static List<MobTrackerNoteDTO> getTrackerNotes(string communityUserId, integer offset) {
        List<MobTrackerNoteDTO> trackerNoteList = new List<MobTrackerNoteDTO>();

        for(Tracker_Note__c noteRecord : [SELECT Contact__c,
                                                 Date_Time__c,
                                                 Name,
                                                 Notes__c
                                          FROM Tracker_Note__c
                                          WHERE Contact__r.Community_User_ID__c =: communityUserId
                                          ORDER BY Date_Time__c DESC
                                          LIMIT 50
                                          OFFSET : offset]){
            trackerNoteList.add(new MobTrackerNoteDTO(noteRecord));
        }
        
        return trackerNoteList;
    }  

    /**
     * this method gets the tracker notes for the specified community user and offset and date
     * and can be re-used to aggregate data in other services
     */
    global static List<MobTrackerNoteDTO> getTrackerNotesForToday(string communityUserId, integer offset) {
        List<MobTrackerNoteDTO> trackerNoteList = new List<MobTrackerNoteDTO>();

        for(Tracker_Note__c noteRecord : [SELECT Contact__c,
                                                 Date_Time__c,
                                                 Name,
                                                 Notes__c
                                          FROM Tracker_Note__c
                                          WHERE Contact__r.Community_User_ID__c =: communityUserId
                                          AND Date_Time__c = TODAY
                                          ORDER BY Date_Time__c DESC
                                          LIMIT 50
                                          OFFSET : offset]){
            trackerNoteList.add(new MobTrackerNoteDTO(noteRecord));
        }
        
        return trackerNoteList;
    }  

    /**
     * method to delete the tracker note record
     * @param id - the record id 
     * @return HTTP/1.1 200 success
     * @return HTTP/1.1 404 not found if error is encountered
     */
    @HttpDelete
    global static void doDelete() {
        Map<String, Object> returnMap = new Map<String, Object>();
        RestRequest request = RestContext.request;
        String recordId = request.params.get('id');
        
        try{
            delete new Tracker_Note__c(Id = recordId);

            RestContext.response.statusCode = 200;

            returnMap.put('success', true);
        }
        catch(Exception ex){
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
            returnMap.put('error', ex.getMessage());
        }

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnMap)); 
    }      
    
    /**
     * global DTO class that represents a flat structure of the
     * Tracker Note sObject
     */
    global class MobTrackerNoteDTO {
        public string id {get; set;}
        public string contactId {get; set;}
        public string noteDateTime {get; set;}
        public string title {get; set;}
        public string note {get; set;}
        
        /**
         * constructor
         */
        public MobTrackerNoteDTO(Tracker_Note__c trackerNoteRecord) {
            this.id = trackerNoteRecord.Id;
            this.contactId = trackerNoteRecord.Contact__c;
            //check if we need to format this to GMT or store as separate date and time fields
            this.title = !(trackerNoteRecord.Name instanceOf ID) ? trackerNoteRecord.Name.stripHtmlTags() : '';
            this.noteDateTime = trackerNoteRecord.Date_Time__c.format();
            this.note = !String.isBlank(trackerNoteRecord.Notes__c) ? trackerNoteRecord.Notes__c.stripHtmlTags() : '';
        }

        /**
         * utility method to map the fields from this object to a Tracker_Note__c object
         * @return Tracker_Note__c
         */
        public Tracker_Note__c mapToTrackerNote() {
            Tracker_Note__c trackerNoteRecord = new Tracker_Note__c(
                Id = this.Id
               ,Contact__c = this.contactId
               ,Date_Time__c = DateTime.parse(this.noteDateTime)
               ,Name = this.title
               ,Notes__c = this.note
            );
            
            return trackerNoteRecord;
        }
    }
}