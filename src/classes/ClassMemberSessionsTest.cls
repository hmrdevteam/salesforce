/* Test class for ClassMemberSessions
 * @Date: 12/16/2016
 * @Author Mustafa Ahmed (HMR)
 * @JIRA: https://reside.jira.com/browse/HPRP-751
 * TODO: @Aslesha increase the coverage to more than 83% in future sprints
 */

@isTest(SeeAllData=False)

public class ClassMemberSessionsTest {

    static testMethod void firstTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        //create and insert a user
        User userObj = new User(Alias = 'stan321', Email='standuser321@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing321', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com');
        insert userObj;

        //create and insert a account
        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        //create and insert a contact
        Contact testContactObj = new Contact(FirstName = 'ftest', LastName = 'testcontact', Email = 'test@testing.com', Account = testAccountObj);
        insert testContactObj;

        //create and insert a coach
        Coach__c coachObj = new Coach__c(hmr_Coach__c = userObj.id, hmr_Email__c = 'test@testing.com', hmr_Class_Strength__c = 20);
        insert coachObj;

        //create and insert a program
         Program__c programObj = new Program__c(Name = 'test program', Days_in_1st_Order_Cycle__c = 21, hmr_Has_Phone_Coaching__c = true);
        insert programObj;

        //create and insert a class
        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Coach__c = coachObj.id, Program__c = programObj.id,
                                         hmr_Active_Date__c = Date.today(), hmr_Class_Type__c = 'P1 PP', hmr_First_Class_Date__c = Date.today(),
                                         hmr_Time_of_Day__c = 'Noon',hmr_Start_Time__c = '12:00 PM', hmr_End_Time__c = '1:00 PM',hmr_Coference_Call_Number__c =
                                         '1234567890', hmr_Participant_Code__c = 'Placeholder', hmr_Host_Code__c = 'Placeholder');
        insert classObj;

        //create and insert a coaching session
        Coaching_Session__c coachingSessObj = new Coaching_Session__c(hmr_Class__c = classObj.id, hmr_Class_Date__c = Date.today(), hmr_Start_Time__c = '12:00 PM',
                                                                       hmr_End_Time__c = '1:00 PM', hmr_Coach__c = coachObj.id);
        insert coachingSessObj;

        //create and insert a class member
        Class_Member__c classMemberObj = new Class_Member__c(hmr_Contact_Name__c = testContactObj.Id, hmr_Class__c = classObj.id);
        insert classMemberObj;

        //create and insert a session attendee
        Session_Attendee__c sessionAttendeeObj = new Session_Attendee__c(hmr_Client_Name__c = testContactObj.Id, hmr_Coaching_Session__c = coachingSessObj.id, hmr_Class_Member__c = classMemberObj.id);
        insert sessionAttendeeObj;

        //create and insert a program membership
        Program_Membership__c programMemObj = new Program_Membership__c(Program__c = programObj.id, hmr_Status__c = 'Active',hmr_Contact__c = testContactObj.id);
        insert programMemObj;

        //load the page with the session attendee id
        Test.setCurrentPageReference(new PageReference('Page.ClientMakeupSession'));
        System.currentPageReference().getParameters().put('id', sessionAttendeeObj.id);


        List<Coaching_Session__c> searchedClassListTest = new List<Coaching_Session__c>();
        searchedClassListTest.add(coachingSessObj);

        List<Session_Attendee__c> searchedSessionListTest = new List<Session_Attendee__c>();
        searchedSessionListTest.add(sessionAttendeeObj);

        Set<Id> classWithMembershipSetTest = new Set<Id>();
        classWithMembershipSetTest.add(classObj.id);

        Session_Attendee__c SAL = new Session_Attendee__c();
        ApexPages.StandardController ctrl = new ApexPages.StandardController(SAL);
        ClassMemberSessions controllerObj = new ClassMemberSessions((ctrl));

        //assign the values in the controller and run all the methods
        controllerObj.sessionAtt = sessionAttendeeObj;
        controllerObj.sessionDate = controllerObj.sessionAtt.hmr_Class_Date__c;
        controllerObj.parentClassMembership = classMemberObj;
        controllerObj.parentClass = classObj;

        controllerObj.noRecords = FALSE;
        controllerObj.success = FALSE;
        controllerObj.error = TRUE;
        controllerObj.selectedSession = coachingSessObj.id;
        controllerObj.sessionDate = Date.today();
        controllerObj.moreRecordLink = TRUE;
        controllerObj.recordsToShow = 12;
        controllerObj.numberOfRecords = 12;
        controllerObj.previous();
        controllerObj.next();
        controllerObj.getprev();
        controllerObj.getnxt();
        controllerObj.loadMoreResults();
        controllerObj.selectSessionId();
        controllerObj.assignSession();

        Test.stopTest();
    }

     static testMethod void secondTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stanUs1', Email='standusers3227@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing3217', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanusesr7@org.com');
        insert userObj;

        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName = 'stest', LastName = 'testcontact', Account = testAccountObj);
        insert testContatObj;

        Test.setCurrentPageReference(new PageReference('Page.ClientMakeupSession'));
        System.currentPageReference().getParameters().put('id', '13534');
        String idFromUrl = apexpages.currentpage().getparameters().get('id');

        Session_Attendee__c SAL = new Session_Attendee__c();
        ApexPages.StandardController ctrl = new ApexPages.StandardController(SAL);
        ClassMemberSessions controllerObj = new ClassMemberSessions((ctrl));

        controllerObj.noRecords = FALSE;
        controllerObj.getprev();
        controllerObj.getnxt();

        System.assertEquals('13534', idFromUrl);

        Test.stopTest();
    }

     static testMethod void thirdTestMethod(){

        Test.startTest();
        Profile prof = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User userObj = new User(Alias = 'stan3212', Email='standusers321@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing3212', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, hmr_IsCoach__c = TRUE,
                                TimeZoneSidKey='America/Los_Angeles', UserName='stanusesr@org.com');
        insert userObj;


        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        Contact testContatObj = new Contact(FirstName = 'ttest', LastName = 'testcontact', Account = testAccountObj);
        insert testContatObj;

        Test.setCurrentPageReference(new PageReference('Page.ClientMakeupSession'));

        Session_Attendee__c SAL = new Session_Attendee__c();
        ApexPages.StandardController ctrl = new ApexPages.StandardController(SAL);
        ClassMemberSessions controllerObj = new ClassMemberSessions((ctrl));

        controllerObj.noRecords = FALSE;
        controllerObj.getprev();
        controllerObj.getnxt();

        Test.stopTest();
    }
}