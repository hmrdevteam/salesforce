/**
* Class to create Case, when user submits contact us form
* Creating CASE
* Objects referenced --> CASE
*
* @Date: 2016-01-16
* @Author Utkarsh Goswami (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-1745
*/

global with sharing class HMR_CMS_ContactUs_ServiceInterface implements cms.ServiceInterface {

    private static Pattern brPattern = Pattern.compile('<br( )?(/)?>');
    private static Pattern tagPattern = Pattern.compile('<.*?>');


   /**
    * Service method called from Contactus JS
    *
    *  @param Map<String, String> --> Map of data from the contact us page
    *  @return String -> JSON String
    */


    public String executeRequest(Map<String, String> params) {
     //   String action = params.get('action');

        try {
            String action = params.get('action');
            if(action == 'getContact') {
                return getContactCaseJSON(params);
            }
        } catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());

            return '{"success":false,"message":' + JSON.serialize(message) + '}';
        }

        // No actions matched and no error occurred
        return '{"success":false,"message":"Invalid Action"}';
    }

     /**
    * Called from the Service method on 'getContact' action
    *
    *  @param Map<String, String> --> Map of data from the contact us page
    *  @return String -> JSON String
    */


    private String getContactCaseJSON(Map<String, String> params) {

        String returnJSON = '';
        String issue = params.get('issue');
        String subject = params.get('subject');
        String description = params.get('description');
		String userId = params.get('userId');
        String email = params.get('email');
        String guestname = params.get('guestname');
        String phone = params.get('phone');

        String caseOrigin = 'New';

        if(!String.isBlank(caseOrigin)){

            try{
                User userObj = [SELECT Id, ContactId FROM User WHERE Id = :userId];

                Case caseObject= new Case();
                //check to see if the user has a ContactId -- this would eliminate guest users
                if(userObj.ContactId != null){
                    //map the contact field for logged in users
                    caseObject.ContactId = userObj.ContactId;
                } else {
                    //map the "web" fields for guest users
                    caseObject.SuppliedName = guestname;
                    caseObject.SuppliedEmail = email;
                    caseObject.SuppliedPhone = phone;
                }


                caseObject.Type = issue;
                caseObject.Origin = 'Community';
                caseObject.Status = 'New';
                caseObject.Subject = subject;
                caseObject.Description = description;
                Database.DMLOptions dmo = new Database.DMLOptions();
				dmo.assignmentRuleHeader.useDefaultRule = true;
                dmo.EmailHeader.TriggerUserEmail = true;
				Database.insert(caseObject, dmo);
               	//insert caseObject;

                 returnJSON = '{"success":"true","URL":"","message":"Case registered successfully"}';
            }
            catch(Exception ex){
                returnJSON = '{"success":"false","URL":"","message":"Exception in Case Submission"}';
            }

        }

        return returnJSON;

    }

    public static Type getType() {
        return HMR_CMS_RegistrationServiceInterface.class;
    }

}