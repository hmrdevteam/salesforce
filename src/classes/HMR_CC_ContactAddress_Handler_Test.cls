/**
* Test Class Coverage of the HMR_ContactAddress_Handler Class
*
* @Date: 05/23/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/
@isTest
private class HMR_CC_ContactAddress_Handler_Test {
    @isTest
    private static void testFieldFormatsOnInsert(){
        ccrz__E_ContactAddr__c contactAddressRecord = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'first',
                                                                                 ccrz__LastName__c = 'last',
                                                                                 ccrz__MiddleName__c = 'middle',
                                                                                 ccrz__AddressFirstline__c = '1234 main street',
                                                                                 ccrz__AddressSecondline__c = 'apartment 1b',
                                                                                 ccrz__City__c = 'boston',
                                                                                 ccrz__State__c = 'massachusetts',
                                                                                 ccrz__Country__c = 'united states',
                                                                                 ccrz__CountryISOCode__c = 'us',
                                                                                 ccrz__StateISOCode__c = 'ma');
        
        Test.startTest();
        
        insert contactAddressRecord;
        
        Test.stopTest();
        
        ccrz__E_ContactAddr__c contactAddressResult = [SELECT ccrz__FirstName__c, ccrz__LastName__c, ccrz__City__c, ccrz__CountryISOCode__c FROM ccrz__E_ContactAddr__c WHERE Id =: contactAddressRecord.Id];
        //Verify the capitalization was updated
        System.assert(contactAddressResult.ccrz__FirstName__c.contains('First'));
        System.assert(contactAddressResult.ccrz__LastName__c.contains('Last'));
        System.assert(contactAddressResult.ccrz__City__c.contains('Boston'));
        System.assert(contactAddressResult.ccrz__CountryISOCode__c.contains('US'));
    }

    @isTest
    private static void testFieldFormatsOnUpdate(){
        ccrz__E_ContactAddr__c contactAddressRecord = new ccrz__E_ContactAddr__c(ccrz__FirstName__c = 'first',
                                                                                 ccrz__LastName__c = 'last',
                                                                                 ccrz__MiddleName__c = 'middle',
                                                                                 ccrz__AddressFirstline__c = '1234 main street',
                                                                                 ccrz__AddressSecondline__c = 'apartment 1b',
                                                                                 ccrz__City__c = 'boston',
                                                                                 ccrz__State__c = 'massachusetts',
                                                                                 ccrz__Country__c = 'united states',
                                                                                 ccrz__CountryISOCode__c = 'us',
                                                                                 ccrz__StateISOCode__c = 'ma');
        
        insert contactAddressRecord;


        Test.startTest();
        
        ccrz__E_ContactAddr__c contactAddressToUpdateRecord = [SELECT ccrz__FirstName__c, ccrz__LastName__c, ccrz__City__c, ccrz__CountryISOCode__c FROM ccrz__E_ContactAddr__c WHERE Id =: contactAddressRecord.Id];
        contactAddressToUpdateRecord.ccrz__FirstName__c = 'first';
        contactAddressToUpdateRecord.ccrz__LastName__c = 'last';
        contactAddressToUpdateRecord.ccrz__City__c = 'boston';
        contactAddressToUpdateRecord.ccrz__CountryISOCode__c = 'us';
        update contactAddressToUpdateRecord;
        
        Test.stopTest();
        
        ccrz__E_ContactAddr__c contactAddressResult = [SELECT ccrz__FirstName__c, ccrz__LastName__c, ccrz__City__c, ccrz__CountryISOCode__c FROM ccrz__E_ContactAddr__c WHERE Id =: contactAddressRecord.Id];
        //Verify the capitalization was updated
        System.assert(contactAddressResult.ccrz__FirstName__c.contains('First'));
        System.assert(contactAddressResult.ccrz__LastName__c.contains('Last'));
        System.assert(contactAddressResult.ccrz__City__c.contains('Boston'));
        System.assert(contactAddressResult.ccrz__CountryISOCode__c.contains('US'));
    }
}