/**
* HMR Program Community Force.com Site URL Rewriter Class
* This class implements the Salesforce Site.UrlRewriter class to handle both CC and OCMS
* incoming page URL's and call CC and OCMS Rewriter Class references appropriately
* Classes referenced --> cms.OrchestraCMSRewriter, ccrz.cc_SiteURLRewriter
*
* ABBREVIATIONS: CC --> CloudCraze, OCMS --> Orchestra Content Management System
*
* @Date: 2017-02-16
* @Modified:
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA: related to this story with no sub task, https://reside.jira.com/browse/HPRP-2496
*/
global class HMR_SiteUrlRewriter implements Site.UrlRewriter {
    //instantitate CC and OCMS rewrite objects
    cms.OrchestraCMSRewriter cmsRewriterObj = new cms.OrchestraCMSRewriter();
    ccrz.cc_SiteURLRewriter ccrzRewriterObj = new ccrz.cc_SiteURLRewriter();

    /**
    *  mapRequestUrl, override salesforce mapRequestUrl method
    *  method to return the OCMS or CC Page reference based on the friendlyUrl page reference passed by Salesforce
    *
    *  @param friendlyUrl: the incoming friendlyUrl from Salesforce
    *  @return PageReference - the actual page reference returned from OCMS or CC based on param: friendlyUrl
    */
    global PageReference mapRequestUrl(PageReference friendlyUrl) {
        //get the Page Url
        String pageUrl = friendlyUrl.getUrl();
        String originalURLParameters = '';

        System.debug('@@@@@@@@@');
        System.debug('@@@@@@@@@ pageUrl');
        System.debug(pageUrl);

        String pageNameTest = pageUrl.substringAfter('/recipes/');
        System.debug('@@@@@@@@@');
        System.debug('@@@@@@@@@ pageNameTest');
        System.debug(pageNameTest);

        if(pageUrl.contains('?')) {
            pageUrl = pageUrl.substringBefore('?');
            originalURLParameters = pageUrl.substringAfter('?');
        }

        String pageNameUrl = pageUrl.substringAfter('/');

        List<HMR_Url_Mapping__mdt> HMRURLMappingList =
            [SELECT Id, DeveloperName, MasterLabel, Active__c, New_URL__c, Page_Type__c, Salesforce_URL__c FROM HMR_Url_Mapping__mdt
                WHERE Active__c = TRUE AND New_URL__c = :pageNameUrl];

        Set<Id> HMRURLMappingIds = new Set<Id>();

        if(HMRURLMappingList != null && HMRURLMappingList.size() > 0) {
            for(HMR_Url_Mapping__mdt HMRURLMapping: HMRURLMappingList) {
                HMRURLMappingIds.add(HMRURLMapping.Id);
            }
            List<HMR_URL_Parameter__mdt> HMRURLParameters =
                [SELECT Id, DeveloperName, MasterLabel, Parent_URL_Mapping__c, Parameter_Key__c, Parameter_Value__c FROM HMR_URL_Parameter__mdt
                    WHERE Parent_URL_Mapping__c IN :HMRURLMappingIds];

            String newPageURLParams = '?';

            if(HMRURLParameters != null && HMRURLParameters.size() > 0) {
                for(HMR_URL_Parameter__mdt HMRURLParameter: HMRURLParameters) {
                    newPageURLParams += HMRURLParameter.Parameter_Key__c + '=' + HMRURLParameter.Parameter_Value__c + '&';
                 }
            }
            else {
                if(originalURLParameters == '') {
                    newPageURLParams = '';
                }
                else {
                    newPageURLParams += originalURLParameters;
                }
            }
            return new PageReference('/' + HMRURLMappingList[0].Salesforce_URL__c + newPageURLParams);
        }
        else {
            //check if the url is a CC or OCMS url
            //NOTE: OCMS urls startswith 'cms' and CC url's starts with 'ccrz'
            if(pageUrl.startsWith('/ccrz')) {
                //pass the incoming Salesforce friendlyUrl to CC rewriter Object
                return ccrzRewriterObj.mapRequestUrl(friendlyUrl);
            }
            else if(pageUrl.startsWith('/c__')) {
                return new PageReference(pageUrl);
            }
            else if(pageUrl.startsWith('/recipes/')) {
                String pageName = pageUrl.substringAfter('/recipes/');
                if(!String.isBlank(pageName)) {
                    if(pageName.contains('/')) {
                        String pageDetailURL = pageName.substringAfterLast('/');
                        return cmsRewriterObj.mapRequestUrl(new PageReference('/recipe-detail?name='+ pageDetailURL));
                    }
                    else {
                        return cmsRewriterObj.mapRequestUrl(new PageReference('/' + pageName));
                    }
                }
                else {
                    return cmsRewriterObj.mapRequestUrl(new PageReference('/' + pageName));
                }
            }
            else if(pageUrl.startsWith('/resources/community-success-stories/')) {
                String pageName = pageUrl.substringAfter('/resources/community-success-stories/');

                if(!String.isBlank(pageName))
                    return cmsRewriterObj.mapRequestUrl(new PageReference('/success-story-user-detail?name=' + pageName));
                else
                    return cmsRewriterObj.mapRequestUrl(new PageReference(pageUrl));
                
            }
            else if(pageUrl.startsWith('/resources/forum/')) {
                String pageName = pageUrl.substringAfter('/resources/forum/');
                if(!String.isBlank(pageName)) {
                    if(pageName.contains('/')) {
                        String pageDetailURL = pageName.substringAfterLast('/');
                        return cmsRewriterObj.mapRequestUrl(new PageReference(pageDetailUrl));
                    }
                    else {
                        return cmsRewriterObj.mapRequestUrl(new PageReference('/questions-' + pageName));
                    }
                }
                else {
                    return cmsRewriterObj.mapRequestUrl(new PageReference('/' + pageName));
                }
            }
            else if(pageUrl.startsWith('/resources/')) {
                String pageName = pageUrl.substringAfter('/resources/');
                if(!String.isBlank(pageName)) {
                    if(pageName.contains('/')) {
                        String pageDetailURL = pageName.substringAfterLast('/');
                        return cmsRewriterObj.mapRequestUrl(new PageReference('/article-detail?name=' + pageDetailUrl));
                    }
                    else {
                        if(pageName.containsIgnoreCase('forum') || pageName.containsIgnoreCase('success-stories'))
                            return cmsRewriterObj.mapRequestUrl(new PageReference('/' + pageName));
                        else
                            return cmsRewriterObj.mapRequestUrl(new PageReference('/knowledge-' + pageName));
                    }
                }
                else {
                    return cmsRewriterObj.mapRequestUrl(new PageReference('/' + pageName));
                }
            }
            else {
                return cmsRewriterObj.mapRequestUrl(friendlyUrl);
            }
        }
        return null;
    }

    /**
    *  generateUrlFor, override salesforce generateUrlFor method
    *  method to return the List of Page references that are combined CC and OCMS
    *
    *  @param salesforceUrls: the list of incoming page references from Salesforce
    *  @return List of PageReference - the list of page references that includes both CC and OCMS list of Pagereferences
    */
    global List<PageReference> generateUrlFor(List<PageReference> salesforceUrls) {
        //instantitate List of Page references to hold CC and OCMS return Page references
        List<PageReference> cmsUrls = new List<PageReference>();
        List<PageReference> ccrzUrls = new List<PageReference>();
        //instantitate List of Page reference to be returned
        List<PageReference> returnUrls = new List<PageReference>();

        //loop throught the incoming salesforce page references
        for(PageReference incomingSalesforceUrl : salesforceUrls) {
            //get the url from incoming salesforce page references
            String url = incomingSalesforceUrl.getUrl();
            //check if the url is a CC or OCMS url
            if(url.startsWith('/ccrz')){
                //add the incoming Salesforce page reference to CC Page reference list
                ccrzUrls.add(new PageReference(url));
            }
            else {
                //add the incoming Salesforce page reference to OCMS Page reference list
                cmsUrls.add(new PageReference(url));
            }
        }
        //loop through the list of OCMS list of page references by calling generateUrlFor method for OCMS rewrtier Object
        for(PageReference pageRef: cmsRewriterObj.generateUrlFor(cmsUrls)) {
            //add the OCMS page reference to the list of page references to be returned
            returnUrls.add(pageRef);
        }
        //loop through the list of CCRZ list of page references by calling generateUrlFor method for CC rewrtier Object
        for(PageReference pageRef: ccrzRewriterObj.generateUrlFor(ccrzUrls)) {
            //add the CC page reference to the list of page references to be returned
            returnUrls.add(pageRef);
        }
        return returnUrls;
    }
}