/**
* Holiday Utility Class
* Contains methods to determine if a date is a holiday date configured in ORG Company Holidays 
* this Utility only covers Yearly Recurring Holidays, if you have configured a recurring holiday 
* recurring Monthly, Weekly or Daily, this Utility will not return any Holiday Name
* 
* Objects referenced --> Holiday 
* 
* @Date: 2016-11-18
* @Modified: 2016-12-08
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 
* @JIRA: Story https://reside.jira.com/browse/HPRP-615, Task https://reside.jira.com/browse/HPRP-688
*/

public class HolidayUtility {
	public static List<Holiday> companyHolidays = [SELECT Id, ActivityDate, RecurrenceType, RecurrenceDayOfMonth, RecurrenceMonthOfYear,  
								EndTimeInMinutes, IsAllDay, IsRecurrence, Name, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly,
								RecurrenceInstance, RecurrenceInterval, RecurrenceStartDate, StartTimeInMinutes				
								from Holiday];
	//use to get the week day string from the Recurrence Mask flag in Holiday record
	public static final Map<Integer, String> recurrenceDayOfWeekMaskSet = new Map<Integer, String>{
		1 => 'Sunday', 
		2 => 'Monday', 
		4 => 'Tuesday', 
		8 => 'Wednesday', 
		16 => 'Thursday', 
		32 => 'Friday', 
		64 => 'Saturday'
	};
	//use to get the Month string from the current date get month which is an integer
	public static final Map<Integer, String> monthsMap = new Map<Integer, String>{
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December'
	};
	//use to get the recurrence instance of weekday (Mon, Tues, Wed, etc.) from the current date
	public static final Map<Integer, String> recurrenceStr = new Map<Integer, String>{
		1 => 'First',
		2 => 'Second',
		3 => 'Third',
		5 => 'Last'
	};

	/**
	* constructor 
	* Initializing the companyHolidays list in the constructor
	* Querying all the holiday records in the ORG
	*
    */
	public HolidayUtility() {
		/*companyHolidays = [SELECT Id, ActivityDate, RecurrenceType, RecurrenceDayOfMonth, RecurrenceMonthOfYear,  
								EndTimeInMinutes, IsAllDay, IsRecurrence, Name, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly,
								RecurrenceInstance, RecurrenceInterval, RecurrenceStartDate, StartTimeInMinutes				
								from Holiday];*/
	}


	/**
	*  dayOfWeek
	*  Utility function to return String text of day of week
	*  based on the date passed
	*
	*  @param dateToCheck in Date datatype
	*  @return String - day of the week e.g. Monday, Tuesday, etc.
	*/
	public static String dayOfWeek(Date dateToCheck){	   
		String day = '';
		if(dateToCheck != null) {
			Datetime dt = (DateTime)(dateToCheck + 1);
			day = dt.format('EEEE'); 
		}
		return day;
	}	

	/**
	*  isHoliday
	*  Utility function to check if present date is official 
	*  registered holiday in the org (object Holiday)
	* 
	*  @param currentDate 
	*  @return String - Name of the Holiday if currentDate is a holiday.
	*/           
    public static String isHoliday(Date currentDate){	    
	    //declare the Holiday name string to be returned
	    String holidayNameStr = '';
	    //loop through the company holidays
        for(Holiday hDay:companyHolidays){
        	//compare against the recurring holidays	        	        	        
	        if(hDay.IsRecurrence){
	        	//get the recurring month of the year from the holiday
	        	String recurrenceMonthoftheYear = hDay.RecurrenceMonthOfYear;
	        	//get String month from current date
	        	String monthOfDate = monthsMap.get(currentDate.month());
	        	//only compares against for Yearly recurring holidays
	        	//TODO: add logic to compare against Monthly, Weekly and Daily Recurrences
	        	if(hDay.RecurrenceType == 'RecursYearly' || hDay.RecurrenceType == 'RecursYearlyNth') {
	        		//if the recurrence is on a specific Month Day of the month
	        		//i.e. July 4th, Dec 25th, Dec 31st, etc
	        		if(hDay.RecurrenceDayOfMonth != null){
	        			//in this scenario, need to check the day of the current date against the recurrence day of the month
	        			//and compare the month of date against the Month of the recurring Holiday
		        		if(currentDate.day() == hDay.RecurrenceDayOfMonth && monthOfDate.toLowerCase() == recurrenceMonthoftheYear.toLowerCase()){
		        			//set the holiday name string
		        			holidayNameStr = hDay.Name;
		        			//exit out of loop, end of comparison, no need to compare against the remaining holiday records
		        			break;
		        		}
		        	}
		        	else {  
		        		//this block compares against holidays that are on a specific weekday of the month
		        		//for e.g. Last Monday of May is Memorial day, First Monday of September is Labor dat   
		        		//get the day of the week string                   
                    	String dayOfWeek =  dayOfWeek(currentDate);   
                    	//Mathematical formula to get an Int number for the recurrenc of the week day in the month
                    	//for e.g. if the date is Dec 8 2016, then the below int will be 2 
                    	//becuase Dec 8 is on the second Thursday of the month of Dec in Year 2016
                    	Integer recurrenceInstanceFortheWeekDayInt = (Integer)Math.ceil(currentDate.day() / 7.0);
                    	//now from this Int we need the str recurrence instance like First, Second, Third, Fourth, Last
                    	//as this is what the holiday record contains
                    	//declare the string
                    	String recurrenceInstanceFortheWeekDayStr = '';                    	
                    	//the only logic that needs to get compare is if the int is 4
                    	//as there may or may not be a fifth or last weekday for that month
                    	if(recurrenceInstanceFortheWeekDayInt == 4) {
                    		//check if you add 7 days to the date and the month changes
                    		//then that weekday is the last weekday of the month
                    		if(currentDate.addDays(7).month() > currentDate.month()) {
                    			recurrenceInstanceFortheWeekDayStr = 'Last';
                    		}
                    		else {
                    			//else it is the fourth weekday of the month
                    			recurrenceInstanceFortheWeekDayStr = 'Fourth';
                    		}
                    	}
                    	else {
                    		//for recurrence int values 1,2,3,5 get the string value from the Map
                    		recurrenceInstanceFortheWeekDayStr = recurrenceStr.get(recurrenceInstanceFortheWeekDayInt);
                    	}                    	
                    	//here 3 conditions needs to be satisfied
                    	//Last Monday of May, 
                    	//Last (weekday recurrence instance)
                    	//Monday (weekday of the recurring Holiday)
                    	//May (month of the recurring Holiday)
                    	if(monthOfDate.toLowerCase() == recurrenceMonthoftheYear.toLowerCase() &&
                    		dayOfWeek.toLowerCase() == recurrenceDayOfWeekMaskSet.get(hDay.RecurrenceDayOfWeekMask).toLowerCase() &&
                    		recurrenceInstanceFortheWeekDayStr.toLowerCase() ==  hDay.RecurrenceInstance.toLowerCase()) {
                    		//set the Holiday string
                    		holidayNameStr = hDay.Name;
                    		//exit out of loop, end of comparison, no need to compare against the remaining holiday records
                    		break;
                    	}                     
	        		}
	        	}		     
	        }	
	        else {
	        	//if the Holiday is not a recurring holiday, just call daysBetween to verify the same date
	        	if(currentDate.daysBetween(hDay.ActivityDate) == 0) {
	        		//set the Holiday string
            		holidayNameStr = hDay.Name;
            		//exit out of loop, end of comparison, no need to compare against the remaining holiday records
            		break;
	        	}
	        }
	        
	    }
	    //return the Holiday name string
	    return holidayNameStr;
	}       
}