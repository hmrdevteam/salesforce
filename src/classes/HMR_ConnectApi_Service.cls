/**
* Apex Service Class for ConnectApi Functionality
*
* @Date: 2017-05-11
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-05-11
* @JIRA: 
*/

public with sharing class HMR_ConnectApi_Service {
    public string communityId = '';

    public HMR_ConnectApi_Service(){
        this.communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;
    }
    
    public ConnectApi.FeedElement postQuestionAndAnswer(string subject, string body){    
        return postQuestionAndAnswer('me', subject, body);
    }
    
    public ConnectApi.FeedElement postQuestionAndAnswerWithRichText(string subjectId, string subject, string body){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.QuestionAndAnswersCapabilityInput questionAndAnswersCapabilityInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        feedItemInput.subjectId = subjectId;
        
        messageBodyInput.messageSegments = messageBodyInput.messageSegments = ConnectApiHelper.getMessageSegmentInputs(body);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        feedElementCapabilitiesInput.questionAndAnswers = questionAndAnswersCapabilityInput;
        questionAndAnswersCapabilityInput.questionTitle = subject;
        
        ConnectApi.FeedElement insertedFeedElement = ConnectApi.ChatterFeeds.postFeedElement(communityId, feedItemInput);
        return insertedFeedElement;
    }
    
    public ConnectApi.FeedElement postQuestionAndAnswer(string subjectId, string subject, string body){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.QuestionAndAnswersCapabilityInput questionAndAnswersCapabilityInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        feedItemInput.subjectId = subjectId;
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        textSegmentInput.text = body;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        feedElementCapabilitiesInput.questionAndAnswers = questionAndAnswersCapabilityInput;
        questionAndAnswersCapabilityInput.questionTitle = subject;
        
        ConnectApi.FeedElement insertedFeedElement = ConnectApi.ChatterFeeds.postFeedElement(communityId, feedItemInput);
        return insertedFeedElement;
    }
    
    public ConnectApi.FeedElement updateQuestionAndAnswer(string feedElementId, string subjectId, string subject, string body){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.QuestionAndAnswersCapabilityInput questionAndAnswersCapabilityInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        textSegmentInput.text = body;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        feedElementCapabilitiesInput.questionAndAnswers = questionAndAnswersCapabilityInput;
        questionAndAnswersCapabilityInput.questionTitle = subject;
        
        ConnectApi.FeedElement editedFeedElement = ConnectApi.ChatterFeeds.updateFeedElement(communityId, feedElementId, feedItemInput);
        
        if(!String.isBlank(subjectId))
            ConnectApi.Topics.assignTopic(communityId, feedElementId, subjectId);
        
        return editedFeedElement;
    }
    
    public ConnectApi.FeedElement updateQuestionAndAnswerWithRichText(string feedElementId, string subjectId, string subject, string body){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.QuestionAndAnswersCapabilityInput questionAndAnswersCapabilityInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = ConnectApiHelper.getMessageSegmentInputs(body);

        feedItemInput.body = messageBodyInput;

        questionAndAnswersCapabilityInput.questionTitle = subject;
        feedElementCapabilitiesInput.questionAndAnswers = questionAndAnswersCapabilityInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        ConnectApi.FeedElement editedFeedElement = ConnectApi.ChatterFeeds.updateFeedElement(communityId, feedElementId, feedItemInput);
        
        if(!String.isBlank(subjectId))
            ConnectApi.Topics.assignTopic(communityId, feedElementId, subjectId);

        return editedFeedElement;
    }
    
    public List<QuestionResultRecord> getQuestionsByTopicId(string topicId){
        List<QuestionResultRecord> questionResultList = new List<QuestionResultRecord>();
        
        if(!String.isBlank(topicId)){
            ConnectApi.FeedElementPage feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topicId, '', null, ConnectApi.FeedSortOrder.Relevance);
            
            if(feedElementPage != null && feedElementPage.elements.size() > 0){
                for(ConnectApi.FeedElement feedElement : feedElementPage.elements){
                    if(feedElement.capabilities.questionAndAnswers != null)
                        questionResultList.add(new QuestionResultRecord(feedElement));
                }
            }
        }
        
        questionResultList.sort();
        
        return questionResultList;
    }
    
    public List<QuestionResultRecord> getMostPopularQuestions(){
        List< QuestionResultRecord> mostPopularQuestionList = new List<QuestionResultRecord>();
        HMR_Topic_Service topicService = new HMR_Topic_Service();
        List<Category_Map__mdt> topicList = topicService.getTopicList();

        for(Category_Map__mdt topic : topicList){
            string topicId = topicService.getTopicId(topic.Topic_Name__c);
            if(!String.isBlank(topicId)){
                ConnectApi.FeedElementPage feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topicId, '', null, ConnectApi.FeedSortOrder.Relevance);
                if(feedElementPage != null && feedElementPage.elements.size() > 0){
                    for(ConnectApi.FeedElement feedElement : feedElementPage.elements){
                        if(feedElement.capabilities.questionAndAnswers != null)
                            mostPopularQuestionList.add(new QuestionResultRecord(feedElement));
                    }
                }
            }
        }    
        
        mostPopularQuestionList.sort();
        
        return mostPopularQuestionList; 
    }
    
    public class QuestionResultRecord implements Comparable{
        public string id {get; set;}
        public decimal rating {get; set;}
        public string title {get; set;}
        public string urlLink {get; set;}
        public boolean hasBestAnswer{get; set;}
        public integer commentCount{get; set;}
        public List<String> relatedTopicList{get; set;}
        
        public QuestionResultRecord(ConnectApi.FeedElement feedElement){
            this.id = feedElement.id;
            this.title = feedElement.capabilities.questionAndAnswers.questionTitle;
            this.urlLink = feedElement.url;
            this.hasBestAnswer = feedElement.capabilities.questionAndAnswers.bestAnswer != null;
            this.relatedTopicList = new List<String>();
            if(feedElement.capabilities.comments != null && feedElement.capabilities.comments.page != null)
                this.commentCount = feedElement.capabilities.comments.page.total;
            if(feedElement.capabilities.topics != null && feedElement.capabilities.topics.items != null){
                for(ConnectApi.Topic topic : feedElement.capabilities.topics.items){
                    if(topic.nonLocalizedName != 'HMR Suggested') //Skip HMR Suggested
                        relatedTopicList.add(topic.nonLocalizedName);
                }
            }
        }
        
        public Integer compareTo(Object compareTo){
            QuestionResultRecord compareToResultRecord = (QuestionResultRecord)compareTo;     
            return commentCount == compareToResultRecord.commentCount ? 0 : commentCount < compareToResultRecord.commentCount ? 1 : -1;
        }
    }
}