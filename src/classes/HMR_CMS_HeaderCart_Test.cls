/*
Developer: Rafa Hernandez (Magnet360)
Date: March 21st 2017
Log: Missing Meaningful Asserts (if needed) and test for parameter method.
Target Class(ses): HMR_CMS_HeaderCart_ContentTemplate
*/

@isTest
private class HMR_CMS_HeaderCart_Test {
	private static String title = 'my_article_title';
    private static String summary = 'my_article_summary';
	@isTest static void test_method_global() {
		// Implement test code
		HMR_CMS_HeaderCart_ContentTemplate headerCall = new HMR_CMS_HeaderCart_ContentTemplate();
		String propertyValue = headerCall.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
		headerCall.testAttributes = new Map<String, String> {
            'title' => title,
            'summary' => summary
        };
        String cartlabel = headerCall.CartLabel;
        String cartImage = headerCall.CartIconImage;
       	Profile profileId = [SELECT Id FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];
       	Cookie currCartId = ApexPages.currentPage().getCookies().get('currCartId');
       	currCartId = new Cookie('currCartId','1231231231231',null,-1,false);

       	ApexPages.currentPage().setCookies(new Cookie[]{currCartId});
       	String renderHMTLcurrentUser = headerCall.getHTML();
        Integer rand = Math.round(Math.random()*1000);
		cc_dataFActory dataFActory = new cc_dataFActory();
		cc_dataFActory.setupTestUser();
		cc_dataFActory.setupTestUser1();
		ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
		newCart = cc_dataFActory.createCart();
		cc_dataFActory.createRootCategories();
		cc_dataFActory.setupCatalog();
		//cc_dataFActory.createExternalOrders(10);
		Id productId = cc_dataFActory.products.get(6).Id;
		cc_dataFActory.addCartItem(newCart,productId,1,100.54);
		String renderHMTL = headerCall.getHTML();
         Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccApiCart.CARTSTATUS => 'Open',
                ccrz.ccApiCart.ACTIVECART => true,                   
                ccrz.ccAPI.API_VERSION => 3,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                        //ccrz.ccAPI.SZ_REL => new List<String>(),
                        //ccrz.ccAPI.SZ_ASSC => false
                    }
                }
        };
		Integer itemCartNumer = headerCall.getCartItemCount(inputData);
		system.assertEquals(itemCartNumer,1);
		String renderHMTLcurrentUserWithCart = headerCall.getHTML();
		System.runAs(cc_dataFActory.testUser) {
  //       // The following code runs as user 'u' 
         String renderHMTLasUser = headerCall.getHTML();
		}

	}
		@isTest static void test_method_guest_user() {

			HMR_CMS_HeaderCart_ContentTemplate headerCallGuestUser = new HMR_CMS_HeaderCart_ContentTemplate();
			Cookie currCartId = ApexPages.currentPage().getCookies().get('currCartId');
	       	currCartId = new Cookie('currCartId','1231231231231',null,-1,false);
			ApexPages.currentPage().setCookies(new Cookie[]{currCartId});
       		Profile profileIdGuest = [SELECT Id FROM Profile WHERE Name = 'HMR Program Community Profile' LIMIT 1];
			
			cc_dataFActory dataFActoryGuest = new cc_dataFActory();
			cc_dataFActory.setupTestGuest();
			system.debug(cc_dataFActory.testUserGuest.profileId);
			System.runAs(cc_dataFActory.testUserGuest) {
         		String renderHMTLGuest = headerCallGuestUser.getHTML();
			}

		}
}