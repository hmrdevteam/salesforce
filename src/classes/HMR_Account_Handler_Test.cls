/**
* Test Class Coverage of the HMR_Account_Handler Class
*
* @Date: 05/23/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/19/2017
* @JIRA:
*/
@isTest
private class HMR_Account_Handler_Test {
    @isTest
    private static void testFieldFormatsOnInsert(){
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HMR Customer Account').getRecordTypeId();
        Account accountRecord = new Account(Name= 'test account', RecordTypeId = customerRecordTypeId);
        
        Test.startTest();
        
        insert accountRecord;
        
        Test.stopTest();
        
        Account accountResult = [SELECT Name FROM Account WHERE Id =: accountRecord.Id];
        //Verify the capitalization was updated
        System.assert(accountResult.Name.contains('Test Account'));
    }
    
    @isTest
    private static void testFieldFormatsOnUpdate(){
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HMR Customer Account').getRecordTypeId();
        Account accountRecord = new Account(Name= 'test account', RecordTypeId = customerRecordTypeId);
        insert accountRecord;
        
        Account accountRecordToUpdate = [SELECT Name, RecordTypeId FROM Account WHERE Id =: accountRecord.Id];
        
        Test.startTest();
        
        accountRecordToUpdate.Name = 'test account';
        update accountRecordToUpdate;
        
        Test.stopTest();
        
        Account accountResult = [SELECT Name FROM Account WHERE Id =: accountRecordToUpdate.Id];
        //Verify the capitalization was updated
        System.assert(accountResult.Name.contains('Test Account'));
    }
}