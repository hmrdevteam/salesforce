/**
* Remote Extension to get CC Coupons from HMR Coupon Code entered by user on the Cart View
* @Date: 2017-1-23
* @Author Pranay Mistry (Magnet 360)
* @updated by: 6-4-2017 Aslesha(Mindtree)
* @update: check portal user flag to show the special discount section
* @JIRA:
*/
global class CC_HMR_Coupon_Ext {

    public String specialDiscFlag {get; set;}
    public Id CategoryIdForRedirection {get;set;}
    public CC_HMR_Coupon_Ext () {
         specialDiscFlag = 'No';
         String chekPortalUser = Apexpages.currentPage().getParameters().get('portalUser');
         if(chekPortalUser != null && chekPortalUser != '') {
            specialDiscFlag='Yes';
         }
         else
            specialDiscFlag='No';
        /*
        List<PermissionSetAssignment> lstcurrentUserPerSet =    [   SELECT Id, PermissionSet.Name,AssigneeId
                                                                FROM PermissionSetAssignment
                                                                WHERE AssigneeId = :Userinfo.getUserId() ];
        system.debug('lstcurrentUserPerSet====>' + lstcurrentUserPerSet);

        for (PermissionSetAssignment psa: lstcurrentUserPerSet)
        {
            system.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
            if(psa.PermissionSet.Name.equals('Program Specialist'))
                specialDiscFlag='Yes';
            else
              specialDiscFlag='No';
        }
         */

       CategoryIdForRedirection = [Select Id from ccrz__E_Category__c where Name='HMR Products' limit 1].Id;
    }

    //Method to apply cc Coupons to cart on click of '#addCouponBtn'
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCCCoupons(final ccrz.cc_RemoteActionContext ctx, String hmrCoupon,
                                                            List<String> cartProductIds, String contactId){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        System.debug('XX! in ccCoupons' + contactId);
        if(contactId == ''){
            System.debug('XX! empty' + contactId);
        }
        try {
            Map<String, ccrz__E_Coupon__c> dataMap = new Map<String, ccrz__E_Coupon__c>();

            //If contact is an HMR Employee coupons can't be applied to their orders
            List<Contact> empContact = [SELECT Id, Name FROM Contact WHERE Id = :contactId AND Account.Name = 'HMR Employees' LIMIT 1];
            if(!empContact.isEmpty()){
                res.success = false;
                res.data = dataMap;
                return res;
            }
            //Retrieve HMR coupon entered by client
            List<HMR_Coupon__c> HMRCoupons = [SELECT Id, Name FROM HMR_Coupon__c WHERE Name = :hmrCoupon AND hmr_Active__c = True AND Name NOT IN ('ProgramDiscount', 'ProgramMember') LIMIT 1];
            HMR_Coupon__c cc_HMRCoupon;
            if(!HMRCoupons.isEmpty()) {
                cc_HMRCoupon = HMRCoupons[0];
            }
            else{
                res.success = false;
                res.data = dataMap;
                return res;
            }

            //Retrieve Standard Interim order coupon
            List<HMR_Coupon__c> interimHMRCoupons = [SELECT Id, Name FROM HMR_Coupon__c WHERE Name = 'ProgramMember' AND hmr_Active__c = True LIMIT 1];
            System.debug('!Interim  ' + interimHMRCoupons.size());
            HMR_Coupon__c interimHMRCoupon;
            if(!interimHMRCoupons.isEmpty()){
                interimHMRCoupon = interimHMRCoupons[0];
            }
            //Create Map to store cart contents
            Map<Id, ccrz__E_Product__c> cc_CartProducts = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, Program__c FROM ccrz__E_Product__c WHERE Id IN :cartProductIds]);
            //variable to store program id associated to a kit
            Id programId;
            //coupon order type
            String orderType;
            //coupon Interim order type
            String interimOrderType;
            Boolean prgMember = false;
            String prgPhase;
            //If contactId parameter is filled check to see if client has an existing program membership
            if(contactId != ''){
                List<Program_Membership__c> progMembership = new List<Program_Membership__c>();
                progMembership = [SELECT Id, Name, hmr_Contact__c, Program__r.Name  FROM Program_Membership__c
                                    WHERE hmr_Status__c = 'Active' AND hmr_First_Order_Number__c <> null AND hmr_Contact__c = :contactId LIMIT 1];
                if(!progMembership.isEmpty()){
                    prgMember = true;
                    //Get Name of program phase
                    prgPhase = progMembership[0].Program__r.Name;
                }
            }
            //Set appropriate interim oder type based on Phase
            if(prgPhase != null){
                if(prgPhase == 'P1 Healthy Solutions'){
                    interimOrderType = 'P1 Interim';
                }
                else if(prgPhase == 'P1 Healthy Shakes'){
                     interimOrderType = 'HSS Interim';
                }
                else if(prgPhase == 'Phase 2'){
                    interimOrderType = 'P2 Interim';
                }
                else{
                    interimHMRCoupon = null;
                }
            }
            //Loop through cart items to determine if the cart contains a dynamic kit associated to a program
            for(ccrz__E_Product__c cartProd: cc_CartProducts.values()) {
                if(cartProd.Program__c != null) {
                    programId = cartProd.Program__c;
                    break;
                }
            }
            //If an associated program is found, set order type of coupon depending on the program phase
            if(programId != null){
                List<Program__c> kitProgram = new List<Program__c>([SELECT Id, Name FROM Program__c
                    WHERE Id = :programId]);
                if(!kitProgram.isEmpty()){
                    if(kitProgram[0].Name == 'P1 Healthy Shakes'){
                        orderType = 'HSS 1st Order';
                    }
                    else if(kitProgram[0].Name == 'P1 Healthy Solutions'){
                        orderType = 'P1 1st Order';
                    }
                    else if(kitProgram[0].Name == 'Phase 2'){
                        orderType = 'P2';
                    }
                }
            }
            //Query the associated cc Coupon to be applied to this order
            //if cart contains a kit and this client is not already a Program Member, apply the appropriate 1st order type coupon
            List<ccrz__E_Coupon__c> ccCoupons;
            if(orderType != null && prgMember == false) {
                ccCoupons = new List<ccrz__E_Coupon__c>(
                    [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c, hmr_Program__c FROM ccrz__E_Coupon__c
                        WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND hmr_Program__c = :programId AND Coupon_Order_Type__c = :orderType]);
            }
            //if cart does not contain a kit, coupon order type will be Interim if client is already a program member and A La Carte if they are not
            else {
                if(prgMember == true && interimOrderType != null){
                    ccCoupons = new List<ccrz__E_Coupon__c>(
                        [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                            WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);

                    if(ccCoupons.isEmpty()  && interimHMRCoupon != null ){
                        ccCoupons = new List<ccrz__E_Coupon__c>(
                            [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                                WHERE HMR_Coupon__c = :interimHMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);
                    }
                }
                else if(prgMember == true && interimHMRCoupon != null && interimOrderType != null){
                    ccCoupons = new List<ccrz__E_Coupon__c>(
                        [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                            WHERE HMR_Coupon__c = :interimHMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);
                }
                else {
                    ccCoupons = new List<ccrz__E_Coupon__c>(
                        [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                            WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = 'A la Carte']);
                }
            }

            System.debug('ccCoupons size #getCCCoupons' + ccCoupons.size());
            for(ccrz__E_Coupon__c ccCoupon: ccCoupons){
                dataMap.put(String.valueOf(ccCoupon.Id), ccCoupon);
            }
            //If ccCoupon with matching criteria was not found return res.success = false
            if(ccCoupons.isEmpty()){
                res.success = false;
            }
            //ccCoupon found, return success = true
            else{
                res.success = true;
            }
            res.data = dataMap;
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getCCCoupons ' + res + ' ' + err.getMessage());
            res.success = false;
            res.data = err.getMessage();
        }
        res.success = true;
        return null;
    }

    //Method automatically applies coupon to cart if client is part of an Account with an associated HMR Coupon
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCCSavedCoupons(final ccrz.cc_RemoteActionContext ctx, String accountId, List<String> cartProductIds,
                                                                String contactId, String currentCoupon){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            Map<String, ccrz__E_Coupon__c> dataMap = new Map<String, ccrz__E_Coupon__c>();
            if(accountId == null || accountId == ''){
                accountId = '';
            }
            if(currentCoupon == null || currentCoupon == ''){
                currentCoupon = '';
            }
            Boolean exisitngDefaultCoupon = false;
            //Retrieve current HMR coupon applied to cart if exists
            List<HMR_Coupon__c> currentHMRCoupons = [SELECT Id, Name FROM HMR_Coupon__c WHERE Name = :currentCoupon AND  Default_Consumer_Coupon__c = True LIMIT 1];
            if(!currentHMRCoupons.isEmpty() || currentCoupon.contains('ProgramMember')){
                exisitngDefaultCoupon = true;
            }
            //If contact is an HMR Employee coupons can't be applied to their orders
            List<Account> empAccount = [SELECT Id, Name FROM Account WHERE Id = :accountId AND Account.Name = 'HMR Employees' LIMIT 1];
            if(!empAccount.isEmpty()){
                res.success = false;
                if(exisitngDefaultCoupon == false){
                    res.data = dataMap;
                }
                else{
                    res.data = 'Remove Coupon';
                }
                return res;
            }
            //Retrieve HMR coupon associated to client's account
            List<HMR_Coupon__c> HMRCoupons = [SELECT Id, Name, Account__r.RecordType.DeveloperName FROM HMR_Coupon__c WHERE Account__c = :accountId AND hmr_Active__c = True LIMIT 1];
            //Retrieve Standard Interim order coupon
            List<HMR_Coupon__c> interimHMRCoupons = [SELECT Id, Name FROM HMR_Coupon__c WHERE Name = 'ProgramMember' AND hmr_Active__c = True LIMIT 1];
            //Retrieve MASTER 1st order coupon
            List<HMR_Coupon__c> masterHMRCoupons = [SELECT Id, Name FROM HMR_Coupon__c WHERE Name = 'ProgramDiscount' AND  Default_Consumer_Coupon__c = True AND hmr_Active__c = True LIMIT 1];
            HMR_Coupon__c cc_HMRCoupon; HMR_Coupon__c interimHMRCoupon; HMR_Coupon__c cc_MASTERCoupon;
            if(!HMRCoupons.isEmpty()){
                cc_HMRCoupon = HMRCoupons[0];
            }
            if(!interimHMRCoupons.isEmpty()){
                interimHMRCoupon = interimHMRCoupons[0];
            }
            if(!masterHMRCoupons.isEmpty() && (HMRCoupons.isEmpty() || HMRCoupons[0].Account__r.RecordType.DeveloperName != 'Corporate_Account')){
                cc_MASTERCoupon = masterHMRCoupons[0];
            }
            //Declare map to store cart contents
            Map<Id, ccrz__E_Product__c> cc_CartProducts = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, Program__c, ccrz__ProductType__c FROM ccrz__E_Product__c WHERE Id IN :cartProductIds]);

            //variable to store program id associated to a kit
            Id programId;
            //coupon order type
            String orderType;
            //coupon Interim order type
            String interimOrderType;

            Boolean prgMember = false;
            String prgPhase;
            //Check to see if client already has an active program membership
            List<Program_Membership__c> progMembership = new List<Program_Membership__c>();
             progMembership = [SELECT Id, Name, hmr_Contact__c, Program__r.Name  FROM Program_Membership__c
                WHERE hmr_Status__c = 'Active' AND hmr_First_Order_Number__c <> null AND hmr_Contact__c = :contactId];
            if(!progMembership.isEmpty()){
                prgMember = true;
                //Get Name of program phase
                prgPhase = progMembership[0].Program__r.Name;
            }
            //Loop through cart items to determine if the cart contains a dynamic kit associated to a program
            for(ccrz__E_Product__c cartProd: cc_CartProducts.values()) {
                if(cartProd.Program__c != null && cartProd.ccrz__ProductType__c == 'Dynamic Kit') {
                    programId = cartProd.Program__c;
                    break;
                }
            }
            //If an associated program is found, set order type of coupon depending on the program phase
            if(programId != null && prgMember == false){
                List<Program__c> kitProgram = new List<Program__c>(
                    [SELECT Id, Name FROM Program__c WHERE Id = :programId]);
                if(!kitProgram.isEmpty()){
                    if(kitProgram[0].Name == 'P1 Healthy Shakes'){
                        orderType = 'HSS 1st Order';
                        if(cc_MASTERCoupon != null && cc_HMRCoupon == null){
                            cc_HMRCoupon = cc_MASTERCoupon;
                        }
                    }
                    else if(kitProgram[0].Name == 'P1 Healthy Solutions'){
                        orderType = 'P1 1st Order';
                        if(cc_MASTERCoupon != null && cc_HMRCoupon == null){
                            cc_HMRCoupon = cc_MASTERCoupon;
                        }
                    }
                    else if(kitProgram[0].Name == 'Phase 2'){
                        orderType = 'P2';
                        if(cc_MASTERCoupon != null && cc_HMRCoupon == null){
                            cc_HMRCoupon = cc_MASTERCoupon;
                        }
                    }
                }
            }
            //Set appropriate interim order type based on Phase
            if(prgPhase != null){
                if(prgPhase == 'P1 Healthy Solutions'){
                    interimOrderType = 'P1 Interim';
                }
                else if(prgPhase == 'P1 Healthy Shakes'){
                     interimOrderType = 'HSS Interim';
                }
                else if(prgPhase == 'Phase 2'){
                    interimOrderType = 'P2 Interim';
                }
                else{
                    interimHMRCoupon = null;
                }
            }
            //Query the associated cc Coupon to be applied to this order
            //if cart contains a kit and this client is not already a Program Member, apply the appropriate 1st order type coupon
            List<ccrz__E_Coupon__c> ccCoupons;
            if(orderType != null && cc_HMRCoupon != null) {
                ccCoupons = new List<ccrz__E_Coupon__c>(
                    [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c, hmr_Program__c FROM ccrz__E_Coupon__c
                   WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND hmr_Program__c = :programId AND Coupon_Order_Type__c = :orderType]);
            }
            //if cart does not contain a kit, coupon order type will be Interim if client is already a program member and A La Carte if they are not
            else {
                if(prgMember == true && interimOrderType != null){
                    if(cc_HMRCoupon != null){
                        ccCoupons = new List<ccrz__E_Coupon__c>(
                            [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                                WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);

                        if(ccCoupons.isEmpty() && interimHMRCoupon != null){
                            ccCoupons = new List<ccrz__E_Coupon__c>(
                                [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                                    WHERE HMR_Coupon__c = :interimHMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);
                        }
                    }
                    else if(interimHMRCoupon != null){
                        ccCoupons = new List<ccrz__E_Coupon__c>(
                            [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                                WHERE HMR_Coupon__c = :interimHMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = :interimOrderType]);
                    }
                }
                else if(cc_HMRCoupon != null){
                    ccCoupons = new List<ccrz__E_Coupon__c>(
                        [SELECT Id, ccrz__CouponName__c, ccrz__CouponCode__c, HMR_Coupon__c FROM ccrz__E_Coupon__c
                            WHERE HMR_Coupon__c = :cc_HMRCoupon.Id AND ccrz__Enabled__c = True AND Coupon_Order_Type__c = 'A la Carte']);
                }
                else{
                    res.success = false;
                    if(exisitngDefaultCoupon == false){
                        res.data = dataMap;
                    }
                    else{
                        res.data = 'Remove Coupon';
                    }
                    return res;
                }
            }
            System.debug('ccCoupons size getCCSavedCoupons' + ccCoupons.size());
            for(ccrz__E_Coupon__c ccCoupon: ccCoupons){
                dataMap.put(String.valueOf(ccCoupon.Id), ccCoupon);
            }
            //If ccCoupon with matching criteria was not found return res.success = false
            if(ccCoupons.isEmpty()){
                res.success = false;
                if(exisitngDefaultCoupon == false){
                    res.data = dataMap;
                }
                else{
                    res.data = 'Remove Coupon';
                }
            }
            //ccCoupon found, return success = true
            else{
                res.success = true;
                res.data = dataMap;
            }
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getCCSavedCoupons ' + res + ' ' + err.getMessage());
            res.success = false;
            res.data = err.getMessage();
        }
        res.success = true;
        return null;
    }

    /*
          This method is used to add discunt to the cart mannually
          JIRA:https://reside.jira.com/browse/HPRP-2642
    */
    @RemoteAction
    global static ccrz.cc_RemoteActionResult adjustHMRDiscount(final ccrz.cc_RemoteActionContext ctx,final String cartId, Decimal discountAmount){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.success = false;
        ccrz__E_Cart__c cart = [SELECT Id,ccrz__AdjustmentAmount__c,ccrz__AdjustmentReason__c,ccrz__SubtotalAmount__c,ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE  ccrz__EncryptedId__c=:cartId ];
        cart.ccrz__AdjustmentAmount__c=discountAmount * -1;
        update cart;
        res.success = true;
        return res;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getExtendedDataForProducts(final ccrz.cc_RemoteActionContext ctx, Id productId ){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            Map<String,ccrz__E_Product__c > dataMap = new Map<String, ccrz__E_Product__c>();
            Map<Id, ccrz__E_Product__c> cc_ProductsMap = new Map<Id, ccrz__E_Product__c>(
                [SELECT Id, Name, hmr_Ingredients__c, ccrz__ShortDesc__c, ccrz__SKU__c,
                    (SELECT ccrz__Spec__r.Name, ccrz__Spec__r.ccrz__SpecGroup__c, ccrz__SpecValue__c
                        FROM ccrz__Product_Specs__r Order by ccrz__Spec__r.ccrz__sequence__c ASC),
                    (SELECT ccrz__MediaType__c, ccrz__URI__c FROM ccrz__E_ProductMedias__r
                        WHERE ccrz__MediaType__c = 'Product Image Thumbnail')
                        FROM ccrz__E_Product__c WHERE Id =:productId]
            );

            for(ccrz__E_Product__c productItem: cc_ProductsMap.values()){
                dataMap.put(String.valueOf('prodinfo'), productItem);
            }
            res.success = true;
            res.data = dataMap;
            return res;
        }
        catch (Exception err){
            System.debug('Exception in getExtendedDataForProducts ' + res + ' ' + err.getMessage());
        }
        res.success = true;
        return null;
    }

    //Remote method will take Coaching Selection from Step 3 of Kit Config and sets value on Cart.
    @RemoteAction
    global static ccrz.cc_RemoteActionResult setCoachingSelection(final ccrz.cc_RemoteActionContext ctx, String cartEncrypteId, Boolean coachingSelection){
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false;
        try {
            if(cartEncrypteId != null && cartEncrypteId != '') {
                ccrz__E_Cart__c cartToUpdate = [SELECT Id, ccrz__EncryptedId__c, Coaching_Selection__c
                                                FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = :cartEncrypteId];
                if(cartToUpdate != null && coachingSelection != null) {
                    cartToUpdate.Coaching_Selection__c = coachingSelection;
                    update cartToUpdate;
                    res.success = true;
                    res.data = cartToUpdate;
                    return res;
                }
            }
            res.data = null;
            return res;
        }
        catch(Exception ex) {
            System.debug('Exception in CC_Ctrl_ProductDataExtension setCoachingSelection ' + res + ' ' + ex.getMessage());
            res.data = ex.getMessage();
        }
        res.data = null;
        return res;
    }
}