/* Unit Test for DropClient
*
* Objects referenced --> User, Coach__c,  Class__c, Class_Member__c
*
* @Date: 12/6/2016
* @Author Utkarsh Goswami (Mindtree)
* @Modified: Nathan Anderson (M360)
* @JIRA: Issue https://reside.jira.com/browse/HPRP-183, Task https://reside.jira.com/browse/HPRP-759
*/



@isTest
private Class DropClient_Test{

    static testMethod void test1(){

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'stan321', Email='standuser321@testorg.com',
                 EmailEncodingKey='UTF-8', LastName='Testing321', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='stanuser@org.com', hmr_IsCoach__c = TRUE);
                insert u;

        //create and insert a account
        Account testAccountObj = new Account(name = 'test Account');
        insert testAccountObj;

        //create and insert a contact
        Contact testContactObj = new Contact(FirstName='test', LastName = 'test contact', Email = 'test@testing.com', Account = testAccountObj);
        insert testContactObj;

        Coach__c coachObj = new Coach__c(hmr_Coach__c = u.id, hmr_Email__c = 'testcoach@xyz.com', hmr_Class_Strength__c = 12);
        insert coachObj;

        Class__c classObj = new Class__c(hmr_Class_Name__c = 'testing Class', hmr_Class_Type__c = 'P2 PP', hmr_Time_of_Day__c = 'Noon',
                                hmr_Start_Time__c = '11:00 AM', hmr_End_Time__c = '12:00 PM', hmr_Active_Date__c = Date.today(),
                                hmr_First_Class_Date__c = Date.today(), hmr_Coference_Call_Number__c = '1234567890', hmr_Participant_Code__c = 'Placeholder',
                                hmr_Host_Code__c = 'Placeholder', hmr_Coach__c = coachObj.id);
        insert classObj;

        Program__c progObj = new Program__c(Name='test', Days_in_1st_Order_Cycle__c = 14);

        insert progObj;

        Program_Membership__c pmObj = new Program_Membership__c(Program__c = progObj.Id, hmr_Status__c = 'Active', hmr_Contact__c = testContactObj.Id);

        insert pmObj;

        Class_Member__c classMemberObj = new Class_Member__c(hmr_Contact_Name__c = testContactObj.Id, hmr_Class__c = classObj.id);
        insert classMemberObj;

        ccrz__E_Term__c ccTerm = new ccrz__E_Term__c(ccrz__Title__c='Health', ccrz__Enabled__c=true, Consent_Type__c='Health',
                                                     Version_Number__c=1, ccrz__Locale__c='en_US', ccrz__Description__c='Test', ccrz__Storefront__c='DefaultStore');

        insert ccTerm;

        Consent_Agreement__c consentObj = new Consent_Agreement__c(Contact__c=testContactObj.Id, Version__c=ccTerm.Id, Status__c='Consented', Agreement_Timestamp__c=System.now());
        insert consentObj;

        Test.startTest();

            Test.setCurrentPageReference(new PageReference('Page.DropAClient_Popup'));
            System.currentPageReference().getParameters().put('Id', classMemberObj.id);

            DropClient controllerObj = new DropClient();

           // controllerObj.classMember();
            controllerObj.classMemberDetail = classMemberObj;
            controllerObj.dropReason = 'Medical';
            //controllerObj.dropReason = 'Test reason to Drop';
            controllerObj.closeWindow = TRUE;
            //controllerObj.classMemberDetail = classMemberObj;
            controllerObj.removeClient();
            controllerObj.dropReason = '';
            controllerObj.removeClient();
            controllerObj.getReasons();


            List<Class_Member__c> cmList = [Select id from Class_Member__c where hmr_Class__c = :classObj.id ];
            System.assertEquals(1,cmList.size());

        Test.stopTest();

    }


}