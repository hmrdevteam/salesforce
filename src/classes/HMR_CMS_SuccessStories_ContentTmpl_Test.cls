/*
Developer: Utkarsh Goswami (Mindtree)
Date: July 31st, 2017
Target Class: HMR_CMS_SuccessStories_ContentTemplate
*/

@isTest
private class HMR_CMS_SuccessStories_ContentTmpl_Test {
    @isTest 
    private static void testGetHtml() {
        HMR_CMS_SuccessStories_ContentTemplate successstories = new HMR_CMS_SuccessStories_ContentTemplate();          
        String propertyValue = successstories.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');  
        
        String respHTML = successstories.getHTML();              
        
        System.assert(!String.isBlank(respHTML));
    }              
}