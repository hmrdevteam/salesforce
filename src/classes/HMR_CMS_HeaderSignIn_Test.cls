/*
Developer: Mustafa Ahmed (HMR)
Date: March 22nd 2017
Target Class: HMR_CMS_HeaderSignIn_ContentTemplate
*/

@isTest
public class HMR_CMS_HeaderSignIn_Test {
        @isTest static void test_general_method() {
        // Implement test code
        HMR_CMS_HeaderSignIn_ContentTemplate HeaderSignInTemplate = new HMR_CMS_HeaderSignIn_ContentTemplate();         
                String propertyValue = HeaderSignInTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');       
                String DisplayLabel = HeaderSignInTemplate.DisplayLabel;
                String DisplayIconImage = HeaderSignInTemplate.DisplayIconImage;
                String renderHMTL = HeaderSignInTemplate.getHTML();
                System.Assert(renderHMTL != null);          
    }
    
            public static Id getProfileIdByName(String profileName) {
                return [select Id from Profile where name = :profileName].Id;
                    }
    
        @isTest static void test_general_method2() {
        // Implement test code
        HMR_CMS_HeaderSignIn_ContentTemplate HeaderSignInTemplate2 = new HMR_CMS_HeaderSignIn_ContentTemplate();            
                String propertyValue = HeaderSignInTemplate2.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');      
                String DisplayLabel = HeaderSignInTemplate2.DisplayLabel;
                String DisplayIconImage = HeaderSignInTemplate2.DisplayIconImage;
                String exString = HeaderSignInTemplate2.exString; 
           


            ccrz__E_AccountGroup__c accountGroup1 = new ccrz__E_AccountGroup__c(
            Name = 'TestAccountGroup1',
            ccrz__PriceListSelectionMethod__c = 'Best Price1'
            );
            insert accountGroup1;
        
            ccrz__E_AccountGroup__c accountGroup2 = new ccrz__E_AccountGroup__c(
                Name = 'TestAccountGroup2',
                ccrz__PriceListSelectionMethod__c = 'Best Price2'
            );
            insert accountGroup2;
       
        

        List<Account> actLst = new List<Account>();
        
        Account accountTest1 = new Account(
            name = 'Test Account1',
            ccrz__E_AccountGroup__c = accountGroup1.Id,
            Standard_HSAH_Kit_Discount__c = 2.22,
            hmr_HSAH_Standard_Kit_Price__c = 222.22,
            Standard_HSS_Kit_Discount__c = 1.11,
            hmr_HSS_Standard_Kit_Price__c = 111.11
        );
        actlst.add(accountTest1);

        Account accountTest2 = new Account(
            name = 'Test Account2',
            ccrz__E_AccountGroup__c = accountGroup2.Id,
            Standard_HSAH_Kit_Discount__c = 2.20,
            hmr_HSAH_Standard_Kit_Price__c = 222.00,
            Standard_HSS_Kit_Discount__c = 1.10,
            hmr_HSS_Standard_Kit_Price__c = 111.00
        );
        actlst.add(accountTest2);
        
        insert actlst;
        
        

        Contact testContactObj1 = new Contact(
            FirstName='testName1',
            LastName = 'testcontact1', 
            Email = 'test1@testing.com', 
            hmr_Contact_Original_Source__c = 'Referral',
            hmr_Contact_Original_Source_Detail__c = 'Friend',
            AccountId = accountTest1.Id
        );
        insert testContactObj1;

        Contact testContactObj2 = new Contact(
            FirstName='testName2',
            LastName = 'testcontact2', 
            Email = 'test2@testing.com', 
            hmr_Contact_Original_Source__c = 'Referral',
            hmr_Contact_Original_Source_Detail__c = 'Friend',
            AccountId = accountTest2.Id
        );
        insert testContactObj2;

        

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];


        
        User userWithRole = new User(
                                        alias = 'hasrole', 
                                       email='userwithrole@roletest1.com', 
                                       //userroleid = r.id, 
                                       emailencodingkey='UTF-8', 
                                       lastname='Testing', 
                                       languagelocalekey='en_US',
                                       localesidkey='en_US', 
                                       profileid = p.id,
                                       timezonesidkey='America/Los_Angeles', 
                                       ContactId = testContactObj2.Id,
                                       username='userwithrole@testorg.com');
  
        Database.insert(userWithRole);

        
    System.runAs ( new User(Id = UserInfo.getUserId()) )
        {             
            Profile prof1 = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User' LIMIT 1];

            Profile prof2 = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Program Community Profile' LIMIT 1];
        
            Profile prof3 = [SELECT Id, Name FROM Profile WHERE Name = 'Guest License User' LIMIT 1];
        
        
        
            UserRole r = [Select Name, Id from UserRole where Name ='President' LIMIT 1];


         
            ID myid1 = getProfileIdByName('HMR Customer Community User');
            ID myid2 = getProfileIdByName('HMR Program Community Profile');
            ID myid3 = getProfileIdByName('Guest License User');
            
    User testUser1 = new User(
                        Alias = 'standt',
                        Email = 'test1@testing.com',
                        EmailEncodingKey = 'UTF-8',
                        LastName = 'Testing',
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US',
                        ProfileId = myid1,
                        IsActive = true,
                        //UserRoleId = r.Id,
                        TimeZoneSidKey = 'America/New_York',
                        ContactId = testContactObj1.Id,
                        UserName = 'test@test3.com'); 
        insert testUser1;

        User testUser2 = new User(
                        Alias = 'standt',
                        Email = 'test1@test.com',
                        EmailEncodingKey = 'UTF-8',
                        LastName = 'Testing',
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey = 'en_US',
                        ProfileId = myid2,
                        IsActive = true,
                        //UserRoleId = r.Id,
                        TimeZoneSidKey = 'America/New_York',
                        //ContactId = testContactObj2.Id,
                        UserName = 'test1789979879669@test.com'); 
        insert testUser2;
        }      
      
        System.runAs (userWithRole){            
                String renderHMTL = HeaderSignInTemplate2.getHTML();
                System.Assert(renderHMTL != null);           
        }                           
    }
}