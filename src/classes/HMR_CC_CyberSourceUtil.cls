/**
* This class is a controller for cyber source call  
* @Date: 
* @Author: Pranay (Magnet360)
* @Updated by : Aslesha(Mindtree)
* @JIRA:
*/
global with sharing class HMR_CC_CyberSourceUtil{

   
    public String formAction { get; set; }
    public Map<String, String> responseCC { get; set; }
    public Set<String> responseKeys { get; set; } 
    public Map<String, Object> signedFields { get; set; }
    public Map<String, Object> unsignedFields { get; set; }
    public DateTime signedDt { get; set; }
    public String signedDtString { get; set; }
    public String signedDtString2 { get; set; }
    public String isResponse {get; set;}
    public Boolean CSresponseflag{get;set;}
    public ccrz__E_StoredPayment__c myCreditCardSP{get;set;}
    public string saveFlag{get;set;}
    //public String todayString = DateTime.now().millisecond().format();
    //public String access_key = 'deb2f6ab757b3ad099322f4eea87446b';
    //public String access_key = 'cce6a56f34033364be8bf9b92a69437a';cce6a56f34033364be8bf9b92a69437a
    public String access_key = '5ef05dfec9223193a05f7fe23f5a36fc'; // 3.30 CY

    //public String profile_id = '84E8F595-8136-4021-BD17-6BAB936483DC';
    //public String profile_id = 'D51FF152-E2ED-4487-87D2-F76C9368020A';
    public String profile_id = 'B5EA3769-8401-4878-A88B-820BC49A8103'; //3.30 CY
    public String exString {get; set;}
    public String JLLCCRZDebug{get; set;}
    
    public static String spDisplayName{get;set;}
    public String spCardType{get;set;}
    public String spCardNumber{get;set;}
    public String spCVN{get;set;}
    public String spExpDate{get;set;}
    public String storedPaymentId{get;set;}
    public String pageMode{get;set;}
    public Boolean CSResponseErrorFlag{get;set;}
    public String  CSREsponseErrorMessage{get;set;}

    public String pidCookie{get;set;}
    
    //JLLCCRZDebug:
    //public String secretKey = 'QRupfbLLPnSd5JCDPRLLDKbWosfFEPP1VsvzjzDUC3CDrgeUBfeGBgQO3sxsryh2xWbJK4OaPplLoJOgxELvZqlGcuKPSLbWuHVxZ2y1i21BA4b2j8U+1Ppr25my3e10iJVG/b9lnmumsA8Q9gE6ccXF2Ju+D8ELrrrD75PV70SvCRrgKxxY9iKV7RE/YUNM8sAyL9z5JZXwwJwTuNJopft43ftbK2XLFqc7suXtFc48WFDKax8QtgotpvTETpcH59uv+oh34H1HgPMRbsDKBsTvcYKNbEtI+0oOfYCgVovHqJZ0Q5ZkTrPXgyJFCTC/TIlXZq3HvS1EV73MEOhKuA==';
    //public String secretKey = '13f52a3879954191a3cfbe8ad6bfeeeb2b7030bf6c5042bb9cf463b0894dcdb457ab3fe9502b4cb09c9441a2575015f6db23212fc84448e49cd29fb204a47f159aff2c9a011e417a8bf32afeaaad20657b4c682f1e0e4ba6aae141e6eb3747b486d8e0f885d04e5dac61a8f7ce6fa67fb4d0e80d1a7b4befa0e7989f0bac6932';
    //public String secretKey = 'acf304e564b04425a6d0eb663741bb6845c19145beda4ce9ae0a8f945a546f48b625fd4567bb490e8548078ec6c35cc707509ecbf9124a1298a07cd3114a5be51d97d3879f4d4e41b7e1cd122ca110d7e7194876fb634b589be8d5efef7edd978c2c65fb082243b68f8fe2647b2cb4c9e5230186e4d6419aba10a8d46866343b';                             
    public String secretKey = '0ad59ddb8bf24560904674666918529b28ba266652024b089337ea6d1b3cde9d51f619da1658446290525d6c688516716dad718e4f6e435182edfdf195a6b550a27666600e104cabb019b76d74da143d8f675a23b5a846ef8f3f120ff9004fc6e9c9c4c7a2bf4fc9929397e317da2cbe1e28e1c9a9204411adc8489e44a9d381'; //3.30 CY
    public String signature  { get; set; }
 
    public ccrz__E_StoredPayment__c  objStoredPayment;
    
    public ccrz__E_StoredPayment__c   getobjStoredPayment(){
        return objStoredPayment;
    }
 

    public Map<String, String> signedFieldValues = new Map<String, String>{
        'access_key' => access_key,
        'profile_id' => profile_id,
        'transaction_type' => 'authorization',
        //'transaction_type' => 'create_payment_token',        
        'amount' => '100.00',
        'currency' => 'USD',
        'payment_method' => 'card',
        'bill_to_forename' => 'John',
        'bill_to_surname' => 'Doe',
        'bill_to_email' => 'null@cybersource.com',
        'bill_to_phone' => '02890888888',
        'bill_to_address_line1' => '1 Card Lane',
        'bill_to_address_city' => 'My City',
        'bill_to_address_state' => 'CA',
        'bill_to_address_country' => 'US',
        'bill_to_address_postal_code' => '94043',
        'locale' => 'en-us'
        //CY
        //'merchant_defined_data' => 'test'
    };

    public String getReferenceTime() {
        DateTime current = System.now();
        Long timeInMili = current.getTime();
        return String.valueOf(timeInMili);
    }

    public static String uuid() {
        final String hex = EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        return hex.SubString(0,8)+ '-' + hex.SubString(8,12) + '-' + hex.SubString(12,16) + '-' + hex.SubString(16,20) + '-' + hex.substring(20);
    }

    public static String sign(final String token, final String secret) {
        String signatureString = EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(token), Blob.valueOf(secret)));
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(token), Blob.valueOf(secret)));
    }

    public DateTime getUTCDateTimeInGMT(DateTime dt){
        Datetime GMTDate = Datetime.newInstanceGmt(
            dt.year(),
            dt.month(),
            dt.day(),
            dt.hour(),
            dt.minute(),
            dt.second()
        );
        return GMTDate;
    }

    public String getUTCDateTime(){
        DateTime oUTSDateTime = DateTime.now();//.addMinutes(362);
        //signedDt = oUTSDateTime;
        signedDtString = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'GMT');
        //signedDtString2 = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String strUTCDateTime = signedDtString;        
        return strUTCDateTime;
    }

    public HMR_CC_CyberSourceUtil() {
    
        CSresponseflag=true;
        CSResponseErrorFlag=false;

        //if (ApexPages.currentPage().getCookies().get('pid').getValue() != null && ApexPages.currentPage().getCookies().get('pid').getValue() != 'undefined') {
        //    pidCookie = ApexPages.currentPage().getCookies().get('pid').getValue();
        //}
        
        
        

        // storedPaymentId = apexpages.currentpage().getparameters().get('pid');

        if (apexpages.currentpage().getparameters().get('pid') != null) {

            storedPaymentId = ApexPages.currentpage().getparameters().get('pid');

        } else if (ApexPages.currentPage().getParameters() != null) {
            responseCC = ApexPages.currentPage().getParameters();
            responseKeys = responseCC.keySet();
            for(String str: responseKeys){
                  if(str=='req_merchant_secure_data1' && responseCC.get(str)!='') {
                    storedPaymentId = String.valueOf(responseCC.get('req_merchant_secure_data1'));
                  } 
            }
        }

        //} else if (pidCookie != null && pidCookie != 'undefined') {

        //    storedPaymentId = pidCookie;

        //}
        system.debug('stored paymentid : ' + storedPaymentId);
        pageMode=apexpages.currentpage().getparameters().get('mode');
        if (storedPaymentId !=null && storedPaymentId != 'undefined'){
            List<ccrz__E_StoredPayment__c> tempStoredPayments = [Select ccrz__DisplayName__c , ccrz__AccountNumber__c,ccrz__AccountType__c,ccrz__Token__c,ccrz__ExpMonth__c,ccrz__ExpYear__c 
                                                                FROM ccrz__E_StoredPayment__c  
                                                                where id=:storedPaymentId];
            if (tempStoredPayments.size() > 0) {
                objStoredPayment = tempStoredPayments[0];
                spDisplayName=objStoredPayment.ccrz__DisplayName__c;
                spCardNumber=objStoredPayment.ccrz__AccountNumber__c;
                spCardType=objStoredPayment.ccrz__AccountType__c;
                spExpDate=objStoredPayment.ccrz__ExpMonth__c+'-'+objStoredPayment.ccrz__ExpYear__c;
            }
            //objStoredPayment =[Select ccrz__DisplayName__c , ccrz__AccountNumber__c,ccrz__AccountType__c,ccrz__Token__c,ccrz__ExpMonth__c,ccrz__ExpYear__c FROM ccrz__E_StoredPayment__c  where id=:storedPaymentId]; 
            //spDisplayName=objStoredPayment.ccrz__DisplayName__c;
            //spCardNumber=objStoredPayment.ccrz__AccountNumber__c;
            //spCardType=objStoredPayment.ccrz__AccountType__c;
            //spExpDate=objStoredPayment.ccrz__ExpMonth__c+'-'+objStoredPayment.ccrz__ExpYear__c;
        }
               
        myCreditCardSP = new ccrz__E_StoredPayment__c ();
       
        if(ApexPages.currentPage().getParameters() != null) {
            responseCC = ApexPages.currentPage().getParameters();
            responseKeys = responseCC.keySet();
           
              for(String str: responseKeys){
                  if(str=='auth_code'){
                    CSresponseflag=false;
                   }
                   system.debug('######## ERRRRRRROR reposnse'+str+'======'+responseCC.get(str));
                  if(str=='decision' && responseCC.get(str)=='ERROR')
                  {
                    
                    CSResponseErrorFlag=true;
                    system.debug('in if flag should be true'+CSResponseErrorFlag);
                    CSREsponseErrorMessage=String.valueOf(responseCC.get('message'));
                  
                  } 
           }
           
        }
        
        signedFieldValues.put('transaction_uuid', uuid());
        signedFieldValues.put('signed_date_time', getUTCDateTime());
        signedFieldValues.put('reference_number', getReferenceTime());

        signedFieldValues.put('merchant_secure_data1', storedPaymentId);
        //signedFieldValues.put('reference_number', uuid());

        initFormFields();
        formAction = 'https://testsecureacceptance.cybersource.com/silent/pay';
        //formAction = 'https://testsecureacceptance.cybersource.com/silent/token/create';        
    }
    
   

 
    public virtual void initFormFields() {
        this.signedFields = new Map<String, String>();
        this.unsignedFields = new Map<String, String>();

        // signed fields
        List<String> signedFieldNames = cc_pgcs_ApiConstants.CS_REQ_F_AUTH_CREATETOKEN;
        // optional fields: state(US)phone
        //signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_BILLTO_ADDR_STATE);
        signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_BILLTO_PHONE);
        signedFieldNames.add('merchant_secure_data1');
        //signedFieldNames.add(cc_pgcs_ApiContants.CS_REQ_MERCHANT_DEFINED_DATA);
        //for(Integer i=1;i<=19;i++) signedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_MERCHANT_DEFINED_DATA + i);
        // sort for predictable order
        //signedFieldNames.sort();
        // seed with blank values
        for (String s : signedFieldNames) {
            if(signedFieldValues.get(s) != null) {
                signedFields.put(s, (Object)signedFieldValues.get(s));
            }
            else {
                signedFields.put(s,'');
            }           
        }

        // list fields
        signedFields.put(cc_pgcs_ApiConstants.CS_REQ_SIGNEDFIELDNAMES, String.join(signedFieldNames,','));

        List<String> unsignedFieldNames = cc_pgcs_ApiConstants.CS_REQ_F_UNSIGNED;
        // optional fields: CVN
        unsignedFieldNames.add(cc_pgcs_ApiConstants.CS_REQ_CARDCVN);
        // sort for predictable order
        //unsignedFieldNames.sort();
        // seed with blank values
        for (String u : unsignedFieldNames) {
            unsignedFields.put(u,'');
        }
        // list fields
        signedFields.put(cc_pgcs_ApiConstants.CS_REQ_UNSIGNEDFIELDNAMES, String.join(unsignedFieldNames,','));

        List<String> signedFieldNamesArray = ((String)signedFields.get('signed_field_names')).Split(',');
        List<String> dataToSign = new List<String>();

        for (String signedFieldName: signedFieldNamesArray){
            dataToSign.add(signedFieldName + '=' + signedFields.get(signedFieldName));
        }

        String finalDataForSignatureCC = String.join(dataToSign, ',');
        
        JLLCCRZDebug = finalDataForSignatureCC;

        signature = sign(finalDataForSignatureCC, secretKey);
    }
    
    
/**
 * Remote action to save/update the payment method
 *
 * @param ccrz.cc_RemoteActionContext : Cloud craze remote action context
 * @param String jsonCCData) : input data from the submitted Form.
 * 
 *
 * @return void : ccrz.cc_RemoteActionResult
 *
 * @Author: Aslesha (Mindtree)
 * @Date: 03/24/2017
 */
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveCCNumber(ccrz.cc_RemoteActionContext ctx, String jsonCCData) {
      
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        res.inputContext = ctx;
        res.success = false; 
        try {            
            /*Map<String,Object> defaultStoreInput = new Map<String,Object>{
                ccrz.cc_hk_Payment.PARAM_STOREFRONT => 'DefaultStore'
            };
            HMR_cc_hk_Payment paymentHook = new HMR_cc_hk_Payment();*/
            system.debug('jsonCCData=========>'+jsonCCData);
            ccrz.cc_hk_Payment.TransactionPaymentParams ccForm = (ccrz.cc_hk_Payment.TransactionPaymentParams)JSON.deserialize(jsonCCData, ccrz.cc_hk_Payment.TransactionPaymentParams.class);

            Map<String,Object> createParams = new Map<String,Object>{

                ccrz.cc_hk_Payment.PARAM_TRANSACTION_PROCESSED_DATA => ccForm

            };
                        
            //ccrz.cc_CallContext.currAccountId = test.isRunningTest() ? :ccrz.cc_CallContext.currAccountId;
            String currentAccountId = ccrz.cc_CallContext.currAccountId;
            String storefront =  ccrz.cc_CallContext.storefront;
            
            ccrz__E_StoredPayment__c pmt = new ccrz__E_StoredPayment__c(

                ccrz__Account__c =currentAccountId,

                ccrz__User__c = ccrz.cc_CallContext.currUserId,

                ccrz__Storefront__c =storefront,
          
                ccrz__DisplayName__c =  ccForm.DisplayName,
                
                ccrz__AccountNumber__c=ccForm.AccountNumber,
                
                ccrz__AccountType__c=ccForm.AccountType,
                
                ccrz__Token__c=ccForm.Token,                 
                
                ccrz__ExpMonth__c=ccForm.expirationMonth,
                
                ccrz__ExpYear__c=ccForm.expirationYear,
                        
                ccrz__PaymentType__c=ccForm.PaymentType
            );
            
            system.debug('pmt==='+pmt);
            //system.debug('storedPaymentId==='+storedPaymentId);
         
            if(ccForm.storedPaymentId==null || ccForm.storedPaymentId==''){
                insert pmt;
                ccForm.storedPaymentId = pmt.Id;
            }else
            {
              pmt.id=ccForm.storedPaymentId;
              update pmt;
              //upsert pmt; 
            }      

            // Cloud craze hook call
            //Map<String,Object> createResult = paymentHook.createStoredPayment(createParams);
              res.success = true;
            
          
        } catch(exception e) {
            system.debug('Exception---'+e.getMessage());
            system.debug('Exception line Number---'+e.getStackTraceString());
            res.success = false;

        } finally {

            //ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','savePONumber');
            //ccrz.ccLog.close(result);

        }
        
        return res;
        
    }
}