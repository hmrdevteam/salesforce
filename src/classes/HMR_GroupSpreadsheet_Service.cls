/**
* Service class to get group data
*
* @Date: 2018-03-25
* @Author: Zach Engman/Utkarsh Goswami (Mindtree)
* @Modified: 2018-03-29 - Utkarsh Goswami/Zach Engman
* @JIRA:
*/


public without sharing class HMR_GroupSpreadsheet_Service{
    private static final integer HS_WeeklyShakeMin = 21;
    private static final integer HS_WeeklyEntreeMin = 14;
    private static final integer HS_WeeklyFruitVegMin = 35;
    private static final integer HS_WeeklyPAMin = 2000;
    private static final integer HS_WeeklyInBoxMin = 7;
    private static final integer HS_WeeklyMet325Min = 7;

    public HMR_GroupSpreadsheet_Service(){}
    
    public GroupSpreadsheet getGroupDataSpreadsheet(string classId){
        
        GroupSpreadsheet groupSSheet;
        List<Id> coachingSessionIds = new List<Id>();
        
        List<Class__c> classList = [SELECT Id, Name, hmr_Class_Name__c, Program__r.hmr_Phase_Type__c FROM Class__c WHERE id = :classId];
        
        if(classList.size() > 0 ){
        
            List<Coaching_Session__c> coachingSessionList = [SELECT hmr_Class_Date__c FROM Coaching_Session__c WHERE hmr_Class__c = :classId AND hmr_Class_Date__c <= TODAY ORDER BY hmr_Class_Date__c DESC];


            //Get the attendance statistics for each coaching session
            Map<String, Integer> attendanceMap = new Map<String, Integer>();
            Map<String, Integer> studentMap = new Map<String, Integer>();

            for(AggregateResult result : [SELECT hmr_Coaching_Session__c, hmr_Attendance__c, count(id) counter
                                          FROM Session_Attendee__c
                                          WHERE hmr_Coaching_Session__r.hmr_Class__c =: classId
                                          GROUP BY hmr_Coaching_Session__c, hmr_Attendance__c]){
                
                string coachingSessionId = (String)result.get('hmr_Coaching_Session__c');
                string attendanceStatus = (String)result.get('hmr_Attendance__c');
                integer counter = Integer.valueOf(result.get('counter'));

                if(studentMap.containsKey(coachingSessionId)){
                    studentMap.put(coachingSessionId, studentMap.get(coachingSessionId) + counter);
                }
                else{
                    studentMap.put(coachingSessionId, counter);
                }

                if(!String.isBlank(attendanceStatus) && attendanceStatus == 'Attended'){
                    attendanceMap.put(coachingSessionId, counter);
                }
            }

            //Get the activity log statistics by individual
            Map<String, List<AggregateResult>> activityLogSummaryByIndividualMap = new Map<String, List<AggregateResult>>();
            for(AggregateResult result : [SELECT Session_Attendee__r.hmr_Coaching_Session__c sessionId,
                                                 Contact__c, 
                                                 Contact__r.hmr_Gender__c gender,
                                                 COUNT(Id) activityCount,
                                                 SUM(ShakesCereals__c) shakeTotal, 
                                                 SUM(Entrees__c) entreeTotal, 
                                                 SUM(BenefitBars__c) barTotal,
                                                 SUM(FruitsVegetables__c) fruitAndVegetableTotal, 
                                                 SUM(TotalPhysicalActivityCaloriesRollUp__c) physicalActivityTotal, 
                                                 SUM(Met_325__c) met325Total, SUM(In_The_Box__c) inBoxTotal,
                                                 MAX(CurrentWeight__c) maxWeight, 
                                                 MIN(CurrentWeight__c) minWeight
                                          FROM ActivityLog__c
                                          WHERE Session_Attendee__r.hmr_Coaching_Session__c IN: coachingSessionList
                                          GROUP BY Session_Attendee__r.hmr_Coaching_Session__c, Contact__c, Session_Attendee__r.hmr_Coaching_Session__r.hmr_Class_Date__c, Contact__r.hmr_Gender__c
                                          ORDER BY Contact__c, Session_Attendee__r.hmr_Coaching_Session__r.hmr_Class_Date__c DESC]){

                String sessionId = (String)result.get('sessionId');

                if(activityLogSummaryByIndividualMap.containsKey(sessionId)){
                    activityLogSummaryByIndividualMap.get(sessionId).add(result);
                }
                else{
                    activityLogSummaryByIndividualMap.put(sessionId, new List<AggregateResult>{result});
                } 
            }
 
            //Get the activity log statistics for each coaching session
            Map<String, AggregateResult> activityLogStatisticMap = new Map<String, AggregateResult>();
            for(AggregateResult result : [SELECT Session_Attendee__r.hmr_Coaching_Session__c sessionId, 
                                                 SUM(BenefitBars__c) barTotal, 
                                                 SUM(ShakesCereals__c) shakeTotal, 
                                                 SUM(Entrees__c) entreeTotal, 
                                                 SUM(FruitsVegetables__c) fruitAndVegetableTotal, 
                                                 SUM(TotalPhysicalActivityCaloriesRollUp__c) physicalActivityTotal, 
                                                 SUM(Met_325__c) met325Total, SUM(In_The_Box__c) inBoxTotal,
                                                 SUM(TotalPhysicalActivityCaloriesRollUp__c) totalPA
                                          FROM ActivityLog__c
                                          WHERE Session_Attendee__r.hmr_Coaching_Session__c IN: coachingSessionList
                                          GROUP BY Session_Attendee__r.hmr_Coaching_Session__c]){
                activityLogStatisticMap.put((String)result.get('sessionId'), result);
            }

            
            groupSSheet = new GroupSpreadsheet(classList[0], coachingSessionList, studentMap, attendanceMap, activityLogStatisticMap, activityLogSummaryByIndividualMap);
        }

        return groupSSheet;
    }
    
    public class GroupSpreadsheet{
    
        public String groupName{get;set;}
        public String programType {get;set;}
        public List<WeekSummary> weekSummaryList{get;set;}
    
        public GroupSpreadsheet(Class__c classData, List<Coaching_Session__c> coachingSessionList, Map<String, Integer> studentMap, Map<String, Integer> attendanceMap, Map<String, AggregateResult> aggregateResultMap, Map<String, List<AggregateResult>> activityLogSummaryByIndividualMap){
        
            this.groupName = classData.hmr_Class_Name__c;
            this.programType = classData.Program__r.hmr_Phase_Type__c;
            this.weekSummaryList = new List<WeekSummary>();

            for(Coaching_Session__c cs : coachingSessionList){
                weekSummaryList.add(new WeekSummary(cs, studentMap.get(cs.id), attendanceMap.get(cs.id), aggregateResultMap.get(cs.id), activityLogSummaryByIndividualMap.get(cs.id)));
            }

            //calculate the metrics that span weeks
            for(integer i = coachingSessionList.size() - 1; i > 0; i--){
                weekSummaryList[i] = calculateCrossSummaryMetrics(weekSummaryList[i], 
                                                                  activityLogSummaryByIndividualMap.get(coachingSessionList[i].id), 
                                                                  activityLogSummaryByIndividualMap.get(coachingSessionList[i - 1].id), 
                                                                  ((i - 2) >= 0 ? activityLogSummaryByIndividualMap.get(coachingSessionList[i - 2].id) : null));
            }       
        }

        private WeekSummary calculateCrossSummaryMetrics(WeekSummary weekToCalculate, List<AggregateResult> currentWeekMetricList, List<AggregateResult> priorWeekMetricList, List<AggregateResult> threeWeekMetricList){
            Map<String, AggregateResult> priorWeekMetricMap = new Map<String, AggregateResult>();
            Map<String, AggregateResult> threeWeekMetricMap = new Map<String, AggregateResult>();

            //Convert to maps to easily get matching records by contact
            if(priorWeekMetricList != null){
                for(AggregateResult contactResult : priorWeekMetricList){
                    priorWeekMetricMap.put((String)contactResult.get('Contact__c'), contactResult);
                }
            }

            if(threeWeekMetricList != null){
                for(AggregateResult contactResult : threeWeekMetricList){
                    threeWeekMetricMap.put((String)contactResult.get('Contact__c'), contactResult);
                }
            }

            if(currentWeekMetricList != null){
                for(AggregateResult contactResult : currentWeekMetricList){
                    String contactId = (String)contactResult.get('Contact__c');
                    String gender = (String)contactResult.get('gender');//assign gender so we have for the maintenance calories                
                    Decimal mostRecentWeekWeightMin = 0;
                    Decimal weekTwoWeightMax = 0;
                    Decimal weekThreeWeightMax = 0;

                    //weight calculations for each individual
                    if(contactResult.get('maxWeight') != null){ // we know if max has a value, min also does
                        mostRecentWeekWeightMin = (Decimal)contactResult.get('maxWeight');
                        
                        if(priorWeekMetricMap.get(contactId) != null && priorWeekMetricMap.get(contactId).get('maxWeight') != null){
                            weekTwoWeightMax = (Decimal)priorWeekMetricMap.get(contactId).get('maxWeight');
                        }
                        
                        if(threeWeekMetricMap.get(contactId) != null && threeWeekMetricMap.get(contactId).get('maxWeight') != null){
                            weekThreeWeightMax = (Decimal)threeWeekMetricMap.get(contactId).get('maxWeight');
                        }
                    }

                    if(mostRecentWeekWeightMin > 0 && weekTwoWeightMax > 0){
                        weekToCalculate.oneWeekWeightChangeList.add(mostRecentWeekWeightMin - weekTwoWeightMax);
                    }

                    if(mostRecentWeekWeightMin > 0 && weekThreeWeightMax > 0){
                        weekToCalculate.threeWeekWeightChangeList.add(mostRecentWeekWeightMin - weekThreeWeightMax);
                    }

                    if(mostRecentWeekWeightMin > 0 && weekTwoWeightMax > 0 && weekThreeWeightMax > 0){
                         integer maintenanceCalories = !String.isBlank(gender) && gender == 'Female' ? 10 : 11;
                         

                         weekToCalculate.percentThreeWeekExpectedWeightLossList.add((Math.abs(mostRecentWeekWeightMin - weekThreeWeightMax) / 
                                                                               (
                                                                                ((((mostRecentWeekWeightMin * maintenanceCalories * 7) + 2000) - 10600) / 3500) + 
                                                                                ((((weekTwoWeightMax * maintenanceCalories * 7) + 2000) - 10600) / 3500) + 
                                                                                ((((weekThreeWeightMax * maintenanceCalories * 7) + 2000) - 10600) / 3500)
                                                                               ) * 100.0).setScale(1, RoundingMode.HALF_UP));
                
                    }
                }
            }

            return weekToCalculate;
        }
    }
    
    public Class WeekSummary{

        public Date sessionDate {get; set;}
        public String sessionDateFormatted {get{
            return DateTime.newInstance(sessionDate, Time.newInstance(0,0,0,0)).format('MM/dd/YYYY');
        }}
        public Integer classSize {get; set;}
        public Integer attendance {get; set;}
        public Decimal attendancePercent {get{
            return classSize > 0 ? (attendance * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Integer barTotal {get; set;}
        public Integer metMinShakeCount {get; set;}
        public Integer shakeTotal {get; set;}
        public Integer metMinEntreeCount {get; set;}
        public Integer entreeTotal {get; set;}
        public Integer metMinFruitVegCount {get; set;}
        public Integer fruitVegTotal {get; set;}
        public Integer metMinMealReplacementCount {get; set;}
        public Integer mealReplacementTotal {get{
            return shakeTotal + entreeTotal + barTotal;
        }}
        public Integer metMinPhysicalActivityCount {get; set;}
        public Integer physicalActivityCaloriesTotal {get; set;}
        public Integer metMin325Count {get; set;}
        public Integer met325Total {get; set;}
        public Integer metMinDaysInTheBoxCount {get; set;}
        public Integer daysInTheBox {get; set;}
        public Integer metTICount {get; set;}
        public Integer activityCount {get; set;}
        public Double totalPA {get;set;}
        public Decimal endOfWeekPercent{get{
            return classSize > 0 ? ((activityCount * 1.0) / (classSize * 7.0) * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal shakePercent {get{
            return classSize > 0 ? (metMinShakeCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal shakeAverage {get{
            return classSize > 0 ? (shakeTotal * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        
        public Decimal entreePercent {get{
            return classSize > 0 ? (metMinEntreeCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal entreeAverage {get{
            return classSize > 0 ? (entreeTotal * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        
        public Decimal mealReplacementPercent {get{
            return classSize > 0 ? (metMinMealReplacementCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal mealReplacementAverage {get{
            return classSize > 0 ? (mealReplacementTotal * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}

        public Decimal fruitVegPercent {get{
            return classSize > 0 ? (metMinFruitVegCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal fruitVegAverage {get{
            return classSize > 0 ? (fruitVegTotal * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        
        public Decimal totalPhysicalActivityCaloriesPercent {get{
            return classSize > 0 ? (metMinPhysicalActivityCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal totalPhysicalActivityCaloriesAverage {get{
            return classSize > 0 ? (physicalActivityCaloriesTotal * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        
        public Decimal met325TotalPercent {get{
            return classSize > 0 ? (metMin325Count * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal met325TotalAverage {get{
            return classSize > 0 ? (met325Total * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        
        public Decimal daysInTheBoxPercent {get{
            return classSize > 0 ? (metMinDaysInTheBoxCount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}
        public Decimal daysInTheBoxAverage {get{
            return classSize > 0 ? (daysInTheBox * 1.0 / classSize).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}

        public Decimal metTIPercent {get{
            return classSize > 0 ? (metTICount * 1.0 / classSize * 100.0).setScale(1, RoundingMode.HALF_UP) : 0.0;
        }}

        private List<Decimal> oneWeekWeightChangeList {get; set;}
        private List<Decimal> threeWeekWeightChangeList {get; set;}
        private List<Decimal> percentThreeWeekExpectedWeightLossList {get; set;}

        //get average weight metrics
        public Decimal oneWeekWeightChangeAverage {get{
            if(oneWeekWeightChangeList.size() > 0){
                Decimal totalWeightChange = 0.0;
                //add up to get totals
                for(Decimal weightChange : oneWeekWeightChangeList){
                    totalWeightChange += weightChange;
                }
                //divide to get average
                return (totalWeightChange / oneWeekWeightChangeList.size()).setScale(1, RoundingMode.HALF_UP);
            }
            else
                return 0.0;
        }}
        public Decimal threeWeekWeightChangeAverage {get{
            if(threeWeekWeightChangeList.size() > 0){
                Decimal totalWeightChange = 0.0;
                //add up to get totals
                for(Decimal weightChange : threeWeekWeightChangeList){
                    totalWeightChange += weightChange;
                }
                //divide to get average
                return (totalWeightChange / threeWeekWeightChangeList.size()).setScale(1, RoundingMode.HALF_UP);
            }
            else
                return 0.0;
        }}
        public Decimal percentThreeWeekExpectedWeightLoss{get{
            if(percentThreeWeekExpectedWeightLossList.size() > 0){
                Decimal totalPercentChange = 0.0;
                //add up to get totals
                for(Decimal percentChange : percentThreeWeekExpectedWeightLossList){
                    totalPercentChange += percentChange;
                }
                //divide to get average
                return (totalPercentChange / percentThreeWeekExpectedWeightLossList.size()).setScale(1, RoundingMode.HALF_UP);
            }
            else
                return 0.0;
        }}

        public WeekSummary(Coaching_Session__c coachingSessionRecord, integer classSize, integer attendance, AggregateResult metrics, List<AggregateResult> individualMetricList){
            
            this.sessionDate = coachingSessionRecord.hmr_Class_Date__c;
            this.classSize = 0;
            this.attendance = 0;
            this.barTotal = 0;
            this.metMinShakeCount = 0;
            this.shakeTotal = 0;
            this.metMinEntreeCount = 0;
            this.entreeTotal = 0;
            this.metMinFruitVegCount = 0;
            this.fruitVegTotal = 0;
            this.metMinPhysicalActivityCount = 0;
            this.physicalActivityCaloriesTotal = 0;
            this.metMin325Count = 0;
            this.met325Total = 0;
            this.metMinDaysInTheBoxCount = 0;
            this.daysInTheBox = 0;
            this.metMinMealReplacementCount = 0;
            this.metTICount = 0;
            this.totalPA = 0;
            this.activityCount = 0;

            //instantiate the lists
            this.oneWeekWeightChangeList = new List<Decimal>();
            this.threeWeekWeightChangeList = new List<Decimal>();
            this.percentThreeWeekExpectedWeightLossList = new List<Decimal>();

            if(classSize != null){
                this.classSize = classSize;
            }

            if(attendance != null){
                this.attendance = attendance;
            }

            if(metrics != null){
                if(metrics.get('barTotal') != null){
                    this.barTotal = Integer.valueOf(metrics.get('barTotal'));
                }
                if(metrics.get('shakeTotal') != null){
                    this.shakeTotal = Integer.valueOf(metrics.get('shakeTotal'));
                }
                if(metrics.get('entreeTotal') != null){
                    this.entreeTotal = Integer.valueOf(metrics.get('entreeTotal'));
                }
                if(metrics.get('fruitAndVegetableTotal') != null){
                    this.fruitVegTotal = Integer.valueOf(metrics.get('fruitAndVegetableTotal'));
                }
                if(metrics.get('physicalActivityTotal') != null){
                    this.physicalActivityCaloriesTotal = Integer.valueOf(metrics.get('physicalActivityTotal'));
                }
                if(metrics.get('met325Total') != null){
                    this.met325Total = Integer.valueOf(metrics.get('met325Total'));
                }
                if(metrics.get('inBoxTotal') != null){
                    this.daysInTheBox = Integer.valueOf(metrics.get('inBoxTotal'));
                }
                if(metrics.get('totalPA') != null){
                    this.totalPA = Double.valueOf(metrics.get('totalPA'));
                }
            }
  
            Integer shakeTotal = 0;
            Integer entreeTotal = 0;
            Integer barTotal = 0;
            Integer fvTotal = 0;
            Integer paTotal = 0;

            if(individualMetricList != null){
                for(AggregateResult result : individualMetricList){ //this list is sorted by contact in descending order by date of coaching session
                    if(result.get('activityCount') != null){
                        activityCount += Integer.valueOf(result.get('activityCount'));
                    }

                    if(result.get('shakeTotal') != null){
                        if(Integer.valueOf(result.get('shakeTotal')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyShakeMin){
                            shakeTotal = Integer.valueOf(result.get('shakeTotal'));
                            this.metMinShakeCount++;
                        }
                    }

                    if(result.get('entreeTotal') != null){
                        if(Integer.valueOf(result.get('entreeTotal')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyEntreeMin){
                            entreeTotal = Integer.valueOf(result.get('entreeTotal'));
                            this.metMinEntreeCount++;
                        }
                    }

                    if(result.get('barTotal') != null){
                        barTotal = Integer.valueOf(result.get('barTotal'));
                    }

                    if(result.get('fruitAndVegetableTotal') != null){
                        if(Integer.valueOf(result.get('fruitAndVegetableTotal')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyFruitVegMin){
                            fvTotal = Integer.valueOf(result.get('fruitAndVegetableTotal'));
                            this.metMinFruitVegCount++;
                        }
                    }

                    if(result.get('physicalActivityTotal') != null){
                        if(Integer.valueOf(result.get('physicalActivityTotal')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyPAMin){
                            paTotal = Integer.valueOf(result.get('physicalActivityTotal'));
                            this.metMinPhysicalActivityCount++;
                        }
                    }

                    if(result.get('met325Total') != null){
                        if(Integer.valueOf(result.get('met325Total')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyMet325Min){
                            this.metMin325Count++;
                        }
                    }

                    if(result.get('inBoxTotal') != null){
                        if(Integer.valueOf(result.get('inBoxTotal')) >= HMR_GroupSpreadsheet_Service.HS_WeeklyInBoxMin){
                            this.metMinDaysInTheBoxCount++;
                        }
                    }

                    //met TI - needed for phase 2 version
                    if((shakeTotal + entreeTotal + barTotal) >= HS_WeeklyEntreeMin && fvTotal >= HS_WeeklyFruitVegMin && paTotal >= HMR_GroupSpreadsheet_Service.HS_WeeklyPAMin){
                        this.metTICount++;
                    }
                    //met meal replacement count for phase 2 version
                    if((shakeTotal + entreeTotal + barTotal) >= HS_WeeklyEntreeMin){
                        this.metMinMealReplacementCount++;
                    }
                }
            }

            
        }
    
    }
}