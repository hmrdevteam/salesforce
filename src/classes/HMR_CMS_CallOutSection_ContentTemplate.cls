/**
* Apex Content Template Controller for Call Out sections on Marketing/Community Pages
*
* @Date: 07/19/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA: https://reside.jira.com/browse/HPRP-4015
*/
global virtual class HMR_CMS_CallOutSection_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_CallOutSection_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_CallOutSection_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	//CSS Class for Section Background Color Property
    public String CSSClassForBackgroundColor{
        get{
            return getPropertyWithDefault('CSSClassForBackgroundColor', 'bg-grey');
        }
    }

	//Property for Description inside Modal Popup
    public String CallOutDisplayMessage{
        get{
            return getPropertyWithDefault('CallOutDisplayMessage', '');
        }
    }

	//Property for selecting the font color for the call out display message
	public String FontColorBlackOrWhite{
        get{
            return getPropertyWithDefault('FontColorBlackOrWhite', '[]');
        }
    }

	//CMS Link Property for the Button Target URL
	public cms.Link CallOutButtonURLLinkObj{
        get{
            return getPropertyLink('CallOutButtonURLLinkObj');
        }
    }

	//Label of the call out Button
	public String CallOutButtonURLLabelText{
        get{
            return getPropertyWithDefault('CallOutButtonURLLabelText', '');
        }
    }

	global virtual override String getHTML(){
		String html = '';
		String callOutButtonURL = (this.CallOutButtonURLLinkObj != null) ? this.CallOutButtonURLLinkObj.targetPage : '#';
		html += '<section class="call-out-section ' + CSSClassForBackgroundColor + '">' +
					'<div class="container-fluid">' +
						'<div class="container">' +
							'<div class="row">' +
								'<div class="col-sm-9">' +
									'<p class="' + FontColorBlackOrWhite + '">' + CallOutDisplayMessage + '</p>' +
								'</div>' +
								'<div class="col-sm-3">' +
									'<div class="btn-container">' +
										'<a href="' + callOutButtonURL + '" class="btn btn-primary hmr-btn-blue hmr-btn-small">' + CallOutButtonURLLabelText + '</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>'+
					'</div>' +
				'</section>';
		return html;
	}
}