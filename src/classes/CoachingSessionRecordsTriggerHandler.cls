/**
* Trigger Handler for CoachingSessionRecords.trigger
* Managing (Creating, Deleting) Coaching Session records based on class records information,
* Objects referenced --> Class__c, Coaching_Session__c
*
* @Date: 2016-11-21
* @Author Pranay Mistry (Magnet 360)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: https://reside.jira.com/browse/HPRP-576
*/

public class CoachingSessionRecordsTriggerHandler {
    /**
    *  ProcessClassAndCoachingSessionRecords
    *  static function to process class and related coaching session records
    *  only called when there is a change in the class Active status and when
    *  a new class record is created. called from handler method
    *  
    *  @param Class record --> the newly created or updated class record with change in Active Status
    *  @param Coaching Sessions Map --> any existing coaching session records related to the updated class record
    *  @param Coaching Sessions List to insert --> invoked in the handler method
    *  @return void
    */
    public static void ProcessClassAndCoachingSessionRecords(Class__c newUpdatedClass, Map<Id, Coaching_Session__c> existingCoachingSessions, List<Coaching_Session__c> coachingSessionsToInsert) {      
        Date now = Date.today();
        system.debug('!!!!! existingCoachingSessions ' + existingCoachingSessions.size());
        //check for if the class is made inactive and if there are any related coaching session records
        if(newUpdatedClass.hmr_Active__c == false && !existingCoachingSessions.values().isEmpty()){
            //loop through the related coaching session records
            for(Coaching_Session__c futureCoachingSessionToDelete: existingCoachingSessions.values()) {
                //compare the class date with today
                if(futureCoachingSessionToDelete.hmr_Class_Date__c < now) {
                    //remove past coaching session records from the map that will be deleted
                    existingCoachingSessions.remove(futureCoachingSessionToDelete.Id);
                }
            }
        }
        else {
             // check is class is one Time class
            if(newUpdatedClass.hmr_One_Time_Class__c!=true){
                Boolean includeFirstClassDate = false;
                //check if the First class date for the class is in future or today
                if(newUpdatedClass.hmr_First_Class_Date__c >= now) {
                    //need to set this flag for DateUtil method
                    includeFirstClassDate = true;
                }
                //get recurring dates from DateUtil Utitliy (weekly recurrence, hence 5th argument value is 7)
                //date range is from Class First class date plus 1 year (hence 2nd argument value  is 1)            
                List<Date> coachingSessionDates = DateUtil.getRecurringDates(newUpdatedClass.hmr_First_Class_Date__c, 1, 0, 0, 7, includeFirstClassDate);                                    
                if(!coachingSessionDates.isEmpty()) {
                    for(Date coachingSessionDate: coachingSessionDates) {
                        //only create coaching session records for future, hence this validation
                        if(coachingSessionDate >= now) {
                            //add coaching session record details
                            Coaching_Session__c coachingSession = new Coaching_Session__c(
                                    hmr_Class__c = newUpdatedClass.Id,
                                    hmr_Class_Date__c = coachingSessionDate,
                                    hmr_Coach__c = newUpdatedClass.hmr_Coach__c,
                                    hmr_Start_Time__c = newUpdatedClass.hmr_Start_Time__c,
                                    hmr_End_Time__c =  newUpdatedClass.hmr_End_Time__c       
                                );                       
                            //add the coaching session record to the list
                            coachingSessionsToInsert.add(coachingSession);
                        }
                        else { 
                            //no need to create a coaching session record in the past
                            System.debug('This recurring Date occured in the Past, no need to create a coachingSession' + ' ' + coachingSessionDate);
                        }
                    }
                }
            } 
            else {
                Coaching_Session__c oneTimeCoachingSession = new Coaching_Session__c(
                    hmr_Class__c = newUpdatedClass.Id,
                    hmr_Class_Date__c = newUpdatedClass.hmr_First_Class_Date__c,
                    hmr_Coach__c = newUpdatedClass.hmr_Coach__c,
                    hmr_Start_Time__c = newUpdatedClass.hmr_Start_Time__c,
                    hmr_End_Time__c =  newUpdatedClass.hmr_End_Time__c       
                ); 
                //add the coaching session record to the list
                coachingSessionsToInsert.add(oneTimeCoachingSession);
            }
        }
    }

    /**
    *  handleAfterInsert
    *  Trigger Handler method for CoachingSessionRecords.trigger
    *  only called for After trigger, after class record is created or updated
    *  
    *  @param Trigger.new --> List of new or updated Class records
    *  @param Trigger.OldMap --> List of old version of the class records
    *  @return void
    */
    public static void handleAfterInsert(List<Class__c> newUpdatedClasses, Map<Id, Class__c> oldClassVersionsMap) {
        // handler logic
        Set<Id> classIds = new Set<Id>(); 
        //get all Class Id's
        for(Class__c newUpdatedClass: newUpdatedClasses) {
            classIds.add(newUpdatedClass.Id);
        }
          //Coaching sesion list to be updated
        List<Coaching_Session__c> coachingSessionListUpdate=new List<Coaching_Session__c> ();
        
        //Coaching session list to be deleted
        List<Coaching_Session__c> coachingSessionListDelete=new List<Coaching_Session__c> ();
        
        //get all existing future Coaching sessions related to all Class records
        Map<Id, Coaching_Session__c> existingCoachingSessions = new Map<Id, Coaching_Session__c>(
            [SELECT Id, hmr_Class__c, hmr_Class_Name__c, hmr_Class_Date__c FROM Coaching_Session__c 
                WHERE hmr_Class__c In :classIds AND hmr_Class_Date__c >= TODAY]);
          system.debug('existing coaching session'+ existingCoachingSessions.size());      
        //instantiating coaching session lists to insert and delete                        
        List<Coaching_Session__c> coachingSessionsToInsert = new List<Coaching_Session__c>();
        //List<Coaching_Session__c> coachingSessionsToDelete = new List<Coaching_Session__c>();

        for(Class__c newUpdatedClass: newUpdatedClasses) {
            //check if the Trigger is an insert or an update
            if(oldClassVersionsMap != null && !oldClassVersionsMap.values().isEmpty()) {
                   //get older version of the class record
                Class__c getOldVersionOfClass = oldClassVersionsMap.get(newUpdatedClass.Id);
              
                //verify if there is change in any of below field 
                // Day of the week, Time, Coach, Start Date, Class Strength, Conference call #, participant code, host code
                if(getOldVersionOfClass.hmr_Start_Time__c != newUpdatedClass.hmr_Start_Time__c || getOldVersionOfClass.hmr_Coach__c != newUpdatedClass.hmr_Coach__c){
                    for(Coaching_Session__c existingCoachingSession: existingCoachingSessions.values()) {
                        Coaching_Session__c coachingSessionUpdate = new Coaching_Session__c();
                        coachingSessionUpdate.Id=existingCoachingSession.Id;
                        coachingSessionUpdate.hmr_Start_Time__c = newUpdatedClass.hmr_Start_Time__c;
                        coachingSessionUpdate.hmr_End_Time__c= newUpdatedClass.hmr_End_Time__c;
                        coachingSessionUpdate.hmr_Coach__c = newUpdatedClass.hmr_Coach__c;
                        coachingSessionListUpdate.add(coachingSessionUpdate);
                    }
                } 
                
                //Validation for one time class checkbox
                if(getOldVersionofClass.hmr_One_Time_Class__c != newUpdatedClass.hmr_One_Time_Class__c) {
                    //you cannot update an active class to be a one time class
                    if(newUpdatedClass.hmr_One_Time_Class__c == true && getOldVersionOfClass.hmr_Active__c == true) {
                        newUpdatedClass.hmr_One_Time_Class__c.addError('You cannont update an active class to be a One Time class!');
                    }
                    else {
                        //you can update an inactive class to be a one time class
                        if(getOldVersionOfClass.hmr_Active__c == true) {
                            for(Coaching_Session__c existingCoachingSession: existingCoachingSessions.values()) {
                                if(existingCoachingSession.hmr_Class__c == newUpdatedClass.Id) {
                                    coachingSessionListDelete.add(existingCoachingSession);
                                }
                            }
                        }
                    }
                }
                
                //Validation for first class date should not be less than today.
                if((getOldVersionofClass.hmr_First_Class_Date__c != newUpdatedClass.hmr_First_Class_Date__c) && 
                        newUpdatedClass.hmr_First_Class_Date__c < Date.today()) {
                      newUpdatedClass.hmr_First_Class_Date__c.addError('First Class Date Can Not be less than Today!');
                }
                
                  
                //If class is deactivated OR 'First class Date' is changed delete all coaching session records 
                if((getOldVersionOfClass.hmr_Active__c != newUpdatedClass.hmr_Active__c && newUpdatedClass.hmr_Active__c == false) ||
                   (getOldVersionofClass.hmr_First_Class_Date__c != newUpdatedClass.hmr_First_Class_Date__c && newUpdatedClass.hmr_First_Class_Date__c > Date.today())) {
                        for(Coaching_Session__c existingCoachingSession: existingCoachingSessions.values()) {
                            if(existingCoachingSession.hmr_Class__c == newUpdatedClass.Id) {
                                coachingSessionListDelete.add(existingCoachingSession);
                            }
                        } 	 
                    //call method if there is a change in the Active status field
                }
                if((getOldVersionOfClass.hmr_Active__c != newUpdatedClass.hmr_Active__c && newUpdatedClass.hmr_Active__c == true) ||
                   (getOldVersionofClass.hmr_First_Class_Date__c != newUpdatedClass.hmr_First_Class_Date__c && newUpdatedClass.hmr_First_Class_Date__c > Date.today())) {
                    ProcessClassAndCoachingSessionRecords(newUpdatedClass, existingCoachingSessions, coachingSessionsToInsert); 
                }
            }
            else {
                //call method if a new class record is inserted
                ProcessClassAndCoachingSessionRecords(newUpdatedClass, existingCoachingSessions, coachingSessionsToInsert);                    
            }            
        } 
        try {    
               
            if(!coachingSessionListDelete.isEmpty())
                delete coachingSessionListDelete;               
               
            if(!coachingSessionsToInsert.isEmpty())    
                insert coachingSessionsToInsert;
                
            if(!coachingSessionListUpdate.isEmpty()){
                update coachingSessionListUpdate;
            }         
        }
        catch(Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}