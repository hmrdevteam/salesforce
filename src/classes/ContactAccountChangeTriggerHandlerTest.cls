/**
* @Date: 08/22/2017
* @Author: Ali Pierre (HMR)
*/

@isTest(SeeAllData=False)
public class ContactAccountChangeTriggerHandlerTest {
    static testMethod void testP1(){

        Test.startTest();

        Date dt = Date.today();
        
        List<Account> acctList = new List<Account>();

        //create an Account       
        Account testAccountObj = new Account(Name = 'Test Account');
        acctList.add(testAccountObj);

        Account testAccountObj2 = new Account(Name = 'Test Account2');
        acctList.add(testAccountObj2);

        Account testAccountObj3 = new Account(Name = 'Test Account3');
        acctList.add(testAccountObj3);
        insert acctList;

        //create Contacts associated to the Account created and insert them
        List<Contact> conList = new List<Contact>();        
       	
       	Contact testContactObj = new Contact(FirstName = 'Test', LastName = 'Test', Account = testAccountObj2, hmr_Received_Free_Entrees__c = false);
        conList.add(testContactObj);

        Contact testContactObj2 = new Contact(FirstName = 'Test', LastName = 'Test2', Account = testAccountObj, hmr_Received_Free_Entrees__c = false);
        conList.add(testContactObj2);
        insert conList; 
        
        //create the different kinds of available Programs and insert them
        List<Program__c> prgList = new List<Program__c>();        
        
        Program__c testProgramObj1 = new Program__c(Name = 'P1 Healthy Shakes', Days_in_1st_Order_Cycle__c = 14, Free_Promo_Item__c = null);
        prgList.add(testProgramObj1);

        Program__c testProgramObj2 = new Program__c(Name = 'P1 Healthy Solutions', Days_in_1st_Order_Cycle__c = 21, Free_Promo_Item__c = null);
        prgList.add(testProgramObj2);

        Program__c testProgramObj3= new Program__c(Name = 'Phase 2', Days_in_1st_Order_Cycle__c = 30, Free_Promo_Item__c = null);
        prgList.add(testProgramObj3); 
        insert prgList;

        //create HMR Coupons associated to the Account created and insert them
        List<HMR_Coupon__c> hmrCouponList = new List<HMR_Coupon__c>();

        HMR_Coupon__c testHmrCouponObj1 = new HMR_Coupon__c(Name = 'MERCK', Account__c = testAccountObj.Id, hmr_Start_Date__c = dt, hmr_End_Date__c = Date.newInstance( 2099, 1, 1 ));
        hmrCouponList.add(testHmrCouponObj1);
        insert hmrCouponList;

        //create coupons rule and insert
        List<ccrz__E_Rule__c> ccRuleList = new List<ccrz__E_Rule__c>();

        ccrz__E_Rule__c testRuleObj1 = new ccrz__E_Rule__c(ccrz__Name__c = 'COUPON RULE',ccrz__RuleType__c = 'CartTotal' , ccrz__RuleSource__c = 'Coupon', ccrz__RuleMinQty__c = 1, ccrz__RuleMinAmt__c = 50.00, ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Enabled__c = True);
        ccRuleList.add(testRuleObj1);

        insert ccRuleList;

        //create CC Coupons associated to HMR Coupns created and insert them
        List<ccrz__E_Coupon__c> ccCouponList = new List<ccrz__E_Coupon__c>();

        ccrz__E_Coupon__c testCouponObj1 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_1' , ccrz__CouponCode__c = 'MERCK_P1_1', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P1 1st Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 10.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj1);        
        ccrz__E_Coupon__c testCouponObj2 = new ccrz__E_Coupon__c(ccrz__CouponName__c = 'MERCK_P1_Reorder' , ccrz__CouponCode__c = 'MERCK_P1_Reorder', ccrz__CouponType__c = 'Percentage', Coupon_Order_Type__c = 'P1 Re-Order', HMR_Coupon__c = testHmrCouponObj1.Id , ccrz__DiscountAmount__c = 20.00, ccrz__DiscountType__c = 'Percentage', ccrz__CartTotalAmount__c = 50.00, ccrz__MaxUse__c = 10, ccrz__MinQty__c = 1 , hmr_Program__c = testProgramObj2.Id , ccrz__Rule__c = testRuleObj1.Id, ccrz__RuleType__c = 'CartTotal' ,ccrz__StartDate__c = dt, ccrz__EndDate__c = Date.newInstance( 2099, 1, 1 ), ccrz__Storefront__c = 'DefaultStore', ccrz__TotalUsed__c = 0);
        ccCouponList.add(testCouponObj2);
        
        insert ccCouponList;
        
        //create product categories and insert them
        List<ccrz__E_Category__c> CatList = new List<ccrz__E_Category__c>();        
        
        ccrz__E_Category__c testCategoryObj1 = new ccrz__E_Category__c(Name = 'Materials', ccrz__CategoryID__c = '0');
        CatList.add(testCategoryObj1);        
        ccrz__E_Category__c testCategoryObj2 = new ccrz__E_Category__c(Name = 'Entrees', ccrz__CategoryID__c = '100');
        CatList.add(testCategoryObj2);        
        ccrz__E_Category__c testCategoryObj3= new ccrz__E_Category__c(Name = 'Bars', ccrz__CategoryID__c = '200');
        CatList.add(testCategoryObj3);
        ccrz__E_Category__c testCategoryObj4= new ccrz__E_Category__c(Name = 'Shake/Soup', ccrz__CategoryID__c = '300');
        CatList.add(testCategoryObj4);
        ccrz__E_Category__c testCategoryObj5= new ccrz__E_Category__c(Name = 'Cereal', ccrz__CategoryID__c = '400');
        CatList.add(testCategoryObj5);
        ccrz__E_Category__c testCategoryObj6= new ccrz__E_Category__c(Name = '120', ccrz__CategoryID__c = '500');
        CatList.add(testCategoryObj6);
        ccrz__E_Category__c testCategoryObj7= new ccrz__E_Category__c(Name = 'Kits', ccrz__CategoryID__c = '600');
        CatList.add(testCategoryObj7);
        insert CatList;

        
        //create different kinds of Products, with & without kits, and insert them
        List<ccrz__E_Product__c> prdList = new List<ccrz__E_Product__c>();
        
        ccrz__E_Product__c testProductObj1 = new ccrz__E_Product__c(Name= 'Beef Stew', ccrz__SKU__c = '111111', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj1); 
        ccrz__E_Product__c testProductObj2 = new ccrz__E_Product__c(Name= 'Cheese Ravioli', ccrz__SKU__c = '222222', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj2);
        ccrz__E_Product__c testProductObj4 = new ccrz__E_Product__c(Name= 'P1 Kit', ccrz__SKU__c = '444444', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Dynamic Kit', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'), Program__c = testProgramObj2.Id);
        prdList.add(testProductObj4);
        ccrz__E_Product__c testProductObj8 = new ccrz__E_Product__c(Name= 'P1 Materials', ccrz__SKU__c = '546545', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Materials', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj8);
        ccrz__E_Product__c testProductObj120 = new ccrz__E_Product__c(Name= '120 Vanilla Shake', ccrz__SKU__c = '65646', ccrz__Quantityperunit__c = 1.000000, ccrz__ProductType__c = 'Product', ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdList.add(testProductObj120); 
        insert prdList;
  

        for(Program__c prg : prgList){
            if(prg.Name == 'P1 Healthy Solutions'){
                prg.Standard_Kit__c = testProductObj4.Id;
                prg.Standard_Kit_Materials__c = testProductObj8.Id;
            }
            prg.Free_Promo_Item__c = testProductObj2.Id;
        }
        update prgList;

        //create Product Category records
        List<ccrz__E_ProductCategory__c> prdCatList = new List<ccrz__E_ProductCategory__c>();
        
        ccrz__E_ProductCategory__c testProductCatObj1 = new ccrz__E_ProductCategory__c( ccrz__Product__c = testProductObj1.Id, ccrz__Category__c = testCategoryObj2.Id,  ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj1); 
        ccrz__E_ProductCategory__c testProductCatObj2 = new ccrz__E_ProductCategory__c(  ccrz__Product__c = testProductObj2.Id, ccrz__Category__c = testCategoryObj2.Id, ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj2);
        ccrz__E_ProductCategory__c testProductCatObj3 = new ccrz__E_ProductCategory__c(  ccrz__Product__c = testProductObj4.Id, ccrz__Category__c = testCategoryObj7.Id, ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj3);
        ccrz__E_ProductCategory__c testProductCatObj4 = new ccrz__E_ProductCategory__c(  ccrz__Product__c = testProductObj8.Id, ccrz__Category__c = testCategoryObj1.Id, ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj4);
        ccrz__E_ProductCategory__c testProductCatObj5 = new ccrz__E_ProductCategory__c(  ccrz__Product__c = testProductObj120.Id, ccrz__Category__c = testCategoryObj6.Id, ccrz__StartDate__c = date.parse('11/11/2016'), ccrz__EndDate__c = date.parse('12/31/2099'));
        prdCatList.add(testProductCatObj5);
        insert prdCatList;
        
        //insert CC cart records for Program orders
        List<ccrz__E_Cart__c> cartList = new List<ccrz__E_Cart__c>();
        
        //P1 Kit Cart
        ccrz__E_Cart__c testCartObj1 = new ccrz__E_Cart__c(ccrz__Contact__c = testContactObj2.Id, ccrz__Account__c = testAccountObj.Id);
        cartList.add(testCartObj1);

        ccrz__E_Cart__c testCartObj2 = new ccrz__E_Cart__c(ccrz__Contact__c = testContactObj.Id, ccrz__Account__c = testAccountObj2.Id, ccrz__CartStatus__c = 'Open');
        cartList.add(testCartObj2);
        insert cartList;

        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        
        
        //P1 Kit
        ccrz__E_CartItem__c testItemObj40 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__AbsoluteDiscount__c= (150.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (150.00 - (150.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))), ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c,  ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj7.Id, ccrz__ProductType__c = 'Dynamic Kit');
        cartItemList.add(testItemObj40);        
        //Materials
        ccrz__E_CartItem__c testItemObj130 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 0.00, ccrz__Product__c = testProductObj8.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c = 0.00,  ccrz__Category__c = testCategoryObj1.Id, ccrz__ProductType__c = 'Materials');
        cartItemList.add(testItemObj130);
        //Cheese Ravioli
        ccrz__E_CartItem__c testItemObj60 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 21, ccrz__AbsoluteDiscount__c= (42.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (42.00 - (42.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))),  ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c, ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj2.Id, ccrz__ProductType__c = 'Product');
        cartItemList.add(testItemObj60);
        //Beef Stew
        ccrz__E_CartItem__c testItemObj70 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 22, ccrz__AbsoluteDiscount__c= (44.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (44.00 - (44.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))),  ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c,  ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj2.Id, ccrz__ProductType__c = 'Product');
        cartItemList.add(testItemObj70);
        //120 Shake
        ccrz__E_CartItem__c testItemObj80 = new ccrz__E_CartItem__c(ccrz__Cart__c =testCartObj1.Id, ccrz__Price__c = 20.00, ccrz__Product__c = testProductObj120.Id, ccrz__Quantity__c = 5, ccrz__AbsoluteDiscount__c= (100.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (100.00 - (100.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))),  ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c,  ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj6.Id, ccrz__ProductType__c = 'Product');
        cartItemList.add(testItemObj80);
		System.debug('!!HERE' + testProductObj4.ccrz__ProductType__c);
        insert cartItemList;
        
        
        //Declare a new List of Program Membership object
        List<Program_Membership__c> prgMemList = new List<Program_Membership__c>();

        //Create Program Membership for testContactObj2 to associate to a test order
        Program_Membership__c prgMemObj1 = new Program_Membership__c(Program__c = testProgramObj2.id, hmr_Status__c = 'Active',hmr_Contact__c = testContactObj2.id, hmr_Enrollment_Date__c = dt.addDays(-5));
        prgMemList.add(prgMemObj1);

        insert prgMemList;

        //insert the Orders with status as Submitted
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();
                
        
        ccrz__E_Order__c testOrderObj3 = new ccrz__E_Order__c(ccrz__Contact__c = testContactObj.Id, ccrz__Account__c =  testAccountObj2.id, ccrz__OrderDate__c = dt.addDays(6), ccrz__OrderStatus__c = 'Template', hmr_Program_Member__c = True, hmr_Program_Membership__c = prgMemObj1.Id);
        orderList.add(testOrderObj3);
        //P1 Kit
        ccrz__E_Order__c testOrderObj4 = new ccrz__E_Order__c(ccrz__OriginatedCart__c = testCartObj1.Id, ccrz__Contact__c = testContactObj2.Id, ccrz__Account__c =  testAccountObj.id, ccrz__OrderDate__c = dt.addDays(6));
        orderList.add(testOrderObj4);
        insert orderList;


        
        //insert the corresponding Order items for the inserted Orders
        List<ccrz__E_OrderItem__c> orderItemList = new List<ccrz__E_OrderItem__c>();        
        //P1 Kit
        ccrz__E_OrderItem__c testItemObj4 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 150.00, ccrz__Product__c = testProductObj4.Id, ccrz__Quantity__c = 1, ccrz__AbsoluteDiscount__c= (150.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (150.00 - (150.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))), ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c, ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj7.Id);
        orderItemList.add(testItemObj4);     
        //Beef Ravioli
        ccrz__E_OrderItem__c testItemObj6 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj1.Id, ccrz__Quantity__c = 21, ccrz__AbsoluteDiscount__c= (42.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (42.00 - (42.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))),  ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c, ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj2.Id);
        orderItemList.add(testItemObj6);
        //Beef Stew
        ccrz__E_OrderItem__c testItemObj7 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj3.Id, ccrz__Price__c = 2.00, ccrz__Product__c = testProductObj2.Id, ccrz__Quantity__c = 22, ccrz__AbsoluteDiscount__c= (44.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100)), ccrz__SubAmount__c = (44.00 - (44.00 * (testCouponObj1.ccrz__DiscountAmount__c / 100))),  ccrz__PercentDiscount__c = testCouponObj1.ccrz__DiscountAmount__c, ccrz__Coupon__c = testCouponObj1.Id, ccrz__Category__c = testCategoryObj2.Id);
        orderItemList.add(testItemObj7);
        //Materials
        ccrz__E_OrderItem__c testItemObj13 = new ccrz__E_OrderItem__c(ccrz__Order__c =testOrderObj4.Id, ccrz__Price__c = 0.00, ccrz__Product__c = testProductObj8.Id, ccrz__Quantity__c = 1, ccrz__SubAmount__c = 0.00, ccrz__Category__c = testCategoryObj1.Id);
        orderItemList.add(testItemObj13);          
        
        
        insert orderItemList;

         //insert payment details
        List<ccrz__E_TransactionPayment__c> paymentList = new List<ccrz__E_TransactionPayment__c>();
        
        //P1 Kit
        ccrz__E_TransactionPayment__c testpaymentDetailObj1 = new ccrz__E_TransactionPayment__c(
                        ccrz__AccountNumber__c = '54544',
                        ccrz__CCOrder__c = testOrderObj4.Id,
                        ccrz__PaymentType__c = 'Visa',
                        ccrz__ExpirationMonth__c = 12,
                        ccrz__ExpirationYear__c = 2018,
                        ccrz__AccountType__c = 'Credit Card',
                        ccrz__Token__c = '45454hgjhf');
        paymentList.add(testpaymentDetailObj1);
        insert paymentList;

         //checks to see Template Orders were created for order
        List<ccrz__E_Order__c> templateOrderList = new List<ccrz__E_Order__c>(
            [SELECT Id, ccrz__OrderStatus__c FROM ccrz__E_Order__c 
             WHERE hmr_Order_Type_c__c Like '%Reorder%' AND ccrz__OrderStatus__c = 'Template']);
        
        System.debug('@@@@@@@ in TEST CLASS templateOrderList' + ' ' + templateOrderList.size());
        
        if(!templateOrderList.isEmpty()){
            System.assertEquals(templateOrderList.size(), 1);
        }
        
        for(ccrz__E_Order__c complete : orderList){
            complete.ccrz__OrderStatus__c = 'Completed';
        }
        update orderList;
        
         //checks to see Cloned Orders were created for the 4 orders
        List<ccrz__E_Order__c> clonedOrderList = new List<ccrz__E_Order__c>(
            [SELECT Id, ccrz__OrderStatus__c, ccrz__Account__c, Program_Membership_Status__c FROM ccrz__E_Order__c 
             WHERE hmr_Order_Type_c__c Like '%Reorder%' AND ccrz__OrderStatus__c <> 'Template']);
        
        System.debug('@@@@@@@ in TEST CLASS clonedOrderList' + ' ' + clonedOrderList.size());
        System.debug('@@@@@@@ in TEST CLASS Cloned order' + ' ' + clonedOrderList);
        
        if(!clonedOrderList.isEmpty()){
            System.assertEquals(clonedOrderList.size(), 1);
        }

        List<Contact> conctList = new List<Contact>(
            [SELECT Id, AccountId FROM Contact]);
        
        for(Contact c : conctList){    
        	c.AccountId = testAccountObj3.Id;
        }

        System.debug('@@@@@@@ in TEST CLASS Cloned order' + ' ' + conctList);
        
        update conctList;

        Test.stopTest();
    }
}