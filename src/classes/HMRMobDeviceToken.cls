/**
 * Rest service to store the user's device token.  The Contact can have many tokens and these tokens are stored
 * in json format in the Contact.hmr_Diet_Tracker_Token__c field
 * This token is used to send push notifications
 * @author Javier Arroyo
 * @see HMRPushNotificationJob
 * @see HMRMobDeviceTokenDTO
 */ 
@RestResource(urlMapping='/mobile/user/token/*')
global with sharing class HMRMobDeviceToken {

    @HttpPost
    global static void doPost(HMRDeviceTokenDTO token) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();
        
        String contactId = RestContext.request.params.get('contactId');
        Contact[] contacts = [ select id, hmr_Diet_Tracker_Token__c from contact where id =: contactId ];
        if ( contacts.size() == 0 ) {
            RestContext.response.statusCode = 404;            
            resultMap.put('success', true);
        }
        
        Contact c = contacts[0];
        List<HMRDeviceTokenDTO> tokens;
        // no tokens exist, just insert the new one
        if (c.hmr_Diet_Tracker_Token__c == null) {
            // just insert the new token
            tokens = new List<HMRDeviceTokenDTO>();
            tokens.add(token);
            c.hmr_Diet_Tracker_Token__c = JSON.serialize(tokens);
            update c;
        }
        else {
            tokens = (List<HMRDeviceTokenDTO>)JSON.deserialize(c.hmr_Diet_Tracker_Token__c, List<HMRDeviceTokenDTO>.class);
            System.debug('===> tokens');
            System.debug(tokens);
            // if the token os and device type match, then replace it, otherwise, add the new token
            Boolean found = false;
            for(HMRDeviceTokenDTO t: tokens) {                
                if (t.os == token.os && t.deviceType == token.deviceType) {
                    //replace it
                    t.token = token.token;
                    found = true;
                    break;
                }                
            }           
            if (!found) {
                //append the new token to the list
                tokens.add(token);                
            }
            c.hmr_Diet_Tracker_Token__c = JSON.serialize(tokens);
            update c;
        }
        RestContext.response.statusCode = 201;
        resultMap.put('success', true);
		response.responseBody = Blob.valueOf(JSON.serialize(resultMap));        
    }
    
    /*
    @HttpGet
    global static void doGet() {
        String contactId = RestContext.request.params.get('contactId');
        Contact[] contacts = [ select id, hmr_Diet_Tracker_Token__c from contact where id =: contactId ];
        if (contacts.size() == 1) {
            //return contacts[0].hmr_Diet_Tracker_Token__c;
            RestContext.response.responseBody = Blob.valueOf(contacts[0].hmr_Diet_Tracker_Token__c);
        }
        else {
	        RestContext.response.responseBody = Blob.valueOf( '{"message": "no token for this contact"}' );
        }
    }*/
	   
}