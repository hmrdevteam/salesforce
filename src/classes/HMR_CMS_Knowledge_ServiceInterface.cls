/**
* Apex Conversation Service Interface for Join the Conversation
*
* @Date: 2017-07-24
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-07-24
* @JIRA: 
*/
global class HMR_CMS_Knowledge_ServiceInterface implements cms.ServiceInterface {
    global HMR_CMS_Knowledge_ServiceInterface() {}
    global HMR_CMS_Knowledge_ServiceInterface(cms.CoreController controller){}

    /**
    *
    * @param params a map of parameters including at minimum a value for 'action'
    * @return a JSON-serialized response string
    */
    public String executeRequest(Map<String, String> params) {
        Map<String, Object> returnMap = new Map<string, Object>();

        try {
            String action = params.get('action');
            HMR_Knowledge_Service knowledgeService = new HMR_Knowledge_Service();

            if(action == 'getNextCardSet'){
                String listTitle = String.escapeSingleQuotes(params.get('listTitle'));
                String objectType = String.escapeSingleQuotes(params.get('objectType'));
                String dataCategory = String.escapeSingleQuotes(params.get('dataCategory'));
                String recipeCategory = String.escapeSingleQuotes(params.get('recipeCategory'));
                String subFilters = String.escapeSingleQuotes(params.get('subFilters'));
                Integer offset = Integer.valueOf(params.get('offset'));
                Integer recordCount = params.get('recordCount') != null ? Integer.valueOf(params.get('recordCount')) : 0;
                String recordsShown = params.get('recordsShown') != null ? String.escapeSingleQuotes(params.get('recordsShown')) : '';
                String colorBlocksShown = params.get('colorBlocksShown') != null ? String.escapeSingleQuotes(params.get('colorBlocksShown')) : '';
                HMR_CMS_CardList_ContentTemplate template = new HMR_CMS_CardList_ContentTemplate(listTitle, objectType, dataCategory, recipeCategory, subFilters, offset, recordCount, recordsShown, colorBlocksShown);
                returnMap.put('htmlResult', template.getHTML());
                returnMap.put('replaceAll', offset == 0);
                returnMap.put('success', true);
            }
            else if(action == 'getVote'){
                String articleName = String.escapeSingleQuotes(params.get('articleName'));
                Vote voteRecord = knowledgeService.getVote(articleName);
                returnMap.put('voteRecord', voteRecord);
                returnMap.put('success', true);
            }
            else if(action == 'getVoteCount'){
                String articleName = String.escapeSingleQuotes(params.get('articleName'));
                Integer voteCount = knowledgeService.getVoteCount(articleName);
                returnMap.put('voteCount', voteCount);
                returnMap.put('success', true);
            }
            else if(action == 'likeArticle') {
                String articleName = String.escapeSingleQuotes(params.get('articleName'));
                Vote voteRecord = knowledgeService.voteForKnowledgeArticle(articleName);
                returnMap.put('voteRecord', voteRecord);
                returnMap.put('success', true);
            }
            
            else if(action == 'getNextPage') {
                String pageNumber = String.escapeSingleQuotes(params.get('pageNumber'));
                HMR_CMS_SuccessStoryList_ContentTemplate successListTemplate = new HMR_CMS_SuccessStoryList_ContentTemplate(pageNumber);
                returnMap.put('htmlResult', successListTemplate.getHTML());
               // returnMap.put('replaceAll', offset == 0);
                returnMap.put('success', true);
            }
            
            else{
                //Invalid Action
                returnMap.put('success',false);
                returnMap.put('message','Invalid Action');
            } 
        } 
        catch(Exception e) {
            // Unexpected Error
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMap.put('success',false);
            returnMap.put('message',JSON.serialize(message));
        }
        
        return JSON.serialize(returnMap);
    }

     public static Type getType() {
        return HMR_CMS_Knowledge_ServiceInterface.class;
    }
}