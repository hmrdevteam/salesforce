/*****************************************************
 * Author: Joey Zhuang
 * Created Date: 11 May 2017
 * Created By: Joey Zhuang
 * Last Modified Date: 2017-06-05
 * Last Modified By: Zach Engman - Updated for deletions and to use our trigger framework
 * Description:
 When Stored Payment is updated
 Update all related Transaction Payment to this up to date one
 All template order and pending order.
 Code Review Comments: (Pranay Mistry) add Exception handling (5/12/17: 12:15 AM)
 * ****************************************************/ 
 public without sharing class StoredPayment_TriggerHandler {
   private boolean isExecuting = false;
   private integer batchSize = 0;
   public static Boolean isBeforeDeleteFlag = false;
   public static Boolean isAfterUpdateFlag = false;

   public StoredPayment_TriggerHandler(boolean executing, integer size){
     isExecuting = executing;
     batchSize = size;
   }

   public void onBeforeDelete(List<ccrz__E_StoredPayment__c> storedPaymentList, Map<Id, ccrz__E_StoredPayment__c> storedPaymentMap){
      Set<Id> autoDeliveryStoredPaymentSet = getAutoDeliveryOrInProcessStoredPaymentSet(storedPaymentList);
      if(autoDeliveryStoredPaymentSet.size() > 0){
         for(ccrz__E_StoredPayment__c storedPaymentRecord : storedPaymentList){
            if(autoDeliveryStoredPaymentSet.contains(storedPaymentRecord.Id))
               storedPaymentRecord.addError(System.Label.HMR_Delete_Credit_Card_In_Use_Message);
         }
      }
   }

   private Set<Id> getAutoDeliveryOrInProcessStoredPaymentSet(List<ccrz__E_StoredPayment__c> storedPaymentList){
      Set<Id> autoDeliveryStoredPaymentSet = new Set<Id>();
      Set<Id> userIdSet = new Set<Id>();
      for(ccrz__E_StoredPayment__c storedPaymentRecord : storedPaymentList){
         if(!String.isBlank(storedPaymentRecord.ccrz__User__c) &&  !userIdSet.contains(storedPaymentRecord.ccrz__User__c)){
            userIdSet.add(storedPaymentRecord.ccrz__User__c);
         }
      }

      if(userIdSet.size() > 0){
         Map<Id, ccrz__E_TransactionPayment__c> storedPaymentIdToTransactionRecordMap = new Map<Id, ccrz__E_TransactionPayment__c>();

         for(ccrz__E_TransactionPayment__c transactionPaymentRecord : [SELECT ccrz__StoredPayment__c 
                                                                       FROM ccrz__E_TransactionPayment__c 
                                                                       WHERE ccrz__User__c IN: userIdSet 
                                                                        AND (ccrz__CCOrder__r.ccrz__OrderStatus__c = 'Pending' 
                                                                             OR ccrz__CCOrder__r.ccrz__OrderStatus__c = 'Order Submitted' 
                                                                             OR (ccrz__CCOrder__r.ccrz__OrderStatus__c = 'Template' AND ccrz__CCOrder__r.hmr_Program_Membership__r.hmr_Status__c = 'Active'))]){
            if(!String.isBlank(transactionPaymentRecord.ccrz__StoredPayment__c))
               storedPaymentIdToTransactionRecordMap.put(transactionPaymentRecord.ccrz__StoredPayment__c, transactionPaymentRecord);
         }
     
         if(storedPaymentIdToTransactionRecordMap.size() > 0){
            for(ccrz__E_StoredPayment__c storedPaymentRecord : storedPaymentList){
               if(storedPaymentIdToTransactionRecordMap.containsKey(storedPaymentRecord.Id))
                  autoDeliveryStoredPaymentSet.add(storedPaymentRecord.Id);
            }
            
         }

      }

      return autoDeliveryStoredPaymentSet;
   }

   public void onAfterUpdate(List<ccrz__E_StoredPayment__c> newObj, Map<Id, ccrz__E_StoredPayment__c> newMap, List<ccrz__E_StoredPayment__c> oldObj, Map<Id, ccrz__E_StoredPayment__c> oldMap) {
   	list<Id> updatedToken = new list<Id>();
   	//if token is updated, mark this stored payment as need to update
   	for(ccrz__E_StoredPayment__c sp: newObj){
   		if(sp.ccrz__Token__c != oldMap.get(sp.Id).ccrz__Token__c)
   		updatedToken.add(sp.Id);
   	}

   	if(updatedToken.size()>0){
   		//query the transaction payment with Template or Pending Status
   		list<ccrz__E_TransactionPayment__c> updateMe = [Select ccrz__Token__c, ccrz__CCOrder__r.ccrz__OrderStatus__c, ccrz__CCOrder__c,ccrz__StoredPayment__c From ccrz__E_TransactionPayment__c where ccrz__StoredPayment__c in : updatedToken AND (ccrz__CCOrder__r.ccrz__OrderStatus__c = 'Template' OR ccrz__CCOrder__r.ccrz__OrderStatus__c = 'Pending')];
   		for(ccrz__E_TransactionPayment__c tp: updateMe){

         	tp.ccrz__Token__c = newMap.get(tp.ccrz__StoredPayment__c).ccrz__Token__c;
   		}
   		//udpate the token
   		if(updateMe.size()>0){
   			update updateMe;
   		}
   	}
   }
}