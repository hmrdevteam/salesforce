/**
* @Date: 
* @Author:
* @JIRA: 
* @Modified: Joey Zhuang(Magnet360) 8/18/2017
* 
* Class HMR_CC_PlaceOrderBatchJob
*/

@isTest
private class HMR_CC_PlaceOrderBatchJob_Test {
    /******************************************************************************
* Test the constructor for the CartDetail page override
*/
    static testMethod void constructor_Test(){
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        
        ccrz__E_AccountGroup__c accountGrp = new ccrz__E_AccountGroup__c(Name = 'PortalAccount');
        insert accountGrp;
        
        Account testAcc = new Account(Name = 'testAcc',ccrz__E_AccountGroup__c= accountGrp.Id);
        insert testAcc; 
        
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
        
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert userTest;
        
        
        List<Contact> contac = [Select LastName From Contact Where Id= :userTest.contactId];
        List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(1);
        orderList[0].ccrz__EncryptedId__c = 'TestEncryptedId';
        orderList[0].ccrz__OrderStatus__c = 'Pending';
        orderList[0].hmr_Order_Type_c__c = 'P2 1st Order';
        update orderList[0];
        
        ccrz__E_TransactionPayment__c tp = new ccrz__E_TransactionPayment__c(); 
		//tp.ccrz__AccountNumber__c = responseMap.get('req_card_number');
		//tp.ccrz__AccountType__c = 'cc';
		//tp.Card_Type__c = Decimal.valueOf(responseMap.get('req_card_type'));
		//tp.ccrz__Amount__c = Decimal.valueOf(responseMap.get('auth_amount'));
		//tp.ccrz__RequestAmount__c = Decimal.valueOf(responseMap.get('req_amount'));
		tp.ccrz__Token__c = 'aaa1234341241';
		tp.ccrz__CCOrder__c = orderList[0].Id;
		insert tp;

		map<string,string> fakeResponse = new map<string,string>();
		fakeResponse.put('decision','ACCEPT');
		fakeResponse.put('req_card_number','123123123123');
		fakeResponse.put('req_card_type','01');
		fakeResponse.put('auth_amount','13');
		fakeResponse.put('req_amount','13');
		fakeResponse.put('message','message');
		fakeResponse.put('req_card_expiry_date','022019123');
		fakeResponse.put('payment_token','022019');
		fakeResponse.put('req_transaction_uuid','req_transaction_uuid');
		fakeResponse.put('reason_code','reason_code');
		fakeResponse.put('req_transaction_type','req_transaction_type');

        Test.setMock(HttpCalloutMock.class, new HMR_Cybersource_CalloutMock(200, 'Success', 'Success', new Map<String, String>()));
        Test.startTest();
	    HMR_CC_PlaceOrderBatchJob b = new HMR_CC_PlaceOrderBatchJob();
	    b.testResponse = fakeResponse;
	    database.executebatch(b);	    

		fakeResponse.put('decision','NOTACCEPT');
		b.testResponse = fakeResponse;
	    database.executebatch(b);	    
	    Test.stopTest();
    }
}