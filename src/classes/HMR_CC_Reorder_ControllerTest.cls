/**
* Test Class Coverage of the HMR_CMS_Reorder_Controller
*
* @Date: 08/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/18/2017
* @JIRA: 
*/
@isTest
private class HMR_CC_Reorder_ControllerTest {
    @testSetup
    private static void setupTestData(){
        //Setup Catalog
        cc_dataFactory.setupCatalog();
        //Create test user
        User userRecord = cc_dataFactory.testUser;

        //Create kit product
        ccrz__E_Product__c ccrzKitProduct = new ccrz__E_Product__c(Name = 'Healthy Solutions® Quick Start® Kit'
                                                                   ,ccrz__SKU__c = 'HSQSKRVC'
                                                                   ,ccrz__ProductType__c = 'Dynamic Kit'
                                                                   ,ccrz__ProductStatus__c = 'Released'
                                                                   ,hmr_ProductActive__c = true
                                                                   ,ccrz__Storefront__c = 'DefaultStore'
                                                                   ,ccrz__QuantityPerUnit__c = 1
                                                                   ,ccrz__StartDate__c = Date.today()
                                                                   ,ccrz__EndDate__c = Date.today());
        //Create shake product
        ccrz__E_Product__c ccrzShakeProduct = new ccrz__E_Product__c(Name = 'HMR 120 Vanilla Shake'
                                                                   ,ccrz__SKU__c = 'IH120V'
                                                                   ,ccrz__ProductType__c = 'Product'
                                                                   ,hmr_ProductActive__c = true
                                                                   ,ccrz__ProductStatus__c = 'Released'
                                                                   ,ccrz__Storefront__c = 'DefaultStore'
                                                                   ,ccrz__QuantityPerUnit__c = 1
                                                                   ,ccrz__StartDate__c = Date.today()
                                                                   ,ccrz__EndDate__c = Date.today());
            

        insert new List<ccrz__E_Product__c>{ccrzKitProduct, ccrzShakeProduct};

        //Create kit category
        ccrz__E_Category__c ccrzKitCategory = new ccrz__E_Category__c(Name = 'Kits'
                                                                      ,ccrz__CategoryID__c = '800'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 700);
        //Create bars category
        ccrz__E_Category__c ccrzBarCategory = new ccrz__E_Category__c(Name = 'Bars'
                                                                      ,ccrz__CategoryID__c = '100'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 100);
        //Create entrees category
        ccrz__E_Category__c ccrzEntreeCategory = new ccrz__E_Category__c(Name = 'Entrees'
                                                                          ,ccrz__CategoryID__c = '200'
                                                                          ,ccrz__StartDate__c = Date.today()
                                                                          ,ccrz__EndDate__c = Date.today()
                                                                          ,ccrz__Sequence__c = 200);

        //Create flavorings category
        ccrz__E_Category__c ccrzFlavoringCategory = new ccrz__E_Category__c(Name = 'Flavorings'
                                                                          ,ccrz__CategoryID__c = '300'
                                                                          ,ccrz__StartDate__c = Date.today()
                                                                          ,ccrz__EndDate__c = Date.today()
                                                                          ,ccrz__Sequence__c = 300);

        //Create shake category
        ccrz__E_Category__c ccrzShakeCategory = new ccrz__E_Category__c(Name = 'Shake/Soup'
                                                                      ,ccrz__CategoryID__c = '400'
                                                                      ,ccrz__StartDate__c = Date.today()
                                                                      ,ccrz__EndDate__c = Date.today()
                                                                      ,ccrz__Sequence__c = 400);
        //Create hmr product category
        ccrz__E_Category__c ccrzHMRProductCategory = new ccrz__E_Category__c(Name = 'HMR Products'
                                                                              ,ccrz__CategoryID__c = '700'
                                                                              ,ccrz__StartDate__c = Date.today()
                                                                              ,ccrz__EndDate__c = Date.today()
                                                                              ,ccrz__Sequence__c = 700);
        //Create materials category
        ccrz__E_Category__c ccrzMaterialsCategory = new ccrz__E_Category__c(Name = 'Materials'
                                                                              ,ccrz__CategoryID__c = '900'
                                                                              ,ccrz__StartDate__c = Date.today()
                                                                              ,ccrz__EndDate__c = Date.today()
                                                                              ,ccrz__Sequence__c = 900);

        insert new List<ccrz__E_Category__c>{ccrzKitCategory, ccrzBarCategory, ccrzEntreeCategory, ccrzFlavoringCategory, ccrzShakeCategory, ccrzHMRProductCategory, ccrzMaterialsCategory};

        //Create kit product category
        ccrz__E_ProductCategory__c ccrzKitProductCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzKitProduct.Id
                                                                                            ,ccrz__Category__c = ccrzKitCategory.Id
                                                                                            ,ccrz__StartDate__c = Date.today()
                                                                                            ,ccrz__EndDate__c = Date.today());
        //Create shake product category
        ccrz__E_ProductCategory__c ccrzShakeProductCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzShakeProduct.Id
                                                                                            ,ccrz__Category__c = ccrzShakeCategory.Id
                                                                                            ,ccrz__StartDate__c = Date.today()
                                                                                            ,ccrz__EndDate__c = Date.today());
            
        insert new List<ccrz__E_ProductCategory__c>{ccrzKitProductCategory, ccrzShakeProductCategory};

        //Create program
        Program__c programRecord = new Program__c(Name = 'P1 Healthy Solutions'
                                                 ,Standard_Kit__c = ccrzKitProduct.Id
                                                 ,Admin_Kit__c = ccrzKitProduct.Id
                                                 ,hmr_Program_Type__c = 'Healthy Solutions'
                                                 ,hmr_Phase_Type__c = 'P1'
                                                 ,Days_in_1st_Order_Cycle__c = 21);
        insert programRecord;

        //Create program membership 
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id
                                                                                  ,hmr_Active__c = true
                                                                                  ,hmr_Status__c = 'Active');
        insert programMembershipRecord;

        //Create order record
        List<ccrz__E_Order__c> orderList = cc_dataFactory.createOrders(3);
        orderList[0].ccrz__EncryptedId__c = 'TestEncryptedId';
        orderList[0].hmr_Order_Type_c__c = 'P1 Reorder';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        orderList[0].ccrz__Contact__c = userRecord.Contact.Id;
        orderList[1].ccrz__EncryptedId__c = 'TestEncrypted02Id';
        orderList[1].hmr_Order_Type_c__c = 'P2 1st Order';
        orderList[1].hmr_Program_Membership__c = programMembershipRecord.Id;
        orderList[1].ccrz__Contact__c = userRecord.Contact.Id;
        orderList[2].ccrz__EncryptedId__c = 'TestEncryptedTemplateId';
        orderList[2].hmr_Order_Type_c__c = 'P1 1st Order';
        orderList[2].ccrz__OrderStatus__c = 'Template';
        orderList[2].hmr_Program_Membership__c = programMembershipRecord.Id;
        orderList[2].ccrz__Contact__c = userRecord.Contact.Id;
        update orderList;

        ccrz__E_OrderItem__c parentKitOrderItem = new ccrz__E_OrderItem__c(ccrz__Order__c = orderList[1].Id
                                                                          ,ccrz__OrderLineType__c = 'Major'
                                                                          ,ccrz__Product__c = ccrzKitProduct.Id
                                                                          ,ccrz__Quantity__c = 1
                                                                          ,ccrz__OriginalQuantity__c = 1
                                                                          ,ccrz__Price__c = 200.00
                                                                          ,ccrz__SubAmount__c = 200.00);
        insert parentKitOrderItem;
    }

    @isTest
    private static void testSavingOrderWithUpdatesAsNonPortalUser(){
        User userRecord = [SELECT Id FROM User WHERE alias = 'cctest'];

        Test.setCurrentPage(Page.HMR_CC_Reorder);
        ApexPages.currentPage().getParameters().put('Id', 'TestEncryptedId');

        System.runAs(userRecord){
            Test.startTest();

            HMR_CC_Reorder_Controller controller = new HMR_CC_Reorder_Controller();
            controller.saveAdjustedOrder();

            Test.stopTest();
        }

        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error));
    }

    @isTest
    private static void testSavingOrderWithUpdatesAsPortalUser(){
        User userRecord = [SELECT Id FROM User WHERE alias = 'cctest'];

        Test.setCurrentPage(Page.HMR_CC_Reorder);
        ApexPages.currentPage().getParameters().put('Id', 'TestEncrypted02Id');
        ApexPages.currentPage().getParameters().put('portalUser', userRecord.Id);

        System.runAs(userRecord){
            Test.startTest();

            HMR_CC_Reorder_Controller controller = new HMR_CC_Reorder_Controller();
            controller.saveAdjustedOrder();

            Test.stopTest();
        }

        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error));
    }

    @isTest
    private static void testGetProperties(){
        Test.startTest();

        HMR_CC_Reorder_Controller controller = new HMR_CC_Reorder_Controller();
        Boolean isProduction = HMR_CC_Reorder_Controller.IsProduction();
        Integer entreeMinimum = controller.entreeMinimum;
        Integer shakeMinimum = controller.shakeMinimum;
        Integer lactoseFreeMinimum = controller.lactoseFreeShakeMinimum;
        Integer orderTotalP2Minimum = controller.orderTotalP2;
        Integer csrOrderTotalP2Minimum = controller.csrOrderTotalP2;
        Integer csrShakeMinimum = controller.csrShakeMinimum;
        Integer csrEntreeMinimum = controller.csrEntreeMinimum;
        List<Order_Requirement__mdt> orderRequirementList = controller.orderRequirementList;

        Test.stopTest();
    }

    /******************************************************************************
    * Test the constructor for the CartDetail page override
*/
    static testMethod void constructor_Test(){
        Id p = [select id from profile where name='HMR Customer Community User'].id;
        
        ccrz__E_AccountGroup__c accountGrp = new ccrz__E_AccountGroup__c(Name = 'PortalAccount');
        insert accountGrp;
        
        Account testAcc = new Account(Name = 'testAcc',ccrz__E_AccountGroup__c= accountGrp.Id);
        insert testAcc; 
        
        Contact con = new Contact(FirstName = 'first', LastName ='testCon',AccountId = testAcc.Id);
        insert con;  
        
        User userTest = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert userTest;
        
        
        List<Contact> contac = [Select LastName From Contact Where Id= :userTest.contactId];
        
        
        System.runAs(userTest){ 
            
            Test.setCurrentPageReference(new PageReference('Page.HMR_CC_Reorder')); 
            //System.currentPageReference().getParameters().put('Id', orderList[0].ccrz__EncryptedId__c);  
            
            String productId = 'testProductId';
            String sku = 'testsku';
            String name = 'testname';
            Decimal price = 120;
            Decimal quant = 12;
            
            ccrz__E_Product__c ccrzProduct = new ccrz__E_Product__c(Name = 'test prod', ccrz__SKU__c = 'TestSku', ccrz__Quantityperunit__c = 2,
                                                                    ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today());
            
            insert ccrzProduct;
            
            HMR_CC_Reorder_Controller.ItemDetailWrapper itemDetailWrap = new HMR_CC_Reorder_Controller.ItemDetailWrapper();
            HMR_CC_Reorder_Controller.ItemDetailWrapper itemDetailWrap1 = new HMR_CC_Reorder_Controller.ItemDetailWrapper(sku, name, price, quant, false);
            HMR_CC_Reorder_Controller.ItemDetailWrapper itemDetailWrap2 = new HMR_CC_Reorder_Controller.ItemDetailWrapper(ccrzProduct);
            
            itemDetailWrap.productId = productId;
            itemDetailWrap.sku = sku;
            itemDetailWrap.name = name;
            itemDetailWrap.price = price;
            itemDetailWrap.quantity = quant;
            
            String catName = 'test category';
            String instruction = 'test instruct';
            
            List<HMR_CC_Reorder_Controller.ItemDetailWrapper> itmDetList = new List<HMR_CC_Reorder_Controller.ItemDetailWrapper>();
            itmDetList.add(itemDetailWrap);
            itmDetList.add(itemDetailWrap1);
            itmDetList.add(itemDetailWrap2);
            
            HMR_CC_Reorder_Controller.CategoryWrapper categoryWrap = new HMR_CC_Reorder_Controller.CategoryWrapper(catName, instruction);
            HMR_CC_Reorder_Controller.CategoryWrapper categoryWrap2 = new HMR_CC_Reorder_Controller.CategoryWrapper(catName, instruction);
            HMR_CC_Reorder_Controller.CategoryWrapper categoryWrap3 = new HMR_CC_Reorder_Controller.CategoryWrapper(catName, instruction, itmDetList);
            
            categoryWrap.name = catName;
            categoryWrap.instruction = instruction;
            categoryWrap.itemList = itmDetList;
            
            categoryWrap.compareTo(categoryWrap);
            
            
            
            Program__c programObject = new Program__c(Name = 'Test Program', Standard_Kit__c = ccrzProduct.id, Days_in_1st_Order_Cycle__c = 21);
            insert programObject;
            
            Program_Membership__c programMem = new Program_Membership__c(Program__c = programObject.id);
            insert programMem;
            
            ccrz__E_CompositeProduct__c compProd = new ccrz__E_CompositeProduct__c(ccrz__Composite__c = ccrzProduct.id);
            insert compProd;
            
            ccrz__E_Category__c ccrzCategory = new ccrz__E_Category__c(Name = 'test Category', ccrz__CategoryID__c = 'testId', ccrz__StartDate__c = Date.today(),
                                                                       ccrz__EndDate__c = Date.today(),ccrz__Sequence__c = 2000);
            insert ccrzCategory;
            
            ccrz__E_ProductCategory__c prodCategory = new ccrz__E_ProductCategory__c(ccrz__Product__c = ccrzProduct.id, 
                                                                                     ccrz__Category__c = ccrzCategory.id, ccrz__StartDate__c = Date.today(), ccrz__EndDate__c = Date.today() );
            
            insert prodCategory;
            
            ccrz__E_Order__c ccrzOrder = new ccrz__E_Order__c(ccrz__OrderDate__c = Date.today(), ccrz__Contact__c = userTest.contactId, ccrz__OrderStatus__c = 'Template',
                                                              hmr_Program_Membership__c = programMem.id, ccrz__EncryptedId__c = 'testencid', hmr_Order_Type_c__c = 'Auto Order');
            insert ccrzOrder;
            
            
            ccrz__E_OrderItem__c orderItem = new ccrz__E_OrderItem__c(ccrz__Category__c = ccrzCategory.id, ccrz__DisplayProduct__c = ccrzProduct.id,
                                                                      ccrz__Order__c = ccrzOrder.id, ccrz__Price__c = 1200, ccrz__Quantity__c = 2, ccrz__SubAmount__c = 1000
                                                                     );
            
            insert orderItem;
            
            String jsnStr = JSON.Serialize(orderItem);
            
            
            //  Contact testCon = [Select Id,AccountId from Contact where Id= :thisUser.contactId Limit 1];
            Contact testCon = [ select Id,AccountId,MailingState  from contact limit 1 ];
            testCon.AccountId  = testAcc.Id;
            testCon.MailingState = 'IL';
            update testCon;
            
            
            HMR_CC_Reorder_Controller ctrl = new HMR_CC_Reorder_Controller();
            
            Map<string, ccrz__E_OrderItem__c> orderItemMap = new Map<string, ccrz__E_OrderItem__c>();
            
            orderItemMap.put('test',orderItem);
            
            Map<string, ccrz__E_Product__c> productMap = new Map<string, ccrz__E_Product__c>();
            productMap.put('test', ccrzProduct);
            
            Map<string, ccrz__E_Category__c> CategoryMap = new Map<string, ccrz__E_Category__c>();
            CategoryMap.put('Bars', ccrzCategory);
            
            Map<string, HMR_CC_Reorder_Controller.CategoryWrapper> wrpMp = new Map<string, HMR_CC_Reorder_Controller.CategoryWrapper>();
            wrpMp.put('test',categoryWrap);
            
            
            HMR_CC_Reorder_Controller.OrderWrapper orderWrp = new HMR_CC_Reorder_Controller.OrderWrapper(ccrzOrder, wrpMp);
            
            orderWrp.shipAmount = 1200;
            orderWrp.subtotalAmount = 1400;
            orderWrp.taxAmount = 150;
            orderWrp.taxTotalAmount = 190;
            orderWrp.totalAmount = 1500;
            orderWrp.totalDiscount = 150;
            orderWrp.totalSurcharge = 80;
            orderWrp.orderType = 'test Order';
            orderWrp.wrapMap = wrpMp;
            
            orderWrp.couponType = 'fnf';
            orderWrp.couponOrderTotalAmount = 11;
            orderWrp.couponAmount = 12;
            
            
            ctrl.orderWrap = orderWrp;
            ctrl.programMembershipId = programMem.id;
            ctrl.contactId = userTest.contactId;
            ctrl.orderId = 'orderId';
            ctrl.encOrderId = 'testencid';
            ctrl.isCSRFlow = 'csrFlow';
            ctrl.portalUser = 'portalUser';
            ctrl.oneTimeOrder = FALSE;
            ctrl.orderItemMap = orderItemMap;
            ctrl.productMap = productMap;
            ctrl.categoryMap = CategoryMap;
            ctrl.wrapMap = wrpMp;
            ctrl.csrEntreeMinimum = null;
            ctrl.csrShakeMinimum = null;
            ctrl.orderRequirementList= null;
            ctrl.orderTotalP2 = null; 
            ctrl.entreeMinimum = null;
            ctrl.orderTotalP2 = null;
            List<Order_Requirement__mdt> orderRequirement = ctrl.orderRequirementList;
            Integer csrEntreeMin = ctrl.csrEntreeMinimum;
            Integer csrShakeMin = ctrl.csrShakeMinimum;
            Integer orTotalP2 = ctrl.orderTotalP2;
            Integer shakeMin = ctrl.shakeMinimum;
            Integer entreeMin = ctrl.entreeMinimum;
            
            ctrl.saveAdjustedOrder();
            
            System.assert(orderRequirement.size() > 0);
        }
    }
}