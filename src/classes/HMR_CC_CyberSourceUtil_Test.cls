/**
* @Date: 3/31/2017
* @Author: Aslesha Kokate (Mindtree)
* @JIRA: https://reside.jira.com/browse/HPRP-
*
* This test class covers HMR_CC_CyberSourceUtil Class
* Uses cc_dataFActory for test data
*/


@isTest
private class HMR_CC_CyberSourceUtil_Test {

  @isTest static void test_method_one() {
   
    cc_dataFActory.setupTestUser();
    User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            
    System.runAs ( thisUser ) {
    
        ccrz__E_StoredPayment__c objStoredpayment=new ccrz__E_StoredPayment__c(Name='Test');
        insert objStoredPayment;
        
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        
        System.currentPageReference().getParameters().put('mode', 'Edit');
        System.currentPageReference().getParameters().put('pid', objStoredPayment.id);  
        
        Cookie spCookie= new Cookie('pid',objStoredPayment.id,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{spCookie});
       
        HMR_CC_CyberSourceUtil objCyberClass=new HMR_CC_CyberSourceUtil();
      
        objCyberClass.getobjStoredPayment();
        DateTime tstTime=objCyberClass.signedDt;
        String signedDtString2=objCyberClass.signedDtString2;
        String isResponse=objCyberClass.isResponse;
        String saveFlag=objCyberClass.saveFlag;
        String exString=objCyberClass.exString;
        String spDisplayName=HMR_CC_CyberSourceUtil.spDisplayName;
        String spCardType=objCyberClass.spCardType;
        String spCardNumber=objCyberClass.spCardNumber;
        String spCVN=objCyberClass.spCVN;
        String spExpDate=objCyberClass.spExpDate;
        String CSREsponseErrorMessage=objcyberClass.CSREsponseErrorMessage;
        
        DateTime sampledate = Datetime.newInstance(2020, 2, 17);
      
        objCyberClass.getUTCDateTimeInGMT(sampledate);
            
        
        //check if parameters are set for authcode & dicision
        
        System.currentPageReference().getParameters().put('auth_code', '1234');
        System.currentPageReference().getParameters().put('decision', 'ERROR');
        System.currentPageReference().getParameters().put('pid',null ); 
        
        HMR_CC_CyberSourceUtil objCyberClass1=new HMR_CC_CyberSourceUtil();   
       
        system.assert(objcyberClass.signedFieldValues!=null); 
       
         }     
  }    


 @isTest static void test_method_two() {
   
    cc_dataFActory.setupTestUser();
    system.debug('annonomus account=====>'+cc_dataFActory.anonAccount);
    User thisUser = [ select Id,Name,AccountId from User where AccountId !=:NULL AND  IsActive=:true Limit 1];
    system.debug('acccount iddddddd========'+thisuser.AccountId);
    List<Account> acc = [Select Id from Account where id=:thisuser.AccountId limit 1];
   
   //acc.recordTypeId='01241000000Wlt4';
   //update acc; 
    
    system.debug(thisuser);
    
            
    System.runAs ( thisUser ) {

        Map<String , String> formdata=New Map<String, String>();
        formData.put('accountType','cc');
        formData.put('AccountNumber','4242424242424242');
        formData.put('Token','wd05DMHdNMVF3T0RvMU1Eb3hOeTR6TkRKYSwxeDV3Y');
        formData.put('PaymentType','001');
        formData.put('DisplayName','Test Payment');
        formData.put('expirationYear','11');
        formData.put('expirationMonth','2020');
        formData.put('auth_code','1233');
                   
        String jsonParam = JSON.serialize(formData);
       // String jsonParam='{"AccountType":"cc","AccountNumber":"xxxxxxxxxxxx4242","Token":"Ahj/7wSTCiaIFO+OX4NGESDdu1bMWjltFYV5zmbCmJb+u38WYClv67fxZ6QOnEG7KGTSTL0YrkWpYMJMKJogU745fg0YIVVG","PaymentType":"card","DisplayName":"test2 json","expirationMonth":"11","expirationYear":"2020","storedPaymentId":""}';
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ccrz.cc_RemoteActionResult rar = HMR_CC_CyberSourceUtil.saveCCNumber(ctx,jsonParam );
      //  System.assert(rar.success); 
        
        }
              

    }  
    
    @isTest static void test_method_three() {
   
        cc_dataFActory.setupTestUser();
        system.debug('annonomus account=====>'+cc_dataFActory.anonAccount);
        User thisUser = [ select Id,Name,AccountId from User where AccountId !=:NULL AND  IsActive=:true Limit 1];
        system.debug('acccount iddddddd========'+thisuser.AccountId);
        List<Account> acc = [Select Id from Account where id=:thisuser.AccountId limit 1];
        
        ccrz__E_StoredPayment__c objStoredpayment=new ccrz__E_StoredPayment__c(Name='Test');
        insert objStoredPayment;
   
        Map<String, String> responseCC = new Map<String,String>();
        Set<String> responseKey = new Set<String>();
        
        Map<String, Object> signedFields = new Map<String, Object>();
        Map<String, Object> unsignedFields = new Map<String, Object>();
        
        Map<String,String> userAddress = new Map<String, String>();
        userAddress.put('bill_to_forename', 'first');  
        userAddress.put('bill_to_surname', 'testCon');
        userAddress.put('bill_to_email', 'test@test.com');
        
        Map<String,String> unSigned = new Map<String, String>();
        unSigned.put('test', 'test');
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        HMR_CC_CyberSourceUtil utilController = new HMR_CC_CyberSourceUtil();
        utilController.formAction = 'test';
        utilController.responseCC = responseCC;
        utilController.responseKeys = responseKey;
        utilController.signedFields = signedFields;
        utilController.unsignedFields = unsignedFields;
        utilController.signedDtString = 'test';
        utilController.CSresponseflag = FALSE;
        utilController.myCreditCardSP = objStoredPayment;
        utilController.JLLCCRZDebug = 'test';
        utilController.storedPaymentId = 'testId';
        utilController.pageMode = 'test';
        utilController.CSResponseErrorFlag = TRUE;
        utilController.pidCookie = 'test';
        utilController.signature = 'test';
        
        utilController.getReferenceTime();
        HMR_CC_CyberSourceUtil.uuid();
        utilController.getUTCDateTime();
        utilController.initFormFields();
       // HMR_CC_CyberSourceUtil.CreatePaymentToken(ctx, thisUser.Id, userAddress, unSigned, '12:12', '12.:15');
        
        

    }  

}