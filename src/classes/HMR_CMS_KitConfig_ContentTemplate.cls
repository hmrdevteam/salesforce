/*
* Apex Content Template Controller for the Kit Config page
*
* @Date: 10/03/2017
* @Author Ali Pierre (HMR)
* @JIRA:
*/


global virtual class HMR_CMS_KitConfig_ContentTemplate extends cms.ContentTemplateController{

  public static boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
  }

  global HMR_CMS_KitConfig_ContentTemplate(cms.createContentController cc) {
    super(cc);
  }

  global HMR_CMS_KitConfig_ContentTemplate() {}

  /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	global String returnNotNullAndNotEmptyString(String value) {
		if(value != null && value != '') {
			return value;
		}
		return '';
	}

     public String kitSKU{
         get{
            return getPropertyWithDefault('kitSKU', '[]');
        }
     }

     public Boolean isAdmin {
		get {
            return getAttribute('isAdmin') == 'true';
        }
	}

	global virtual override String getHTML() {
		String html = '';
		String kitName = kitSKU;
		String baseUrl = IsProduction() ? '' : Site.getBaseUrl();
		try {
			//Set 'proceed to cart' button click function based on Admin setting
			String addToCartFunction = isAdmin == true ? 'hmrFlow.addCartClickAdmin' :'hmrFlow.addCartClick';
			//Declare and initialize Set of string with selected Kit SKU to pass into API call
			Set<String> kitProduct_SKUList = new Set<String>{kitSKU};

			Map<String, Object> kitInputData = new Map<String, Object>{
			    ccrz.ccAPI.API_VERSION => 6,
		        ccrz.ccApiProduct.PRODUCTSKULIST => kitProduct_SKUList,
				ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
				ccrz.ccAPIProduct.PARAM_BY_ASC => true,
		        ccrz.ccAPI.SIZING => new Map<String, Object>{
		            ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
		                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
		            }
		        }
			};

			Map<String, Object> kitOutputProductData = ccrz.ccAPIProduct.fetch(kitInputData);
			List<Map<String, Object>> kitOutputProductList = (List<Map<String, Object>>) kitOutputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);
			if(kitOutputProductList[0].get('sfdcName') != null){
              kitName = (String) kitOutputProductList[0].get('sfdcName');
            }

			Set<String> composite_sfidSet = new Set<String>();//Set to store composite product Ids for API call


			Map<String,Integer> quantityMap = new Map<String, Integer>();//Map of default product quantities within kit
			Map<String,String> productGroupMap = new Map<String, String>(); //Map of Product Groups/Categories

		    List<Map<String, Object>> compositeProductList = (List<Map<String, Object>>) kitOutputProductList[0].get('compositeProductsS');
		    //(FOR TESTING PURPOSES)Declare a Set of string containing a limited amount of SKUs to avoid exceeding CPU limit
		    Set<String> testSkuSet = new Set<String>{'IBSN', 'ICHBR', 'IH120C', 'IH120V', 'I70C', 'I70V'};
		    //Loop through kit product's composite products list and populate composite_sfidSet for 2nd Product API to retrieve product info
		    for(Map<String, Object> compositeProductObj : compositeProductList){
		        String product_sfid = '';
		        String product_SKU = '';
		        String product_Group = '';
		        Integer quantity = 0;

		        Map<String, Object> componentRelationshipMap = (Map<String, Object>)compositeProductObj.get('componentR');
		        Map<String, Object> componentProductGroupMap = (Map<String, Object>)compositeProductObj.get('productGroupR');

		        product_sfid = String.valueOf(componentRelationshipMap.get('sfid'));
		        product_SKU = String.valueOf(componentRelationshipMap.get('SKU'));
		        product_Group = String.valueOf(componentProductGroupMap.get('sfdcName'));

		        quantityMap.put(product_SKU, Integer.valueOf(compositeProductObj.get('quantity')));
		        productGroupMap.put(product_SKU, product_Group);

		        if(Test.isRunningTest()){ //If test is running add product Id only if the SKU is in the test SKU set
		        	if(testSkuSet.contains(product_SKU)){
		        		composite_sfidSet.add(product_sfid);
		        	}
		        }
		        else{
		        	composite_sfidSet.add(product_sfid);
		        }
		    }

			Map<String, Object> inputData = new Map<String, Object>{
			    ccrz.ccAPI.API_VERSION => 6,
		        ccrz.ccApiProduct.PRODUCTIDLIST => composite_sfidSet,
				ccrz.ccAPIProduct.PARAM_BY_SEQ => true,
				ccrz.ccAPIProduct.PARAM_BY_ASC => true,
		        ccrz.ccAPI.SIZING => new Map<String, Object>{
		            ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{
		                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
		            }
		        }
			};

			Map<String, Object> outputProductData = ccrz.ccAPIProduct.fetch(inputData);
			if(outputProductData != null && outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
			    List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputProductData.get(ccrz.ccAPIProduct.PRODUCTLIST);
				if(outputProductList.size() > 0) {

					//Get Product Ingredients Logic Start
					Set<String> product_sfid_Set = HMR_CMS_CCRZ_Utility.getProduct_keySet(outputProductList, 'sfid');
					Map<String, String> productIngredientsMap = HMR_CMS_CCRZ_Utility.getProductIngredients(product_sfid_Set);
					//Get Product Ingredients Logic End


					//Product Specs Logic Start
			        Set<String> productSpecIDSet = HMR_CMS_CCRZ_Utility.getProductSpecIDSet(outputProductList);
			        Map<String, Object> inputProductSpecData = new Map<String, Object>{
			            ccrz.ccAPISpec.SPECIDLIST => productSpecIDSet,
			            ccrz.ccAPI.API_VERSION => 3
			        };
			        Map<String, Object> outputProductSpectData = ccrz.ccAPISpec.fetch(inputProductSpecData);
			        List<Map<String, Object>> productSpecsMainList = new List<Map<String, Object>>();
			        if (outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST) != null) {
			            productSpecsMainList = (List<Map<String, Object>>) outputProductSpectData.get(ccrz.ccAPISpec.SPECLIST);
			        }
					String calorieSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Calories');
					String totalServingsSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Total Servings');
					String proteinSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Protein');
					String vegetarianSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Vegetarian');
					String servingSizeSpecId = HMR_CMS_CCRZ_Utility.getProductSpecRecord_sfid(productSpecsMainList, 'Serving Size');
					//Product Specs Logic End
					html+= '<main id="hmr_kit_config_step1" class="hmr_kit_config_step1">'+
							    '<section class="kit-config-content-section">'+
							        '<div class="container header">'+
							            '<div class="row">'+
							                '<div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">'+
							                    '<div class="row">'+
							                        '<h2>';
							                        if(kitSKU.contains('SHAKE')){
							                        	html+= '<span>Choose Your Shakes</span>';
							                        }
							                        else{
							                            html+= 'Step 1 of 3: <span>Shakes</span>';
							                        }
							                        html += '</h2>'+
							                    '</div>'+
							                '</div>'+
							            '</div>'+
							        '</div>';
							        if(!isAdmin){
							        	html+= '<div class="container kit-config-sub-header">'+
								            '<div class="row">'+
								                '<div class="hmr-form-control">'+
								                    '<input id="lacFree" type="checkbox" class="blue">'+
								                    '<label class="form-control__label hmr-allcaps blue" for="lacFree">';
								                    if(kitSKU.contains('SHAKE')){
								                        html += 'LACTOSE FREE SHAKES +$14.00';
								                    }
								                    else{
								                    	html += 'LACTOSE FREE SHAKES +$41.75';
								                    }
								                    html += '</label>'+
								                '</div>'+
								            '</div>'+
								        '</div>'+
								        '<div class="container-fluid">'+
								            '<div class="row">'+
								                '<div class="col-lg-1 col-md-1 bg-lightbluetransparent visible-lg visible-md">'+
								                '</div>'+
								                '<div class="col-lg-10 col-md-10 middle">'+
								                    '<div class="container no-padding">'+
								                        '<div class="row">'+
								                            '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 bg-lightbluetransparent hmr-flex">'+
								                                '<div class="container">'+
								                                    '<div class="col-lg-12 hmr-form-control no-padding">'+
								                                        '<label class="big-label heading no-padding">'+
								                                            'Choose One:'+
								                                        '</label>'+
								                                    '</div>'+
                                                                    '<div class="hidden-sm hidden-xs visible-lg visible-md">'+
    								                                    '<div class="col-lg-12 hmr-form-control no-padding">'+
    								                                        '<input id="vanilla" type="radio" name="shakes" class="blue">'+
    								                                        '<label class="form-control__label big-label heading" for="vanilla">'+
    								                                            'Vanilla'+
    								                                        '</label>'+
    								                                    '</div>'+
    								                                    '<div class="col-lg-12 hmr-form-control no-padding">'+
    								                                        '<input id="chocolate" type="radio" name="shakes" class="blue">'+
    								                                        '<label class="form-control__label big-label heading" for="chocolate">'+
    								                                            'Chocolate'+
    								                                        '</label>'+
    								                                    '</div>'+
    								                                    '<div class="col-lg-12 hmr-form-control no-padding">'+
    								                                        '<input id="vanillchocolate" type="radio" name="shakes" class="blue">'+
    								                                        '<label class="form-control__label big-label heading" for="vanillchocolate">'+
    								                                            'Vanilla &amp; Chocolate'+
    								                                        '</label>'+
    								                                    '</div>'+
                                                                    '</div>'+
								                                '</div>'+
								                            '</div>';
							        }
							        else{
								        html+= '<div class="container no-padding admin-container">'+
								            '<div class="row">'+
								                '<div class="col-lg-10 col-md-10 middle">'+
								                    '<div class="container no-padding">'+
								                        '<div class="row">';
							        }

					//Product HTML rendering Loop Start
					Integer tabIndex = 0;
					String entreeCardsHtml = '';
                    Boolean addMobileVanillaButton = false;
                    Boolean addMobileChocolateButton = false;
                    Boolean addMobileVanillaChocolateButton = false;
                    String greyBackgroundCSSClass = ' bg-grey';
			        for(Map<String, Object> productObject: outputProductList) {
						tabIndex++;
                        if(math.mod(tabIndex, 2) == 0) {
                            greyBackgroundCSSClass = '';
                        }
                        else {
                            greyBackgroundCSSClass = ' bg-grey';
                        }
			            String productMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image');
						String productThumbnailMediaURL = HMR_CMS_CCRZ_Utility.getProductMediaURL(productObject, 'Product Image Thumbnail');
						//Spec Logic Start
						String allergenHTML = HMR_CMS_CCRZ_Utility.getProductAllergenInfoHTML(productObject);
						String calorieSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, calorieSpecId);
						String totalServingsSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, totalServingsSpecId);
						String protienSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, proteinSpecId);
						String vegetarianSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, vegetarianSpecId);
						String servingSizeSpecification = HMR_CMS_CCRZ_Utility.getProductSpecRecord_SpecificationValue(productObject, servingSizeSpecId);
						//Spec Logic End

						//Product List HTML rendering Start
			            if(productObject.get('productStatus') == 'Released' && !String.isEmpty(calorieSpecification) && !String.isEmpty(totalServingsSpecification) && (returnNotNullAndNotEmptyString(productGroupMap.get(String.valueOf(productObject.get('SKU')))) == 'Shakes' || returnNotNullAndNotEmptyString(productGroupMap.get(String.valueOf(productObject.get('SKU')))) == 'Entrees') && (returnNotNullAndNotEmptyString(String.valueOf(productObject.get('SKU'))) != 'IMGC' || isAdmin == true)) {
							//Get Product Nutritions Map Start
							Map<String, String> productNutritionsMap = HMR_CMS_CCRZ_Utility.getProductNutritionsMap(productSpecsMainList, productObject);
							//Get Product Nutritions Map End
							String product_sfid = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfid')));
							String productSKU = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('SKU')));
							String productName = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('sfdcName')));
							String productDescription = returnNotNullAndNotEmptyString(String.valueOf(productObject.get('shortDesc')));
							String productIngredients = returnNotNullAndNotEmptyString(String.valueOf(productIngredientsMap.get(product_sfid)));
							String productGroup = returnNotNullAndNotEmptyString(productGroupMap.get(String.valueOf(productObject.get('SKU'))));

							//Append html block to appropriate html string variable
							if(productGroup == 'Shakes'){
								String productFlavor = productName.contains('Chocolate') ? 'chocolate' : 'vanilla';
								String containerType = productSKU.contains('120') ? 'can' : 'box';
								html += HMR_CMS_CCRZ_Utility.getProductDetails_IngredientsModalHTML(productIngredients, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
								html += HMR_CMS_CCRZ_Utility.getProductDetails_NutritionsModalHTML(productNutritionsMap, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
								html += HMR_CMS_CCRZ_Utility.getProductDetails_DescriptionModalHTML(productSKU, productThumbnailMediaURL, productName, productDescription, totalServingsSpecification,
																								containerType, calorieSpecification, protienSpecification, servingSizeSpecification);

                                if(!addMobileVanillaButton && productFlavor == 'vanilla') {
                                    html += '<div class="col-lg-12 hmr-form-control shake-selection-control hidden-lg hidden-md visible-sm visible-xs">'+
                                                '<input id="vanilla_m" type="radio" name="shakes_m" data-id="vanilla" class="blue">'+
                                                '<label class="form-control__label big-label heading" for="vanilla_m">'+
                                                    'Vanilla'+
                                                '</label>'+
                                                '<a class="hmr-allcaps pull-right hidden-lg hidden-md shake-details" data-flavor="vanilla" data-c="true">Details +</a>'+
                                            '</div>';
                                    addMobileVanillaButton = true;
                                }
                                if(!addMobileChocolateButton && productFlavor == 'chocolate') {
                                    html += '<div class="col-lg-12 hmr-form-control shake-selection-control hidden-lg hidden-md visible-sm visible-xs">'+
                                                '<input id="chocolate_m" type="radio" name="shakes_m" data-id="chocolate" class="blue">'+
                                                '<label class="form-control__label big-label heading" for="chocolate_m">'+
                                                    'Chocolate'+
                                                '</label>'+
                                                '<a class="hmr-allcaps pull-right hidden-lg hidden-md shake-details" data-flavor="chocolate" data-c="true">Details +</a>'+
                                            '</div>';
                                    addMobileChocolateButton = true;
                                }
                                if(!productSKU.contains('70')){
									html += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 prod-card-wrapper NLF hidden-sm hidden-xs NLF' + productFlavor + '" data-sku="' + productSKU + '" data-flavor="' + productFlavor + '">';
								}
								else{
									if(!isAdmin){
										html += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 prod-card-wrapper LF hide hidden-sm hidden-xs LF' + productFlavor + '" data-sku="' + productSKU + '" data-flavor="' + productFlavor + '">';
									}
									else{
										html += '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 prod-card-wrapper LF hidden-sm hidden-xs LF' + productFlavor + '" data-sku="' + productSKU + '" data-flavor="' + productFlavor + '">';
									}
								}
					            html += '<div class="product-card">';
					            if(isAdmin == false){
					            	html+= HMR_CMS_CCRZ_Utility.productDetailsCardHTML_NoeCommerce(productSKU, productMediaURL, productName, allergenHTML, calorieSpecification);
					            }
					            else{
					            	html+= HMR_CMS_CCRZ_Utility.productDetailsCardHTML_Kit(productSKU, productMediaURL, productName, allergenHTML, calorieSpecification, totalServingsSpecification, tabIndex, quantityMap, 'shakes', isAdmin);
					            }
						    	html += '</div>' + '</div>' + '</div>';
                                if(!addMobileVanillaChocolateButton && addMobileChocolateButton && addMobileVanillaButton) {
                                    html += '<div class="col-lg-12 hmr-form-control shake-selection-control hidden-lg hidden-md visible-sm visible-xs">'+
                                                '<input id="vanillchocolate_m" type="radio" name="shakes_m" data-id="vanillchocolate" class="blue">'+
                                                '<label class="form-control__label big-label heading" for="vanillchocolate_m">'+
                                                    'Vanilla &amp; Chocolate'+
                                                '</label>'+
                                            '</div>';
                                    addMobileVanillaChocolateButton = true;
                                }
							}
							else if(productGroup == 'Entrees'){
								entreeCardsHtml += HMR_CMS_CCRZ_Utility.getProductDetails_IngredientsModalHTML(productIngredients, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
								entreeCardsHtml += HMR_CMS_CCRZ_Utility.getProductDetails_NutritionsModalHTML(productNutritionsMap, productName, productSKU, productThumbnailMediaURL, totalServingsSpecification);
								entreeCardsHtml += HMR_CMS_CCRZ_Utility.getProductDetails_DescriptionModalHTML(productSKU, productThumbnailMediaURL, productName, productDescription, totalServingsSpecification,
																								'box', calorieSpecification, protienSpecification, '');
                                Integer qty = quantityMap.get(productSKU) != null ? Integer.valueOf(quantityMap.get(productSKU)) : 0;
                        		String disabledEnabledString = qty == 0 ? 'class="btn subtract-item-btn isMobile disabled" disabled="disabled"' : 'class="btn isMobile subtract-item-btn"';
                                String typeOfFood = (vegetarianSpecification == 'True') ? ' veg' : ' nonVeg';
                                String groupClass = 'entrees';
                                entreeCardsHtml += '<div id="prodCardWrapperRow_' + productSKU + '" class="row hidden-lg hidden-md product-row-mobile' + greyBackgroundCSSClass + typeOfFood + '">' +
                                                        '<div class="col-xs-6 col-sm-6">' +
                                                            '<div class="hmr-desc-text">' +
                                                                productName +
                                                            '</div>' +
                                                            '<a class="hmr-allcaps hidden-lg hidden-md prod-mobile-details" data-sku="' + productSKU + '">Details +</a>'+
                                                        '</div>' +
                                                        '<div class="col-xs-6 col-sm-6">' +
                                                            '<span class="item-counter-group text-center pull-right">' +
                                                                 '<div class="input-group input-group-sm">' +
                                                                     '<span class="input-group-btn">' +
                                                                        '<button ' + disabledEnabledString + ' data-limit="0" type="button" data-sku="' + productSKU + '" onclick="hmrFlow.removeItem(this, 1, \'entrees\');">-</button>'+
                                                                    '</span>' +
                                                                    '<input tabIndex="' + tabIndex + '" type="text" readonly="true" id="quantity_counter_mobile_' + productSKU + '" name="qty" value="' + qty + '" class="form-control input-text entry plus_minus_mobile entrees" maxlength="3" minlength="1" data-sku="' + productSKU + '"/>' +
                                                                    '<span class="input-group-btn">' +
                                                                        '<button class="btn add-item-btn isMobile" type="button" data-sku="' + productSKU + '" onclick="hmrFlow.addItem(this, 1, \'entrees\');">+</button>' +
                                                                    '</span>' +
                                                                '</div>' +
                                                             '</span>' +
                                                        '</div>' +
                                                   '</div>';
                                if(vegetarianSpecification == 'True'){
									entreeCardsHtml += '<div id="prodCardWrapper_' + productSKU + '" class="col-lg-4 col-md-6 col-sm-6 col-xs-12 veg prod-card-wrapper hidden-sm hidden-xs' + greyBackgroundCSSClass + '" data-sku="' + productSKU + '">';
								}
								else{
									entreeCardsHtml += '<div id="prodCardWrapper_' + productSKU + '" id="" class="col-lg-4 col-md-6 col-sm-6 col-xs-12 nonVeg prod-card-wrapper hidden-sm hidden-xs' + greyBackgroundCSSClass + '" data-sku="' + productSKU + '">';
								}

						        entreeCardsHtml += '<div class="product-card">';
								entreeCardsHtml += HMR_CMS_CCRZ_Utility.productDetailsCardHTML_Kit(productSKU, productMediaURL, productName, allergenHTML, calorieSpecification, totalServingsSpecification, tabIndex, quantityMap, 'entrees', isAdmin);
								entreeCardsHtml += '</div>' + '</div>'+ '</div>';
							}
						}
					}
					//Product List HTML rendering End
					html += '</div>'+
		                    '</div>'+
		                	'</div>'+
		                	'<div class="col-lg-1 col-md-1">'+
		                	'</div>'+
		            		'</div>'+
		            		'<div class="row">'+
                            '<div class="col-lg-1 col-md-1 visible-lg visible-md"></div>'+
                            '<div class="col-lg-10 col-md-10 col-xs-12 col-sm-12 tostep2_wrapper">';
			                //'<div class="mobile-px-15">';
                    if(kitSKU.contains('SHAKE')){
                        html+= '<button data-loading-text="<i class=\'hmr-button-loading-inverse\'></i>" id="hmr-hss-proceed-cart" class="btn callout-btn hmr-btn-blue btn-block-opacity btn-height addToCart" disabled="disabled" onclick="' + addToCartFunction + '(\'' + 'shakes' + '\');">Proceed to Cart</button>';
                    }
                    else{
                    	//html+= '<button id="toStep2" class="btn-block-opacity btn-height disabled" onclick="$(document).scrollTop(0);toggle(\'' + 'hmr_kit_config_step2' + '\',\'' + 2 + '\',false);return false;" disabled="">Next: Step 2</button>';
                    	html+= '<button data-loading-text="<i class=\'hmr-button-loading-inverse\'></i>" id="toStep2" class="btn callout-btn hmr-btn-blue btn-block-opacity btn-height" onclick="hmrFlow.goToStep(\'' + 'hmr_kit_config_step2' + '\', false);" disabled="disabled">Next: Step 2</button>';
                    }
		        	html +=	'</div>'+
                            '<div class="col-lg-1 col-md-1 visible-lg visible-md"></div>'+
                            '</div>' + '</section>' + '</main>';
		    		if(!kitSKU.contains('SHAKE')){
		    			html += '<main id="hmr_kit_config_step2" class="hmr_kit_config_step2 hide">'+
								    '<div id="step2Header" class="container-fluid kit-config-header fixed-progress">'+
								        '<div class="container">'+
								            '<div class="row">'+
								                '<div class="col-lg-8 col-md-9 col-sm-9 col-xs-10">'+
								                    '<div class="row">'+
								                        '<h2>'+
								                            'Step 2 of 3: <span>Entrees</span>'+
								                            // '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Keep our standard 42 entree assortment or customize your own below." '+
								                            //     'class="visible-md-inline visible-sm-inline visible-xs-inline">'+
								                            //     '<span class="glyphicon glyphicon-info-sign"></span>'+
								                            // '</a>'+
								                        '</h2>'+
								                        '<p>Keep our standard 42 entree assortment or customize your own below. <span class="text-15 italic">Calories do not include added vegetables.</span></p>'+
								                    '</div>'+
								                '</div>'+
								                '<div class="col-lg-4 col-md-3 col-sm-3 col-xs-2">'+
								                    '<div class="progress pull-right" id="strength-bar"></div>'+
								                '</div>'+
								            '</div>'+
								        '</div>'+
								    '</div>'+
								    '<div class="spacer-step2"></div>'+
								    '<div class="container kit-config-main kit-config-main-no-scroll">'+
								        '<div class="row">'+
								            '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
								                '<div class="hmr-form-control vegetarian">'+
								                    '<input id="vegFilter" type="checkbox" class="blue">'+
								                    '<label class="form-control__label hmr-allcaps blue" for="vegFilter">'+
								                        'Vegetarian'+
								                    '</label>'+
								                '</div>'+
								            '</div>'+
								        '</div>'+
								        '<div class="row prod-list">';
								        html+= entreeCardsHtml +
								        '</div>'+
								        '<div class="row kit_config_nav_row">'+
			                                '<div class="col-sm-6 col-xs-6">'+
			                                    //'<button id="backToStep1" class="btn-block-opacity opacity-none margin-top-50 btn-height" onclick="$(document).scrollTop(0);toggle(\'' + 'hmr_kit_config_step1' + '\',\'' + 1 + '\',false);return false;">Back: Step 1</button>'+
			                                    '<button id="backToStep1" class="btn callout-btn hmr-btn-blue btn-block-opacity opacity-none margin-top-50 btn-height" onclick="hmrFlow.goToStep(\'' + 'hmr_kit_config_step1' + '\', false);">Back: Step 1</button>'+
			                                '</div>'+
			                                '<div class="col-sm-6 col-xs-6">'+
			                                    //'<button id="toStep3" class="btn-block-opacity margin-top-50 btn-height opacity-none" onclick="$(document).scrollTop(0);toggle(\'' + 'hmr_kit_config_step3' + '\',\'' + 2 + '\',false);return false;">Next: Step 3</button>'+
			                                	'<button id="toStep3" class="btn callout-btn hmr-btn-blue btn-block-opacity margin-top-50 btn-height opacity-none" onclick="hmrFlow.goToStep(\'' + 'hmr_kit_config_step3' + '\', false);">Next: Step 3</button>'+
			                                '</div>'+
			                            '</div>'+
								    '</div>'+
								'</main>'+
        						'<main id="hmr_kit_config_step3" class="hmr_kit_config_step3 hide">' +
    							    '<div class="container step_3">' +
    							        '<div class="row header-row">' +
    							            '<div class="col-lg-12 no-left-padding">' +
    							                '<h2>' +
    							                    'Step 3 of 3: <span>Coaching</span>' +
    							                '</h2>' +
    							                '<h6>With your program enrollment you have the option to join free weekly 50-minute group phone coaching sessions for accountability and personalized advice.</h6>' +
    							            '</div>' +
    							        '</div>' +
    							        '<div class="row more-details-row">' +
    							            '<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 col-md-push-7">' +
    							                '<img src="https://www.myhmrprogram.com/ContentMedia/CoachValueProp/CoachingData.png" />' +
    							                '<div class="row">' +
    							                    '<p class="text-11b">' +
    							                        '*Average weight loss for the Healthy Solutions at Home program with phone coaching is 23 lbs. at 12 weeks.' +
    							                        '<a href="http://www.nature.com/ijo/journal/v31/n8/full/0803568a.html" target="_blank">Donnelly JE et al. Int J Obes 2007;31:1270-1276.</a>' +
    							                        'Average weight loss without coaching is 13 lbs. at 12 weeks.' +
    							                        '<a href="http://www.obesityresearchclinicalpractice.com/article/S1871-403X(09)00033-7/abstract" target="_blank">Obes Clin Pract 2009:3:149-157</a>' +
    							                    '</p>' +
    							                '</div>' +
    							            '</div>' +
    							            '<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 no-left-padding col-md-pull-5">' +
    							                '<div id="yesCoaching" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coaching_card yes coaching-selected" onclick="hmrFlow.changeCoach(\'' + '#yesCoaching' + '\');">' +
    							                    '<h2>' +
    							                        'Yes' +
    							                        '<div class="hmr-desc-text col-lg-12">I want FREE Coaching</div>' +
    							                    '</h2>' +
    							                '</div>' +
    							                '<div id="noCoaching" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coaching_card no" onclick="hmrFlow.changeCoach(\'' + '#noCoaching' + '\');">' +
    							                    '<h2>' +
    							                        'No' +
    							                        '<div class="hmr-desc-text col-lg-12">I want to diet on my own</div>' +
    							                    '</h2>' +
    							                '</div>' +
    							                '<div class="col-lg-5 no-padding col-md-6 col-sm-12 col-xs-12">' +
    							                    //'<a href="#" id="backToStep2" onclick="goToStep(\'' + 'hmr_kit_config_step2' + '\', false);" class="btn callout-btn hmr-btn-blue top">Back to Step 2</a>' +
    							                    '<button id="backToStep2" class="btn callout-btn hmr-btn-blue top" onclick="hmrFlow.goToStep(\'' + 'hmr_kit_config_step2' + '\', false);">Back: Step 2</button>'+
    							                '</div>' +
    							                '<div class="col-lg-5 no no-padding col-md-6 col-sm-12 col-xs-12">' +
    							                    //'<a href="#" id="hmr-main-proceed-cart" onclick="' + addToCartFunction + '(\'' + 'solutions' + '\');" class="btn callout-btn hmr-btn-blue top">Proceed to Cart</a>' +
    							                    '<button data-loading-text="<i class=\'hmr-button-loading-inverse\'></i>" class="btn callout-btn hmr-btn-blue top" id="hmr-main-proceed-cart" onclick="' + addToCartFunction + '(\'' + 'solutions' + '\');">Proceed to Cart</button>'+
    							                '</div>' +
    							            '</div>' +
    							        '</div>' +
    							    '</div>' +
    							'</main>';
				        	}
		        	html+= '<input type="hidden" id="current_kitSKU" value="' + kitSKU + '"/>';
		        	html+= '<input type="hidden" id="current_shakeFlavor" value=""/>';
		        	if(kitSKU.contains('SHAKE')){
		        		html+= '<input type="hidden" id="current_phaseType" value="shakes"/>';
		        	}
		        	else{
		        		html+= '<input type="hidden" id="current_phaseType" value="solutions"/>';
		        	}
		        	html+= '<input type="hidden" id="current_kitName" value="' + kitName + '"/>';
				}
			}
		}
		catch(Exception ex) {
			html += 'Something went wrong in Kit Config' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
		}
		return html;
	}
}