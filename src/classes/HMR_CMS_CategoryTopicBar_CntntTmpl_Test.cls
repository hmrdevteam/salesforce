/**
* Test Class Coverage of the HMR_CMS_CategoryTopicBar_ContentTemplate
*
* @Date: 07/31/2017
* @Author Utkarsh Goswami (Mindtree) / Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/17/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_CategoryTopicBar_CntntTmpl_Test {
    @isTest 
    private static void testGetHtml(){
        HMR_CMS_CategoryTopicBar_ContentTemplate controller = new HMR_CMS_CategoryTopicBar_ContentTemplate(); 

        Test.startTest();

        controller.testAttributes.put('FilterCategory', 'DataCategory');
    	controller.testAttributes.put('SelectedCategory', 'None');	
    	String htmlResult = controller.getHTML();     

        Test.stopTest();        
        
        System.assertEquals('DataCategory', controller.FilterCategory);
        System.assertEquals('None', controller.SelectedCategory);
        System.assert(!String.isBlank(htmlResult));
    }       
    
    @isTest
    private static void testPropertyWithDefault(){
    	HMR_CMS_CategoryTopicBar_ContentTemplate controller = new HMR_CMS_CategoryTopicBar_ContentTemplate();

    	Test.startTest();
		
        //Invalid Property
        String result = controller.getPropertyWithDefault('RecipeCategory', 'EatingWell');

    	Test.stopTest();

    	System.assertEquals('EatingWell', result);
    }

   	@isTest
    private static void testContentControllerConstructor(){
        HMR_CMS_CategoryTopicBar_ContentTemplate controller;
        
        Test.startTest();
        //This always fails as no way to get context but here for coverage
        try{
            controller = new HMR_CMS_CategoryTopicBar_ContentTemplate(null);
        }
        catch(Exception ex){}
        
        Test.stopTest();
        
        System.assert(controller == null);
    }
}