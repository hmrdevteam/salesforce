/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_Plans_ContentTemplate
*/

@isTest
private class HMR_CMS_Plans_Content_Test{
  
  @isTest 
   static void testMethodForHealthySolutions() {
   
    Test.startTest();
      
    HMR_CMS_Plans_ContentTemplate plansContentTmpl = new HMR_CMS_Plans_ContentTemplate();
    String propertyValue = plansContentTmpl.getPropertyWithDefault('planName','Healthy Solutions');
    String planName = plansContentTmpl.planName;
    String planImage1 = plansContentTmpl.planImage1;
    String planImage2 = plansContentTmpl.planImage2;
    String planImage3 = plansContentTmpl.planImage3;
    String planImage4 = plansContentTmpl.planImage4;
    String planImage5 = plansContentTmpl.planImage5;
    String planImage6 = plansContentTmpl.planImage6;
    
    
    plansContentTmpl.testAttributes = new Map<String, String> {'planName' => 'Healthy Solutions'};   
    
    Program__c progObj = new Program__c(Name='TestProg',hmr_Program_Type__c = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
    insert progObj;
    
    List<Program__c> progList = new List<Program__c>();
    progList.add(progObj);
    
    plansContentTmpl.pList = progList;
    plansContentTmpl.isGuestUser = TRUE;
    plansContentTmpl.isShakeOption = TRUE;
    plansContentTmpl.disctPrice = 12;
    plansContentTmpl.stdPrice = 12;
    plansContentTmpl.sku = 'TESt1';
    plansContentTmpl.numberOfEntress = '20'; 
    plansContentTmpl.numberOfCerealServings = '20'; 
    plansContentTmpl.numberOfShakesServings = '12';
    plansContentTmpl.numberOfSampleEntress = '10';
    plansContentTmpl.debugString = 'Test';
    
    String returnHTML = plansContentTmpl.getHTML();
    
    System.assert(!String.isBlank(returnHTML));
    
  }
  
  @isTest 
   static void testMethodForHealthyShakes() {
   
    Test.startTest();
      
    HMR_CMS_Plans_ContentTemplate plansContentTmpl = new HMR_CMS_Plans_ContentTemplate();
    String propertyValue = plansContentTmpl.getPropertyWithDefault('planName','Healthy Shakes');
    String planName = plansContentTmpl.planName;
    String planImage1 = plansContentTmpl.planImage1;
    String planImage2 = plansContentTmpl.planImage2;
    String planImage3 = plansContentTmpl.planImage3;
    String planImage4 = plansContentTmpl.planImage4;
    String planImage5 = plansContentTmpl.planImage5;
    String planImage6 = plansContentTmpl.planImage6;
    
    
    plansContentTmpl.testAttributes = new Map<String, String> {'planName' => 'Healthy Shakes'};   
    
    Program__c progObj = new Program__c(Name='TestProg',hmr_Program_Type__c = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
    insert progObj;
    
    List<Program__c> progList = new List<Program__c>();
    progList.add(progObj);
    
    plansContentTmpl.pList = progList;
    plansContentTmpl.isGuestUser = TRUE;
    plansContentTmpl.isShakeOption = TRUE;
    plansContentTmpl.disctPrice = 12;
    plansContentTmpl.stdPrice = 12;
    plansContentTmpl.sku = 'TESt1';
    plansContentTmpl.numberOfEntress = '20'; 
    plansContentTmpl.numberOfCerealServings = '20'; 
    plansContentTmpl.numberOfShakesServings = '12';
    plansContentTmpl.numberOfSampleEntress = '10';
    plansContentTmpl.debugString = 'Test';
    
    String returnHTML = plansContentTmpl.getHTML();
    
    System.assert(!String.isBlank(returnHTML));
    
    Test.stopTest();
  }
}