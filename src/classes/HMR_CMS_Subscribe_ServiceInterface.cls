global class HMR_CMS_Subscribe_ServiceInterface implements cms.ServiceInterface {
    private static Pattern brPattern = Pattern.compile('<br( )?(/)?>');
    private static Pattern tagPattern = Pattern.compile('<.*?>');

    global HMR_CMS_Subscribe_ServiceInterface(){}

    global HMR_CMS_Subscribe_ServiceInterface(cms.CoreController controller){}
    /**
     *
     * @param params a map of parameters including at minimum a value for 'action'
     * @return a JSON-serialized response string
     */
    public String executeRequest(Map<String, String> params) {
     //   String action = params.get('action');
        map<String, Object> returnMe = new map<string, Object>();
        try {
            String action = params.get('action');
            if(action == 'createLead') {
                returnMe = createLeadRecord(params);
            }else{
                //invalid action
                returnMe.put('success',false);
                returnMe.put('message','Invalid Action');
            }
        } catch(Exception e) {
            // Unexpected error
            System.debug(e.getStackTraceString());
            String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
            returnMe.put('success',false);
            returnMe.put('message',JSON.serialize(message));
            //return '{"setdata":"","success":false,"message":' + JSON.serialize(message) + '}';
        }

        // No actions matched and no error occurred
        return JSON.serialize(returnMe);
    }

    public map<String, Object> createLeadRecord(Map<String, String> params){

        String fName = params.get('firstName');
        String lName = params.get('lastName');
        String email = params.get('email');
        String zipCode = params.get('zipcode');
        map<String, Object> returnMe = new map<string, Object>();
        Boolean authUser = false;

        String userId = UserInfo.getUserId();
        String userProfileId = UserInfo.getProfileId();
        Id authUserProfileId = [SELECT Id, Name FROM Profile WHERE Name = 'HMR Customer Community User'].Id;

        Contact c = new Contact();

        if(userProfileId == authUserProfileId){
            Id contactId = [SELECT Id, ContactId FROM User WHERE Id = :userId].ContactId;
            c = [SELECT Id, HMR_Email_Preference__c, MailingPostalCode, Patient_Type__c, HasOptedOutOfEmail FROM Contact WHERE Id =:contactId];
        }


        String returnJSON;
        if(authUser = true && c.HMR_Email_Preference__c == true){
            returnMe.put('success',false);
            returnMe.put('message','You have already subscribed to HMR newsletters');
        }
        else{
            // List<Account> accountList;
        if(!String.isBlank(fName) && !String.isBlank(lName) && !String.isBlank(email) && !String.isBlank(zipCode)){
            try{
                Set<Id> accountIdList = new Set<Id>();

                List<Zip_Code_Assignment__c> zipCodeList = new List<Zip_Code_Assignment__c>(
                    [SELECT Name, Account__c FROM Zip_Code_Assignment__c WHERE Name = :zipCode]);

                if(!zipCodeList.isEmpty()) {
                    for(Zip_Code_Assignment__c zc: zipCodeList) {
                        accountIdList.add(zc.Account__c);
                    }
                }


            /*
                accountList = new List<Account>([SELECT Id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                                   BillingCountry, BillingLatitude, BillingLongitude,HMR_Account_Name_1_Optional__c,Phone
                                   FROM Account WHERE Id IN :accountIdList]);


                if(accountList.isEmpty()){
                    returnMe.put('setdata','');
                    returnMe.put('success',true);
                    returnMe.put('message','set is empty');

                    //returnJSON = '{"setdata":"","success":true,"message":"set is empty"}';
                }
                else{
                    returnMe.put('setdata',accountList);
                    returnMe.put('success',true);
                    returnMe.put('message','set data');

                    //returnJSON = '{"setdata":'+ JSON.serialize(accountList) + ',"success":true,"message":"set data"}';
                }
            */

                 List<Lead> existingLeadEmails = new List<Lead>([SELECT Id, Name, Email FROM Lead WHERE Email = :email]);

                 List<Contact> existingContactEmails = new List<Contact>([SELECT Id, Name, Email FROM Contact WHERE Email = :email]);

                 if(existingLeadEmails.size() > 0 || existingContactEmails.size() > 0){

                    returnMe.put('success',false);
                    returnMe.put('message','This email address is already in use');
                 }
                 else{

                    String leadUrl =  Apexpages.currentPage().getUrl();

                    if(leadUrl.contains('Knowledge')){
                        leadUrl = 'Knowledge Page Form';
                    }
                    else if(leadUrl.contains('Recipe')){
                        leadUrl = 'Recipe Page Form';
                    }
                    else if(leadUrl.contains('Home')){
                        leadUrl = 'Home Page Form';
                    }
                    else{
                        leadUrl = 'Home Page Form';
                    }
                    //if caught in Zip Code, add Account information and Set Lead Type to Clinic
                    if(accountIdList.size() > 0) {
                        Account acct = [SELECT Id,Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
                                           BillingCountry, BillingLatitude, BillingLongitude,HMR_Account_Name_1_Optional__c,Phone
                                           FROM Account WHERE Id IN :accountIdList LIMIT 1];

                        Lead newLead = new Lead();

                        newLead.FirstName = fName;
                        newLead.LastName = lName;
                        newLead.Email = email;
                        newLead.PostalCode = zipCode;
                        newLead.Status = 'New';
                        newLead.hmr_Lead_Type__c = 'Clinic';
                        newLead.Company = acct.Name;
                        newLead.LeadSource = 'Website';
                        newLead.hmr_Lead_Source_Detail__c = leadUrl;
                        newLead.Account__c = acct.Id;

                        insert newLead;

                        returnMe.put('success',true);
                        returnMe.put('message','Lead Created');
                    } else {

                         Lead newLead = new Lead();

                         newLead.FirstName = fName;
                         newLead.LastName = lName;
                         newLead.Email = email;
                         newLead.PostalCode = zipCode;
                         newLead.Status = 'New';
                         newLead.hmr_Lead_Type__c = 'Consumer';
                         newLead.Company = fName + ' ' + lName;
                         newLead.LeadSource = 'Website';
                         newLead.hmr_Lead_Source_Detail__c = leadUrl;

                         insert newLead;

                         returnMe.put('success',true);
                         returnMe.put('message','Lead Created');
                     }
                 }

             }
            catch(Exception e){
                String message = e.getMessage() + ' ' + e.getTypeName() + ' ' + String.valueOf(e.getLineNumber());
                returnMe.put('success',false);
                returnMe.put('message',JSON.serialize(message));
            }
        }
        else{
            returnMe.put('success',false);
            returnMe.put('message','Please enter all the values');
        }
    }

    return returnMe;

    }

    public static Type getType() {
        return HMR_CMS_Subscribe_ServiceInterface.class;
    }
}