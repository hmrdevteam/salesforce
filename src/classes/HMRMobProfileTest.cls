/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/05/2018
 * Created By: Zach Engman
 * Last Modified Date: 01/05/2018
 * Last Modified By:
 * Description: Test Coverage for the HMRMobProfile REST Service
 * ****************************************************/
@isTest
private class HMRMobProfileTest {
    @testSetup
    private static void setupData(){
        User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;

        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;

        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;
    }

    @isTest
    private static void testGetProfile(){
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.params.put('communityUserId', userRecord.Id);
        request.httpMethod = 'GET';

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.stopTest();

        //Verify the profile was successfully returned
        System.assertEquals(userRecord.ContactId, profileResult.id);
    }

    @isTest 
    private static void testGetProfileWithNoId(){
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.httpMethod = 'GET';

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.stopTest();

        //Verify null is returned
        System.assert(profileResult == null);
    }

    @isTest
    private static void testGetProfileWithInvalidId(){
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.params.put('communityUserId', '000000000000000000');
        request.httpMethod = 'GET';

        RestContext.request = request;
        RestContext.response = response;

        Test.startTest();

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.stopTest();

        //Verify null is returned
        System.assert(profileResult == null);
    }

    @isTest
    private static void testCreateProfile(){
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        Program__c programRecord = [SELECT Id FROM Program__c WHERE Name = 'Healthy Solutions'];
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        Map<String, String> requestParamMap = new Map<String, String>{
            'communityUserId' => userRecord.Id
           ,'id' => userRecord.ContactId
           ,'birthdate' => '1990-12-12'
           ,'gender' => 'Male'
           ,'heightFeet' => '6'
           ,'heightInches' => '3'
           ,'initialWeight' => '180'
           ,'weightLossGoal' => '10'
           ,'motivation' => 'Feel Better'
           ,'supportNeeded' => 'Be More Active'
           ,'dietStartDate' => Date.today().Year() + '-' + Date.today().month() + '-' + Date.today().day()
           ,'membershipType' => 'Self-directed'
        };

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.params.putAll(requestParamMap);
        request.httpMethod = 'POST';

        RestContext.request = request;
        RestContext.response = response;

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.startTest();

        profileResult.birthDate = Date.newInstance(1990, 12, 12);
        HMRMobProfile.doPost(profileResult);

        Test.stopTest();

        //Verify the birthdate was updated
        Contact contactRecord = [SELECT hmr_Date_Of_Birth__c FROM Contact WHERE Id =: userRecord.ContactId];
        //Note: Until we fix/add logic for program__c this will be null
      //  System.assertEquals(null, contactRecord.hmr_Date_Of_Birth__c);
          System.assert(!String.isBlank(String.valueOf(contactRecord.hmr_Date_Of_Birth__c)));
    }

    @isTest
    private static void testUpdateProfile(){
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.params.put('communityUserId', userRecord.Id);
        request.httpMethod = 'PATCH';

        RestContext.request = request;
        RestContext.response = response;

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.startTest();

        profileResult.weightLossGoal = 20.0;
        String updateResult = HMRMobProfile.updateProfile(profileResult);

        Test.stopTest();

        //Verify success
        System.assertEquals(204, RestContext.response.statusCode);
    }

    @isTest
    private static void testUpdateUserProfileWithInvalidData(){
        User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/profile/';
        request.params.put('communityUserId', userRecord.Id);
        request.httpMethod = 'PATCH';

        RestContext.request = request;
        RestContext.response = response;

        HMRMobProfile.MobProfileDTO profileResult = HMRMobProfile.doGet();

        Test.startTest();

        profileResult.id = '000000000000000000'; //passing an invalid 18-char id
        String updateResult = HMRMobProfile.updateProfile(profileResult);

        Test.stopTest();

        //Verify error response
        System.assertEquals(404, RestContext.response.statusCode);
    }
}