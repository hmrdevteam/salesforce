/**
* Service class to get group data for coaching view
*
* @Date: 2018-02-27
* @Author: Utkarsh Goswami (Mindtree)
* @Modified: Zach Engman
* @JIRA:
*/


public without sharing class HMR_GroupData_CoachingView_Controller {

    public string activeSessionId {get; set;}
    public string currentWeekSessionId {get; set;}
    public string columnClass {get{
        return this.isHealthySolutions ? 'side-ways-hs' : 'side-ways-p2';
    }}
    public boolean isHealthySolutions {get{
        return this.groupData.programName == 'Healthy Solutions';
    }}
    public HMR_GroupData_Service.GroupData groupData {get; set;} 
    public List<SelectOption> sessionSelectOptionList {get; set;}
    public transient string handoutListJSON {get; set;}
    
    public HMR_GroupData_CoachingView_Controller(){
        HMR_GroupData_Service dataService = new HMR_GroupData_Service();
        String coachingSessionId = ApexPages.currentPage().getParameters().get('id');
        
        this.sessionSelectOptionList = new List<SelectOption>();

        this.groupData = dataService.getGroupDataForCoachingSession(coachingSessionId);
        //groupMemberList = groupData.groupMemberList;

        if(this.groupData.sessionList != null && this.groupData.sessionList.size() > 0){
            for(HMR_GroupData_Service.CoachingSession sessionRecord : this.groupData.sessionList){
                this.sessionSelectOptionList.add(new SelectOption(sessionRecord.id.left(15), sessionRecord.displayDate));
            }

            this.activeSessionId = coachingSessionId.left(15);
            this.currentWeekSessionid = groupData.sessionList[0].Id.left(15);
        }

        handleHandoutData();
    }

    public HMR_GroupData_CoachingView_Controller(ApexPages.StandardController controller){  
        this();
    }

    public PageReference sessionChanged(){
        HMR_GroupData_Service dataService = new HMR_GroupData_Service();

        this.groupData = dataService.getGroupDataForCoachingSession(this.activeSessionId);

        handleHandoutData();

        return null;
    }

    private void handleHandoutData(){
        //move to transient JSON to reduce viewstate
        this.handoutListJSON = JSON.serialize(this.groupData.groupHandoutList);

        for(HMR_GroupData_Service.GroupHandout handout : this.groupData.groupHandoutList){
            handout.body = null;
        }
    }

    public PageReference save(){
        List<Session_Attendee__c> updateList = new List<Session_Attendee__c>();

        for(HMR_GroupData_Service.GroupMember groupMember : groupData.groupMemberList){
            if(!String.isBlank(groupMember.sessionAttendeeId)){
                if(groupMember.attendedSession){
                    updateList.add(new Session_Attendee__c(Id = groupMember.sessionAttendeeId,
                                                           hmr_Attendance__c = 'Attended',
                                                           Coach_Note__c = groupMember.currentWeekNote));
                }
                else if(groupMember.currentAttendanceValue != 'Make-up Scheduled'){
                    updateList.add(new Session_Attendee__c(Id = groupMember.sessionAttendeeId,
                                                           hmr_Attendance__c = 'No Show',
                                                           Coach_Note__c = groupMember.currentWeekNote));
                }
                else if(groupMember.currentWeekNote != groupMember.originalCurrentWeekNote){
                    updateList.add(new Session_Attendee__c(Id = groupMember.sessionAttendeeId,
                                                           Coach_Note__c = groupMember.currentWeekNote));
                }
            }

            if(!String.isBlank(groupMember.nextWeekSessionAttendeeId)){
                if(groupMember.nextWeekNote != groupMember.originalNextWeekNote){
                    updateList.add(new Session_Attendee__c(Id = groupMember.nextWeekSessionAttendeeId,
                                                           Coach_Note__c = groupMember.nextWeekNote));
                }
            }
        }

        if(updateList.size() > 0){
            update updateList;
        }

        return null;
    }
}