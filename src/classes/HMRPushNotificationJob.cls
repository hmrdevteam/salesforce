/**
 * Batchable class to send push notifications to HMR mobile application
 * There are four types of jobs, each type queries for different records that meet the selection criteria - see user stories
 * Notifications are stored in Push_Notification__mdt custom metadata
 * @JIRA: 5426, 5427, 4843, 4842
 * @Date: 2018-02-14
 * @Author: Javier Arroyo
 * @Modified:
*/
public class HMRPushNotificationJob implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    private HMRBaseReminderBuilder builder;
    public Integer notificationCount {get; private set;}

    /**
     * factory method to create the object that is responsible for getting the list of users that need
     * to receive a push notification.  The builder object is based on the HMRNotificationType
     */
    public HMRPushNotificationJob(HMRNotificationType nType) {
        notificationCount = 0;
        if ( nType == HMRNotificationType.DAY_BEFORE_START_DATE_REMIDER ) {
            builder = new HMRDayBeforeStartDateReminder();
        }
        else if (nType == HMRNotificationType.WEIGHIN_REMINDER) {
            builder = new HMRWeighinReminder();
        }
        else if (nType == HMRNotificationType.INACTIVE_REMINDER) {
            builder = new HMRInactiveReminder();
        }
        else if ( nType == HMRNotificationType.FIRST_DAY_OF_DIET_REMINDER ) {
            builder = new HMRFirstDayOfDietReminder();
        }
        else if ( nType == HMRNotificationType.TEST_REMINDER ) {
            builder = new HMRTestReminder();
        }
    }

    public List<Contact> start(Database.BatchableContext bc) {
    	return builder.getContacts();
    }

    /**
     * execute the job
   	 */
    public void execute(Database.BatchableContext BC, List<Contact> contactList) {
        List<String> tokenList = new List<String>();
        System.debug('===> number of contacts:' + contactList.size());
        for(Contact c: contactList) {
            if ( c.hmr_Diet_Tracker_Token__c != null && c.hmr_Diet_Tracker_Token__c != '') {
                // extract the tokens from the contact.hmr_Diet_Tracker_Token__c - tokens are stored in JSON format
                List<HMRDeviceTokenDTO> tokens = (List<HMRDeviceTokenDTO>)JSON.deserialize(c.hmr_Diet_Tracker_Token__c, List<HMRDeviceTokenDTO>.class);
                for(HMRDeviceTokenDTO t: tokens) {
                    tokenList.add(t.token);
                    notificationCount++;
                }
            }
        }
        if (tokenList.size() > 0) {
            System.debug('===> json:' + builder.getAndroidNoticationToJson(tokenList));
            sendNotificationToFCM( builder.getAndroidNoticationToJson(tokenList) );
        }
        System.debug('===> total number of notifications:' + notificationCount);
    }

    public void finish(Database.BatchableContext BC) {
    }

	/**
	 * send a POST request to firebase endpoint to send the notifications
	 */
    private void sendNotificationToFCM(String payload) {
        Firebase__mdt fb = [select key__c, url__c from Firebase__mdt where developerName = 'fcm'];
        
        Http h = new Http();
    	HttpRequest req = new HttpRequest();
      	req.setEndpoint(fb.url__c);

      	req.setMethod('POST');
        req.setHeader('Authorization', fb.key__c);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(payload);
        req.setCompressed(true);
        try {
          HttpResponse res = h.send(req);
          System.debug('===> sendNotificationToFCM status: ' + res.getStatus());
        }
        catch(System.CalloutException e) {
            System.debug('===> error in push notification callout:' + e.getMessage());
        }
    }

    /**
     * base reminder class - implements HMRReminderBuilder
     * uses strategy pattern to build the json representation of the push notification
     */
    abstract class HMRBaseReminderBuilder {
        private HMRAndroidNotification androidNotification;

        /**
         * get the list of users that need to receive this notification
         */
        public virtual List<Contact> getContacts() { return null; }

        /**
         * get the notification body for android
         */
        public String getAndroidNoticationToJson(List<String> to) {
            // lazy initialization - we only need on instance of this notification
            if (androidNotification == null) {
                androidNotification = this.getAndroidNotification();
            }
            androidNotification.registration_ids  = to;
            return JSON.serialize(androidNotification, true);
        }

        public abstract HMRAndroidNotification getAndroidNotification();

        /**
         * utility method to get a list of users
         * based on their diet start date
         */
        public List<Contact> getContactsByDateStartDate(Date dietStartDate) {
            return [select c.hmr_Diet_Tracker_Token__c, c.Id, c.Email from Contact c
                    where Id in
                      (select hmr_Contact__c from Program_Membership__c
                        WHERE hmr_Status__c = 'Active' and
                            hmr_Diet_Start_Date__c = :dietStartDate
                    )];
        }

        public Integer getRandomNumber(Integer size){
            Double d = math.random() * size;
            return d.intValue();
        }
    }
    
    /**
     * this class is only to test the process of sending reminders - it would not be used by any scheduled jobs
     */
    class HMRTestReminder extends HMRBaseReminderBuilder {
        public override List<Contact> getContacts() {
                        
            return[select c.hmr_Diet_Tracker_Token__c, c.Id, c.Email from Contact c
                    		where c.LastName = 'Pushnotification' ];                        
        }

        public override HMRAndroidNotification getAndroidNotification() {
            Push_Notification__mdt pnRecord = [select Type__c, title__c, body__c, priority__c from Push_Notification__mdt
        where developerName = 'Day_Before_Start_Date_Reminder' limit 1];

            return new HMRAndroidNotification('Test Notification: sent to all contacts whose last name = Pushnotification', 'Day Before Start Date Reminder', 'Day Before Start Date Reminder', true, pnRecord.priority__c);
        }
    }    

    /**
     * jira: 5426 - T-1 be reminded on the day prior to my diet start date...
     * Only 1 version of the notification
   * Notification is sent at 2pm EST
     */
    class HMRDayBeforeStartDateReminder extends HMRBaseReminderBuilder {
        public override List<Contact> getContacts() {
            Date tomorrowsDate = Date.today() + 1;
            
            return[select c.hmr_Diet_Tracker_Token__c, c.Id, c.Email from Contact c
                    		where Id in
                      			(select hmr_Contact__c from Program_Membership__c
                        		WHERE 
                                	hmr_Status__c = 'Active' and
                                 	Program__r.RemoteProgram__c = true and
                            		hmr_Diet_Start_Date__c = :tomorrowsDate)];                        
        }

        public override HMRAndroidNotification getAndroidNotification() {
            Push_Notification__mdt pnRecord = [select Type__c, title__c, body__c, priority__c from Push_Notification__mdt
        		where developerName = 'Day_Before_Start_Date_Reminder' limit 1];

            return new HMRAndroidNotification(pnRecord.body__c, pnRecord.Type__c, pnRecord.title__c, true, pnRecord.priority__c);
        }
    }

    /**
     * Jira - 5427: Weigh-in Push Notification
     * As a user I want to be prompted to enter the app on the 1st day of everyone's diet week to log weight. 
     * OR the first 'Day 1' after users start date for Healthy Solutions At Home Auto Delivery.
     * All clients with current program membership START DATE =NULL OR START DATE < Today 
     */
    class HMRWeighinReminder extends HMRBaseReminderBuilder {

        public override List<Contact> getContacts() {
            Date today = Date.today();
            return[select c.hmr_Diet_Tracker_Token__c, c.Id, c.Email from Contact c
                    		where Id in
                      			(select hmr_Contact__c 
                                 from Program_Membership__c
                                 where
                        			hmr_Status__c = 'Active' and
                                 	Program__r.RemoteProgram__c = true and
                                 	Program__r.hmr_Program_Type__c = 'Healthy Solutions' and
                            		(hmr_Diet_Start_Date__c = null or hmr_Diet_Start_Date__c < :today))];             
        }

        /**
         * return 1 of the 8 notifications randomly
         */
        public override HMRAndroidNotification getAndroidNotification() {
			List<Push_Notification__mdt> notificationList =
                    [select title__c, body__c, priority__c, type__c from Push_Notification__mdt
                      where type__c = 'Weigh In Reminder'
                      order by title__c asc];
            System.debug('---> List size:' + notificationList.size());
            Integer randomNum = getRandomNumber(8) - 1; 
            System.debug('---> random number: ' + randomNum);
            
            Push_Notification__mdt notification = notificationList.get( randomNum );
            return new HMRAndroidNotification(notification.body__c, notification.type__c, notification.title__c, true, notification.priority__c);
        }
    }

    /**
     * jira: 4843 - notification to open the app and track activity if I have been inactive
     * Delivery Timing: Evening of first day not tracking. (8PM EST) (last modified date is 8pm EST - 30 hours)
     * There will be 9 different messages which appear randomly
     * Can't write a test class for this method because soql uses SystemModStamp
     */
    class HMRInactiveReminder extends HMRBaseReminderBuilder {
        Datetime rightNow = Datetime.now();
        Datetime nowMinus24 = rightNow.addHours(-24);
        Datetime nowMinus48 = rightNow.addHours(-48);
        Datetime nowMinus72 = rightNow.addHours(-72);
        
        // get a list of all the contacts with YES activity 3 days ago
        public Set<Id> getIdsWithActivityRange(Datetime startDate, Datetime endDate) {
			Set<Id> ids = new Map<Id, Contact>([
                  select c.Id from Contact c
            		where Id in
            			(Select contact__c from ActivityLog__c
                      	where SystemModStamp > :startDate and SystemModStamp  < :endDate)
                  ]).keySet();
            return ids;
        }
        
        public Set<Id> getIdsWithActivity3DaysAgo() {
            return getIdsWithActivityRange(nowMinus72, nowMinus48);
        } 
        
        public Set<Id> getIdsWithActivity2DaysAgo() {
            return getIdsWithActivityRange(nowMinus48, nowMinus24);
        }         
        
        // get a list of all the contacts with no activity in the last X number of hours
        public Set<Id> getIdsNoActivityFor(Datetime hrs) {
			Set<Id> ids = new Map<Id, Contact>([
                  select c.Id from Contact c
            		where  c.ProfileBuildProcessComplete__c = true and
                		Id not in
            			(Select contact__c from ActivityLog__c
                      	where SystemModStamp > :hrs and SystemModStamp  < :rightNow)
                  ]).keySet();
            return ids;
        }
        
        public override List<Contact> getContacts() {
            //System.debug('---> now   :' + rightNow);
            //System.debug('---> now zone:' + rightNow.format());
            //System.debug('---> result:' + nowMinus24);
            Set<Id> mapAllIds = new Set<Id>();

            Set<Id> noActIds = getIdsNoActivityFor(nowMinus48);
            mapAllIds.addAll(noActids);
            
            
            //retain all the elements that had activity on the third day
            mapAllIds.retainAll(getIdsWithActivity3daysAgo());

            Set<Id> noActFor24hrs = getIdsNoActivityFor(nowMinus24);
            //retain all the elements that had activity on the 2nd day
            noActFor24hrs.retainAll(getIdsWithActivity2DaysAgo());
            
            mapAllIds.addAll(noActFor24hrs);
            
            return [select c.hmr_Diet_Tracker_Token__c, c.Id, c.Email 
                                   from Contact c
                                   where c.Id in : noActids 
                                  ];
        }

        /**
         * return 1 of the 9 notifications randomly
         */
        public override HMRAndroidNotification getAndroidNotification() {
            List<Push_Notification__mdt> notificationList =
                [select title__c, body__c, priority__c, type__c from Push_Notification__mdt
          where type__c = 'Inactive Reminder'
          order by title__c asc];
            Push_Notification__mdt notification = notificationList.get( getRandomNumber(9) - 1 );
            return new HMRAndroidNotification(notification.body__c, notification.type__c, notification.title__c, true, notification.priority__c);
        }
    }

    /**
     * jira: 4842 - As a user I want to be prompted to enter the app on the first day of my diet
     * Only HSAH P1 Auto Delivery receives this notification
     */
    class HMRFirstDayOfDietReminder extends HMRBaseReminderBuilder {

        public override List<Contact> getContacts() {
            return getContactsByDateStartDate(Date.today());
        }

         public override HMRAndroidNotification getAndroidNotification() {
            Push_Notification__mdt pnRecord = [select Type__c, title__c, body__c, priority__c from Push_Notification__mdt
        		where developerName = 'First_Day_of_Diet' limit 1];

            return new HMRAndroidNotification(pnRecord.body__c, pnRecord.type__c, pnRecord.title__c, true, pnRecord.priority__c);
        }
    }

    /**
     * represents an Android notification
     */
    public class HMRAndroidNotification {
        public List<String> registration_ids {get; set;}
        public Notification notification {get; set;}
        public Data data {get; set;}
        public String priority {get; set;}        
        public HMRAndroidNotification(String body, String sType, String title, Boolean content_available, String priority) {
            notification = new Notification(body, title, content_available, priority, sType);
            this.priority = priority;
            this.data = new Data(sType);            
        }
    }
    class Notification {
        public String body {get; set;}
        public String title {get; set;}

        public Notification(String body, String title, Boolean content_available, String priority, String sType) {
            this.body = body;
            this.title = title;
        }
    }
    class Data {
        public String activity {get; set;}
        public Data(String activity) {
            this.activity = activity;
        }
    }
}