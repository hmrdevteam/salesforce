/*
Developer: Mustafa Ahmed (HMR)
Date: March 22nd 2017
Target Class: HMR_CMS_Home_HeroSection_ContentTemplate
*/

@isTest
public class HMR_CMS_Home_HeroSection_Test {
		@isTest static void test_general_method() {
		// Implement test code
		HMR_CMS_Home_HeroSection_ContentTemplate HomeHeroSectionTemplate = new HMR_CMS_Home_HeroSection_ContentTemplate();			
        		String propertyValue = HomeHeroSectionTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');		  
            
            	String TitleText = HomeHeroSectionTemplate.TitleText;
                String SubTitleText = HomeHeroSectionTemplate.SubTitleText;
                String BackgroundImage = HomeHeroSectionTemplate.BackgroundImage;
                String USNewsIconImage_First = HomeHeroSectionTemplate.USNewsIconImage_First;
                String USNewsIconImage_Second = HomeHeroSectionTemplate.USNewsIconImage_Second;
                String ButtonLabelText = HomeHeroSectionTemplate.ButtonLabelText;
                String OtherLinkLabelText = HomeHeroSectionTemplate.OtherLinkLabelText;
            
            	cms.Link ButtonLinkObj = HomeHeroSectionTemplate.ButtonLinkObj;
            	cms.Link OtherLinkLinkObj = HomeHeroSectionTemplate.OtherLinkLinkObj;
            
            	String renderHMTL = HomeHeroSectionTemplate.getHTML(); 
        		System.Assert(renderHMTL != null);         
	}
}