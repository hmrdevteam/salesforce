/**
* Apex Rest Resource for Weight Activity
* @Date: 2018-01-11
* @Author: Utkarsh Goswami (Magnet 360)
* @Modified: Zach Engman 2018-02-15 
* @JIRA:
*/
@RestResource(urlMapping='/mobile/activity/weightData/*')  
global with sharing class HMRMobWeightActivity {
    
    /**
     * insert the Activity sObject
     * @param MobWeightActivityDTO
     * @see MobWeightActivityDTO
     * @return MobWeightActivityDTO - the DTO containing the weight activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 500 Internal Server Error
     */
    @HttpPost
    global static void doPost(MobWeightActivityDTO weightActivityLog) {
        RestResponse response = RestContext.response;
        Map<String, Object> resultMap = new Map<String, Object>();

        try{
            ActivityLog__c activityLog = weightActivityLog.mapToActivityLog();
            //Check for existing record
            List<ActivityLog__c> activityLogList = [SELECT Id 
                                                    FROM ActivityLog__c 
                                                    WHERE Program_Membership__c =: activityLog.Program_Membership__c
                                                    AND DateDataEnteredFor__c =: activityLog.DateDataEnteredFor__c];
            if(activityLogList.size() == 0){
                insert activityLog;

                response.statusCode = 201;

                resultMap.put('success', true);
                resultMap.put('id', activityLog.Id);
                resultMap.put('weightActivityLog', new MobWeightActivityDTO(activityLog));
            }
            else{
                response.statusCode = 405;

                resultMap.put('success', false);
                resultMap.put('error', 'Activity record already created for date specified');
            }
        }
        catch(Exception ex){
            response.statusCode = 500;
            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * update the Activity object
     * @param MobWeightActivityDTO
     * @see MobWeightActivityDTO
     */
    @HttpPatch
    global static void updateWeightActivity(MobWeightActivityDTO weightActivityLog) {
        Map<String, Object> resultMap = new Map<String, Object>();
        RestResponse response = RestContext.response;

        try{
            ActivityLog__c activityLogRecord = weightActivityLog.mapToActivityLog();
            //Get the latest version of the record, get all necessary fields to send back in case of mismatch
            List<ActivityLog__c> serverVersionList = [SELECT Contact__c
                                                            ,Program_Membership__c
                                                            ,DateDataEnteredFor__c
                                                            ,CurrentWeight__c
                                                            ,Weight_Version__c 
                                                      FROM ActivityLog__c 
                                                      WHERE Id =: activityLogRecord.Id];

            if(serverVersionList.size() > 0){
                if(activityLogRecord.Weight_Version__c == null || serverVersionList[0].Weight_Version__c == null || serverVersionList[0].Weight_Version__c == activityLogRecord.Weight_Version__c){
                    
                    activityLogRecord.Weight_Version__c = activityLogRecord.Weight_Version__c == null ? 0 : activityLogRecord.Weight_Version__c + 1;

                    update activityLogRecord;
                    
                    response.statuscode = 200;

                    resultMap.put('success', true);
                    resultMap.put('weightActivityLog', new MobWeightActivityDTO(activityLogRecord));
                }
                else{
                    response.statuscode = 405;

                    resultMap.put('success', false);
                    resultMap.put('error', 'record version mismatch');
                    resultMap.put('weightActivityLog', new MobWeightActivityDTO(serverVersionList[0]));
                }
            }
            else{
                response.statuscode = 405;

                resultMap.put('success', false);
                resultMap.put('error', 'record has been deleted on the server');
            } 
        }
        catch(Exception ex){
            response.statuscode = 404;

            resultMap.put('success', false);
            resultMap.put('error', ex.getMessage());
        }

        response.responseBody = Blob.valueOf(JSON.serialize(resultMap));
    }
    
    /**
     * method to get the weight activity record for the day
     * @param contactId - the related contact record id 
     * @param activityDate - the related date to log the activity for
     * @return MobWeightActivityDTO - the DTO containing the weight activity for the contact for the date (HTTP 200 OK)
     * @return HTTP/1.1 404 Not Found if error is encountered
     */
    @HttpGet
    global static void doGet() {
        RestResponse response = RestContext.response;
        Map<String, Object> returnMap = new Map<String, Object>();
        Map<String, String> paramMap = RestContext.request.params;

        if(paramMap.get('contactId') != null && paramMap.get('activityDate') != null){
            HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
            String contactId = paramMap.get('contactId');
            Date activityDate = Date.valueOf(paramMap.get('activityDate'));
            Program_Membership__c programMembershipRecord = programMembershipService.getProgramMembership(contactId, activityDate);
            MobWeightActivityDTO weightActivityDTO;   
            //verify a program membership record existed for the date
            if(programMembershipRecord != null){       
                List<ActivityLog__c> activityLogList = [SELECT Contact__c
                                                              ,Program_Membership__c
                                                              ,DateDataEnteredFor__c
                                                              ,CurrentWeight__c
                                                              ,Weight_Version__c
                                                         FROM ActivityLog__c 
                                                         WHERE Contact__c = :contactId
                                                          AND Program_Membership__c =: programMembershipRecord.Id
                                                          AND DateDataEnteredFor__c =: activityDate];

                RestContext.response.statuscode = 200;
                                                         
                if(activityLogList.size() > 0)
                    weightActivityDTO = getWeightActivity(activityLogList[0]); 
                else
                    weightActivityDTO = new MobWeightActivityDTO(contactId, programMembershipRecord.Id, activityDate);

                returnMap.put('success', true);
                returnMap.put('weightActivityLog', weightActivityDTO);
            }
            else{
                RestContext.response.statuscode = 404;

                returnMap.put('success', false);
                returnMap.put('error', Label.Invalid_Activity_Date);
            }
        }
        else{
            RestContext.response.statuscode = 404;

            returnMap.put('success', false);
            returnMap.put('error', 'Parameters: contactId and activityDate are required');
        }
        
        response.responseBody = Blob.valueOf(JSON.serialize(returnMap));  
    }

    global static MobWeightActivityDTO getWeightActivity(ActivityLog__c activityLog) {
         MobWeightActivityDTO mobileDTO = new MobWeightActivityDTO(activityLog);

         return mobileDTO; 
     }

     /**
     * This method will not be exposed to the UI.  If the user decides to delete a weight entry, the UI will call patch and null out
     * the weight field.  This is bc we need the rest of the activity data to remain in the record
     * method to delete the activity record - TODO - what if diet is recorded
     * @param id - the record id 
     * @return HTTP/1.1 200 success
     * @return HTTP/1.1 404 not found if error is encountered
     */
    @HttpDelete
    global static void deleteActivity() {
        Map<String, Object> returnMap = new Map<String, Object>();
        RestRequest request = RestContext.request;
        String recordId = request.params.get('id');
        
        /*
        try{
            delete new ActivityLog__c(Id = recordId);

            RestContext.response.statusCode = 200;

            returnMap.put('success', true);
        }
        catch(Exception ex){
            RestContext.response.statusCode = 404;

            returnMap.put('success', false);
            returnMap.put('error', ex.getMessage());
        }
    */
        String errorMsg = 'This method will not be exposed to the UI.  If the user decides to delete a weight entry, '
                  + 'the UI will call PATCH and NULL out the weight field.  This is bc we need the rest of the ' 
                  + 'activity data to remain in the record';
        returnMap.put('success', false);
        returnMap.put('error', errorMsg);

        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnMap)); 
    }     
    
    /**
     * global DTO class that represents a flat structure of the
     * weight activity related-fields from the ActivityLog__c sObject
     */
    global class MobWeightActivityDTO extends HMRActivityDTO{
        public Decimal currentWeight {get;set;}
        
        /**
         * constructors
         */
        public MobWeightActivityDTO(String contactId, String programMembershipId, Date activityDate){
            this.contactId = contactId;
            this.programMembershipId = programMembershipId;
            this.activityDate = activityDate;
        }
        public MobWeightActivityDTO(ActivityLog__c ac) {
            this.recordId = ac.Id;
            this.recordVersion = ac.Weight_Version__c == null ? 0 : (Integer)ac.Weight_Version__c;
            this.contactId = ac.Contact__c;
            this.programMembershipId = ac.Program_Membership__c;
            this.activityDate = ac.DateDataEnteredFor__c;
            this.currentWeight = ac.CurrentWeight__c; 
        }  

        public ActivityLog__c mapToActivityLog() {
            ActivityLog__c activityLogRecord = new ActivityLog__c(
                Id = this.recordId
               ,Weight_Version__c = this.recordVersion
               ,Contact__c = this.contactId
               ,Program_Membership__c = this.programMembershipId
               ,DateDataEnteredFor__c = this.activityDate
               ,CurrentWeight__c = this.currentWeight
            );
            
            return activityLogRecord;
        }
        
    }
        
}