/*
Developer: Utkarsh Goswami (Mindtree)
Date: March 26th, 2017
Target Class(ses): HMR_CMS_ZipCodeFinderServiceInterface
*/

@isTest
private class HMR_CMS_ZipCodeFinderServInterface_Test{
  
  // method for possitive test for zipcode fonder logic
  @isTest static void firstMethod() {
    // Implement test code
    HMR_CMS_ZipCodeFinderServiceInterface zipcodeFinderController = new HMR_CMS_ZipCodeFinderServiceInterface();
    
    Map<String, String> params = new Map<String, String>();
    params.put('action','getDetailsOfZipCode');
    params.put('zipcode','123456');
    
    Account testAcc = new Account(name = 'testAcc');
    insert testAcc;
    
    Zip_Code_Assignment__c zipCodeAssign = new Zip_Code_Assignment__c(Account__c = testAcc.id, Name = '123456');
    insert zipCodeAssign;
 
   
   
    String resp = zipcodeFinderController.executeRequest(params);
    
    delete testAcc;
    
    zipcodeFinderController.executeRequest(params);
    
    HMR_CMS_ZipCodeFinderServiceInterface.getType();
    
    System.debug('First -> ' + resp );
    
    System.assert(!String.isBlank(resp));
    
  }
  
 // method to cover else part by giving wrong 'action' value
  @isTest static void secondMethod() { 
      
        HMR_CMS_ZipCodeFinderServiceInterface zipcodeFinderController = new HMR_CMS_ZipCodeFinderServiceInterface();
         
        Map<String, String> params = new Map<String, String>();
        params.put('action','getDetails');
        params.put('zipcode','123456');    
        String resp = zipcodeFinderController.executeRequest(params);
               
        System.assertEquals('{"message":"Invalid Action","success":false,"setdata":""}', resp);
  }
  
  // method for testing else part i.e. if the zipcode is blank
  @isTest static void thirdMethod() { 
      
        HMR_CMS_ZipCodeFinderServiceInterface zipcodeFinderController = new HMR_CMS_ZipCodeFinderServiceInterface();
    
        Map<String, String> params = new Map<String, String>();
        params.put('action','getDetailsOfZipCode');
        params.put('zipcode','');    
        String resp = zipcodeFinderController.executeRequest(params);

        System.assertEquals('{"message":"Please Enter Zipcode","success":false,"setdata":""}', resp);
  }
  
  // method for negetive testing
   @isTest static void fourthMethod() { 
      
        HMR_CMS_ZipCodeFinderServiceInterface zipcodeFinderController = new HMR_CMS_ZipCodeFinderServiceInterface();
    
        Map<String, String> params; 
        
        String resp = zipcodeFinderController.executeRequest(params);  
        
        System.assert(resp.contains('false')); 
      
  }
}