/*
Developer: Mustafa Ahmed (HMR)
Date: March 22nd 2017
Target Class: HMR_CMS_ZipCodeFinder_ContentTemplate
*/

@isTest
public class HMR_CMS_ZipCodeFinder_Test {
		@isTest static void test_general_method() {
		// Implement test code
		HMR_CMS_ZipCodeFinder_ContentTemplate ZipCodeFinderTemplate = new HMR_CMS_ZipCodeFinder_ContentTemplate();			
                String propertyValue = ZipCodeFinderTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');		
                
            	String TitleText = ZipCodeFinderTemplate.TitleText; 
                String DescriptionText = ZipCodeFinderTemplate.DescriptionText; 
                String BackgroundImage = ZipCodeFinderTemplate.BackgroundImage; 
                String NoThanksLinkLabelText = ZipCodeFinderTemplate.NoThanksLinkLabelText; 
                cms.Link NoThanksLinkLinkObj = ZipCodeFinderTemplate.NoThanksLinkLinkObj;                
                
                String renderHMTL = ZipCodeFinderTemplate.getHTML(); 
        		System.Assert(renderHMTL != null); 
	}
}