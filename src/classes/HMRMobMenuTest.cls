/**
* Apex Test Coverage of the HMRMobMenu Class
* @Date: 2018-01-17
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2018-01-17 
* @JIRA:
*/
@isTest
private class HMRMobMenuTest {
	@testSetup
    private static void setupData(){
      User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;
    }

	@isTest
    private static void testGetMenuRecords(){
      User userRecord = [SELECT Id FROM User WHERE Alias = 'ccTest'];
      List<HMRMobMenu.MobMenuDTO> menuDTOList;

      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/menu/';
      request.httpMethod = 'GET';

      RestContext.request = request;
      RestContext.response = response;

      Test.startTest();

      System.runAs(userRecord){
      	 HMRMobMenu.doGet();
      }

      Test.stopTest();

      //Verify the menu records were successfully returned
      System.assertEquals(200, response.statuscode);
    }
}