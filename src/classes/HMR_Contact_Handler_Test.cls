/**
* Test Class Coverage of the HMR_Contact_Handler Class
*
* @Date: 05/23/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 05/23/2017
* @JIRA:
*/
@isTest
private class HMR_Contact_Handler_Test {
    @isTest
    private static void testFieldFormatsOnInsert(){
        Contact contactRecord = new Contact(FirstName = 'lowerfirst', LastName = 'lowerlast', MailingState = 'CA');

        Test.startTest();

        insert contactRecord;

        Test.stopTest();

        Contact contactResult = [SELECT FirstName, LastName FROM Contact WHERE Id =: contactRecord.Id];
        //Verify the capitalization was updated
        //System.assert(contactResult.FirstName.contains('Lowerfirst'));
        //System.assert(contactResult.LastName.contains('Lowerlast'));
    }

    @isTest
    private static void testFieldFormatsOnUpdate(){
        Contact contactRecord = new Contact(FirstName = 'lowerfirst', LastName = 'lowerlast', MailingState = 'CA');
        insert contactRecord;

        Contact contactRecordToUpdate = [SELECT FirstName, LastName FROM Contact WHERE Id =: contactRecord.Id];

        Test.startTest();

        contactRecordToUpdate.FirstName = 'lowerfirst';
        contactRecordToUpdate.LastName = 'lowerlast';
        update contactRecordToUpdate;

        Test.stopTest();

        Contact contactResult = [SELECT FirstName, LastName FROM Contact WHERE Id =: contactRecordToUpdate.Id];
        //Verify the capitalization was updated
        //System.assert(contactResult.FirstName.contains('Lowerfirst'));
        //System.assert(contactResult.LastName.contains('Lowerlast'));
    }
}