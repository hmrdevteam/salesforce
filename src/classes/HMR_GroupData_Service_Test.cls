/*****************************************************
 * Author: Zach Engman
 * Created Date: 03/07/2018
 * Created By: Zach Engman
 * Last Modified Date: 03/08/2018
 * Last Modified By:
 * Description: Test Coverage for the HMR_GroupData_Service Class
 * ****************************************************/
@isTest
private class HMR_GroupData_Service_Test {
	@testSetup
	private static void setupTestData(){
	    User userRecord = cc_dataFactory.testUser;
      Contact contactRecord = cc_dataFactory.testUser.Contact;

      Program__c programRecord = HMR_TestFactory.createProgram();
      insert programRecord;

      Program_Membership__c programMembershipRecord = HMR_TestFactory.createProgramMembership(programRecord.Id, contactRecord.Id);
      insert programMembershipRecord;

      //Create coach
      User coachUserRecord = HMR_TestFactory.createUser('HMR Standard User', true);
      insert coachUserRecord;

      Coach__c coachRecord = HMR_TestFactory.createCoach(coachUserRecord.Id);
      insert coachRecord;

      //Setup the group/class and sessions
      Class__c classRecord = HMR_TestFactory.createClass(programRecord.Id, coachRecord.Id);
      insert classRecord;

      Class_Member__c classMemberRecord = HMR_TestFactory.createClassMember(classRecord.Id, contactRecord.Id, coachRecord.Id, programMembershipRecord.Id);
      insert classMemberRecord;

      Coaching_Session__c coachingSessionRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      coachingSessionRecord.hmr_Class_Date__c = Date.today().addDays(-7);
      insert coachingSessionRecord;

      //Add future coaching session
      Coaching_Session__c coachingSessionFutureRecord = HMR_TestFactory.createCoachingSession(classRecord.Id, coachRecord.Id);
      insert coachingSessionFutureRecord;

      Session_Attendee__c sessionAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionRecord.Id, classMemberRecord.Id, contactRecord.Id);
      sessionAttendeeRecord.hmr_Attendance__c = 'Attended';
      insert sessionAttendeeRecord;

      //Add future attendee record
      Session_Attendee__c sessionFutureAttendeeRecord = HMR_TestFactory.createSessionAttendee(coachingSessionFutureRecord.Id, classMemberRecord.Id, contactRecord.Id);
      insert sessionFutureAttendeeRecord;

      //Create the handout records
      List<Handouts__c> handoutList = HMR_TestFactory.createHandoutList(programRecord.Id, 3);
      insert handoutList;
      
      List<Attachment> attachmentList = new List<Attachment>();
      for(Handouts__c handoutRecord : handoutList){
          Attachment attachmentRecord = new Attachment(Name='Unit Test Attachment',
                                                      body = Blob.valueOf('Unit Test Attachment Body'),
                                                      parentId = handoutRecord.Id);  

          attachmentList.add(attachmentRecord);
      }
      insert attachmentList;

      //Create activity log records
      List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
      
      for(integer i = 0; i < 7; i++){
        ActivityLog__c activityLogRecord = HMR_TestFactory.createActivityLog(programMembershipRecord.Id, contactRecord.Id);
        
        activityLogRecord.DateDataEnteredFor__c = Date.today().addDays(i * -1);
        activityLogRecord.Group_Member__c = classMemberRecord.Id;
        activityLogRecord.Session_Attendee__c = sessionAttendeeRecord.Id;

        activityLogList.add(activityLogRecord);
      }

      insert activityLogList;
	}

  @isTest
  private static void testGetGroupData(){
    HMR_GroupData_Service.GroupData groupDataResult;
    User userRecord = [SELECT ContactId FROM User WHERE Alias = 'ccTest'];

    Test.startTest();

    System.runAs(userRecord){
      HMR_GroupData_Service groupDataService = new HMR_GroupData_Service();

      groupDataResult = groupDataService.getGroupDataForUser(userRecord.Id);
      groupDataResult.groupMemberList.sort();
    }

    //Getter properties
    String coachName = groupDataResult.coachName;
    String weekStartDateFormatted = groupDataResult.weekStartDateFormatted;
    String weekEndDateFormatted = groupDataResult.weekEndDateFormatted;
    String displayName = groupDataResult.groupMemberList[0].displayName;
    integer metShakeMinimumPercent = groupDataResult.metricTotalData.metShakeMinimumPercent;
    integer metEntreeMinimumPercent = groupDataResult.metricTotalData.metEntreeMinimumPercent;
    integer metFruitAndVegetableMinimumPercent = groupDataResult.metricTotalData.metFruitAndVegetableMinimumPercent;
    integer metPhysicalActivityMinimumPercent = groupDataResult.metricTotalData.metPhysicalActivityMinimumPercent;
    integer metDaysInBoxPercent = groupDataResult.metricTotalData.metDaysInBoxPercent;
    integer met325Percent = groupDataResult.metricTotalData.met325Percent;
    integer metTIPercent = groupDataResult.metricTotalData.metTIPercent;
    integer metHMRFoodsPercent = groupDataResult.metricTotalData.metHMRFoodsPercent;

    Test.stopTest();

    System.assert(groupDataResult != null);
  }

  @isTest
  private static void testGetGroupDataForCoachingSession(){
    HMR_GroupData_Service.GroupData groupDataResult;
    User userRecord = [SELECT ContactId FROM User WHERE hmr_IsCoach__c =: true ORDER BY CreatedDate DESC LIMIT 1];
    Coaching_Session__c coachingSessionRecord = [SELECT Id FROM Coaching_Session__c ORDER BY hmr_Class_Date__c LIMIT 1];

    Test.startTest();

    System.runAs(userRecord){
      HMR_GroupData_Service groupDataService = new HMR_GroupData_Service();

      groupDataResult = groupDataService.getGroupDataForCoachingSession(coachingSessionRecord.Id);
      groupDataResult.groupMemberList.sort();
    }

    Test.stopTest();

    System.assert(groupDataResult != null);
  }

}