/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-05-2023
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@RestResource(urlMapping='/mobile/user/sendEmail/*')
global without sharing  class HMRSendEmail {
    @Httppost
    global static String sendEmailToUser(String toemail, String subject, String bodyText, String fileName, String fileBody) {
        EmailResponse response;
        try{
            OrgWideEmailAddress owa = [select id, Address, DisplayName from OrgWideEmailAddress limit 1];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new List<String>{toemail});
            mail.setSubject(subject);
            mail.setPlainTextBody(bodyText); 
            mail.setReplyTo(UserInfo.getUserEmail());
            mail.setOrgWideEmailAddressId(owa.ID);
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            if(fileName != null && fileBody != null){
                attachment.setFileName(fileName);
                attachment.setBody(EncodingUtil.base64Decode(fileBody));
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment });
            }
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            if (results[0].isSuccess()) {
                response = new EmailResponse(true, 'Successfully sent email!');
                return JSON.serialize(response);
            } else {
                response = new EmailResponse(false, 'Unable to send email. Please try again!');
                return JSON.serialize(response);
            }
        }catch(Exception ex){
            System.debug(LoggingLevel.ERROR, 'Email send operation failed: ' + ex.getMessage());
            response = new EmailResponse(false, 'Unable to send email. Please try again!');
            return JSON.serialize(response);
        }
    }
    public class EmailResponse {
        public Boolean isSuccess;
        public String message;
        
        public EmailResponse(Boolean isSuccess, String message) {
            this.isSuccess = isSuccess;
            this.message = message;
        }
    }
}