/**
* Test Class Coverage of the HMR_CC_ProgramMembership_Service Class
*
* @Date: 06/20/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 06/20/2017
* @JIRA:
*/

@isTest
private class HMR_CC_ProgramMembership_Service_Test{

  @isTest 
  private static void testCanUpdateDietStartDateForReorder() {
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(2);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Pending';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        orderList[1].hmr_Order_Type_c__c = 'P1 Reorder';
        orderList[1].hmr_Program_Membership__c = programMembershipRecord.Id;
        update orderList;
     
        Test.startTest();
        
        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Boolean canUpdate =  programMembershipService.canUpdateDietStartDate(contactRecord.Id, programMembershipRecord.Id);

        Test.stopTest();

        //Verify the order date can be updated
        System.assert(canUpdate);
    }  

  @isTest 
  private static void testCanUpdateDietStartDateForReorderInverse() {
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(2);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Pending';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        orderList[1].ccrz__OrderStatus__c = 'Completed';
        orderList[1].hmr_Order_Type_c__c = 'P1 Reorder';
        orderList[1].hmr_Program_Membership__c = programMembershipRecord.Id;
        update orderList;
     
        Test.startTest();
        
        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Boolean canUpdate =  programMembershipService.canUpdateDietStartDate(contactRecord.Id, programMembershipRecord.Id);

        Test.stopTest();

        //Verify the order date cannot be updated
        System.assert(!canUpdate);
    }  

    @isTest
    private static void testGetDietStartDateFromProgramMembership(){
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;

        Test.startTest();

        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Date dietStartDate = programMembershipService.getDietStartDate(contactRecord.Id);

        Test.stopTest();

        //Verify the Diet Start date was Returned
        System.assert(dietStartDate == Date.today());
    }

    @isTest
    private static void testGetDietStartDateFromPendingOrder(){
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(1);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id);
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Pending';
        update orderList;

        Test.startTest();

        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Date dietStartDate = programMembershipService.getDietStartDate(contactRecord.Id);

        Test.stopTest();

        //Verify the Diet Start date was Returned
        System.assert(dietStartDate == Date.today());
    }

    @isTest
    private static void testGetDietStartDateFromTemplateOrder(){
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(1);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id);
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Template';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        update orderList;

        Test.startTest();

        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        Date dietStartDate = programMembershipService.getDietStartDate(contactRecord.Id);

        Test.stopTest();

        //Verify the Diet Start date was Returned
        System.assert(dietStartDate == Date.today());
    }

    @isTest
    private static void testUpdateDietStartDateUsingPendingOrder(){
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(1);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id);
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Pending';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        update orderList;

        Test.startTest();

        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        programMembershipService.updateDietStartDate(contactRecord.Id, programMembershipRecord.Id, Date.today());

        Test.stopTest();

        ccrz__E_Order__c orderResult = [SELECT ccrz__OrderDate__c FROM ccrz__E_Order__c WHERE Id =: orderList[0].Id];
        //Verify the Order Date was Updated
        System.assert(orderResult.ccrz__OrderDate__c == Date.today());
    }

    @isTest
    private static void testUpdateDietStartDateUsingTemplateOrder(){
        cc_dataFactory.setupTestUser();
        Contact contactRecord = cc_dataFactory.testUser.Contact;
        List<ccrz__E_Order__c>  orderList = cc_dataFactory.createOrders(1);
        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;
        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id);
        insert programMembershipRecord;

        orderList[0].ccrz__OrderStatus__c = 'Template';
        orderList[0].hmr_Program_Membership__c = programMembershipRecord.Id;
        update orderList;

        Test.startTest();

        HMR_CC_ProgramMembership_Service programMembershipService = new HMR_CC_ProgramMembership_Service();
        programMembershipService.updateDietStartDate(contactRecord.Id, programMembershipRecord.Id, Date.today());

        Test.stopTest();

        ccrz__E_Order__c orderResult = [SELECT ccrz__OrderDate__c FROM ccrz__E_Order__c WHERE Id =: orderList[0].Id];
        //Verify the Order Date was Updated
        System.assert(orderResult.ccrz__OrderDate__c == Date.today());
    }

}