/*****************************************************
 * Author: Zach Engman
 * Created Date: 05/30/2017
 * Created By: Zach Engman
 * Last Modified Date: 01/22/2018
 * Last Modified By: N. Anderson - Re-worked logic for 2.1 release -- Updates to clone records, updates to filter only by OwnerId
 * Description: Handler for the ccrz__E_Order__c object trigger
 * ****************************************************/
public without sharing class HMR_CC_Order_Handler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isAfterInsertFlag = false;
    public static Boolean isAfterUpdateFlag = false;

    public HMR_CC_Order_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }

    public void onAfterInsert(List<ccrz__E_Order__c> orderList, Map<Id, ccrz__E_Order__c> orderMap){
        if(!isAfterInsertFlag)
            isAfterInsertFlag = true;

        handleAddressBookInserts(orderMap);
    }

    public void onAfterUpdate(List<ccrz__E_Order__c> oldOrderList, List<ccrz__E_Order__c> newOrderList, Map<Id, ccrz__E_Order__c> oldOrderMap, Map<Id, ccrz__E_Order__c> newOrderMap){
        if(!isAfterUpdateFlag)
            isAfterUpdateFlag = true;
    }

    //Prepare the address records for synchronization to address book entries
    private List<ccrz__E_AccountAddressBook__c> handleAddressBookInserts(Map<Id, ccrz__E_Order__c> orderMap){
    	Map<Id, Id> addressToOrderMap = new Map<Id, Id>();
    	List<ccrz__E_AccountAddressBook__c> addressBookList = new List<ccrz__E_AccountAddressBook__c>();

    	for(ccrz__E_Order__c order : orderMap.values()){
            //Exclude Anonymous Orders
            if(!order.ccrz__IsAnonymous__c){
            	if(!String.isBlank(order.ccrz__BillTo__c) && !addressToOrderMap.containsKey(order.ccrz__BillTo__c))
            		addressToOrderMap.put(order.ccrz__BillTo__c, order.Id);
            	if(!String.isBlank(order.ccrz__ShipTo__c) && !addressToOrderMap.containsKey(order.ccrz__ShipTo__c))
            		addressToOrderMap.put(order.ccrz__ShipTo__c, order.Id);
            }
        }

        if(addressToOrderMap.size() > 0)
        	addressBookList = synchronizeAddressBookRecordsWithNewOrderAddresses(orderMap, addressToOrderMap);

         return addressBookList;
     }

    //Associates the contact address books to the account record through the ccrz__E_AccountAddressBook__c custom object
    private List<ccrz__E_AccountAddressBook__c> synchronizeAddressBookRecordsWithNewOrderAddresses(Map<Id, ccrz__E_Order__c> orderMap, Map<Id, Id> addressToOrderMap){
        List<ccrz__E_AccountAddressBook__c> addressBookInsertList = new List<ccrz__E_AccountAddressBook__c>();
        //List<ccrz__E_AccountAddressBook__c> addressBookInsertList2 = new List<ccrz__E_AccountAddressBook__c>();
        Set<Id> handledAddressSet = new Set<Id>();
        Set<Id> ownerIdSet = new Set<Id>();
        ccrz__E_ContactAddr__c emptyAddress = new ccrz__E_ContactAddr__c();
        Id newBillingAddressId;
        Id newShippingAddressId;

        //Account Id to Address Type and Map of those Addresses of that Type
        Map<Id, Map<String, Map<String, ccrz__E_AccountAddressBook__c>>> existingAddressMap = new Map<Id, Map<String, Map<String, ccrz__E_AccountAddressBook__c>>>();


        //Get the set of accounts involved
        for(ccrz__E_Order__c orderRecord : orderMap.values()){
            if(!String.isBlank(orderRecord.OwnerId) && !ownerIdSet.contains(orderRecord.OwnerId))
                ownerIdSet.add(orderRecord.OwnerId);
        }

        for(ccrz__E_AccountAddressBook__c existingContactAddressRecord : [SELECT ccrz__Account__c, ccrz__AddressType__c, ccrz__E_ContactAddress__c, ccrz__E_ContactAddress__r.ccrz__FirstName__c, ccrz__E_ContactAddress__r.ccrz__LastName__c, ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c, ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c, ccrz__E_ContactAddress__r.ccrz__City__c, ccrz__E_ContactAddress__r.ccrz__PostalCode__c, OwnerId FROM ccrz__E_AccountAddressBook__c WHERE OwnerId IN: ownerIdSet]){
            if(existingContactAddressRecord.ccrz__E_ContactAddress__r != null && String.isBlank(existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c))
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c = '';

            //String addressTypeKey = String.format('{0}_{1}', new String[]{existingContactAddressRecord.ccrz__Account__c, existingContactAddressRecord.ccrz__AddressType__c});
            String addressRecordKey = String.format('{0}_{1}_{2}_{3}_{4}_{5}', new String[]{
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__FirstName__c,
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__LastName__c,
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__City__c,
                existingContactAddressRecord.ccrz__E_ContactAddress__r.ccrz__PostalCode__c});

            if(existingAddressMap.containsKey(existingContactAddressRecord.OwnerId)){
                Map<String, Map<String, ccrz__E_AccountAddressBook__c>> addressMap = existingAddressMap.get(existingContactAddressRecord.OwnerId);
                if(addressMap.containsKey(existingContactAddressRecord.ccrz__AddressType__c)){
                    Map<String, ccrz__E_AccountAddressBook__c> addressTypeMap = addressMap.get(existingContactAddressRecord.ccrz__AddressType__c);
                    if(!addressTypeMap.containsKey(addressRecordKey)){
                        addressTypeMap.put(addressRecordKey, existingContactAddressRecord);
                        addressMap.put(existingContactAddressRecord.ccrz__AddressType__c, addressTypeMap);
                        existingAddressMap.put(existingContactAddressRecord.OwnerId, addressMap);
                    }
                }
                else{
                    addressMap.put(existingContactAddressRecord.ccrz__AddressType__c, new Map<String, ccrz__E_AccountAddressBook__c>{addressRecordKey => existingContactAddressRecord});
                    existingAddressMap.put(existingContactAddressRecord.OwnerId, addressMap);
                }
            }
            else{
                Map<String, Map<String, ccrz__E_AccountAddressBook__c>> addressMap = new Map<String, Map<String, ccrz__E_AccountAddressBook__c>>();
                addressMap.put(existingContactAddressRecord.ccrz__AddressType__c, new Map<String, ccrz__E_AccountAddressBook__c>{addressRecordKey => existingContactAddressRecord});
                existingAddressMap.put(existingContactAddressRecord.OwnerId, addressMap);
            }
        }


        //Find list of addresses having address book entry to exclude from map
        for(ccrz__E_AccountAddressBook__c accountAddressBookRecord : [SELECT ccrz__E_ContactAddress__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__E_ContactAddress__c IN: addressToOrderMap.keySet()])
            addressToOrderMap.remove(accountAddressBookRecord.ccrz__E_ContactAddress__c);

        if(addressToOrderMap.size() > 0){
        	//Get the Address Records to Determine if they are the same
        	Map<Id, ccrz__E_ContactAddr__c> contactAddressMap = new Map<Id, ccrz__E_ContactAddr__c>([SELECT ccrz__FirstName__c,
                                                                                                            ccrz__LastName__c,
                                                                                                            ccrz__AddressFirstline__c,
                                                                                                            ccrz__AddressSecondline__c,
                                                                                                            ccrz__City__c,
                                                                                                            ccrz__State__c,
                                                                                                            ccrz__StateISOCode__c,
                                                                                                            ccrz__Country__c,
                                                                                                            ccrz__CountryISOCode__c,
                                                                                                            ccrz__PostalCode__c,
                                                                                                            OwnerId
                                                                                                            FROM ccrz__E_ContactAddr__c
                                                                                                            WHERE Id IN: addressToOrderMap.keySet()]);

	        for(Id addressId : addressToOrderMap.keySet()){
	        	if(!handledAddressSet.contains(addressId)){
		        	ccrz__E_Order__c orderRecord = orderMap.get(addressToOrderMap.get(addressId));
                    System.debug('!!');
                    System.debug('!! ' + orderRecord);

		        	if(orderRecord != null){
		        		ccrz__E_ContactAddr__c billingAddress = contactAddressMap.get(orderRecord.ccrz__BillTo__c);
		        		ccrz__E_ContactAddr__c shippingAddress = contactAddressMap.get(orderRecord.ccrz__ShipTo__c);
                        //Set up new CA objects to hold clones so we don't create address book records for Order BillTo and ShipTo records
                        ccrz__E_ContactAddr__c billingClone = new ccrz__E_ContactAddr__c();
                        ccrz__E_ContactAddr__c shippingClone = new ccrz__E_ContactAddr__c();

                        //Handle nulls by setting to blank
                        if(billingAddress != null && String.isBlank(billingAddress.ccrz__AddressSecondline__c))
                            billingAddress.ccrz__AddressSecondline__c = '';
                        if(shippingAddress != null && String.isBlank(shippingAddress.ccrz__AddressSecondline__c))
                            shippingAddress.ccrz__AddressSecondline__c = '';

		        		//Determine if both are the same address, if so, use same address id and add to handled to prevent duplicates
		        		if(billingAddress != null && shippingAddress != null &&
                           billingAddress.ccrz__FirstName__c == shippingAddress.ccrz__FirstName__c &&
                           billingAddress.ccrz__LastName__c == shippingAddress.ccrz__LastName__c &&
		        		   billingAddress.ccrz__AddressFirstline__c == shippingAddress.ccrz__AddressFirstline__c &&
                           billingAddress.ccrz__AddressSecondline__c == shippingAddress.ccrz__AddressSecondline__c &&
		        		   billingAddress.ccrz__City__c == shippingAddress.ccrz__City__c &&
                           billingAddress.ccrz__PostalCode__c == shippingAddress.ccrz__PostalCode__c){
		        			handledAddressSet.add(orderRecord.ccrz__BillTo__c);
		        			handledAddressSet.add(orderRecord.ccrz__ShipTo__c);
		        		}

		        		//Determine Address Type Used
                        //if the address matches the bill to and is not a duplicate, enter the logic to create a new address/address book record
		        		if(orderRecord.ccrz__BillTo__c == addressId && !isDuplicateAddress(existingAddressMap, billingAddress, orderRecord, 'Billing')){
                            //Check to see if the address exists as the other type
                            ccrz__E_ContactAddr__c otherTypeAddressId = doesExistAsOtherAddressType(existingAddressMap, billingAddress, orderRecord, 'Shipping');
                            //If the address does exist as the other type, set the id so we create a new address book, but not a new address
                            if(otherTypeAddressId != emptyAddress) {
                                newBillingAddressId = otherTypeAddressId.Id;
                            //otherwise, clone the address and set the id to the new cloned record so we create a new address book and a clean address
                            } else {
                                billingClone = billingAddress.clone(false,false);
                            }
                            if(billingClone != emptyAddress){
                                insert billingClone;
                                newBillingAddressId = billingClone.Id;
                            }

                            //set up the new address book record with the correct address record, add to list.
                            addressBookInsertList.add(new ccrz__E_AccountAddressBook__c(ccrz__Account__c = orderRecord.ccrz__Account__c,
									                                                    ccrz__AccountId__c = orderRecord.ccrz__Account__c,
									                                                    ccrz__AddressType__c = 'Billing',
									                                                    ccrz__E_ContactAddress__c = newBillingAddressId,
									                                                    ccrz__Default__c = true,
                                                                                        ccrz__Owner__c = orderRecord.OwnerId,
                                                                                        OwnerId = orderRecord.OwnerId));

		        		}

                        //if the address matches the ship to, or a handled address and is not a duplicate, enter the logic to create a new address/address book record
		        		if((orderRecord.ccrz__ShipTo__c == addressId || handledAddressSet.contains(orderRecord.ccrz__ShipTo__c)) && !isDuplicateAddress(existingAddressMap, shippingAddress, orderRecord, 'Shipping')){
                            //Check to see if the address exists as the other type
                            ccrz__E_ContactAddr__c otherTypeAddressId = doesExistAsOtherAddressType(existingAddressMap, shippingAddress, orderRecord, 'Billing');
                            //If the address does exist as the other type, set the id so we create a new address book, but not a new address
                            if(otherTypeAddressId != emptyAddress) {
                                newShippingAddressId = otherTypeAddressId.Id;
                            //if the address was handled in the billing address loop, set the id so we create a new address book but not a duplicate address
                            } else if (handledAddressSet.contains(orderRecord.ccrz__ShipTo__c)){
                                newShippingAddressId = newBillingAddressId;
                            //otherwise, clone the address and set the id to the new cloned record so we create a new address book and a clean address
                            } else {
                                shippingClone = shippingAddress.clone(false,false);
                            }
                            if(shippingClone != emptyAddress){
                                insert shippingClone;
                                newShippingAddressId = shippingClone.Id;

                            }

                            //set up the new address book record with the correct address record, add to list.
		        			addressBookInsertList.add(new ccrz__E_AccountAddressBook__c(ccrz__Account__c = orderRecord.ccrz__Account__c,
		                                                                        		ccrz__AccountId__c = orderRecord.ccrz__Account__c,
		                                                                        	    ccrz__AddressType__c = 'Shipping',
		                                                                        		ccrz__E_ContactAddress__c = newShippingAddressId,
		                                                                        		ccrz__Default__c = true,
                                                                                        ccrz__Owner__c = orderRecord.OwnerId,
                                                                                        OwnerId = orderRecord.OwnerId));

		        		}
		        	}
	        	}
	        }

	        if(addressBookInsertList.size() > 0)
	        	insert addressBookInsertList;
        }

        return addressBookInsertList;
    }

    //Checks for a duplicate matching address
    private boolean isDuplicateAddress(Map<Id, Map<String, Map<String, ccrz__E_AccountAddressBook__c>>> existingAddressMap, ccrz__E_ContactAddr__c addressRecord, ccrz__E_Order__c orderRecord, String addressType){
        Map<String, Map<String, ccrz__E_AccountAddressBook__c>> addressTypeMap = existingAddressMap.get(orderRecord.OwnerId);
        boolean isDuplicate = false;

        if(addressTypeMap != null){
            Map<String, ccrz__E_AccountAddressBook__c> addressMap = addressTypeMap.get(addressType);
            if(addressMap != null){
                String addressRecordKey = String.format('{0}_{1}_{2}_{3}_{4}_{5}', new String[]{addressRecord.ccrz__FirstName__c,  addressRecord.ccrz__LastName__c, addressRecord.ccrz__AddressFirstline__c, addressRecord.ccrz__AddressSecondline__c, addressRecord.ccrz__City__c, addressRecord.ccrz__PostalCode__c});
                isDuplicate = addressMap.containsKey(addressRecordKey);
            }
        }

        return isDuplicate;
    }

    //Checks of address already exists as billing and is now being added as shipping or vice versa
    private ccrz__E_ContactAddr__c doesExistAsOtherAddressType(Map<Id, Map<String, Map<String, ccrz__E_AccountAddressBook__c>>> existingAddressMap, ccrz__E_ContactAddr__c addressRecord, ccrz__E_Order__c orderRecord, String addressType){
        Map<String, Map<String, ccrz__E_AccountAddressBook__c>> addressTypeMap = existingAddressMap.get(orderRecord.OwnerId);
        ccrz__E_ContactAddr__c existingAddressId = new ccrz__E_ContactAddr__c();

        if(addressTypeMap != null){
            String otherAddressType = addressType;
            //String otherAddressType = addressType == 'Shipping' ? 'Billing' : 'Shipping';
            Map<String, ccrz__E_AccountAddressBook__c> addressMap = addressTypeMap.get(otherAddressType);
            if(addressMap != null){
                String addressRecordKey = String.format('{0}_{1}_{2}_{3}_{4}_{5}', new String[]{addressRecord.ccrz__FirstName__c,  addressRecord.ccrz__LastName__c, addressRecord.ccrz__AddressFirstline__c, addressRecord.ccrz__AddressSecondline__c, addressRecord.ccrz__City__c, addressRecord.ccrz__PostalCode__c});
                if(addressMap.containsKey(addressRecordKey)){
                    ccrz__E_AccountAddressBook__c duplicateRecord = addressMap.get(addressRecordKey);
                    existingAddressId = [SELECT Id, ccrz__FirstName__c,
                                                    ccrz__LastName__c,
                                                    ccrz__AddressFirstline__c,
                                                    ccrz__AddressSecondline__c,
                                                    ccrz__City__c,
                                                    ccrz__State__c,
                                                    ccrz__StateISOCode__c,
                                                    ccrz__Country__c,
                                                    ccrz__CountryISOCode__c,
                                                    OwnerId,
                                                    ccrz__PostalCode__c FROM ccrz__E_ContactAddr__c
                                                    WHERE Id = : duplicateRecord.ccrz__E_ContactAddress__c];
                }
            }
        }

        return existingAddressId;
    }
}