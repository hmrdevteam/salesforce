/**
* Apex Content Template Controller for Hero Section on Coach Value Proposition Page(linked from Home Page)
*
* @Date: 07/17/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

global virtual class HMR_CMS_CoachValueHero_ContentTemplate extends cms.ContentTemplateController {
	global HMR_CMS_CoachValueHero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_CoachValueHero_ContentTemplate() {}
	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
	*/
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
	*/
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	//Title Label Property
	//The Middle section in the hero content (Why Coaching?)
    public String HeroTitleText{
        get{
            return getPropertyWithDefault('HeroTitleText', 'Why Coaching?');
		}
    }

	//the short description that goes exactly below the above title in the middle section
	public String HeroSectionDescription{
        get{
            return getPropertyWithDefault('HeroSectionDescription', 'A lot of people don\'t realize that successful weight management is actually a skill. Just like with any new skill, you have to first learn it, and then practice it over and over in order to master it. That\'s where your HMR Coach can help');
        }
    }

	public String returnNotNullAndNotEmptyString(String value) {
		if(value != null && value != '') {
			return value;
		}
		return '';
	}

	global virtual override String getHTML(){
		String html = '';
		String infoHTML = '';
		infoHTML += '<div>' +
						'<h2>' + returnNotNullAndNotEmptyString(HeroTitleText) + '</h2>' +
					'</div>' +
					'<div class="clinic-info-wrapper">' +
						'<div class="hmr-info">' + returnNotNullAndNotEmptyString(HeroSectionDescription) + '</div>' +
					'</div>';
		html += '<section class="hmr-cliniclanding-hero-section bg-typecover bg-CoachValueProp">' +
			        '<div class="container visible-lg visible-md">' +
			            '<div class="hero-row">' +
							infoHTML +
			            '</div>' +
			        '</div>' +
			    '</section>' +
				'<section class="hmr-cliniclanding-text-section bg-grey">' +
					'<div class="container visible-sm visible-xs">' +
						'<div class="non-hero-row">' +
							infoHTML +
						'</div>' +
					'</div>' +
				'</section>';
		return html;
	}

	/**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_CoachValueHero_ContentTemplate.class;
    }
}