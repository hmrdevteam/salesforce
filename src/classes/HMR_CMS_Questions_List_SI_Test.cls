/*
Developer: Joey Zhuang (Magnet360)
Date: Aug 17th, 2017
Log: 
Target Class(ses): HMR_CMS_JoinConversation_ContentTemplate
*/

@isTest(seealldata=true)//See All Data Required for ConnectApi Methods (Connect Api methods are not supported in data siloed tests)
private class HMR_CMS_Questions_List_SI_Test{
  
  	@isTest static void test_general_method() {
  		map<string, string> param = new map<string,string>();
  		param.put('action','getFeedElements2');
  		param.put('pageNumber','1');
  		param.put('topicName','Eating Wells');

		ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
	    List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
	    testItemList.add(new ConnectApi.FeedItem());
	    testItemList.add(new ConnectApi.FeedItem());
	    testPage.elements = testItemList;

	    String communityId = [SELECT Salesforce_Id__c FROM Salesforce_Id_Reference__mdt WHERE DeveloperName = 'HMR_Community_Id'].Salesforce_Id__c;

	    HMR_Topic_Service topicService = new HMR_Topic_Service();
	    string topicId = topicService.getTopicId('Eating Well');
	    //system.debug('+++++^^'+communityId);
		HMR_ConnectApi_Service connectApiService = new HMR_ConnectApi_Service();
	 	ConnectApi.FeedElement fe = connectApiService.postQuestionAndAnswer(topicId, 'subject', 'body');
	 	ConnectApi.FeedElement fe2 = connectApiService.postQuestionAndAnswer(topicId, 'subject2', 'body2');

	    ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(communityId, ConnectApi.FeedType.Topics, topicId, testPage);



  		HMR_CMS_Questions_List_ServiceInterface si = new HMR_CMS_Questions_List_ServiceInterface();
  		si.executeRequest(param);

  		param.put('action','getFeedElements');
  		si.executeRequest(param);

  		param.put('topicName','Eating Wellss');
  		si.executeRequest(param);

  		param.put('topicName','Eating Well');
  		si.executeRequest(param);

  		param.put('pageNumber','a');
  		si.executeRequest(param);

  		si.executeRequest(null);

   		HMR_CMS_Questions_List_ServiceInterface.getType();
	    
	    try{
	        HMR_CMS_Questions_List_ServiceInterface qc = new HMR_CMS_Questions_List_ServiceInterface(null);
	    }catch(Exception e){}

  	}
  
}