/**
* Apex Service Class for the Stored Payments
*
* @Date: 2017-11-21
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-11-21
* @JIRA: 
*/
public with sharing class HMR_StoredPayment_Service {
    private string userId;

    public HMR_StoredPayment_Service(){
        this(UserInfo.getUserId());
    }

    public HMR_StoredPayment_Service(string userId) {
        ccrz.cc_RemoteActionContext contextCCRZ = new ccrz.cc_RemoteActionContext();
        contextCCRZ.portalUserId = userId;    
        ccrz.cc_CallContext.initRemoteContext(contextCCRZ);

        this.userId = userId;
    }

    /**
    * Fetches stored payment data from cloudcraze using the class user id.
    * @return the list of stored payment records
    */
    public List<StoredPaymentRecord> getByOwnerId(){
        return getByOwnerId(this.userId);
    }
    /**
    * Fetches stored payment data from cloudcraze using the owner id.
    * @param ownerId the user id of the person retrieving the stored payment data for
    * @return the list of stored payment records
    */
    public List<StoredPaymentRecord> getByOwnerId(string ownerId){
        List<StoredPaymentRecord> storedPaymentList = new List<StoredPaymentRecord>();
        Map<String, ccrz__E_StoredPayment__c> storedPaymentMap = new Map<String, ccrz__E_StoredPayment__c>([SELECT Id FROM ccrz__E_StoredPayment__c WHERE OwnerId =: ownerId]);
        Set<Id> storedPaymentIdSet = new Set<Id>();

        if(storedPaymentMap.size() > 0){
            Map<String, Object> inputDataMap = new Map<String, Object>{
                ccrz.ccApi.API_Version => 6
               ,ccrz.ccAPIStoredPayment.IDS => storedPaymentMap.keySet() 
            };

            try{
                Map<String, Object> outputDataMap = ccrz.ccApiStoredPayment.fetch(inputDataMap);
                if(outputDataMap.get(ccrz.ccAPIStoredPayment.STOREDPAYMENTSLIST) != null){
                    List<Map<String, Object>> outputStoredPaymentList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccAPIStoredPayment.STOREDPAYMENTSLIST);
                    for(Map<String, Object> storedPaymentRecordMap : outputStoredPaymentList){
                        storedPaymentList.add(new StoredPaymentRecord(storedPaymentRecordMap));
                        storedPaymentIdSet.add((String)storedPaymentRecordMap.get('sfid'));
                    }
                }
            }
            catch(Exception ex){
                System.Debug(ex);
            }
        }

        if(storedPaymentIdSet.size() > 0){
            Set<Id> addressIdSet = new Set<Id>();

            for(ccrz__E_StoredPayment__c sp : [SELECT HMR_Contact_Address__c FROM ccrz__E_StoredPayment__c WHERE Id IN: storedPaymentIdSet]){
                for(StoredPaymentRecord storedPayment : storedPaymentList){ 
                    if(storedPayment.id == sp.Id)
                        storedPayment.addressId = sp.HMR_Contact_Address__c;
                }
                if(!String.isBlank(sp.HMR_Contact_Address__c))
                    addressIdSet.add(sp.HMR_Contact_Address__c);
            }

            if(addressIdSet.size() > 0){
                Map<String, Object> addressMap = HMR_CMS_AddressService.getAddressesByIds(addressIdSet);
                Map<Id, HMR_CMS_AddressService.AddressRecord> storedPaymentAddressRecordMap = new Map<Id, HMR_CMS_AddressService.AddressRecord>();

                if(addressMap.get('addressList') != null){
                    List<Map<String, Object>> addressMapList = (List<Map<String, Object>>)addressMap.get('addressList');

                    for(Map<String, Object> addressRecordMap : addressMapList) {  
                        HMR_CMS_AddressService.AddressRecord addressRecord = new HMR_CMS_AddressService.AddressRecord(addressRecordMap);
                        storedPaymentAddressRecordMap.put(addressRecord.id, addressRecord);   
                    }
                }

                for(StoredPaymentRecord sp : storedPaymentList)
                    sp.addressRecord = storedPaymentAddressRecordMap.get(sp.addressId);
            }

        }

        return storedPaymentList;
    }
    
    public Map<String, Object> deleteStoredPayment(Id storedPaymentId) {
    		Map<String, Object> inputDataMap = new Map<String, Object>{
                ccrz.ccApi.API_Version => 6
               ,ccrz.ccAPIStoredPayment.ID => storedPaymentId
        };
        
        Map<String, Object> responseData = this.callDeleteStoredPayment(inputDataMap);
        return responseData;
    }
    
    private Map<String, Object> callDeleteStoredPayment(Map<String, Object> inputDataMap) {
    		return ccrz.ccApiStoredPayment.remove(inputDataMap);
    }

    //Wrapper class to hold stored payment record and related object data
    public class StoredPaymentRecord{
        public string id {get; private set;}
        public string account {get; private set;}
        public string accountNumber {get; private set;}
        public string accountType {get; private set;}
        public string addressId {get; private set;}
        public HMR_CMS_AddressService.AddressRecord addressRecord {get; private set;}
        public string displayName {get; private set;}
        public boolean enabled {get; private set;}
        public string accountNumberLastFour {get{return !String.isBlank(accountNumber) && accountNumber.length() >= 4 ? accountNumber.right(4) : ''; }}
        public string paymentType {get; private set;}
        public string token {get; private set;}
        public integer expirationMonth {get; private set;}
        public integer expirationYear {get; private set;}
        public integer sequence {get; private set;}

        public StoredPaymentRecord(Map<String, Object> storedPaymentRecordMap){
            this.id = (String)storedPaymentRecordMap.get('sfid');
            this.account = (String)storedPaymentRecordMap.get('account');
            this.accountNumber = (String)storedPaymentRecordMap.get('accountNumber');
            this.accountType = (String)storedPaymentRecordMap.get('accountType');
            this.addressId = (String)storedPaymentRecordMap.get('address');
            this.displayName = (String)storedPaymentRecordMap.get('displayName');
            this.enabled = (Boolean)storedPaymentRecordMap.get('enabled');
            this.paymentType = (String)storedPaymentRecordMap.get('paymentType');
            this.token = (String)storedPaymentRecordMap.get('token');
            this.expirationMonth = Integer.valueOf(storedPaymentRecordMap.get('expMonth'));
            this.expirationYear = Integer.valueOf(storedPaymentRecordMap.get('expYear'));
            this.sequence = Integer.valueOf(storedPaymentRecordMap.get('sequence'));
        }
    }
}