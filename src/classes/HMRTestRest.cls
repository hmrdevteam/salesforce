@RestResource(urlMapping='/TestRest/*')
global class HMRTestRest {

    @HttpGet
    global static String doGet() {
        return 'Current user:' + UserInfo.getUserName() + ' session:' + UserInfo.getSessionId();
    }
}