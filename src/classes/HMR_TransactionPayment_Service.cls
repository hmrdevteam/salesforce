/**
* Apex Service Class for Transaction Payments
*
* @Date: 2017-10-25
* @Author: Zach Engman (Magnet 360)
* @Modified: Zach Engman 2017-10-25
* @JIRA: HPRP-4340
*/
public with sharing class HMR_TransactionPayment_Service {
    public HMR_TransactionPayment_Service() {}

    /**
    * Fetches transaction payment data from cloudcraze using the order id.
    * @param orderId the salesforce order id of the transaction payment to return the data for
    * @return the transaction payment record
    */
    public TransactionPaymentRecord getByOrderId(string orderId){
        Map<String, Object> inputDataMap = new Map<String, Object>{
            ccrz.ccApi.API_Version => 6
           ,ccrz.ccApiTransactionPayment.ORDERS => new Set<String>{orderId}
        };

        return getTransactionPayment(inputDataMap);
    }

    /**
    * Fetches transaction payment data from cloudcraze
    * @param inputDataMap the cloud craze formatted input data map request parameter
    * @return the transaction payment record
    */
    private TransactionPaymentRecord getTransactionPayment(Map<String, Object> inputDataMap){
        TransactionPaymentRecord paymentRecord;

        try{
            Map<String, Object> outputDataMap = ccrz.ccApiTransactionPayment.fetch(inputDataMap);

            if(outputDataMap.get(ccrz.ccAPITransactionPayment.TRANSACTIONPAYMENTSLIST) != null){
                List<Map<String, Object>> outputTransactionPaymentsList = (List<Map<String, Object>>)outputDataMap.get(ccrz.ccAPITransactionPayment.TRANSACTIONPAYMENTSLIST);
                paymentRecord = new TransactionPaymentRecord(outputTransactionPaymentsList[0]);
            }   
        }
        catch(Exception ex){
            System.Debug(ex);
        }

        return paymentRecord;
    }

    //Wrapper class to hold transaction payment record and related object data
    public class TransactionPaymentRecord{
        public string id {get; private set;}
        public string accountId {get; private set;}
        //Last 4 digits of credit card
        public string accountNumber {get; private set;}
        public decimal amount {get; private set;}
        public integer expirationMonth {get; private set;}
        public integer expirationYear {get; private set;}
        //For credit cards this is the card type
        public string paymentType {get; private set;}
        
        public TransactionPaymentRecord(Map<String, Object> paymentDataMap){
            this.id = (String)paymentDataMap.get('sfid');
            this.accountId = (String)paymentDataMap.get('account');
            this.accountNumber = ((String)paymentDataMap.get('accountNumber')).right(4);
            this.amount = (Decimal)paymentDataMap.get('amount');
            this.expirationMonth = (Integer)paymentDataMap.get('expirationMonth');
            this.expirationYear = (Integer)paymentDataMap.get('expirationYear');
            this.paymentType = (String)paymentDataMap.get('paymentType');
        }
    }
}