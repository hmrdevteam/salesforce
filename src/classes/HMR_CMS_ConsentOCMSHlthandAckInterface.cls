/**
* @Date: 2017-05-23
* @Author Mustafa Ahmed (HMR)
* @JIRA: 
*/

global class HMR_CMS_ConsentOCMSHlthandAckInterface implements cms.ServiceInterface {
    
    public HMR_CMS_ConsentOCMSHlthandAckInterface() {}
    
        public String executeRequest(Map<String, String> params) {            
            Map<String, Object> responseString = new Map<String, Object>();    
            
             if(params.get('ContactId') != null && !String.isBlank((String)params.get('ContactId'))) { 
                 		   Id conId = params.get('ContactId');
                    Contact conUser = [SELECT Id, Account.RecordType.DeveloperName, AccountId FROM Contact WHERE Id = :conId LIMIT 1];                 
                 if (conUser.Account.RecordType.DeveloperName == 'Corporate_Account')
                 {
                     List<ccrz__E_Term__c> consentData1 = [Select ccrz__Description__c, ccrz__Title__c, Consent_Type__c, 
                                                          Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE 
                                                          Consent_Type__c = 'Health' AND Employer_Account__c = :conUser.AccountId
                                                            AND ccrz__Enabled__c = TRUE LIMIT 1]; 
                     return JSON.serialize(consentData1); 
                 }
                                  
                 else{                     
                     List<ccrz__E_Term__c> consentData2 = [Select ccrz__Description__c, ccrz__Title__c, Consent_Type__c, 
                                                          Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE 
                                                          Consent_Type__c = 'Health' AND Employer_Account__c = null
                                                            AND ccrz__Enabled__c = TRUE LIMIT 1];
                     return JSON.serialize(consentData2); 
                 	}            
                }
            else {
                	 List<ccrz__E_Term__c> consentData3 = [Select ccrz__Description__c, ccrz__Title__c, Consent_Type__c, 
                                                          Employer_Account__c, ccrz__Enabled__c FROM ccrz__E_Term__c WHERE 
                                                          Consent_Type__c = 'Health' AND Employer_Account__c = null
                                                            AND ccrz__Enabled__c = TRUE LIMIT 1];
                     return JSON.serialize(consentData3);   
            }   
        } 
    
      public static Type getType() {
            return HMR_CMS_ConsentOCMSHlthandAckInterface.class;
        }  
}