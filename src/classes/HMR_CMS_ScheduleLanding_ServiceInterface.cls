/**
* Service Interface Class to get the schedule Details
*
* @Date: 2018-02-22
* @Author: Utkarsh Goswami (Magnet 360)
* @Modified: 2018-02-22 Zach Engman (Magnet 360)
* @JIRA: 
*/
global with sharing class HMR_CMS_ScheduleLanding_ServiceInterface implements cms.ServiceInterface{
  /**
    *
    * @param params a map of parameters including at minimum a value for 'action'
    * @return a JSON-serialized response string
   */
    public String executeRequest(Map<String, String> params) {
      Map<String, Object> returnMap = new Map<String, Object>{'success' => true};
        String action = params.get('action');
        String userId = params.get('userId');

        if(!String.isBlank(action) && !String.isBlank(userId)){

            if(action == 'getCoachingSelection'){
                
                List<Program_Membership__c> programList = [SELECT Coaching_Selection__c FROM Program_Membership__c WHERE hmr_Status__c = 'Active' AND hmr_Contact__r.Community_User_ID__c =: userId];
                boolean coachingSelection = false;

                if(programList.size() > 0){
                    coachingSelection = programList[0].Coaching_Selection__c == 'Yes';
                }

                returnMap.put('coachingSelection', coachingSelection);
            }
        }
        else{
            returnMap.put('success', false);
            returnMap.put('error', 'action and userId required');
        }

        return JSON.serialize(returnMap);
    }

  public static Type getType() {
        return HMR_CMS_ScheduleLanding_ServiceInterface.class;
    }
}