/**
* Test Class Coverage of the HMR_CMS_CCRZ_MyAccount_ServiceInterface
*
* @Date: 11/23/2017
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @JIRA: 
*/
@isTest
private class HMR_CMS_CCRZ_MyAccount_ServInt_Test{

    @isTest
    private static void firstTestMethod(){
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='test1122@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test1122@testorg.com');
        
        insert u;
        
        Account testAcc = new Account( Name = 'test', hmr_Active__c = TRUE);
        insert testAcc;
       
       
       Map<String, String> params = new Map<String, String>();
       params.put('action','getAccountAddresses');
       params.put('userId',u.id);
       params.put('accountId', testAcc.id);
        
        
        HMR_CMS_CCRZ_MyAccount_ServiceInterface accountServiceInerface = new HMR_CMS_CCRZ_MyAccount_ServiceInterface();   
        accountServiceInerface.executeRequest(params) ;       
        params.put('action','confirmPassword');
        accountServiceInerface.executeRequest(params) ;
        params.put('action','getStoredPayments');
        accountServiceInerface.executeRequest(params) ;
        params.put('action','invalid');
        accountServiceInerface.executeRequest(params) ;
        params.put('action',null);
        accountServiceInerface.executeRequest(params) ;
        params.put('userId',null);
        params.put('accountId',null);
        accountServiceInerface.executeRequest(params);
    }


    @isTest
    private static void testGetAboutMeInfo() {
        Map<String, String> params = getBaseParamMap('getAboutMeInfo');

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
        assertResponseContainsListOfSize(responseMap, 'contactInfo', 1);
    }

    @isTest
    private static void testUpdateAboutMeInfo() {
        // create the base param map
        Map<String, String> params = getBaseParamMap('updateAboutMeInfo');

        // add the updated about me info to the param map
        params.put('contactData', JSON.serialize(new Map<String, String> {
            'firstName' => 'TestUpdateAboutMeFirstName',
            'lastName' => 'TestUpdateAboutMeLastName'
        }));

        // invoke the account service interface
        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
        assertResponseContainsListOfSize(responseMap, 'contactInfo', 1);

        // data is searched for in the response string since casting contactInfo to HMR_Contact_Service.ContactRecord
        // throws a unexpected salesforce error
        System.assert(responseString.contains('TestUpdateAboutMeFirstName'), 'expected firstName to be updated to testUpdateAboutMeFirstName');
        System.assert(responseString.contains('TestUpdateAboutMeLastName'), 'expected firstName to be updated to testUpdateAboutMeFirstName');
    }

    @isTest
    private static void testCreateAddress() {
        // create the base param map
        Map<String, String> params = getBaseParamMap('createAddress');

        HMR_CMS_AddressService.AddressRecord addressRecord = new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
                'addressFirstline' => '123 Test St',
                'city' => 'Test Town',
                'stateISOCode' => 'CO',
                'postalCode' => '12345'
        });

        addressRecord.isBillingAddress = true;
        addressRecord.isDefaultBillingAddress = true;

        params.put('addressData', JSON.serialize(addressRecord));

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);

        assertResponseContainsSuccessFlag(responseMap);
        assertResponseContainsListOfSize(responseMap, 'addresses', 1);
    }

    @isTest
    private static void testUpdateAddress() {
        Map<String, String> params = getBaseParamMap('updateAddress');

        HMR_CMS_AddressService.AddressRecord addressRecord = new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
                'sfid' => cc_dataFActory.billToAddress.Id,
                'addressFirstline' => '123 Test St'
        });

        addressRecord.isBillingAddress = true;
        addressRecord.isDefaultBillingAddress = true;

        params.put('addressData', JSON.serialize(addressRecord));

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);

        assertResponseContainsSuccessFlag(responseMap);
        assertResponseContainsListOfSize(responseMap, 'addresses', 1);

        // data is searched for in the response string since casting contactInfo to HMR_Contact_Service.ContactRecord
        // throws a unexpected salesforce error
        System.assert(responseString.contains('123 Test St'), 'expected "addressFirstline" to be "123 Test St"');
    }

    @isTest
    private static void testDeleteAddress() {
        Map<String, String> params = getBaseParamMap('deleteAddress');

        HMR_CMS_AddressService.AddressRecord addressRecord = new HMR_CMS_AddressService.AddressRecord(new Map<String, Object> {
                'sfid' => cc_dataFActory.billToAddress.Id
        });

        params.put('addressData', JSON.serialize(addressRecord));

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
        assertResponseContainsListOfSize(responseMap, 'addresses', 0);
    }

    @isTest(SeeAllData=true)
    static void testUpdatePhoto() {
        boolean runStartStopStatements = true;
        testUpdateAvatar(runStartStopStatements);
    }

    @isTest(SeeAllData=true)
    static void testDeletePhoto() {
        boolean runStartStopStatements = false;
        testUpdateAvatar(runStartStopStatements);

        Map<String, String> params = getBaseParamMap('removePhoto');

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
    }

    @isTest
    static void deleteStoredPayment() {
//        cc_dataFactory.setupTestUser();
//        ccrz__E_ContactAddr__c contactAddressRecord = cc_dataFactory.getContactAddress();
//        insert contactAddressRecord;
        cc_dataFactory.setupCatalog();
        cc_dataFactory.createCart();

        Map<String, String> params = getBaseParamMap('deleteStoredPayment');

        Test.startTest();
        String responseString = executeAccountServiceInterfaceRequest(params);
        Test.stopTest();

        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
    }
    
    @isTest
    private static void testGetType(){
    
        HMR_CMS_CCRZ_MyAccount_ServiceInterface.getType();
    
    }

    private static Map<String, String> getBaseParamMap(String action) {
        return new Map<String, String> {
            'userId' => cc_dataFactory.testUser.id,
            'accountId' => cc_dataFActory.testAccount.Id,
            'action' => action
        };
    }

    private static String executeAccountServiceInterfaceRequest(Map<String, String> params) {
        HMR_CMS_CCRZ_MyAccount_ServiceInterface accountServiceInterface = new HMR_CMS_CCRZ_MyAccount_ServiceInterface();
        String responseString = accountServiceInterface.executeRequest(params);
        System.debug('*** ' + params.get('action') + ' response: ' + responseString);
        return responseString;
    }

    private static void assertResponseContainsSuccessFlag(Map<String, Object> responseMap) {
        System.assertEquals(true, Boolean.valueOf(responseMap.get('success')), 'response was not flagged as successful');
    }

    private static void assertResponseContainsListOfSize(Map<String, Object> responseMap, String listName, Integer size) {
        // The list casted List<Object> to work around a salesforce bug
        List<Object> responseList = (List<Object>) responseMap.get(listName);
        System.assertEquals(size, responseList.size(), listName + ' is expected to be of size: ' + size);
    }

    private static void testUpdateAvatar(boolean runStartStopStatements) {
        Map<String, String> params = getBaseParamMap('updatePhoto');

        // add image data to param list
        String imgData = 'data:R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==';
        params.put('photo', imgData);

        if(runStartStopStatements) {
            Test.startTest();
        }

        String responseString = executeAccountServiceInterfaceRequest(params);

        if(runStartStopStatements) {
            Test.stopTest();
        }


        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        assertResponseContainsSuccessFlag(responseMap);
    }
}