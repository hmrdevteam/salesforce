/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/05/2018
 * Created By: Zach Engman
 * Last Modified Date: 01/05/2018
 * Last Modified By:
 * Description: Test Coverage for the HMRMobRegisterUser REST Service
 * ****************************************************/
@IsTest
private class HMRMobRegisterUserTest {
    @isTest
    private static void testRegister() {
        User userRecord = cc_dataFactory.testUserGuest;
        Date dob = Date.newInstance(1990, 2, 17);
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestURI = '/services/apexrest/mobile/user/communityUser/';
        request.httpMethod = 'POST';

        RestContext.request = request;
        RestContext.response = response;
        
        Test.startTest();

        System.runAs(userRecord) {
            HMRMobRegisterUser.doPost(userRecord.FirstName,  
                                      userRecord.LastName,  
                                      String.format('{0}.{1}@magnet360.com',new String[]{userRecord.FirstName, userRecord.LastName}), 
                                      userRecord.LastName + '!99', 
                                      dob);
        }

        Test.stopTest();

        //Verify invalid registration - won't work outside active site
        System.assertEquals(500, RestContext.response.statusCode);
    }
    
    
     @isTest
    private static void testDoesUserExist(){
    
      User userRecord = cc_dataFactory.testUserGuest;
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/communityUser';
      request.httpMethod = 'GET';
      request.params.put('email', userRecord.email);

      RestContext.request = request;
      RestContext.response = response;
      
      Test.startTest();

      HMRMobRegisterUser.doesUserExist();
      
      Test.stopTest();
      
      //Verify return status
      System.assertEquals(200, response.statusCode);
      
    }
    
    @isTest
    private static void testUserAlreadyExsistsInvalid(){
    
      User userRecord = cc_dataFactory.testUserGuest;
      RestRequest request = new RestRequest();
      RestResponse response = new RestResponse();

      request.requestURI = '/services/apexrest/mobile/user/communityUser';
      request.httpMethod = 'GET';
      request.params.put('email', 'test');

      RestContext.request = request;
      RestContext.response = response;
      
      Test.startTest();

          HMRMobRegisterUser.doesUserExist();
      
      Test.stopTest();
      
      //Verify return status
      System.assertEquals(200, response.statusCode);
      
    }

}