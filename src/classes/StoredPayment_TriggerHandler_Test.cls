/**
* Test Class Coverage of the HMR_CC_Order_Selector Class
*
* @Date: 05/25/2017
* @Author Utkarsh Goswami (Mindtree)
* @Modified: 
* @Class : StoredPayment_TriggerHandler
*/

@isTest
private class StoredPayment_TriggerHandler_Test{
  
    @isTest
    private static void test_method_first(){
    
        ccrz__E_StoredPayment__c storedPayObj1 = new ccrz__E_StoredPayment__c(Name = 'test1', ccrz__Token__c = 'testToken' );
        insert storedPayObj1;
        
        ccrz__E_Order__c orderDet = new ccrz__E_Order__c(ccrz__AdjustmentReason__c= 'test order',ccrz__BuyerFirstName__c = 'test buyer',ccrz__OrderStatus__c = 'Template' );
        insert orderDet;
        
        ccrz__E_TransactionPayment__c transacPayment1 = new ccrz__E_TransactionPayment__c(ccrz__CCOrder__c = orderDet.Id, ccrz__Token__c = 'test',ccrz__StoredPayment__c = storedPayObj1.id);
        insert transacPayment1 ;
    
        storedPayObj1.ccrz__Token__c = 'test2';
        
        update storedPayObj1;
       // ccrz__E_StoredPayment__c storedPayObj2 = new ccrz__E_StoredPayment__c(Name = 'test2', ccrz__Token__c = 'testToken2' );
      //  insert storedPayObj2;
       
    }
}