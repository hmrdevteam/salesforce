/**
* Test Class Coverage of the HMR_CMS_Knowledge_ServiceInterface
*
* @Date: 08/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/18/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_Conversation_ServiceInt_Test {
	@isTest
	private static void testGetConversationDataRequest(){
		HMR_CMS_Conversation_ServiceInterface service = new HMR_CMS_Conversation_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'getConversationData');

		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testInvalidActionRequest(){
		HMR_CMS_Conversation_ServiceInterface service = new HMR_CMS_Conversation_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('invalidMethod', '');

		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(result.containsIgnoreCase('Invalid Action'));
	}

    @isTest
    private static void testGetType(){
        Test.startTest();

        Type typeResult = HMR_CMS_Conversation_ServiceInterface.getType();

        Test.stopTest();
    }
}