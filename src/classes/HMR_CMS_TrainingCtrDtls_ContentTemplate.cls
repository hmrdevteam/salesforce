/**
* Apex Content Template Controller for Details and exclusions Section on
* Training Center Page for Lexington Clinic (HMR Training Center)
*
* @Date: 07/17/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/
global virtual class HMR_CMS_TrainingCtrDtls_ContentTemplate extends cms.ContentTemplateController {
	global HMR_CMS_TrainingCtrDtls_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_TrainingCtrDtls_ContentTemplate() {}
	/**
	 * A shorthand to retrieve a default value for a property if it hasn't been saved.
	 *
	 * @param propertyName the property name, passed directly to getProperty
	 * @param defaultValue the default value to use if the retrieved property is null
	*/
	@TestVisible
	private String getPropertyWithDefault(String propertyName, String defaultValue) {
		String property = getAttribute(propertyName);
		if(property == null) {
			return defaultValue;
		}
		else {
			return property;
		}
	}
	/** Provides an easy way to define attributes during testing */
	@TestVisible
	private Map<String, String> testAttributes = new Map<String, String>();

	/** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
	 * map in a test context.
	*/
	private String getAttribute(String attributeName) {
		if(Test.isRunningTest()) {
			return testAttributes.get(attributeName);
		}
		else {
			return getProperty(attributeName);
		}
	}

	//CSS Class for Background Color Property
    public String CSSClassForBackgroundColor{
        get{
            return getPropertyWithDefault('CSSClassForBackgroundColor', 'bg-orange');
        }
    }

	//CSS Class property for Font Color
	public String FontColorBlackOrWhite{
        get{
            return getPropertyWithDefault('FontColorBlackOrWhite', '[]');
        }
    }

	//Property for the display message
    public String DisplayMessage{
        get{
            return getPropertyWithDefault('DisplayMessage', 'JOIN TODAY AND RECEIVE A WEEK OF FREE FOOD AFTER ATTENDING YOUR FIRST CLASS');
        }
    }

	//Property for the link display for modal popup
    public String LinkDisplayText{
        get{
            return getPropertyWithDefault('LinkDisplayText', 'SEE DETAILS & EXCLUSIONS');
        }
    }

	//Property for Title inside Modal Popup
    public String ModalTitle{
        get{
            return getPropertyWithDefault('ModalTitle', 'Offer Details');
        }
    }

	//Property for Description inside Modal Popup
    public String ModalDescription{
        get{
            return getPropertyWithDefault('ModalDescription', '');
        }
    }


	global virtual override String getHTML(){
		String html = '';
		html += '<div class="hmr-modal modal fade" id="details_exclusions">' +
			        '<div class="hmr-clinic-modal-dialog modal-dialog" role="document">' +
			            '<div class="modal-content hmr-modal-content">' +
			                '<div class="modal-header hmr-modal-header">' +
			                    '<h5 class="modal-title">' + ModalTitle + '</h5>' +
			                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
			                        '<span aria-hidden="true">&times;</span>' +
			                    '</button>' +
			                '</div>' +
			                '<div class="modal-body hmr-modal-body">' +
			                    '<p>' + ModalDescription + '</p>' +
			                '</div>' +
			            '</div>' +
		        	'</div>' +
				'</div>';
		html += '<section class="join-today-see-details-section ' + FontColorBlackOrWhite + ' ' +  CSSClassForBackgroundColor + '">' +
			        '<div class="container">' +
			            '<div class="row">' +
			                '<div class="col-xs-12">' +
			                    '<h3 class="hmr-allcaps">' + DisplayMessage + '</h3>' +
			                    '<div class="hmr-allcaps-container">' +
			                        '<a class="hmr-allcaps ' + FontColorBlackOrWhite + '" data-toggle="modal" data-target="#details_exclusions">' + LinkDisplayText + '</a>' +
			                        '<a class="hmr-allcaps hmr-allcaps-carat white" data-toggle="modal" data-target="#details_exclusions">&gt;</a>' +
			                    '</div>' +
			                '</div>' +
			            '</div>' +
			        '</div>' +
			    '</section>';
		return html;
	}

	/**
     * getType --> global method to override cms.ServiceInterface
     * @param
     * @return a JSON-serialized response string
     */
    public static Type getType() {
        return HMR_CMS_TrainingCtrDtls_ContentTemplate.class;
    }
}