/**
* Test Class Coverage of the HMR_CMS_Knowledge_ServiceInterface
*
* @Date: 08/18/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/18/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_Knowledge_ServiceInterface_Test {

	@isTest
	private static void testGetNextCardSetRequest(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'getNextCardSet');
		parameterMap.put('listTitle', 'Recipes');
		parameterMap.put('objectType', 'Recipes');
		parameterMap.put('dataCategory', 'All');
		parameterMap.put('recipeCategory', 'All');
		parameterMap.put('subFilters', '');
		parameterMap.put('offset', '0');
		parameterMap.put('recordCount', '0');
		parameterMap.put('recordsShown', '');
		parameterMap.put('colorBlocksShown', '');

		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testGetVoteRequest(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'getVote');
		parameterMap.put('articleName', 'test-blog-01');
		
		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testGetVoteCountRequest(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'getVoteCount');
		parameterMap.put('articleName', 'test-blog-01');
		
		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testLikeArticleRequest(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'likeArticle');
		parameterMap.put('articleName', 'test-blog-01');
		
		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testGetNextPage(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('action', 'getNextPage');
		parameterMap.put('pageNumber', '1');
		
		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(!String.isBlank(result));
	}

	@isTest
	private static void testInvalidActionRequest(){
		HMR_CMS_Knowledge_ServiceInterface service = new HMR_CMS_Knowledge_ServiceInterface();
		Map<String, String> parameterMap = new Map<String, String>();

		parameterMap.put('invalidMethod', '');

		Test.startTest();

		String result = service.executeRequest(parameterMap);

		Test.stopTest();

		System.assert(result.containsIgnoreCase('Invalid Action'));
	}

	@isTest
    private static void testCoreControllerConstructor(){
        HMR_CMS_Knowledge_ServiceInterface controller;
        
        Test.startTest();
     
        controller = new HMR_CMS_Knowledge_ServiceInterface(null);

        Test.stopTest();
        
        System.assert(controller != null);
    }

    @isTest
    private static void testGetType(){
        Test.startTest();

        Type typeResult = HMR_CMS_Knowledge_ServiceInterface.getType();

        Test.stopTest();
    }
}