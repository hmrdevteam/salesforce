/**
* Test Class Coverage of the HMR_CMS_ArticleDetail_ContentTemplate
*
* @Date: 08/16/2017
* @Author Zach Engman (Magnet 360)
* @Modified: Zach Engman 08/16/2017
* @JIRA: 
*/
@isTest
private class HMR_CMS_ArticleDetail_ContentTmpl_Test {
	@testSetup
	private static void setTestData(){
		Blog_Post__kav completeBlogArticle = new Blog_Post__kav(Title='Test Blog'
															   ,UrlName='test-blog-complete'
															   ,Language='en_US'
															   ,Caption_1__c = 'Test Caption 1'
															   ,Caption_2__c = 'Test Caption 2'
															   ,Caption_3__c = 'Test Caption 3'
															   ,File_Download_URL__c = 'http://www.hmrprogram.com'
															   ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
															   ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
															   ,Image_1__c = 'http://www.hmrprogram.com'
															   ,Image_2__c = 'http://www.hmrprogram.com'
															   ,Image_3__c = 'http://www.hmrprogram.com'
															   ,Post_Content__c = 'Test Caption 1'
															   ,Post_Content_2__c = 'Test Content 2'
															   ,Post_Content_3__c = 'Test Content 3'
															   ,Post_Content_4__c = 'Test Content 4'
															   ,Takeaways__c = 'Test Takeaway'
															   ,Teaser__c = 'Test Teaser'
															   ,Video_Code__c = 'ABC'
															   ,Video_Image_URI__c = 'http://www.hmrprogram.com'
															   ,Video_Title__c = 'Sample Video');
		Blog_Post__kav partial01BlogArticle = new Blog_Post__kav(Title='Test Blog'
															    ,UrlName='test-blog-partial01'
															    ,Language='en_US'
															    ,Caption_1__c = 'Test Caption 1'
															    ,File_Download_URL__c = 'http://www.hmrprogram.com'
															    ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
															    ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
															    ,Image_1__c = 'http://www.hmrprogram.com');
		Blog_Post__kav partial02BlogArticle = new Blog_Post__kav(Title='Test Blog'
															    ,UrlName='test-blog-partial02'
															    ,Language='en_US'
															    ,Caption_1__c = 'Test Caption 1'
															    ,Caption_2__c = 'Test Caption 2'
															    ,File_Download_URL__c = 'http://www.hmrprogram.com'
															    ,Hero_Image_URI__c = 'http://www.hmrprogram.com'
															    ,Hero_Mobile_Image_URI__c = 'http://www.hmrprogram.com'
															    ,Image_1__c = 'http://www.hmrprogram.com'
															    ,Image_2__c = 'http://www.hmrprogram.com'
															    ,Post_Content__c = 'Test Caption 1'
															    ,Post_Content_2__c = 'Test Content 2');
		Blog_Post__kav incompleteBlogArticle = new Blog_Post__kav(Title='Test Blog'
															   ,UrlName='test-blog-incomplete'
															   ,Language='en_US');

		List<Blog_Post__kav> blogArticleList = new List<Blog_Post__kav>{completeBlogArticle, partial01BlogArticle, partial02BlogArticle, incompleteBlogArticle};
		insert blogArticleList;
        
		List<Blog_Post__kav> blogArticleToPublishList = [SELECT KnowledgeArticleId FROM Blog_Post__kav WHERE Id IN: blogArticleList];
		for(Blog_Post__kav blogRecord : blogArticleToPublishList)
			KbManagement.PublishingService.publishArticle(blogRecord.KnowledgeArticleId, true);
	}

	@isTest
    private static void testGetHtmlFromQueryStringForCompleteBlogArticle(){
        HMR_CMS_ArticleDetail_ContentTemplate controller = new HMR_CMS_ArticleDetail_ContentTemplate();

        Test.startTest();

        Test.setCurrentPage(Page.HMR_CMS_KnowledgeArticle_PageTemplate);
        ApexPages.currentPage().getParameters().put('name', 'test-blog-complete');
     
        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }

    @isTest
    private static void testGetHtmlFromQueryStringForPartial01BlogArticle(){
        HMR_CMS_ArticleDetail_ContentTemplate controller = new HMR_CMS_ArticleDetail_ContentTemplate();

        Test.startTest();

        Test.setCurrentPage(Page.HMR_CMS_KnowledgeArticle_PageTemplate);
        ApexPages.currentPage().getParameters().put('name', 'test-blog-partial01');
     
        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }

    @isTest
    private static void testGetHtmlFromQueryStringForPartial02BlogArticle(){
        HMR_CMS_ArticleDetail_ContentTemplate controller = new HMR_CMS_ArticleDetail_ContentTemplate();

        Test.startTest();

        Test.setCurrentPage(Page.HMR_CMS_KnowledgeArticle_PageTemplate);
        ApexPages.currentPage().getParameters().put('name', 'test-blog-partial02');
     
        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }

    @isTest
    private static void testGetHtmlFromQueryStringForIncompleteBlogArticle(){
        HMR_CMS_ArticleDetail_ContentTemplate controller = new HMR_CMS_ArticleDetail_ContentTemplate();

        Test.startTest();

        Test.setCurrentPage(Page.HMR_CMS_KnowledgeArticle_PageTemplate);
        ApexPages.currentPage().getParameters().put('name', 'test-blog-incomplete');
     
        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }
    
    @isTest
    private static void testGetHtmlFromUrlForCompleteBlogArticle(){
        HMR_CMS_ArticleDetail_ContentTemplate controller = new HMR_CMS_ArticleDetail_ContentTemplate();

        Test.startTest();

        Test.setCurrentPage(new PageReference('/apex/HMR_CMS_KnowledgeArticle_PageTemplate/test-blog-complete'));

        String htmlResult = controller.getHTML();
        
        Test.stopTest();
        
        //Verify the html was returned
        System.assert(!String.isBlank(htmlResult));
    }
}