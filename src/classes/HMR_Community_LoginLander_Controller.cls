public with sharing class HMR_Community_LoginLander_Controller {
	public String returnURL { get; set; }
	public HMR_Community_LoginLander_Controller() {
		PageReference returnURLPageReference = checkConsent();
		returnURL = returnURLPageReference.getUrl();
	}

	public PageReference checkConsent() {
		try{
			String userId = UserInfo.getUserId();
	        Id clientId = [SELECT ContactId FROM User WHERE Id = :userId LIMIT 1].ContactId;
	        List<Consent_Agreement__c> cList = [SELECT Id, Consent_Type__c FROM Consent_Agreement__c WHERE Status__c = 'Notified' AND Contact__c = :clientId];
	        PageReference redirectPage = new PageReference(Site.getBaseUrl() + '/my-account');
	        redirectPage.setRedirect(true);
        	if(cList.size() > 0) {
            	redirectPage = new PageReference(Site.getBaseUrl() + '/consent');
        	}
        	return redirectPage;
		}
		catch(Exception ex) {
			PageReference redirectPage = new PageReference(Site.getBaseUrl());
			redirectPage.setRedirect(true);
			return redirectPage;
		}
        return null;
	}
}