/*****************************************************
 * Author: Zach Engman
 * Created Date: 11/07/2017
 * Created By: Zach Engman
 * Last Modified Date: 11/07/2017
 * Last Modified By:
 * Description: Handler for Cart Trigger - Default Address Assignments
 * ****************************************************/
public with sharing class HMR_CC_Cart_Handler {
	private boolean isExecuting = false;
    private integer batchSize = 0;
    public static Boolean isBeforeInsertFlag = false;
    public static Boolean isBeforeUpdateFlag = false;
    
    public HMR_CC_Cart_Handler(boolean executing, integer size){
        isExecuting = executing;
        batchSize = size;
    }

    public void onBeforeInsert(List<ccrz__E_Cart__c> cartList){
        if(!isBeforeInsertFlag){
            isBeforeInsertFlag = true;

           assignDefaultAddresses(cartList);
        }
    }

    public void onBeforeUpdate(List<ccrz__E_Cart__c> oldCartList, List<ccrz__E_Cart__c> newCartList, Map<Id, ccrz__E_Cart__c> oldCartMap, Map<Id, ccrz__E_Cart__c> newCartMap){
        if(!isBeforeUpdateFlag){
            isBeforeUpdateFlag = true;

            assignDefaultAddresses(newCartList);
        }
    }

    public void assignDefaultAddresses(List<ccrz__E_Cart__c> cartList){
    	List<ccrz__E_Cart__c> cartUpdateList = new List<ccrz__E_Cart__c>();
    	Set<Id> accountIdSet = new Set<Id>();
    	Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('HMR Customer Account').getRecordTypeId();

    	for(ccrz__E_Cart__c cartRecord : cartList){
    		//Exclude guest carts and check shipping as we will populate both shipping and billing
    		if(!cartRecord.ccrz__AnonymousId__c 
    			&& String.isBlank(cartRecord.ccrz__ShipTo__c) 
                && String.isBlank(cartRecord.ccrz__BillTo__c)
    			&& !String.isBlank(cartRecord.ccrz__Account__c)
    			&& !String.isBlank(cartRecord.ccrz__Contact__c)){
    			accountIdSet.add(cartRecord.ccrz__Account__c);
    			cartUpdateList.add(cartRecord);
    		}
    	}

    	if(cartUpdateList.size() > 0){
    		Map<Id, Map<String, ccrz__E_AccountAddressBook__c>> contactToAddressMap = new Map<Id, Map<String, ccrz__E_AccountAddressBook__c>>();
            Map<Id, Map<String, ccrz__E_ContactAddr__c>> contactToClonedAddressMap = new Map<Id, Map<String, ccrz__E_ContactAddr__c>>();
            List<ccrz__E_ContactAddr__c> contactAddressCloneList = new List<ccrz__E_ContactAddr__c>();

    		for(ccrz__E_AccountAddressBook__c addressBookRecord : [SELECT ccrz__Account__c
																		 ,ccrz__AddressType__c
																		 ,ccrz__E_ContactAddress__c 
																		 ,ccrz__E_ContactAddress__r.Contact_Id__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__City__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__CompanyName__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__Country__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__CountryISOCode__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__DaytimePhone__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__Email__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__FirstName__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__HomePhone__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__LastName__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__MailStop__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__MiddleName__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__PostalCode__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__ShippingComments__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__State__c
                                                                         ,ccrz__E_ContactAddress__r.ccrz__StateISOCode__c
                                                                         ,ccrz__E_ContactAddress__r.OwnerId
																    FROM ccrz__E_AccountAddressBook__c 
																    WHERE ccrz__Account__c IN: accountIdSet
																     	 AND ccrz__Default__c = true]){
                if(contactToAddressMap.containsKey(addressBookRecord.ccrz__E_ContactAddress__r.Contact_Id__c)){
                    Map<String, ccrz__E_AccountAddressBook__c> addressMap = contactToAddressMap.get(addressBookRecord.ccrz__E_ContactAddress__r.Contact_Id__c);
                    addressMap.put(addressBookRecord.ccrz__AddressType__c, addressBookRecord);
                    contactToAddressMap.put(addressBookRecord.ccrz__E_ContactAddress__r.Contact_Id__c, addressMap);
                }
                else
                    contactToAddressMap.put(addressBookRecord.ccrz__E_ContactAddress__r.Contact_Id__c, new Map<String, ccrz__E_AccountAddressBook__c>{addressBookRecord.ccrz__AddressType__c => addressBookRecord});
            }

    		for(ccrz__E_Cart__c cartRecord : cartUpdateList){
                if(contactToAddressMap.containsKey(cartRecord.ccrz__Contact__c)){
                    Map<String, ccrz__E_AccountAddressBook__c> addressMap = contactToAddressMap.get(cartRecord.ccrz__Contact__c);

                    //Shipping Address
                    if(addressMap.containsKey('Shipping')){
                        ccrz__E_ContactAddr__c shippingAddress = addressMap.get('Shipping').ccrz__E_ContactAddress__r;
                        ccrz__E_ContactAddr__c shippingAddressClone = shippingAddress.clone(false, true);
                        contactAddressCloneList.add(shippingAddressClone);
                        contactToClonedAddressMap.put(cartRecord.ccrz__Contact__c, new Map<String, ccrz__E_ContactAddr__c>{'Shipping' => shippingAddressClone});
                    }

                    //Billing Address
                    if(addressMap.containsKey('Billing')){
                        ccrz__E_ContactAddr__c billingAddress = addressMap.get('Billing').ccrz__E_ContactAddress__r;
                        ccrz__E_ContactAddr__c billingAddressClone = billingAddress.clone(false, true);
                        contactAddressCloneList.add(billingAddressClone);
                        if(contactToClonedAddressMap.containsKey(cartRecord.ccrz__Contact__c)){
                            Map<String, ccrz__E_ContactAddr__c> addressTypeToContactAddressMap = contactToClonedAddressMap.get(cartRecord.ccrz__Contact__c);
                            addressTypeToContactAddressMap.put('Billing', billingAddressClone);
                            contactToClonedAddressMap.put(cartRecord.ccrz__Contact__c, addressTypeToContactAddressMap);
                        }
                        else
                            contactToClonedAddressMap.put(cartRecord.ccrz__Contact__c, new Map<String, ccrz__E_ContactAddr__c>{'Billing' => billingAddressClone});
                    }
                }
    		}

            if(contactToClonedAddressMap.size() > 0){
                insert contactAddressCloneList;

                for(ccrz__E_Cart__c cartRecord : cartUpdateList){
                    if(contactToClonedAddressMap.containsKey(cartRecord.ccrz__Contact__c)){
                        Map<String, ccrz__E_ContactAddr__c> addressMap = contactToClonedAddressMap.get(cartRecord.ccrz__Contact__c);

                        //Shipping Address
                        if(addressMap.containsKey('Shipping'))
                            cartRecord.ccrz__ShipTo__c = addressMap.get('Shipping').Id;
                        
                        //Billing Address
                        if(addressMap.containsKey('Billing'))
                            cartRecord.ccrz__BillTo__c = addressMap.get('Billing').Id;
                    }
                }
            }
    	}
    }
}