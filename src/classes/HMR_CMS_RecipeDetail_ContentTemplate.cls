/**
* Apex Content Template Controller for Recipe Details
*
* @Date: 03/20/2017
* @Author Joey Zhuang (Magnet 360)
* @Modified: Zach Engman 08/10/2017
* @JIRA:
*/

global virtual with sharing class HMR_CMS_RecipeDetail_ContentTemplate extends cms.ContentTemplateController{
    global HMR_CMS_RecipeDetail_ContentTemplate(cms.createContentController cc){
        super(cc);

    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    //recipe record
    public Recipe__kav recipe {get;set;}

    global HMR_CMS_RecipeDetail_ContentTemplate(){ }

    global virtual override String getHTML() {
        String html = '<div class="Recipe_Detail">' +
                             '<section id="r_hero" class="hero clearfix relative">' +
                                 '<div class="container">' +
                                     '<div class="hero-content hc">' +
                                         '<h2 class="recipes">RECIPES</h2>' +
                                         '<h1 class="recipe-title" id="r_title"></h1>' +
                                         '<div class="recipe-spec-items">' +
                                             '<div class="text-center recipe-spec item border-right">' +
                                                 '<img class="detail-icon float-left" src="https://www.myhmrprogram.com/ContentMedia/Resources/ClockWhite.svg" id="r_time"/>' +
                                             '</div>' +
                                             '<div class="text-center recipe-spec item" style="width: 80px; padding-left: 10px">' + //TODO: Remove Hard Style
                                                '<img class="detail-icon float-left" src="https://www.myhmrprogram.com/ContentMedia/Resources/HeartWhite.svg" id="r_rating">' +
                                             '</div>' +
                                         '</div>' +
                                     '</div>' +
                                 '</div>' +
                             '</section>' +
                             '<section>' +
                                 '<div class="container">' +
                                     '<div class="recipe-description">' +
                                         '<div class="row">' +
                                             '<div class="col-xs-12">' +
                                                 '<div class="user-detail">' +
                                                     '<img class="img-circle" src="https://www.myhmrprogram.com/ContentMedia/RecipeHome/DefaultProfilePicture.png" />' +
                                                     '<div class="name">HMR Team</div>' +
                                                     '<div class="date" id="r_date"></div>' +
                                                 '</div>' +
                                             '</div>' +
                                         '</div>' +
                                         '<h6 class="text-thin" id="r_summary"></h6>' +
                                     '</div>' +
                                     '<div class="recipe-ingredients">' +
                                         '<h6 class="header">INGREDIENTS</h6>' +
                                         '<ul id="r_ingredients">' +

                                         '</ul>' +
                                     '</div>' +
                                     '<div class="recipe-directions">' +
                                         '<h6 class="header">DIRECTIONS</h6>' +
                                         '<ol id="r_instructions" style="list-style-type: none;">' +

                                         '</ol>' +
                                     '</div>' +
                                     '<div class="recipe-directions">' +
                                         '<h6 class="header">PER SERVING</h6>' +
                                     '</div>' +
                                     '<div class="recipe-list clearfix">' +
                                         '<div class="recipe-list-item">' +
                                             '<div class="card recipe-card recipe-card">' +
                                                 '<div class="card-content recipe-card-content">' +
                                                     '<h5 class="no-spacing text-thin detail-card-text-big" id="r_calories">0</h5>' +
                                                     '<h5 class="no-spacing detail-card-text-small">calories</h5>' +
                                                 '</div>' +
                                             '</div>' +
                                         '</div>' +
                                         '<div class="recipe-list-item">' +
                                             '<div class="card recipe-card recipe-card">' +
                                                 '<div class="card-content recipe-card-content">' +
                                                     '<h5 class="no-spacing text-thin detail-card-text-big" id="r_fServe">0</h5>' +
                                                     '<h5 class="no-spacing detail-card-text-small">fruit servings</h5>' +
                                                 '</div>' +
                                             '</div>' +
                                         '</div>' +
                                         '<div class="recipe-list-item">' +
                                             '<div class="card recipe-card recipe-card">' +
                                                 '<div class="card-content recipe-card-content">' +
                                                     '<h5 class="no-spacing detail-card-text-big" id="r_vServe">0</h5>' +
                                                     '<h5 class="no-spacing detail-card-text-small">vegetable servings</h5>' +
                                                 '</div>' +
                                             '</div>' +
                                         '</div>' +
                                     '</div>' +
                                     '<div class="clearfix"></div>' +
                                 '</div>' +
                             '</section>' +
                        '</div>';
        return html;
    }

}