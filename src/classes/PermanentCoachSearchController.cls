/*
* @Date: 2017-01-19
* @Author Nathan Anderson (M360)
* @Modified: 
* @JIRA: Issue https://reside.jira.com/browse/HPRP-1858
*/


public Class PermanentCoachSearchController{

    public String selectedCoachId {get;set;}
    public List<Coach__c> newCoachList{get;set;}
    public Class__c classDetail {get;set;}
    String classId;
    public Set<String> timeSet;
    
    public PermanentCoachSearchController(ApexPages.StandardController controller) {    
         

       // getting the Class id from url
        classId = apexpages.currentpage().getparameters().get('Id');
        try{
               // getting the original Class details.
               // fetching Class records for the id which we get from url.
                classDetail = [Select hmr_Class_Day__c, hmr_Start_Time__c, hmr_End_Time__c, hmr_Coach__r.hmr_Coach__r.name, hmr_Coach__c,
                                       hmr_Class_Name__c,hmr_Class_Type__c from Class__c where id = :classId];

                Set<String> timeSet = TimeConversionUtility.getSetOfTimes(classDetail.hmr_Start_Time__c);
                
                // getting the list of all the coaches with following conditions.
                // must be active
                // must present in list of available coachs
                // must not present in list of unavailable coaches.
                //newCoachList = [Select id, name, hmr_Active__c,hmr_Coach__r.name from Coach__c where hmr_Active__c = TRUE AND Id IN :coachIdSet];
                newCoachList = [Select id, name, hmr_Active__c,hmr_Coach__r.name from Coach__c where hmr_Active__c = TRUE AND hmr_Coach__r.IsActive = TRUE  
                                  AND Id NOT IN(SELECT hmr_Coach__c FROM Class__c WHERE hmr_Class_Day__c = :classDetail.hmr_Class_Day__c 
                                                                AND hmr_Start_Time__c IN :timeSet)];  

                System.debug('New Coach LIst -> ' + newCoachList);                              
             
              // checking if the list is empty, that is coaches are not available
               if(newCoachList.size() == 0){
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Coaches are not available'));
               }
        }
        
        catch(Exception ex){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Record'));
              System.debug('Error -> ' + ex.getMessage());
        }

    }
    
    public void selectedCoach(){
    
        // assinging selected coach Id to the variable
        selectedCoachId = System.currentPagereference().getParameters().get('newCoachId');
        System.debug('Selected Coach -> ' + selectedCoachId );
    }
   
    public PageReference assignCoach(){
    
        // checking if the selected coach is not the original coach
  
        classDetail.hmr_Coach__c = selectedCoachId;
        
        try{
            update classDetail;
        }
        catch(DMLException ex){
            System.debug(ex.getMessage());
        } 
        
        PageReference retPage = new PageReference('/'+ classId); 
        return retPage;
    
    }
    
    
    public PageReference returnToPreviousPage(){
    
        PageReference retPage = new PageReference('/'+ classId); 
        return retPage;
    
    }
}