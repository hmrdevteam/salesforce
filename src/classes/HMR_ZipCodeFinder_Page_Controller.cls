/**
* Apex Controller for HMR Zip Code Finder Page
*
* @Date: 05/25/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

public class HMR_ZipCodeFinder_Page_Controller {
	public String ZipCodeMappings { get; set; }
	public string portalUser { get; set; }
	public String userFirstName { get; set; }
	public HMR_ZipCodeFinder_Page_Controller() {
		try {
			List<Zip_Code_Assignment__c> zipCodeAssignmentsList =
				[SELECT Name, hmr_Account_Location_Type__c FROM Zip_Code_Assignment__c WHERE Account__r.hmr_Active__c = TRUE AND Account__r.hmr_Referral__c != 'DNR' ORDER BY Name ASC NULLS LAST];
			ZipCodeMappings = '';
			if(zipCodeAssignmentsList != null && !zipCodeAssignmentsList.isEmpty()) {
				ZipCodeMappings = JSON.serialize(zipCodeAssignmentsList);
			}
			portalUser = ApexPages.currentPage().getParameters().get('portalUser');
			if(portalUser == null) {
	            portalUser = '';
	        }
			//get contact ID
			Id userId = String.isBlank(portalUser) ? UserInfo.getUserId() : portalUser;
			User u = [SELECT FirstName, ContactID from User WHERE Id =: userId];
			userFirstName = u.FirstName;
		}
		catch(Exception ex) {
			ZipCodeMappings = JSON.serialize(ex);
			System.debug('Exception in HMR_ZipCodeFinder_Page_Controller ' + ex.getMessage());
		}
	}
}