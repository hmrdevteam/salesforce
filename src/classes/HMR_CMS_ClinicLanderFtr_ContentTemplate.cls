/**
* Apex Content Template Controller for Footer Section on Clinic Landing Page
*
* @Date: 05/24/2017
* @Author Pranay Mistry (Magnet 360)
* @Modified:
* @JIRA:
*/

global virtual class HMR_CMS_ClinicLanderFtr_ContentTemplate extends cms.ContentTemplateController{
	global HMR_CMS_ClinicLanderFtr_ContentTemplate(cms.createContentController cc){
        super(cc);
    }
	global HMR_CMS_ClinicLanderFtr_ContentTemplate() {}

	/**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
 	*/
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
 	*/
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

	global virtual override String getHTML(){
		String html = '';
		html += '<section class="hmr-page-section hmr-data-section">' +
				    '<div class="container">' +
				        '<div class="row">' +
				            '<h2>Proven Effective</h2>' +
				        '</div>' +
				        '<div class="row">' +
				            '<div class="col-sm-6">' +
				                '<div class="hmr-data-card bg-darkblue">' +
				                    '<div class="row">' +
				                        '<div class="italic first">Decision Free<span>&reg;</span> in-clinic plan</div>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<h1 class="white">43-66 lbs</h1>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<div class="hmr-allcaps hmr-data-content">At 12-26 Weeks<sup class="sup-bold">1</sup></div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				            '<div class="col-sm-6">' +
				                '<div class="hmr-data-card bg-purple">' +
				                    '<div class="row">' +
				                        '<div class="italic second">Healthy Solutions<span>&reg;</span> in-clinic plan</div>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<h1 class="white">28-37 lbs</h1>' +
				                    '</div>' +
				                    '<div class="row">' +
				                        '<div class="hmr-allcaps hmr-data-content">At 12-26 Weeks<sup class="sup-bold">2</sup></div>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				        '<div class="row">' +
				            '<p>' +
								'These are the results we get for people who completed between 12-26 weeks.<br />' +
				                '<sup class="sup-book">1</sup><a href="https://www.ncbi.nlm.nih.gov/pubmed/19631049" target="_blank">J Am Diet Assoc 2009;109:1417-1421;</a> <a href="http://www.nature.com/ijo/journal/v31/n3/full/0803423a.html" target="_blank">Int J Obes 2007;31:488-493;</a> <a href="https://www.ncbi.nlm.nih.gov/pubmed/16192259" target="_blank">J Am Coll Nutr 2005;24:347-353.</a><br/>' +
				                '<sup class="sup-book">2</sup><a href="https://www.ncbi.nlm.nih.gov/pubmed/19631049" target="_blank">J Am Diet Assoc 2009;109:1417-1421;</a> <a href="https://www.ncbi.nlm.nih.gov/pubmed/21904103" target="_blank">Postgrad Med 2011;123:205-213;</a> <a href="http://www.nature.com/ijo/journal/v31/n8/full/0803568a.html" target="_blank">Int J Obes 2007;31:1270-1276;</a> <a href="http://onlinelibrary.wiley.com/doi/10.1002/oby.20334/full" target="_blank">Obes 2013;21:1951-1959.</a><br />' +
				                'There is no guarantee of specific results. Results can vary.' +
				            '</p>' +
				        '</div>' +
				    '</div>' +
				'</section>' +
				'<section class="clinic-information-request-section">' +
				    '<div class="container-fluid">' +
				        '<div class="container">' +
				            '<div class="row">' +
				                '<div class="col-sm-9">' +
				                    '<p>' +
				                        'Request an appointment for a free information session.' +
				                    '</p>' +
				                '</div>' +
				                '<div class="col-sm-3">' +
				                    '<div class="btn-container">' +
				                        '<a id="clinicLanderFtrDialogCall" onclick="javascript:cldg.f();" data-toggle="modal" data-target="#clinicInfoModalDialog" class="btn btn-primary hmr-btn-blue hmr-btn-small">Get Started</a>' +
				                    '</div>' +
				                '</div>' +
				            '</div>' +
				        '</div>' +
				    '</div>' +
				'</section>';
		return html;
	}
}