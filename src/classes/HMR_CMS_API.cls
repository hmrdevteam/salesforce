/**
* 
* @Date: 2018-01-23
* @Author: Zach Engman (Magnet 360)
* @Modified:
* @JIRA:
*/
public with sharing virtual class HMR_CMS_API {
	@testVisible
	protected string siteName = 'HMR_Program_Community';
	@testVisible
	protected string apiVersion = '5.0';

	public HMR_CMS_API(){}
}