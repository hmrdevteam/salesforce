/*****************************************************
 * Author: Zach Engman
 * Created Date: 01/12/2018
 * Created By: Zach Engman
 * Last Modified Date: 01/12/2018
 * Last Modified By:
 * Description: Test Coverage for the HMRMobActivityHistory REST Service
 * ****************************************************/
@isTest
private class HMRMobActivityHistoryTest {
	final static integer HistoryDays = 14;

	@testSetup
	private static void setupData(){
		User userRecord = cc_dataFactory.testUser;
        Contact contactRecord = cc_dataFactory.testUser.Contact;

        Program__c programRecord = new Program__c(Name = 'Healthy Solutions', Days_in_1st_Order_Cycle__c = 30);
        insert programRecord;

        Program_Membership__c programMembershipRecord = new Program_Membership__c(Program__c = programRecord.Id, 
                                                                                  hmr_Active__c = true,
                                                                                  hmr_Status__c = 'Active',
                                                                                  hmr_Contact__c = contactRecord.Id,
                                                                                  hmr_Diet_Start_Date__c = Date.today());
        insert programMembershipRecord;

        List<ActivityLog__c> activityLogList = new List<ActivityLog__c>();
        
        for(integer i = 0; i < HistoryDays; i ++){
	         activityLogList.add(new ActivityLog__c
	         (
	        	Contact__c = contactRecord.Id
	           ,Program_Membership__c = programMembershipRecord.Id
	           ,DateDataEnteredFor__c = Date.today().addDays(-1 * i)
	           ,CurrentWeight__c = 170 + i
	           ,Entrees__c = i
	           ,FruitsVegetables__c = i
	           ,ShakesCereals__c = i
	           ,Water__c = 8
	        ));
    	}

    	insert activityLogList;

    	List<PhysicalActivity__c> physicalActivityLogList = new List<PhysicalActivity__c>();

    	for(ActivityLog__c activityLogRecord : activityLogList){
    		for(integer i = 0; i < 3; i++){
    			physicalActivityLogList.add(new PhysicalActivity__c(
    				Activity_Log__c = activityLogRecord.Id
    			   ,NameOfPhysicalActivity__c = (i == 0 ? 'Tennis' : i == 1 ? 'Jogging' : 'Swimming')
    			   ,TotalPhysicalActivityCalories__c = 100 * i
    			));
    		}
    	}

    	insert physicalActivityLogList;
	}

	@isTest
	private static void testGetActivityHistory(){
		User userRecord = [SELECT Id FROM User WHERE Alias = 'ccTest'];
		Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c
														 FROM Program_Membership__c
														 LIMIT 1];

		Date dateFrom = Date.today().addDays(-10);
		Date dateTo = Date.today();
		RestRequest request = new RestRequest();
    	RestResponse response = new RestResponse();
    	Map<String, String> parameterMap = new Map<String, String>{
    		'contactId' => programMembershipRecord.hmr_Contact__c
  		   ,'programMembershipId' => programMembershipRecord.Id
  		   ,'activityDateFrom' => dateFrom.Year() + '-' + dateFrom.Month() + '-' + dateFrom.Day()
  		   ,'activityDateTo' => dateTo.Year() + '-' + dateTo.Month() + '-' + dateTo.Day()
    	};

    	request.requestURI = '/mobile/activity/history/';
    	request.params.putAll(parameterMap);
    	request.httpMethod = 'GET';

    	RestContext.request = request;
    	RestContext.response = response;

    	Test.startTest();

    	System.runAs(userRecord) {
    		HMRMobActivityHistory.doGet();
    	}

    	Test.stopTest();

    	//Verify the data was retrieved
    	System.assertEquals(200, RestContext.response.statusCode);
	}

	@isTest
	private static void testGetActivityHistoryMissingParameters(){
		User userRecord = [SELECT Id FROM User WHERE Alias = 'ccTest'];
		Program_Membership__c programMembershipRecord = [SELECT hmr_Contact__c
														 FROM Program_Membership__c
														 LIMIT 1];

		Date dateFrom = Date.today().addDays(-10);
		Date dateTo = Date.today();
		RestRequest request = new RestRequest();
    	RestResponse response = new RestResponse();
    	Map<String, String> parameterMap = new Map<String, String>{
  		    'activityDateFrom' => dateFrom.Year() + '-' + dateFrom.Month() + '-' + dateFrom.Day()
  		   ,'activityDateTo' => dateTo.Year() + '-' + dateTo.Month() + '-' + dateTo.Day()
    	};

    	request.requestURI = '/mobile/activity/history/';
    	request.params.putAll(parameterMap);
    	request.httpMethod = 'GET';

    	RestContext.request = request;
    	RestContext.response = response;

    	Test.startTest();

    	System.runAs(userRecord) {
    		HMRMobActivityHistory.doGet();
    	}

    	Test.stopTest();

    	//Verify the error response was returned
    	System.assertEquals(404, RestContext.response.statusCode);
	}
}