/**
 ---- This class is not in use please goto HMR_CMS_ConsentAgr_ContentTemplate class ---
*
* Apex Content Template Controller for Consent Agreement
*
* @Date: 2017-04-04
* @Author Utkarsh Goswami (Mindtree)
* @Modified: Pranay Mistry 2017-01-06
* @JIRA: 
*/
global virtual with sharing class HMR_CMS_ConsentAgreement_ContentTemplate extends cms.ContentTemplateController{
  //need two constructors, 1 to initialize CreateContentController a
  global HMR_CMS_ConsentAgreement_ContentTemplate(cms.CreateContentController cc) {
        
        super(cc);
    //    getConsentData();
        
    }
    //2 no ARG constructor
    global HMR_CMS_ConsentAgreement_ContentTemplate() {
        super();
    }

    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        } 
        else {
            return property;
        }
    }
    
    public List<ccrz__E_Term__c> getConsentData(){
    
        List<ccrz__E_Term__c> consentData = [Select ccrz__Description__c,ccrz__Title__c FROM ccrz__E_Term__c WHERE Consent_Type__c = :consentType
                                            AND ccrz__Enabled__c = TRUE LIMIT 1];
        return consentData;
    
    }

    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        } 
        else {
            return getProperty(attributeName);
        }
    }

    public String consentType{    
        get{
            return getProperty('consentType');
        }    
    }
    
    public String renderType{    
        get{
            return getProperty('renderType');
        }    
    }

    public String showOnScreen{
        get{
            return getProperty('showOnScreen');
        }
    }


    global virtual override String getHTML() {    
        String html = '';

        List<ccrz__E_Term__c> consentDataOfSelectedConsent = getConsentData();
        system.debug('LIST OF Content Types');
        system.debug(consentDataOfSelectedConsent);
        
        if(!consentDataOfSelectedConsent.isEmpty()){
        
            if(renderType == 'Modal'){
                html = createConsentTypeMarkupForPopup(consentDataOfSelectedConsent[0].ccrz__Description__c,consentDataOfSelectedConsent[0].ccrz__Title__c);
            }
            
            if(renderType == 'Content'){
                html = createConsentTypeMarkupForPopup(consentDataOfSelectedConsent[0].ccrz__Description__c,consentDataOfSelectedConsent[0].ccrz__Title__c);
            }
        
        }
        return html;          
    } 
    
    public String createConsentTypeMarkupForPopup(String description, String consentTitle){
    
        String html = '';
        
        html += '<span data-toggle="modal" data-target="#termsModal" id="terms-and-condition">' + consentTitle + '</span> <br />';
        
        html += '<div id="'+ consentType + '" class="hmr-modal modal fade" role="dialog">';
            html += '<div class="modal-dialog">';
                html += '<div class="modal-content">';
                    html += '<div class="modal-header" id = "' + consentTitle + '" >';
                        html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                            html += '<span aria-hidden="true">&times;</span>';
                        html += '</button>';
                         html += '<h4 class="modal-title text-center">' + consentTitle + '</h4>';
                    html += '</div>';
                    html += '<div class="modal-body">';
                        html += '<p>';
                            html += description;
                        html += '</p>';
                    html += '</div>';
                    html += '<div class="modal-footer">';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        
        
        return html;
    
    }
    
    
    public String createConsentTypeMarkupContent(String description,String consentTitle){
    
        String html = '';
        
        html += '<div  class=" fade" role="dialog">';
            html += '<div class="modal-dialog">';
                html += '<div class="modal-content">';
                    html += '<div class="modal-header">';
                        html += '<h4 class="modal-title">' + consentTitle + '</h4>';
                    html += '</div>';
                    html += '<div class="modal-body">';
                        html += '<p>' + description;
                        html += '</p>';
                    html += '</div>';
                    html += '<div class="modal-footer">';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        
        return html;
    
    }
}