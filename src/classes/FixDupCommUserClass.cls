/**
* @Date: 2017-07-20
* @Author Mustafa Ahmed (HMR)
*/

public class FixDupCommUserClass {

	Public date beginDate {get;set;}
	Public date endDate {get;set;}
	Public List<User> todaysUsers {get;set;}	
	Public String xUsers;
	Public Set<String> xEmails = new Set<String>();
	Public Set<String> xContactId = new Set<String>();
	Public List<Contact> xtodaysUsers {get;set;}
	Public String txUsers;
	Public Set<String> txEmails = new Set<String>();
	Public List<User> txtodaysUsers {get;set;}
	

	Public FixDupCommUserClass() {
			beginDate = System.today();
			endDate = System.today();
	}

	Public PageReference getxtodaysUsers(){
		if (xEmails != null){
			xEmails.clear();	
		}	

		if (txEmails != null){
			txEmails.clear();	
		}	
		
		Datetime beginDT = datetime.newInstance(beginDate.year(), beginDate.month(),beginDate.day());

		endDate = endDate.addDays(1);

		Datetime endDT = datetime.newInstance(endDate.year(), endDate.month(),endDate.day());

		Profile commProf = [SELECT Id FROM Profile WHERE Name='HMR Customer Community User'];

		todaysUsers = [SELECT ContactId, Email FROM User WHERE ProfileId = :commProf.Id AND CreatedDate >= :beginDT AND CreatedDate <= :endDT and ContactId != ''];

		//todaysUsers = [SELECT Email FROM User WHERE ProfileId = :commProf.Id AND CreatedDate >= :beginDT AND CreatedDate <= :endDT AND CreatedById = '005410000019Emm'];

		endDate = endDate.addDays(-1);

		System.debug('the size of todays users ' + todaysUsers.size());

			for(User xUser: todaysUsers){				
					xUsers = xUser.Email;
					xEmails.add(xUsers);
					xUsers = xUser.ContactId;
					xContactId.add(xUsers);					           
	            }         
        xtodaysUsers = [SELECT id, Email, Name FROM Contact WHERE Id Not IN :xContactId AND Email IN :xEmails ORDER BY Email ASC];   

        System.debug('the size of xtodaysUsers ' + xtodaysUsers.size());

        	for(Contact txUser: xtodaysUsers){				
				txUsers = txUser.Email;
				//txUsers = txUsers.substring(0,txUsers.length()-6);
				//txUsers = txUsers.substring(2,txUsers.length());
				txEmails.add(txUsers);								           
            }
        txtodaysUsers = [SELECT ContactId, Email, Contact.Name FROM User WHERE ProfileId = :commProf.Id AND Email IN :txEmails ORDER BY Email ASC];

        System.debug('the size of txtodaysUsers ' + txtodaysUsers.size());

            return null;
	}  
	
}