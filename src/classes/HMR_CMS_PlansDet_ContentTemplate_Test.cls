/*
Developer: Utkarsh Goswami (Mindtree)
Date: June 07th, 2017
Modified: 
Modified Date : March 27th 2017
Target Class: HMR_CMS_PlansDetails_ContentTemplate
*/

@isTest
public class HMR_CMS_PlansDet_ContentTemplate_Test{  

        
    @isTest static void test_general_method() { 

        
                Program__c testProgram = new Program__c(
                    Name = 'Test Solutions',
                    hmr_Has_Phone_Coaching__c = false,
                    hmr_AD_Mandatory_For_Phone__c = false,
                    hmr_Phase_Type__c = 'P1',
                    hmr_Program_Description__c = 'Program Test Description',
                    hmr_Program_Display_Name__c = 'P1 Test Program',
                    hmr_Program_Type__c = 'Healthy Solutions',
                    Days_in_1st_Order_Cycle__c = 21
                );
        
                insert testProgram;
                
                Program__c testProgram2 = new Program__c(
                    Name = 'Test Shakes',
                    hmr_Has_Phone_Coaching__c = false,
                    hmr_AD_Mandatory_For_Phone__c = false,
                    hmr_Phase_Type__c = 'P1',
                    hmr_Program_Description__c = 'Program Test Description',
                    hmr_Program_Display_Name__c = 'P1 Test Program',
                    hmr_Program_Type__c = 'Healthy Solutions',
                    Days_in_1st_Order_Cycle__c = 21
                );
        
                insert testProgram2;
                
                HMR_CMS_PlansDetails_ContentTemplate plansPageTemplate = new HMR_CMS_PlansDetails_ContentTemplate();
                String propertyValue = plansPageTemplate.getPropertyWithDefault('somePropertyValue','DefaultValueofProperty');
                
                 plansPageTemplate.testAttributes = new Map<String, String> {
                    'PlansDetailsTitle' => 'title',
                    'PlansDetailsDescription' => 'desc',
                    'ProgramType' => 'HSAH'
                };

                String PlansDetailsTitle = plansPageTemplate.PlansDetailsTitle;                
                String PlansDetailsDescription = plansPageTemplate.PlansDetailsDescription;
                String ProgramType = plansPageTemplate.ProgramType;

                String renderHMTL = plansPageTemplate.getHTML();
                
                plansPageTemplate.testAttributes = new Map<String, String> {
                    'PlansDetailsTitle' => 'title',
                    'PlansDetailsDescription' => 'desc',
                    'ProgramType' => 'HSS'
                };
                
                String renderHTML = plansPageTemplate.getHTML();
                
                System.assert(!String.isBlank(renderHTML));
                
    }
    
}