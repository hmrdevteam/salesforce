global virtual with sharing class HMR_CMS_KitConfig_Hero_ContentTemplate extends cms.ContentTemplateController{
    global HMR_CMS_KitConfig_Hero_ContentTemplate(){}
	global HMR_CMS_KitConfig_Hero_ContentTemplate(cms.createContentController cc){
        super(cc);
    }


    /**
     * A shorthand to retrieve a default value for a property if it hasn't been saved.
     *
     * @param propertyName the property name, passed directly to getProperty
     * @param defaultValue the default value to use if the retrieved property is null
     */
    @TestVisible
    private String getPropertyWithDefault(String propertyName, String defaultValue) {
        String property = getAttribute(propertyName);
        if(property == null) {
            return defaultValue;
        }
        else {
            return property;
        }
    }
    /** Provides an easy way to define attributes during testing */
    @TestVisible
    private Map<String, String> testAttributes = new Map<String, String>();

    /** Delegates to ContentTemplateController#getProperty in normal execution. Accesses the {@link #testAttributes}
     * map in a test context.
     */
    private String getAttribute(String attributeName) {
        if(Test.isRunningTest()) {
            return testAttributes.get(attributeName);
        }
        else {
            return getProperty(attributeName);
        }
    }

    public boolean IsProduction() {
        return ( UserInfo.getOrganizationId() == '00D410000008Uj1EAE' );
    }

    public String kitName{
        get{
           return getAttribute('kitName');
       }
    }

    public String kitHeading{
        get{
           return getAttribute('kitHeading');
       }
    }

    public String heroImageURL{
        get{
           return getAttribute('heroImageURL');
       }
    }
    public String detailsLine1{
        get{
           return getAttribute('detailsLine1');
       }
    }
    public String detailsLine2{
        get{
           return getAttribute('detailsLine2');
       }
    }
    public String detailsLine3{
        get{
           return getAttribute('detailsLine3');
       }
    }
    public String detailsLine4{
        get{
           return getAttribute('detailsLine4');
       }
    }
    public String detailsLine5{
        get{
           return getAttribute('detailsLine5');
       }
    }

    global String returnNotNullAndNotEmptyString(String value) {
        if(value != null && value != '') {
            return value;
        }
      return '';
    }

    global virtual override String getHTML() {
        String html = '';
        String baseUrl = IsProduction() ? '' : Site.getBaseUrl();
        Set<String> kitContentDetails = new Set<String>{detailsLine1, detailsLine2, detailsLine3, detailsLine4, detailsLine5};
        String hssClass = ' kit-config-img-shakes';String hssHero = ' shakes-hero';
        if(!kitName.containsIgnoreCase('HEALTHY SHAKES')) {
            hssClass = '';hssHero = '';
        }
        try{
             html += '<section class="kit-config-hero' + hssHero + '">'+
                         '<div class="container-fluid">'+
                             '<div class="row contents">'+
                                 '<div class="col-lg-1 col-md-1 visible-lg visible-md">'+
                                 '</div>'+
                                 '<div class="col-sm-12 col-xs-12 hidden-lg hidden-md visible-sm visible-xs kit-config-img bg-typecontain' + hssClass + '">'+
                                 '</div>'+
                                 '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">'+
                                     '<div class="row">'+
                                         '<div class="col-lg-12 hmr-allcaps white">'+
                                             kitName +
                                         '</div>'+
                                     '</div>'+
                                     '<div class="row">'+
                                         '<div class="col-lg-12 white">'+
                                             '<h2>' + kitHeading + '</h2>'+
                                         '</div>'+
                                     '</div>'+
                                     '<div class="row">'+
                                         '<div class="col-lg-12 white">'+
                                             '<ul>';
                                             for(String contentDetail : kitContentDetails){
                                                 if(returnNotNullAndNotEmptyString(contentDetail) != ''){
                                                     html += '<li>' + contentDetail +'</li>';
                                                 }
                                             }
                                             html += '</ul>'+
                                         '</div>'+
                                     '</div>'+
                                 '</div>'+
                                 '<div class="col-lg-7 col-md-7 visible-lg visible-md hidden-sm hidden-xs kit-config-img bg-typecontain' + hssClass + '">'+
                                 '</div>'+
                             '</div>'+
                         '</div>'+
                     '</section>';
        }
        catch(Exception ex){
            html += 'Something went wrong in Kit Config Hero ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
        }
      return html;
    }
}