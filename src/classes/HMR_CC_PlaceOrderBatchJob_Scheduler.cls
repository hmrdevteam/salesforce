/*****************************************************
 * Author: Joey Zhuang
 * Created Date: 10 May 2017
 * Created By: Joey Zhuang
 * Last Modified Date: 
 * Last Modified By: 
 * Description:

 	Processes all the Orders 
    	Only when the Order.Contact.Consent is checked true
	    	with status as Pending and Order Date of today (You can only do 10 callouts per batch Job)
	        What exactly is processing an order
	            Process a Cybersource Transaction
	                update existing transaction Payment Record with appropriate status
	                    Success                        
	                        Change the status of Order itself to "Order Submittted"
	                        
	                    Failure
	                        There is a count of Credit Card declines/Failures (Roll Up summary on the order record) 
	    else 
	    	update the Program Membership record related to the Order. You just set the New_Phase__c field to 'Drop' and the trigger will take over. 
	    	update: Change Reason = "No Consent"

 * ****************************************************/
global class HMR_CC_PlaceOrderBatchJob_Scheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		try {
			HMR_CC_PlaceOrderBatchJob b = new HMR_CC_PlaceOrderBatchJob();
			database.executebatch(b,10);
		}
		catch(Exception ex) {
			System.debug('The HMR_CC_PlaceOrderBatchJob_Scheduler failed ' + ex.getMessage());
		}		
	}
}